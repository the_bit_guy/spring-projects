/**
 * 
 */
package com.bb.springboot.mongodb.components;

/**
 * @author Bhabadyuti Bal
 *
 * Pojo for cart entry items
 */
public class ShoppingCartEntry {

	private String imageUrl;
	private String productName;
	private double price;
	private int quantity;
	private double productTotalPrice;
	
	public ShoppingCartEntry() {
	}
	
	public ShoppingCartEntry(String imageUrl, String productName, double price, int quantity,
			double productTotalPrice) {
		this.imageUrl = imageUrl;
		this.productName = productName;
		this.price = price;
		this.quantity = quantity;
		this.productTotalPrice = productTotalPrice;
	}

	/**
	 * @return the imageUrl
	 */
	public String getImageUrl() {
		return imageUrl;
	}

	/**
	 * @param imageUrl the imageUrl to set
	 */
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	/**
	 * @return the productName
	 */
	public String getProductName() {
		return productName;
	}

	/**
	 * @param productName the productName to set
	 */
	public void setProductName(String productName) {
		this.productName = productName;
	}

	/**
	 * @return the price
	 */
	public double getPrice() {
		return price;
	}

	/**
	 * @param price the price to set
	 */
	public void setPrice(double price) {
		this.price = price;
	}

	/**
	 * @return the quantity
	 */
	public int getQuantity() {
		return quantity;
	}

	/**
	 * @param quantity the quantity to set
	 */
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	/**
	 * @return the productTotalPrice
	 */
	public double getProductTotalPrice() {
		return productTotalPrice;
	}

	/**
	 * @param productTotalPrice the productTotalPrice to set
	 */
	public void setProductTotalPrice(double productTotalPrice) {
		this.productTotalPrice = productTotalPrice;
	}
	
	
}
