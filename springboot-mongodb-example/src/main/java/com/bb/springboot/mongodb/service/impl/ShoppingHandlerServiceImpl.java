/**
 * 
 */
package com.bb.springboot.mongodb.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.bb.springboot.mongodb.components.ShoppingCartEntry;
import com.bb.springboot.mongodb.components.ShoppingCartMap;
import com.bb.springboot.mongodb.document.Product;
import com.bb.springboot.mongodb.service.PricingStrategyService;
import com.bb.springboot.mongodb.service.ProductService;
import com.bb.springboot.mongodb.service.ShoppingHandlerService;
import com.bb.springboot.mongodb.utils.PriceUtilities;

/**
 * @author Bhabadyuti Bal
 *
 */
@Service
public class ShoppingHandlerServiceImpl implements ShoppingHandlerService {

	@Autowired
	@Qualifier("productServiceImpl")
	private ProductService productService;
	
	@Autowired
	private PricingStrategyService pricingStrategyService;

	@Override
	public List<ShoppingCartEntry> getShoppingCartEntries(ShoppingCartMap shoppingCartMap) {
		List<ShoppingCartEntry> shoppingCarts = new ArrayList<>();

		for (String productCode : shoppingCartMap.getCartItems().keySet()) {
			Product p = productService.findByProductCode(productCode);

			ShoppingCartEntry s = new ShoppingCartEntry();

			int quantity = shoppingCartMap.getQuantity(productCode);

			s.setImageUrl(p.getImageUrl());
			s.setProductName(p.getProductName());
			s.setPrice(p.getPrice());
			s.setProductTotalPrice(p.getPrice() * quantity);
			s.setQuantity(quantity);
			shoppingCarts.add(s);
		}
		return shoppingCarts;
	}

	@Override
	public String getTotalPrice(List<ShoppingCartEntry> shoppingCartEntries) {

		return PriceUtilities.roundToTwoDecimalPlaces(pricingStrategyService.getTotal(shoppingCartEntries));
	}

	@Override
	public String getTotalTax(List<ShoppingCartEntry> shoppingCartEntries) {
		return PriceUtilities.roundToTwoDecimalPlaces(pricingStrategyService.getTotal(shoppingCartEntries));
	}

}
