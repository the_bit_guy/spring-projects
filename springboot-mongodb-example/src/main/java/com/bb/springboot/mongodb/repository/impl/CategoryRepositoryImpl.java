/**
 * 
 */
package com.bb.springboot.mongodb.repository.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Repository;

import com.bb.springboot.mongodb.repository.CategoryRepository;

/**
 * @author Bhabadyuti Bal
 *
 */
@Repository
public class CategoryRepositoryImpl implements CategoryRepository {

	@Autowired
	MongoTemplate mongoTemplate;
	
	/* (non-Javadoc)
	 * @see com.bb.springboot.mongodb.repository.CategoryRepository#findAll()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<String> findAll() {
		return mongoTemplate.getCollection("products").distinct("categories");
	}

}
