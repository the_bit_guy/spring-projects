/**
 * 
 */
package com.bb.springboot.mongodb.utils;

import java.util.List;

import com.bb.springboot.mongodb.components.ShoppingCartEntry;
import com.bb.springboot.mongodb.service.PricingStrategyService;

/**
 * @author Bhabadyuti Bal
 *
 */
public class PriceStrategyTax implements PricingStrategyService{

	@Override
	public double getTotal(List<ShoppingCartEntry> shoppingCartEntries) {double total = 0.0;
	
	for(ShoppingCartEntry e : shoppingCartEntries) {
		total += e.getProductTotalPrice() * 0.18;
	}
	
	return total;}

}
