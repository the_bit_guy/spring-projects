/**
 * 
 */
package com.bb.springboot.mongodb.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.bb.springboot.mongodb.document.Users;

/**
 * @author Bhabadyuti Bal
 *
 */
public interface UsersRepository extends MongoRepository<Users, Integer>{

	
}
