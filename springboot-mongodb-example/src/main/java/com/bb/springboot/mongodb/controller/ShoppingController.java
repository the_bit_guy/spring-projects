/**
 * 
 */
package com.bb.springboot.mongodb.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.bb.springboot.mongodb.components.ShoppingCartEntry;
import com.bb.springboot.mongodb.components.ShoppingCartMap;
import com.bb.springboot.mongodb.service.ShoppingHandlerService;

/**
 * @author Bhabadyuti Bal
 *
 */
@Controller
public class ShoppingController {
	
	
	@Autowired
	private ShoppingHandlerService shoppingHandlerService;
	
	@Autowired
	private ShoppingCartMap shoppingCartMap;
	
	@RequestMapping(value = "/shopping-cart")
	public ModelAndView shoppingCart() {
		ModelAndView model = new ModelAndView("shopping-cart");
		
		List<ShoppingCartEntry> shoppingCartEntries = shoppingHandlerService.getShoppingCartEntries(shoppingCartMap);
		
		model.addObject("shoppingCartEnries", shoppingCartEntries);
		model.addObject("shoppingItemSize", shoppingCartMap.getItemSize());
		model.addObject("totalPrice", shoppingHandlerService.getTotalPrice(shoppingCartEntries));
		model.addObject("taxPrice", shoppingHandlerService.getTotalTax(shoppingCartEntries));
		
		return model;
	}
}
