package com.bb.springboot.mongodb.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@Configuration
@EnableMongoRepositories(basePackages="com.bb.springboot.mongodb.repository")
public class MongoDbConfig {

	
}
