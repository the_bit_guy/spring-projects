/**
 * 
 */
package com.bb.springboot.mongodb.service;

import java.util.List;

import com.bb.springboot.mongodb.document.Product;

/**
 * @author Bhabadyuti Bal
 *
 */
public interface ProductService {

	public List<Product> findAllProducts();
	
	public Product findByProductCode(String productCode);

	public List<Product> findProductsByCategory(String categoryName);

	public List<Product> findProductsByName(String searchString);
}
