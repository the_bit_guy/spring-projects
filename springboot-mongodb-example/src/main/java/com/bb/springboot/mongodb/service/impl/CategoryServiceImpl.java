/**
 * 
 */
package com.bb.springboot.mongodb.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bb.springboot.mongodb.repository.CategoryRepository;
import com.bb.springboot.mongodb.service.CategoryService;

/**
 * @author Bhabadyuti Bal
 *
 */
@Service
public class CategoryServiceImpl implements CategoryService {
	
	@Autowired
	private CategoryRepository categoryRepository;

	@Override
	public List<String> findAllCategories() {
		List<String> categories = categoryRepository.findAll();
		return categories;
	}

}
