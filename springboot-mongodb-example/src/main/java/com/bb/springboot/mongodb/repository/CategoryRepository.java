/**
 * 
 */
package com.bb.springboot.mongodb.repository;

import java.util.List;

/**
 * @author Bhabadyuti Bal
 *
 */
public interface CategoryRepository {

	public List<String> findAll();
	
}
