/**
 * 
 */
package com.bb.springboot.mongodb.document;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Bhabadyuti Bal
 *
 */
@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode
@Document(collection="products")
public class Product {

	@Id private String id;
	private List<String> categories;
	private String productCode;
	private String productName;
	private String imageUrl;
	private double price;
	private int size;
	private String color;
	private List<String> productTags; 
	
	
}
