/**
 * 
 */
package com.bb.springboot.mongodb.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.bb.springboot.mongodb.document.Product;
import com.bb.springboot.mongodb.repository.ProductRepository;
import com.bb.springboot.mongodb.service.ProductService;

/**
 * @author Bhabadyuti Bal
 *
 */
@Service
@Qualifier("productSafeServiceImpl")
public class ProductSafeServiceImpl implements ProductService {
	
	@Autowired
	ProductRepository productRepo;

	@Override
	public List<Product> findAllProducts() {
		List<Product> findAllList = this.productRepo.findAll();
		return findAllList;
	}

	@Override
	public Product findByProductCode(String productCode) {
		return null;
	}

	@Override
	public List<Product> findProductsByCategory(String categoryName) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Product> findProductsByName(String searchString) {
		// TODO Auto-generated method stub
		return null;
	}

}
