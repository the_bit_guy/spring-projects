package com.bb.springboot.mongodb.document;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 
 * @author Bhabadyuti Bal
 *
 */
@Getter
@Setter
@ToString
@EqualsAndHashCode
@AllArgsConstructor
@Document
public class Users {

	@Id
	private Integer id;
	private String firstName;
	private String lastName;
	private String emial;
	private String username;
	private String password;
	private String address;
	
	
	
}
