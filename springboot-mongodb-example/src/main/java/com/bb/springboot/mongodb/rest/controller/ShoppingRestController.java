/**
 * 
 */
package com.bb.springboot.mongodb.rest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bb.springboot.mongodb.components.ShoppingCartMap;

/**
 * @author Bhabadyuti Bal
 *
 */
@RestController
@RequestMapping(value = "/cartServices")
public class ShoppingRestController {

	
	@Autowired
	private ShoppingCartMap shoppingCartMap;
	
	@RequestMapping("/addToCart")
	public void addToCart(
			@RequestParam(value = "productCode") String productCode, 
			@RequestParam(value = "quantity") int quantity
	) {
		shoppingCartMap.addItem(productCode, quantity);
	}
}
