/**
 * 
 */
package com.bb.springboot.mongodb.service;

import java.util.List;

/**
 * @author Bhabadyuti Bal
 *
 */
public interface CategoryService {
	
	public List<String> findAllCategories();

}
