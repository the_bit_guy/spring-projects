package com.bb.springboot.mongodb.controller;

import java.util.Collections;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.bb.springboot.mongodb.components.ShoppingCartMap;
import com.bb.springboot.mongodb.document.Product;
import com.bb.springboot.mongodb.repository.ProductRepository;
import com.bb.springboot.mongodb.service.CategoryService;
import com.bb.springboot.mongodb.service.ProductService;


@RestController
@RequestMapping(value="/products")
public class ProductsController {
	
	@Resource
	@Qualifier("productSafeServiceImpl")
	private ProductService productService;
	
	@Autowired
	private CategoryService categoryService;
	
	@Autowired
	private ProductRepository productRepository;
	
	// session scoped POJOs
	@Autowired
	private ShoppingCartMap shoppingCartMap;

	@GetMapping("/all")
	public List<Product> findAllProducts(){
		List<Product> products = productRepository.findAll();
		List<Product> synchronizedList = Collections.synchronizedList(products);
		return synchronizedList;
	}
	
	
	@RequestMapping(value = "/")
	public ModelAndView listProducts() {
		
		List<String> categories = categoryService.findAllCategories();
		List<Product> products = productService.findAllProducts();
		
		ModelAndView model = new ModelAndView("products");
		
		model.addObject("productList", products);
		model.addObject("categoryList", categories);
		
		return model;
	}
	
	/*
	 * Product with search string
	 */
	@RequestMapping(value = "/products", params="srch-term")
	public ModelAndView listProductsByNameSearch(@RequestParam("srch-term") String searchTerm) {
		List<Product> products = productService.findProductsByName(searchTerm);
		List<String> categories = categoryService.findAllCategories();
		
		ModelAndView model = new ModelAndView("products");
		
		model.addObject("categoryList", categories);
		model.addObject("productList", products);
		
		return model;
	}

	
	@RequestMapping(value = "/products-by-category-{categoryName}")
	public ModelAndView listProductsByCategory(@PathVariable("categoryName") String categoryName) {
		List<Product> products = productService.findProductsByCategory(categoryName);
		List<String> categories = categoryService.findAllCategories();
		
		ModelAndView model = new ModelAndView("products");
		
		model.addObject("categoryList", categories);
		model.addObject("productList", products);
		
		return model;
	}
	
	@RequestMapping(value = "/product-details-{productCode}")
	public ModelAndView listProductById(@PathVariable("productCode") String productCode) {
		
		Product product = productService.findByProductCode(productCode);
		
		ModelAndView model = new ModelAndView("product-details");
		model.addObject("product", product);
		model.addObject("shoppingCartMap", shoppingCartMap);
		
		return model;
	}

}
