package com.bb.springboot.mongodb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootMongodbExampleApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootMongodbExampleApplication.class, args);
	}
}
