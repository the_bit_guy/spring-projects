/**
 * 
 */
package com.bb.springboot.mongodb.rest.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.bb.springboot.mongodb.document.Product;
import com.bb.springboot.mongodb.service.ProductService;

/**
 * @author Bhabadyuti Bal
 *
 */
@RestController
@RequestMapping(value = "/productServices")
public class ProductsRestController {
	
	@Autowired
	@Qualifier("productServiceImpl")
	private ProductService productService;
	
	@RequestMapping(value = "/getProductsByCategories/{categoryName}", method = RequestMethod.GET)
	public @ResponseBody List<Product> getProductById(@PathVariable String categoryName) {
		return productService.findProductsByCategory(categoryName);
	}
	
	@RequestMapping(value = "/getProductByProductCode/{productCode}", method = RequestMethod.GET)
	public @ResponseBody Product getProductByProductCode(@PathVariable String productCode) {
		return productService.findByProductCode(productCode);
	}
}
