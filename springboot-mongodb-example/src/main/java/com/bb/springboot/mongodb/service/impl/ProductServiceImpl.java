/**
 * 
 */
package com.bb.springboot.mongodb.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.bb.springboot.mongodb.document.Product;
import com.bb.springboot.mongodb.repository.ProductRepository;
import com.bb.springboot.mongodb.service.ProductService;

/**
 * @author Bhabadyuti Bal
 *
 */
@Service
@Qualifier("productServiceImpl")
public class ProductServiceImpl implements ProductService {
	
	
	
	@Autowired
	private ProductRepository productRepository;
	
	@Override
	public List<Product> findAllProducts() {
		return productRepository.findAll();
	}

	@Override
	public Product findByProductCode(String productCode) {
		return productRepository.findByProductCode(productCode);
	}
	
	@Override
	public List<Product> findProductsByCategory(String categoryName) {
		return productRepository.findProductsByCategory(categoryName);
	}

	@Override
	public List<Product> findProductsByName(String searchString) {
		return productRepository.findProductsByProductNameRegex(searchString);
	}
	
}
