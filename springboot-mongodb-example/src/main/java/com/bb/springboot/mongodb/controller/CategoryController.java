/**
 * 
 */
package com.bb.springboot.mongodb.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bb.springboot.mongodb.service.CategoryService;

/**
 * @author Bhabadyuti Bal
 *
 */
@RestController
public class CategoryController {
	
	@Autowired
	private CategoryService categoryService;

	@RequestMapping(value = "/getCategories", method = RequestMethod.GET)
	public List<String> getCategories() {
		return categoryService.findAllCategories();
	}
}
