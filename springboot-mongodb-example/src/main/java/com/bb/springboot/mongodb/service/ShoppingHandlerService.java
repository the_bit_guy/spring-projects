/**
 * 
 */
package com.bb.springboot.mongodb.service;

import java.util.List;

import com.bb.springboot.mongodb.components.ShoppingCartEntry;
import com.bb.springboot.mongodb.components.ShoppingCartMap;

/**
 * @author Bhabadyuti Bal
 *
 */
public interface ShoppingHandlerService {

	public List<ShoppingCartEntry> getShoppingCartEntries(ShoppingCartMap shoppingCartMap);

	public String getTotalPrice(List<ShoppingCartEntry> shoppingCartEntries);

	public String getTotalTax(List<ShoppingCartEntry> shoppingCartEntries);

}
