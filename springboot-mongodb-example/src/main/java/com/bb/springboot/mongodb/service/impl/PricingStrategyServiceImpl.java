/**
 * 
 */
package com.bb.springboot.mongodb.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.bb.springboot.mongodb.components.ShoppingCartEntry;
import com.bb.springboot.mongodb.service.PricingStrategyService;

/**
 * @author Bhabadyuti Bal
 *
 */
@Service
public class PricingStrategyServiceImpl implements PricingStrategyService {

	/* 
	 * @see com.bb.springboot.mongodb.service.PricingStrategyService#getTotal(java.util.List)
	 * calculates the total price of cart items
	 */
	@Override
	public double getTotal(List<ShoppingCartEntry> shoppingCartEntries) {
		
		double total = 0.0;
		for (ShoppingCartEntry shoppingCartEntry : shoppingCartEntries) {
			total = total + shoppingCartEntry.getPrice();
		}
		return total;
	}

}
