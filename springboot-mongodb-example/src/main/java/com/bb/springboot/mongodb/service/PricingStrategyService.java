/**
 * 
 */
package com.bb.springboot.mongodb.service;

import java.util.List;

import com.bb.springboot.mongodb.components.ShoppingCartEntry;

/**
 * @author Bhabadyuti Bal
 *
 */
public interface PricingStrategyService {

	public double getTotal(List<ShoppingCartEntry> shoppingCartEntries);
}
