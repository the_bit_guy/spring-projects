/**
 * 
 */
package com.sb.ecom.enums;

/**
 * @author Bhabadyuti Bal
 *
 */

public enum Quantity {

	WEIGHT("weight"),
	UNITS("units");
	
	private String quantity;
	
	private Quantity(String quantity) {
		this.quantity = quantity;
	}
	
	public String toString() {
		return this.quantity;
	}
}
