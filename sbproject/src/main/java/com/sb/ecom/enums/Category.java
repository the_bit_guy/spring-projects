/**
 * 
 */
package com.sb.ecom.enums;

/**
 * @author Bhabadyuti Bal
 *
 */
public enum Category {

	MAIN_CAT("main-cat"),
	SUB_CAT("sub-cat");
	
	private String category; 
	
	private Category(String category) {
		this.category = category;
	}
	
	@Override
	public String toString() {
		return this.category;
	}
}
