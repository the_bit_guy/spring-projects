/**
 * 
 */
package com.sb.ecom.enums;

/**
 * @author Bhabadyuti Bal
 *
 */
public enum Benefits {

	NUTRITION_FACT("nutritionalFacts"),
	BENEFIT_FACT("benefitsFacts");
	
	private String benefits;
	
	private Benefits(String benefits) {
		this.benefits = benefits;
	}
	
	@Override
	public String toString() {
		return this.benefits;
	}
}
