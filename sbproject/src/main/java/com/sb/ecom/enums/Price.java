/**
 * 
 */
package com.sb.ecom.enums;

/**
 * @author Bhabadyuti Bal
 *
 */
public enum Price {

	RETAIL_PRICE("retailPrice"),
	SALE_PRICE("salePrice");
	
	private String price;
	
	private Price(String price) {
		this.price = price;
	}
	
	public String toString() {
		return this.price;
	}
}
