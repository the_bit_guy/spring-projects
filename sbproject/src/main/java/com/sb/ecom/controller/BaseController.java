/**
 * 
 */
package com.sb.ecom.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author Bhabadyuti Bal
 *
 */
@Controller
public class BaseController {
	

	@RequestMapping("/")
	public String index() {
		return "index";
	}
	
	
	@RequestMapping("/about")
	public String about() {
		return "about";
	}
	
}
