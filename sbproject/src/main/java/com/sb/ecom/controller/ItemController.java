/**
 * 
 */
package com.sb.ecom.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;

/**
 * @author Bhabadyuti Bal
 *
 */
@Slf4j
@RestController
@RequestMapping(value="/items")
public class ItemController {

	@GetMapping("/findAll")
	public @ResponseBody List<String> getAllItems(){
		log.info(">> findALl");
		log.info("Current Thread findAll: {}", Thread.currentThread());
		List<String> list = new ArrayList<String>();
		for (int i = 0; i < 1000000; i++) {
			list.add("ssssss");
		}
		String[] array = new String[] {"abc", "def", "edf"};
		List<String> arrLuat = Arrays.asList(array);
		list.addAll(arrLuat);
		log.info("<< findALl");
		return list;
	}
	
	@GetMapping("/findById/{id}")
	public @ResponseBody String getAllItems(@PathVariable("id") String id){
		log.info(">> findById");
		log.info("Current Thread findById: {}", Thread.currentThread());
		log.info("<< findById");
		return id;
	}
	
}
