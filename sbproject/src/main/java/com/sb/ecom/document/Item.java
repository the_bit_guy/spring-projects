/**
 * 
 */
package com.sb.ecom.document;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.sb.ecom.vo.ItemBenefits;
import com.sb.ecom.vo.ItemCategory;
import com.sb.ecom.vo.ItemPrice;
import com.sb.ecom.vo.ItemQuantity;

import lombok.Getter;
import lombok.Setter;

/**
 * @author Bhabadyuti Bal
 *
 */
@Getter
@Setter
@Document(collection="items")
public class Item implements Serializable  {

	private static final long serialVersionUID = 1L;
	
	@Id
	private String id;
	private String uniqueId;
	private String title;
	private String doctype;
	private String shortDesc;
	private String longDesc;
	private ItemCategory itemCategory;
	private ItemQuantity itemQty;
	private List<String> itemImageUrls;
	private ItemPrice itemPrice;
	private String discountPercentage;
	private String totalInventory;
	private String seasonTag;
	private String isActive;
	private String reviewScore;
	private String reviewText;
	private ItemBenefits benefits;
	
	
	public Item() {
	}
	
	/**
	 * @param title
	 * @param sku
	 * @param doctype
	 * @param shortDesc
	 * @param longDesc
	 * @param itemCategory
	 * @param itemQty
	 * @param itemImageUrls
	 * @param itemPrice
	 * @param discountPercentage
	 * @param totalInventory
	 * @param seasonTag
	 * @param isActive
	 * @param reviewScore
	 * @param reviewText
	 * @param benefits
	 */
	public Item(String title, String doctype, String shortDesc, String longDesc, ItemCategory itemCategory,
			ItemQuantity itemQty, List<String> itemImageUrls, ItemPrice itemPrice, String discountPercentage,
			String totalInventory, String seasonTag, String isActive, String reviewScore, String reviewText,
			ItemBenefits benefits) {
		this.title = title;
		this.doctype = doctype;
		this.shortDesc = shortDesc;
		this.longDesc = longDesc;
		this.itemCategory = itemCategory;
		this.itemQty = itemQty;
		this.itemImageUrls = itemImageUrls;
		this.itemPrice = itemPrice;
		this.discountPercentage = discountPercentage;
		this.totalInventory = totalInventory;
		this.seasonTag = seasonTag;
		this.isActive = isActive;
		this.reviewScore = reviewScore;
		this.reviewText = reviewText;
		this.benefits = benefits;
	}

}
