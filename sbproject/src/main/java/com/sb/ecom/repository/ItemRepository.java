/**
 * 
 */
package com.sb.ecom.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.sb.ecom.document.Item;

/**
 * @author Bhabadyuti Bal
 *
 */
@Repository
public interface ItemRepository extends MongoRepository<Item, String> {

}
