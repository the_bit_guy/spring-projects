/**
 * 
 */
package com.sb.ecom.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sb.ecom.document.Item;
import com.sb.ecom.repository.ItemRepository;
import com.sb.ecom.service.ItemService;

import lombok.extern.slf4j.Slf4j;

/**
 * @author Bhabadyuti Bal
 *
 */
@Slf4j
@Service
public class ItemServiceImpl implements ItemService {
	
	@Autowired
	ItemRepository itemRepository;

	@Override
	public List<Item> findAll() {
		log.info(">> getAllItems ");
		return this.itemRepository.findAll();
	}

}
