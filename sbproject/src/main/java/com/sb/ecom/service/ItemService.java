/**
 * 
 */
package com.sb.ecom.service;

import java.util.List;

import com.sb.ecom.document.Item;

/**
 * @author Bhabadyuti Bal
 *
 */
public interface ItemService {

	List<Item> findAll();
}
