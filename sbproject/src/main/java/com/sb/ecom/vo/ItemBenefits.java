/**
 * 
 */
package com.sb.ecom.vo;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * @author Bhabadyuti Bal
 *
 */
@Getter @Setter
@ToString @NoArgsConstructor
public class ItemBenefits {
	
	
	private String nutritionalFacts;
	private String benefitsFacts;
	

	/**
	 * @param nutritionalFacts
	 * @param benefitsFacts
	 */
	public ItemBenefits(String nutritionalFacts, String benefitsFacts) {
		super();
		this.nutritionalFacts = nutritionalFacts;
		this.benefitsFacts = benefitsFacts;
	}

}
