/**
 * 
 */
package com.sb.ecom.vo;

import java.io.Serializable;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * @author Bhabadyuti Bal
 *
 */
@Getter @Setter
@ToString @NoArgsConstructor
public class ItemPrice implements Serializable{

	private static final long serialVersionUID = 1L;
	
	
	private String retailPrice;
	private SalePrice salePrice;

	/**
	 * @param retailPrice
	 * @param salePrice
	 */
	public ItemPrice(String retailPrice, SalePrice salePrice) {
		this.retailPrice = retailPrice;
		this.salePrice = salePrice;
	}

	@Getter @Setter
	static class SalePrice {
		String price;
		String saleEndDate;
	}
}
