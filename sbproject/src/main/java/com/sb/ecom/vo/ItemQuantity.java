/**
 * 
 */
package com.sb.ecom.vo;

import java.io.Serializable;
import java.util.List;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;



/**
 * @author Bhabadyuti Bal
 *
 */
@Getter @Setter
@ToString @NoArgsConstructor
public class ItemQuantity implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<String> qtyAsWeight;
	private List<String> qtyAsUnits;
	
	/**
	 * @param qtyAsWeight
	 * @param qtyAsUnits
	 */
	public ItemQuantity(List<String> qtyAsWeight, List<String> qtyAsUnits) {
		this.qtyAsWeight = qtyAsWeight;
		this.qtyAsUnits = qtyAsUnits;
	}

}
