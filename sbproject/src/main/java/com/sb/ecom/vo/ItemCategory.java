/**
 * 
 */
package com.sb.ecom.vo;

import java.io.Serializable;
import java.util.List;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * @author Bhabadyuti Bal
 *
 */
@Getter @Setter
@ToString @NoArgsConstructor
public class ItemCategory implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	List<String> mainCategories;
	List<String> subCategories;
	
	/**
	 * @param mainCategories
	 * @param subCategories
	 */
	public ItemCategory(List<String> mainCategories, List<String> subCategories) {
		this.mainCategories = mainCategories;
		this.subCategories = subCategories;
	}

}
