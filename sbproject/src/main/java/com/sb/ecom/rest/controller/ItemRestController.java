/**
 * 
 */
package com.sb.ecom.rest.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.sb.ecom.document.Item;
import com.sb.ecom.service.ItemService;

import lombok.extern.slf4j.Slf4j;

/**
 * @author Bhabadyuti Bal
 *
 */
@Slf4j
@RestController
public class ItemRestController {

	@Autowired
	private ItemService itemService;
	
	@GetMapping("/getItems")
	public @ResponseBody List<Item> getAllItems(HttpServletRequest httpServletRequest) {
		log.info("> getAllItems ");
		return this.itemService.findAll();
	}
	
}
