/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bluetree.smssender;

/**
 *
 * @author Raghu
 */
public class SMSRequest {
 
    private Long id;
    private String mobileNumber;
    private String message;
    private String status;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    
    
}
