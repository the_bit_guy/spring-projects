package com.bluetree.smssender;

import com.bluetree.repository.SMSStore;
import com.bluetree.utils.Constants;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
/**
 * Hello world!
 *
 */
public class App {

    private static ClassPathXmlApplicationContext ctx;
    Logger logger = Logger.getLogger(this.getClass());
    public static void main(String[] args) {
        new App().start();
    }

    public void start() {
        // open/read the application context file
        ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
        SMSStore smsStore = (SMSStore) ctx.getBean(Constants.SMSSTORE);
        while(true){
            List<SMSRequest> smsRequests = smsStore.findSMSByStatusNew();
            for(SMSRequest sr: smsRequests){
                SMSSender ss = new SMSSender(smsStore.getSendUrl(), smsStore.getUsername(), smsStore.getPassword(), smsStore.getSender(),
                        smsStore.getToLabel(), smsStore.getFromLabel(), smsStore.getMessageLabel(), smsStore.getUsernameLabel(),
                        smsStore.getPasswordLabel());
                ss.setSmsStore(smsStore);
                try {
                    updateSMSSendProgress(sr.getId(), smsStore);
                    ss.sendSMS(sr.getMobileNumber(), sr.getMessage(), sr.getId().toString());
                } catch (Exception ex) {
                    logger.error("smsrequest:"+sr.getMobileNumber(), ex);
                }
            }
            smsRequests = null;
            try {
                logger.debug("sleeping...");
               Thread.sleep(1000);
            } catch (InterruptedException ex) {
                logger.error(ex);
            }
            //break;
        }
    }
    
    private void updateSMSSendProgress(Long requestId, SMSStore smsStore){
        SMSResponse sr = new SMSResponse();
        sr.setId(requestId.toString());
        sr.setStatus("INPROGRESS");
        sr.setResponse("SENDING PROGRESS");
        smsStore.updateSMSResponse(sr);
    }
}
