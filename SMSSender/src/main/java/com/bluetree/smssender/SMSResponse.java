/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bluetree.smssender;

/**
 *
 * @author Raghu
 */
public class SMSResponse {
    private String id;
    private String status;    
    private String response;

    public SMSResponse() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }
    
    
}
