/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.bluetree.smssender;

import com.bluetree.repository.SMSStore;
import com.bluetree.utils.Constants;
import com.bluetree.utils.GenericThreadPoolExecutor;
import java.io.FileReader;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Properties;
import org.apache.log4j.Logger;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.GetMethod;

/**
 *
 * @author raghu
 */
public class SMSSender implements Runnable {

    protected final Logger log = Logger.getLogger(this.getClass());
    private String sendUrl;
    private String username;
    private String password;
    private String sender;
    private String requestId;
    private NameValuePair [] dataParameter = null;
    private SMSStore smsStore;
    
    private String toLabel;
    private String fromLabel;
    private String messageLabel;
    private String usernameLabel;
    private String passwordLabel;    

    public SMSSender(String sendUrl, String username, String password, String sender,
            String toLabel, String fromLabel, String messageLabel, String usernameLabel, String passwordLabel) {
        this.sendUrl = sendUrl;
        this.username = username;
        this.password = password;
        this.sender = sender;
        
        this.toLabel = toLabel;
        this.fromLabel = fromLabel;
        this.messageLabel = messageLabel;
        this.usernameLabel = usernameLabel;
        this.passwordLabel = passwordLabel;
    }
    

   /**
     * http://10.100.18.116:9090/csms/PushURL.cgi?USERNAME=api&PASSWORD=aircel&ORIGIN_ADDR=AIRCEL&MOBILENO="+msisdn+"&MESSAGE=”+Msg+” &TYPE=0
     */

    public void sendSMS(final String mobileNo, final String message, String requestId) throws Exception {
        this.requestId = requestId;
        dataParameter = prepareQueryString(mobileNo, message);
        GenericThreadPoolExecutor.getGenericThreadPoolExecutor().runTask(this);
    }

    public void run() {
        HttpClient client = new HttpClient();
        GetMethod method = new GetMethod(sendUrl);
        SMSResponse sr = new SMSResponse();
        try {
            sr.setId(requestId);
            log.info("URL:" + sendUrl);
            log.info("MessageData:");printNameValuePair(dataParameter);
            
            method.setQueryString(dataParameter);
            int statusCode = client.executeMethod(method);
            if (statusCode == HttpStatus.SC_OK) {
                String response = method.getResponseBodyAsString();
                log.info("SMS Response:" + response);
                sr.setStatus("SENT");
                sr.setResponse(response);
            }else {
                log.info("Method failed: " + method.getStatusLine());
                sr.setStatus("ERROR");
                sr.setResponse(method.getStatusCode()+":"+method.getStatusText());
            }
        } catch (Exception e) {
            log.error("HTTP Error in sending SMS." , e);
            sr.setStatus("ERROR");
            sr.setResponse(e.getMessage());            
        } finally {
            if (method != null) {
                method.releaseConnection();
            }
        }
        try {
            log.info("Updating sms response for requestid:");
            smsStore.updateSMSResponse(sr);
        }catch(Exception e){
            log.error(e);
        }
    }

    public NameValuePair[] prepareQueryString(String mobileNo, String message) throws GeneralSecurityException, IOException {

        /*
        NameValuePair nv1 = new NameValuePair("username", username);
        NameValuePair nv2 = new NameValuePair("password", password);
        NameValuePair nv3 = new NameValuePair("sender", sender);
        NameValuePair nv4 = new NameValuePair("to", mobileNo);
        NameValuePair nv5 = new NameValuePair("message", message);
        */
        NameValuePair nv1 = new NameValuePair(getUsernameLabel(), username);
        NameValuePair nv2 = new NameValuePair(getPasswordLabel(), password);
        NameValuePair nv3 = new NameValuePair(getFromLabel(), sender);
        NameValuePair nv4 = new NameValuePair(getToLabel(), mobileNo);
        NameValuePair nv5 = new NameValuePair(getMessageLabel(), message);
        
        return new NameValuePair[] {nv1, nv2, nv3, nv4, nv5};
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getSendUrl() {
        return sendUrl;
    }

    public void setSendUrl(String sendUrl) {
        this.sendUrl = sendUrl;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public SMSStore getSmsStore() {
        return smsStore;
    }

    public void setSmsStore(SMSStore smsStore) {
        this.smsStore = smsStore;
    }

    private void printNameValuePair(NameValuePair [] dataParameter){
        StringBuffer sb = new StringBuffer();
        for(NameValuePair nv: dataParameter){
            sb.append(nv.getName());sb.append("=");sb.append(nv.getValue());sb.append("&");
        }
        log.info(sb.toString());
    }

    public String getToLabel() {
        return toLabel;
    }

    public void setToLabel(String toLabel) {
        this.toLabel = toLabel;
    }

    public String getFromLabel() {
        return fromLabel;
    }

    public void setFromLabel(String fromLabel) {
        this.fromLabel = fromLabel;
    }

    public String getMessageLabel() {
        return messageLabel;
    }

    public void setMessageLabel(String messageLabel) {
        this.messageLabel = messageLabel;
    }

    public String getUsernameLabel() {
        return usernameLabel;
    }

    public void setUsernameLabel(String usernameLabel) {
        this.usernameLabel = usernameLabel;
    }

    public String getPasswordLabel() {
        return passwordLabel;
    }

    public void setPasswordLabel(String passwordLabel) {
        this.passwordLabel = passwordLabel;
    }
        
}
