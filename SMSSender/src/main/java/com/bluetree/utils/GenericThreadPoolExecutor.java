/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.bluetree.utils;

/**
 *
 * @author raghu
 */

import java.util.concurrent.*;
import java.util.*;

public class GenericThreadPoolExecutor {

    int poolSize = 2;
    int maxPoolSize = 5;
    long keepAliveTime = 10;
    ThreadPoolExecutor threadPool = null;
    final ArrayBlockingQueue<Runnable> queue = new ArrayBlockingQueue<Runnable>(50);

    private static GenericThreadPoolExecutor gtpe = null;

    public static GenericThreadPoolExecutor getGenericThreadPoolExecutor() {
        if(gtpe == null){
            gtpe = new GenericThreadPoolExecutor();
        }
        return gtpe;
    }
    
    private GenericThreadPoolExecutor() {
        threadPool = new ThreadPoolExecutor(poolSize, maxPoolSize,
                keepAliveTime, TimeUnit.SECONDS, queue);
    }

    public void runTask(Runnable task) {
        // System.out.println("Task count.."+threadPool.getTaskCount() );
        // System.out.println("Queue Size before assigning the
        // task.."+queue.size() );
        threadPool.execute(task);
        // System.out.println("Queue Size after assigning the
        // task.."+queue.size() );
        // System.out.println("Pool Size after assigning the
        // task.."+threadPool.getActiveCount() );
        // System.out.println("Task count.."+threadPool.getTaskCount() );
        System.out.println("Task count.." + queue.size());
    }

    public void shutDown(){
        threadPool.shutdown();
    }
    
}
