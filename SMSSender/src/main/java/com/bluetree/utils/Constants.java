/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.bluetree.utils;

/**
 *
 * @author raghu
 */
public class Constants {

    public static final String SMSSENDER = "smsSender";
    public static final String SMSSTORE = "smsStore";
    public static final String PROTECTEDCONFIGFILE = "protectedConfigFile";

    public static final String STATUS_ARRIVED = "ARRIVED";
    public static final String STATUS_PROCESSED = "PROCESSED";
    public static final String STATUS_ERROR = "ERROR";
    
    public static final String SMSSENDFROM = "BLUETREE";
    
}
