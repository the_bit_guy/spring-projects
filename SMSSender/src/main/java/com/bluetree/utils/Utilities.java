/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.bluetree.smsc.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author raghu
 */
public class Utilities {

    /**
     *Converts given date string to Date object by considering the given format to read date String.
     *@param dtStr date string to be converted to Date
     *@param format format string ( see <code>java.text.SimpleDate format</code>)
     *@return java.sql.Date date if the dtStr represents a valid date else null
     *@see java.text.SimpleDateFormat.
     */
    public static Date parseDate(String dtStr, String format) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(format);
            sdf.setLenient(false);
            return sdf.parse(dtStr);
        } catch (ParseException pe) {
            pe.printStackTrace();
            return null;
        }
    }

}
