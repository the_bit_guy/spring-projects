/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bluetree.repository;

import com.bluetree.smssender.SMSRequest;
import com.bluetree.smssender.SMSResponse;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import javax.sql.DataSource;
import org.apache.log4j.*;

/**
 *
 * @author raghu
 */
public class SMSStore {

    protected final Logger log = Logger.getLogger(this.getClass());
    private Connection wfcConn = null;
    private String updateQuery = "UPDATE sms_queue SET status = ?, sent_time = ?,sent_response = ? WHERE id = ?";
    private String selectQuery;

    private String sendUrl;
    private String username;
    private String password;
    private String sender;
    private DataSource dataSource;
    
    private String toLabel;
    private String fromLabel;
    private String messageLabel;
    private String usernameLabel;
    private String passwordLabel;

    public String getUpdateQuery() {
        return updateQuery;
    }

    public void setUpdateQuery(String updateQuery) {
        this.updateQuery = updateQuery;
    }

    public void setWfcDataSource(DataSource wfcDataSource) {
        try {
            this.dataSource = wfcDataSource;
            wfcConn = wfcDataSource.getConnection();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    private Connection getConnection(){
        try {
            testConnection(this.wfcConn);
            return this.wfcConn;
        } catch (SQLException ex) {
            log.error("Error getConnection()=", ex);
            try {
                this.wfcConn = dataSource.getConnection();
            } catch (SQLException ex1) {
                log.error("Error getConnection2()=", ex);
            }
        }
        return this.wfcConn;
    }
    
    private void testConnection(Connection conn) throws SQLException{
        String query = "select 1 from person_type";
        Statement pstmt = null;
        try {
            pstmt = conn.createStatement();
            pstmt.executeQuery(query);
        } catch (SQLException se) {
            se.printStackTrace();
            log.error(se);
            
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
                log.error(e);
            }
        }
    }
    
    public synchronized void updateSMSResponse(SMSResponse response){
        PreparedStatement pstmt = null;
        try {
            pstmt = getConnection().prepareStatement(updateQuery);
            pstmt.setString(1, response.getStatus());
            pstmt.setTimestamp(2, new Timestamp(System.currentTimeMillis()));
            pstmt.setString(3, response.getResponse());
            pstmt.setLong(4, Long.parseLong(response.getId()));
            pstmt.execute();
        } catch (SQLException se) {
            se.printStackTrace();
            log.error(se);
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
                log.error(e);
            }
        }
    }
    //id,mobile_number,message,status from sms_queue where status='new'
    public synchronized List<SMSRequest> findSMSByStatusNew(){
        PreparedStatement pstmt = null;
        List<SMSRequest> smsRequests = new ArrayList<SMSRequest> ();
        try {
            pstmt = getConnection().prepareStatement(selectQuery);
            ResultSet rs = pstmt.executeQuery();
            while(rs.next()){
                SMSRequest sr = new SMSRequest();
                sr.setId(rs.getLong("id"));
                sr.setMobileNumber(rs.getString("mobile_number"));
                sr.setMessage(rs.getString("message"));
                sr.setStatus(rs.getString("status"));
                smsRequests.add(sr);
            }
        } catch (SQLException se) {
            se.printStackTrace();
            log.error(se);
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
                log.error(e);
            }
        }        
        return smsRequests;
    }

    public String getSendUrl() {
        return sendUrl;
    }

    public void setSendUrl(String sendUrl) {
        this.sendUrl = sendUrl;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getSelectQuery() {
        return selectQuery;
    }

    public void setSelectQuery(String selectQuery) {
        this.selectQuery = selectQuery;
    }

    public String getToLabel() {
        return toLabel;
    }

    public void setToLabel(String toLabel) {
        this.toLabel = toLabel;
    }

    public String getFromLabel() {
        return fromLabel;
    }

    public void setFromLabel(String fromLabel) {
        this.fromLabel = fromLabel;
    }

    public String getMessageLabel() {
        return messageLabel;
    }

    public void setMessageLabel(String messageLabel) {
        this.messageLabel = messageLabel;
    }

    public String getUsernameLabel() {
        return usernameLabel;
    }

    public void setUsernameLabel(String usernameLabel) {
        this.usernameLabel = usernameLabel;
    }

    public String getPasswordLabel() {
        return passwordLabel;
    }

    public void setPasswordLabel(String passwordLabel) {
        this.passwordLabel = passwordLabel;
    }

    
}
