package com.magna.xmsystem.dependencies.utils;

public enum APPLICATION {
	XMADMIN("caxstartadmin"),
	XMMENU("caxstartmenu"),
	XMHOTLINE("caxstarthotline"),
	XMBATCH("caxstartbatch");
	
	/**
	 * Instantiates a new Application.
	 *
	 * @param applicationName the String
	 */
	APPLICATION(String applicationName) {
		this.applicationName = applicationName;
	}

	/** The applicationName. */
	private String applicationName;

	/**
	 * @return the applicationName
	 */
	public String getApplicationName() {
		return applicationName;
	}

	
	public static APPLICATION getApplication(String applicationName) {
        for (APPLICATION value : values()) {
            if (applicationName.equalsIgnoreCase(value.getApplicationName())) {
                return value;
            }
        }
        return null;
    }
}
