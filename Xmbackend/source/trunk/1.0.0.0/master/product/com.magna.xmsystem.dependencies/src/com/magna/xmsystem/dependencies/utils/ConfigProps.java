package com.magna.xmsystem.dependencies.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ConfigProps extends Properties {
	private static final long serialVersionUID = 1L;
	/**
	 * Logger instance
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(ConfigProps.class);

	private static ConfigProps thisRef;

	private ConfigProps() {
		super();
		try {
			final String fileURL = XMSystemUtil.getConfigPropertiesURL();
			try {
				this.load(new FileInputStream(fileURL + "config.properties"));
				thisRef = this;
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (Exception ex) {
			LOGGER.error("Execption ocuured in reading properties file", ex); //$NON-NLS-1$
		}
	}

	public static ConfigProps getInstance() {
		if (thisRef == null) {
			new ConfigProps();
		}
		return thisRef;
	}
}
