package com.magna.xmsystem.dependencies.utils;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Base64;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Path;
import org.eclipse.jface.resource.DeviceResourceException;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.jface.resource.LocalResourceManager;
import org.eclipse.jface.resource.ResourceManager;
import org.eclipse.jface.viewers.DecorationOverlayIcon;
import org.eclipse.jface.viewers.IDecoration;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magna.xmbackend.vo.enums.IDateTimeFormatConst;
import com.magna.xmsystem.dependencies.Activator;

/**
 * Util class for XMSystem.
 *
 * @author shashwat.anand
 */
public final class XMSystemUtil {

	/** Logger instance. */
	private static final Logger LOGGER = LoggerFactory.getLogger(XMSystemUtil.class);
	
	/** Member variable 'this ref' for {@link XMSystemUtil}. */
	private static XMSystemUtil thisRef;
	
	/** The image cache. */
	private final Map<String, ImageDescriptor> imageCache;
	
	/** The ticket. */
	private String ticket;
	
	/** The admin menu access ticket. */
	private String adminMenuAccessTicket;
	
	/** Member variable for {@link ResourceManager}. */
	private final ResourceManager resourceManager;

	/**
	 * private constructor.
	 */
	private XMSystemUtil() {
		imageCache = new HashMap<>();
		thisRef = this;
		this.resourceManager = new LocalResourceManager(JFaceResources.getResources());
	}
	
	/**
	 * Gets the single instance of XMSystemUtil.
	 *
	 * @return single instance of XMSystemUtil
	 */
	public static XMSystemUtil getInstance() {
		if (thisRef == null) {
			new XMSystemUtil();
		}
		return thisRef;
	}


	/**
	 * Checks if is empty.
	 *
	 * @param inputStr the input str
	 * @return true, if is empty
	 */
	public static boolean isEmpty(final String inputStr) {
		if (inputStr != null && !inputStr.isEmpty()) {
			return false;
		}
		return true;
	}

	/**
	 * Get the file name from given file path.
	 *
	 * @param filePath
	 *            {@link String}
	 * @return {@link String}
	 */
	public static String getFileName(final String filePath) {
		return filePath.substring(filePath.lastIndexOf('\\') + 1, filePath.lastIndexOf('.'));
	}

	/**
	 * Get the file name with extension from given file path.
	 *
	 * @param filePath
	 *            {@link String}
	 * @return {@link String}
	 */
	public static String getFileNameWithExtension(final String filePath) {
		return filePath.substring(filePath.lastIndexOf('\\') + 1);
	}
	
	/**
	 * Method to encode file to imagestring (base64).
	 *
	 * @param filePath
	 *            {@link String}
	 * @return {@link String}
	 */
	public static String encodeImage(final String filePath) {
		String imageDataString = null;
		try {
			final File file = new File(filePath); // NOPMD by shashwat.anand on 5/11/17 12:57 PM
			final FileInputStream imageInFile = new FileInputStream(file);
			final byte imageByteArray[] = new byte[(int) file.length()];
			imageInFile.read(imageByteArray);

			imageDataString = Base64.getEncoder().encodeToString(imageByteArray);

			imageInFile.close();
		} catch (Exception e) {
			LOGGER.error("Execption ocuured at encoding image", e); //$NON-NLS-1$
		} // NOPMD by shashwat.anand on 5/11/17 12:57 PM
		return imageDataString;
	}

	/**
	 * Method to decode imagestring (base64) to image descriptor.
	 *
	 * @param imageDataString
	 *            {@link String}
	 * @return {@link ImageDescriptor}
	 */
	public static ImageDescriptor decodeImage(final String imageDataString) {
		ImageDescriptor imageDescriptor = null; // NOPMD by shashwat.anand on 5/11/17 12:57 PM
		try {
			final byte[] imageByteArray = Base64.getDecoder().decode(imageDataString);
			final InputStream imageIS = new ByteArrayInputStream(imageByteArray);
			final ImageData imageData = new ImageData(imageIS);
			imageDescriptor = ImageDescriptor.createFromImageData(imageData);
			imageIS.close();
		} catch (Exception e) { // NOPMD by shashwat.anand on 5/11/17 12:57 PM
			LOGGER.error("Execption ocuured at decoding image", e); //$NON-NLS-1$
		}
		return imageDescriptor;
	}

	/**
	 * Gets the XMSystem root folder.
	 *
	 * @return {@link File}
	 */
	/*public static File getXMSystemRootFolder(final boolean create) {
		String userHome = System.getenv("user.root");
		if (isEmpty(userHome)) {
			userHome = System.getProperty("user.home");
			final File file = new File(userHome + File.separator + Constants.XMSYSTEM_ROOT_FOLDER);
			if (create && !file.exists()) {
				file.mkdirs();
			}
			return file;
		}
		return null;
	}*/

	/**
	 * Gets the XMSystem icons folder.
	 *
	 * @return {@link File}
	 */
	/*public static File getXMSystemIconFolder(boolean create) {
		File xmSystemRootFolder = getXMSystemRootFolder(create);
		if (xmSystemRootFolder != null) {
			File iconFolder = new File(
					xmSystemRootFolder.getAbsolutePath() + File.separator + Constants.XMSYSTEM_ICON_FOLDER);
			if (create && !iconFolder.exists()) {
				iconFolder.mkdirs();
			}
			return iconFolder;
		}
		return null;
	}*/

	/**
	 * Gets the XMSystem server icons Folder.
	 *
	 * @return {@link File}
	 */
	public static File getXMSystemServerIconsFolder() {
		String cadIconDir = XmSystemEnvProcess.getInstance().getenv("XM_ICONS");
		if(!isEmpty(cadIconDir)) {
			final File iconFolder = new File(cadIconDir.trim());
			//LOGGER.info("Server icon folder location : " + iconFolder.getAbsolutePath());
			if (iconFolder.exists()) {
				return iconFolder;
			}
		}
		
		return null;
	}
	

	/**
	 * Gets the CAD drive.
	 *
	 * @return the CAD drive
	 */
	public static File getCADDrive() {
		String cadDrive = XmSystemEnvProcess.getInstance().getenv("CAD_DRIVE_LETTER");
		if (!isEmpty(cadDrive)) { //$NON-NLS-1$
			final File cadDriveFile = new File(cadDrive.trim());
			if (cadDriveFile.exists()) {
				return cadDriveFile;
			}
		}
		return null;
	}
	
	/**
	 * Gets the CAD system server script folder.
	 *
	 * @return the CAD system server script folder
	 */
	public static File getCADSystemServerScriptFolder() {
		String cadScriptDir = XmSystemEnvProcess.getInstance().getenv("CAD_SCRIPT_DIR");
		if (!isEmpty(cadScriptDir)) { //$NON-NLS-1$
			final File scriptFolder = new File(cadScriptDir.trim());
			if (scriptFolder.exists()) {
				return scriptFolder;
			}
		}
		return null;
	}
	
	/**
	 * Gets the XM system server script folder.
	 *
	 * @return the XM system server script folder
	 */
	public static File getXMSystemServerScriptFolder() {
		String cadScriptDir = XmSystemEnvProcess.getInstance().getenv("XM_SCRIPTS");
		if (!isEmpty(cadScriptDir)) { //$NON-NLS-1$
			final File scriptFolder = new File(cadScriptDir.trim());
			if (scriptFolder.exists()) {
				return scriptFolder;
			}
		}
		return null;
	}
	
	/**
	 * Gets the site name.
	 *
	 * @return the site name
	 */
	public static String getSiteName() {
		return XmSystemEnvProcess.getInstance().getenv("XM_SITE");
	}
	
	/**
	 * Gets the admin area name.
	 *
	 * @return the admin area name
	 */
	public static String getAdminAreaName() {
		return XmSystemEnvProcess.getInstance().getenv("XM_ADMIN_AREA");
	} 
	
	/**
	 * Sets the admin area name.
	 *
	 * @param adminArea the new admin area name
	 */
	public static void setAdminAreaName(String adminArea) {
		XmSystemEnvProcess.getInstance().updateEnvVariable("XM_ADMIN_AREA", adminArea == null ? "" : adminArea);
	}
	
	/**
	 * Gets the inits the.
	 *
	 * @return the inits the
	 */
	public static String getINIT() {
		return XmSystemEnvProcess.getInstance().getenv("XM_INIT");
	} 
	
	/**
	 * Sets the inits the.
	 *
	 * @param init the new inits the
	 */
	public static void setINIT(String init) {
			XmSystemEnvProcess.getInstance().updateEnvVariable("XM_INIT", init == null ? "" : init);
	}
	
	/**
	 * Gets the host name.
	 *
	 * @return the host name
	 */
	public static String getHostName() {
		return XmSystemEnvProcess.getInstance().getenv("XM_HOST");
	}
	
	/**
	 * Gets the project name.
	 *
	 * @return the project name
	 */
	public static String getProjectName() {
		return XmSystemEnvProcess.getInstance().getenv("XM_PROJ");
	}
	
	/**
	 * Sets the project name.
	 *
	 * @param projectName the new project name
	 */
	public static void setProjectName(String projectName) {
		XmSystemEnvProcess.getInstance().updateEnvVariable("XM_PROJ", projectName == null ? "" : projectName);
	}
	
	/**
	 * Gets the task name.
	 *
	 * @return the task name
	 */
	public static String getTaskName() {
		return XmSystemEnvProcess.getInstance().getenv("XM_TASK");
	}
	
	/**
	 * Sets the task name.
	 *
	 * @param taskName the new task name
	 */
	public static void setTaskName(String taskName) {
		XmSystemEnvProcess.getInstance().updateEnvVariable("XM_TASK", taskName == null ? "" : taskName);
	}
	
	/**
	 * Sets the lang name.
	 *
	 * @param language the new lang name
	 */
	public static void setLanguage(String language) {
		XmSystemEnvProcess.getInstance().updateEnvVariable("XM_LANG", language == null ? "" : language);
	}
	
	/**
	 * Gets the language.
	 *
	 * @return the language
	 */
	public static String getLanguage() {
		return XmSystemEnvProcess.getInstance().getenv("XM_LANG");
	}
	
	/**
	 * Gets the XM backend url.
	 *
	 * @return the XM backend url
	 */
	public static String getXMBackendUrl() {
		return XMSystemProperties.getInstance().getProperty("XMBACKEND_SERVICE_URL");
	}

	/**
	 * Gets the builds the number.
	 *
	 * @return the builds the number
	 */
	public static String getBuildNumber() {
		return ConfigProps.getInstance().getProperty("BUILD_NUMBER", "1.1");
	}
	
	/**
	 * Gets the XM admin default project exipry.
	 *
	 * @return the XM admin default project exipry
	 */
	/*public static String getXMAdminDefaultProjectExipry() {
		//return XMSystemProperties.getInstance().getProperty("XMADMIN_PROJECT_EXPIRY_DAYS");
		String fileURL = getXmsystemPropertiesURL();
		Properties prop = new Properties();
		try {
			prop.load(new FileInputStream(fileURL+"xmsystem.properties"));
			return prop.getProperty("XMADMIN_PROJECT_EXPIRY_DAYS");
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}*/
	
	/**
	 * Gets the Config Properties.
	 *
	 * @return the fileURL
	 */
	public static String getConfigPropertiesURL() {
		Bundle bundle = Activator.getContext().getBundle();
		URL resource = bundle.getResource("properties");
		try {
			String fileURL = FileLocator.toFileURL(resource).getFile();
			return fileURL;
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		return null;
		
	}

	/**
	 * Gets the image.
	 *
	 * @param classObj the class obj
	 * @param iconPath the icon path
	 * @return the image
	 */
	public Image getImage(final Class<?> classObj, final String iconPath) {
		return getImage(classObj, iconPath, true, false);
	}
	
	/**
	 * Gets the image.
	 *
	 * @param classObj the class obj
	 * @param iconPath the icon path
	 * @param isActive the is active
	 * @param doScale the do scale
	 * @return the image
	 */
	public Image getImage(final Class<?> classObj, final String iconPath, final boolean isActive, final boolean doScale) {
		Image image = null;
		Image disabledImage = null;
		ImageDescriptor disabledImageDescriptor = null;
		try {
			final Bundle bundle = FrameworkUtil.getBundle(classObj);
			URL url = FileLocator.find(bundle, new Path(iconPath), null);
			if (url == null) {
				url = (new File(iconPath)).toURI().toURL();
			}
			String imageKey = url.toString();
			if (!isActive) {
				imageKey = imageKey + "-Deactive";
			}
			ImageDescriptor imageDescriptor;
			if ((imageDescriptor = imageCache.get(imageKey)) == null) {
				imageDescriptor = ImageDescriptor.createFromURL(url);
				if (!isActive) {
					disabledImageDescriptor = ImageDescriptor.createWithFlags(imageDescriptor, SWT.IMAGE_GRAY);
					final URL deactiveImgUrl = FileLocator.find(bundle, new Path("icons/overlay/png/7x8/deactive.png"), null);
					if (url != null) {
						imageDescriptor = ImageDescriptor.createFromURL(deactiveImgUrl);
						disabledImage = (Image) this.resourceManager.find(disabledImageDescriptor);
						if (disabledImage == null) {
							ImageData imageData = disabledImageDescriptor.getImageData();
							if (imageData != null && (imageData.width > 16 || imageData.height > 16) && doScale) {
								disabledImageDescriptor = scaleImage(Display.getDefault(), disabledImageDescriptor, 16, 16);
							}
							disabledImage = this.resourceManager.createImage(disabledImageDescriptor);
						}
						imageDescriptor = new DecorationOverlayIcon(disabledImage, imageDescriptor, IDecoration.BOTTOM_RIGHT);
					}
				}
				imageCache.put(imageKey, imageDescriptor);
			}
			if ((imageDescriptor = imageCache.get(imageKey)) != null) {
				image = (Image) this.resourceManager.find(imageDescriptor);
				if (image == null) {
					ImageData imageData = imageDescriptor.getImageData();
					if (imageData != null && (imageData.width > 16 || imageData.height > 16) && doScale) {
						imageDescriptor = scaleImage(Display.getDefault(), imageDescriptor, 16, 16);
					}
					image = this.resourceManager.createImage(imageDescriptor);
				}
			}
			/*if (disabledImage != null && !disabledImage.isDisposed()) {
				this.resourceManager.destroyImage(disabledImageDescriptor);
			}*/
		} catch (DeviceResourceException e) {
			LOGGER.warn("Unable to create image ! " + e);
		} catch (MalformedURLException e) {
			LOGGER.warn("Unable to create image ! " + e);
		}
		return image;
	}
	
	/**
	 * Get the image from {@link ResourceManager}.
	 *
	 * @param classObj the class obj
	 * @param iconPath            {@link String} Path of Image
	 * @return {@link Image}
	 */
	/*public Image getImage(final Class<?> classObj, final String iconPath, final ResourceManager resourceManager,
			boolean isActive, boolean doScale) {
		Image image = null;
		Image disabledImage = null;
		try {
			final Bundle bundle = FrameworkUtil.getBundle(classObj);
			URL url = FileLocator.find(bundle, new Path(iconPath), null);
			if (url == null) {
				url = (new File(iconPath)).toURI().toURL();
			}
			String imageKey = url.toString();
			if (!isActive) {
				imageKey = imageKey + "-Deactive";
			}
			ImageDescriptor imageDescriptor;
			if ((imageDescriptor = imageCache.get(imageKey)) == null) {
				imageDescriptor = ImageDescriptor.createFromURL(url);
				image = (Image) resourceManager.find(imageDescriptor);
				if (image == null) {
					ImageData imageData = imageDescriptor.getImageData();
					if ((imageData.width > 16 || imageData.height > 16) && doScale) {
						imageDescriptor = scaleImage(Display.getDefault(), imageDescriptor, 16, 16);
					}
					image = resourceManager.createImage(imageDescriptor);
				}
				if (!isActive) {
					final ImageDescriptor disabledImageDescriptor = ImageDescriptor.createWithFlags(imageDescriptor, SWT.IMAGE_GRAY);
					final URL deactiveImgUrl = FileLocator.find(bundle, new Path("icons/overlay/png/7x8/deactive.png"), null);
					if (url != null) {
						imageDescriptor = ImageDescriptor.createFromURL(deactiveImgUrl);
						disabledImage = (Image) resourceManager.find(disabledImageDescriptor);
						if (disabledImage == null) {
							disabledImage = resourceManager.createImage(disabledImageDescriptor);
						}
						imageDescriptor = new DecorationOverlayIcon(disabledImage, imageDescriptor, IDecoration.BOTTOM_RIGHT);
						imageCache.put(imageKey, imageDescriptor);
					}
				} else {
					imageCache.put(imageKey, imageDescriptor);
				}
			}
			image = (Image) resourceManager.find(imageDescriptor);
			if (image == null) {
				ImageData imageData = imageDescriptor.getImageData();
				if ((imageData.width > 16 || imageData.height > 16) && doScale) {
					imageDescriptor = scaleImage(Display.getDefault(), imageDescriptor, 16, 16);
				}
				image = resourceManager.createImage(imageDescriptor);
			}
			if (disabledImage != null && !disabledImage.isDisposed()) {
				disabledImage.dispose();
			}
			
		} catch (DeviceResourceException e) {
			LOGGER.warn("Unable to create image ! " + e);
		} catch (MalformedURLException e) {
			LOGGER.warn("Unable to create image ! " + e);
		}
		
		return image;
	}*/

	/**
	 * Gets the disabled icon
	 * @param classObj
	 * @param iconPath
	 * @return {@link Image}
	 */
	public Image getDisabledImage(final Class<?> classObj, final String iconPath) {
		Image disabledImage = null;
		try {
			final Bundle bundle = FrameworkUtil.getBundle(classObj);
			URL url = FileLocator.find(bundle, new Path(iconPath), null);
			if (url == null) {
				url = (new File(iconPath)).toURI().toURL();
			}
			String imageKey = url.toString();
			imageKey = imageKey + "-Disable";
			ImageDescriptor imageDescriptor;
			if ((imageDescriptor = imageCache.get(imageKey)) == null) {
				imageDescriptor = ImageDescriptor.createFromURL(url);
				final ImageDescriptor disabledImageDescriptor = ImageDescriptor.createWithFlags(imageDescriptor, SWT.IMAGE_DISABLE);
				disabledImage = (Image) this.resourceManager.find(disabledImageDescriptor);
				if (disabledImage == null) {
					disabledImage = this.resourceManager.createImage(disabledImageDescriptor);
				}
				imageCache.put(imageKey, disabledImageDescriptor);
			} else {
				disabledImage = (Image) this.resourceManager.find(imageDescriptor);
				if (disabledImage == null) {
					disabledImage = this.resourceManager.createImage(imageDescriptor);
				}
			}			
		} catch (DeviceResourceException e) {
			LOGGER.warn("Unable to create disable image ! " + e);
		} catch (MalformedURLException e) {
			LOGGER.warn("Unable to create disable image ! " + e);
		}
		return disabledImage;
	}
	
	/**
	 * Gets the system user name.
	 *
	 * @return the system user name
	 */
	public static String getSystemUserName() {
		return XmSystemEnvProcess.getInstance().getenv("XM_USER");
	}
	
	/**
	 * Gets the time stamp.
	 *
	 * @return the time stamp
	 */
	public static String getTimeStamp() {
		final LocalDateTime currentTime = LocalDateTime.now();
		final DateTimeFormatter dtf = DateTimeFormatter.ofPattern(IDateTimeFormatConst.UI_DATE_TIME_FORMAT, Locale.ENGLISH);
		return dtf.format(currentTime);
	}
	
	/**
	 * Gets the history log time date format.
	 *
	 * @return the history log time date format
	 */
	public static SimpleDateFormat getHistoryLogTimeDateFormat() {
		return new SimpleDateFormat(IDateTimeFormatConst.UI_DATE_TIME_FORMAT, Locale.ENGLISH);
	}

	/**
	 * Gets the ticket.
	 *
	 * @return the ticket
	 */
	public String getTicket() {
		return ticket;
	}

	/**
	 * Sets the ticket.
	 *
	 * @param ticket the ticket to set
	 */
	public void setTicket(String ticket) {
		this.ticket = ticket;
	}

	/**
	 * Gets the admin menu access ticket.
	 *
	 * @return the admin menu access ticket
	 */
	public String getAdminMenuAccessTicket() {
		return adminMenuAccessTicket;
	}

	/**
	 * Sets the admin menu access ticket.
	 *
	 * @param adminMenuAccessTicket
	 *            the new admin menu access ticket
	 */
	public void setAdminMenuAccessTicket(String adminMenuAccessTicket) {
		this.adminMenuAccessTicket = adminMenuAccessTicket;
	}

	/**
	 * Sets the system user name.
	 *
	 * @param username
	 *            the new system user name
	 */
	public static void setSystemUserName(final String username) {
		String usernameValue = username == null ? "" : username;
		XmSystemEnvProcess.getInstance().updateEnvVariable("USERNAME", usernameValue);
		XmSystemEnvProcess.getInstance().updateEnvVariable("XM_USER", usernameValue);
	}
	
	/**
	 * Method for Cleanup files on exit.
	 */
	public static void cleanupFilesOnExit() {
		try {
			String userRoot = System.getProperty("user.home");
			File file1 = new File(userRoot + "/.swt");
			File file2 = new File(userRoot + "/.eclipse");
			File file3 = new File(userRoot + "/.oracle_jre_usage");

			FileUtils.forceDeleteOnExit(file1);
			FileUtils.forceDeleteOnExit(file2);
			FileUtils.forceDeleteOnExit(file3);
		} catch (IOException exception) {
			LOGGER.info("Exception in cleanupFilesOnExit: " + exception);
		}
	}
	
	/**
	 * Scale image.
	 *
	 * @param display the display
	 * @param imageDescriptor the image descriptor
	 * @param outMaxWidth the out max width
	 * @param outMaxHeight the out max height
	 * @return the image descriptor
	 */
	public static ImageDescriptor scaleImage(Display display, ImageDescriptor imageDescriptor, int outMaxWidth, int outMaxHeight) {
		if (imageDescriptor == null) {
			return null;
		}

		ImageData imageData = imageDescriptor.getImageData();
		if (imageData == null) {
			return imageDescriptor;
		}

		int newHeight = outMaxHeight;
		int newWidth = (imageData.width * newHeight) / imageData.height;
		if (newWidth > outMaxWidth) {
			newWidth = outMaxWidth;
			newHeight = (imageData.height * newWidth) / imageData.width;
		}

		// Use GC.drawImage
		Image outImage = new Image(display, newWidth, newHeight);
		GC gc = new GC(outImage);
		Image oldImage = imageDescriptor.createImage();
		gc.drawImage(oldImage, 0, 0, imageData.width, imageData.height, 0, 0, newWidth, newHeight);
		ImageDescriptor outImageDesc = ImageDescriptor.createFromImage(outImage);

		oldImage.dispose();
		gc.dispose();

		return outImageDesc;
	}
	
	/**
	 * Resize the image.
	 *
	 * @param image the image
	 * @param width the width
	 * @param height the height
	 * @return the image
	 */
	public static Image resize(final Image image, final int width, final int height) {
		final Image scaled = new Image(Display.getDefault(), width, height);
		final GC gc = new GC(scaled);
		gc.setAntialias(SWT.ON);
		gc.setInterpolation(SWT.HIGH);
		gc.drawImage(image, 0, 0, image.getBounds().width, image.getBounds().height, 0, 0, width, height);
		gc.dispose();
		image.dispose(); // don't forget about me!
		return scaled;
	}
	
	/**
	 * Force active.
	 *
	 * @param shell the shell
	 */
	public static void forceActive(final Shell shell) {
		shell.getDisplay().asyncExec(new Runnable() {
	        public void run() {
	            shell.forceActive();
	        }
	    });
	}
}
