package com.magna.xmsystem.dependencies.utils;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.eclipse.core.runtime.Platform;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class XmSystemEnvProcess.
 */
public class XmSystemEnvProcess {

	/**
	 * Logger instance
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(XmSystemEnvProcess.class);

	/**
	 * XMSystemProperties static reference
	 */
	private static XmSystemEnvProcess thisRef;
	
	/** The Constant DEFAULT_LANGUAGE. */
	//private String DEFAULT_LANGUAGE = "english";

	private Map<String, String> environment;

	/**
	 * Constructor
	 */
	private XmSystemEnvProcess() {
		try {
			thisRef = this;
			/*if (!XMSystemUtil.isEmpty(System.getenv("XM_LANG"))) {
				DEFAULT_LANGUAGE = System.getenv("XM_LANG");
			}*/
		} catch (Exception ex) {
			LOGGER.error("Execption ocuured in creating instance of XMSystemEnvVariables instance", ex); //$NON-NLS-1$
		}
	}

	/**
	 * Gets the single instance of XmSystemEnvProcess.
	 *
	 * @return single instance of XmSystemEnvProcess
	 */
	public static XmSystemEnvProcess getInstance() {
		if (thisRef == null) {
			new XmSystemEnvProcess();
		}
		return thisRef;
	}

	/**
	 * Method for Start.
	 *
	 * @param application {@link APPLICATION}
	 */
	public void start(final APPLICATION application) {
		this.environment = new HashMap<String, String>(System.getenv());
		initSystemSpecificVariables();
		//initCADSpecificVariables();
		initVariables();
		setUpLogger(application);
	}

	/**
	 * Sets the up logger.
	 *
	 * @param application the new up logger
	 */
	public void setUpLogger(final APPLICATION application) {
		final URL url = Platform.getInstanceLocation().getURL();
		if (url != null) {
			SLF4JConfigurator.configure(url.getPath() + application.getApplicationName() + "/rcplogs");
			File file = new File("LOG_DIR_IS_UNDEFINED");
			try {
				FileUtils.deleteDirectory(file);
			} catch (IOException e) {
				LOGGER.error("Unable to delete : " + file.getAbsolutePath() + " ! " + e);
			}
		}
	}

	/**
	 * Method for Inits the system specific variables.
	 */
	private void initSystemSpecificVariables() {
		this.environment.put("XM_VERSION", "1.2");
		this.environment.put("XM_INIT", "0");
		this.environment.put("XM_USER", System.getenv("USERNAME"));
		this.environment.put("XM_HOST", getHostName());
		//this.environment.put("XM_LANG", DEFAULT_LANGUAGE);
		
		this.environment.put("CAD_mylogin", System.getenv("USERNAME"));
	}
	
	/**
	 * Init CAD specific variables.
	 *//*
	private void initCADSpecificVariables() {
		this.environment.put("CAD_mylogin", System.getenv("USERNAME"));
		this.environment.put("CAD_APPL_DIR", getApplicationDir());
		this.environment.put("CAD_XMENU_DIR", getApplicationDir());
	}*/

	/**
	 * Method for Initializing environmental variables, if there is no bath
	 * execution.
	 */
	private void initVariables() {
		String cadDriveLetter;
		if ((cadDriveLetter = System.getenv("CAD_DRIVE_LETTER")) != null) { //$NON-NLS-1$
			cadDriveLetter = cadDriveLetter.trim();
		}
		String xmSiteName;
		if ((xmSiteName = System.getenv("XM_SITENAME")) != null) { //$NON-NLS-1$
			xmSiteName = xmSiteName.trim();
		}
		String systemPath;
		if ((systemPath = System.getenv("SYSTEM_PATH")) != null) { //$NON-NLS-1$
			this.environment.put("Path", systemPath);
		}
		String xmSystemInstalltionPath = "c:/cax/xmsystem/";
		if (OSValidator.isUnixOrLinux()) {
			xmSystemInstalltionPath = System.getProperty("user.home") + "/cax/xmsystem/";
			// cadDriveLetter = "/run/user/1000/gvfs/smb-share:server=192.168.1.107,share=magna_share";
		}

		if (this.environment.get("XM_PATH") == null) { //$NON-NLS-1$
			this.environment.put("XM_PATH", xmSystemInstalltionPath);
		}
		if (this.environment.get("XM_CONFIG") == null) { //$NON-NLS-1$
			this.environment.put("XM_CONFIG", xmSystemInstalltionPath + "/xmsystem.properties");
		}
		if (this.environment.get("CAD_SCRIPT_DIR") == null) { //$NON-NLS-1$
			this.environment.put("CAD_SCRIPT_DIR", cadDriveLetter + "/scripts");
		}
		if (this.environment.get("XM_SCRIPTS") == null) { //$NON-NLS-1$
			this.environment.put("XM_SCRIPTS", cadDriveLetter + "/scripts");
		}
		if (this.environment.get("XM_ICONS") == null) { //$NON-NLS-1$
			this.environment.put("XM_ICONS", cadDriveLetter + "/icons");
		}
		if (this.environment.get("XM_SITE") == null) { //$NON-NLS-1$
			this.environment.put("XM_SITE", xmSiteName);
		}

		if (this.environment.get("CAD_APPL_DIR") == null) { //$NON-NLS-1$
			this.environment.put("CAD_APPL_DIR", getApplicationDir());
		}
		if (this.environment.get("CAD_XMENU_DIR") == null) { //$NON-NLS-1$
			this.environment.put("CAD_XMENU_DIR", getApplicationDir());
		}
	}

	/**
	 * Gets the env.
	 *
	 * @param envVariable {@link String}
	 * @return the env
	 */
	public String getenv(String variable) {
		return this.environment.get(variable);
	}
	
	/**
	 * Gets the environment map.
	 *
	 * @return the environment map
	 */
	public Map<String, String> getEnvironmentMap() {
		return this.environment;
	}

	/**
	 * Update env variable.
	 *
	 * @param key
	 *            the key
	 * @param value
	 *            the value
	 */
	public void updateEnvVariable(String key, String value) {
		this.environment.put(key, value);
	}

	/**
	 * Gets the host name.
	 *
	 * @return the host name
	 */
	private String getHostName() {
		String xmHost = null;

		try {
			java.net.InetAddress localMachine = java.net.InetAddress.getLocalHost();
			if ((xmHost = localMachine.getHostName()) != null) {
				xmHost = xmHost.trim();
			}
		} catch (UnknownHostException e) {
			LOGGER.error("Exception occured while getting host name!");
		}
		return xmHost;
	}
	
	/**
	 * Gets the application dir.
	 *
	 * @return the application dir
	 */
	private String getApplicationDir() {
		String appDirNameFinal = null;
		try {
			final String appDirName = System.getProperty("user.dir");
			appDirNameFinal = appDirName.replace("\\", "/");
		} catch (Exception e) {
			LOGGER.error("Exception occured while getting cad application directory name!");
		}

		return appDirNameFinal;
	}
}
