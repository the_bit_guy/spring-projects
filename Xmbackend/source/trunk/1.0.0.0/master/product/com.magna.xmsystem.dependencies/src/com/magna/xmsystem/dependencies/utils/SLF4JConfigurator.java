package com.magna.xmsystem.dependencies.utils;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import org.osgi.framework.FrameworkUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.joran.JoranConfigurator;
import ch.qos.logback.core.joran.spi.JoranException;
import ch.qos.logback.core.util.StatusPrinter;

public final class SLF4JConfigurator {

	private static final String BUNDLE_NAME = FrameworkUtil.getBundle(SLF4JConfigurator.class).getSymbolicName();
	private static final String LOGBACK_XML = "platform:/plugin/" + BUNDLE_NAME + "/logback.xml";

	public static void configure(final String logDir) {
		// Reset Current SLF4J config
		JoranConfigurator configurator = new JoranConfigurator();
		LoggerContext loggerContext = (LoggerContext) LoggerFactory.getILoggerFactory();
		configurator.setContext(loggerContext);
		loggerContext.reset();
		
		loggerContext.putProperty("LOG_DIR", logDir);
		// Read Configuration file
		try {
			URL configFile = new URL(LOGBACK_XML);
			InputStream configurationStream = configFile.openStream();
			configurator.doConfigure(configurationStream);
			configurationStream.close();
		} catch (JoranException | IOException e) {
			// Problem when reading file...
			e.printStackTrace();
		}
		StatusPrinter.printInCaseOfErrorsOrWarnings(loggerContext);

		// Confirm message
		Logger logger = LoggerFactory.getLogger(SLF4JConfigurator.class);
		logger.info("Logging configuration initialised.");
	}
}