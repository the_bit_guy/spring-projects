package com.magna.xmsystem.dependencies.utils;

/**
 * The Enum Activation.
 * 
 * @author shashwat.anand
 */
public enum Activation {
	ACTIVATE, DEACTIVATE;

	/**
	 * Gets the activation enum.
	 *
	 * @param activatation the activatation
	 * @return the activation enum
	 */
	public static Activation getActivationEnum(String activatation) {
        for (Activation value : values()) {
            if (activatation.equalsIgnoreCase(value.name())) {
                return value;
            }
        }
        return null;
    }
}
