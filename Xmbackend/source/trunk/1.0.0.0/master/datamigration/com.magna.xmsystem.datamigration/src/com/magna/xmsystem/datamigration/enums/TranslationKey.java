package com.magna.xmsystem.datamigration.enums;

import java.io.Serializable;

/**
 * Enum for Translation key.
 *
 * @author Chiranjeevi.Akula
 */
public enum TranslationKey implements Serializable {
	
	NAME, DESC
}
