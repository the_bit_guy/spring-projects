package com.magna.xmsystem.datamigration.vo.directory;

import com.magna.xmsystem.datamigration.util.GenerateUuid;

/**
 * Class for Directory object reference.
 *
 * @author Chiranjeevi.Akula
 */
public class DirectoryObjectReference {

	/** Member variable 'id' for {@link String}. */
	private String id;

	/** Member variable 'directory id' for {@link String}. */
	private String directoryId;

	/** Member variable 'object id' for {@link String}. */
	private String objectId;

	/** Member variable 'object type' for {@link String}. */
	private String objectType;

	/**
	 * Constructor for DirectoryObjectReference Class.
	 */
	public DirectoryObjectReference() {
		this.id = GenerateUuid.getUuid();
	}

	/**
	 * Constructor for DirectoryObjectReference Class.
	 *
	 * @param directoryId
	 *            {@link String}
	 * @param objectId
	 *            {@link String}
	 * @param objectType
	 *            {@link String}
	 */
	public DirectoryObjectReference(String directoryId, String objectId, String objectType) {
		this.id = GenerateUuid.getUuid();
		this.directoryId = directoryId;
		this.objectId = objectId;
		this.objectType = objectType;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id
	 *            the new id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Gets the directory id.
	 *
	 * @return the directory id
	 */
	public String getDirectoryId() {
		return directoryId;
	}

	/**
	 * Sets the directory id.
	 *
	 * @param directoryId
	 *            the new directory id
	 */
	public void setDirectoryId(String directoryId) {
		this.directoryId = directoryId;
	}

	/**
	 * Gets the object id.
	 *
	 * @return the object id
	 */
	public String getObjectId() {
		return objectId;
	}

	/**
	 * Sets the object id.
	 *
	 * @param objectId
	 *            the new object id
	 */
	public void setObjectId(String objectId) {
		this.objectId = objectId;
	}

	/**
	 * Gets the object type.
	 *
	 * @return the object type
	 */
	public String getObjectType() {
		return objectType;
	}

	/**
	 * Sets the object type.
	 *
	 * @param objectType
	 *            the new object type
	 */
	public void setObjectType(String objectType) {
		this.objectType = objectType;
	}
}
