package com.magna.xmsystem.datamigration.vo.site;

import java.util.ArrayList;
import java.util.List;

import com.magna.xmsystem.datamigration.enums.Status;
import com.magna.xmsystem.datamigration.util.GenerateUuid;

/**
 * Class for Site.
 *
 * @author Chiranjeevi.Akula
 */
public class Site {

	/** Member variable 'id' for {@link String}. */
	private String id;
	
	/** Member variable 'name' for {@link String}. */
	private String name;
	
	/** Member variable 'status' for {@link String}. */
	private String status;
	
	/** Member variable 'icon name' for {@link String}. */
	private String iconName;

	/** Member variable 'site translations' for {@link List<SiteTranslation>}. */
	private List<SiteTranslation> siteTranslations;

	/**
	 * Constructor for Site Class.
	 */
	public Site() {
		this.id = GenerateUuid.getUuid();
		this.siteTranslations = new ArrayList<>();
	}

	/**
	 * Constructor for Site Class.
	 *
	 * @param status {@link String}
	 * @param iconId {@link String}
	 * @param siteTranslations {@link List<SiteTranslation>}
	 */
	public Site(String name, String status, String iconId, List<SiteTranslation> siteTranslations) {
		this.id = GenerateUuid.getUuid();
		this.name = name;
		this.status = status;
		this.iconName = iconId;
		this.siteTranslations = siteTranslations;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 *
	 * @param statusCode the new status
	 */
	public void setStatus(int statusCode) {
		if (statusCode == 1) {
			this.status = Status.ACTIVE.toString();
		} else {
			this.status = Status.INACTIVE.toString();
		}
	}

	/**
	 * Gets the icon name.
	 *
	 * @return the icon name
	 */
	public String getIconName() {
		return iconName;
	}

	/**
	 * Sets the icon name.
	 *
	 * @param iconName the new icon name
	 */
	public void setIconName(String iconName) {
		this.iconName = iconName;
	}

	/**
	 * Gets the site translations.
	 *
	 * @return the site translations
	 */
	public List<SiteTranslation> getSiteTranslations() {
		return siteTranslations;
	}

	/**
	 * Sets the site translation.
	 *
	 * @param siteTranslation the new site translation
	 */
	public void setSiteTranslation(SiteTranslation siteTranslation) {
		this.siteTranslations.add(siteTranslation);
	}

	
	/**
	 * Sets the site translations.
	 *
	 * @param siteTranslations the new site translations
	 */
	public void setSiteTranslations(List<SiteTranslation> siteTranslations) {
		this.siteTranslations = siteTranslations;
	}

}
