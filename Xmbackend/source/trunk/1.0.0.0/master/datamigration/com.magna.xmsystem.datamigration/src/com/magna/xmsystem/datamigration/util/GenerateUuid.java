package com.magna.xmsystem.datamigration.util;

import java.util.UUID;

/**
 * Class for Generate uuid.
 *
 * @author Chiranjeevi.Akula
 */
public class GenerateUuid {
	
    /**
     * Gets the uuid.
     *
     * @return the uuid
     */
    public static String getUuid() {
        final String uuid = String.valueOf(UUID.randomUUID());
        return uuid;
    }
}
