package com.magna.xmsystem.datamigration.vo.relations;

import com.magna.xmsystem.datamigration.enums.Status;
import com.magna.xmsystem.datamigration.util.GenerateUuid;

/**
 * Class for Site admin area start application.
 *
 * @author Chiranjeevi.Akula
 */
public class SiteAdminAreaStartApplication {

	/** Member variable 'id' for {@link String}. */
	private String id;

	/** Member variable 'site admin area id' for {@link String}. */
	private String siteAdminAreaId;

	/** Member variable 'start application id' for {@link String}. */
	private String startApplicationId;

	/** Member variable 'status' for {@link String}. */
	private String status;

	/**
	 * Constructor for SiteAdminAreaStartApplication Class.
	 */
	public SiteAdminAreaStartApplication() {
		this.id = GenerateUuid.getUuid();
	}

	/**
	 * Constructor for SiteAdminAreaStartApplication Class.
	 *
	 * @param siteAdminAreaId
	 *            {@link String}
	 * @param userApplicationId
	 *            {@link String}
	 * @param status
	 *            {@link String}
	 */
	public SiteAdminAreaStartApplication(String siteAdminAreaId, String userApplicationId, String status) {
		this.id = GenerateUuid.getUuid();
		this.siteAdminAreaId = siteAdminAreaId;
		this.startApplicationId = userApplicationId;
		this.status = status;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id
	 *            the new id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Gets the site admin area id.
	 *
	 * @return the site admin area id
	 */
	public String getSiteAdminAreaId() {
		return siteAdminAreaId;
	}

	/**
	 * Sets the site admin area id.
	 *
	 * @param siteAdminAreaId
	 *            the new site admin area id
	 */
	public void setSiteAdminAreaId(String siteAdminAreaId) {
		this.siteAdminAreaId = siteAdminAreaId;
	}

	/**
	 * Gets the start application id.
	 *
	 * @return the start application id
	 */
	public String getStartApplicationId() {
		return startApplicationId;
	}

	/**
	 * Sets the start application id.
	 *
	 * @param startApplicationId
	 *            the new start application id
	 */
	public void setStartApplicationId(String startApplicationId) {
		this.startApplicationId = startApplicationId;
	}

	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 *
	 * @param statusCode
	 *            the new status
	 */
	public void setStatus(String statusCode) {
		if ("1".equals(statusCode)) {
			this.status = Status.ACTIVE.toString();
		} else {
			this.status = Status.INACTIVE.toString();
		}
	}
}
