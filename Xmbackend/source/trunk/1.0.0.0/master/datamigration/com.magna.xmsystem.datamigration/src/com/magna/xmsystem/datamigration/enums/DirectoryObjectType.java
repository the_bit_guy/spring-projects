package com.magna.xmsystem.datamigration.enums;

/**
 * Enum for Directory object type.
 *
 * @author Chiranjeevi.Akula
 */
public enum DirectoryObjectType {
	USER,PROJECT,USERAPPLICATION,PROJECTAPPLICATION
}
