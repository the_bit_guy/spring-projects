package com.magna.xmsystem.datamigration.jdbc;

import java.util.Date;
import java.util.List;

import com.magna.xmsystem.datamigration.jdbc.newdb.NewDatabase;
import com.magna.xmsystem.datamigration.jdbc.olddb.OldDatabase;
import com.magna.xmsystem.datamigration.vo.adminarea.AdminArea;
import com.magna.xmsystem.datamigration.vo.baseapplication.BaseApplication;
import com.magna.xmsystem.datamigration.vo.directory.Directory;
import com.magna.xmsystem.datamigration.vo.directory.DirectoryObjectReference;
import com.magna.xmsystem.datamigration.vo.project.Project;
import com.magna.xmsystem.datamigration.vo.projectapplication.ProjectApp;
import com.magna.xmsystem.datamigration.vo.relations.SiteAdminArea;
import com.magna.xmsystem.datamigration.vo.relations.SiteAdminAreaProject;
import com.magna.xmsystem.datamigration.vo.relations.SiteAdminAreaProjectProjectApplication;
import com.magna.xmsystem.datamigration.vo.relations.SiteAdminAreaProjectStartApplication;
import com.magna.xmsystem.datamigration.vo.relations.SiteAdminAreaStartApplication;
import com.magna.xmsystem.datamigration.vo.relations.SiteAdminAreaUserApplication;
import com.magna.xmsystem.datamigration.vo.relations.UserAdminAreaProjectProjectApplication;
import com.magna.xmsystem.datamigration.vo.relations.UserAdminAreaUserApplication;
import com.magna.xmsystem.datamigration.vo.relations.UserProject;
import com.magna.xmsystem.datamigration.vo.relations.UserStartApplication;
import com.magna.xmsystem.datamigration.vo.site.Site;
import com.magna.xmsystem.datamigration.vo.startapplication.StartApp;
import com.magna.xmsystem.datamigration.vo.user.User;
import com.magna.xmsystem.datamigration.vo.userapplication.UserApp;
import com.magna.xmsystem.datamigration.vo.userhistory.UserHistory;

/**
 * Class for JDBC execution.
 *
 * @author Chiranjeevi.Akula
 */
public class JDBCExecution {
	
	/** The Constant JDBC_DRIVER. */
	private final static String JDBC_DRIVER = "oracle.jdbc.driver.OracleDriver";

	/** Member variable 'old db url' for {@link String}. */
	private static String OLD_DB_URL;
	
	/** Member variable 'old db username' for {@link String}. */
	private static String OLD_DB_USERNAME;
	
	/** Member variable 'old db password' for {@link String}. */
	private static String OLD_DB_PASSWORD;

	/** Member variable 'new db url' for {@link String}. */
	private static String NEW_DB_URL;
	
	/** Member variable 'new db username' for {@link String}. */
	private static String NEW_DB_USERNAME;
	
	/** Member variable 'new db password' for {@link String}. */
	private static String NEW_DB_PASSWORD;

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {

		try {
			Class.forName(JDBC_DRIVER);

			initOldDBDetails();
			initNewDBDetails();

			OldDatabase oldDatabase = new OldDatabase(OLD_DB_URL, OLD_DB_USERNAME, OLD_DB_PASSWORD);
			NewDatabase newDatabase = new NewDatabase(NEW_DB_URL, NEW_DB_USERNAME, NEW_DB_PASSWORD);

			System.out.println("Data Migration Started on " + (new Date()) + "....");
			
			if (newDatabase.clearTablesData()) {
				
				newDatabase.loadPropertyConfig();
				
				newDatabase.insertCustomIcons();
				
				oldDatabase.loadTranslationData();
				
				System.out.println("Migrating Base Data....");
				
				List<Site> sites = oldDatabase.getSiteData();
				if (!sites.isEmpty()) {
					newDatabase.insertSiteData(sites);
				}
				List<Project> projects = oldDatabase.getProjectData();
				if (!projects.isEmpty()) {
					newDatabase.insertProjectData(projects);
				}
				List<User> users = oldDatabase.getUserData();
				if (!users.isEmpty()) {
					newDatabase.insertUserData(users);
				}
				List<BaseApplication> baseApplications = oldDatabase.getBaseAppData();
				if (!baseApplications.isEmpty()) {
					newDatabase.insertBaseAppData(baseApplications);
				}
				List<UserApp> userApplications = oldDatabase.getUserApplicationData();
				if (!userApplications.isEmpty()) {
					newDatabase.insertUserApplicationData(userApplications);
				}
				List<ProjectApp> projectApplications = oldDatabase.getProjectApplicationData();
				if (!projectApplications.isEmpty()) {
					newDatabase.insertProjectApplicationData(projectApplications);
				}
				List<StartApp> startApplications = oldDatabase.getStartApplicationData();
				if (!startApplications.isEmpty()) {
					newDatabase.insertStartApplicationData(startApplications);
				}
				List<Directory> directories = oldDatabase.getDirectoryData();
				if (!directories.isEmpty()) {
					newDatabase.insertDirectoryData(directories);
				}
				
				System.out.println("Migrating Relation Data....");
				
				//Making Relations
				List<DirectoryObjectReference> directoryObjectRefs = oldDatabase.getDirectoryObjectRefData();
				if (!directoryObjectRefs.isEmpty()) {
					newDatabase.insertDirectoryObjectRefData(directoryObjectRefs);
				}
				List<DirectoryObjectReference> adminMenuDirectoryObjectRefs = oldDatabase.getAdminMenuDirectoryObjectRefData();
				if (!adminMenuDirectoryObjectRefs.isEmpty()) {
					newDatabase.insertAdminMenuDirectoryObjectRefData(adminMenuDirectoryObjectRefs);
				}
				List<UserProject> userProjectRels = oldDatabase.getUserProjectRel();
				if (!userProjectRels.isEmpty()) {
					newDatabase.insertUserProjectRelData(userProjectRels);
				}				
				List<UserStartApplication> userStartAppRels = oldDatabase.getUserStartAppRel();
				if (!userStartAppRels.isEmpty()) {
					newDatabase.insertUserStartAppRelData(userStartAppRels);
				}
				
				System.out.println("Migrating Administration Area Based Relation Data....");
				
				//Making AA Based Relations				
				List<AdminArea> adminAreas = newDatabase.getAdminAreaData();
				if(adminAreas != null && !adminAreas.isEmpty()) {
					List<SiteAdminArea> siteAARels = oldDatabase.getSiteAARel(adminAreas);
					if(!siteAARels.isEmpty()) {
						newDatabase.insertSiteAdminAreaRelData(siteAARels);
					}
					
					List<SiteAdminAreaProject> siteAAProjectRels = oldDatabase.getSiteAAProjectRel(adminAreas);
					if(!siteAAProjectRels.isEmpty()) {
						newDatabase.insertSiteAdminAreaProjectRelData(siteAAProjectRels);
					}
					
					List<SiteAdminAreaUserApplication> siteAAUserAppRels = oldDatabase.getSiteAAUserAppRel(adminAreas);
					if(!siteAAUserAppRels.isEmpty()) {
						newDatabase.insertSiteAdminAreaUserAppRelData(siteAAUserAppRels);
					}
					
					List<SiteAdminAreaStartApplication> siteAAStartAppRels = oldDatabase.getSiteAAStartAppRel(adminAreas);
					if(!siteAAStartAppRels.isEmpty()) {
						newDatabase.insertSiteAdminAreaStartAppRelData(siteAAStartAppRels);
					}
					
					List<SiteAdminAreaProjectProjectApplication> siteAAProjectProjectAppRels = oldDatabase.getSiteAAProjectProjectAppRel(adminAreas);
					if(!siteAAProjectProjectAppRels.isEmpty()) {
						newDatabase.insertSiteAdminAreaProjectProjectAppRelData(siteAAProjectProjectAppRels);
					}
					
					List<SiteAdminAreaProjectStartApplication> siteAAProjectStartAppRels = oldDatabase.getSiteAAProjectStartAppRel(adminAreas);
					if(!siteAAProjectStartAppRels.isEmpty()) {
						newDatabase.insertSiteAdminAreaProjectStartAppRelData(siteAAProjectStartAppRels);
					}
					
					List<UserAdminAreaUserApplication> userSiteAAUserAppRels = oldDatabase.getUserSiteAAUserAppRel();
					if(!userSiteAAUserAppRels.isEmpty()) {
						newDatabase.insertUserSiteAdminAreaUserAppRelData(userSiteAAUserAppRels);
					}
					
					List<UserAdminAreaProjectProjectApplication> userSiteAAProjectProjectAppRels = oldDatabase.getUserSiteAAProjectProjectAppRel();
					if(!userSiteAAProjectProjectAppRels.isEmpty()) {
						newDatabase.insertUserSiteAdminAreaProjectProjectAppRelData(userSiteAAProjectProjectAppRels);
					}
				}
				
				System.out.println("Migrating User History Data. Please wait, this may take a while....");
				
				List<UserHistory> userStatus = oldDatabase.getUserStatus();
				if (!userStatus.isEmpty()) {
					newDatabase.insertUserHistoryData(userStatus);
				}
				
				List<UserHistory> userHistory = oldDatabase.getUserHistory();
				if (!userHistory.isEmpty()) {
					newDatabase.insertUserHistoryData(userHistory);
				}
			}
			
			System.out.println("Data Migration Successful on " + (new Date()) + "....");			
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Method for Inits the new DB details.
	 */
	private static void initNewDBDetails() {
		NEW_DB_URL = "jdbc:oracle:thin:@localhost:1521:XE";
		NEW_DB_USERNAME = "magna";
		NEW_DB_PASSWORD = "magna";
		
		/*NEW_DB_URL = "jdbc:oracle:thin:@192.168.1.107:1521:XE";
		NEW_DB_USERNAME = "magnadb";
		NEW_DB_PASSWORD = "magnadb";*/
		
		boolean isDebug = java.lang.management.ManagementFactory.getRuntimeMXBean().getInputArguments().toString()
				.indexOf("-agentlib:jdwp") > 0;
		System.out.println("Debug Mode : " + isDebug);
	}

	/**
	 * Method for Inits the old DB details.
	 */
	private static void initOldDBDetails() {
		OLD_DB_URL = "jdbc:oracle:thin:@localhost:1521:XE";
		OLD_DB_USERNAME = "CISADM3";
		OLD_DB_PASSWORD = "CISADM3";
	}

}
