package com.magna.xmsystem.datamigration.readexcel;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.magna.xmsystem.datamigration.util.MigrationConstents;

/**
 * Class for Read data from excel.
 *
 * @author Chiranjeevi.Akula
 */
public class ReadDataFromExcel {

	/**
	 * Gets the site admin areas from excel.
	 *
	 * @param siteName
	 *            {@link String}
	 * @return the site admin areas from excel
	 */
	public List<Map<String, String>> getSiteAdminAreasFromExcel(String siteName) {
		List<Map<String, String>> adminAreas = new ArrayList<>();
		try {
			if (siteName != null && !siteName.isEmpty()) {
				String excelFilePath = MigrationConstents.Excels.EXCEL_PATH + MigrationConstents.Excels.SITE_ADMINAREA_MAPPING_EXCEL;
				FileInputStream inputStream = new FileInputStream(new File(excelFilePath));

				Workbook workbook = new XSSFWorkbook(inputStream);
				Sheet firstSheet = workbook.getSheetAt(0);
				Iterator<Row> iterator = firstSheet.iterator();

				while (iterator.hasNext()) {
					Row nextRow = iterator.next();
					String site = getCellText(nextRow.getCell(0));
					if (siteName.equals(site)) {
						String adminArea = getCellText(nextRow.getCell(1));
						String status = getCellText(nextRow.getCell(2));
						if (adminArea != null && !adminArea.isEmpty() && status != null && !status.isEmpty()) {
							Map<String, String> details = new HashMap<>();
							details.put("name", adminArea);
							details.put("status", status);
							adminAreas.add(details);
						}
					}
				}

				workbook.close();
				inputStream.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return adminAreas;
	}

	/**
	 * Gets the AA projects from excel.
	 *
	 * @param siteName
	 *            {@link String}
	 * @param adminAreaName
	 *            {@link String}
	 * @return the AA projects from excel
	 */
	public List<Map<String, String>> getAAProjectsFromExcel(String siteName, String adminAreaName) {
		List<Map<String, String>> projects = new ArrayList<>();
		try {
			if (siteName != null && !siteName.isEmpty() && adminAreaName != null && !adminAreaName.isEmpty()) {
				String excelFilePath = MigrationConstents.Excels.EXCEL_PATH
						+ MigrationConstents.Excels.SITE_ADMINAREA_PROJECT_MAPPING_EXCEL;
				FileInputStream inputStream = new FileInputStream(new File(excelFilePath));

				Workbook workbook = new XSSFWorkbook(inputStream);
				Sheet firstSheet = workbook.getSheetAt(0);
				Iterator<Row> iterator = firstSheet.iterator();

				while (iterator.hasNext()) {
					Row nextRow = iterator.next();
					String site = getCellText(nextRow.getCell(0));
					String adminArea = getCellText(nextRow.getCell(1));
					if (siteName.equals(site) && adminAreaName.equals(adminArea)) {
						String project = getCellText(nextRow.getCell(2));
						String status = getCellText(nextRow.getCell(3));
						if (project != null && !project.isEmpty() && status != null && !status.isEmpty()) {
							Map<String, String> details = new HashMap<>();
							details.put("name", project);
							details.put("status", status);
							projects.add(details);
						}
					}
				}

				workbook.close();
				inputStream.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return projects;
	}

	/**
	 * Gets the AA user apps from excel.
	 *
	 * @param siteName
	 *            {@link String}
	 * @param adminAreaName
	 *            {@link String}
	 * @return the AA user apps from excel
	 */
	public List<Map<String, String>> getAAUserAppsFromExcel(String siteName, String adminAreaName) {
		List<Map<String, String>> userAppMap = new ArrayList<>();
		try {
			if (siteName != null && !siteName.isEmpty() && adminAreaName != null && !adminAreaName.isEmpty()) {
				String excelFilePath = MigrationConstents.Excels.EXCEL_PATH
						+ MigrationConstents.Excels.SITE_ADMINAREA_USERAPP_MAPPING_EXCEL;
				FileInputStream inputStream = new FileInputStream(new File(excelFilePath));

				Workbook workbook = new XSSFWorkbook(inputStream);
				Sheet firstSheet = workbook.getSheetAt(0);
				Iterator<Row> iterator = firstSheet.iterator();

				while (iterator.hasNext()) {
					Row nextRow = iterator.next();
					String site = getCellText(nextRow.getCell(0));
					String adminArea = getCellText(nextRow.getCell(1));
					if (siteName.equals(site) && adminAreaName.equals(adminArea)) {
						String userAppName = getCellText(nextRow.getCell(2));
						String status = getCellText(nextRow.getCell(3));
						String relType = getCellText(nextRow.getCell(4));
						if (userAppName != null && !userAppName.isEmpty() && status != null && !status.isEmpty()
								&& relType != null && !relType.isEmpty()) {
							Map<String, String> details = new HashMap<>();
							details.put("name", userAppName);
							details.put("status", status);
							details.put("relType", relType);
							userAppMap.add(details);
						}
					}
				}

				workbook.close();
				inputStream.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return userAppMap;
	}

	/**
	 * Gets the AA start apps from excel.
	 *
	 * @param siteName
	 *            {@link String}
	 * @param adminAreaName
	 *            {@link String}
	 * @return the AA start apps from excel
	 */
	public List<Map<String, String>> getAAStartAppsFromExcel(String siteName, String adminAreaName) {
		List<Map<String, String>> startAppMap = new ArrayList<>();
		try {
			if (siteName != null && !siteName.isEmpty() && adminAreaName != null && !adminAreaName.isEmpty()) {
				String excelFilePath = MigrationConstents.Excels.EXCEL_PATH
						+ MigrationConstents.Excels.SITE_ADMINAREA_STARTAPP_MAPPING_EXCEL;
				FileInputStream inputStream = new FileInputStream(new File(excelFilePath));

				Workbook workbook = new XSSFWorkbook(inputStream);
				Sheet firstSheet = workbook.getSheetAt(0);
				Iterator<Row> iterator = firstSheet.iterator();

				while (iterator.hasNext()) {
					Row nextRow = iterator.next();
					String site = getCellText(nextRow.getCell(0));
					String adminArea = getCellText(nextRow.getCell(1));
					if (siteName.equals(site) && adminAreaName.equals(adminArea)) {
						String startAppName = getCellText(nextRow.getCell(2));
						String status = getCellText(nextRow.getCell(3));
						if (startAppName != null && !startAppName.isEmpty() && status != null && !status.isEmpty()) {
							Map<String, String> details = new HashMap<>();
							details.put("name", startAppName);
							details.put("status", status);
							startAppMap.add(details);
						}
					}
				}

				workbook.close();
				inputStream.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return startAppMap;
	}

	
	/**
	 * Gets the AA project project apps from excel.
	 *
	 * @param siteName {@link String}
	 * @param adminAreaName {@link String}
	 * @param projectName {@link String}
	 * @return the AA project project apps from excel
	 */
	public List<Map<String, String>> getAAProjectProjectAppsFromExcel(String siteName, String adminAreaName, String projectName) {
		List<Map<String, String>> projectAppMap = new ArrayList<>();
		try {
			if (siteName != null && !siteName.isEmpty() && adminAreaName != null && !adminAreaName.isEmpty() && projectName != null && !projectName.isEmpty()) {
				String excelFilePath = MigrationConstents.Excels.EXCEL_PATH
						+ MigrationConstents.Excels.SITE_ADMINAREA_PROJECT_PROJECTAPP_MAPPING_EXCEL;
				FileInputStream inputStream = new FileInputStream(new File(excelFilePath));

				Workbook workbook = new XSSFWorkbook(inputStream);
				Sheet firstSheet = workbook.getSheetAt(0);
				Iterator<Row> iterator = firstSheet.iterator();

				while (iterator.hasNext()) {
					Row nextRow = iterator.next();
					String site = getCellText(nextRow.getCell(0));
					String adminArea = getCellText(nextRow.getCell(1));
					String project = getCellText(nextRow.getCell(2));
					if (siteName.equals(site) && adminAreaName.equals(adminArea) && projectName.equals(project)) {
						String projectAppName = getCellText(nextRow.getCell(3));
						String status = getCellText(nextRow.getCell(4));
						String relType = getCellText(nextRow.getCell(5));
						if (projectAppName != null && !projectAppName.isEmpty() && status != null && !status.isEmpty()
								&& relType != null && !relType.isEmpty()) {
							Map<String, String> details = new HashMap<>();
							details.put("name", projectAppName);
							details.put("status", status);
							details.put("relType", relType);
							projectAppMap.add(details);
						}
					}
				}

				workbook.close();
				inputStream.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return projectAppMap;
	}
	
	
	/**
	 * Gets the AA project start apps from excel.
	 *
	 * @param siteName {@link String}
	 * @param adminAreaName {@link String}
	 * @param projectName {@link String}
	 * @return the AA project start apps from excel
	 */
	public List<Map<String, String>> getAAProjectStartAppsFromExcel(String siteName, String adminAreaName, String projectName) {
		List<Map<String, String>> startAppMap = new ArrayList<>();
		try {
			if (siteName != null && !siteName.isEmpty() && adminAreaName != null && !adminAreaName.isEmpty() && projectName != null && !projectName.isEmpty()) {
				String excelFilePath = MigrationConstents.Excels.EXCEL_PATH
						+ MigrationConstents.Excels.SITE_ADMINAREA_PROJECT_STARTAPP_MAPPING_EXCEL;
				FileInputStream inputStream = new FileInputStream(new File(excelFilePath));

				Workbook workbook = new XSSFWorkbook(inputStream);
				Sheet firstSheet = workbook.getSheetAt(0);
				Iterator<Row> iterator = firstSheet.iterator();

				while (iterator.hasNext()) {
					Row nextRow = iterator.next();
					String site = getCellText(nextRow.getCell(0));
					String adminArea = getCellText(nextRow.getCell(1));
					String project = getCellText(nextRow.getCell(2));
					if (siteName.equals(site) && adminAreaName.equals(adminArea) && projectName.equals(project)) {
						String startAppName = getCellText(nextRow.getCell(3));
						String status = getCellText(nextRow.getCell(4));
						if (startAppName != null && !startAppName.isEmpty() && status != null && !status.isEmpty()) {
							Map<String, String> details = new HashMap<>();
							details.put("name", startAppName);
							details.put("status", status);
							startAppMap.add(details);
						}
					}
				}

				workbook.close();
				inputStream.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return startAppMap;
	}
	
	/**
	 * Gets the cell text.
	 *
	 * @param cell
	 *            {@link Cell}
	 * @return the cell text
	 */
	private String getCellText(final Cell cell) {
		String cellText = null;
		cell.setCellType(Cell.CELL_TYPE_STRING);
		cellText = cell.getStringCellValue();

		return cellText == null ? "" : cellText.trim();
	}
}