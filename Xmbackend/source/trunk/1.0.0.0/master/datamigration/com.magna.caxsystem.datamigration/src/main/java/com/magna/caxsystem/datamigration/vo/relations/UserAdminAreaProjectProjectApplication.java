package com.magna.caxsystem.datamigration.vo.relations;

import com.magna.caxsystem.datamigration.enums.UserRelationType;
import com.magna.caxsystem.datamigration.util.GenerateUuid;

// TODO: Auto-generated Javadoc
/**
 * Class for User admin area project project application.
 *
 * @author Chiranjeevi.Akula
 */
public class UserAdminAreaProjectProjectApplication {

	/** Member variable 'id' for {@link String}. */
	private String id;

	/** Member variable 'user project id' for {@link String}. */
	private String userProjectId;

	/** Member variable 'site admin area project id' for {@link String}. */
	private String siteAdminAreaProjectId;
	
	/** Member variable 'project app id' for {@link String}. */
	private String projectAppId;

	/** Member variable 'rel type' for {@link String}. */
	private String relType;

	/**
	 * Constructor for UserAdminAreaProjectProjectApplication Class.
	 */
	public UserAdminAreaProjectProjectApplication() {
		this.id = GenerateUuid.getUuid();
	}

	/**
	 * Constructor for UserAdminAreaProjectProjectApplication Class.
	 *
	 * @param userProjectId {@link String}
	 * @param siteAdminAreaProjectId {@link String}
	 * @param projectAppId {@link String}
	 * @param relType {@link String}
	 */
	public UserAdminAreaProjectProjectApplication(String userProjectId, String siteAdminAreaProjectId, String projectAppId, String relType) {
		this.id = GenerateUuid.getUuid();
		this.userProjectId = userProjectId;
		this.siteAdminAreaProjectId = siteAdminAreaProjectId;
		this.projectAppId = projectAppId;
		this.relType = relType;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Gets the user project id.
	 *
	 * @return the user project id
	 */
	public String getUserProjectId() {
		return userProjectId;
	}

	/**
	 * Sets the user project id.
	 *
	 * @param userProjectId the new user project id
	 */
	public void setUserProjectId(String userProjectId) {
		this.userProjectId = userProjectId;
	}

	/**
	 * Gets the site admin area project id.
	 *
	 * @return the site admin area project id
	 */
	public String getSiteAdminAreaProjectId() {
		return siteAdminAreaProjectId;
	}

	/**
	 * Sets the site admin area project id.
	 *
	 * @param siteAdminAreaProjectId the new site admin area project id
	 */
	public void setSiteAdminAreaProjectId(String siteAdminAreaProjectId) {
		this.siteAdminAreaProjectId = siteAdminAreaProjectId;
	}

	/**
	 * Gets the project app id.
	 *
	 * @return the project app id
	 */
	public String getProjectAppId() {
		return projectAppId;
	}

	/**
	 * Sets the project app id.
	 *
	 * @param projectAppId the new project app id
	 */
	public void setProjectAppId(String projectAppId) {
		this.projectAppId = projectAppId;
	}

	/**
	 * Gets the rel type.
	 *
	 * @return the rel type
	 */
	public String getRelType() {
		return relType;
	}

	/**
	 * Sets the rel type.
	 *
	 * @param relCode the new rel type
	 */
	public void setRelType(String relCode) {
		if ("1".equals(relCode)) {
			this.relType = UserRelationType.NOTALLOWED.toString();
		} else {
			this.relType = UserRelationType.ALLOWED.toString();
		}
	}
}
