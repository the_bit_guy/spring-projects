package com.magna.caxsystem.datamigration.vo.relations;

import com.magna.caxsystem.datamigration.enums.Status;
import com.magna.caxsystem.datamigration.util.GenerateUuid;

/**
 * Class for Site admin area project start application.
 *
 * @author Chiranjeevi.Akula
 */
public class SiteAdminAreaProjectStartApplication {

	/** Member variable 'id' for {@link String}. */
	private String id;

	/** Member variable 'site admin area project id' for {@link String}. */
	private String siteAdminAreaProjectId;

	/** Member variable 'start application id' for {@link String}. */
	private String startApplicationId;

	/** Member variable 'status' for {@link String}. */
	private String status;

	/**
	 * Constructor for SiteAdminAreaProjectStartApplication Class.
	 */
	public SiteAdminAreaProjectStartApplication() {
		this.id = GenerateUuid.getUuid();
	}

	/**
	 * Constructor for SiteAdminAreaProjectStartApplication Class.
	 *
	 * @param siteAdminAreaProjectId
	 *            {@link String}
	 * @param startApplicationId
	 *            {@link String}
	 * @param status
	 *            {@link String}
	 */
	public SiteAdminAreaProjectStartApplication(String siteAdminAreaProjectId, String startApplicationId,
			String status) {
		this.id = GenerateUuid.getUuid();
		this.siteAdminAreaProjectId = siteAdminAreaProjectId;
		this.startApplicationId = startApplicationId;
		this.status = status;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id
	 *            the new id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Gets the site admin area project id.
	 *
	 * @return the site admin area project id
	 */
	public String getSiteAdminAreaProjectId() {
		return siteAdminAreaProjectId;
	}

	/**
	 * Sets the site admin area project id.
	 *
	 * @param siteAdminAreaProjectId
	 *            the new site admin area project id
	 */
	public void setSiteAdminAreaProjectId(String siteAdminAreaProjectId) {
		this.siteAdminAreaProjectId = siteAdminAreaProjectId;
	}

	/**
	 * Gets the start application id.
	 *
	 * @return the start application id
	 */
	public String getStartApplicationId() {
		return startApplicationId;
	}

	/**
	 * Sets the start application id.
	 *
	 * @param startApplicationId
	 *            the new start application id
	 */
	public void setStartApplicationId(String startApplicationId) {
		this.startApplicationId = startApplicationId;
	}

	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 *
	 * @param statusCode
	 *            the new status
	 */
	public void setStatus(String statusCode) {
		if ("1".equals(statusCode)) {
			this.status = Status.ACTIVE.toString();
		} else {
			this.status = Status.INACTIVE.toString();
		}
	}
}
