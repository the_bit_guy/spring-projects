package com.magna.caxsystem.datamigration.jdbc.olddb;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magna.caxsystem.datamigration.enums.DirectoryObjectType;
import com.magna.caxsystem.datamigration.enums.LanguageCode;
import com.magna.caxsystem.datamigration.enums.PLATFORMS;
import com.magna.caxsystem.datamigration.enums.TranslationKey;
import com.magna.caxsystem.datamigration.readexcel.ReadDataFromExcel;
import com.magna.caxsystem.datamigration.util.ObjectNameIdDetails;
import com.magna.caxsystem.datamigration.vo.adminarea.AdminArea;
import com.magna.caxsystem.datamigration.vo.baseapplication.BaseAppTranslation;
import com.magna.caxsystem.datamigration.vo.baseapplication.BaseApplication;
import com.magna.caxsystem.datamigration.vo.directory.Directory;
import com.magna.caxsystem.datamigration.vo.directory.DirectoryObjectReference;
import com.magna.caxsystem.datamigration.vo.directory.DirectoryTranslation;
import com.magna.caxsystem.datamigration.vo.project.Project;
import com.magna.caxsystem.datamigration.vo.project.ProjectTranslation;
import com.magna.caxsystem.datamigration.vo.projectapplication.ProjectApp;
import com.magna.caxsystem.datamigration.vo.projectapplication.ProjectAppTranslation;
import com.magna.caxsystem.datamigration.vo.relations.SiteAdminArea;
import com.magna.caxsystem.datamigration.vo.relations.SiteAdminAreaProject;
import com.magna.caxsystem.datamigration.vo.relations.SiteAdminAreaProjectProjectApplication;
import com.magna.caxsystem.datamigration.vo.relations.SiteAdminAreaProjectStartApplication;
import com.magna.caxsystem.datamigration.vo.relations.SiteAdminAreaStartApplication;
import com.magna.caxsystem.datamigration.vo.relations.SiteAdminAreaUserApplication;
import com.magna.caxsystem.datamigration.vo.relations.UserAdminAreaProjectProjectApplication;
import com.magna.caxsystem.datamigration.vo.relations.UserAdminAreaUserApplication;
import com.magna.caxsystem.datamigration.vo.relations.UserProject;
import com.magna.caxsystem.datamigration.vo.relations.UserStartApplication;
import com.magna.caxsystem.datamigration.vo.site.Site;
import com.magna.caxsystem.datamigration.vo.site.SiteTranslation;
import com.magna.caxsystem.datamigration.vo.startapplication.StartApp;
import com.magna.caxsystem.datamigration.vo.startapplication.StartAppTranslation;
import com.magna.caxsystem.datamigration.vo.user.User;
import com.magna.caxsystem.datamigration.vo.user.UserTranslation;
import com.magna.caxsystem.datamigration.vo.userapplication.UserApp;
import com.magna.caxsystem.datamigration.vo.userapplication.UserAppTranslation;
import com.magna.caxsystem.datamigration.vo.userhistory.UserHistory;

/**
 * Class for Old database.
 *
 * @author Chiranjeevi.Akula
 */
public class OldDatabase {
	
	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(OldDatabase.class);
	
	/** Member variable 'old db url' for {@link String}. */
	private String OLD_DB_URL;
	
	/** Member variable 'old db username' for {@link String}. */
	private String OLD_DB_USERNAME;
	
	/** Member variable 'old db password' for {@link String}. */
	private String OLD_DB_PASSWORD;

	private Map<TranslationKey, Map<String,Map<LanguageCode, String>>> translations;
	
	private List<Site> sites;
	
	private List<Project> projects;
	
	private List<UserApp> userApps;
	
	private List<ProjectApp> projectApps;
	
	private List<StartApp>  startApps;
	
	private List<User> users;
	
	private List<Directory> directories;

	private List<SiteAdminArea> siteAdminAreas;

	private List<SiteAdminAreaProject> siteAdminAreaProjects;

	private List<SiteAdminAreaUserApplication> siteAdminAreaUserApps;	

	private List<SiteAdminAreaStartApplication> siteAdminAreaStartApps;

	private List<SiteAdminAreaProjectProjectApplication> siteAdminAreaProjectProjectApps;

	private List<SiteAdminAreaProjectStartApplication> siteAdminAreaProjectStartApps;

	private List<UserProject> userProjects;

	private List<UserStartApplication> userStartApps;

	private List<DirectoryObjectReference> directoryObjectRefs;
	
	private List<DirectoryObjectReference> adminMenudirectoryObjectRefs;
	
	private List<UserAdminAreaUserApplication> userAdminAreaUserApplications;
	
	private List<UserAdminAreaProjectProjectApplication> userAdminAreaProjectProjectApplications;

	/**
	 * Constructor for OldDatabase Class.
	 *
	 * @param OLD_DB_URL {@link String}
	 * @param OLD_DB_USERNAME {@link String}
	 * @param OLD_DB_PASSWORD {@link String}
	 */
	public OldDatabase(final String OLD_DB_URL, final String OLD_DB_USERNAME, final String OLD_DB_PASSWORD) {
		this.OLD_DB_URL = OLD_DB_URL;
		this.OLD_DB_USERNAME = OLD_DB_USERNAME;
		this.OLD_DB_PASSWORD = OLD_DB_PASSWORD;
	}

	/**
	 * Gets the site data.
	 *
	 * @return the site data
	 */
	public List<Site> getSiteData() {
		Connection connection = null;
		Statement statement = null;

		sites = new ArrayList<>();

		try {
			connection = DriverManager.getConnection(OLD_DB_URL, OLD_DB_USERNAME, OLD_DB_PASSWORD);
			statement = connection.createStatement();
			String sql;
			sql = "SELECT * FROM CISADM3.SITES";
			ResultSet rs = statement.executeQuery(sql);

			while (rs.next()) {
				Site site = new Site();
				site.setName(rs.getString("NAME"));
				site.setIconName(rs.getString("ICONP"));
				site.setStatus(rs.getInt("ACTIVE"));
				LanguageCode[] languageCodes = LanguageCode.values();
				for (LanguageCode languageCode : languageCodes) {
					SiteTranslation siteTranslation = new SiteTranslation(languageCode.toString());
					siteTranslation.setDescription(rs.getString("DESCRIPTION"));
					siteTranslation.setRemarks(rs.getString("HELPTEXT"));
					site.setSiteTranslation(siteTranslation);
				}
				sites.add(site);
			}
			rs.close();
			statement.close();
			connection.close();
		} catch (Exception e) {
			LOGGER.error(e.toString());
		} finally {
			try {
				if (statement != null) {
					statement.close();
				}
			} catch (SQLException sqlException) {
				LOGGER.error(sqlException.toString());
			}
			try {
				if (connection != null) {
					connection.close();
				}
			} catch (SQLException sqlException) {
				LOGGER.error(sqlException.toString());
			}
		}
		return sites;
	}

	/**
	 * Gets the project data.
	 *
	 * @return the project data
	 */
	public List<Project> getProjectData() {
		Connection connection = null;
		Statement statement = null;

		projects = new ArrayList<>();

		try {
			connection = DriverManager.getConnection(OLD_DB_URL, OLD_DB_USERNAME, OLD_DB_PASSWORD);
			statement = connection.createStatement();
			String sql;
			sql = "SELECT * FROM CISADM3.PROJECTS WHERE NAME <> '*UserTasks*'";
			ResultSet rs = statement.executeQuery(sql);

			while (rs.next()) {
				Project project = new Project();
				project.setName(rs.getString("NAME"));
				project.setIconName(rs.getString("ICONP"));
				project.setStatus(rs.getInt("ACTIVE"));
				LanguageCode[] languageCodes = LanguageCode.values();
				for (LanguageCode languageCode : languageCodes) {
					ProjectTranslation projectTranslation = new ProjectTranslation(languageCode.toString());					
					projectTranslation.setDescription(rs.getString("DESCRIPTION"));
					projectTranslation.setRemarks(rs.getString("HELPTEXT"));
					project.setProjectTranslation(projectTranslation);
				}
				projects.add(project);
			}
			rs.close();
			statement.close();
			connection.close();
		} catch (Exception e) {
			LOGGER.error(e.toString());
		} finally {
			try {
				if (statement != null) {
					statement.close();
				}
			} catch (SQLException sqlException) {
				LOGGER.error(sqlException.toString());
			}
			try {
				if (connection != null) {
					connection.close();
				}
			} catch (SQLException sqlException) {
				LOGGER.error(sqlException.toString());
			}
		}
		return projects;
	}

	/**
	 * Gets the base app data.
	 *
	 * @return the base app data
	 */
	public List<BaseApplication> getBaseAppData() {
		Connection connection = null;
		Statement statement = null;

		List<BaseApplication> baseApplications = new ArrayList<>();

		try {
			connection = DriverManager.getConnection(OLD_DB_URL, OLD_DB_USERNAME, OLD_DB_PASSWORD);
			statement = connection.createStatement();
			String sql;
			sql = "SELECT * FROM CISADM3.TASKS WHERE NAME NOT IN ('*OpenProject*', '*OpenSite*', '*OpenUser*', '*ProjectEvent*', '*SiteEvent*', '*SysLogin*', '*UserEvent*', '*WaitTask*')";
			ResultSet rs = statement.executeQuery(sql);

			while (rs.next()) {
				BaseApplication baseApplication = new BaseApplication();
				baseApplication.setName(rs.getString("NAME"));
				baseApplication.setIconName(rs.getString("ICONP"));
				baseApplication.setStatus(rs.getInt("ACTIVE"));
				String program = rs.getString("PROGRAM");
				baseApplication.setProgram(program);
				baseApplication.setPlatforms(getSupportedPlatforms(program));
				LanguageCode[] languageCodes = LanguageCode.values();
				for (LanguageCode languageCode : languageCodes) {
					BaseAppTranslation baseAppTranslation = new BaseAppTranslation(languageCode.toString());					
					baseAppTranslation.setDescription(rs.getString("DESCRIPTION"));
					baseAppTranslation.setRemarks(rs.getString("HELPTEXT"));
					baseApplication.setBaseAppTranslation(baseAppTranslation);
				}
				baseApplications.add(baseApplication);
			}
			rs.close();
			statement.close();
			connection.close();
		} catch (Exception e) {
			LOGGER.error(e.toString());
		} finally {
			try {
				if (statement != null) {
					statement.close();
				}
			} catch (SQLException sqlException) {
				LOGGER.error(sqlException.toString());
			}
			try {
				if (connection != null) {
					connection.close();
				}
			} catch (SQLException sqlException) {
				LOGGER.error(sqlException.toString());
			}
		}
		return baseApplications;
	}

	/**
	 * Gets the user data.
	 *
	 * @return the user data
	 */
	public List<User> getUserData() {
		Connection connection = null;
		Statement statement = null;

		users = new ArrayList<>();

		try {
			connection = DriverManager.getConnection(OLD_DB_URL, OLD_DB_USERNAME, OLD_DB_PASSWORD);
			statement = connection.createStatement();
			String sql;
			sql = "SELECT * FROM CISADM3.USERS";
			ResultSet rs = statement.executeQuery(sql);

			while (rs.next()) {
				User user = new User();
				user.setUserName(rs.getString("NAME"));
				user.setIconName(rs.getString("ICONP"));
				user.setStatus(rs.getInt("ACTIVE"));
				LanguageCode[] languageCodes = LanguageCode.values();
				for (LanguageCode languageCode : languageCodes) {
					UserTranslation userTranslation = new UserTranslation(languageCode.toString());
					userTranslation.setDescription(rs.getString("DESCRIPTION"));
					userTranslation.setRemarks(rs.getString("HELPTEXT"));
					user.setUserTranslation(userTranslation);
				}
				users.add(user);
			}
			rs.close();
			statement.close();
			connection.close();
		} catch (Exception e) {
			LOGGER.error(e.toString());
		} finally {
			try {
				if (statement != null) {
					statement.close();
				}
			} catch (SQLException sqlException) {
				LOGGER.error(sqlException.toString());
			}
			try {
				if (connection != null) {
					connection.close();
				}
			} catch (SQLException sqlException) {
				LOGGER.error(sqlException.toString());
			}
		}
		return users;
	}

	/**
	 * Gets the user application data.
	 *
	 * @return the user application data
	 */
	public List<UserApp> getUserApplicationData() {
		Connection connection = null;
		Statement statement = null;

		userApps = new ArrayList<>();

		Map<String, Map<LanguageCode, String>> nameTranslations = translations.get(TranslationKey.NAME);
		Map<String, Map<LanguageCode, String>> descTranslations = translations.get(TranslationKey.DESC);
		
		try {
			connection = DriverManager.getConnection(OLD_DB_URL, OLD_DB_USERNAME, OLD_DB_PASSWORD);
			statement = connection.createStatement();
			String sql;
			sql = "SELECT * FROM CISADM3.PROJECTTASKS WHERE USERTASK = 1";
			ResultSet rs = statement.executeQuery(sql);

			while (rs.next()) {
				UserApp userApp = new UserApp();
				String appName = rs.getString("NAME");
				userApp.setName(appName);
				userApp.setDescription(rs.getString("DESCRIPTION"));
				userApp.setIconName(rs.getString("ICONP"));
				userApp.setStatus(rs.getInt("ACTIVE"));
				String baseAppName = rs.getString("TASK");
				userApp.setBaseAppName(baseAppName);
				userApp.setPosition(rs.getString("PARENTNAME"));
				userApp.setIsSingleton("false");

				if (baseAppName == null && isParent(connection, appName)) {
					userApp.setIsParent("true");
				} else {
					userApp.setIsParent("false");
				}

				Map<LanguageCode, String> nameTransMap = nameTranslations.get(appName);
				Map<LanguageCode, String> descTransMap = descTranslations.get(appName);
				
				LanguageCode[] languageCodes = LanguageCode.values();
				for (LanguageCode languageCode : languageCodes) {
					UserAppTranslation userAppTranslation = new UserAppTranslation(languageCode.toString());

					if (nameTransMap != null) {
						String name = nameTransMap.get(languageCode);
						if (name != null) {
							userAppTranslation.setName(name);
						}
					}

					if (descTransMap != null) {
						String desc = descTransMap.get(languageCode);
						if (desc != null) {
							userAppTranslation.setDescription(desc);
						}
					}

					userAppTranslation.setRemarks(rs.getString("HELPTEXT"));
					userApp.setUserAppTranslation(userAppTranslation);
				}
				userApps.add(userApp);
			}
			rs.close();
			statement.close();
			connection.close();
		} catch (Exception e) {
			LOGGER.error(e.toString());
		} finally {
			try {
				if (statement != null) {
					statement.close();
				}
			} catch (SQLException sqlException) {
				LOGGER.error(sqlException.toString());
			}
			try {
				if (connection != null) {
					connection.close();
				}
			} catch (SQLException sqlException) {
				LOGGER.error(sqlException.toString());
			}
		}
		return userApps;
	}

	/**
	 * Gets the project application data.
	 *
	 * @return the project application data
	 */
	public List<ProjectApp> getProjectApplicationData() {
		Connection connection = null;
		Statement statement = null;

		projectApps = new ArrayList<>();

		Map<String, Map<LanguageCode, String>> nameTranslations = translations.get(TranslationKey.NAME);
		Map<String, Map<LanguageCode, String>> descTranslations = translations.get(TranslationKey.DESC);
		
		try {
			connection = DriverManager.getConnection(OLD_DB_URL, OLD_DB_USERNAME, OLD_DB_PASSWORD);
			statement = connection.createStatement();
			String sql;
			sql = "SELECT * FROM CISADM3.PROJECTTASKS WHERE USERTASK = 0 AND NAME NOT IN('*ButtonTask*', '*IconTask*', '*MenuTask*')";
			ResultSet rs = statement.executeQuery(sql);

			while (rs.next()) {
				ProjectApp projectApp = new ProjectApp();
				String appName = rs.getString("NAME");
				projectApp.setName(appName);
				projectApp.setDescription(rs.getString("DESCRIPTION"));
				projectApp.setIconName(rs.getString("ICONP"));
				projectApp.setStatus(rs.getInt("ACTIVE"));
				String baseAppName = rs.getString("TASK");
				projectApp.setBaseAppName(baseAppName);
				projectApp.setPosition(rs.getString("PARENTNAME"));
				projectApp.setIsSingleton("false");

				if (baseAppName == null && isParent(connection, appName)) {
					projectApp.setIsParent("true");
				} else {
					projectApp.setIsParent("false");
				}

				Map<LanguageCode, String> nameTransMap = nameTranslations.get(appName);
				Map<LanguageCode, String> descTransMap = descTranslations.get(appName);
				
				LanguageCode[] languageCodes = LanguageCode.values();
				for (LanguageCode languageCode : languageCodes) {
					ProjectAppTranslation projectAppTranslation = new ProjectAppTranslation(languageCode.toString());

					if (nameTransMap != null) {
						String name = nameTransMap.get(languageCode);
						if (name != null) {
							projectAppTranslation.setName(name);
						}
					}

					if (descTransMap != null) {
						String desc = descTransMap.get(languageCode);
						if (desc != null) {
							projectAppTranslation.setDescription(desc);
						}
					}
					
					projectAppTranslation.setRemarks(rs.getString("HELPTEXT"));
					projectApp.setProjectAppTranslation(projectAppTranslation);
				}
				projectApps.add(projectApp);
			}
			rs.close();
			statement.close();
			connection.close();
		} catch (Exception e) {
			LOGGER.error(e.toString());
		} finally {
			try {
				if (statement != null) {
					statement.close();
				}
			} catch (SQLException sqlException) {
				LOGGER.error(sqlException.toString());
			}
			try {
				if (connection != null) {
					connection.close();
				}
			} catch (SQLException sqlException) {
				LOGGER.error(sqlException.toString());
			}
		}
		return projectApps;
	}
	
	/**
	 * Gets the start application data.
	 *
	 * @return the start application data
	 */
	public List<StartApp> getStartApplicationData() {
		Connection connection = null;
		Statement statement = null;

		startApps = new ArrayList<>();

		try {
			connection = DriverManager.getConnection(OLD_DB_URL, OLD_DB_USERNAME, OLD_DB_PASSWORD);
			statement = connection.createStatement();
			String sql;
			sql = "SELECT * FROM CISADM3.INITTASKS";
			ResultSet rs = statement.executeQuery(sql);

			while (rs.next()) {
				StartApp startApp = new StartApp();
				startApp.setName(rs.getString("NAME"));
				startApp.setIconName(rs.getString("ICONP"));
				startApp.setStatus(rs.getInt("ACTIVE"));
				startApp.setMessage(false);
				String baseAppName = rs.getString("TASK");
				startApp.setBaseAppName(baseAppName);

				LanguageCode[] languageCodes = LanguageCode.values();
				for (LanguageCode languageCode : languageCodes) {
					StartAppTranslation startAppTranslation = new StartAppTranslation(languageCode.toString());
					startAppTranslation.setDescription(rs.getString("DESCRIPTION"));
					startAppTranslation.setRemarks(rs.getString("HELPTEXT"));
					startApp.setStartAppTranslation(startAppTranslation);
				}
				startApps.add(startApp);
			}
			rs.close();
			statement.close();
			connection.close();
		} catch (Exception e) {
			LOGGER.error(e.toString());
		} finally {
			try {
				if (statement != null) {
					statement.close();
				}
			} catch (SQLException sqlException) {
				LOGGER.error(sqlException.toString());
			}
			try {
				if (connection != null) {
					connection.close();
				}
			} catch (SQLException sqlException) {
				LOGGER.error(sqlException.toString());
			}
		}
		return startApps;
	}
	
	/**
	 * Checks if is parent.
	 *
	 * @param connection {@link Connection}
	 * @param appName {@link String}
	 * @return true, if is parent
	 */
	private boolean isParent(Connection connection, String appName) {
		Statement stmt = null;
		boolean isDataExists = false;
		try {
			stmt = connection.createStatement();
			int rowCount = stmt.executeUpdate("SELECT NAME FROM CISADM3.PROJECTTASKS WHERE PARENTNAME = '" + appName + "'");
			if (rowCount > 0) {
				isDataExists = true;
			}
			stmt.close();
			connection.commit();
		} catch (Exception e) {
			LOGGER.error(e.toString());
		} finally {
			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException sqlException) {
				LOGGER.error(sqlException.toString());
			}
		}
		return isDataExists;
	}	
	
	/**
	 * Gets the site AA rel.
	 *
	 * @param adminAreas {@link List<AdminArea>}
	 * @return the site AA rel
	 */
	public List<SiteAdminArea> getSiteAARel(final List<AdminArea> adminAreas) {
		
		siteAdminAreas = new ArrayList<>();

		try {
			for(Site site : sites) {
				String siteName = site.getName();
				
				ReadDataFromExcel readDataFromExcel = new ReadDataFromExcel();
				List<Map<String, String>> adminAreasList = readDataFromExcel.getSiteAdminAreasFromExcel(siteName);
				if(adminAreasList != null && !adminAreasList.isEmpty()) {
					for (Map<String, String> adminAreaMap : adminAreasList) {
					    SiteAdminArea siteAdminArea = new SiteAdminArea();
						siteAdminArea.setSiteId(site.getId());
						siteAdminArea.setAdminAreaId(ObjectNameIdDetails.getAAIdByName(adminAreas, adminAreaMap.get("name")));
						siteAdminArea.setStatus(adminAreaMap.get("status"));
						
						siteAdminAreas.add(siteAdminArea);
					}			
				}
			}
		} catch (Exception e) {
			LOGGER.error(e.toString());
		} 
		return siteAdminAreas;
	}
	
	/**
	 * Gets the site AA project rel.
	 *
	 * @param adminAreas {@link List<AdminArea>}
	 * @return the site AA project rel
	 */
	public List<SiteAdminAreaProject> getSiteAAProjectRel(final List<AdminArea> adminAreas) {
		
		siteAdminAreaProjects = new ArrayList<>();

		try {
			for(SiteAdminArea siteAdminArea : siteAdminAreas) {
				String siteName = ObjectNameIdDetails.getSiteNameById(sites, siteAdminArea.getSiteId());
				String adminAreaName = ObjectNameIdDetails.getAANameById(adminAreas, siteAdminArea.getAdminAreaId());
				
				ReadDataFromExcel readDataFromExcel = new ReadDataFromExcel();
				List<Map<String, String>> projectsList = readDataFromExcel.getAAProjectsFromExcel(siteName, adminAreaName);
				if(projectsList != null && !projectsList.isEmpty()) {
					for (Map<String, String> projectMap : projectsList) {
						SiteAdminAreaProject siteAdminAreaProject = new SiteAdminAreaProject();
						siteAdminAreaProject.setSiteAdminAreaId(siteAdminArea.getId());
						siteAdminAreaProject.setProjectId(ObjectNameIdDetails.getProjectIdByName(projects, projectMap.get("name")));
						siteAdminAreaProject.setStatus(projectMap.get("status"));
						
						siteAdminAreaProjects.add(siteAdminAreaProject);
					}			
				}
			}
		} catch (Exception e) {
			LOGGER.error(e.toString());
		} 
		return siteAdminAreaProjects;
	}
	
	/**
	 * Gets the site AA user app rel.
	 *
	 * @param adminAreas {@link List<AdminArea>}
	 * @return the site AA user app rel
	 */
	public List<SiteAdminAreaUserApplication> getSiteAAUserAppRel(final List<AdminArea> adminAreas) {
		
		siteAdminAreaUserApps = new ArrayList<>();

		try {
			for(SiteAdminArea siteAdminArea : siteAdminAreas) {
				String siteName = ObjectNameIdDetails.getSiteNameById(sites, siteAdminArea.getSiteId());
				String adminAreaName = ObjectNameIdDetails.getAANameById(adminAreas, siteAdminArea.getAdminAreaId());
				
				ReadDataFromExcel readDataFromExcel = new ReadDataFromExcel();
				List<Map<String, String>> userAppsList = readDataFromExcel.getAAUserAppsFromExcel(siteName, adminAreaName);
				if(userAppsList != null && !userAppsList.isEmpty()) {
					for (Map<String, String> userAppMap : userAppsList) {
						SiteAdminAreaUserApplication siteAdminAreaUserApplication = new SiteAdminAreaUserApplication();
						siteAdminAreaUserApplication.setSiteAdminAreaId(siteAdminArea.getId());
						siteAdminAreaUserApplication.setUserApplicationId(ObjectNameIdDetails.getUserAppIdByName(userApps, userAppMap.get("name")));
						siteAdminAreaUserApplication.setStatus(userAppMap.get("status"));
						siteAdminAreaUserApplication.setRelType(userAppMap.get("relType"));
						
						siteAdminAreaUserApps.add(siteAdminAreaUserApplication);
					}			
				}
			}
		} catch (Exception e) {
			LOGGER.error(e.toString());
		} 
		return siteAdminAreaUserApps;
	}
	
	/**
	 * Gets the site AA start app rel.
	 *
	 * @param adminAreas {@link List<AdminArea>}
	 * @return the site AA start app rel
	 */
	public List<SiteAdminAreaStartApplication> getSiteAAStartAppRel(final List<AdminArea> adminAreas) {
		
		siteAdminAreaStartApps = new ArrayList<>();

		try {
			for(SiteAdminArea siteAdminArea : siteAdminAreas) {
				String siteName = ObjectNameIdDetails.getSiteNameById(sites, siteAdminArea.getSiteId());
				String adminAreaName = ObjectNameIdDetails.getAANameById(adminAreas, siteAdminArea.getAdminAreaId());
				
				ReadDataFromExcel readDataFromExcel = new ReadDataFromExcel();
				List<Map<String, String>> userAppsList = readDataFromExcel.getAAStartAppsFromExcel(siteName, adminAreaName);
				if(userAppsList != null && !userAppsList.isEmpty()) {
					for (Map<String, String> userAppMap : userAppsList) {
						SiteAdminAreaStartApplication siteAdminAreaStartApplication = new SiteAdminAreaStartApplication();
						siteAdminAreaStartApplication.setSiteAdminAreaId(siteAdminArea.getId());
						siteAdminAreaStartApplication.setStartApplicationId(ObjectNameIdDetails.getStartAppIdByName(startApps, userAppMap.get("name")));
						siteAdminAreaStartApplication.setStatus(userAppMap.get("status"));
						
						siteAdminAreaStartApps.add(siteAdminAreaStartApplication);
					}			
				}
			}
		} catch (Exception e) {
			LOGGER.error(e.toString());
		} 
		return siteAdminAreaStartApps;
	}
	
	/**
	 * Gets the site AA project project app rel.
	 *
	 * @param adminAreas {@link List<AdminArea>}
	 * @return the site AA project project app rel
	 */
	public List<SiteAdminAreaProjectProjectApplication> getSiteAAProjectProjectAppRel(final List<AdminArea> adminAreas) {
		
		siteAdminAreaProjectProjectApps = new ArrayList<>();

		try {
			for(SiteAdminAreaProject siteAdminAreaProject : siteAdminAreaProjects) {
				String siteAdminAreaId = siteAdminAreaProject.getSiteAdminAreaId();
				String projectName = ObjectNameIdDetails.getProjectNameById(projects, siteAdminAreaProject.getProjectId());
				String siteName = null;
				String adminAreaName = null;
				for(SiteAdminArea siteAdminArea : siteAdminAreas) {
					if(siteAdminArea.getId().equals(siteAdminAreaId)) {
						siteName = ObjectNameIdDetails.getSiteNameById(sites, siteAdminArea.getSiteId());
						adminAreaName = ObjectNameIdDetails.getAANameById(adminAreas, siteAdminArea.getAdminAreaId());
					}
				}				
				
				ReadDataFromExcel readDataFromExcel = new ReadDataFromExcel();
				List<Map<String, String>> projectAppsList = readDataFromExcel.getAAProjectProjectAppsFromExcel(siteName, adminAreaName, projectName);
				if(projectAppsList != null && !projectAppsList.isEmpty()) {
					for (Map<String, String> projectAppMap : projectAppsList) {
						SiteAdminAreaProjectProjectApplication siteAdminAreaProjectApplication = new SiteAdminAreaProjectProjectApplication();
						siteAdminAreaProjectApplication.setSiteAdminAreaProjectId(siteAdminAreaProject.getId());
						siteAdminAreaProjectApplication.setProjectApplicationId(ObjectNameIdDetails.getProjectAppIdByName(projectApps, projectAppMap.get("name")));
						siteAdminAreaProjectApplication.setStatus(projectAppMap.get("status"));
						siteAdminAreaProjectApplication.setRelType(projectAppMap.get("relType"));
						
						siteAdminAreaProjectProjectApps.add(siteAdminAreaProjectApplication);
					}			
				}
			}
		} catch (Exception e) {
			LOGGER.error(e.toString());
		} 
		return siteAdminAreaProjectProjectApps;
	}
	
	/**
	 * Gets the site AA project start app rel.
	 *
	 * @param adminAreas {@link List<AdminArea>}
	 * @return the site AA project start app rel
	 */
	public List<SiteAdminAreaProjectStartApplication> getSiteAAProjectStartAppRel(final List<AdminArea> adminAreas) {
		
		siteAdminAreaProjectStartApps = new ArrayList<>();

		try {
			for(SiteAdminAreaProject siteAdminAreaProject : siteAdminAreaProjects) {
				String siteAdminAreaId = siteAdminAreaProject.getSiteAdminAreaId();
				String projectName = ObjectNameIdDetails.getProjectNameById(projects, siteAdminAreaProject.getProjectId());
				String siteName = null;
				String adminAreaName = null;
				for(SiteAdminArea siteAdminArea : siteAdminAreas) {
					if(siteAdminArea.getId().equals(siteAdminAreaId)) {
						siteName = ObjectNameIdDetails.getSiteNameById(sites, siteAdminArea.getSiteId());
						adminAreaName = ObjectNameIdDetails.getAANameById(adminAreas, siteAdminArea.getAdminAreaId());
					}
				}				
				
				ReadDataFromExcel readDataFromExcel = new ReadDataFromExcel();
				List<Map<String, String>> startAppsList = readDataFromExcel.getAAProjectStartAppsFromExcel(siteName, adminAreaName, projectName);
				if(startAppsList != null && !startAppsList.isEmpty()) {
					for (Map<String, String> startAppMap : startAppsList) {
						SiteAdminAreaProjectStartApplication  siteAdminAreaProjectStartApplication = new SiteAdminAreaProjectStartApplication();
						siteAdminAreaProjectStartApplication.setSiteAdminAreaProjectId(siteAdminAreaProject.getId());
						siteAdminAreaProjectStartApplication.setStartApplicationId(ObjectNameIdDetails.getStartAppIdByName(startApps, startAppMap.get("name")));
						siteAdminAreaProjectStartApplication.setStatus(startAppMap.get("status"));
						
						siteAdminAreaProjectStartApps.add(siteAdminAreaProjectStartApplication);
					}			
				}
			}
		} catch (Exception e) {
			LOGGER.error(e.toString());
		} 
		return siteAdminAreaProjectStartApps;
	}
	
	/**
	 * Gets the user project rel.
	 *
	 * @return the user project rel
	 */
	public List<UserProject> getUserProjectRel() {
		Connection connection = null;
		Statement statement = null;
		
		userProjects = new ArrayList<>();

		try {
			connection = DriverManager.getConnection(OLD_DB_URL, OLD_DB_USERNAME, OLD_DB_PASSWORD);
			statement = connection.createStatement();
			String sql;
			sql = "SELECT USERNAME, PROJECT, P_ACTIVE FROM CISADM3.USER_REL WHERE USERNAME IS NOT NULL AND PROJECT IS NOT NULL AND P_ACTIVE IS NOT NULL";
			ResultSet rs = statement.executeQuery(sql);
			
			while (rs.next()) {
				String userId = ObjectNameIdDetails.getUserIdByName(users, rs.getString("USERNAME"));
				String projectId = ObjectNameIdDetails.getProjectIdByName(projects, rs.getString("PROJECT"));
				String status = rs.getInt("P_ACTIVE") + "";
				if(userId != null && !userId.isEmpty() && projectId != null && !projectId.isEmpty() && status != null && !status.isEmpty()) {
					UserProject userProject = new UserProject();
					userProject.setUserId(userId);
					userProject.setProjectId(projectId);
					userProject.setStatus(status);
					
					userProjects.add(userProject);
				}				
			}
			rs.close();
			statement.close();
			connection.close();
		} catch (Exception e) {
			LOGGER.error(e.toString());
		} finally {
			try {
				if (statement != null) {
					statement.close();
				}
			} catch (SQLException sqlException) {
				LOGGER.error(sqlException.toString());
			}
			try {
				if (connection != null) {
					connection.close();
				}
			} catch (SQLException sqlException) {
				LOGGER.error(sqlException.toString());
			}
		}
		return userProjects;
	}
	
	/**
	 * Gets the user start app rel.
	 *
	 * @return the user start app rel
	 */
	public List<UserStartApplication> getUserStartAppRel() {
		Connection connection = null;
		Statement statement = null;
		
		userStartApps = new ArrayList<>();

		try {
			connection = DriverManager.getConnection(OLD_DB_URL, OLD_DB_USERNAME, OLD_DB_PASSWORD);
			statement = connection.createStatement();
			String sql;
			sql = "SELECT USERNAME, INITTASK, IT_ACTIVE FROM CISADM3.USER_REL WHERE USERNAME IS NOT NULL AND INITTASK IS NOT NULL AND IT_ACTIVE IS NOT NULL";
			ResultSet rs = statement.executeQuery(sql);
			
			while (rs.next()) {
				String userId = ObjectNameIdDetails.getUserIdByName(users, rs.getString("USERNAME"));
				String startAppId = ObjectNameIdDetails.getStartAppIdByName(startApps, rs.getString("INITTASK"));
				String status = rs.getInt("IT_ACTIVE") + "";
				if(userId != null && !userId.isEmpty() && startAppId != null && !startAppId.isEmpty() && status != null && !status.isEmpty()) {
					UserStartApplication userStartApplication = new UserStartApplication();
					userStartApplication.setUserId(userId);
					userStartApplication.setStartAppId(startAppId);
					userStartApplication.setStatus(status);
					
					userStartApps.add(userStartApplication);
				}				
			}
			rs.close();
			statement.close();
			connection.close();
		} catch (Exception e) {
			LOGGER.error(e.toString());
		} finally {
			try {
				if (statement != null) {
					statement.close();
				}
			} catch (SQLException sqlException) {
				LOGGER.error(sqlException.toString());
			}
			try {
				if (connection != null) {
					connection.close();
				}
			} catch (SQLException sqlException) {
				LOGGER.error(sqlException.toString());
			}
		}
		return userStartApps;
	}
	
	
	/**
	 * Gets the directory data.
	 *
	 * @return the directory data
	 */
	public List<Directory> getDirectoryData() {
		Connection connection = null;
		Statement statement = null;

		directories = new ArrayList<>();

		try {
			connection = DriverManager.getConnection(OLD_DB_URL, OLD_DB_USERNAME, OLD_DB_PASSWORD);
			statement = connection.createStatement();
			String sql;
			sql = "SELECT * FROM CISADM3.GROUPS WHERE NAME <> 'Admins'";
			ResultSet rs = statement.executeQuery(sql);

			while (rs.next()) {
				Directory directory = new Directory();
				directory.setName(rs.getString("NAME"));
				directory.setIconName(rs.getString("ICONP"));
				LanguageCode[] languageCodes = LanguageCode.values();
				for (LanguageCode languageCode : languageCodes) {
					DirectoryTranslation directoryTranslation = new DirectoryTranslation(languageCode.toString());					
					directoryTranslation.setDescription(rs.getString("DESCRIPTION"));
					directoryTranslation.setRemarks(rs.getString("HELPTEXT"));
					directory.setDirectoryTranslation(directoryTranslation);
				}
				directories.add(directory);
			}
			rs.close();
			statement.close();
			connection.close();
		} catch (Exception e) {
			LOGGER.error(e.toString());
		} finally {
			try {
				if (statement != null) {
					statement.close();
				}
			} catch (SQLException sqlException) {
				LOGGER.error(sqlException.toString());
			}
			try {
				if (connection != null) {
					connection.close();
				}
			} catch (SQLException sqlException) {
				LOGGER.error(sqlException.toString());
			}
		}
		return directories;
	}
	
	
	/**
	 * Gets the directory object ref data.
	 *
	 * @return the directory object ref data
	 */
	public List<DirectoryObjectReference> getDirectoryObjectRefData() {
		Connection connection = null;
		Statement statement = null;

		directoryObjectRefs = new ArrayList<>();

		try {
			connection = DriverManager.getConnection(OLD_DB_URL, OLD_DB_USERNAME, OLD_DB_PASSWORD);
			statement = connection.createStatement();
			String sql;
			sql = "SELECT * FROM CISADM3.GROUPREF WHERE GROUPNAME <> 'Admins' ORDER BY GROUPNAME";
			ResultSet rs = statement.executeQuery(sql);

			while (rs.next()) {
				String type = rs.getString("OBJECTTYPE");
				String directoryId = ObjectNameIdDetails.getDirectoryIdByName(directories, rs.getString("GROUPNAME"));
				String objectId = null;
				String objectType = null;
				if("PROJECT".equals(type)) {
					objectId = ObjectNameIdDetails.getProjectIdByName(projects, rs.getString("OBJECTID"));
					objectType = DirectoryObjectType.PROJECT.name();
				} else if("USER".equals(type)) {
					objectId = ObjectNameIdDetails.getUserIdByName(users, rs.getString("OBJECTID"));
					objectType = DirectoryObjectType.USER.name();
				} else if("PROJECTTASK".equals(type)) {
					objectId = ObjectNameIdDetails.getProjectAppIdByName(projectApps, rs.getString("OBJECTID"));
					objectType = DirectoryObjectType.PROJECTAPPLICATION.name();
					if(objectId == null || objectId.isEmpty()) {
						objectId = ObjectNameIdDetails.getUserAppIdByName(userApps, rs.getString("OBJECTID"));
						objectType = DirectoryObjectType.USERAPPLICATION.name();
					}
				} 
				
				if(directoryId != null && !directoryId.isEmpty() && objectId != null && !objectId.isEmpty() && objectType != null && !objectType.isEmpty()) {
					DirectoryObjectReference directoryObjectReference = new DirectoryObjectReference();
					directoryObjectReference.setDirectoryId(directoryId);
					directoryObjectReference.setObjectId(objectId);
					directoryObjectReference.setObjectType(objectType);

					directoryObjectRefs.add(directoryObjectReference);
				}
				
			}
			rs.close();
			statement.close();
			connection.close();
		} catch (Exception e) {
			LOGGER.error(e.toString());
		} finally {
			try {
				if (statement != null) {
					statement.close();
				}
			} catch (SQLException sqlException) {
				LOGGER.error(sqlException.toString());
			}
			try {
				if (connection != null) {
					connection.close();
				}
			} catch (SQLException sqlException) {
				LOGGER.error(sqlException.toString());
			}
		}
		return directoryObjectRefs;
	}
	
	/**
	 * Gets the admin menu directory object ref data.
	 *
	 * @return the admin menu directory object ref data
	 */
	public List<DirectoryObjectReference> getAdminMenuDirectoryObjectRefData() {
		Connection connection = null;
		Statement statement = null;

		adminMenudirectoryObjectRefs = new ArrayList<>();

		try {
			connection = DriverManager.getConnection(OLD_DB_URL, OLD_DB_USERNAME, OLD_DB_PASSWORD);
			statement = connection.createStatement();
			String sql;
			sql = "SELECT * FROM CISADM3.GROUPREF WHERE GROUPNAME = 'Admins'";
			ResultSet rs = statement.executeQuery(sql);

			while (rs.next()) {
				String type = rs.getString("OBJECTTYPE");
				String objectId = null;
				String objectType = null;
				if("USER".equals(type)) {
					objectId = ObjectNameIdDetails.getUserIdByName(users, rs.getString("OBJECTID"));
					objectType = DirectoryObjectType.USER.name();
				} else if("PROJECTTASK".equals(type)) {
					objectId = ObjectNameIdDetails.getProjectAppIdByName(projectApps, rs.getString("OBJECTID"));
					objectType = DirectoryObjectType.PROJECTAPPLICATION.name();
					if(objectId == null || objectId.isEmpty()) {
						objectId = ObjectNameIdDetails.getUserAppIdByName(userApps, rs.getString("OBJECTID"));
						objectType = DirectoryObjectType.USERAPPLICATION.name();
					}
				} 
				
				if(objectId != null && !objectId.isEmpty() && objectType != null && !objectType.isEmpty()) {
					DirectoryObjectReference directoryObjectReference = new DirectoryObjectReference();
					directoryObjectReference.setObjectId(objectId);
					directoryObjectReference.setObjectType(objectType);

					adminMenudirectoryObjectRefs.add(directoryObjectReference);
				}
				
			}
			rs.close();
			statement.close();
			connection.close();
		} catch (Exception e) {
			LOGGER.error(e.toString());
		} finally {
			try {
				if (statement != null) {
					statement.close();
				}
			} catch (SQLException sqlException) {
				LOGGER.error(sqlException.toString());
			}
			try {
				if (connection != null) {
					connection.close();
				}
			} catch (SQLException sqlException) {
				LOGGER.error(sqlException.toString());
			}
		}
		return adminMenudirectoryObjectRefs;
	}
	
	/**
	 * Gets the user site AA user app rel.
	 *
	 * @return the user site AA user app rel
	 */
	public List<UserAdminAreaUserApplication> getUserSiteAAUserAppRel() {
		Connection connection = null;
		Statement statement = null;
		
		userAdminAreaUserApplications = new ArrayList<>();

		try {
			connection = DriverManager.getConnection(OLD_DB_URL, OLD_DB_USERNAME, OLD_DB_PASSWORD);
			statement = connection.createStatement();
			String sql;
			sql = "SELECT USERNAME, PROJECTTASK, PT_DENIED FROM CISADM3.USER_REL WHERE USERNAME IS NOT NULL AND PROJECT = '*UserTasks*' AND PROJECTTASK IS NOT NULL AND PT_DENIED IS NOT NULL";
			ResultSet rs = statement.executeQuery(sql);
			
			while (rs.next()) {
				String userId = ObjectNameIdDetails.getUserIdByName(users, rs.getString("USERNAME"));
				String userAppId = ObjectNameIdDetails.getUserAppIdByName(userApps, rs.getString("PROJECTTASK"));
				String relType = rs.getInt("PT_DENIED") + "";
				
				if(userId != null && !userId.isEmpty() && userAppId != null && !userAppId.isEmpty() && relType != null && !relType.isEmpty()) {
					for(SiteAdminArea siteAdminArea : siteAdminAreas) {
						String siteAdminAreaId = siteAdminArea.getId();
						if(siteAdminAreaId != null && !siteAdminAreaId.isEmpty()) {
							UserAdminAreaUserApplication userAdminAreaUserApplication = new UserAdminAreaUserApplication();
							userAdminAreaUserApplication.setUserId(userId);
							userAdminAreaUserApplication.setSiteAdminAreaId(siteAdminAreaId);
							userAdminAreaUserApplication.setUserAppId(userAppId);
							userAdminAreaUserApplication.setRelType(relType);
							
							userAdminAreaUserApplications.add(userAdminAreaUserApplication);
						}
					}
				}			
			}
			
			rs.close();
			statement.close();
			connection.close();
		} catch (Exception e) {
			LOGGER.error(e.toString());
		} finally {
			try {
				if (statement != null) {
					statement.close();
				}
			} catch (SQLException sqlException) {
				LOGGER.error(sqlException.toString());
			}
			try {
				if (connection != null) {
					connection.close();
				}
			} catch (SQLException sqlException) {
				LOGGER.error(sqlException.toString());
			}
		}
		return userAdminAreaUserApplications;
	}

	/**
	 * Gets the user site AA project project app rel.
	 *
	 * @return the user site AA project project app rel
	 */
	public List<UserAdminAreaProjectProjectApplication> getUserSiteAAProjectProjectAppRel() {
		Connection connection = null;
		Statement statement = null;
		
		userAdminAreaProjectProjectApplications = new ArrayList<>();

		try {
			connection = DriverManager.getConnection(OLD_DB_URL, OLD_DB_USERNAME, OLD_DB_PASSWORD);
			statement = connection.createStatement();
			String sql;
			sql = "SELECT USERNAME, PROJECT, PROJECTTASK, PT_DENIED FROM CISADM3.USER_REL WHERE USERNAME IS NOT NULL AND PROJECT <> '*UserTasks*' AND PROJECTTASK IS NOT NULL AND PT_DENIED IS NOT NULL";
			ResultSet rs = statement.executeQuery(sql);
			
			while (rs.next()) {
				String userId = ObjectNameIdDetails.getUserIdByName(users, rs.getString("USERNAME"));
				String projectId = ObjectNameIdDetails.getProjectIdByName(projects, rs.getString("PROJECT"));
				String projectAppId = ObjectNameIdDetails.getProjectAppIdByName(projectApps, rs.getString("PROJECTTASK"));
				String relType = rs.getInt("PT_DENIED") + "";
				
				if(userId != null && !userId.isEmpty() && projectId != null && !projectId.isEmpty() && projectAppId != null && !projectAppId.isEmpty() && relType != null && !relType.isEmpty()) {
					String userProjectId = null;
					for(UserProject userProject : userProjects) {
						if( userId.equals(userProject.getUserId()) && projectId.equals(userProject.getProjectId()) ) {
							userProjectId = userProject.getId();
						}
					}
					
					if(userProjectId != null && !userProjectId.isEmpty()) {
						for(SiteAdminAreaProject siteAdminAreaProject : siteAdminAreaProjects) {
							if( projectId.equals(siteAdminAreaProject.getProjectId()) ) {
								String siteAdminAreaProjectId = siteAdminAreaProject.getId();
								UserAdminAreaProjectProjectApplication userAdminAreaProjectProjectApplication = new UserAdminAreaProjectProjectApplication();
								userAdminAreaProjectProjectApplication.setUserProjectId(userProjectId);
								userAdminAreaProjectProjectApplication.setSiteAdminAreaProjectId(siteAdminAreaProjectId);
								userAdminAreaProjectProjectApplication.setProjectAppId(projectAppId);
								userAdminAreaProjectProjectApplication.setRelType(relType);
								
								userAdminAreaProjectProjectApplications.add(userAdminAreaProjectProjectApplication);
							}
						}
					}					
				}			
			}
			
			rs.close();
			statement.close();
			connection.close();
		} catch (Exception e) {
			LOGGER.error(e.toString());
		} finally {
			try {
				if (statement != null) {
					statement.close();
				}
			} catch (SQLException sqlException) {
				LOGGER.error(sqlException.toString());
			}
			try {
				if (connection != null) {
					connection.close();
				}
			} catch (SQLException sqlException) {
				LOGGER.error(sqlException.toString());
			}
		}
		return userAdminAreaProjectProjectApplications;
	}
	
	/**
	 * Method for Load translation data.
	 */
	public void loadTranslationData() {
		Connection connection = null;
		Statement statement = null;

		translations= new HashMap<>();
		
		Map<String,Map<LanguageCode, String>> nameTranslations= new HashMap<>();
		Map<String,Map<LanguageCode, String>> descTranslations= new HashMap<>();

		try {
			connection = DriverManager.getConnection(OLD_DB_URL, OLD_DB_USERNAME, OLD_DB_PASSWORD);
			statement = connection.createStatement();
			String sql;
			sql = "SELECT TRSL.PROJECTTASKS, TRSL.NAME, TRSL.DESCRIPTION, TRSL.LANGUAGE FROM CISADM3.TRANSLATION TRSL WHERE TRSL.PROJECTTASKS IS NOT NULL AND TRSL.LANGUAGE IN ('german', 'english') ORDER BY TRSL.PROJECTTASKS";
			ResultSet rs = statement.executeQuery(sql);

			while (rs.next()) {
				String appName = rs.getString("PROJECTTASKS");
				String name = rs.getString("NAME");
				String description = rs.getString("DESCRIPTION");
				String language = rs.getString("LANGUAGE");
				
				Map<LanguageCode, String> nameTranslationMap = nameTranslations.get(appName);
				if(nameTranslationMap == null) {
					nameTranslationMap = new HashMap<>();
					if("english".equals(language)) {
						nameTranslationMap.put(LanguageCode.en, name);						
					} else if("german".equals(language)) {
						nameTranslationMap.put(LanguageCode.de, name);
					}
					nameTranslations.put(appName, nameTranslationMap);
				} else {
					if("english".equals(language)) {
						nameTranslationMap.put(LanguageCode.en, name);
					} else if("german".equals(language)) {
						nameTranslationMap.put(LanguageCode.de, name);
					}
				}
				
				Map<LanguageCode, String> descTranslationMap = descTranslations.get(appName);
				if(descTranslationMap == null) {
					descTranslationMap = new HashMap<>();
					if("english".equals(language)) {
						descTranslationMap.put(LanguageCode.en, description);						
					} else if("german".equals(language)) {
						descTranslationMap.put(LanguageCode.de, description);
					}
					descTranslations.put(appName, descTranslationMap);
				} else {
					if("english".equals(language)) {
						descTranslationMap.put(LanguageCode.en, description);
					} else if("german".equals(language)) {
						descTranslationMap.put(LanguageCode.de, description);
					}
				}				
			}
			rs.close();
			statement.close();
			connection.close();
		} catch (Exception e) {
			LOGGER.error(e.toString());
		} finally {
			try {
				if (statement != null) {
					statement.close();
				}
			} catch (SQLException sqlException) {
				LOGGER.error(sqlException.toString());
			}
			try {
				if (connection != null) {
					connection.close();
				}
			} catch (SQLException sqlException) {
				LOGGER.error(sqlException.toString());
			}
		}
		
		translations.put(TranslationKey.NAME, nameTranslations);
		translations.put(TranslationKey.DESC, descTranslations);
	}
	
	/**
	 * Gets the user status.
	 *
	 * @return the user status
	 */
	public List<UserHistory> getUserStatus() {
		Connection connection = null;
		Statement statement = null;

		List<UserHistory> userHistoryList = new ArrayList<>();

		try {
			connection = DriverManager.getConnection(OLD_DB_URL, OLD_DB_USERNAME, OLD_DB_PASSWORD);
			statement = connection.createStatement();
			String sql = "SELECT * FROM CISADM3.USER_STATUS WHERE USETIME > '01-JUN-16' ORDER BY ID";
			ResultSet rs = statement.executeQuery(sql);

			while (rs.next()) {
				UserHistory userHistory = new UserHistory();
				userHistory.setLogTime(rs.getDate("USETIME"));
				userHistory.setSite(rs.getString("SITE"));
				userHistory.setProject(rs.getString("PROJECT"));
				userHistory.setUsername(rs.getString("USERNAME"));
				userHistory.setHost(rs.getString("HOST"));
				userHistory.setApplication(rs.getString("TASK"));
				userHistory.setPid(rs.getInt("PID"));
				userHistory.setResult(rs.getString("RESULT"));
				userHistory.setArgs(rs.getString("ARGS"));
				userHistory.setUserStatus(true);

				userHistoryList.add(userHistory);
			}
			rs.close();
			statement.close();
			connection.close();
		} catch (Exception e) {
			LOGGER.error(e.toString());
		} finally {
			try {
				if (statement != null) {
					statement.close();
				}
			} catch (SQLException sqlException) {
				LOGGER.error(sqlException.toString());
			}
			try {
				if (connection != null) {
					connection.close();
				}
			} catch (SQLException sqlException) {
				LOGGER.error(sqlException.toString());
			}
		}
		return userHistoryList;
	}
	
	/**
	 * Gets the user history.
	 *
	 * @return the user history
	 */
	public List<UserHistory> getUserHistory() {
		Connection connection = null;
		Statement statement = null;

		List<UserHistory> userHistoryList = new ArrayList<>();

		try {
			connection = DriverManager.getConnection(OLD_DB_URL, OLD_DB_USERNAME, OLD_DB_PASSWORD);
			statement = connection.createStatement();
			String sql = "SELECT * FROM CISADM3.USERHISTORY WHERE USETIME > '01-JUN-16' ORDER BY ID";
			ResultSet rs = statement.executeQuery(sql);

			while (rs.next()) {
				UserHistory userHistory = new UserHistory();
				userHistory.setLogTime(rs.getDate("USETIME"));
				userHistory.setSite(rs.getString("SITE"));
				userHistory.setProject(rs.getString("PROJECT"));
				userHistory.setUsername(rs.getString("USERNAME"));
				userHistory.setHost(rs.getString("HOST"));
				userHistory.setApplication(rs.getString("TASK"));
				userHistory.setPid(rs.getInt("PID"));
				userHistory.setResult(rs.getString("RESULT"));
				userHistory.setArgs(rs.getString("ARGS"));
				userHistory.setUserStatus(false);

				userHistoryList.add(userHistory);
			}
			rs.close();
			statement.close();
			connection.close();
		} catch (Exception e) {
			LOGGER.error(e.toString());
		} finally {
			try {
				if (statement != null) {
					statement.close();
				}
			} catch (SQLException sqlException) {
				LOGGER.error(sqlException.toString());
			}
			try {
				if (connection != null) {
					connection.close();
				}
			} catch (SQLException sqlException) {
				LOGGER.error(sqlException.toString());
			}
		}
		return userHistoryList;
	}
	
	/**
	 * Gets the supported platforms.
	 *
	 * @param program {@link String}
	 * @return the supported platforms
	 */
	private String getSupportedPlatforms(String program) {
		/*if(program != null && !program.isEmpty()) {
			if(program.contains(".exe") || program.contains(".bat")) {
				return PLATFORMS.WINDOWS64.name() + ";" + PLATFORMS.WINDOWS32.name();
			} else if(program.contains(".ksh") || program.contains(".sh")) {
				return PLATFORMS.LINUX64.name();
			} else {
				return PLATFORMS.WINDOWS64.name() + ";" + PLATFORMS.WINDOWS32.name() + ";" + PLATFORMS.LINUX64.name();
			}
		}
		return null;*/
		
		return PLATFORMS.WINDOWS64.name() + ";" + PLATFORMS.WINDOWS32.name() + ";" + PLATFORMS.LINUX64.name();
	}
}