package com.magna.caxsystem.datamigration.util;

/**
 * Class for Migration constents.
 *
 * @author Chiranjeevi.Akula
 */
public class MigrationConstents {

	/** Member variable 'migration config' for {@link String}. */
	public static String MIGRATION_CONFIG = "caxsystemMigration.properties";
	
	/**
	 * Class for Icons.
	 *
	 * @author Chiranjeevi.Akula
	 */
	public static class Icons {

		/** Member variable 'icons path' for {@link String}. *//*
		public static String ICONS_PATH = "D:/Magna/DataMigration/icons";*/
		
		/** Member variable 'site' for {@link String}. */
		public static String SITE = "sites.png";

		/** Member variable 'admin area' for {@link String}. */
		public static String ADMIN_AREA = "administrationarea.png";

		/** Member variable 'project' for {@link String}. */
		public static String PROJECT = "projects.png";

		/** Member variable 'base application' for {@link String}. */
		public static String BASE_APPLICATION = "basictasks.png";

		/** Member variable 'user' for {@link String}. */
		public static String USER = "users.png";

		/** Member variable 'user application' for {@link String}. */
		public static String USER_APPLICATION = "ausertasks.png";

		/** Member variable 'project application' for {@link String}. */
		public static String PROJECT_APPLICATION = "aprojecttasks.png";

		/** Member variable 'start application' for {@link String}. */
		public static String START_APPLICATION = "starttasks.png";

		/** Member variable 'directory' for {@link String}. */
		public static String DIRECTORY = "directory.png";
	}
	
	/**
	 * Class for Excels.
	 *
	 * @author Chiranjeevi.Akula
	 */
	public static class Excels {
		
		/** Member variable 'excel path' for {@link String}. *//*
		public static String EXCEL_PATH = "D:/Magna/DataMigration/Client_Update/DataMigartionExcels/";*/
		
		/** Member variable 'site adminarea mapping excel' for {@link String}. */
		public static String SITE_ADMINAREA_MAPPING_EXCEL = "Site_AdminArea_Mapping.xlsx";
		
		/** Member variable 'site adminarea project mapping excel' for {@link String}. */
		public static String SITE_ADMINAREA_PROJECT_MAPPING_EXCEL = "Site_AdminArea_Project_Mapping.xlsx";
		
		/** Member variable 'site adminarea userapp mapping excel' for {@link String}. */
		public static String SITE_ADMINAREA_USERAPP_MAPPING_EXCEL = "Site_AdminArea_UserApplication_Mapping.xlsx";
		
		/** Member variable 'site adminarea startapp mapping excel' for {@link String}. */
		public static String SITE_ADMINAREA_STARTAPP_MAPPING_EXCEL = "Site_AdminArea_StartApplication_Mapping.xlsx";	
		
		/** Member variable 'site adminarea project projectapp mapping excel' for {@link String}. */
		public static String SITE_ADMINAREA_PROJECT_PROJECTAPP_MAPPING_EXCEL = "Site_AdminArea_Project_ProjectApplication_Mapping.xlsx";
		
		/** Member variable 'site adminarea project startapp mapping excel' for {@link String}. */
		public static String SITE_ADMINAREA_PROJECT_STARTAPP_MAPPING_EXCEL = "Site_AdminArea_Project_StartApplication_Mapping.xlsx";
	}
}
