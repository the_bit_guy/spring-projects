package com.magna.caxsystem.datamigration.vo.relations;

import com.magna.caxsystem.datamigration.enums.Status;
import com.magna.caxsystem.datamigration.util.GenerateUuid;

/**
 * Class for User project.
 *
 * @author Chiranjeevi.Akula
 */
public class UserProject {

	/** Member variable 'id' for {@link String}. */
	private String id;

	/** Member variable 'user id' for {@link String}. */
	private String userId;

	/** Member variable 'project id' for {@link String}. */
	private String projectId;

	/** Member variable 'status' for {@link String}. */
	private String status;

	/**
	 * Constructor for UserProject Class.
	 */
	public UserProject() {
		this.id = GenerateUuid.getUuid();
	}

	/**
	 * Constructor for UserProject Class.
	 *
	 * @param userId
	 *            {@link String}
	 * @param projectId
	 *            {@link String}
	 * @param status
	 *            {@link String}
	 */
	public UserProject(String userId, String projectId, String status) {
		this.id = GenerateUuid.getUuid();
		this.userId = userId;
		this.projectId = projectId;
		this.status = status;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id
	 *            the new id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Gets the user id.
	 *
	 * @return the user id
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * Sets the user id.
	 *
	 * @param userId
	 *            the new user id
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * Gets the project id.
	 *
	 * @return the project id
	 */
	public String getProjectId() {
		return projectId;
	}

	/**
	 * Sets the project id.
	 *
	 * @param projectId
	 *            the new project id
	 */
	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 *
	 * @param statusCode
	 *            the new status
	 */
	public void setStatus(String statusCode) {
		if ("1".equals(statusCode)) {
			this.status = Status.ACTIVE.toString();
		} else {
			this.status = Status.INACTIVE.toString();
		}
	}
}
