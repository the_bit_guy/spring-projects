package com.magna.caxsystem.datamigration.jdbc.newdb;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magna.caxsystem.datamigration.util.GenerateUuid;
import com.magna.caxsystem.datamigration.util.MigrationConstents;
import com.magna.caxsystem.datamigration.util.MigrationUtil;
import com.magna.caxsystem.datamigration.vo.adminarea.AdminArea;
import com.magna.caxsystem.datamigration.vo.adminarea.AdminAreaTranslation;
import com.magna.caxsystem.datamigration.vo.baseapplication.BaseAppTranslation;
import com.magna.caxsystem.datamigration.vo.baseapplication.BaseApplication;
import com.magna.caxsystem.datamigration.vo.directory.Directory;
import com.magna.caxsystem.datamigration.vo.directory.DirectoryObjectReference;
import com.magna.caxsystem.datamigration.vo.directory.DirectoryTranslation;
import com.magna.caxsystem.datamigration.vo.project.Project;
import com.magna.caxsystem.datamigration.vo.project.ProjectTranslation;
import com.magna.caxsystem.datamigration.vo.projectapplication.ProjectApp;
import com.magna.caxsystem.datamigration.vo.projectapplication.ProjectAppTranslation;
import com.magna.caxsystem.datamigration.vo.relations.SiteAdminArea;
import com.magna.caxsystem.datamigration.vo.relations.SiteAdminAreaProject;
import com.magna.caxsystem.datamigration.vo.relations.SiteAdminAreaProjectProjectApplication;
import com.magna.caxsystem.datamigration.vo.relations.SiteAdminAreaProjectStartApplication;
import com.magna.caxsystem.datamigration.vo.relations.SiteAdminAreaStartApplication;
import com.magna.caxsystem.datamigration.vo.relations.SiteAdminAreaUserApplication;
import com.magna.caxsystem.datamigration.vo.relations.UserAdminAreaProjectProjectApplication;
import com.magna.caxsystem.datamigration.vo.relations.UserAdminAreaUserApplication;
import com.magna.caxsystem.datamigration.vo.relations.UserProject;
import com.magna.caxsystem.datamigration.vo.relations.UserStartApplication;
import com.magna.caxsystem.datamigration.vo.site.Site;
import com.magna.caxsystem.datamigration.vo.site.SiteTranslation;
import com.magna.caxsystem.datamigration.vo.startapplication.StartApp;
import com.magna.caxsystem.datamigration.vo.startapplication.StartAppTranslation;
import com.magna.caxsystem.datamigration.vo.user.User;
import com.magna.caxsystem.datamigration.vo.user.UserTranslation;
import com.magna.caxsystem.datamigration.vo.userapplication.UserApp;
import com.magna.caxsystem.datamigration.vo.userapplication.UserAppTranslation;
import com.magna.caxsystem.datamigration.vo.userhistory.UserHistory;

/**
 * Class for New database.
 *
 * @author Chiranjeevi.Akula
 */
public class NewDatabase {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(NewDatabase.class);
	
	/** Member variable 'new db url' for {@link String}. */
	private String NEW_DB_URL;
	
	/** Member variable 'new db username' for {@link String}. */
	private String NEW_DB_USERNAME;
	
	/** Member variable 'new db password' for {@link String}. */
	private String NEW_DB_PASSWORD;
	
	/** Member variable 'property config map' for {@link Map<String,String>}. */
	private Map<String, String> propertyConfigMap = new HashMap<>();

	/**
	 * Constructor for NewDatabase Class.
	 *
	 * @param NEW_DB_URL {@link String}
	 * @param NEW_DB_USERNAME {@link String}
	 * @param NEW_DB_PASSWORD {@link String}
	 */
	public NewDatabase(final String NEW_DB_URL, final String NEW_DB_USERNAME, final String NEW_DB_PASSWORD) {
		this.NEW_DB_URL = NEW_DB_URL;
		this.NEW_DB_USERNAME = NEW_DB_USERNAME;
		this.NEW_DB_PASSWORD = NEW_DB_PASSWORD;
	}
	
	/**
	 * Method for Load property config.
	 */
	public void loadPropertyConfig() {
		Connection connection = null;
		Statement statement = null;
		String selectSql = "SELECT PROPERTY, VALUE FROM PROPERTY_CONFIG_TBL";

		try {
			connection = DriverManager.getConnection(NEW_DB_URL, NEW_DB_USERNAME, NEW_DB_PASSWORD);
			statement = connection.createStatement();
			ResultSet rs = statement.executeQuery(selectSql);
			while (rs.next()) {
				String property = rs.getString("PROPERTY");
				String value = rs.getString("VALUE");
				if(property != null && !property.isEmpty() && value != null && !value.isEmpty()) {
					propertyConfigMap.put(property, value);
				}
			}
			rs.close();
			statement.close();
			connection.close();
		} catch (Exception e) {
			LOGGER.error(e.toString());
		} finally {
			try {
				if (statement != null) {
					statement.close();
				}
			} catch (SQLException sqlException) {
				LOGGER.error(sqlException.toString());
			}
			try {
				if (connection != null) {
					connection.close();
				}
			} catch (SQLException sqlException) {
				LOGGER.error(sqlException.toString());
			}
		}
	}

	/**
	 * Method for Insert custom icons.
	 */
	public void insertCustomIcons() {
		Connection connection = null;
		PreparedStatement preStmt = null;
		String insertSql = "INSERT INTO ICONS_TBL (ICON_ID, ICON_NAME, ICON_TYPE) VALUES (?, ?, ?)";

		try {
			connection = DriverManager.getConnection(NEW_DB_URL, NEW_DB_USERNAME, NEW_DB_PASSWORD);
			File folder = new File(MigrationUtil.getInstance().getIconsPath());
			if (folder.exists() && folder.isDirectory()) {
				File[] listOfFiles = folder.listFiles();

				for (File file : listOfFiles) {
					if (file.isFile()) {
						String iconName = file.getName();
						String selectSql = "SELECT ICON_ID FROM ICONS_TBL WHERE ICON_NAME = '" + iconName + "'";

						if (!isDataExists(connection, selectSql)) {
							preStmt = connection.prepareStatement(insertSql);
							preStmt.setString(1, GenerateUuid.getUuid());
							preStmt.setString(2, iconName);
							preStmt.setString(3, "CUSTOM");

							preStmt.executeUpdate();
							preStmt.close();
						}
					}
				}
			}

			connection.commit();
			connection.close();
		} catch (Exception e) {
			LOGGER.error(e.toString());
		} finally {
			try {
				if (preStmt != null) {
					preStmt.close();
				}
			} catch (SQLException sqlException) {
				LOGGER.error(sqlException.toString());
			}
			try {
				if (connection != null) {
					connection.close();
				}
			} catch (SQLException sqlException) {
				LOGGER.error(sqlException.toString());
			}
		}
	}
	
	/**
	 * Method for Insert site data.
	 *
	 * @param sites {@link List<Site>}
	 */
	public void insertSiteData(final List<Site> sites) {
		Connection connection = null;
		PreparedStatement preStmt = null;
		PreparedStatement preStmtTranslation = null;
		String selectSql = "SELECT * FROM SITES_TBL";
		String insertSql = "INSERT INTO SITES_TBL (SITE_ID, NAME, STATUS, ICON_ID, CREATE_DATE, UPDATE_DATE) VALUES (?, ?, ?, ?, SYSTIMESTAMP, SYSTIMESTAMP)";
		String insertSqlTranslation = "INSERT INTO SITE_TRANSLATION_TBL (SITE_TRANSLATION_ID, SITE_ID, DESCRIPTION, REMARKS, LANGUAGE_CODE, CREATE_DATE, UPDATE_DATE) VALUES (?, ?, ?, ?, ?, SYSTIMESTAMP, SYSTIMESTAMP)";

		try {
			connection = DriverManager.getConnection(NEW_DB_URL, NEW_DB_USERNAME, NEW_DB_PASSWORD);
			if (!isDataExists(connection, selectSql)) {
				for (Site site : sites) {
					preStmt = connection.prepareStatement(insertSql);
					preStmt.setString(1, site.getId());
					preStmt.setString(2, site.getName());
					preStmt.setString(3, site.getStatus());
					preStmt.setString(4, getIconId(connection, MigrationConstents.Icons.SITE, site.getIconName()));

					int flag = preStmt.executeUpdate();
					preStmt.close();

					if (flag > 0) {
						List<SiteTranslation> siteTranslations = site.getSiteTranslations();
						for (SiteTranslation siteTranslation : siteTranslations) {
							preStmtTranslation = connection.prepareStatement(insertSqlTranslation);
							preStmtTranslation.setString(1, siteTranslation.getId());
							preStmtTranslation.setString(2, site.getId());
							preStmtTranslation.setString(3, siteTranslation.getDescription());
							preStmtTranslation.setString(4, siteTranslation.getRemarks());
							preStmtTranslation.setString(5, siteTranslation.getLanguageCode());
							preStmtTranslation.executeUpdate();
							preStmtTranslation.close();
						}
					}
				}
			}
			connection.commit();
			connection.close();
		} catch (Exception e) {
			LOGGER.error(e.toString());
		} finally {
			try {
				if (preStmt != null) {
					preStmt.close();
				}
				if (preStmtTranslation != null) {
					preStmtTranslation.close();
				}
			} catch (SQLException sqlException) {
				LOGGER.error(sqlException.toString());
			}
			try {
				if (connection != null) {
					connection.close();
				}
			} catch (SQLException sqlException) {
				LOGGER.error(sqlException.toString());
			}
		}
	}

	/**
	 * Method for Insert project data.
	 *
	 * @param projects {@link List<Project>}
	 */
	public void insertProjectData(final List<Project> projects) {
		Connection connection = null;
		PreparedStatement preStmt = null;
		PreparedStatement preStmtTranslation = null;
		String selectSql = "SELECT * FROM PROJECTS_TBL";
		String insertSql = "INSERT INTO PROJECTS_TBL (PROJECT_ID, NAME, STATUS, ICON_ID, CREATE_DATE, UPDATE_DATE) VALUES (?, ?, ?, ?, SYSTIMESTAMP, SYSTIMESTAMP)";
		String insertSqlTranslation = "INSERT INTO PROJECT_TRANSLATION_TBL (PROJECT_TRANSLATION_ID, PROJECT_ID, DESCRIPTION, REMARKS, LANGUAGE_CODE, CREATE_DATE, UPDATE_DATE) VALUES (?, ?, ?, ?, ?, SYSTIMESTAMP, SYSTIMESTAMP)";

		try {
			connection = DriverManager.getConnection(NEW_DB_URL, NEW_DB_USERNAME, NEW_DB_PASSWORD);
			if (!isDataExists(connection, selectSql)) {
				for (Project project : projects) {
					preStmt = connection.prepareStatement(insertSql);
					preStmt.setString(1, project.getId());
					preStmt.setString(2, project.getName());
					preStmt.setString(3, project.getStatus());
					preStmt.setString(4, getIconId(connection, MigrationConstents.Icons.PROJECT, project.getIconName()));

					int flag = preStmt.executeUpdate();
					preStmt.close();

					if (flag > 0) {
						List<ProjectTranslation> projectTranslations = project.getProjectTranslations();
						for (ProjectTranslation projectTranslation : projectTranslations) {
							preStmtTranslation = connection.prepareStatement(insertSqlTranslation);
							preStmtTranslation.setString(1, projectTranslation.getId());
							preStmtTranslation.setString(2, project.getId());
							preStmtTranslation.setString(3, projectTranslation.getDescription());
							preStmtTranslation.setString(4, projectTranslation.getRemarks());
							preStmtTranslation.setString(5, projectTranslation.getLanguageCode());
							preStmtTranslation.executeUpdate();
							preStmtTranslation.close();
						}
					}
				}
			}
			connection.commit();
			connection.close();
		} catch (Exception e) {
			LOGGER.error(e.toString());
		} finally {
			try {
				if (preStmt != null) {
					preStmt.close();
				}
				if (preStmtTranslation != null) {
					preStmtTranslation.close();
				}
			} catch (SQLException sqlException) {
				LOGGER.error(sqlException.toString());
			}
			try {
				if (connection != null) {
					connection.close();
				}
			} catch (SQLException sqlException) {
				LOGGER.error(sqlException.toString());
			}
		}
	}

	/**
	 * Method for Insert base app data.
	 *
	 * @param baseApplications {@link List<BaseApplication>}
	 */
	public void insertBaseAppData(final List<BaseApplication> baseApplications) {
		Connection connection = null;
		PreparedStatement preStmt = null;
		PreparedStatement preStmtTranslation = null;
		String selectSql = "SELECT * FROM BASE_APPLICATIONS_TBL";
		String insertSql = "INSERT INTO BASE_APPLICATIONS_TBL (BASE_APPLICATION_ID, NAME, STATUS, PLATFORMS, PROGRAM, ICON_ID, CREATE_DATE, UPDATE_DATE) VALUES (?, ?, ?, ?, ?, ?, SYSTIMESTAMP, SYSTIMESTAMP)";
		String insertSqlTranslation = "INSERT INTO BASE_APP_TRANSLATION_TBL (BASE_APP_TRANSLATION_ID, BASE_APPLICATION_ID, DESCRIPTION, REMARKS, LANGUAGE_CODE, CREATE_DATE, UPDATE_DATE) VALUES (?, ?, ?, ?, ?, SYSTIMESTAMP, SYSTIMESTAMP)";

		try {
			connection = DriverManager.getConnection(NEW_DB_URL, NEW_DB_USERNAME, NEW_DB_PASSWORD);
			if (!isDataExists(connection, selectSql)) {
				for (BaseApplication baseApplication : baseApplications) {
					preStmt = connection.prepareStatement(insertSql);
					preStmt.setString(1, baseApplication.getId());
					preStmt.setString(2, baseApplication.getName());
					preStmt.setString(3, baseApplication.getStatus());
					preStmt.setString(4, baseApplication.getPlatforms());
					preStmt.setString(5, baseApplication.getProgram());
					preStmt.setString(6, getIconId(connection, MigrationConstents.Icons.BASE_APPLICATION, baseApplication.getIconName()));

					int flag = preStmt.executeUpdate();
					preStmt.close();

					if (flag > 0) {
						List<BaseAppTranslation> baseAppTranslations = baseApplication.getBaseAppTranslations();
						for (BaseAppTranslation baseAppTranslation : baseAppTranslations) {
							preStmtTranslation = connection.prepareStatement(insertSqlTranslation);
							preStmtTranslation.setString(1, baseAppTranslation.getId());
							preStmtTranslation.setString(2, baseApplication.getId());							
							preStmtTranslation.setString(3, baseAppTranslation.getDescription());
							preStmtTranslation.setString(4, baseAppTranslation.getRemarks());
							preStmtTranslation.setString(5, baseAppTranslation.getLanguageCode());
							preStmtTranslation.executeUpdate();
							preStmtTranslation.close();
						}
					}
				}
			}
			connection.commit();
			connection.close();
		} catch (Exception e) {
			LOGGER.error(e.toString());
		} finally {
			try {
				if (preStmt != null) {
					preStmt.close();
				}
				if (preStmtTranslation != null) {
					preStmtTranslation.close();
				}
			} catch (SQLException sqlException) {
				LOGGER.error(sqlException.toString());
			}
			try {
				if (connection != null) {
					connection.close();
				}
			} catch (SQLException sqlException) {
				LOGGER.error(sqlException.toString());
			}
		}
	}

	/**
	 * Method for Insert user data.
	 *
	 * @param users {@link List<User>}
	 */
	public void insertUserData(final List<User> users) {
		Connection connection = null;
		PreparedStatement preStmt = null;
		PreparedStatement preStmtTranslation = null;
		String selectSql = "SELECT * FROM USERS_TBL";
		String insertSql = "INSERT INTO USERS_TBL (USER_ID, USERNAME, FULL_NAME, EMAIL_ID, TELEPHONE_NUMBER, DEPARTMENT, STATUS, ICON_ID, CREATE_DATE, UPDATE_DATE) VALUES (?, ?, ?, ?, ?, ?, ?, ?, SYSTIMESTAMP, SYSTIMESTAMP)";
		String insertSqlTranslation = "INSERT INTO USER_TRANSLATION_TBL (USER_TRANSLATION_ID, USER_ID, DESCRIPTION, REMARKS, LANGUAGE_CODE, CREATE_DATE, UPDATE_DATE) VALUES (?, ?, ?, ?, ?, SYSTIMESTAMP, SYSTIMESTAMP)";

		try {
			connection = DriverManager.getConnection(NEW_DB_URL, NEW_DB_USERNAME, NEW_DB_PASSWORD);
			if (!isDataExists(connection, selectSql)) {
				for (User user : users) {
					preStmt = connection.prepareStatement(insertSql);
					preStmt.setString(1, user.getId());
					preStmt.setString(2, user.getUserName());
					preStmt.setString(3, user.getFullName());
					preStmt.setString(4, user.getEmail());
					preStmt.setString(5, user.getTelephone());
					preStmt.setString(6, user.getDepartment());
					preStmt.setString(7, user.getStatus());
					preStmt.setString(8, getIconId(connection, MigrationConstents.Icons.USER, user.getIconName()));

					int flag = preStmt.executeUpdate();
					preStmt.close();

					if (flag > 0) {
						List<UserTranslation> userTranslations = user.getUserTranslations();
						for (UserTranslation userTranslation : userTranslations) {
							preStmtTranslation = connection.prepareStatement(insertSqlTranslation);
							preStmtTranslation.setString(1, userTranslation.getId());
							preStmtTranslation.setString(2, user.getId());
							preStmtTranslation.setString(3, userTranslation.getDescription());
							preStmtTranslation.setString(4, userTranslation.getRemarks());
							preStmtTranslation.setString(5, userTranslation.getLanguageCode());
							preStmtTranslation.executeUpdate();
							preStmtTranslation.close();
						}
					}
				}
			}
			connection.commit();
			connection.close();
		} catch (Exception e) {
			LOGGER.error(e.toString());
		} finally {
			try {
				if (preStmt != null) {
					preStmt.close();
				}
				if (preStmtTranslation != null) {
					preStmtTranslation.close();
				}
			} catch (SQLException sqlException) {
				LOGGER.error(sqlException.toString());
			}
			try {
				if (connection != null) {
					connection.close();
				}
			} catch (SQLException sqlException) {
				LOGGER.error(sqlException.toString());
			}
		}
	}

	/**
	 * Method for Insert user application data.
	 *
	 * @param userApplications {@link List<UserApp>}
	 */
	public void insertUserApplicationData(final List<UserApp> userApplications) {
		Connection connection = null;
		PreparedStatement preStmt = null;
		PreparedStatement preStmtTranslation = null;
		String selectSql = "SELECT * FROM USER_APPLICATIONS_TBL";
		String insertSql = "INSERT INTO USER_APPLICATIONS_TBL (USER_APPLICATION_ID, BASE_APPLICATION_ID, NAME, DESCRIPTION, STATUS, ICON_ID, IS_PARENT, IS_SINGLETON, POSITION, CREATE_DATE, UPDATE_DATE) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, SYSTIMESTAMP, SYSTIMESTAMP)";
		String insertSqlTranslation = "INSERT INTO USER_APP_TRANSLATION_TBL (USER_APP_TRANSLATION_ID, USER_APPLICATION_ID, NAME, DESCRIPTION, REMARKS, LANGUAGE_CODE, CREATE_DATE, UPDATE_DATE) VALUES (?, ?, ?, ?, ?, ?, SYSTIMESTAMP, SYSTIMESTAMP)";

		try {
			connection = DriverManager.getConnection(NEW_DB_URL, NEW_DB_USERNAME, NEW_DB_PASSWORD);
			if (!isDataExists(connection, selectSql)) {
				for (UserApp userApplication : userApplications) {
					preStmt = connection.prepareStatement(insertSql);
					preStmt.setString(1, userApplication.getId());
					preStmt.setString(2, getBaseApplicationId(connection, userApplication.getBaseAppName()));
					preStmt.setString(3, userApplication.getName());
					preStmt.setString(4, userApplication.getDescription());
					preStmt.setString(5, userApplication.getStatus());
					preStmt.setString(6, getIconId(connection, MigrationConstents.Icons.USER_APPLICATION, userApplication.getIconName()));
					preStmt.setString(7, userApplication.getIsParent());
					preStmt.setString(8, userApplication.getIsSingleton());
					preStmt.setString(9, userApplication.getPosition());

					int flag = preStmt.executeUpdate();
					preStmt.close();

					if (flag > 0) {
						List<UserAppTranslation> userAppTranslations = userApplication.getUserAppTranslations();
						for (UserAppTranslation userAppTranslation : userAppTranslations) {
							preStmtTranslation = connection.prepareStatement(insertSqlTranslation);
							preStmtTranslation.setString(1, userAppTranslation.getId());
							preStmtTranslation.setString(2, userApplication.getId());
							preStmtTranslation.setString(3, userAppTranslation.getName());
							preStmtTranslation.setString(4, userAppTranslation.getDescription());
							preStmtTranslation.setString(5, userAppTranslation.getRemarks());
							preStmtTranslation.setString(6, userAppTranslation.getLanguageCode());
							preStmtTranslation.executeUpdate();
							preStmtTranslation.close();
						}
					}
				}
				updatePositionNameWithId(connection, "USER_APPLICATIONS_TBL");
			}
			connection.commit();
			connection.close();
		} catch (Exception e) {
			LOGGER.error(e.toString());
		} finally {
			try {
				if (preStmt != null) {
					preStmt.close();
				}
				if (preStmtTranslation != null) {
					preStmtTranslation.close();
				}
			} catch (SQLException sqlException) {
				LOGGER.error(sqlException.toString());
			}
			try {
				if (connection != null) {
					connection.close();
				}
			} catch (SQLException sqlException) {
				LOGGER.error(sqlException.toString());
			}
		}
	}

	/**
	 * Method for Insert project application data.
	 *
	 * @param projectApplications {@link List<ProjectApp>}
	 */
	public void insertProjectApplicationData(final List<ProjectApp> projectApplications) {
		Connection connection = null;
		PreparedStatement preStmt = null;
		PreparedStatement preStmtTranslation = null;
		String selectSql = "SELECT * FROM PROJECT_APPLICATIONS_TBL";
		String insertSql = "INSERT INTO PROJECT_APPLICATIONS_TBL (PROJECT_APPLICATION_ID, BASE_APPLICATION_ID, NAME, DESCRIPTION, STATUS, ICON_ID, IS_PARENT, IS_SINGLETON, POSITION, CREATE_DATE, UPDATE_DATE) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, SYSTIMESTAMP, SYSTIMESTAMP)";
		String insertSqlTranslation = "INSERT INTO PROJECT_APP_TRANSLATION_TBL (PROJECT_APP_TRANSLATION_ID, PROJECT_APPLICATION_ID, NAME, DESCRIPTION, REMARKS, LANGUAGE_CODE, CREATE_DATE, UPDATE_DATE) VALUES (?, ?, ?, ?, ?, ?, SYSTIMESTAMP, SYSTIMESTAMP)";

		try {
			connection = DriverManager.getConnection(NEW_DB_URL, NEW_DB_USERNAME, NEW_DB_PASSWORD);
			if (!isDataExists(connection, selectSql)) {
				for (ProjectApp projectApplication : projectApplications) {
					preStmt = connection.prepareStatement(insertSql);
					preStmt.setString(1, projectApplication.getId());
					preStmt.setString(2, getBaseApplicationId(connection, projectApplication.getBaseAppName()));
					preStmt.setString(3, projectApplication.getName());
					preStmt.setString(4, projectApplication.getDescription());
					preStmt.setString(5, projectApplication.getStatus());
					preStmt.setString(6, getIconId(connection, MigrationConstents.Icons.PROJECT_APPLICATION, projectApplication.getIconName()));
					preStmt.setString(7, projectApplication.getIsParent());
					preStmt.setString(8, projectApplication.getIsSingleton());
					preStmt.setString(9, projectApplication.getPosition());

					int flag = preStmt.executeUpdate();
					preStmt.close();

					if (flag > 0) {
						List<ProjectAppTranslation> projectAppTranslations = projectApplication.getProjectAppTranslations();
						for (ProjectAppTranslation projectAppTranslation : projectAppTranslations) {
							preStmtTranslation = connection.prepareStatement(insertSqlTranslation);
							preStmtTranslation.setString(1, projectAppTranslation.getId());
							preStmtTranslation.setString(2, projectApplication.getId());
							preStmtTranslation.setString(3, projectAppTranslation.getName());
							preStmtTranslation.setString(4, projectAppTranslation.getDescription());
							preStmtTranslation.setString(5, projectAppTranslation.getRemarks());
							preStmtTranslation.setString(6, projectAppTranslation.getLanguageCode());
							preStmtTranslation.executeUpdate();
							preStmtTranslation.close();
						}
					}
				}
				updatePositionNameWithId(connection, "PROJECT_APPLICATIONS_TBL");
			}
			connection.commit();
			connection.close();
		} catch (Exception e) {
			LOGGER.error(e.toString());
		} finally {
			try {
				if (preStmt != null) {
					preStmt.close();
				}
				if (preStmtTranslation != null) {
					preStmtTranslation.close();
				}
			} catch (SQLException sqlException) {
				LOGGER.error(sqlException.toString());
			}
			try {
				if (connection != null) {
					connection.close();
				}
			} catch (SQLException sqlException) {
				LOGGER.error(sqlException.toString());
			}
		}
	}
	
	/**
	 * Method for Insert start application data.
	 *
	 * @param startApplications {@link List<StartApp>}
	 */
	public void insertStartApplicationData(final List<StartApp> startApplications) {
		Connection connection = null;
		PreparedStatement preStmt = null;
		PreparedStatement preStmtTranslation = null;
		String selectSql = "SELECT * FROM START_APPLICATIONS_TBL";
		String insertSql = "INSERT INTO START_APPLICATIONS_TBL (START_APPLICATION_ID, NAME, BASE_APPLICATION_ID, IS_MESSAGE, START_MESSAGE_EXPIRY_DATE, STATUS, ICON_ID, CREATE_DATE, UPDATE_DATE) VALUES (?, ?, ?, ?, ?, ?, ?, SYSTIMESTAMP, SYSTIMESTAMP)";
		String insertSqlTranslation = "INSERT INTO START_APP_TRANSLATION_TBL (START_APP_TRANSLATION_ID, START_APPLICATION_ID, DESCRIPTION, REMARKS, LANGUAGE_CODE, CREATE_DATE, UPDATE_DATE) VALUES (?, ?, ?, ?, ?, SYSTIMESTAMP, SYSTIMESTAMP)";

		try {
			connection = DriverManager.getConnection(NEW_DB_URL, NEW_DB_USERNAME, NEW_DB_PASSWORD);
			if (!isDataExists(connection, selectSql)) {
				for (StartApp startApplication : startApplications) {
					preStmt = connection.prepareStatement(insertSql);
					preStmt.setString(1, startApplication.getId());
					preStmt.setString(2, startApplication.getName());
					preStmt.setString(3, getBaseApplicationId(connection, startApplication.getBaseAppName()));
					preStmt.setString(4, Boolean.toString(startApplication.isMessage()));
					preStmt.setDate(5, startApplication.getMessageExpityDate());
					preStmt.setString(6, startApplication.getStatus());
					preStmt.setString(7, getIconId(connection, MigrationConstents.Icons.START_APPLICATION, startApplication.getIconName()));

					int flag = preStmt.executeUpdate();
					preStmt.close();

					if (flag > 0) {
						List<StartAppTranslation> startAppTranslations = startApplication.getStartAppTranslations();
						for (StartAppTranslation startAppTranslation : startAppTranslations) {
							preStmtTranslation = connection.prepareStatement(insertSqlTranslation);
							preStmtTranslation.setString(1, startAppTranslation.getId());
							preStmtTranslation.setString(2, startApplication.getId());							
							preStmtTranslation.setString(3, startAppTranslation.getDescription());
							preStmtTranslation.setString(4, startAppTranslation.getRemarks());
							preStmtTranslation.setString(5, startAppTranslation.getLanguageCode());
							preStmtTranslation.executeUpdate();
							preStmtTranslation.close();
						}
					}
				}
			}
			connection.commit();
			connection.close();
		} catch (Exception e) {
			LOGGER.error(e.toString());
		} finally {
			try {
				if (preStmt != null) {
					preStmt.close();
				}
				if (preStmtTranslation != null) {
					preStmtTranslation.close();
				}
			} catch (SQLException sqlException) {
				LOGGER.error(sqlException.toString());
			}
			try {
				if (connection != null) {
					connection.close();
				}
			} catch (SQLException sqlException) {
				LOGGER.error(sqlException.toString());
			}
		}
	}
	
	/**
	 * Method for Update position name with id.
	 *
	 * @param connection {@link Connection}
	 * @param tableName {@link String}
	 */
	private void updatePositionNameWithId(Connection connection, String tableName) {
		Statement stmt = null;
		if (tableName != null) {
			String selectSql = null;
			if ("USER_APPLICATIONS_TBL".equals(tableName)) {
				selectSql = "SELECT tbl.USER_APPLICATION_ID APPLICATION_ID, tbl.NAME from USER_APPLICATIONS_TBL tbl";
			} else if ("PROJECT_APPLICATIONS_TBL".equals(tableName)) {
				selectSql = "SELECT tbl.PROJECT_APPLICATION_ID APPLICATION_ID, tbl.NAME from PROJECT_APPLICATIONS_TBL tbl";
			}
			if (selectSql != null) {
				Map<String, String> applicationIDNameMap = new HashMap<String, String>();
				try {

					stmt = connection.createStatement();
					ResultSet rs;
					rs = stmt.executeQuery(selectSql);

					while (rs.next()) {
						applicationIDNameMap.put(rs.getString("APPLICATION_ID"), rs.getString("NAME"));				
					}
					rs.close();

					if (!applicationIDNameMap.isEmpty()) {
						Set<String> idKeySet = applicationIDNameMap.keySet();
						for (String idKey : idKeySet) {
							String updateSql = "UPDATE " + tableName + " SET POSITION = '" + idKey
									+ "' WHERE POSITION = '" + applicationIDNameMap.get(idKey) + "'";
							stmt.executeUpdate(updateSql);
						}
					}

					stmt.close();
				} catch (Exception e) {
					LOGGER.error(e.toString());
				} finally {
					try {
						if (stmt != null) {
							stmt.close();
						}
					} catch (SQLException sqlException) {
						LOGGER.error(sqlException.toString());
					}
				}
			}
		}
	}

	/**
	 * Gets the icon id.
	 *
	 * @param connection {@link Connection}
	 * @param defaultIconName {@link String}
	 * @param iconName {@link String}
	 * @return the icon id
	 */
	private String getIconId(Connection connection, String defaultIconName, String iconName) {
		String iconId = null;
		Statement stmt = null;
		String dbIconName = iconName;
		if (dbIconName == null) {
			dbIconName = defaultIconName;
		}
		try {
			stmt = connection.createStatement();
			ResultSet rs;
			rs = stmt.executeQuery("SELECT ICON_ID FROM ICONS_TBL WHERE ICON_NAME = '" + dbIconName + "'");

			while (rs.next()) {
				iconId = rs.getString("ICON_ID");
			}
			rs.close();

			if (iconId == null && defaultIconName != null && !defaultIconName.equals(dbIconName)) {
				rs = stmt.executeQuery("SELECT ICON_ID FROM ICONS_TBL WHERE ICON_NAME = '" + defaultIconName + "'");

				while (rs.next()) {
					iconId = rs.getString("ICON_ID");
				}
				rs.close();
			}
			stmt.close();
		} catch (Exception e) {
			LOGGER.error(e.toString());
		} finally {
			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException sqlException) {
				LOGGER.error(sqlException.toString());
			}
		}
		return iconId;
	}

	/**
	 * Method for Clear tables data.
	 *
	 * @return true, if successful
	 */
	public boolean clearTablesData() {
		boolean isSuccess = false;
		Connection connection = null;
		ArrayList<String> sqlStatements = new ArrayList<>();
		sqlStatements.add("DELETE FROM USER_HISTORY_TBL");
		sqlStatements.add("DELETE FROM ADMIN_MENU_CONFIG_TBL");
		sqlStatements.add("DELETE FROM DIRECTORY_TBL");
		sqlStatements.add("DELETE FROM START_APPLICATIONS_TBL");
		sqlStatements.add("DELETE FROM PROJECT_APPLICATIONS_TBL");
		sqlStatements.add("DELETE FROM USER_APPLICATIONS_TBL");
		sqlStatements.add("DELETE FROM BASE_APPLICATIONS_TBL");
		sqlStatements.add("DELETE FROM USERS_TBL");
		sqlStatements.add("DELETE FROM PROJECTS_TBL");
		sqlStatements.add("DELETE FROM SITES_TBL");

		try {
			connection = DriverManager.getConnection(NEW_DB_URL, NEW_DB_USERNAME, NEW_DB_PASSWORD);
			for (String sqlStatement : sqlStatements) {
				isSuccess = deleteData(connection, sqlStatement);
				if (!isSuccess) {
					break;
				}
			}
			connection.commit();
			connection.close();
		} catch (Exception e) {
			LOGGER.error(e.toString());
		} finally {
			try {
				if (connection != null) {
					connection.close();
				}
			} catch (SQLException sqlException) {
				LOGGER.error(sqlException.toString());
			}
		}
		return isSuccess;
	}

	/**
	 * Method for Delete data.
	 *
	 * @param connection {@link Connection}
	 * @param deleteSql {@link String}
	 * @return true, if successful
	 */
	private boolean deleteData(Connection connection, String deleteSql) {
		Statement stmt = null;
		boolean isSuccess = false;
		try {
			stmt = connection.createStatement();
			int rowCount = stmt.executeUpdate(deleteSql);
			if (rowCount >= 0) {
				isSuccess = true;
			}
			stmt.close();
			connection.commit();
		} catch (Exception e) {
			LOGGER.error(e.toString());
		} finally {
			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException sqlException) {
				LOGGER.error(sqlException.toString());
			}
		}
		return isSuccess;
	}

	/**
	 * Gets the base application id.
	 *
	 * @param connection {@link Connection}
	 * @param baseAppName {@link String}
	 * @return the base application id
	 */
	private String getBaseApplicationId(Connection connection, String baseAppName) {
		String baseApplicationId = null;
		Statement stmt = null;
		if (baseAppName != null && baseAppName.length() > 0) {
			try {
				stmt = connection.createStatement();
				ResultSet rs;
				rs = stmt.executeQuery("SELECT BASE_APPLICATION_ID FROM BASE_APPLICATIONS_TBL WHERE NAME = '"
						+ baseAppName + "'");

				while (rs.next()) {
					baseApplicationId = rs.getString("BASE_APPLICATION_ID");
				}
				rs.close();
				stmt.close();
			} catch (Exception e) {
				LOGGER.error(e.toString());
			} finally {
				try {
					if (stmt != null) {
						stmt.close();
					}
				} catch (SQLException sqlException) {
					LOGGER.error(sqlException.toString());
				}
			}
		}
		return baseApplicationId;
	}

	/**
	 * Checks if is data exists.
	 *
	 * @param connection {@link Connection}
	 * @param selectSql {@link String}
	 * @return true, if is data exists
	 */
	private boolean isDataExists(Connection connection, String selectSql) {
		Statement stmt = null;
		boolean isDataExists = true;
		try {
			stmt = connection.createStatement();
			int rowCount = stmt.executeUpdate(selectSql);
			if (rowCount == 0) {
				isDataExists = false;
			} else {
				LOGGER.info("Data exists in query - " + selectSql);
			}
			stmt.close();
			connection.commit();
		} catch (Exception e) {
			LOGGER.error(e.toString());
		} finally {
			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException sqlException) {
				LOGGER.error(sqlException.toString());
			}
		}
		return isDataExists;
	}
	
	
	/**
	 * Gets the admin area data.
	 *
	 * @return the admin area data
	 */
	public List<AdminArea> getAdminAreaData() {
		Connection connection = null;
		Statement statement1 = null;
		Statement statement2 = null;

		List<AdminArea> adminAreas = new ArrayList<>();

		try {
			connection = DriverManager.getConnection(NEW_DB_URL, NEW_DB_USERNAME, NEW_DB_PASSWORD);
			statement1 = connection.createStatement();
			statement2 = connection.createStatement();
			String sql;
			sql = "SELECT * FROM ADMIN_AREAS_TBL";
			ResultSet rs = statement1.executeQuery(sql);
			
			while (rs.next()) {
				AdminArea adminArea = new AdminArea();
				adminArea.setId(rs.getString("ADMIN_AREA_ID"));
				adminArea.setName(rs.getString("NAME"));
				adminArea.setIconId(rs.getString("ICON_ID"));
				adminArea.setSingletonAppTimeout(rs.getString("SINGLETON_APP_TIMEOUT"));
				adminArea.setHotlineContactEmail(rs.getString("HOTLINE_CONTACT_EMAIL"));
				adminArea.setHotlineContactNumber(rs.getString("HOTLINE_CONTACT_NUMBER"));
				adminArea.setStatus(rs.getString("STATUS"));
				
				ResultSet rs2 = statement2.executeQuery("SELECT * FROM ADMIN_AREA_TRANSLATION_TBL WHERE ADMIN_AREA_ID = '"+rs.getString("ADMIN_AREA_ID")+"'");
				while (rs2.next()) {
					AdminAreaTranslation  adminAreaTranslation = new AdminAreaTranslation(rs2.getString("LANGUAGE_CODE"));
					adminAreaTranslation.setDescription(rs2.getString("DESCRIPTION"));
					adminAreaTranslation.setRemarks(rs2.getString("REMARKS"));
					adminArea.setAdminAreaTranslation(adminAreaTranslation);
				}
				rs2.close();
				
				adminAreas.add(adminArea);
			}
			rs.close();
			statement1.close();
			statement2.close();
			connection.close();
		} catch (Exception e) {
			LOGGER.error(e.toString());
		} finally {
			try {
				if (statement1 != null) {
					statement1.close();
				}
				if (statement2 != null) {
					statement2.close();
				}
			} catch (SQLException sqlException) {
				LOGGER.error(sqlException.toString());
			}
			try {
				if (connection != null) {
					connection.close();
				}
			} catch (SQLException sqlException) {
				LOGGER.error(sqlException.toString());
			}
		}
		return adminAreas;
	}
	
	
	/**
	 * Method for Insert site admin area rel data.
	 *
	 * @param siteAARels {@link List<SiteAdminArea>}
	 */
	public void insertSiteAdminAreaRelData(final List<SiteAdminArea> siteAARels) {
		Connection connection = null;
		PreparedStatement preStmt = null;
		String selectSql = "SELECT * FROM SITE_ADMIN_AREA_REL_TBL";
		String insertSql = "INSERT INTO SITE_ADMIN_AREA_REL_TBL (SITE_ADMIN_AREA_REL_ID, SITE_ID, ADMIN_AREA_ID, STATUS) VALUES (?, ?, ?, ?)";

		try {
			connection = DriverManager.getConnection(NEW_DB_URL, NEW_DB_USERNAME, NEW_DB_PASSWORD);
			if (!isDataExists(connection, selectSql)) {
				for (SiteAdminArea siteAARel : siteAARels) {
					preStmt = connection.prepareStatement(insertSql);
					preStmt.setString(1, siteAARel.getId());
					preStmt.setString(2, siteAARel.getSiteId());
					preStmt.setString(3, siteAARel.getAdminAreaId());
					preStmt.setString(4, siteAARel.getStatus());

					preStmt.executeUpdate();
					preStmt.close();
				}
			}
			connection.commit();
			connection.close();
		} catch (Exception e) {
			LOGGER.error(e.toString());
		} finally {
			try {
				if (preStmt != null) {
					preStmt.close();
				}
			} catch (SQLException sqlException) {
				LOGGER.error(sqlException.toString());
			}
			try {
				if (connection != null) {
					connection.close();
				}
			} catch (SQLException sqlException) {
				LOGGER.error(sqlException.toString());
			}
		}
	}
	
	/**
	 * Method for Insert site admin area project rel data.
	 *
	 * @param siteAAProjects {@link List<SiteAdminAreaProject>}
	 */
	public void insertSiteAdminAreaProjectRelData(final List<SiteAdminAreaProject> siteAAProjects) {
		Connection connection = null;
		PreparedStatement preStmt = null;
		String selectSql = "SELECT * FROM ADMIN_AREA_PROJECT_REL_TBL";
		String insertSql = "INSERT INTO ADMIN_AREA_PROJECT_REL_TBL (ADMIN_AREA_PROJECT_REL_ID, SITE_ADMIN_AREA_REL_ID, PROJECT_ID, STATUS) VALUES (?, ?, ?, ?)";

		try {
			connection = DriverManager.getConnection(NEW_DB_URL, NEW_DB_USERNAME, NEW_DB_PASSWORD);
			if (!isDataExists(connection, selectSql)) {
				for (SiteAdminAreaProject siteAAProject : siteAAProjects) {
					preStmt = connection.prepareStatement(insertSql);
					preStmt.setString(1, siteAAProject.getId());
					preStmt.setString(2, siteAAProject.getSiteAdminAreaId());
					preStmt.setString(3, siteAAProject.getProjectId());
					preStmt.setString(4, siteAAProject.getStatus());

					preStmt.executeUpdate();
					preStmt.close();
				}
			}
			connection.commit();
			connection.close();
		} catch (Exception e) {
			LOGGER.error(e.toString());
		} finally {
			try {
				if (preStmt != null) {
					preStmt.close();
				}
			} catch (SQLException sqlException) {
				LOGGER.error(sqlException.toString());
			}
			try {
				if (connection != null) {
					connection.close();
				}
			} catch (SQLException sqlException) {
				LOGGER.error(sqlException.toString());
			}
		}
	}
	
	/**
	 * Method for Insert site admin area user app rel data.
	 *
	 * @param siteAAUserApps {@link List<SiteAdminAreaUserApplication>}
	 */
	public void insertSiteAdminAreaUserAppRelData(final List<SiteAdminAreaUserApplication> siteAAUserApps) {
		Connection connection = null;
		PreparedStatement preStmt = null;
		String selectSql = "SELECT * FROM ADMIN_AREA_USER_APP_REL_TBL";
		String insertSql = "INSERT INTO ADMIN_AREA_USER_APP_REL_TBL (ADMIN_AREA_USER_APP_REL_ID, SITE_ADMIN_AREA_REL_ID, USER_APPLICATION_ID, STATUS, REL_TYPE) VALUES (?, ?, ?, ?, ?)";

		try {
			connection = DriverManager.getConnection(NEW_DB_URL, NEW_DB_USERNAME, NEW_DB_PASSWORD);
			if (!isDataExists(connection, selectSql)) {
				for (SiteAdminAreaUserApplication siteAAUserApp : siteAAUserApps) {
					preStmt = connection.prepareStatement(insertSql);
					preStmt.setString(1, siteAAUserApp.getId());
					preStmt.setString(2, siteAAUserApp.getSiteAdminAreaId());
					preStmt.setString(3, siteAAUserApp.getUserApplicationId());
					preStmt.setString(4, siteAAUserApp.getStatus());
					preStmt.setString(5, siteAAUserApp.getRelType());

					preStmt.executeUpdate();
					preStmt.close();
				}
			}
			connection.commit();
			connection.close();
		} catch (Exception e) {
			LOGGER.error(e.toString());
		} finally {
			try {
				if (preStmt != null) {
					preStmt.close();
				}
			} catch (SQLException sqlException) {
				LOGGER.error(sqlException.toString());
			}
			try {
				if (connection != null) {
					connection.close();
				}
			} catch (SQLException sqlException) {
				LOGGER.error(sqlException.toString());
			}
		}
	}
	
	
	/**
	 * Method for Insert site admin area start app rel data.
	 *
	 * @param siteAAStartApps {@link List<SiteAdminAreaStartApplication>}
	 */
	public void insertSiteAdminAreaStartAppRelData(final List<SiteAdminAreaStartApplication> siteAAStartApps) {
		Connection connection = null;
		PreparedStatement preStmt = null;
		String selectSql = "SELECT * FROM ADMIN_AREA_START_APP_REL_TBL";
		String insertSql = "INSERT INTO ADMIN_AREA_START_APP_REL_TBL (ADMIN_AREA_START_APP_REL_ID, SITE_ADMIN_AREA_REL_ID, START_APPLICATION_ID, STATUS) VALUES (?, ?, ?, ?)";

		try {
			connection = DriverManager.getConnection(NEW_DB_URL, NEW_DB_USERNAME, NEW_DB_PASSWORD);
			if (!isDataExists(connection, selectSql)) {
				for (SiteAdminAreaStartApplication siteAAStartApp : siteAAStartApps) {
					preStmt = connection.prepareStatement(insertSql);
					preStmt.setString(1, siteAAStartApp.getId());
					preStmt.setString(2, siteAAStartApp.getSiteAdminAreaId());
					preStmt.setString(3, siteAAStartApp.getStartApplicationId());
					preStmt.setString(4, siteAAStartApp.getStatus());

					preStmt.executeUpdate();
					preStmt.close();
				}
			}
			connection.commit();
			connection.close();
		} catch (Exception e) {
			LOGGER.error(e.toString());
		} finally {
			try {
				if (preStmt != null) {
					preStmt.close();
				}
			} catch (SQLException sqlException) {
				LOGGER.error(sqlException.toString());
			}
			try {
				if (connection != null) {
					connection.close();
				}
			} catch (SQLException sqlException) {
				LOGGER.error(sqlException.toString());
			}
		}
	}
	
	
	/**
	 * Method for Insert site admin area project project app rel data.
	 *
	 * @param siteAAProjectProjectApps {@link List<SiteAdminAreaProjectProjectApplication>}
	 */
	public void insertSiteAdminAreaProjectProjectAppRelData(final List<SiteAdminAreaProjectProjectApplication> siteAAProjectProjectApps) {
		Connection connection = null;
		PreparedStatement preStmt = null;
		String selectSql = "SELECT * FROM ADMIN_AREA_PROJ_APP_REL_TBL";
		String insertSql = "INSERT INTO ADMIN_AREA_PROJ_APP_REL_TBL (ADMIN_AREA_PROJ_APP_REL_ID, ADMIN_AREA_PROJECT_REL_ID, PROJECT_APPLICATION_ID, STATUS, REL_TYPE) VALUES (?, ?, ?, ?, ?)";

		try {
			connection = DriverManager.getConnection(NEW_DB_URL, NEW_DB_USERNAME, NEW_DB_PASSWORD);
			if (!isDataExists(connection, selectSql)) {
				for (SiteAdminAreaProjectProjectApplication siteAAProjectProjectApp : siteAAProjectProjectApps) {
					preStmt = connection.prepareStatement(insertSql);
					preStmt.setString(1, siteAAProjectProjectApp.getId());
					preStmt.setString(2, siteAAProjectProjectApp.getSiteAdminAreaProjectId());
					preStmt.setString(3, siteAAProjectProjectApp.getProjectApplicationId());
					preStmt.setString(4, siteAAProjectProjectApp.getStatus());
					preStmt.setString(5, siteAAProjectProjectApp.getRelType());

					preStmt.executeUpdate();
					preStmt.close();
				}
			}
			connection.commit();
			connection.close();
		} catch (Exception e) {
			LOGGER.error(e.toString());
		} finally {
			try {
				if (preStmt != null) {
					preStmt.close();
				}
			} catch (SQLException sqlException) {
				LOGGER.error(sqlException.toString());
			}
			try {
				if (connection != null) {
					connection.close();
				}
			} catch (SQLException sqlException) {
				LOGGER.error(sqlException.toString());
			}
		}
	}
	
	
	/**
	 * Method for Insert site admin area project start app rel data.
	 *
	 * @param siteAAProjectStartApps {@link List<SiteAdminAreaProjectStartApplication>}
	 */
	public void insertSiteAdminAreaProjectStartAppRelData(final List<SiteAdminAreaProjectStartApplication> siteAAProjectStartApps) {
		Connection connection = null;
		PreparedStatement preStmt = null;
		String selectSql = "SELECT * FROM PROJECT_START_APP_REL_TBL";
		String insertSql = "INSERT INTO PROJECT_START_APP_REL_TBL (PROJECT_START_APP_REL_ID, ADMIN_AREA_PROJECT_REL_ID, START_APPLICATION_ID, STATUS) VALUES (?, ?, ?, ?)";

		try {
			connection = DriverManager.getConnection(NEW_DB_URL, NEW_DB_USERNAME, NEW_DB_PASSWORD);
			if (!isDataExists(connection, selectSql)) {
				for (SiteAdminAreaProjectStartApplication siteAAProjectStartApp : siteAAProjectStartApps) {
					preStmt = connection.prepareStatement(insertSql);
					preStmt.setString(1, siteAAProjectStartApp.getId());
					preStmt.setString(2, siteAAProjectStartApp.getSiteAdminAreaProjectId());
					preStmt.setString(3, siteAAProjectStartApp.getStartApplicationId());
					preStmt.setString(4, siteAAProjectStartApp.getStatus());

					preStmt.executeUpdate();
					preStmt.close();
				}
			}
			connection.commit();
			connection.close();
		} catch (Exception e) {
			LOGGER.error(e.toString());
		} finally {
			try {
				if (preStmt != null) {
					preStmt.close();
				}
			} catch (SQLException sqlException) {
				LOGGER.error(sqlException.toString());
			}
			try {
				if (connection != null) {
					connection.close();
				}
			} catch (SQLException sqlException) {
				LOGGER.error(sqlException.toString());
			}
		}
	}
	
	
	/**
	 * Method for Insert user project rel data.
	 *
	 * @param userProjects {@link List<UserProject>}
	 */
	public void insertUserProjectRelData(final List<UserProject> userProjects) {
		Connection connection = null;
		PreparedStatement preStmt = null;
		String selectSql = "SELECT * FROM USER_PROJECT_REL_TBL";
		String insertSql = "INSERT INTO USER_PROJECT_REL_TBL (USER_PROJECT_REL_ID, USER_ID, PROJECT_ID, PROJECT_EXPIRY_DAYS, STATUS, EXPIRY_NOTIF_MAIL_FLAG, EXPIRY_GRACE_NOTIF_MAIL_FLAG, CREATE_DATE, UPDATE_DATE) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";

		String defaultProjectExpiry = propertyConfigMap.get("PROJECT_EXPIRY_DAYS");
		
		try {
			connection = DriverManager.getConnection(NEW_DB_URL, NEW_DB_USERNAME, NEW_DB_PASSWORD);
			if (!isDataExists(connection, selectSql)) {
				for (UserProject userProject : userProjects) {
					preStmt = connection.prepareStatement(insertSql);
					preStmt.setString(1, userProject.getId());
					preStmt.setString(2, userProject.getUserId());
					preStmt.setString(3, userProject.getProjectId());
					//preStmt.setString(4, defaultProjectExpiry != null ? defaultProjectExpiry : "0");
					preStmt.setString(4, "0");
					preStmt.setString(5, userProject.getStatus());
					preStmt.setString(6, "false");
					preStmt.setString(7, "false");
					preStmt.setTimestamp(8, java.sql.Timestamp.valueOf(java.time.LocalDateTime.now()));
					preStmt.setTimestamp(9, java.sql.Timestamp.valueOf(java.time.LocalDateTime.now()));
					
					preStmt.executeUpdate();
					preStmt.close();
				}
			}
			connection.commit();
			connection.close();
		} catch (Exception e) {
			LOGGER.error(e.toString());
		} finally {
			try {
				if (preStmt != null) {
					preStmt.close();
				}
			} catch (SQLException sqlException) {
				LOGGER.error(sqlException.toString());
			}
			try {
				if (connection != null) {
					connection.close();
				}
			} catch (SQLException sqlException) {
				LOGGER.error(sqlException.toString());
			}
		}
	}
	
	
	/**
	 * Method for Insert user start app rel data.
	 *
	 * @param userStartApps {@link List<UserStartApplication>}
	 */
	public void insertUserStartAppRelData(final List<UserStartApplication> userStartApps) {
		Connection connection = null;
		PreparedStatement preStmt = null;
		String selectSql = "SELECT * FROM USER_START_APP_REL_TBL";
		String insertSql = "INSERT INTO USER_START_APP_REL_TBL (USER_START_APP_REL_ID, USER_ID, START_APPLICATION_ID, STATUS) VALUES (?, ?, ?, ?)";

		try {
			connection = DriverManager.getConnection(NEW_DB_URL, NEW_DB_USERNAME, NEW_DB_PASSWORD);
			if (!isDataExists(connection, selectSql)) {
				for (UserStartApplication userStartApp : userStartApps) {
					preStmt = connection.prepareStatement(insertSql);
					preStmt.setString(1, userStartApp.getId());
					preStmt.setString(2, userStartApp.getUserId());
					preStmt.setString(3, userStartApp.getStartAppId());
					preStmt.setString(4, userStartApp.getStatus());

					preStmt.executeUpdate();
					preStmt.close();
				}
			}
			connection.commit();
			connection.close();
		} catch (Exception e) {
			LOGGER.error(e.toString());
		} finally {
			try {
				if (preStmt != null) {
					preStmt.close();
				}
			} catch (SQLException sqlException) {
				LOGGER.error(sqlException.toString());
			}
			try {
				if (connection != null) {
					connection.close();
				}
			} catch (SQLException sqlException) {
				LOGGER.error(sqlException.toString());
			}
		}
	}
	
	/**
	 * Method for Insert directory data.
	 *
	 * @param directories {@link List<Directory>}
	 */
	public void insertDirectoryData(final List<Directory> directories) {
		Connection connection = null;
		PreparedStatement preStmt = null;
		PreparedStatement preStmtTranslation = null;
		String selectSql = "SELECT * FROM DIRECTORY_TBL";
		String insertSql = "INSERT INTO DIRECTORY_TBL (DIRECTORY_ID, NAME, ICON_ID, CREATE_DATE, UPDATE_DATE) VALUES (?, ?, ?, SYSTIMESTAMP, SYSTIMESTAMP)";
		String insertSqlTranslation = "INSERT INTO DIRECTORY_TRANSLATION_TBL (DIRECTORY_TRANSLATION_ID, DIRECTORY_ID, DESCRIPTION, REMARKS, LANGUAGE_CODE, CREATE_DATE, UPDATE_DATE) VALUES (?, ?, ?, ?, ?, SYSTIMESTAMP, SYSTIMESTAMP)";

		try {
			connection = DriverManager.getConnection(NEW_DB_URL, NEW_DB_USERNAME, NEW_DB_PASSWORD);
			if (!isDataExists(connection, selectSql)) {
				for (Directory directory : directories) {
					preStmt = connection.prepareStatement(insertSql);
					preStmt.setString(1, directory.getId());
					preStmt.setString(2, directory.getName());
					preStmt.setString(3, getIconId(connection, MigrationConstents.Icons.DIRECTORY, directory.getIconName()));

					int flag = preStmt.executeUpdate();
					preStmt.close();

					if (flag > 0) {
						List<DirectoryTranslation> directoryTranslations = directory.getDirectoryTranslations();
						for (DirectoryTranslation directoryTranslation : directoryTranslations) {
							preStmtTranslation = connection.prepareStatement(insertSqlTranslation);
							preStmtTranslation.setString(1, directoryTranslation.getId());
							preStmtTranslation.setString(2, directory.getId());							
							preStmtTranslation.setString(3, directoryTranslation.getDescription());
							preStmtTranslation.setString(4, directoryTranslation.getRemarks());
							preStmtTranslation.setString(5, directoryTranslation.getLanguageCode());
							preStmtTranslation.executeUpdate();
							preStmtTranslation.close();
						}
					}
				}
			}
			connection.commit();
			connection.close();
		} catch (Exception e) {
			LOGGER.error(e.toString());
		} finally {
			try {
				if (preStmt != null) {
					preStmt.close();
				}
				if (preStmtTranslation != null) {
					preStmtTranslation.close();
				}
			} catch (SQLException sqlException) {
				LOGGER.error(sqlException.toString());
			}
			try {
				if (connection != null) {
					connection.close();
				}
			} catch (SQLException sqlException) {
				LOGGER.error(sqlException.toString());
			}
		}
	}
	
	
	/**
	 * Method for Insert directory object ref data.
	 *
	 * @param directoryObjectReferences {@link List<DirectoryObjectReference>}
	 */
	public void insertDirectoryObjectRefData(final List<DirectoryObjectReference> directoryObjectReferences) {
		Connection connection = null;
		PreparedStatement preStmt = null;
		String selectSql = "SELECT * FROM DIRECTORY_REF_TBL";
		String insertSql = "INSERT INTO DIRECTORY_REF_TBL (DIRECTORY_REF_ID, DIRECTORY_ID, OBJECT_TYPE, OBJECT_ID, CREATE_DATE, UPDATE_DATE) VALUES (?, ?, ?, ?, SYSTIMESTAMP, SYSTIMESTAMP)";

		try {
			connection = DriverManager.getConnection(NEW_DB_URL, NEW_DB_USERNAME, NEW_DB_PASSWORD);
			if (!isDataExists(connection, selectSql)) {
				for (DirectoryObjectReference directoryObjectReference : directoryObjectReferences) {
					preStmt = connection.prepareStatement(insertSql);
					preStmt.setString(1, directoryObjectReference.getId());
					preStmt.setString(2, directoryObjectReference.getDirectoryId());
					preStmt.setString(3, directoryObjectReference.getObjectType());
					preStmt.setString(4, directoryObjectReference.getObjectId());

					preStmt.executeUpdate();
					preStmt.close();
				}
			}
			connection.commit();
			connection.close();
		} catch (Exception e) {
			LOGGER.error(e.toString());
		} finally {
			try {
				if (preStmt != null) {
					preStmt.close();
				}
			} catch (SQLException sqlException) {
				LOGGER.error(sqlException.toString());
			}
			try {
				if (connection != null) {
					connection.close();
				}
			} catch (SQLException sqlException) {
				LOGGER.error(sqlException.toString());
			}
		}
	}
	
	/**
	 * Method for Insert admin menu directory object ref data.
	 *
	 * @param adminMenuDirectoryObjectRefs {@link List<DirectoryObjectReference>}
	 */
	public void insertAdminMenuDirectoryObjectRefData(final List<DirectoryObjectReference> adminMenuDirectoryObjectRefs) {
		Connection connection = null;
		PreparedStatement preStmt = null;
		String selectSql = "SELECT * FROM ADMIN_MENU_CONFIG_TBL";
		String insertSql = "INSERT INTO ADMIN_MENU_CONFIG_TBL (ADMIN_MENU_CONFIG_ID, OBJECT_TYPE, OBJECT_ID, CREATE_DATE, UPDATE_DATE) VALUES (?, ?, ?, SYSTIMESTAMP, SYSTIMESTAMP)";

		try {
			connection = DriverManager.getConnection(NEW_DB_URL, NEW_DB_USERNAME, NEW_DB_PASSWORD);
			if (!isDataExists(connection, selectSql)) {
				for (DirectoryObjectReference adminMenuDirectoryObjectRef : adminMenuDirectoryObjectRefs) {
					preStmt = connection.prepareStatement(insertSql);
					preStmt.setString(1, adminMenuDirectoryObjectRef.getId());
					preStmt.setString(2, adminMenuDirectoryObjectRef.getObjectType());
					preStmt.setString(3, adminMenuDirectoryObjectRef.getObjectId());

					preStmt.executeUpdate();
					preStmt.close();
				}
			}
			connection.commit();
			connection.close();
		} catch (Exception e) {
			LOGGER.error(e.toString());
		} finally {
			try {
				if (preStmt != null) {
					preStmt.close();
				}
			} catch (SQLException sqlException) {
				LOGGER.error(sqlException.toString());
			}
			try {
				if (connection != null) {
					connection.close();
				}
			} catch (SQLException sqlException) {
				LOGGER.error(sqlException.toString());
			}
		}
	}
	
	/**
	 * Method for Insert user site admin area user app rel data.
	 *
	 * @param userAdminAreaUserApplications {@link List<UserAdminAreaUserApplication>}
	 */
	public void insertUserSiteAdminAreaUserAppRelData(final List<UserAdminAreaUserApplication> userAdminAreaUserApplications) {
		Connection connection = null;
		PreparedStatement preStmt = null;
		String selectSql = "SELECT * FROM USER_USER_APP_REL_TBL";
		String insertSql = "INSERT INTO USER_USER_APP_REL_TBL (USER_USER_APP_REL_ID, USER_ID, SITE_ADMIN_AREA_REL_ID, USER_APPLICATION_ID, USER_REL_TYPE) VALUES (?, ?, ?, ?, ?)";

		try {
			connection = DriverManager.getConnection(NEW_DB_URL, NEW_DB_USERNAME, NEW_DB_PASSWORD);
			if (!isDataExists(connection, selectSql)) {
				for (UserAdminAreaUserApplication userAdminAreaUserApplication : userAdminAreaUserApplications) {
					preStmt = connection.prepareStatement(insertSql);
					preStmt.setString(1, userAdminAreaUserApplication.getId());
					preStmt.setString(2, userAdminAreaUserApplication.getUserId());
					preStmt.setString(3, userAdminAreaUserApplication.getSiteAdminAreaId());
					preStmt.setString(4, userAdminAreaUserApplication.getUserAppId());
					preStmt.setString(5, userAdminAreaUserApplication.getRelType());

					preStmt.executeUpdate();
					preStmt.close();
				}
			}
			connection.commit();
			connection.close();
		} catch (Exception e) {
			LOGGER.error(e.toString());
		} finally {
			try {
				if (preStmt != null) {
					preStmt.close();
				}
			} catch (SQLException sqlException) {
				LOGGER.error(sqlException.toString());
			}
			try {
				if (connection != null) {
					connection.close();
				}
			} catch (SQLException sqlException) {
				LOGGER.error(sqlException.toString());
			}
		}
	}
	
	/**
	 * Method for Insert user site admin area project project app rel data.
	 *
	 * @param userAdminAreaProjectProjectApplications {@link List<UserAdminAreaProjectProjectApplication>}
	 */
	public void insertUserSiteAdminAreaProjectProjectAppRelData(final List<UserAdminAreaProjectProjectApplication> userAdminAreaProjectProjectApplications) {
		Connection connection = null;
		PreparedStatement preStmt = null;
		String selectSql = "SELECT * FROM USER_PROJ_APP_REL_TBL";
		String insertSql = "INSERT INTO USER_PROJ_APP_REL_TBL (USER_PROJ_APP_REL_ID, USER_PROJECT_REL_ID, ADMIN_AREA_PROJECT_REL_ID, PROJECT_APPLICATION_ID, USER_REL_TYPE) VALUES (?, ?, ?, ?, ?)";

		try {
			connection = DriverManager.getConnection(NEW_DB_URL, NEW_DB_USERNAME, NEW_DB_PASSWORD);
			if (!isDataExists(connection, selectSql)) {
				for (UserAdminAreaProjectProjectApplication userAdminAreaProjectProjectApplication : userAdminAreaProjectProjectApplications) {
					preStmt = connection.prepareStatement(insertSql);
					preStmt.setString(1, userAdminAreaProjectProjectApplication.getId());
					preStmt.setString(2, userAdminAreaProjectProjectApplication.getUserProjectId());
					preStmt.setString(3, userAdminAreaProjectProjectApplication.getSiteAdminAreaProjectId());
					preStmt.setString(4, userAdminAreaProjectProjectApplication.getProjectAppId());
					preStmt.setString(5, userAdminAreaProjectProjectApplication.getRelType());

					preStmt.executeUpdate();
					preStmt.close();
				}
			}
			connection.commit();
			connection.close();
		} catch (Exception e) {
			LOGGER.error(e.toString());
		} finally {
			try {
				if (preStmt != null) {
					preStmt.close();
				}
			} catch (SQLException sqlException) {
				LOGGER.error(sqlException.toString());
			}
			try {
				if (connection != null) {
					connection.close();
				}
			} catch (SQLException sqlException) {
				LOGGER.error(sqlException.toString());
			}
		}
	}
	
	/**
	 * Method for Insert user history data.
	 *
	 * @param userHistoryList {@link List<UserHistory>}
	 */
	public void insertUserHistoryData(final List<UserHistory> userHistoryList) {
		Connection connection = null;
		PreparedStatement preStmt = null;
		String insertSql = "INSERT INTO USER_HISTORY_TBL (ID, LOG_TIME, USER_NAME, HOST, SITE, PROJECT, APPLICATION, PID, RESULT, ARGS, IS_USER_STATUS) VALUES (USER_HISTORY_SEQ.nextval, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

		try {
			connection = DriverManager.getConnection(NEW_DB_URL, NEW_DB_USERNAME, NEW_DB_PASSWORD);
			
			for (UserHistory userHistory : userHistoryList) {
				preStmt = connection.prepareStatement(insertSql);
				preStmt.setDate(1, userHistory.getLogTime());
				preStmt.setString(2, userHistory.getUsername());
				preStmt.setString(3, userHistory.getHost());
				preStmt.setString(4, userHistory.getSite());
				preStmt.setString(5, userHistory.getProject());
				preStmt.setString(6, userHistory.getApplication());
				preStmt.setInt(7, userHistory.getPid());
				preStmt.setString(8, userHistory.getResult());
				preStmt.setString(9, userHistory.getArgs());
				preStmt.setString(10, Boolean.toString(userHistory.isUserStatus()));

				preStmt.executeUpdate();
				preStmt.close();
			}
			
			connection.commit();
			connection.close();
		} catch (Exception e) {
			LOGGER.error(e.toString());
		} finally {
			try {
				if (preStmt != null) {
					preStmt.close();
				}
			} catch (SQLException sqlException) {
				LOGGER.error(sqlException.toString());
			}
			try {
				if (connection != null) {
					connection.close();
				}
			} catch (SQLException sqlException) {
				LOGGER.error(sqlException.toString());
			}
		}
	}
}