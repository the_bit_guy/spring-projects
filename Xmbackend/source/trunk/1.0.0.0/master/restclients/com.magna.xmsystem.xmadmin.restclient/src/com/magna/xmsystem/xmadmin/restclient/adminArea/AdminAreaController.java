package com.magna.xmsystem.xmadmin.restclient.adminArea;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.magna.xmbackend.entities.AdminAreasTbl;
import com.magna.xmbackend.exception.CannotCreateObjectException;
import com.magna.xmbackend.vo.adminArea.AdminAreaRequest;
import com.magna.xmbackend.vo.adminArea.AdminAreaResponse;
import com.magna.xmsystem.dependencies.customExceptions.UnauthorizedAccessException;
import com.magna.xmsystem.xmadmin.restclient.RestController;
import com.magna.xmsystem.xmadmin.restclient.util.RestClientExpHandlerUtil;
import com.magna.xmsystem.xmadmin.restclient.util.RestClientSortingUtil;
import com.magna.xmsystem.xmadmin.restclient.util.XMAdminRestClientConstants;

/**
 * Class for Admin area controller.
 *
 * @author Roshan.Ekka
 */
public class AdminAreaController extends RestController {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(AdminAreaController.class);

	/**
	 * Constructor for AdminAreaController Class.
	 */
	public AdminAreaController() {
		super();
	}

	/**
	 * Method for Creates the admin area.
	 *
	 * @param adminAreaRequest
	 *            {@link AdminAreaRequest}
	 * @return the admin areas tbl {@link AdminAreasTbl}
	 */
	public AdminAreasTbl createAdminArea(AdminAreaRequest adminAreaRequest) throws UnauthorizedAccessException, CannotCreateObjectException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.CREATE_ADMIN_AREA);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<AdminAreaRequest> request = new HttpEntity<AdminAreaRequest>(adminAreaRequest, this.headers);
			ResponseEntity<AdminAreasTbl> responseEntity = restTemplate.postForEntity(url, request,
					AdminAreasTbl.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				AdminAreasTbl adminAreaTbl = responseEntity.getBody();
				return adminAreaTbl;
			} else {
				LOGGER.error("Error while calling XMSYSTEM Create Site REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		}  catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				String errorResponse = ((HttpStatusCodeException) e).getResponseBodyAsString();
				String errCode = RestClientExpHandlerUtil.handleCannotCreateObjectException(LOGGER, (HttpStatusCodeException) e);
				throw new CannotCreateObjectException(errorResponse, errCode);
			}else if (e instanceof ResourceAccessException) {
				throw e;
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Create Site REST Service", e); //$NON-NLS-1$
		}
		return null;
	}

	/**
	 * Gets the all admin areas.
	 *
	 * @param isSorted
	 *            {@link boolean}
	 * @return the all admin areas
	 */
	public List<AdminAreasTbl> getAllAdminAreas(final boolean isSorted) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.GET_ADMIN_AREAS);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<AdminAreaResponse> responseList = restTemplate.exchange(url, HttpMethod.GET, requestEntity,
					AdminAreaResponse.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				AdminAreaResponse adminAreaResponseObject = responseList.getBody();
				List<AdminAreasTbl> adminAreaTblList = (List<AdminAreasTbl>) adminAreaResponseObject
						.getAdminAreasTbls();
				return RestClientSortingUtil.sortAdminAreaTbl(adminAreaTblList);
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Get All Administration Area REST Service, returns the status code: " //$NON-NLS-1$
								+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			} else if(e instanceof ResourceAccessException){
				throw e;
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get All Administration Area REST Service", e); //$NON-NLS-1$
		}
		return null;
	}

	/**
	 * Gets the admin area by id.
	 *
	 * @param isSorted
	 *            {@link boolean}
	 * @return the admin area by id
	 */
	public AdminAreasTbl getAdminAreaById(String adminAreaId) throws UnauthorizedAccessException {
		try {
			String url = new String(
					this.serviceUrl + XMAdminRestClientConstants.GET_ADMIN_AREAS_BY_ID + "/" + adminAreaId);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<AdminAreasTbl> responseList = restTemplate.exchange(url, HttpMethod.GET, requestEntity,
					AdminAreasTbl.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				AdminAreasTbl adminAreaResponseObject = responseList.getBody();
				return adminAreaResponseObject;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Get All Administration Area by Id REST Service, returns the status code: " //$NON-NLS-1$
								+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get All Administration Area by Id REST Service", e); //$NON-NLS-1$
		}
		return null;
	}

	/**
	 * Method for Delete admin area.
	 *
	 * @param adminAreaId
	 *            {@link String}
	 * @return true, if successful
	 */
	public boolean deleteAdminArea(String adminAreaId) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.DELETE_ADMIN_AREA + "/" + adminAreaId);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(this.headers);
			ResponseEntity<Boolean> responseEntity = restTemplate.exchange(url, HttpMethod.POST, requestEntity,
					Boolean.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				return responseEntity.getBody();
			} else {
				LOGGER.error("Error while calling XMSYSTEM delete Site REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM delete Site REST Service!", e); //$NON-NLS-1$
		}
		return false;
	}

	/**
	 * Delete multi admin area.
	 *
	 * @param adminAreaIds the admin area ids
	 * @return the admin area response
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 */
	public AdminAreaResponse deleteMultiAdminArea(Set<String> adminAreaIds) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.DELETE_MULTI_ADMIN_AREA);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Set<String>> requestEntity = new HttpEntity<Set<String>>(adminAreaIds,this.headers);
			ResponseEntity<AdminAreaResponse> responseEntity = restTemplate.exchange(url, HttpMethod.POST, requestEntity,
					AdminAreaResponse.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				AdminAreaResponse adminAreaResponse = responseEntity.getBody();
				return adminAreaResponse;
			} else {
				LOGGER.error("Error while calling XMSYSTEM delete Multi AdminArea REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM delete Multi AdminArea REST Service!", e); //$NON-NLS-1$
		}
		return null;
	}
	
	/**
	 * Method for Update admin area.
	 *
	 * @param adminAreaRequest
	 *            {@link XMDunkinRequest}
	 * @return true, if successful
	 */
	public boolean updateAdminArea(AdminAreaRequest adminAreaRequest)
			throws UnauthorizedAccessException, CannotCreateObjectException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.UPDATE_ADMIN_AREA);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<AdminAreaRequest> request = new HttpEntity<AdminAreaRequest>(adminAreaRequest, this.headers);
			ResponseEntity<AdminAreasTbl> responseEntity = restTemplate.postForEntity(url, request,
					AdminAreasTbl.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				return true;
			} else {
				LOGGER.error("Error while calling XMSYSTEM update Adiministration Area REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				String errorResponse = ((HttpStatusCodeException) e).getResponseBodyAsString();
				String errCode = RestClientExpHandlerUtil.handleCannotCreateObjectException(LOGGER,
						(HttpStatusCodeException) e);
				throw new CannotCreateObjectException(errorResponse, errCode);
			}else if (e instanceof ResourceAccessException) {
				throw e;
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM update Adiministration Area REST Service!", e); //$NON-NLS-1$
		}
		return false;
	}

	/**
	 * Method for Update admin area status.
	 *
	 * @param adminAreaId
	 *            {@link String}
	 * @param status
	 *            {@link String}
	 * @return true, if successful
	 */
	public boolean updateAdminAreaStatus(String adminAreaId, String status) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.UPDATE_ADMIN_AREA_STATUS + "/" + status
					+ "/" + adminAreaId);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(this.headers);
			ResponseEntity<Boolean> responseEntity = restTemplate.exchange(url, HttpMethod.POST, requestEntity,
					Boolean.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				return responseEntity.getBody();
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Update Administration Area Status REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Update Administration Area Status REST Service!", e); //$NON-NLS-1$
		}
		return false;
	}

	/**
	 * Multi update admin area status.
	 *
	 * @param adminAreaRequest the admin area request
	 * @return the admin area response
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 */
	public AdminAreaResponse multiUpdateAdminAreaStatus(List<AdminAreaRequest> adminAreaRequest) throws UnauthorizedAccessException {
		try {
			String url = new String(
					this.serviceUrl + XMAdminRestClientConstants.MULTI_UPDATE_ADMIN_AREA_STATUS );

			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<List<AdminAreaRequest>> requestEntity = new HttpEntity<List<AdminAreaRequest>>(adminAreaRequest, this.headers);
			ResponseEntity<AdminAreaResponse> responseEntity = restTemplate.exchange(url, HttpMethod.POST, requestEntity,
					AdminAreaResponse.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				return responseEntity.getBody();
			} else {
				LOGGER.error("Error while calling XMSYSTEM Multi Update adminArea Status REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Multi Update adminArea Status REST Service!", e); //$NON-NLS-1$
		}
		return null;
	}
	
	/**
	 * Gets the AA by project id.
	 *
	 * @param projectId
	 *            {@link String}
	 * @return the AA by project id
	 */
	public AdminAreaResponse getAAByProjectId(String projectId) throws UnauthorizedAccessException {
		try {
			String url = new String(
					this.serviceUrl + XMAdminRestClientConstants.GET_ADMIN_AREA_BY_PROJECT_ID + "/" + projectId);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<AdminAreaResponse> responseList = restTemplate.exchange(url, HttpMethod.GET, requestEntity,
					AdminAreaResponse.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				AdminAreaResponse adminAreaRespObj = responseList.getBody();
				return adminAreaRespObj;
			} else {
				LOGGER.error("Error while calling XMSYSTEM Get All Administration Area By ProjectId REST Service, returns the status code: " //$NON-NLS-1$
								+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get All Administration Area By ProjectId REST Service", e); //$NON-NLS-1$
		}
		return null;
	}

	/**
	 * Gets the AA by user app id.
	 *
	 * @param userAppId
	 *            {@link String}
	 * @return the AA by user app id
	 */
	public AdminAreaResponse getAAByUserAppId(String userAppId) throws UnauthorizedAccessException {
		try {
			String url = new String(
					this.serviceUrl + XMAdminRestClientConstants.GET_ADMIN_AREA_BY_USER_APP_ID + "/" + userAppId);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<AdminAreaResponse> responseList = restTemplate.exchange(url, HttpMethod.GET, requestEntity,
					AdminAreaResponse.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				AdminAreaResponse adminAreaRespObj = responseList.getBody();
				return adminAreaRespObj;
			} else {
				LOGGER.error("Error while calling XMSYSTEM Get All Administration Area By UserAppId REST Service, returns the status code: " //$NON-NLS-1$
								+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get All Administration Area By UserAppId REST Service", e); //$NON-NLS-1$
		}
		return null;
	}

	/**
	 * Gets the AA by project app id.
	 *
	 * @param projectAppId
	 *            {@link String}
	 * @return the AA by project app id
	 */
	public AdminAreaResponse getAAByProjectAppId(final String projectAppId) throws UnauthorizedAccessException {
		try {
			final String url = new String(
					this.serviceUrl + XMAdminRestClientConstants.GET_ADMIN_AREA_BY_PROJECT_APP_ID + "/" + projectAppId);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<AdminAreaResponse> responseList = restTemplate.exchange(url, HttpMethod.GET, requestEntity,
					AdminAreaResponse.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				AdminAreaResponse adminAreaRespObj = responseList.getBody();
				return adminAreaRespObj;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Get All Administration Area By ProjectAppId REST Service, returns the status code: " //$NON-NLS-1$
								+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			}
		}  catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get All Administration Area By ProjectAppId REST Service", e); //$NON-NLS-1$
		}
		return null;
	}

	/**
	 * Gets the AA by start app id.
	 *
	 * @param startAppId
	 *            {@link String}
	 * @return the AA by start app id
	 */
	public AdminAreaResponse getAAByStartAppId(final String startAppId) throws UnauthorizedAccessException {
		try {
			final String url = new String(
					this.serviceUrl + XMAdminRestClientConstants.GET_ADMIN_AREA_BY_START_APP_ID + "/" + startAppId);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<AdminAreaResponse> responseList = restTemplate.exchange(url, HttpMethod.GET, requestEntity,
					AdminAreaResponse.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				AdminAreaResponse adminAreaRespObj = responseList.getBody();
				return adminAreaRespObj;
			} else {
				LOGGER.error("Error while calling XMSYSTEM Get All Administration Area By StartAppId REST Service, returns the status code: " //$NON-NLS-1$
								+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get All Administration Area By StartAppId REST Service", e); //$NON-NLS-1$
		}
		return null;
	}

	/**
	 * Gets the AA by project id and site id.
	 *
	 * @param projectId
	 *            {@link String}
	 * @param siteId
	 *            {@link String}
	 * @return the AA by project id and site id
	 */
	public List<AdminAreasTbl> getAAByProjectIdAndSiteId(String projectId, String siteId)
			throws UnauthorizedAccessException {
		try {
			String url = new String(
					this.serviceUrl + XMAdminRestClientConstants.GET_ADMIN_AREA_BY_PROJECT_ID_AND_SITE_ID + "/"
							+ projectId + "/" + siteId);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<AdminAreaResponse> responseList = restTemplate.exchange(url, HttpMethod.GET, requestEntity,
					AdminAreaResponse.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				AdminAreaResponse adminAreaRespObj = responseList.getBody();
				List<AdminAreasTbl> adminAreaTblList = (List<AdminAreasTbl>) adminAreaRespObj.getAdminAreasTbls();
				return adminAreaTblList;
			} else {
				LOGGER.error("Error while calling XMSYSTEM Get All Administration Area By ProjectId and SiteId REST Service, returns the status code: " //$NON-NLS-1$
								+ statusCode.value());
			} 
		}  catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			} else if (e instanceof ResourceAccessException) {
				throw e;
			}
		} catch (Exception e) {
			LOGGER.error(
					"Error while calling XMSYSTEM Get All Administration Area By ProjectId and SiteId REST Service", e); //$NON-NLS-1$
		}
		return null;
	}

	/**
	 * Gets the AA by user app id and site id.
	 *
	 * @param userAppId
	 *            {@link String}
	 * @param siteId
	 *            {@link String}
	 * @return the AA by user app id and site id
	 */
	public AdminAreaResponse getAAByUserAppIdAndSiteId(String userAppId, String siteId)
			throws UnauthorizedAccessException {
		try {
			String url = new String(
					this.serviceUrl + XMAdminRestClientConstants.GET_ADMIN_AREA_BY_USER_APP_ID_AND_SITE_ID + "/"
							+ userAppId + "/" + siteId);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<AdminAreaResponse> responseList = restTemplate.exchange(url, HttpMethod.GET, requestEntity,
					AdminAreaResponse.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				AdminAreaResponse adminAreaRespObj = responseList.getBody();
				return adminAreaRespObj;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Get All Administration Area By UserAppId and SiteId REST Service, returns the status code: " //$NON-NLS-1$
								+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			}
		} catch (Exception e) {
			LOGGER.error(
					"Error while calling XMSYSTEM Get All Administration Area By UserAppId and SiteId REST Service", e); //$NON-NLS-1$
		}
		return null;
	}

	/**
	 * Gets the AA by start app id and site id.
	 *
	 * @param startAppId
	 *            {@link String}
	 * @param siteId
	 *            {@link String}
	 * @return the AA by start app id and site id
	 */
	public AdminAreaResponse getAAByStartAppIdAndSiteId(String startAppId, String siteId)
			throws UnauthorizedAccessException {
		try {
			String url = new String(
					this.serviceUrl + XMAdminRestClientConstants.GET_ADMIN_AREA_BY_START_APP_ID_AND_SITE_ID + "/"
							+ startAppId + "/" + siteId);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<AdminAreaResponse> responseList = restTemplate.exchange(url, HttpMethod.GET, requestEntity,
					AdminAreaResponse.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				AdminAreaResponse adminAreaRespObj = responseList.getBody();
				return adminAreaRespObj;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Get All Administration Area By StartAppId and SiteId REST Service, returns the status code: " //$NON-NLS-1$
								+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			}
		} catch (Exception e) {
			LOGGER.error(
					"Error while calling XMSYSTEM Get All Administration Area By StartAppId and SiteId REST Service", //$NON-NLS-1$
					e);
		}
		return null;
	}

	/**
	 * Gets the AA by project app id and site id.
	 *
	 * @param projectAppId
	 *            {@link String}
	 * @param siteId
	 *            {@link String}
	 * @return the AA by project app id and site id
	 */
	public AdminAreaResponse getAAByProjectAppIdAndSiteId(String projectAppId, String siteId)
			throws UnauthorizedAccessException {
		try {
			String url = new String(
					this.serviceUrl + XMAdminRestClientConstants.GET_ADMIN_AREA_BY_PROJECT_APP_ID_AND_SITE_ID + "/"
							+ projectAppId + "/" + siteId);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<AdminAreaResponse> responseList = restTemplate.exchange(url, HttpMethod.GET, requestEntity,
					AdminAreaResponse.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				AdminAreaResponse adminAreaRespObj = responseList.getBody();
				return adminAreaRespObj;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Get All Administration Area By projectAppId and SiteId REST Service, returns the status code: " //$NON-NLS-1$
								+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			}
		} catch (Exception e) {
			LOGGER.error(
					"Error while calling XMSYSTEM Get All Administration Area By projectAppId and SiteId REST Service", //$NON-NLS-1$
					e);
		}
		return null;
	}
	
	/**
	 * Gets the AA by site id.
	 *
	 * @param projectAppId the project app id
	 * @param siteId the site id
	 * @return the AA by site id
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 */
	public AdminAreaResponse getAABySiteId(final String siteId) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.GET_AA_BY_SITE_ID + "/" + siteId);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<AdminAreaResponse> responseList = restTemplate.exchange(url, HttpMethod.GET, requestEntity,
					AdminAreaResponse.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				AdminAreaResponse adminAreaRespObj = responseList.getBody();
				return adminAreaRespObj;
			} else {
				LOGGER.error("Error while calling XMSYSTEM Get Administration Areas By SiteId REST Service, returns the status code: " + statusCode.value()); //$NON-NLS-1$
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get Administration Area By SiteId REST Service", e); //$NON-NLS-1$
		}
		return null;
	}
}
