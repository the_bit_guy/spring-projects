package com.magna.xmsystem.xmadmin.restclient.relation;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.magna.xmbackend.entities.RoleUserRelTbl;
import com.magna.xmbackend.exception.CannotCreateObjectException;
import com.magna.xmbackend.vo.rel.RoleUserRelRequest;
import com.magna.xmbackend.vo.rel.RoleUserRelResponse;
import com.magna.xmsystem.dependencies.customExceptions.UnauthorizedAccessException;
import com.magna.xmsystem.xmadmin.restclient.RestController;
import com.magna.xmsystem.xmadmin.restclient.site.SiteController;
import com.magna.xmsystem.xmadmin.restclient.util.RestClientExpHandlerUtil;
import com.magna.xmsystem.xmadmin.restclient.util.RestClientSortingUtil;
import com.magna.xmsystem.xmadmin.restclient.util.XMAdminRestClientConstants;

/**
 * Class for Site admin area rel controller.
 *
 * @author Roshan.Ekka
 */
public class RoleUserRelController extends RestController {
	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(SiteController.class);

	/**
	 * Constructor for SiteController Class.
	 */
	public RoleUserRelController() {
		super();
	}

	/**
	 * Method for Creates the role user rel.
	 *
	 * @param roleUserRelRequest
	 *            {@link List<RoleUserRelRequest>}
	 * @return the role user rel response {@link RoleUserRelResponse}
	 */
	public RoleUserRelResponse createRoleUserRel(List<RoleUserRelRequest> roleUserRelRequest)
			throws UnauthorizedAccessException, CannotCreateObjectException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.CREATE_ROLE_USER_REL);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<List<RoleUserRelRequest>> request = new HttpEntity<List<RoleUserRelRequest>>(roleUserRelRequest,
					this.headers);
			ResponseEntity<RoleUserRelResponse> responseEntity = restTemplate.postForEntity(url, request,
					RoleUserRelResponse.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				RoleUserRelResponse roleUserRelResponse = responseEntity.getBody();
				return roleUserRelResponse;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Create Role User Relation REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				String errorResponse = ((HttpStatusCodeException) e).getResponseBodyAsString();
				String errCode = RestClientExpHandlerUtil.handleCannotCreateObjectException(LOGGER,
						(HttpStatusCodeException) e);
				throw new CannotCreateObjectException(errorResponse, errCode);
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Create Role User Relation REST Service", e); //$NON-NLS-1$
		}
		return null;
	}

	/**
	 * Method for Creates the multi role user rel.
	 *
	 * @param roleUserRelRequest
	 *            {@link List<RoleUserRelRequest>}
	 * @return the role user rel response {@link RoleUserRelResponse}
	 */
	public RoleUserRelResponse createMultiRoleUserRel(List<RoleUserRelRequest> roleUserRelRequest)
			throws UnauthorizedAccessException, CannotCreateObjectException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.CREATE_MULTI_ROLE_USER_REL);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<List<RoleUserRelRequest>> request = new HttpEntity<List<RoleUserRelRequest>>(roleUserRelRequest,
					this.headers);
			ResponseEntity<RoleUserRelResponse> responseEntity = restTemplate.postForEntity(url, request,
					RoleUserRelResponse.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				RoleUserRelResponse roleUserRelResponse = responseEntity.getBody();
				return roleUserRelResponse;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Create Multi Role User Relation REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				String errorResponse = ((HttpStatusCodeException) e).getResponseBodyAsString();
				String errCode = RestClientExpHandlerUtil.handleCannotCreateObjectException(LOGGER,
						(HttpStatusCodeException) e);
				throw new CannotCreateObjectException(errorResponse, errCode);
			} else if (e instanceof ResourceAccessException) {
				throw e;
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Create Multi Role User Relation REST Service", e); //$NON-NLS-1$
		}
		return null;
	}

	/**
	 * Method for Delete role user rel.
	 *
	 * @param roleUserRelId
	 *            {@link String}
	 * @return true, if successful
	 */
	public boolean deleteRoleUserRel(String roleUserRelId) throws UnauthorizedAccessException {
		try {
			String url = new String(
					this.serviceUrl + XMAdminRestClientConstants.DELETE_ROLE_USER_REL + "/" + roleUserRelId);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(this.headers);
			ResponseEntity<Boolean> responseEntity = restTemplate.exchange(url, HttpMethod.DELETE, requestEntity,
					Boolean.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				return responseEntity.getBody();
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM delete Role User Relation REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM delete Role User Relation REST Service!", e); //$NON-NLS-1$
		}
		return false;
	}

	/**
	 * Delete multi role user rel.
	 *
	 * @param ids the ids
	 * @return the role user rel response
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 */
	public RoleUserRelResponse deleteMultiRoleUserRel(Set<String> ids) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.DELETE_MULTI_ROLE_USER_REL);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Set<String>> requestEntity = new HttpEntity<Set<String>>(ids, this.headers);
			ResponseEntity<RoleUserRelResponse> responseEntity = restTemplate.exchange(url, HttpMethod.DELETE,
					requestEntity, RoleUserRelResponse.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				RoleUserRelResponse roleUserRelResponse = responseEntity.getBody();
				return roleUserRelResponse;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM delete Multi RoleUserRel REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM delete Multi RoleUserRel REST Service!", e); //$NON-NLS-1$
		}
		return null;
	}
	
	/**
	 * Gets the all role user rel by role id.
	 *
	 * @param roleId
	 *            {@link String}
	 * @return the all role user rel by role id
	 */
	public List<RoleUserRelTbl> getRoleUserRelByRoleId(final String roleId) throws UnauthorizedAccessException {
		try {
			String url = new String(
					this.serviceUrl + XMAdminRestClientConstants.GET_ROLE_USER_REL_BY_ROLE_ID + "/" + roleId);
			
			List<RoleUserRelTbl> roleUserTblList = new ArrayList<>();
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<RoleUserRelResponse> responseList = restTemplate.exchange(url, HttpMethod.GET, requestEntity,
					RoleUserRelResponse.class);
			
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				RoleUserRelResponse roleUserRelResponseObject = responseList.getBody();
				Iterable<RoleUserRelTbl> roleUserRelTbls = roleUserRelResponseObject.getRoleUserRelTbls();
				for (RoleUserRelTbl roleUserRelTbl : roleUserRelTbls) {
					roleUserTblList.add(roleUserRelTbl);
				}
				return RestClientSortingUtil.sortRoleUserRelTbl(roleUserTblList);
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Get All Role User Relation By RoleId REST Service, returns the status code: " //$NON-NLS-1$
								+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			} else if(e instanceof ResourceAccessException){
				throw e;
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get All Role User Relation By RoleId REST Service", e); //$NON-NLS-1$
		}
		return null;
	}

	/**
	 * Gets the all role user rel by role id role AA rel id.
	 *
	 * @param roleId
	 *            {@link String}
	 * @param roleAARelId
	 *            {@link String}
	 * @return the all role user rel by role id role AA rel id
	 */
	public List<RoleUserRelTbl> getRoleUserRelByRoleIdAndRoleAARelId(final String roleId, String roleAARelId)
			throws UnauthorizedAccessException {
		try {
			String url = new String(
					this.serviceUrl + XMAdminRestClientConstants.GET_ROLE_USER_REL_BY_ROLE_ID_AND_ROLE_ADMINAREA_REL_ID
							+ "/" + roleId + "/" + roleAARelId);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<RoleUserRelResponse> responseList = restTemplate.exchange(url, HttpMethod.GET, requestEntity,
					RoleUserRelResponse.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				RoleUserRelResponse roleUserRelResponseObject = responseList.getBody();
				List<RoleUserRelTbl> RoleUserRelTblList = new ArrayList<>();
				Iterable<RoleUserRelTbl> roleUserRelTbls = roleUserRelResponseObject.getRoleUserRelTbls();
				for (RoleUserRelTbl roleUserRelTbl : roleUserRelTbls) {
					RoleUserRelTblList.add(roleUserRelTbl);
				}
				RestClientSortingUtil.sortRoleUserRelTbl(RoleUserRelTblList);
				return RoleUserRelTblList;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Get All Role User Relation By RoleId and Role AdminArea Relation Id REST Service, returns the status code: " //$NON-NLS-1$
								+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			} else if(e instanceof ResourceAccessException){
				throw e;
			}
		} catch (Exception e) {
			LOGGER.error(
					"Error while calling XMSYSTEM Get All Role User Relation By RoleId and Role AdminArea Relation Id REST Service", //$NON-NLS-1$
					e);
		}
		return null;
	}
}
