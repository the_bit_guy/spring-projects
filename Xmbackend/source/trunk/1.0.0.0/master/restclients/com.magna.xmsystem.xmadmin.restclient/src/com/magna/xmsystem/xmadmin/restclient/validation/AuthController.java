package com.magna.xmsystem.xmadmin.restclient.validation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

import com.magna.xmbackend.vo.enums.Application;
import com.magna.xmbackend.vo.user.AuthResponse;
import com.magna.xmsystem.dependencies.customExceptions.UnauthorizedAccessException;
import com.magna.xmsystem.dependencies.utils.XMSystemUtil;
import com.magna.xmsystem.xmadmin.restclient.RestController;
import com.magna.xmsystem.xmadmin.restclient.util.RestClientExpHandlerUtil;
import com.magna.xmsystem.xmadmin.restclient.util.XMAdminRestClientConstants;

/**
 * The Class AuthController.
 */
public class AuthController extends RestController{
	
	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(AuthController.class);

	/**
	 * Instantiates a new auth controller.
	 */
	public AuthController() {
		super();
	}

	/**
	 * Authorize login.
	 *
	 * @param userCredential the user credential
	 * @return the auth response
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 */
	public AuthResponse authorizeLogin(final String encryptTicket, final String caxAppName)
			throws UnauthorizedAccessException, ResourceAccessException, IllegalArgumentException,
			HttpClientErrorException {
		String url = new String(this.serviceUrl + XMAdminRestClientConstants.GET_AUTHENTICATION_RESPONSE);
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.set("TKT", encryptTicket);
			headers.set("APP_NAME", caxAppName);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);
			ResponseEntity<AuthResponse> responseEntity = restTemplate.exchange(url, HttpMethod.POST, requestEntity,
					AuthResponse.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			HttpHeaders headersFromBackend = responseEntity.getHeaders();
			String tkt = headersFromBackend.getFirst("TKT");

			if (Application.CAX_ADMIN_MENU.name().equals(caxAppName)) {
				XMSystemUtil.getInstance().setAdminMenuAccessTicket(tkt);
			} else {
				XMSystemUtil.getInstance().setTicket(tkt);
			}

			if (statusCode.is2xxSuccessful()) {
				return responseEntity.getBody();
			} else {
				LOGGER.error("Error while calling authorizeLogin REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RuntimeException e) {
			LOGGER.error("Server not reachable :" + url); //$NON-NLS-1$
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				} else if (statusCode.compareTo(HttpStatus.NOT_FOUND) == 0) {
					LOGGER.error("Incorrect server URI in xmsystem.properties file :" + url); //$NON-NLS-1$
					throw new HttpClientErrorException(HttpStatus.NOT_FOUND, "Invalid resource access");
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			} else if (e instanceof ResourceAccessException) {
				throw e;
			} else if (e instanceof IllegalArgumentException) {
				if (e.getMessage().equals("URI is not absolute")) {
					LOGGER.error("Incorrect server URI in xmsystem.properties file :" + url); //$NON-NLS-1$
					throw new IllegalArgumentException("URI is not absolute");
				} else {
					LOGGER.error("Error while calling authorizeLogin REST Service!", e); //$NON-NLS-1$
				}
			}
		} catch (Exception e) {
			LOGGER.error("Server not reachable :" + url); //$NON-NLS-1$
			LOGGER.error("Error while calling authorizeLogin REST Service!", e); //$NON-NLS-1$
		}
		return null;
	}
}
