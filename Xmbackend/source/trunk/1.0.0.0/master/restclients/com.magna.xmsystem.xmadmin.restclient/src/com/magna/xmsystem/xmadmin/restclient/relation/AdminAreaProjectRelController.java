package com.magna.xmsystem.xmadmin.restclient.relation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.magna.xmbackend.entities.AdminAreaProjectRelTbl;
import com.magna.xmbackend.entities.ProjectsTbl;
import com.magna.xmbackend.exception.CannotCreateObjectException;
import com.magna.xmbackend.response.rel.adminareaproject.AdminAreaProjectRelResponseWrapper;
import com.magna.xmbackend.response.rel.adminareaproject.AdminAreaProjectRelationResponse;
import com.magna.xmbackend.response.rel.adminareaproject.AdminAreaProjectResponse;
import com.magna.xmbackend.response.rel.adminareaproject.AdminAreaProjectResponseWrapper;
import com.magna.xmbackend.vo.rel.AdminAreaProjectRelBatchRequest;
import com.magna.xmbackend.vo.rel.AdminAreaProjectRelBatchResponse;
import com.magna.xmbackend.vo.rel.AdminAreaProjectRelIdWithProjectAppRel;
import com.magna.xmbackend.vo.rel.AdminAreaProjectRelIdWithProjectAppRelResponse;
import com.magna.xmbackend.vo.rel.AdminAreaProjectRelIdWithProjectStartAppRel;
import com.magna.xmbackend.vo.rel.AdminAreaProjectRelIdWithProjectStartAppRelResponse;
import com.magna.xmbackend.vo.rel.AdminAreaProjectRelRequest;
import com.magna.xmbackend.vo.rel.AdminAreaProjectRelResponse;
import com.magna.xmsystem.dependencies.customExceptions.UnauthorizedAccessException;
import com.magna.xmsystem.xmadmin.restclient.RestController;
import com.magna.xmsystem.xmadmin.restclient.util.RestClientExpHandlerUtil;
import com.magna.xmsystem.xmadmin.restclient.util.RestClientSortingUtil;
import com.magna.xmsystem.xmadmin.restclient.util.XMAdminRestClientConstants;

/**
 * Class for Site admin area rel controller.
 *
 * @author Roshan.Ekka
 */
public class AdminAreaProjectRelController extends RestController {
	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(AdminAreaProjectRelController.class);

	/**
	 * Constructor for SiteController Class.
	 */
	public AdminAreaProjectRelController() {
		super();
	}

	/**
	 * Constructor for AdminAreaProjectRelController Class.
	 *
	 * @param adminAreaId
	 *            {@link String}
	 */
	public AdminAreaProjectRelController(String adminAreaId) {
		super(adminAreaId);
	}

	/**
	 * Method for Creates the admin area project rel.
	 *
	 * @param adminAreaProjectRelRequest
	 *            {@link AdminAreaProjectRelRequest}
	 * @return the admin area project rel tbl {@link AdminAreaProjectRelTbl}
	 */
	public AdminAreaProjectRelTbl createAdminAreaProjectRel(final AdminAreaProjectRelRequest adminAreaProjectRelRequest)
			throws UnauthorizedAccessException, CannotCreateObjectException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.CREATE_ADMIN_AREA_PROJECT_REL);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<AdminAreaProjectRelRequest> request = new HttpEntity<AdminAreaProjectRelRequest>(
					adminAreaProjectRelRequest, this.headers);
			ResponseEntity<AdminAreaProjectRelTbl> responseEntity = restTemplate.postForEntity(url, request,
					AdminAreaProjectRelTbl.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				AdminAreaProjectRelTbl adminAreaProjectRelTbl = responseEntity.getBody();
				return adminAreaProjectRelTbl;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Create Administration Area Project Relation REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				String errorResponse = ((HttpStatusCodeException) e).getResponseBodyAsString();
				String errCode = RestClientExpHandlerUtil.handleCannotCreateObjectException(LOGGER,
						(HttpStatusCodeException) e);
				throw new CannotCreateObjectException(errorResponse, errCode);
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Create Administration Area Project Relation REST Service", e); //$NON-NLS-1$
		}
		return null;
	}

	/**
	 * Method for Creates the admin area project rel.
	 *
	 * @param adminAreaProjectRelBatchRequest
	 *            {@link AdminAreaProjectRelBatchRequest}
	 * @return the admin area project rel batch response
	 *         {@link AdminAreaProjectRelBatchResponse}
	 * @throws UnauthorizedAccessException
	 *             the unauthorized access exception
	 * @throws CannotCreateObjectException
	 *             the cannot create object exception
	 */
	public AdminAreaProjectRelBatchResponse createAdminAreaProjectRel(
			final AdminAreaProjectRelBatchRequest adminAreaProjectRelBatchRequest)
			throws UnauthorizedAccessException, CannotCreateObjectException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.CREATE_MULTI_PROJECT_ADMIN_AREA_REL);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<AdminAreaProjectRelBatchRequest> request = new HttpEntity<AdminAreaProjectRelBatchRequest>(
					adminAreaProjectRelBatchRequest, this.headers);
			ResponseEntity<AdminAreaProjectRelBatchResponse> responseEntity = restTemplate.postForEntity(url, request,
					AdminAreaProjectRelBatchResponse.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				AdminAreaProjectRelBatchResponse adminAreaProjectRelBatchResponse = responseEntity.getBody();
				return adminAreaProjectRelBatchResponse;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Create Multi Administration Area Project Relation REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				String errorResponse = ((HttpStatusCodeException) e).getResponseBodyAsString();
				String errCode = RestClientExpHandlerUtil.handleCannotCreateObjectException(LOGGER,
						(HttpStatusCodeException) e);
				throw new CannotCreateObjectException(errorResponse, errCode);
			} else if (e instanceof RestClientException) {
				throw e;
			} else if (e instanceof ResourceAccessException) {
				throw e;
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Create Multi Administration Area Project Relation REST Service", //$NON-NLS-1$
					e);
		}
		return null;
	}

	/**
	 * Gets the all admin area project rel.
	 *
	 * @param isSorted
	 *            {@link boolean}
	 * @return the all admin area project rel
	 */
	public List<AdminAreaProjectRelTbl> getAllAdminAreaProjectRel(final boolean isSorted)
			throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.GET_ADMIN_AREA_PROJECT_REL);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<AdminAreaProjectRelResponse> responseList = restTemplate.exchange(url, HttpMethod.GET,
					requestEntity, AdminAreaProjectRelResponse.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				AdminAreaProjectRelResponse siteResponseObject = responseList.getBody();
				List<AdminAreaProjectRelTbl> siteTblList = (List<AdminAreaProjectRelTbl>) siteResponseObject
						.getAdminAreaProjectRelTbls();
				/*
				 * if (isSorted) { siteTblList.sort((siteResponse1,
				 * siteResponse2) -> {
				 * Collection<com.magna.xmbackend.entities.SiteTranslationTbl>
				 * siteTranslation1;
				 * Collection<com.magna.xmbackend.entities.SiteTranslationTbl>
				 * siteTranslation2; if ((siteTranslation1 =
				 * siteResponse1.getSiteTranslationTblCollection()) != null &&
				 * (siteTranslation2 =
				 * siteResponse2.getSiteTranslationTblCollection()) != null) {
				 * String siteName1 = getNameByLanguage(LANG_ENUM.ENGLISH,
				 * siteTranslation1); String siteName2 =
				 * getNameByLanguage(LANG_ENUM.ENGLISH, siteTranslation2);
				 * return siteName1.compareToIgnoreCase(siteName2); } return 0;
				 * }); }
				 */
				return siteTblList;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Get All Administration Area Project Relation REST Service, returns the status code: " //$NON-NLS-1$
								+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get All Administration Area Project Relation REST Service", e); //$NON-NLS-1$
		}
		return null;
	}

	/*
	 * private String getNameByLanguage(final LANG_ENUM langEnum, final
	 * Collection<SiteTranslationTbl> siteTranslations) { for
	 * (SiteTranslationTbl siteTranslation : siteTranslations) { LANG_ENUM
	 * foundLangEnum =
	 * LANG_ENUM.getLangEnum(siteTranslation.getLanguageCode().getLanguageCode()
	 * ); String name; if (foundLangEnum == langEnum && (name =
	 * siteTranslation.getName()) != null) { if (!XMSystemUtil.isEmpty(name)) {
	 * return name; } } } return ""; }
	 */

	/**
	 * Method for Delete admin area project rel.
	 *
	 * @param adminAreaProjRelId
	 *            {@link String}
	 * @return true, if successful
	 * @throws UnauthorizedAccessException
	 *             the unauthorized access exception
	 */
	public boolean deleteAdminAreaProjectRel(String adminAreaProjRelId) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.DELETE_ADMIN_AREA_PROJECT_REL + "/"
					+ adminAreaProjRelId);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(this.headers);
			ResponseEntity<Boolean> responseEntity = restTemplate.exchange(url, HttpMethod.DELETE, requestEntity,
					Boolean.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				return responseEntity.getBody();
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM delete Administration Area Project Relation REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM delete Administration Area Project Relation REST Service!", e); //$NON-NLS-1$
		}
		return false;
	}

	/**
	 * Delete multi admin area project rel.
	 *
	 * @param ids the ids
	 * @return the admin area project rel response
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 */
	public AdminAreaProjectRelResponse deleteMultiAdminAreaProjectRel(Set<String> ids)
			throws UnauthorizedAccessException {
		try {
			String url = new String(
					this.serviceUrl + XMAdminRestClientConstants.DELETE_MULTI_ADMIN_AREA_PROJECT_REL);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Set<String>> requestEntity = new HttpEntity<Set<String>>(ids, this.headers);
			ResponseEntity<AdminAreaProjectRelResponse> responseEntity = restTemplate.exchange(url, HttpMethod.DELETE,
					requestEntity, AdminAreaProjectRelResponse.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				AdminAreaProjectRelResponse adminAreaProjectRelResponse = responseEntity.getBody();
				return adminAreaProjectRelResponse;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM delete multi AdminArea Project Relation REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM delete multi AdminArea Project Relation REST Service!", e); //$NON-NLS-1$
		}
		return null;
	}

	/**
	 * Method for Update admin area project rel status.
	 *
	 * @param adminAreaProjRelId
	 *            {@link String}
	 * @param status
	 *            {@link String}
	 * @return true, if successful
	 */
	public boolean updateAdminAreaProjectRelStatus(String adminAreaProjRelId, String status)
			throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.UPDATE_ADMIN_AREA_PROJECT_REL_STATUS
					+ "/" + status + "/" + adminAreaProjRelId);

			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(this.headers);
			ResponseEntity<Boolean> responseEntity = restTemplate.exchange(url, HttpMethod.POST, requestEntity,
					Boolean.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				return responseEntity.getBody();
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Update Administration Area Project Relation Status REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			}
		} catch (Exception e) {
			LOGGER.error(
					"Error while calling XMSYSTEM Update Administration Area Project Relation Status REST Service!", e); //$NON-NLS-1$
		}
		return false;
	}

	/**
	 * Multi update admin area project rel status.
	 *
	 * @param adminAreaProjectRelRequest the admin area proj app requests
	 * @return the admin area project app rel batch response
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 */
	public AdminAreaProjectResponseWrapper multiUpdateAdminAreaProjectRelStatus(
			List<AdminAreaProjectRelRequest> adminAreaProjectRelRequest) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.MULTI_UPDATE_AA_PROJECT_STATUS);

			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<List<AdminAreaProjectRelRequest>> requestEntity = new HttpEntity<List<AdminAreaProjectRelRequest>>(
					adminAreaProjectRelRequest, this.headers);
			ResponseEntity<AdminAreaProjectResponseWrapper> responseEntity = restTemplate.exchange(url,
					HttpMethod.POST, requestEntity, AdminAreaProjectResponseWrapper.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				return responseEntity.getBody();
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Multi Update AdminAreaProject Status REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Multi Update AdminAreaProject Status REST Service!", e); //$NON-NLS-1$
		}
		return null;
	}
	/**
	 * Gets the all project apps by site admin proj rel.
	 *
	 * @param siteAdminProjRelId
	 *            {@link String}
	 * @return the all project apps by site admin proj rel
	 */
	public List<AdminAreaProjectRelIdWithProjectAppRel> getAllProjectAppsBySiteAdminProjRel(
			final String siteAdminProjRelId) throws UnauthorizedAccessException {
		try {
			final String url = new String(this.serviceUrl
					+ XMAdminRestClientConstants.GET_PROJECT_APPS_BY_SITE_ADMIN_PROJ_REL_ID + "/" + siteAdminProjRelId);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<AdminAreaProjectRelIdWithProjectAppRelResponse> responseList = restTemplate.exchange(url,
					HttpMethod.GET, requestEntity, AdminAreaProjectRelIdWithProjectAppRelResponse.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				AdminAreaProjectRelIdWithProjectAppRelResponse projectAppResponseObject = responseList.getBody();
				List<AdminAreaProjectRelIdWithProjectAppRel> adminAreaProjectRelIdWithProjectAppRels = projectAppResponseObject
						.getAdminAreaProjectRelIdWithProjectAppRels();

				return RestClientSortingUtil.sortAllProjectAppsBySiteAdminProjRelTbl(adminAreaProjectRelIdWithProjectAppRels);
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Get All Project Applications By siteAdminProjRelId REST Service, returns the status code: " //$NON-NLS-1$
								+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			} else if(e instanceof ResourceAccessException){
				throw e;
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get All Project Applications By siteAdminProjRelId REST Service", //$NON-NLS-1$
					e);
		}
		return null;
	}

	/**
	 * Gets the all project apps by site admin proj rel.
	 *
	 * @param siteAdminProjRelId
	 *            {@link String}
	 * @param relationType
	 *            {@link String}
	 * @return the all project apps by site admin proj rel
	 */
	public List<AdminAreaProjectRelIdWithProjectAppRel> getAllProjectAppsBySiteAdminProjRel(
			final String siteAdminProjRelId, final String relationType) throws UnauthorizedAccessException {
		try {
			String url = new String(
					this.serviceUrl + XMAdminRestClientConstants.GET_PROJECT_APPS_BY_SITE_ADMIN_PROJ_REL_ID + "/"
							+ siteAdminProjRelId + "/" + relationType);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<AdminAreaProjectRelIdWithProjectAppRelResponse> responseList = restTemplate.exchange(url,
					HttpMethod.GET, requestEntity, AdminAreaProjectRelIdWithProjectAppRelResponse.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				AdminAreaProjectRelIdWithProjectAppRelResponse projectAppResponseObject = responseList.getBody();
				List<AdminAreaProjectRelIdWithProjectAppRel> adminAreaProjectRelIdWithProjectAppRels = projectAppResponseObject
						.getAdminAreaProjectRelIdWithProjectAppRels();
				
				RestClientSortingUtil.sortAllProjectAppsBySiteAdminProjRelTbl(adminAreaProjectRelIdWithProjectAppRels);
			 return adminAreaProjectRelIdWithProjectAppRels;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Get All Project Applications By siteAdminProjRelId REST Service, returns the status code: " //$NON-NLS-1$
								+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			} else if(e instanceof ResourceAccessException){
				throw e;
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get All Project Applications By siteAdminProjRelId REST Service", //$NON-NLS-1$
					e);
		}
		return null;
	}

	/**
	 * Gets the all start apps by site admin proj rel.
	 *
	 * @param siteAdminProjRelId
	 *            {@link String}
	 * @return the all start apps by site admin proj rel
	 */
	public List<AdminAreaProjectRelIdWithProjectStartAppRel> getAllStartAppsBySiteAdminProjRel(
			final String siteAdminProjRelId) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl
					+ XMAdminRestClientConstants.GET_START_APPS_BY_SITE_ADMIN_PROJ_REL_ID + "/" + siteAdminProjRelId);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<AdminAreaProjectRelIdWithProjectStartAppRelResponse> responseList = restTemplate.exchange(
					url, HttpMethod.GET, requestEntity, AdminAreaProjectRelIdWithProjectStartAppRelResponse.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				AdminAreaProjectRelIdWithProjectStartAppRelResponse startAppResponseObject = responseList.getBody();
				List<AdminAreaProjectRelIdWithProjectStartAppRel> adminAreaProjectRelIdWithProjectStartAppRels = startAppResponseObject
						.getAdminAreaProjectRelIdWithProjectStartAppRels();
				
				return RestClientSortingUtil.sortAllStartAppsBySiteAdminRelTbl(adminAreaProjectRelIdWithProjectStartAppRels);
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Get All Start Applications By siteAdminProjRelId REST Service, returns the status code: " //$NON-NLS-1$
								+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			} else if(e instanceof ResourceAccessException){
				throw e;
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get All Start Applications By siteAdminProjRelId REST Service", //$NON-NLS-1$
					e);
		}
		return null;
	}

	/**
	 * Gets the AA project by id.
	 *
	 * @param adminAreaProjRelId
	 *            {@link String}
	 * @return the AA project by id
	 */
	public AdminAreaProjectRelTbl getAAProjectById(String adminAreaProjRelId) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.GET_ADMIN_AREA_PROJECT_REL_BY_ID + "/"
					+ adminAreaProjRelId);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<AdminAreaProjectRelTbl> responseList = restTemplate.exchange(url, HttpMethod.GET,
					requestEntity, AdminAreaProjectRelTbl.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				AdminAreaProjectRelTbl adminAreaProjRelTblObj = responseList.getBody();
				return adminAreaProjRelTblObj;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Get Administration Area Project Relation by Id REST Service, returns the status code: " //$NON-NLS-1$
								+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get Administration Area Project Relation by Id REST Service", e); //$NON-NLS-1$
		}
		return null;
	}

	/**
	 * Gets the AA project rel by project id.
	 *
	 * @param projectId
	 *            {@link String}
	 * @return the AA project rel by project id
	 */
	public AdminAreaProjectResponseWrapper getAAProjectRelByProjectId(final String projectId)
			throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl
					+ XMAdminRestClientConstants.GET_ADMIN_AREA_PROJECT_REL_BY_PROJECT_ID + "/" + projectId);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<AdminAreaProjectResponseWrapper> responseList = restTemplate.exchange(url, HttpMethod.GET,
					requestEntity, AdminAreaProjectResponseWrapper.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				AdminAreaProjectResponseWrapper adminAreaProjectResponseWrapper = responseList.getBody();
		    List<AdminAreaProjectResponse> adminAreaProjectAppResponse = adminAreaProjectResponseWrapper.getAdminAreaProjectResponses();
			RestClientSortingUtil.sortAAProjectRelByProjectIdTbl(adminAreaProjectAppResponse);
			return adminAreaProjectResponseWrapper;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Get Administration Area Project Relation by projectId REST Service, returns the status code: " //$NON-NLS-1$
								+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			} else if(e instanceof ResourceAccessException){
				throw e;
			}
		} catch (Exception e) {
			LOGGER.error(
					"Error while calling XMSYSTEM Get Administration Area Project Relation by projectId REST Service", //$NON-NLS-1$
					e);
		}
		return null;
	}

	public List<AdminAreaProjectRelationResponse> getAdminAreaProjectRelBySiteAAIdProjectAppId(final String siteAdminAreaId,
			final String projectAppId) throws UnauthorizedAccessException {
		try {
			String url = new String(
					this.serviceUrl + XMAdminRestClientConstants.GET_ADMIN_AREA_PROJ_REL_BY_SITE_AA_ID_PROJECT_ID + "/"
							+ siteAdminAreaId + "/" + projectAppId);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<AdminAreaProjectRelResponseWrapper> responseList = restTemplate.exchange(url, HttpMethod.GET,
					requestEntity, AdminAreaProjectRelResponseWrapper.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				AdminAreaProjectRelResponseWrapper adminAreaProjectResponseWrapper = responseList.getBody();
				List<AdminAreaProjectRelationResponse> adminAreaProjRelResps = adminAreaProjectResponseWrapper.getAdminAreaProjRelResps();
				Collections.sort(adminAreaProjRelResps, new Comparator<AdminAreaProjectRelationResponse>() {

					@Override
					public int compare(AdminAreaProjectRelationResponse o1, AdminAreaProjectRelationResponse o2) {
						ProjectsTbl projectsTbl1 = o1.getProjectId();
						ProjectsTbl projectsTbl2 = o2.getProjectId();
						return RestClientSortingUtil.compareProjectByName(projectsTbl1, projectsTbl2);
					}
				});
				return adminAreaProjRelResps;
			} else {
				LOGGER.error("Error while calling XMSYSTEM Get Administration Area Project Relation by siteAdminAreaId and projectId REST Service, returns the status code: " //$NON-NLS-1$
								+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			} else if(e instanceof ResourceAccessException){
				throw e;
			}
		} catch (Exception e) {
			LOGGER.error(
					"Error while calling XMSYSTEM Get Administration Area Project Relation by by siteAdminAreaId and projectId REST Service", //$NON-NLS-1$
					e);
		}
		return null;
	}

	/**
	 * Gets the AA project rel by site admin area id and project id.
	 *
	 * @param siteAdminAreaRelId
	 *            the site admin area rel id
	 * @param projectId
	 *            the project id
	 * @return the AA project rel by site admin area id and project id
	 * @throws UnauthorizedAccessException
	 *             the unauthorized access exception
	 */
	public AdminAreaProjectRelTbl getAAProjectRelBySiteAdminAreaIdAndProjectId(final String siteAdminAreaRelId,
			final String projectId) throws UnauthorizedAccessException {
		try {
			String url = new String(
					this.serviceUrl + XMAdminRestClientConstants.GET_ADMIN_AREA_PROJECT_REL_BY_SITE_AA_REL_ID_PROJECT_ID
							+ "/" + siteAdminAreaRelId + "/" + projectId);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<AdminAreaProjectRelTbl> responseList = restTemplate.exchange(url, HttpMethod.GET,
					requestEntity, AdminAreaProjectRelTbl.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				AdminAreaProjectRelTbl adminAreaProjectRelTbl = responseList.getBody();
				return adminAreaProjectRelTbl;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Get Administration Area Project Relation by site admin area id and projectId REST Service, returns the status code: " //$NON-NLS-1$
								+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			}
		} catch (Exception e) {
			LOGGER.error(
					"Error while calling XMSYSTEM Get Administration Area Project Relation by site admin area id and projectId REST Service", //$NON-NLS-1$
					e);
		}
		return null;
	}
}
