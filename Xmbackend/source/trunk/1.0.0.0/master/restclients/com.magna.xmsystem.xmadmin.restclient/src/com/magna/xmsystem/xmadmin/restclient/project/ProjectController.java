package com.magna.xmsystem.xmadmin.restclient.project;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.magna.xmbackend.entities.ProjectsTbl;
import com.magna.xmbackend.exception.CannotCreateObjectException;
import com.magna.xmbackend.vo.project.ProjectRequest;
import com.magna.xmbackend.vo.project.ProjectResponse;
import com.magna.xmsystem.dependencies.customExceptions.UnauthorizedAccessException;
import com.magna.xmsystem.xmadmin.restclient.RestController;
import com.magna.xmsystem.xmadmin.restclient.util.RestClientExpHandlerUtil;
import com.magna.xmsystem.xmadmin.restclient.util.RestClientSortingUtil;
import com.magna.xmsystem.xmadmin.restclient.util.XMAdminRestClientConstants;

/**
 * Controller class for Project model which exchange data with REST Service.
 * 
 * @author subash.janarthanan
 *
 */
public class ProjectController extends RestController {

	private static final Logger LOGGER = LoggerFactory.getLogger(ProjectController.class);

	/**
	 * Constructor for ProjectController Class.
	 */

	public ProjectController() {
		super();
	}

	/**
	 * Method for Creates the project.
	 *
	 * @param projectRequest
	 *            {@link ProjectRequest}
	 * @return the projects tbl {@link ProjectsTbl}
	 */
	public ProjectsTbl createProject(final ProjectRequest projectRequest) throws UnauthorizedAccessException, CannotCreateObjectException {
		try {
			final String url = new String(this.serviceUrl + XMAdminRestClientConstants.CREATE_PROJECT);
			final RestTemplate restTemplate = new RestTemplate();
			HttpEntity<ProjectRequest> request = new HttpEntity<ProjectRequest>(projectRequest, this.headers);
			ResponseEntity<ProjectsTbl> responseEntity = restTemplate.postForEntity(url, request, ProjectsTbl.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				ProjectsTbl projectTbl = responseEntity.getBody();
				return projectTbl;
			} else {
				LOGGER.error("Error while calling XMSYSTEM Create Project REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				String errorResponse = ((HttpStatusCodeException) e).getResponseBodyAsString();
				String errCode = RestClientExpHandlerUtil.handleCannotCreateObjectException(LOGGER, (HttpStatusCodeException) e);
				throw new CannotCreateObjectException(errorResponse, errCode);
			}else if (e instanceof ResourceAccessException) {
				throw e;
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Create Project REST Service", e); //$NON-NLS-1$
		}
		return null;
	}

	/**
	 * Gets the all projects.
	 *
	 * @param isSorted
	 *            {@link boolean}
	 * @return the all projects
	 */
	public List<ProjectsTbl> getAllProjects(final boolean isSorted) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.GET_PROJECTS);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<ProjectResponse> responseList = restTemplate.exchange(url, HttpMethod.GET, requestEntity,
					ProjectResponse.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				ProjectResponse projectResponseObject = responseList.getBody();
				List<ProjectsTbl> projectTblList = (List<ProjectsTbl>) projectResponseObject.getProjectsTbls();
				return RestClientSortingUtil.sortProjectTbl(projectTblList);
			} else {
				LOGGER.error("Error while calling XMSYSTEM Get All Projects REST Service, returns the status code: " //$NON-NLS-1$
						+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			} else if(e instanceof ResourceAccessException){
				throw e;
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get All Projects REST Service", e); //$NON-NLS-1$
		}
		return null;
	}

	/**
	 * Method for Delete Project.
	 *
	 * @param projectId
	 *            {@link String}
	 * @return true, if successful
	 */
	public boolean deleteProject(String projectId) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.DELETE_PROJECT + "/" + projectId);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(this.headers);
			ResponseEntity<Boolean> responseEntity = restTemplate.exchange(url, HttpMethod.POST, requestEntity,
					Boolean.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				return responseEntity.getBody();
			} else {
				LOGGER.error("Error while calling XMSYSTEM delete Project REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM delete Project REST Service!", e); //$NON-NLS-1$
		}
		return false;
	}
	
	/**
	 * Delete multi project.
	 *
	 * @param projectIds the project ids
	 * @return the project response
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 */
	public ProjectResponse deleteMultiProject(Set<String> projectIds) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.DELETE_MULTI_PROJECT);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Set<String>> requestEntity = new HttpEntity<Set<String>>(projectIds,this.headers);
			ResponseEntity<ProjectResponse> responseEntity = restTemplate.exchange(url, HttpMethod.POST, requestEntity,
					ProjectResponse.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				ProjectResponse projectResponse = responseEntity.getBody();
				return projectResponse;
			} else {
				LOGGER.error("Error while calling XMSYSTEM delete Multi Project REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM delete Multi Project REST Service!", e); //$NON-NLS-1$
		}
		return null;
	}

	/**
	 * Method for Update project.
	 *
	 * @param projectRequest
	 *            {@link ProjectRequest}
	 * @return true, if successful
	 */
	public boolean updateProject(ProjectRequest projectRequest)
			throws UnauthorizedAccessException, CannotCreateObjectException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.UPDATE_PROJECT);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<ProjectRequest> request = new HttpEntity<ProjectRequest>(projectRequest, this.headers);
			ResponseEntity<ProjectsTbl> responseEntity = restTemplate.postForEntity(url, request, ProjectsTbl.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				return true;
			} else {
				LOGGER.error("Error while calling XMSYSTEM Upadte Project REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				String errorResponse = ((HttpStatusCodeException) e).getResponseBodyAsString();
				String errCode = RestClientExpHandlerUtil.handleCannotCreateObjectException(LOGGER,
						(HttpStatusCodeException) e);
				throw new CannotCreateObjectException(errorResponse, errCode);
			}else if (e instanceof ResourceAccessException) {
				throw e;
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Upadte Project REST Service", e); //$NON-NLS-1$
		}
		return false;
	}

	/**
	 * Method for Update project status.
	 *
	 * @param projectId
	 *            {@link String}
	 * @param status
	 *            {@link String}
	 * @return true, if successful
	 */
	public boolean updateProjectStatus(String projectId, String status) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.UPDATE_PROJECT_STATUS + "/" + status
					+ "/" + projectId);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(this.headers);
			ResponseEntity<Boolean> responseEntity = restTemplate.exchange(url, HttpMethod.POST, requestEntity,
					Boolean.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				return responseEntity.getBody();
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Update Project Status REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Update Project Status REST Service!", e); //$NON-NLS-1$
		}
		return false;
	}

	/**
	 * Multi update project status.
	 *
	 * @param projectRequest the project request
	 * @return the project response
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 */
	public ProjectResponse multiUpdateProjectStatus(List<ProjectRequest> projectRequest)
			throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.MULTI_UPDATE_PROJECT_STATUS);

			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<List<ProjectRequest>> requestEntity = new HttpEntity<List<ProjectRequest>>(projectRequest,
					this.headers);
			ResponseEntity<ProjectResponse> responseEntity = restTemplate.exchange(url, HttpMethod.POST, requestEntity,
					ProjectResponse.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				return responseEntity.getBody();
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Multi Update project Status REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Multi Update project Status REST Service!", e); //$NON-NLS-1$
		}
		return null;
	}
	/**
	 * Gets the projects by id.
	 *
	 * @param projectId
	 *            {@link String}
	 * @return the projects by id
	 */
	public ProjectsTbl getProjectsById(String projectId) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.GET_PROJECT_BY_ID + "/" + projectId);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<ProjectsTbl> responseList = restTemplate.exchange(url, HttpMethod.GET, requestEntity,
					ProjectsTbl.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				ProjectsTbl projectTblObj = responseList.getBody();
				return projectTblObj;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Get All Projects by Id REST Service, returns the status code: " //$NON-NLS-1$
								+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get All Projects by Id REST Service", e); //$NON-NLS-1$
		}
		return null;
	}

	/**
	 * Gets the projects by project app site AA id.
	 *
	 * @param siteId
	 *            {@link String}
	 * @param adminAreaId
	 *            {@link String}
	 * @param projectAppId
	 *            {@link String}
	 * @return the projects by project app site AA id
	 */
	public ProjectResponse getProjectsByProjectAppSiteAAId(String siteId, String adminAreaId, String projectAppId)
			throws UnauthorizedAccessException {
		try {
			String url = new String(
					this.serviceUrl + XMAdminRestClientConstants.GET_PROJECT_BY_PROJECT_APP_SITE_ADMINAREA_ID + "/"
							+ siteId + "/" + adminAreaId + "/" + projectAppId);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<ProjectResponse> responseList = restTemplate.exchange(url, HttpMethod.GET, requestEntity,
					ProjectResponse.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				ProjectResponse projectTblObj = responseList.getBody();
				return projectTblObj;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Get All Projects by Id REST Service, returns the status code: " //$NON-NLS-1$
								+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get All Projects by Id REST Service", e); //$NON-NLS-1$
		}
		return null;
	}

	/**
	 * Gets the projects by start app id.
	 *
	 * @param startAppId
	 *            {@link String}
	 * @return the projects by start app id
	 */
	public List<ProjectsTbl> getProjectsByStartAppId(String startAppId) throws UnauthorizedAccessException {
		try {
			String url = new String(
					this.serviceUrl + XMAdminRestClientConstants.GET_PROJECT_BY_START_APP_ID + "/" + startAppId);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);
			List<ProjectsTbl> projectTblList = new ArrayList<>();
			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<ProjectResponse> responseList = restTemplate.exchange(url, HttpMethod.GET, requestEntity,
					ProjectResponse.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				ProjectResponse projectResponse = responseList.getBody();
				Iterable<ProjectsTbl> projectsTbls = projectResponse.getProjectsTbls();
				for (ProjectsTbl projectsTbl : projectsTbls) {
					projectTblList.add(projectsTbl);
				}
				return RestClientSortingUtil.sortProjectTbl(projectTblList);
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Get All Projects by StartAppId REST Service, returns the status code: " //$NON-NLS-1$
								+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			} else if(e instanceof ResourceAccessException){
				throw e;
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get All Projects by StartAppId REST Service", e); //$NON-NLS-1$
		}
		return null;
	}

	/**
	 * Gets the projects by user id.
	 *
	 * @param userId
	 *            {@link String}
	 * @return the projects by user id
	 */
	public ProjectResponse getProjectsByUserId(String userId) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.GET_PROJECT_BY_USER_ID + "/" + userId);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<ProjectResponse> responseList = restTemplate.exchange(url, HttpMethod.GET, requestEntity,
					ProjectResponse.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				ProjectResponse projectResponse = responseList.getBody();
				return projectResponse;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Get All Projects by UserId REST Service, returns the status code: " //$NON-NLS-1$
								+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get All Projects by UserId REST Service", e); //$NON-NLS-1$
		}
		return null;
	}

	/**
	 * Gets the projects by admin area id.
	 *
	 * @param adminAreaId
	 *            {@link String}
	 * @return the projects by admin area id
	 * @throws UnauthorizedAccessException
	 */
	public List<ProjectsTbl> getProjectsByAdminAreaId(String adminAreaId) throws UnauthorizedAccessException {
		try {
			String url = new String(
					this.serviceUrl + XMAdminRestClientConstants.GET_PROJECT_BY_ADMIN_AREA_ID + "/" + adminAreaId);
			List<ProjectsTbl> projectTblList = new ArrayList<>();
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<ProjectResponse> responseList = restTemplate.exchange(url, HttpMethod.GET, requestEntity,
					ProjectResponse.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				ProjectResponse projectResponse = responseList.getBody();
				Iterable<ProjectsTbl> projectsTbls = projectResponse.getProjectsTbls();
				for (ProjectsTbl projectsTbl : projectsTbls) {
					projectTblList.add(projectsTbl);
				}
				RestClientSortingUtil.sortProjectTbl(projectTblList);
				return projectTblList;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Get All Projects by UserId REST Service, returns the status code: " //$NON-NLS-1$
								+ statusCode.value());
			}
		}  catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get All Projects by UserId REST Service", e); //$NON-NLS-1$
		}
		return null;
	}
	
	/**
	 * Gets the user projects by site id.
	 *
	 * @param siteId {@link String}
	 * @return the user projects by site id
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 */
	public List<ProjectsTbl> getUserProjectsBySiteId(String siteId) throws UnauthorizedAccessException {
		try {
			String url = new String(
					this.serviceUrl + XMAdminRestClientConstants.GET_USER_PROJECTS_BY_SITE_ID + "/" + siteId);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<ProjectResponse> responseList = restTemplate.exchange(url, HttpMethod.GET, requestEntity,
					ProjectResponse.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				ProjectResponse projectResponse = responseList.getBody();
				if(projectResponse != null) {
					Iterable<ProjectsTbl> projectsTbls = projectResponse.getProjectsTbls();
					if(projectsTbls != null) {
						List<ProjectsTbl> projectTblList = (List<ProjectsTbl>) projectsTbls;
						return RestClientSortingUtil.sortProjectTbl(projectTblList);
					}
				}
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Get User Projects By Site Id REST Service, returns the status code: " //$NON-NLS-1$
								+ statusCode.value());
			}
		}  catch (RuntimeException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				} else if (statusCode.compareTo(HttpStatus.NOT_FOUND) == 0) {
					LOGGER.error("Incorrect server URI in xmsystem.properties file"); //$NON-NLS-1$
					throw new ResourceAccessException("Invalid resource access");
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			} else if (e instanceof ResourceAccessException) {
				throw e;
			} else if(e instanceof IllegalArgumentException) {
				throw new IllegalArgumentException();
			} 
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get User Projects By Site Id REST Service", e); //$NON-NLS-1$
		}
		return null;
	}
	
	/**
	 * Gets the projects by site id rel.
	 *
	 * @param siteId the site id
	 * @return the projects by site id rel
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 */
	public ProjectResponse getProjectsBySiteIdRel(final String siteId) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.GET_PROJECT_BY_SITE_RELATION + "/" + siteId);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<ProjectResponse> responseList = restTemplate.exchange(url, HttpMethod.GET, requestEntity, ProjectResponse.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				ProjectResponse projectResponse = responseList.getBody();
				return projectResponse;
			} else {
				LOGGER.error("Error while calling XMSYSTEM Get Projects by SiteId Rel REST Service, returns the status code: " //$NON-NLS-1$
						+ statusCode.value());
			} 
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get Projects by SiteId Rel REST Service", e); //$NON-NLS-1$
		}
		return null;
	}
	
	/**
	 * Gets the projects by AA id rel.
	 *
	 * @param aaId the aa id
	 * @return the projects by AA id rel
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 */
	public ProjectResponse getProjectsByAAIdRel(final String aaId) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.GET_PROJECT_BY_AA_RELATION + "/" + aaId);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<ProjectResponse> responseList = restTemplate.exchange(url, HttpMethod.GET, requestEntity,
					ProjectResponse.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				ProjectResponse projectResponse = responseList.getBody();
				return projectResponse;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Get All Projects by AA Id Rel REST Service, returns the status code: " //$NON-NLS-1$
								+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get All Projects by AA Id Rel UserId REST Service", e); //$NON-NLS-1$
		}
		return null;
	}
	
	/**
	 * Gets the projects by user id rel.
	 *
	 * @param aaId the aa id
	 * @return the projects by user id rel
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 */
	public ProjectResponse getProjectsByUserIdRel(final String userId, final String siteId) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.GET_PROJECT_BY_USER_SITE_RELATION + "/" + userId + "/" + siteId);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<ProjectResponse> responseList = restTemplate.exchange(url, HttpMethod.GET, requestEntity,
					ProjectResponse.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				ProjectResponse projectResponse = responseList.getBody();
				return projectResponse;
			} else {
				LOGGER.error("Error while calling XMSYSTEM Get All Projects by User Id Rel REST Service, returns the status code: " //$NON-NLS-1$
								+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get All Projects by User Id Rel UserId REST Service", e); //$NON-NLS-1$
		}
		return null;
	}
	
	/**
	 * Gets the project by name.
	 *
	 * @param projectName the project name
	 * @return the project by name
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 */
	public ProjectsTbl getProjectByName(final String projectName) throws UnauthorizedAccessException{
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.GET_PROJECT_BY_NAME + "/" + projectName);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<ProjectsTbl> responseList = restTemplate.exchange(url, HttpMethod.GET, requestEntity,
					ProjectsTbl.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				ProjectsTbl projectsTbl = responseList.getBody();
				return projectsTbl;
			} else {
				LOGGER.error("Error while calling XMSYSTEM Get Project By project name REST Service, returns the status code: " //$NON-NLS-1$
						+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get Project By project name REST Service", e); //$NON-NLS-1$
		}
		
		return null;
	}
	
}
