package com.magna.xmsystem.xmadmin.restclient.validation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.magna.xmbackend.vo.permission.ValidationRequest;
import com.magna.xmsystem.dependencies.customExceptions.UnauthorizedAccessException;
import com.magna.xmsystem.xmadmin.restclient.RestController;
import com.magna.xmsystem.xmadmin.restclient.util.RestClientExpHandlerUtil;
import com.magna.xmsystem.xmadmin.restclient.util.XMAdminRestClientConstants;

public class ValidationController extends RestController{

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(ValidationController.class);

	/**
	 * Constructor for SiteController Class.
	 */
	public ValidationController() {
		super();
	}
	
	public ValidationController(String adminAreaId) {
		super(adminAreaId);
	}
	/**
	 * Gets the access allowed.
	 *
	 * @param validationRequest {@link ValidationRequest}
	 * @return the access allowed
	 */
	public boolean getAccessAllowed(ValidationRequest validationRequest) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.GET_ACCESS_ALLOWED);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<ValidationRequest> requestEntity = new HttpEntity<ValidationRequest>(validationRequest, this.headers);
			ResponseEntity<Boolean> responseEntity = restTemplate.exchange(url, HttpMethod.POST, requestEntity,
					Boolean.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				return responseEntity.getBody();
			} else {
				LOGGER.error("Error while calling XMSYSTEM get Access Allowed REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		}  catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			}else if (e instanceof ResourceAccessException) {
				throw e;
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM get Access Allowed REST Service!", e); //$NON-NLS-1$
		}
		return false;
	}
	
	/**
	 * Check hotline permission.
	 *
	 * @return true, if successful
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 */
	public boolean checkHotlinePermission() throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.GET_HOTLINE_PERMISSION);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);
			ResponseEntity<Boolean> responseEntity = restTemplate.exchange(url, HttpMethod.GET, requestEntity, Boolean.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				return responseEntity.getBody();
			} else {
				LOGGER.error("Error while calling XMSYSTEM get Access Allowed REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		}  catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM get Access Allowed REST Service!", e); //$NON-NLS-1$
		}
		return false;
	}
}
