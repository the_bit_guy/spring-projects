package com.magna.xmsystem.xmadmin.restclient.application;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.magna.xmbackend.entities.UserAppTranslationTbl;
import com.magna.xmbackend.entities.UserApplicationsTbl;
import com.magna.xmbackend.exception.CannotCreateObjectException;
import com.magna.xmbackend.vo.userApplication.UserApplicationMenuWrapper;
import com.magna.xmbackend.vo.userApplication.UserApplicationRequest;
import com.magna.xmbackend.vo.userApplication.UserApplicationResponse;
import com.magna.xmsystem.dependencies.customExceptions.UnauthorizedAccessException;
import com.magna.xmsystem.dependencies.utils.LANG_ENUM;
import com.magna.xmsystem.dependencies.utils.XMSystemUtil;
import com.magna.xmsystem.xmadmin.restclient.RestController;
import com.magna.xmsystem.xmadmin.restclient.util.RestClientExpHandlerUtil;
import com.magna.xmsystem.xmadmin.restclient.util.RestClientSortingUtil;
import com.magna.xmsystem.xmadmin.restclient.util.XMAdminRestClientConstants;


/**
 * Class for User app controller.
 *
 * @author Roshan.Ekka
 */
public class UserAppController extends RestController {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(UserAppController.class);

	/**
	 * Constructor for BaseAppController Class.
	 */

	public UserAppController() {
		super();
	}

	
	
	/**
	 * Method for Creates the user application.
	 *
	 * @param userAppRequest {@link UserApplicationRequest}
	 * @return the user applications tbl {@link UserApplicationsTbl}
	 */
	public UserApplicationsTbl createUserApplication(UserApplicationRequest userAppRequest) throws UnauthorizedAccessException, CannotCreateObjectException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.CREATE_USER_APPLICATION);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<UserApplicationRequest> request = new HttpEntity<UserApplicationRequest>(userAppRequest,
					this.headers);
			ResponseEntity<UserApplicationsTbl> responseEntity = restTemplate.postForEntity(url, request,
					UserApplicationsTbl.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				UserApplicationsTbl userApplicationsTbl = responseEntity.getBody();
				return userApplicationsTbl;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Create User Application REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				String errorResponse = ((HttpStatusCodeException) e).getResponseBodyAsString();
				String errCode = RestClientExpHandlerUtil.handleCannotCreateObjectException(LOGGER, (HttpStatusCodeException) e);
				throw new CannotCreateObjectException(errorResponse, errCode);
			}else if (e instanceof ResourceAccessException) {
				throw e;
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Create User Application REST Service", e); //$NON-NLS-1$
		}
		return null;
	}
	
	

	/**
	 * Gets the all user apps.
	 *
	 * @param isSorted {@link boolean}
	 * @return the all user apps
	 */
	public List<UserApplicationsTbl> getAllUserApps(final boolean isSorted) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.GET_USER_APPLICATIONS);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<UserApplicationResponse> responseList = restTemplate.exchange(url, HttpMethod.GET,
					requestEntity, UserApplicationResponse.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				UserApplicationResponse userAppResponse = responseList.getBody();
				List<UserApplicationsTbl> userAppTblsList = (List<UserApplicationsTbl>) userAppResponse
						.getUatsUserApplicationsTbls();
				if (isSorted) {
					userAppTblsList.sort((userAppResponse1, userAppResponse2) -> {
						Collection<UserAppTranslationTbl> userAppTranslation1;
						Collection<UserAppTranslationTbl> userAppTranslation2;
						if ((userAppTranslation1 = userAppResponse1.getUserAppTranslationTblCollection()) != null
								&& (userAppTranslation2 = userAppResponse2
										.getUserAppTranslationTblCollection()) != null) {
							String userAppName1 = getNameByLanguage(LANG_ENUM.ENGLISH, userAppTranslation1);
							String userAppName2 = getNameByLanguage(LANG_ENUM.ENGLISH, userAppTranslation2);
							return userAppName1.compareTo(userAppName2);
						}
						return 0;
					});
				}
				return userAppTblsList;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Get All User Applicatios REST Service, returns the status code: " //$NON-NLS-1$
								+ statusCode.value());
			}
		}  catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get All User Applications REST Service", e); //$NON-NLS-1$
		}
		return null;
	}

	/**
	 * Gets the name by language.
	 *
	 * @param langEnum
	 *            {@link LANG_ENUM}
	 * @param userAppTranslations
	 *            {@link Collection<UserAppTranslationTbl>}
	 * @return the name by language
	 */
	private String getNameByLanguage(final LANG_ENUM langEnum,
			final Collection<UserAppTranslationTbl> userAppTranslations) {
		for (UserAppTranslationTbl userAppTranslation : userAppTranslations) {
			LANG_ENUM foundLangEnum = LANG_ENUM.getLangEnum(userAppTranslation.getLanguageCode().getLanguageCode());
			String name;
			if (foundLangEnum == langEnum && (name = userAppTranslation.getName()) != null) {
				if (!XMSystemUtil.isEmpty(name)) {
					return name;
				}
			}
		}
		return "";
	}

	/**
	 * Method for Delete user app.
	 *
	 * @param userApplicationId
	 *            {@link String}
	 * @return true, if successful
	 */
	public boolean deleteUserApplication(String userApplicationId) throws UnauthorizedAccessException {
		try {
			String url = new String(
					this.serviceUrl + XMAdminRestClientConstants.DELETE_USER_APPLICATION + "/" + userApplicationId);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(this.headers);
			ResponseEntity<Boolean> responseEntity = restTemplate.exchange(url, HttpMethod.DELETE, requestEntity,
					Boolean.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				return responseEntity.getBody();
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM delete User Application REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM delete User Application REST Service!", e); //$NON-NLS-1$
		}
		return false;
	}
	
	
	/**
	 * Delete multi user app.
	 *
	 * @param userAppIds the user app ids
	 * @return the user application response
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 */
	public UserApplicationResponse deleteMultiUserApp(Set<String> userAppIds) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.DELETE_MULTI_USER_APP);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Set<String>> requestEntity = new HttpEntity<Set<String>>(userAppIds,this.headers);
			ResponseEntity<UserApplicationResponse> responseEntity = restTemplate.exchange(url, HttpMethod.DELETE, requestEntity,
					UserApplicationResponse.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				UserApplicationResponse userApplicationResponse = responseEntity.getBody();
				return userApplicationResponse;
			} else {
				LOGGER.error("Error while calling XMSYSTEM delete Multi UserApp REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM delete Multi UserApp REST Service!", e); //$NON-NLS-1$
		}
		return null;
	}

	/**
	 * Method for Update user application.
	 *
	 * @param userAppRequest
	 *            {@link UserApplicationRequest}
	 * @return true, if successful
	 */
	public boolean updateUserApplication(UserApplicationRequest userAppRequest) throws UnauthorizedAccessException, CannotCreateObjectException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.UPDATE_USER_APPLICATION);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<UserApplicationRequest> request = new HttpEntity<UserApplicationRequest>(userAppRequest,
					this.headers);
			ResponseEntity<UserApplicationsTbl> responseEntity = restTemplate.postForEntity(url, request,
					UserApplicationsTbl.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				return true;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Upadte User Application REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				String errorResponse = ((HttpStatusCodeException) e).getResponseBodyAsString();
				String errCode = RestClientExpHandlerUtil.handleCannotCreateObjectException(LOGGER, (HttpStatusCodeException) e);
				throw new CannotCreateObjectException(errorResponse, errCode);
			}else if (e instanceof ResourceAccessException) {
				throw e;
			}
		}  catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Upadte User Application REST Service", e); //$NON-NLS-1$
		}
		return false;
	}

	/**
	 * Method for Update user app status.
	 *
	 * @param userApplicationId
	 *            {@link String}
	 * @param status
	 *            {@link String}
	 * @return true, if successful
	 */
	public boolean updateUserAppStatus(String userApplicationId, String status) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.UPDATE_USER_APPLICATION_STATUS + "/"
					+ status + "/" + userApplicationId);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(this.headers);
			ResponseEntity<Boolean> responseEntity = restTemplate.exchange(url, HttpMethod.POST, requestEntity,
					Boolean.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				return responseEntity.getBody();
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Update User Application Status REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Update User Application Status REST Service!", e); //$NON-NLS-1$
		}
		return false;
	}
	
	/**
	 * Multi update user app status.
	 *
	 * @param userAppRequest the user app request
	 * @return the user application response
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 */
	public UserApplicationResponse multiUpdateUserAppStatus(List<UserApplicationRequest> userAppRequest) throws UnauthorizedAccessException {
		try {
			String url = new String(
					this.serviceUrl + XMAdminRestClientConstants.MULTI_UPDATE_USER_APP_STATUS );

			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<List<UserApplicationRequest>> requestEntity = new HttpEntity<List<UserApplicationRequest>>(userAppRequest, this.headers);
			ResponseEntity<UserApplicationResponse> responseEntity = restTemplate.exchange(url, HttpMethod.POST, requestEntity,
					UserApplicationResponse.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				return responseEntity.getBody();
			} else {
				LOGGER.error("Error while calling XMSYSTEM Multi Update userApp Status REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Multi Update userApp Status REST Service!", e); //$NON-NLS-1$
		}
		return null;
	}
	
	/**
	 * Gets the all user app by base app id.
	 *
	 * @param baseAppId {@link String}
	 * @return the all user app by base app id
	 */
	public List<UserApplicationsTbl> getAllUserAppByBaseAppId(String baseAppId) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.GET_USER_APP_BY_BASE_APP_ID + "/" + baseAppId);
			List<UserApplicationsTbl> baseUserAppRelTblList = new ArrayList<>();
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<UserApplicationResponse> responseList = restTemplate.exchange(url, HttpMethod.GET, requestEntity,
					UserApplicationResponse.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				UserApplicationResponse userAppRespObj = responseList.getBody();
				Iterable<UserApplicationsTbl> uatsUserApplicationsTbls = userAppRespObj.getUatsUserApplicationsTbls();
				for (UserApplicationsTbl siteAdminTbl : uatsUserApplicationsTbls) {
					baseUserAppRelTblList.add(siteAdminTbl);
				}
				
				return RestClientSortingUtil.sortAllUserAppByBaseAppIdTbl(baseUserAppRelTblList);
			} else {
				LOGGER.error("Error while calling XMSYSTEM Get User Application By BaseAppId REST Service, returns the status code: " //$NON-NLS-1$
						+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			} else if(e instanceof ResourceAccessException){
				throw e;
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get All User Appliocation By BaseAppId REST Service", e); //$NON-NLS-1$
		}
		return null;
	}
	
	/**
	 * Gets the all user app by position.
	 *
	 * @param position {@link String}
	 * @return the all user app by position
	 */
	public UserApplicationResponse getAllUserAppByPosition(String position) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.GET_USER_APP_BY_POSITION + "/" + position);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<UserApplicationResponse> responseList = restTemplate.exchange(url, HttpMethod.GET, requestEntity,
					UserApplicationResponse.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				UserApplicationResponse userAppRespObj = responseList.getBody();
				return userAppRespObj;
			} else {
				LOGGER.error("Error while calling XMSYSTEM Get User Application By Position REST Service, returns the status code: " //$NON-NLS-1$
						+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get All User Appliocation By Position REST Service", e); //$NON-NLS-1$
		}
		return null;
	}
	
	/**
	 * Gets the all user app by id.
	 *
	 * @param userAppId {@link String}
	 * @return the all user app by id
	 */
	public UserApplicationsTbl getAllUserAppById(String userAppId) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.GET_USER_APP_BY_ID + "/" + userAppId);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<UserApplicationsTbl> responseList = restTemplate.exchange(url, HttpMethod.GET, requestEntity,
					UserApplicationsTbl.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				UserApplicationsTbl userAppTblRespObj = responseList.getBody();
				return userAppTblRespObj;
			} else {
				LOGGER.error("Error while calling XMSYSTEM Get User Application By Id REST Service, returns the status code: " //$NON-NLS-1$
						+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get All User Appliocation By Id REST Service", e); //$NON-NLS-1$
		}
		return null;
	}
	
	/**
	 * Gets the all child user app by user app id.
	 *
	 * @param userAppId {@link String}
	 * @return the all child user app by user app id
	 */
	public List<UserApplicationsTbl> getAllChildUserAppByUserAppId(String userAppId) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.GET_CHILD_USER_APP_BY_USER_APP_ID + "/" + userAppId);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<UserApplicationResponse> responseList = restTemplate.exchange(url, HttpMethod.GET, requestEntity,
					UserApplicationResponse.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				UserApplicationResponse userAppRespObj = responseList.getBody();
				List<UserApplicationsTbl> userAppChildRelTblList = (List<UserApplicationsTbl>) userAppRespObj
						.getUatsUserApplicationsTbls();
				return RestClientSortingUtil.sortAllUserAppByBaseAppIdTbl(userAppChildRelTblList);
			} else {
				LOGGER.error("Error while calling XMSYSTEM Get All Child User Application By UserAppId REST Service, returns the status code: " //$NON-NLS-1$
						+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			} else if(e instanceof ResourceAccessException){
				throw e;
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get All Child User Appliocation By UserAppId REST Service", e); //$NON-NLS-1$
		}
		return null;
	}
	
	/**
	 * Gets the all user app by status.
	 *
	 * @param status {@link String}
	 * @return the all user app by status
	 */
	public UserApplicationResponse getAllUserAppByStatus(String status) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.GET_USER_APP_BY_STATUS + "/" + status);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<UserApplicationResponse> responseList = restTemplate.exchange(url, HttpMethod.GET, requestEntity,
					UserApplicationResponse.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				UserApplicationResponse userAppRespObj = responseList.getBody();
				return userAppRespObj;
			} else {
				LOGGER.error("Error while calling XMSYSTEM Get All User Application By Status REST Service, returns the status code: " //$NON-NLS-1$
						+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get All User Appliocation By Status REST Service", e); //$NON-NLS-1$
		}
		return null;
	}
	
	/**
	 * Gets the all user apps by positions.
	 *
	 * @return the all user apps by positions
	 */
	public List<UserApplicationsTbl> getAllUserAppsByPositions() throws UnauthorizedAccessException{
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.GET_USER_APP_BY_POSITIONS);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<UserApplicationResponse> responseList = restTemplate.exchange(url, HttpMethod.GET, requestEntity,
					UserApplicationResponse.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				UserApplicationResponse userAppRespObj = responseList.getBody();
				List<UserApplicationsTbl> userAppTblList = (List<UserApplicationsTbl>) userAppRespObj.getUatsUserApplicationsTbls();
				return RestClientSortingUtil.sortUserAppTbl(userAppTblList);
			} else {
				LOGGER.error("Error while calling XMSYSTEM Get All User Application By Positions REST Service, returns the status code: " //$NON-NLS-1$
						+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			} else if(e instanceof ResourceAccessException){
				throw e;
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get All User Appliocation By Positions REST Service", e); //$NON-NLS-1$
		}
		return null;
	}
	
	/**
	 * Gets the all user app by user id.
	 *
	 * @param userId {@link String}
	 * @return the all user app by user id
	 */
	public UserApplicationResponse getAllUserAppByUserId(String userId) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.GET_USER_APP_BY_USER_ID + "/" + userId);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<UserApplicationResponse> responseList = restTemplate.exchange(url, HttpMethod.GET, requestEntity,
					UserApplicationResponse.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				UserApplicationResponse userAppRespObj = responseList.getBody();
				return userAppRespObj;
			} else {
				LOGGER.error("Error while calling XMSYSTEM Get User Application By User Id REST Service, returns the status code: " //$NON-NLS-1$
						+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get All User Appliocation By User Id REST Service", e); //$NON-NLS-1$
		}
		return null;
	}
	
	/**
	 * Gets the user app by AA id and user name.
	 *
	 * @param adminAreaId {@link String}
	 * @return the user app by AA id and user name
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 */
	public UserApplicationMenuWrapper getUserAppByAAIdAndUserName(String adminAreaId) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.GET_USER_APP_BY_AA_ID_AND_USERNAME + "/" + adminAreaId);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<UserApplicationMenuWrapper> responseList = restTemplate.exchange(url, HttpMethod.GET, requestEntity,
					UserApplicationMenuWrapper.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				UserApplicationMenuWrapper userApplicationMenuWrapper = responseList.getBody();
				return userApplicationMenuWrapper;
			} else {
				LOGGER.error("Error while calling XMSYSTEM Get User Application By adminAreaId and userName REST Service, returns the status code: " //$NON-NLS-1$
						+ statusCode.value());
			}
		}  catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			} else if (e instanceof ResourceAccessException) {
				throw e;
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get All User Appliocation By adminAreaId and userName REST Service", e); //$NON-NLS-1$
		}
		return null;
	}
	
	/**
	 * Gets the user app by user app name
	 * 
	 * @param userAppName
	 * @return
	 * @throws UnauthorizedAccessException
	 */
	public UserApplicationsTbl getUserApplicationByName(final String userAppName) throws UnauthorizedAccessException{
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.GET_USER_APP_BY_NAME + "/" + userAppName);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<UserApplicationsTbl> responseList = restTemplate.exchange(url, HttpMethod.GET, requestEntity,
					UserApplicationsTbl.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				UserApplicationsTbl userAppTblRespObj = responseList.getBody();
				return userAppTblRespObj;
			} else {
				LOGGER.error("Error while calling XMSYSTEM Get User Application By name REST Service, returns the status code: " //$NON-NLS-1$
						+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get All User Appliocation By name REST Service", e); //$NON-NLS-1$
		}
		
		return null;
	}
	
	/**
	 * Gets the user app by user app name.
	 *
	 * @param userAppName the user app name
	 * @return the user app by user app name
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 */
	public UserApplicationMenuWrapper getUserAppByUserAppName(final String userAppName) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.GET_USER_APP_BY_USER_APP_NAME + "/" + userAppName);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<UserApplicationMenuWrapper> responseList = restTemplate.exchange(url, HttpMethod.GET, requestEntity,
					UserApplicationMenuWrapper.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				UserApplicationMenuWrapper userApplicationMenuWrapper = responseList.getBody();
				return userApplicationMenuWrapper;
			} else {
				LOGGER.error("Error while calling XMSYSTEM Get User Application By userApp name REST Service, returns the status code: " //$NON-NLS-1$
						+ statusCode.value());
			}
		}  catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			} else if (e instanceof ResourceAccessException) {
				throw e;
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get All User Appliocation By userApp name REST Service", e); //$NON-NLS-1$
		}
		return null;
	}
}
