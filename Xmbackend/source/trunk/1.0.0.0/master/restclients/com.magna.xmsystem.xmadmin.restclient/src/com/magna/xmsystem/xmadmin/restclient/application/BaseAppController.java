package com.magna.xmsystem.xmadmin.restclient.application;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.magna.xmbackend.entities.BaseApplicationsTbl;
import com.magna.xmbackend.exception.CannotCreateObjectException;
import com.magna.xmbackend.vo.baseApplication.BaseApplicationRequest;
import com.magna.xmbackend.vo.baseApplication.BaseApplicationResponse;
import com.magna.xmsystem.dependencies.customExceptions.UnauthorizedAccessException;
import com.magna.xmsystem.xmadmin.restclient.RestController;
import com.magna.xmsystem.xmadmin.restclient.util.RestClientExpHandlerUtil;
import com.magna.xmsystem.xmadmin.restclient.util.RestClientSortingUtil;
import com.magna.xmsystem.xmadmin.restclient.util.XMAdminRestClientConstants;

/**
 * Class for Base app controller.
 *
 * @author Chiranjeevi.Akula
 */
public class BaseAppController extends RestController {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(BaseAppController.class);

	/**
	 * Constructor for BaseAppController Class.
	 */

	public BaseAppController() {
		super();
	}

	/**
	 * Method for Creates the base application.
	 *
	 * @param baseAppRequest
	 *            {@link BaseApplicationRequest}
	 * @return the string {@link String}
	 */
	public BaseApplicationsTbl createBaseApplication(BaseApplicationRequest baseAppRequest) throws UnauthorizedAccessException, CannotCreateObjectException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.CREATE_BASE_APPLICATION);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<BaseApplicationRequest> request = new HttpEntity<BaseApplicationRequest>(baseAppRequest,
					this.headers);
			ResponseEntity<BaseApplicationsTbl> responseEntity = restTemplate.postForEntity(url, request,
					BaseApplicationsTbl.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				BaseApplicationsTbl baseApplicationsTbl = responseEntity.getBody();
				return baseApplicationsTbl;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Create Base Application REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				String errorResponse = ((HttpStatusCodeException) e).getResponseBodyAsString();
				String errCode = RestClientExpHandlerUtil.handleCannotCreateObjectException(LOGGER, (HttpStatusCodeException) e);
				throw new CannotCreateObjectException(errorResponse, errCode);
			}else if (e instanceof ResourceAccessException) {
				throw e;
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Create Base Application REST Service", e); //$NON-NLS-1$
		}
		return null;
	}

	/**
	 * Gets the all base apps.
	 *
	 * @param isSorted
	 *            {@link boolean}
	 * @return the all base apps
	 */
	public List<BaseApplicationsTbl> getAllBaseApps(final boolean isSorted) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.GET_BASE_APPLICATIONS);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<BaseApplicationResponse> responseList = restTemplate.exchange(url, HttpMethod.GET,
					requestEntity, BaseApplicationResponse.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				BaseApplicationResponse baseAppResponse = responseList.getBody();
				List<BaseApplicationsTbl> baseAppTblsList = (List<BaseApplicationsTbl>) baseAppResponse
						.getBaseApplicationsTbls();
				return RestClientSortingUtil.sortBaseAppTbl(baseAppTblsList);
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Get All Base Applicatios REST Service, returns the status code: " //$NON-NLS-1$
								+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			} else if(e instanceof ResourceAccessException){
				throw e;
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get All Base Applications REST Service", e); //$NON-NLS-1$
		}
		return null;
	}

	/**
	 * Method for Delete base application.
	 *
	 * @param baseApplicationId
	 *            {@link String}
	 * @return true, if successful
	 */
	public boolean deleteBaseApplication(String baseApplicationId) throws UnauthorizedAccessException {
		try {
			String url = new String(
					this.serviceUrl + XMAdminRestClientConstants.DELETE_BASE_APPLICATION + "/" + baseApplicationId);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(this.headers);
			ResponseEntity<Boolean> responseEntity = restTemplate.exchange(url, HttpMethod.DELETE, requestEntity,
					Boolean.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				return responseEntity.getBody();
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM delete Base Application REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		}  catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM delete Base Application REST Service!", e); //$NON-NLS-1$
		}
		return false;
	}
	
	/**
	 * Delete multi base app.
	 *
	 * @param baseAppIds the base app ids
	 * @return the base application response
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 */
	public BaseApplicationResponse deleteMultiBaseApp(Set<String> baseAppIds) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.DELETE_MULTI_BASE_APP);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Set<String>> requestEntity = new HttpEntity<Set<String>>(baseAppIds,this.headers);
			ResponseEntity<BaseApplicationResponse> responseEntity = restTemplate.exchange(url, HttpMethod.DELETE, requestEntity,
					BaseApplicationResponse.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				BaseApplicationResponse baseApplicationResponse = responseEntity.getBody();
				return baseApplicationResponse;
			} else {
				LOGGER.error("Error while calling XMSYSTEM delete Multi BaseApplication REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM delete Multi BaseApplication REST Service!", e); //$NON-NLS-1$
		}
		return null;
	}

	/**
	 * Method for Update base application.
	 *
	 * @param baseAppRequest
	 *            {@link BaseApplicationRequest}
	 * @return true, if successful
	 */
	public boolean updateBaseApplication(BaseApplicationRequest baseAppRequest) throws UnauthorizedAccessException, CannotCreateObjectException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.UPDATE_BASE_APPLICATION);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<BaseApplicationRequest> request = new HttpEntity<BaseApplicationRequest>(baseAppRequest,
					this.headers);
			ResponseEntity<BaseApplicationsTbl> responseEntity = restTemplate.postForEntity(url, request,
					BaseApplicationsTbl.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				return true;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Upadte Base Application REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				String errorResponse = ((HttpStatusCodeException) e).getResponseBodyAsString();
				String errCode = RestClientExpHandlerUtil.handleCannotCreateObjectException(LOGGER, (HttpStatusCodeException) e);
				throw new CannotCreateObjectException(errorResponse, errCode);
			}else if (e instanceof ResourceAccessException) {
				throw e;
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Upadte Base Application REST Service", e); //$NON-NLS-1$
		}
		return false;
	}

	/**
	 * Method for Update base app status.
	 *
	 * @param baseApplicationId
	 *            {@link String}
	 * @param status
	 *            {@link String}
	 * @return true, if successful
	 */
	public boolean updateBaseAppStatus(String baseApplicationId, String status) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.UPDATE_BASE_APPLICATION_STATUS + "/"
					+ status + "/" + baseApplicationId);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(this.headers);
			ResponseEntity<Boolean> responseEntity = restTemplate.exchange(url, HttpMethod.POST, requestEntity,
					Boolean.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				return responseEntity.getBody();
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Update Base Application Status REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Update Base Application Status REST Service!", e); //$NON-NLS-1$
		}
		return false;
	}
	
	/**
	 * Multi update base app status.
	 *
	 * @param baseAppRequest the base app request
	 * @return the base application response
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 */
	public BaseApplicationResponse multiUpdateBaseAppStatus(List<BaseApplicationRequest> baseAppRequest) throws UnauthorizedAccessException {
		try {
			String url = new String(
					this.serviceUrl + XMAdminRestClientConstants.MULTI_UPDATE_BASE_APP_STATUS );

			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<List<BaseApplicationRequest>> requestEntity = new HttpEntity<List<BaseApplicationRequest>>(baseAppRequest, this.headers);
			ResponseEntity<BaseApplicationResponse> responseEntity = restTemplate.exchange(url, HttpMethod.POST, requestEntity,
					BaseApplicationResponse.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				return responseEntity.getBody();
			} else {
				LOGGER.error("Error while calling XMSYSTEM Multi Update BaseApp Status REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Multi Update BaseApp Status REST Service!", e); //$NON-NLS-1$
		}
		return null;
	}
	
	/**
	 * Gets the base app by id.
	 *
	 * @param baseAppId {@link String}
	 * @return the base app by id
	 */
	public BaseApplicationsTbl getBaseAppById(String baseAppId) throws UnauthorizedAccessException {
		try {
			String url = new String(
					this.serviceUrl + XMAdminRestClientConstants.GET_BASE_APP_BY_ID + "/" + baseAppId);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<BaseApplicationsTbl> responseList = restTemplate.exchange(url, HttpMethod.GET, requestEntity,
					BaseApplicationsTbl.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				BaseApplicationsTbl baseAppTblObject = responseList.getBody();
				return baseAppTblObject;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Get All Base Application by Id REST Service, returns the status code: " //$NON-NLS-1$
								+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get All Base Application by Id REST Service", e); //$NON-NLS-1$
		}
		return null;
	}
}
