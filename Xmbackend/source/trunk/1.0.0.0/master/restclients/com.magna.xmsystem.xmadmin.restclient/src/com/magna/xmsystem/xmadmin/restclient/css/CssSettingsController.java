/**
 * 
 */
package com.magna.xmsystem.xmadmin.restclient.css;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.magna.xmbackend.exception.CannotCreateObjectException;
import com.magna.xmbackend.vo.baseApplication.BaseApplicationRequest;
import com.magna.xmbackend.vo.css.CssSettingsRequest;
import com.magna.xmbackend.vo.css.CssSettingsResponse;
import com.magna.xmsystem.dependencies.customExceptions.UnauthorizedAccessException;
import com.magna.xmsystem.xmadmin.restclient.RestController;
import com.magna.xmsystem.xmadmin.restclient.util.RestClientExpHandlerUtil;
import com.magna.xmsystem.xmadmin.restclient.util.XMAdminRestClientConstants;

/**
 * @author Bhabadyuti Bal
 *
 */
public class CssSettingsController extends RestController{

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(CssSettingsController.class);

	/**
	 * Constructor for BaseAppController Class.
	 */

	public CssSettingsController() {
		super();
	}

	/**
	 * Method for Creates the base application.
	 *
	 * @param baseAppRequest
	 *            {@link BaseApplicationRequest}
	 * @return the string {@link String}
	 */
	public boolean createCssSetting(final CssSettingsRequest settingsRequest)
			throws UnauthorizedAccessException, CannotCreateObjectException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.CREATE_CSS_FILE);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<CssSettingsRequest> request = new HttpEntity<CssSettingsRequest>(settingsRequest, this.headers);
			ResponseEntity<Boolean> responseEntity = restTemplate.postForEntity(url, request, Boolean.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				Boolean isCreated = responseEntity.getBody();
				return isCreated;
			} else {
				LOGGER.error("Error while calling XMSYSTEM Create CSS settings REST Service, returns the status code: ", statusCode.value()); //$NON-NLS-1$
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				String errorResponse = ((HttpStatusCodeException) e).getResponseBodyAsString();
				String errCode = RestClientExpHandlerUtil.handleCannotCreateObjectException(LOGGER,
						(HttpStatusCodeException) e);
				throw new CannotCreateObjectException(errorResponse, errCode);
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Create CSS settings REST Service", e); //$NON-NLS-1$
		}
		return false;
	}
	
	/**
	 * Gets the css file existence res.
	 *
	 * @return the css file existence res
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 */
	public CssSettingsResponse getCssFileExistenceRes() throws UnauthorizedAccessException, CannotCreateObjectException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.CSS_FILE_EXISTENCE);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

 			ResponseEntity<CssSettingsResponse> responseList = restTemplate.exchange(url, HttpMethod.GET, requestEntity, CssSettingsResponse.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				CssSettingsResponse cssSettingsResponse = responseList.getBody();
				return cssSettingsResponse;
			} else {
				LOGGER.error("Error while calling XMSYSTEM get CSS File Existence Service, returns the status code: " + statusCode.value()); //$NON-NLS-1$
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				} else if (e instanceof HttpStatusCodeException) {
					RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
				}
			}  else if (e instanceof HttpStatusCodeException) {
				String errorResponse = ((HttpStatusCodeException) e).getResponseBodyAsString();
				String errCode = RestClientExpHandlerUtil.handleCannotCreateObjectException(LOGGER,
						(HttpStatusCodeException) e);
				throw new CannotCreateObjectException(errorResponse, errCode);
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM CSS File Existence REST Service", e); //$NON-NLS-1$
		}
		return null;
	}
	
	/**
	 * Delete css setting.
	 *
	 * @param file the file
	 * @return true, if successful
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 * @throws CannotCreateObjectException the cannot create object exception
	 */
	public boolean deleteCssSetting() throws UnauthorizedAccessException, CannotCreateObjectException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.DELETE_CSS_FILE);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(this.headers);
			ResponseEntity<Boolean> responseEntity = restTemplate.exchange(url, HttpMethod.DELETE, requestEntity,
					Boolean.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				return responseEntity.getBody();
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM delete CSS Settings REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		}  catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM delete CSS Settings REST Service!", e); //$NON-NLS-1$
		}
		return false;
	}
	
	/**
	 * Update css setting.
	 *
	 * @param isApplied the is applied
	 * @return true, if successful
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 * @throws CannotCreateObjectException the cannot create object exception
	 */
	public boolean updateCssSetting(final boolean isApplied) throws UnauthorizedAccessException, CannotCreateObjectException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.UPDATE_CSS_APPLIED_STATUS + "/" + isApplied);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(this.headers);
			ResponseEntity<Boolean> responseEntity = restTemplate.exchange(url, HttpMethod.PUT, requestEntity,
					Boolean.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				return responseEntity.getBody();
			} else {
				LOGGER.error("Error while calling XMSYSTEM delete CSS Settings REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		}  catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM delete CSS Settings REST Service!", e); //$NON-NLS-1$
		}
		return false;
	}

}
