package com.magna.xmsystem.xmadmin.restclient.relation;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.magna.xmbackend.entities.SiteAdminAreaRelTbl;
import com.magna.xmbackend.exception.CannotCreateObjectException;
import com.magna.xmbackend.vo.jpa.site.SiteAdminAreaRelIdWithAdminAreaProjRel;
import com.magna.xmbackend.vo.jpa.site.SiteAdminAreaRelIdWithAdminAreaProjRelResponse;
import com.magna.xmbackend.vo.jpa.site.SiteAdminAreaRelIdWithAdminAreaStartAppRel;
import com.magna.xmbackend.vo.jpa.site.SiteAdminAreaRelIdWithAdminAreaStartAppRelResponse;
import com.magna.xmbackend.vo.jpa.site.SiteAdminAreaRelIdWithAdminAreaUsrAppRel;
import com.magna.xmbackend.vo.jpa.site.SiteAdminAreaRelIdWithAdminAreaUsrAppRelResponse;
import com.magna.xmbackend.vo.rel.SiteAdminAreaRelBatchRequest;
import com.magna.xmbackend.vo.rel.SiteAdminAreaRelBatchResponse;
import com.magna.xmbackend.vo.rel.SiteAdminAreaRelRequest;
import com.magna.xmbackend.vo.rel.SiteAdminAreaRelResponse;
import com.magna.xmsystem.dependencies.customExceptions.UnauthorizedAccessException;
import com.magna.xmsystem.xmadmin.restclient.RestController;
import com.magna.xmsystem.xmadmin.restclient.util.RestClientExpHandlerUtil;
import com.magna.xmsystem.xmadmin.restclient.util.RestClientSortingUtil;
import com.magna.xmsystem.xmadmin.restclient.util.XMAdminRestClientConstants;

/**
 * Class for Site admin area rel controller.
 *
 * @author Roshan.Ekka
 */
public class SiteAdminAreaRelController extends RestController {
	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(SiteAdminAreaRelController.class);

	/**
	 * Constructor for SiteController Class.
	 */
	public SiteAdminAreaRelController() {
		super();
	}

	/**
	 * Method for Creates the site admin area rel.
	 *
	 * @param siteAdminAreaRelRequest
	 *            {@link SiteAdminAreaRelRequest}
	 * @return the site admin area rel tbl {@link SiteAdminAreaRelTbl}
	 */
	public SiteAdminAreaRelTbl createSiteAdminAreaRel(SiteAdminAreaRelRequest siteAdminAreaRelRequest)
			throws UnauthorizedAccessException, CannotCreateObjectException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.CREATE_SITE_ADMIN_AREA_REL);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<SiteAdminAreaRelRequest> request = new HttpEntity<SiteAdminAreaRelRequest>(
					siteAdminAreaRelRequest, this.headers);
			ResponseEntity<SiteAdminAreaRelTbl> responseEntity = restTemplate.postForEntity(url, request,
					SiteAdminAreaRelTbl.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				SiteAdminAreaRelTbl siteAdminAreaRelTbl = responseEntity.getBody();
				return siteAdminAreaRelTbl;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Create Site Administration Area Relation REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				String errorResponse = ((HttpStatusCodeException) e).getResponseBodyAsString();
				String errCode = RestClientExpHandlerUtil.handleCannotCreateObjectException(LOGGER,
						(HttpStatusCodeException) e);
				throw new CannotCreateObjectException(errorResponse, errCode);
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Create Site Administration Area Relation REST Service", e); //$NON-NLS-1$
		}
		return null;
	}

	/**
	 * Method for Creates the site admin area rel.
	 *
	 * @param siteAdminAreaRelBatchRequest
	 *            {@link SiteAdminAreaRelBatchRequest}
	 * @return the site admin area rel batch response
	 *         {@link SiteAdminAreaRelBatchResponse}
	 */
	public SiteAdminAreaRelBatchResponse createSiteAdminAreaRel(
			SiteAdminAreaRelBatchRequest siteAdminAreaRelBatchRequest)
			throws UnauthorizedAccessException, CannotCreateObjectException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.CREATE_MULTI_SITE_ADMIN_AREA_REL);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<SiteAdminAreaRelBatchRequest> request = new HttpEntity<SiteAdminAreaRelBatchRequest>(
					siteAdminAreaRelBatchRequest, this.headers);
			ResponseEntity<SiteAdminAreaRelBatchResponse> responseEntity = restTemplate.postForEntity(url, request,
					SiteAdminAreaRelBatchResponse.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				SiteAdminAreaRelBatchResponse siteAdminAreaRelResponse = responseEntity.getBody();
				return siteAdminAreaRelResponse;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Create Site Administration Area Relation REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				String errorResponse = ((HttpStatusCodeException) e).getResponseBodyAsString();
				String errCode = RestClientExpHandlerUtil.handleCannotCreateObjectException(LOGGER,
						(HttpStatusCodeException) e);
				throw new CannotCreateObjectException(errorResponse, errCode);
			} else if (e instanceof ResourceAccessException) {
				throw e;
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Create Site Administration Area Relation REST Service", e); //$NON-NLS-1$
		}
		return null;
	}

	/**
	 * Gets the all site admin area rel.
	 *
	 * @param isSorted
	 *            {@link boolean}
	 * @return the all site admin area rel
	 */
	public List<SiteAdminAreaRelTbl> getAllSiteAdminAreaRel() throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.GET_SITE_ADMIN_AREA_REL);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);
			List<SiteAdminAreaRelTbl> siteAdminAreaRelTblList = new ArrayList<>();
			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<SiteAdminAreaRelResponse> responseList = restTemplate.exchange(url, HttpMethod.GET,
					requestEntity, SiteAdminAreaRelResponse.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				SiteAdminAreaRelResponse siteResponseObject = responseList.getBody();
				
				if (siteResponseObject != null) {
					Iterable<SiteAdminAreaRelTbl> siteAdminRelTbls = siteResponseObject.getSiteAdminAreaRelTbls();
					
					for (SiteAdminAreaRelTbl siteAdminTbl : siteAdminRelTbls) {
						siteAdminAreaRelTblList .add(siteAdminTbl);
					}
				}
				return RestClientSortingUtil.sortSiteAdminAreaTbl(siteAdminAreaRelTblList);
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Get All Site Administration Area Relation REST Service, returns the status code: " //$NON-NLS-1$
								+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			} else if(e instanceof ResourceAccessException){
				throw e;
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get All Site Administration Area Relation REST Service", e); //$NON-NLS-1$
		}
		return null;
	}

	/**
	 * Method for Delete site admin area rel.
	 *
	 * @param siteAARelId
	 *            {@link String}
	 * @return true, if successful
	 */
	public boolean deleteSiteAdminAreaRel(String siteAARelId) throws UnauthorizedAccessException {
		try {
			String url = new String(
					this.serviceUrl + XMAdminRestClientConstants.DELETE_SITE_ADMIN_AREA_REL + "/" + siteAARelId);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(this.headers);
			ResponseEntity<Boolean> responseEntity = restTemplate.exchange(url, HttpMethod.DELETE, requestEntity,
					Boolean.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				return responseEntity.getBody();
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM delete Site Administration Area Relation REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM delete Site Administration Area Relation REST Service!", e); //$NON-NLS-1$
		}
		return false;
	}

	/**
	 * Delete multi site admin area rel.
	 *
	 * @param ids the ids
	 * @return the site admin area rel response
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 */
	public SiteAdminAreaRelResponse deleteMultiSiteAdminAreaRel(Set<String> ids) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.DELETE_MULTI_SITE_ADMIN_AREA_REL);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Set<String>> requestEntity = new HttpEntity<Set<String>>(ids, this.headers);
			ResponseEntity<SiteAdminAreaRelResponse> responseEntity = restTemplate.exchange(url, HttpMethod.DELETE,
					requestEntity, SiteAdminAreaRelResponse.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				SiteAdminAreaRelResponse siteAdminAreaRelResponse = responseEntity.getBody();
				return siteAdminAreaRelResponse;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM delete Multi SiteAdminAreaRel REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM delete Multi SiteAdminAreaRel REST Service!", e); //$NON-NLS-1$
		}
		return null;
	}
	
	/**
	 * Method for Update site admin area rel status.
	 *
	 * @param siteAARelId
	 *            {@link String}
	 * @param status
	 *            {@link String}
	 * @return true, if successful
	 */
	public boolean updateSiteAdminAreaRelStatus(String siteAARelId, String status) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.UPDATE_SITE_ADMIN_AREA_REL_STATUS + "/"
					+ status + "/" + siteAARelId);

			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(this.headers);
			ResponseEntity<Boolean> responseEntity = restTemplate.exchange(url, HttpMethod.POST, requestEntity,
					Boolean.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				return responseEntity.getBody();
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Update Site Administration Area Relation Status REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Update Site Administration Area Relation Status REST Service!", //$NON-NLS-1$
					e);
		}
		return false;
	}

	/**
	 * Multi update site admin area rel status.
	 *
	 * @param siteAdminAreaRelRequest the site admin area rel request
	 * @return the site admin area rel batch response
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 */
	public SiteAdminAreaRelBatchResponse multiUpdateSiteAdminAreaRelStatus(
			List<SiteAdminAreaRelRequest> siteAdminAreaRelRequest) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.MULTI_UPDATE_SITE_AA_STATUS);

			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<List<SiteAdminAreaRelRequest>> requestEntity = new HttpEntity<List<SiteAdminAreaRelRequest>>(
					siteAdminAreaRelRequest, this.headers);
			ResponseEntity<SiteAdminAreaRelBatchResponse> responseEntity = restTemplate.exchange(url, HttpMethod.POST,
					requestEntity, SiteAdminAreaRelBatchResponse.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				return responseEntity.getBody();
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Multi Update siteAdminArea Status REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Multi Update siteAdminArea Status REST Service!", e); //$NON-NLS-1$
		}
		return null;
	}
	
	/**
	 * Gets the all projects by site admin rel.
	 *
	 * @param siteAdminRelId
	 *            {@link String}
	 * @return the all projects by site admin rel
	 */
	public List<SiteAdminAreaRelIdWithAdminAreaProjRel> getAllProjectsBySiteAdminRel(final String siteAdminRelId)
			throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.GET_PROJECTS_BY_SITE_ADMIN_REL_ID + "/"
					+ siteAdminRelId);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<SiteAdminAreaRelIdWithAdminAreaProjRelResponse> responseList = restTemplate.exchange(url,
					HttpMethod.GET, requestEntity, SiteAdminAreaRelIdWithAdminAreaProjRelResponse.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				SiteAdminAreaRelIdWithAdminAreaProjRelResponse siteResponseObject = responseList.getBody();
				List<SiteAdminAreaRelIdWithAdminAreaProjRel> siteTblList = (List<SiteAdminAreaRelIdWithAdminAreaProjRel>) siteResponseObject
						.getSiteAdminAreaRelIdWithAdminAreaProjRel();
				return RestClientSortingUtil.sortProjectsBySiteAdminRelTbl(siteTblList);
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Get All Projects By siteAdminRelId REST Service, returns the status code: " //$NON-NLS-1$
								+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			} else if(e instanceof ResourceAccessException){
				throw e;
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get All Projects By siteAdminRelId REST Service", e); //$NON-NLS-1$
		}
		return null;
	}

	/**
	 * Gets the all user apps by site admin rel.
	 *
	 * @param siteAdminRelId
	 *            {@link String}
	 * @return the all user apps by site admin rel
	 */
	public List<SiteAdminAreaRelIdWithAdminAreaUsrAppRel> getAllUserAppsBySiteAdminRel(final String siteAdminRelId)
			throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.GET_USER_APPS_BY_SITE_ADMIN_REL_ID
					+ "/" + siteAdminRelId);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<SiteAdminAreaRelIdWithAdminAreaUsrAppRelResponse> responseList = restTemplate.exchange(url,
					HttpMethod.GET, requestEntity, SiteAdminAreaRelIdWithAdminAreaUsrAppRelResponse.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				SiteAdminAreaRelIdWithAdminAreaUsrAppRelResponse userAppResponseObject = responseList.getBody();
				List<SiteAdminAreaRelIdWithAdminAreaUsrAppRel> siteAdminAreaRelIdWithAdminAreaUsrAppRel = userAppResponseObject
						.getSiteAdminAreaRelIdWithAdminAreaUsrAppRel();
				return RestClientSortingUtil.sortSiteAdminAreaRelIdWithAdminAreaUsrAppRelTbl(siteAdminAreaRelIdWithAdminAreaUsrAppRel);
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Get All User Applications By siteAdminRelId REST Service, returns the status code: " //$NON-NLS-1$
								+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			} else if(e instanceof ResourceAccessException){
				throw e;
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get All User Applications By siteAdminRelId REST Service", e); //$NON-NLS-1$
		}
		return null;
	}

	/**
	 * Gets the all user apps by site admin rel.
	 *
	 * @param siteAdminRelId
	 *            {@link String}
	 * @param relationType
	 *            {@link String}
	 * @return the all user apps by site admin rel
	 */
	public List<SiteAdminAreaRelIdWithAdminAreaUsrAppRel> getAllUserAppsBySiteAdminRel(final String siteAdminRelId,
			final String relationType) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.GET_USER_APPS_BY_SITE_ADMIN_REL_ID
					+ "/" + siteAdminRelId + "/" + relationType);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<SiteAdminAreaRelIdWithAdminAreaUsrAppRelResponse> responseList = restTemplate.exchange(url,
					HttpMethod.GET, requestEntity, SiteAdminAreaRelIdWithAdminAreaUsrAppRelResponse.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				SiteAdminAreaRelIdWithAdminAreaUsrAppRelResponse userAppResponseObject = responseList.getBody();
				List<SiteAdminAreaRelIdWithAdminAreaUsrAppRel> siteAdminAreaRelIdWithAdminAreaUsrAppRel = userAppResponseObject
						.getSiteAdminAreaRelIdWithAdminAreaUsrAppRel();
				return RestClientSortingUtil.sortSiteAdminAreaRelIdWithAdminAreaUsrAppRelTbl(siteAdminAreaRelIdWithAdminAreaUsrAppRel);
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Get All User Applications By siteAdminRelId REST Service, returns the status code: " //$NON-NLS-1$
								+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			}  else if(e instanceof ResourceAccessException){
				throw e;
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get All User Applications By siteAdminRelId REST Service", e); //$NON-NLS-1$
		}
		return null;
	}

	/**
	 * Gets the all start apps by site admin rel.
	 *
	 * @param siteAdminRelId
	 *            {@link String}
	 * @return the all start apps by site admin rel
	 */
	public List<SiteAdminAreaRelIdWithAdminAreaStartAppRel> getAllStartAppsBySiteAdminRel(final String siteAdminRelId)
			throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.GET_START_APPS_BY_SITE_ADMIN_REL_ID
					+ "/" + siteAdminRelId);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<SiteAdminAreaRelIdWithAdminAreaStartAppRelResponse> responseList = restTemplate.exchange(url,
					HttpMethod.GET, requestEntity, SiteAdminAreaRelIdWithAdminAreaStartAppRelResponse.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				SiteAdminAreaRelIdWithAdminAreaStartAppRelResponse startAppResponseObject = responseList.getBody();
				List<SiteAdminAreaRelIdWithAdminAreaStartAppRel> siteAdminAreaRelIdWithAdminAreaStartAppRel = startAppResponseObject
						.getSiteAdminAreaRelIdWithAdminAreaStartAppRel();
				return RestClientSortingUtil.sortStartAppsBySiteAdminRelTbl(siteAdminAreaRelIdWithAdminAreaStartAppRel);
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Get All Start Applications By siteAdminRelId REST Service, returns the status code: " //$NON-NLS-1$
								+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			} else if(e instanceof ResourceAccessException){
				throw e;
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get All Start Applications By siteAdminRelId REST Service", e); //$NON-NLS-1$
		}
		return null;
	}

	/**
	 * Gets the site admin area rel by site id.
	 *
	 * @param siteId
	 *            {@link String}
	 * @return the site admin area rel by site id
	 */
	public List<SiteAdminAreaRelTbl> getSiteAdminAreaRelBySiteId(final String siteId)
			throws UnauthorizedAccessException {
		try {
			String url = new String(
					this.serviceUrl + XMAdminRestClientConstants.GET_SITE_ADMIN_AREA_REL_BY_SITE_ID + "/" + siteId);
			List<SiteAdminAreaRelTbl> siteAdminAreaRelTblList = new ArrayList<>();
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<SiteAdminAreaRelResponse> responseList = restTemplate.exchange(url, HttpMethod.GET,
					requestEntity, SiteAdminAreaRelResponse.class);

			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				SiteAdminAreaRelResponse siteAdminAreaRelResponse = responseList.getBody();

				if (siteAdminAreaRelResponse != null) {
					Iterable<SiteAdminAreaRelTbl> siteAdminRelTbls = siteAdminAreaRelResponse.getSiteAdminAreaRelTbls();
					for (SiteAdminAreaRelTbl siteAdminTbl : siteAdminRelTbls) {
						siteAdminAreaRelTblList.add(siteAdminTbl);
					}
				}
				return RestClientSortingUtil.sortSiteAdminAreaTbl(siteAdminAreaRelTblList);
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Get All Site Admin Area By Relation By siteId REST Service, returns the status code: " //$NON-NLS-1$
								+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			} else if(e instanceof ResourceAccessException){
				throw e;
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get All Site Admin Area By Relation By siteId REST Service", e); //$NON-NLS-1$
		}
		return null;
	}

	/**
	 * Gets the site admin area rel by AA id.
	 *
	 * @param adminAreaId
	 *            the admin area id
	 * @return the site admin area rel by AA id
	 * @throws UnauthorizedAccessException
	 *             the unauthorized access exception
	 */
	public SiteAdminAreaRelResponse getSiteAdminAreaRelByAAId(final String adminAreaId)
			throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl
					+ XMAdminRestClientConstants.GET_SITE_ADMIN_AREA_REL_BY_ADMIN_AREA_ID + "/" + adminAreaId);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<SiteAdminAreaRelResponse> responseList = restTemplate.exchange(url, HttpMethod.GET,
					requestEntity, SiteAdminAreaRelResponse.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				SiteAdminAreaRelResponse siteAdminAreaRelResponse = responseList.getBody();
				return siteAdminAreaRelResponse;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Get All Site Admin Area By Relation By admin area Id REST Service, returns the status code: " //$NON-NLS-1$
								+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			}
		} catch (Exception e) {
			LOGGER.error(
					"Error while calling XMSYSTEM Get All Site Admin Area By Relation By admin area Id REST Service", //$NON-NLS-1$
					e);
		}
		return null;
	}
}
