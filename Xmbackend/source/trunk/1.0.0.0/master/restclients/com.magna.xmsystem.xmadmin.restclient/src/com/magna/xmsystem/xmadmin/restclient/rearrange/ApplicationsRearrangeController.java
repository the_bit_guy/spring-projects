package com.magna.xmsystem.xmadmin.restclient.rearrange;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.magna.xmbackend.exception.CannotCreateObjectException;
import com.magna.xmbackend.vo.rearrange.RearrangeApplicationsRequest;
import com.magna.xmbackend.vo.rearrange.RearrangeApplicationsResponse;
import com.magna.xmsystem.dependencies.customExceptions.UnauthorizedAccessException;
import com.magna.xmsystem.xmadmin.restclient.RestController;
import com.magna.xmsystem.xmadmin.restclient.util.RestClientExpHandlerUtil;
import com.magna.xmsystem.xmadmin.restclient.util.XMAdminRestClientConstants;

/**
 * The Class ApplicationsRearrangeController.
 */
public class ApplicationsRearrangeController extends RestController {
	
	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(ApplicationsRearrangeController.class);
	
	/**
	 * Instantiates a new applications rearrange controller.
	 */
	public ApplicationsRearrangeController() {
		super();
	}
	
	/**
	 * Creates the or update rearrange setting.
	 *
	 * @param rearrangeApplicationsRequest the rearrange applications request
	 * @return true, if successful
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 * @throws CannotCreateObjectException the cannot create object exception
	 */
	public boolean createOrUpdateRearrangeSetting(final RearrangeApplicationsRequest rearrangeApplicationsRequest)
			throws UnauthorizedAccessException, CannotCreateObjectException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.CREATE_UPDATE_REARRANGE_SETTINGS);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<RearrangeApplicationsRequest> request = new HttpEntity<RearrangeApplicationsRequest>(rearrangeApplicationsRequest, this.headers);
			ResponseEntity<Boolean> responseEntity = restTemplate.postForEntity(url, request, Boolean.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				Boolean isCreated = responseEntity.getBody();
				return isCreated;
			} else {
				LOGGER.error("Error while calling XMSYSTEM Create OR update rearrange settings REST Service, returns the status code: ", statusCode.value()); //$NON-NLS-1$
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				String errorResponse = ((HttpStatusCodeException) e).getResponseBodyAsString();
				String errCode = RestClientExpHandlerUtil.handleCannotCreateObjectException(LOGGER,
						(HttpStatusCodeException) e);
				throw new CannotCreateObjectException(errorResponse, errCode);
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Create OR update rearrange settings REST Service", e); //$NON-NLS-1$
		}
		return false;
	}
	
	/**
	 * Gets the css file existence res.
	 *
	 * @return the css file existence res
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 * @throws CannotCreateObjectException the cannot create object exception
	 */
	public RearrangeApplicationsResponse getRearrangeSettingsRes(final RearrangeApplicationsRequest rearrangeApplicationsRequest) throws UnauthorizedAccessException, CannotCreateObjectException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.FIND_REARRANGE_SETTINGS);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<RearrangeApplicationsRequest> requestEntity = new HttpEntity<RearrangeApplicationsRequest>(rearrangeApplicationsRequest, this.headers);
 			ResponseEntity<RearrangeApplicationsResponse> responseList = restTemplate.exchange(url, HttpMethod.POST, requestEntity, RearrangeApplicationsResponse.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				RearrangeApplicationsResponse rearrangeApplicationsResponse = responseList.getBody();
				return rearrangeApplicationsResponse;
			} else {
				LOGGER.error("Error while calling XMSYSTEM getting rearrange settings Service, returns the status code: " + statusCode.value()); //$NON-NLS-1$
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				} else if (e instanceof HttpStatusCodeException) {
					RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
				}
			}  else if (e instanceof HttpStatusCodeException) {
				String errorResponse = ((HttpStatusCodeException) e).getResponseBodyAsString();
				String errCode = RestClientExpHandlerUtil.handleCannotCreateObjectException(LOGGER,
						(HttpStatusCodeException) e);
				throw new CannotCreateObjectException(errorResponse, errCode);
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM getting rearrange setting REST Service", e); //$NON-NLS-1$
		}
		return null;
	}
	
	/**
	 * Delete reaarange setting.
	 *
	 * @return true, if successful
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 * @throws CannotCreateObjectException the cannot create object exception
	 */
	public boolean deleteReaarangeSetting() throws UnauthorizedAccessException, CannotCreateObjectException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.DELETE_REARRANGE_SETTINGS);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(this.headers);
			ResponseEntity<Boolean> responseEntity = restTemplate.exchange(url, HttpMethod.DELETE, requestEntity,
					Boolean.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				return responseEntity.getBody();
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM deleting rearrange settings REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		}  catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM deleting rearrange Settings REST Service!", e); //$NON-NLS-1$
		}
		return false;
	}
	
}
