package com.magna.xmsystem.xmadmin.restclient.relation;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.magna.xmbackend.entities.AdminAreaProjAppRelTbl;
import com.magna.xmbackend.exception.CannotCreateObjectException;
import com.magna.xmbackend.response.rel.adminareaprojectapp.AdminAreaProjectAppResponse;
import com.magna.xmbackend.response.rel.adminareaprojectapp.AdminAreaProjectAppResponseWrapper;
import com.magna.xmbackend.vo.rel.AdminAreaProjectAppRelBatchRequest;
import com.magna.xmbackend.vo.rel.AdminAreaProjectAppRelBatchResponse;
import com.magna.xmbackend.vo.rel.AdminAreaProjectAppRelRequest;
import com.magna.xmbackend.vo.rel.AdminAreaProjectAppRelResponse;
import com.magna.xmbackend.vo.rel.AdminAreaStartAppRelRequest;
import com.magna.xmbackend.vo.rel.AdminAreaStartAppRelResponse;
import com.magna.xmsystem.dependencies.customExceptions.UnauthorizedAccessException;
import com.magna.xmsystem.xmadmin.restclient.RestController;
import com.magna.xmsystem.xmadmin.restclient.site.SiteController;
import com.magna.xmsystem.xmadmin.restclient.util.RestClientExpHandlerUtil;
import com.magna.xmsystem.xmadmin.restclient.util.RestClientSortingUtil;
import com.magna.xmsystem.xmadmin.restclient.util.XMAdminRestClientConstants;

// TODO: Auto-generated Javadoc
/**
 * Class for Site admin area rel controller.
 *
 * @author Roshan.Ekka
 */
public class AdminAreaProjectAppRelController extends RestController {
	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(SiteController.class);

	/**
	 * Constructor for SiteController Class.
	 */
	public AdminAreaProjectAppRelController() {
		super();
	}

	/**
	 * Constructor for AdminAreaProjectAppRelController Class.
	 *
	 * @param adminAreaId {@link String}
	 */
	public AdminAreaProjectAppRelController(String adminAreaId) {
		super(adminAreaId);
	}

	/**
	 * Method for Creates the admin area proj app rel.
	 *
	 * @param adminAreaProjectAppRelRequest
	 *            {@link AdminAreaProjectAppRelRequest}
	 * @return the admin area proj app rel tbl {@link AdminAreaProjAppRelTbl}
	 * @throws UnauthorizedAccessException
	 *             the unauthorized access exception
	 * @throws CannotCreateObjectException
	 *             the cannot create object exception
	 */
	public AdminAreaProjAppRelTbl createAdminAreaProjAppRel(
			final AdminAreaProjectAppRelRequest adminAreaProjectAppRelRequest)
			throws UnauthorizedAccessException, CannotCreateObjectException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.CREATE_ADMIN_AREA_PROJ_APP_REL);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<AdminAreaProjectAppRelRequest> request = new HttpEntity<AdminAreaProjectAppRelRequest>(
					adminAreaProjectAppRelRequest, this.headers);
			ResponseEntity<AdminAreaProjAppRelTbl> responseEntity = restTemplate.postForEntity(url, request,
					AdminAreaProjAppRelTbl.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				AdminAreaProjAppRelTbl adminAreaProjAppRelTbl = responseEntity.getBody();
				return adminAreaProjAppRelTbl;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Create Administration Area Project Application Relation REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				String errorResponse = ((HttpStatusCodeException) e).getResponseBodyAsString();
				String errCode = RestClientExpHandlerUtil.handleCannotCreateObjectException(LOGGER,
						(HttpStatusCodeException) e);
				throw new CannotCreateObjectException(errorResponse, errCode);
			}
		} catch (Exception e) {
			LOGGER.error(
					"Error while calling XMSYSTEM Create Administration Area Project Application Relation REST Service", //$NON-NLS-1$
					e);
		}
		return null;
	}

	/**
	 * Method for Creates the admin area proj app rel.
	 *
	 * @param adminAreaProjectAppRelBatchRequest
	 *            {@link AdminAreaProjectAppRelBatchRequest}
	 * @return the admin area project app rel batch response
	 *         {@link AdminAreaProjectAppRelBatchResponse}
	 * @throws UnauthorizedAccessException
	 *             the unauthorized access exception
	 * @throws CannotCreateObjectException
	 *             the cannot create object exception
	 */
	public AdminAreaProjectAppRelBatchResponse createAdminAreaProjAppRel(
			AdminAreaProjectAppRelBatchRequest adminAreaProjectAppRelBatchRequest)
			throws UnauthorizedAccessException, CannotCreateObjectException {
		try {
			String url = new String(
					this.serviceUrl + XMAdminRestClientConstants.CREATE_MULTI_PROJECT_APP_ADMIN_AREA_REL);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<AdminAreaProjectAppRelBatchRequest> request = new HttpEntity<AdminAreaProjectAppRelBatchRequest>(
					adminAreaProjectAppRelBatchRequest, this.headers);
			ResponseEntity<AdminAreaProjectAppRelBatchResponse> responseEntity = restTemplate.postForEntity(url,
					request, AdminAreaProjectAppRelBatchResponse.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				AdminAreaProjectAppRelBatchResponse adminAreaProjectAppRelBatchResponse = responseEntity.getBody();
				return adminAreaProjectAppRelBatchResponse;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Create Multi Administration Area Project Application Relation REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				String errorResponse = ((HttpStatusCodeException) e).getResponseBodyAsString();
				String errCode = RestClientExpHandlerUtil.handleCannotCreateObjectException(LOGGER,
						(HttpStatusCodeException) e);
				throw new CannotCreateObjectException(errorResponse, errCode);
			} else if (e instanceof ResourceAccessException) {
				throw e;
			}
		} catch (Exception e) {
			LOGGER.error(
					"Error while calling XMSYSTEM Create Multi Administration Area Project Application Relation REST Service", //$NON-NLS-1$
					e);
		}
		return null;
	}

	/**
	 * Gets the all admin area proj app rel.
	 *
	 * @param isSorted            {@link boolean}
	 * @return the all admin area proj app rel
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 */
	public List<AdminAreaProjAppRelTbl> getAllAdminAreaProjAppRel(final boolean isSorted)
			throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.GET_ADMIN_AREA_PROJECT_APP_REL);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<AdminAreaProjectAppRelResponse> responseList = restTemplate.exchange(url, HttpMethod.GET,
					requestEntity, AdminAreaProjectAppRelResponse.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				AdminAreaProjectAppRelResponse siteResponseObject = responseList.getBody();
				List<AdminAreaProjAppRelTbl> siteTblList = (List<AdminAreaProjAppRelTbl>) siteResponseObject
						.getAdminAreaProjAppRelTbls();
				/*
				 * if (isSorted) { siteTblList.sort((siteResponse1,
				 * siteResponse2) -> {
				 * Collection<com.magna.xmbackend.entities.SiteTranslationTbl>
				 * siteTranslation1;
				 * Collection<com.magna.xmbackend.entities.SiteTranslationTbl>
				 * siteTranslation2; if ((siteTranslation1 =
				 * siteResponse1.getSiteTranslationTblCollection()) != null &&
				 * (siteTranslation2 =
				 * siteResponse2.getSiteTranslationTblCollection()) != null) {
				 * String siteName1 = getNameByLanguage(LANG_ENUM.ENGLISH,
				 * siteTranslation1); String siteName2 =
				 * getNameByLanguage(LANG_ENUM.ENGLISH, siteTranslation2);
				 * return siteName1.compareToIgnoreCase(siteName2); } return 0;
				 * }); }
				 */
				return siteTblList;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Get All Administration Area Project Application Relation REST Service, returns the status code: " //$NON-NLS-1$
								+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			}
		} catch (Exception e) {
			LOGGER.error(
					"Error while calling XMSYSTEM Get All Administration Area Project Application Relation REST Service", //$NON-NLS-1$
					e);
		}
		return null;
	}

	/*
	 * private String getNameByLanguage(final LANG_ENUM langEnum, final
	 * Collection<SiteTranslationTbl> siteTranslations) { for
	 * (SiteTranslationTbl siteTranslation : siteTranslations) { LANG_ENUM
	 * foundLangEnum =
	 * LANG_ENUM.getLangEnum(siteTranslation.getLanguageCode().getLanguageCode()
	 * ); String name; if (foundLangEnum == langEnum && (name =
	 * siteTranslation.getName()) != null) { if (!XMSystemUtil.isEmpty(name)) {
	 * return name; } } } return ""; }
	 */

	/**
	 * Method for Delete admin area proj app rel.
	 *
	 * @param adminAreaProjAppRelId            {@link String}
	 * @return true, if successful
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 */
	public boolean deleteAdminAreaProjAppRel(final String adminAreaProjAppRelId) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.DELETE_ADMIN_AREA_PROJECT_APP_REL + "/"
					+ adminAreaProjAppRelId);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(this.headers);
			ResponseEntity<Boolean> responseEntity = restTemplate.exchange(url, HttpMethod.DELETE, requestEntity,
					Boolean.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				return responseEntity.getBody();
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM delete Administration Area Project Application Relation REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			}
		} catch (Exception e) {
			LOGGER.error(
					"Error while calling XMSYSTEM delete Administration Area Project Application Relation REST Service!", //$NON-NLS-1$
					e);
		}
		return false;
	}
	
	/**
	 * Delete multi admin area proj app rel.
	 *
	 * @param ids the ids
	 * @return the admin area project app rel response
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 */
	public AdminAreaProjectAppRelResponse deleteMultiAdminAreaProjAppRel(Set<String> ids) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.DELETE_MULTI_ADMIN_AREA_PROJECT_APP_REL);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Set<String>> requestEntity = new HttpEntity<Set<String>>(ids,this.headers);
			ResponseEntity<AdminAreaProjectAppRelResponse> responseEntity = restTemplate.exchange(url, HttpMethod.DELETE, requestEntity,
					AdminAreaProjectAppRelResponse.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				AdminAreaProjectAppRelResponse adminAreaProjectAppRelResponse = responseEntity.getBody();
				return adminAreaProjectAppRelResponse;
			} else {
				LOGGER.error("Error while calling XMSYSTEM delete Multi AdminAreaProjectAppRel REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM delete Multi AdminAreaProjectAppRel REST Service!", e); //$NON-NLS-1$
		}
		return null;
	}

	/**
	 * Method for Update admin area proj app rel status.
	 *
	 * @param adminAreaProjAppRelId            {@link String}
	 * @param status            {@link String}
	 * @return true, if successful
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 */
	public boolean updateAdminAreaProjAppRelStatus(final String adminAreaProjAppRelId, final String status)
			throws UnauthorizedAccessException {
		try {
			final String url = new String(
					this.serviceUrl + XMAdminRestClientConstants.UPDATE_ADMIN_AREA_PROJECT_APP_REL_STATUS + "/" + status
							+ "/" + adminAreaProjAppRelId);

			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(this.headers);
			ResponseEntity<Boolean> responseEntity = restTemplate.exchange(url, HttpMethod.POST, requestEntity,
					Boolean.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				return responseEntity.getBody();
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Update Administration Area Project Application Relation Status REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			}
		} catch (Exception e) {
			LOGGER.error(
					"Error while calling XMSYSTEM Update Administration Area Project Application Relation Status REST Service!", //$NON-NLS-1$
					e);
		}
		return false;
	}
	
	/**
	 * Multi update AA project app rel status.
	 *
	 * @param adminAreaProjectAppRelRequest the admin area start app rel request
	 * @return the admin area project app rel response
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 */
	public AdminAreaProjectAppRelResponse multiUpdateAAProjectAppRelStatus(
			List<AdminAreaProjectAppRelRequest> adminAreaProjectAppRelRequest) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.MULTI_UPDATE_AA_PROJECT_APP_STATUS);

			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<List<AdminAreaProjectAppRelRequest>> requestEntity = new HttpEntity<List<AdminAreaProjectAppRelRequest>>(
					adminAreaProjectAppRelRequest, this.headers);
			ResponseEntity<AdminAreaProjectAppRelResponse> responseEntity = restTemplate.exchange(url, HttpMethod.POST,
					requestEntity, AdminAreaProjectAppRelResponse.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				return responseEntity.getBody();
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Multi Update AdminAreaProjectApp Status REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Multi Update AdminAreaProjectApp Status REST Service!", e); //$NON-NLS-1$
		}
		return null;
	}

	/**
	 * Gets the AA project app by id.
	 *
	 * @param adminAreaProjAppRelId            {@link String}
	 * @return the AA project app by id
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 */
	public AdminAreaProjAppRelTbl getAAProjectAppById(final String adminAreaProjAppRelId)
			throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.GET_ADMIN_AREA_PROJECT_APP_REL_BY_ID
					+ "/" + adminAreaProjAppRelId);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<AdminAreaProjAppRelTbl> responseList = restTemplate.exchange(url, HttpMethod.GET,
					requestEntity, AdminAreaProjAppRelTbl.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				AdminAreaProjAppRelTbl adminAreaProjAppRelTblObject = responseList.getBody();
				return adminAreaProjAppRelTblObject;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Get Administration Area Project Application Relation by Id REST Service, returns the status code: " //$NON-NLS-1$
								+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			}
		} catch (Exception e) {
			LOGGER.error(
					"Error while calling XMSYSTEM Get Administration Area Project Application Relation by Id REST Service", //$NON-NLS-1$
					e);
		}
		return null;
	}

	/**
	 * Gets the AA project app rel by AA proj rel.
	 *
	 * @param adminAreaProjRelId            {@link String}
	 * @return the AA project app rel by AA proj rel
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 */
	public AdminAreaProjectAppRelResponse getAAProjectAppRelByAAProjRel(final String adminAreaProjRelId)
			throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl
					+ XMAdminRestClientConstants.GET_ADMIN_AREA_PROJECT_APP_REL_BY_ADMIN_AREA_PROJ_REL_ID + "/"
					+ adminAreaProjRelId);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<AdminAreaProjectAppRelResponse> responseList = restTemplate.exchange(url, HttpMethod.GET,
					requestEntity, AdminAreaProjectAppRelResponse.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				AdminAreaProjectAppRelResponse adminAreaProjectAppRelResponse = responseList.getBody();
				return adminAreaProjectAppRelResponse;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Get Administration Area Project Application Relation by adminAreaProjRelId REST Service, returns the status code: " //$NON-NLS-1$
								+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			}
		} catch (Exception e) {
			LOGGER.error(
					"Error while calling XMSYSTEM Get Administration Area Project Application Relation by adminAreaProjRelId REST Service", //$NON-NLS-1$
					e);
		}
		return null;
	}

	/**
	 * Gets the AA project app rel by AA ID and Project Id.
	 *
	 * @param adminAreaId
	 *            {@link String} * @param porjId {@link String}
	 * @param porjId
	 *            the porj id
	 * @return the AA project app rel by AA ID and Project Id
	 * @throws UnauthorizedAccessException
	 *             the unauthorized access exception
	 */
	public AdminAreaProjectAppRelResponse getAdminAreaProjAppRelationsByAdminAreaIdAndProjId(final String adminAreaId,
			final String porjId) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl
					+ XMAdminRestClientConstants.GET_ADMIN_AREA_PROJECT_APP_REL_BY_ADMIN_AREA_ID_AND_PROJ_ID + "/"
					+ adminAreaId + "/" + porjId);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<AdminAreaProjectAppRelResponse> responseList = restTemplate.exchange(url, HttpMethod.GET,
					requestEntity, AdminAreaProjectAppRelResponse.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				AdminAreaProjectAppRelResponse adminAreaProjectAppRelResponse = responseList.getBody();
				return adminAreaProjectAppRelResponse;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Get Administration Area Project Application Relation by adminAreaProjRelId REST Service, returns the status code: " //$NON-NLS-1$
								+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			}
		} catch (Exception e) {
			LOGGER.error(
					"Error while calling XMSYSTEM Get Administration Area Project Application Relation by adminAreaProjRelId REST Service", //$NON-NLS-1$
					e);
		}
		return null;
	}

	/**
	 * Gets the admin area by proj app id.
	 *
	 * @param projectAppId
	 *            {@link String}
	 * @return the admin area by proj app id
	 * @throws UnauthorizedAccessException
	 *             the unauthorized access exception
	 */
	public List<AdminAreaProjectAppResponse> getAdminAreaByProjAppId(final String projectAppId)
			throws UnauthorizedAccessException {
		try {
			String url = new String(
					this.serviceUrl + XMAdminRestClientConstants.GET_ADMIN_AREA_BY_PROJ_APP_ID + "/" + projectAppId);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<AdminAreaProjectAppResponseWrapper> responseList = restTemplate.exchange(url, HttpMethod.GET,
					requestEntity, AdminAreaProjectAppResponseWrapper.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				AdminAreaProjectAppResponseWrapper adminAreaProjectAppResponseWrapper = responseList.getBody();
				
				List<AdminAreaProjectAppResponse> adminAreaProjectAppResponse = adminAreaProjectAppResponseWrapper.getAdminAreaProjectAppResponse();
				return RestClientSortingUtil.sortAdminAreaByProjAppIdTbl(adminAreaProjectAppResponse);
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Get Administration Area  by projectAppId REST Service, returns the status code: " //$NON-NLS-1$
								+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			} else if(e instanceof ResourceAccessException){
				throw e;
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get Administration Area  by projectAppId REST Service", e); //$NON-NLS-1$
		}
		return null;
	}

	
	/**
	 * Method for Update rel types by ids.
	 *
	 * @param adminAreaProjectAppRelBatchRequest
	 *            {@link AdminAreaProjectAppRelBatchRequest}
	 * @return the admin area project app rel batch response
	 *         {@link AdminAreaProjectAppRelBatchResponse}
	 * @throws UnauthorizedAccessException
	 *             the unauthorized access exception
	 * @throws CannotCreateObjectException
	 *             the cannot create object exception
	 */
	public AdminAreaProjectAppRelBatchResponse updateRelTypesByIds(
			final AdminAreaProjectAppRelBatchRequest adminAreaProjectAppRelBatchRequest)
			throws UnauthorizedAccessException, CannotCreateObjectException {
		try {
			final String url = new String(
					this.serviceUrl + XMAdminRestClientConstants.UPDATE_ADMIN_AREA_PROJECT_APP_REL_TYPE_BY_IDS);
			final RestTemplate restTemplate = new RestTemplate();
			final HttpEntity<AdminAreaProjectAppRelBatchRequest> request = new HttpEntity<AdminAreaProjectAppRelBatchRequest>(
					adminAreaProjectAppRelBatchRequest, this.headers);
			ResponseEntity<AdminAreaProjectAppRelBatchResponse> responseEntity = restTemplate.postForEntity(url,
					request, AdminAreaProjectAppRelBatchResponse.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				AdminAreaProjectAppRelBatchResponse adminAreaProjectAppRelBatchResponse = responseEntity.getBody();
				return adminAreaProjectAppRelBatchResponse;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Update Administartion Area Project Application Relation Types By IDs REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				String errorResponse = ((HttpStatusCodeException) e).getResponseBodyAsString();
				String errCode = RestClientExpHandlerUtil.handleCannotCreateObjectException(LOGGER,
						(HttpStatusCodeException) e);
				throw new CannotCreateObjectException(errorResponse, errCode);
			} else if (e instanceof ResourceAccessException) {
				throw e;
			}
		} catch (Exception e) {
			LOGGER.error(
					"Error while calling XMSYSTEM Update Administartion Area Project Application Relation Types By IDs REST Service", //$NON-NLS-1$
					e);
		}
		return null;
	}
	
	/**
	 * Gets the proj app relations by AA id and project id.
	 *
	 * @param adminAreaId the admin area id
	 * @param projectId the project id
	 * @return the proj app relations by AA id and project id
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 */
	public AdminAreaProjectAppRelResponse getProjAppRelationsByAAIdAndProjectId(final String adminAreaId,
			final String projectId) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl
					+ XMAdminRestClientConstants.GET_PROJECT_BY_AA_PROJECT_RELATION + "/"
					+ adminAreaId + "/" + projectId);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<AdminAreaProjectAppRelResponse> responseList = restTemplate.exchange(url, HttpMethod.GET,
					requestEntity, AdminAreaProjectAppRelResponse.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				AdminAreaProjectAppRelResponse adminAreaProjectAppRelResponse = responseList.getBody();
				return adminAreaProjectAppRelResponse;
			} else {
				LOGGER.error(
						"Error while calling XMSYSTEM Get Project Application Relation by adminArea and Project Id REST Service, returns the status code: " //$NON-NLS-1$
								+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get Project Application Relation by adminArea and Project Id REST Service", e); //$NON-NLS-1$
		}
		return null;
	}
	
	
}
