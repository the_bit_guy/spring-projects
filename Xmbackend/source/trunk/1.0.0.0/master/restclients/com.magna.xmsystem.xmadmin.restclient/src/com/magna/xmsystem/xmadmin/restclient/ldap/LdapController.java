/**
 * 
 */
package com.magna.xmsystem.xmadmin.restclient.ldap;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.magna.xmbackend.vo.ldap.LdapUser;
import com.magna.xmbackend.vo.ldap.LdapUsersResponse;
import com.magna.xmsystem.dependencies.customExceptions.UnauthorizedAccessException;
import com.magna.xmsystem.xmadmin.restclient.RestController;
import com.magna.xmsystem.xmadmin.restclient.util.RestClientExpHandlerUtil;
import com.magna.xmsystem.xmadmin.restclient.util.XMAdminRestClientConstants;

/**
 * The Class LdapController.
 *
 * @author Bhabadyuti Bal
 */
public class LdapController extends RestController {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(LdapController.class);
	
	/**
	 * Instantiates a new ldap controller.
	 */
	public LdapController() {
		super();
	}
	
	/**
	 * Find all ldap user names.
	 *
	 * @return the list
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 */
	public List<String> findAllLdapUserNames() throws UnauthorizedAccessException {
		try {
			final String url = new String(this.serviceUrl + XMAdminRestClientConstants.GET_ALL_LDAP_USERNAMES);
			final RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);
			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<List<String>> responseList = restTemplate.exchange(url, HttpMethod.GET, requestEntity,
					new ParameterizedTypeReference<List<String>>() {});
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				List<String> list = responseList.getBody();
				return list;
			} else {
				LOGGER.error("Error while calling XMSYSTEM Get All LDAP User Names REST Service, returns the status code: " //$NON-NLS-1$
						+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get All LDAP User Names REST Service", e); //$NON-NLS-1$
		}
		return null;
	}
	
	/**
	 * Find all OU names.
	 *
	 * @return the list
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 */
	public List<String> findAllOUNames() throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.GET_ALL_LDAP_OUS);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<List<String>> responseList = restTemplate.exchange(url, HttpMethod.GET, requestEntity,
					new ParameterizedTypeReference<List<String>>() {});
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				List<String> list = responseList.getBody();
				return list;
			} else {
				LOGGER.error("Error while calling XMSYSTEM Get All LDAP OU Names REST Service, returns the status code: " //$NON-NLS-1$
						+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get All LDAP OU Names REST Service", e); //$NON-NLS-1$
		}
		return null;
	}
	
	/**
	 * Find all ldap users.
	 *
	 * @return the list
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 */
	public List<LdapUser> findAllLdapUsers() throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.GET_ALL_LDAP_USERS);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<List<LdapUser>> responseList = restTemplate.exchange(url, HttpMethod.GET, requestEntity,
					new ParameterizedTypeReference<List<LdapUser>>() {});
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				List<LdapUser> ldapUsersList = responseList.getBody();
				return ldapUsersList;
			} else {
				LOGGER.error("Error while calling XMSYSTEM Get All LDAP Users REST Service, returns the status code: " //$NON-NLS-1$
						+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get All LDAP Users REST Service", e); //$NON-NLS-1$
		}
		return null;
	}
	
	
	/**
	 * Find ldap user detail.
	 *
	 * @param userName the user name
	 * @return the ldap user
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 */
	public LdapUser findLdapUserDetail(final String userName) throws UnauthorizedAccessException{
		try {
			final String url = new String(this.serviceUrl + XMAdminRestClientConstants.GET_LDAP_USER_DETAIL+"/"+userName);
			final RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<LdapUser> responseList = restTemplate.exchange(url, HttpMethod.GET, requestEntity,
					LdapUser.class);
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				LdapUser ldapUser = responseList.getBody();
				return ldapUser;
			} else {
				LOGGER.error("Error while calling XMSYSTEM Get All LDAP User Detail Service, returns the status code: " //$NON-NLS-1$
						+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Get All LDAP User Detail REST Service", e); //$NON-NLS-1$
		}
		return null;
	}
	
	
	
	/**
	 * Search all matching user names.
	 *
	 * @param name the name
	 * @return the list
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 */
	public List<String> searchAllMatchingUserNames(final String name) throws UnauthorizedAccessException {
		try {
			final String url = new String(this.serviceUrl + XMAdminRestClientConstants.SEARCH_LDAP_USER_NMAES + "/" + name);
			final RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<List<String>> responseList = restTemplate.exchange(url, HttpMethod.GET, requestEntity,
					new ParameterizedTypeReference<List<String>>() {});
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				List<String> list = responseList.getBody();
				return list;
			} else {
				LOGGER.error("Error while calling XMSYSTEM Search LDAP User Names Service, returns the status code: " //$NON-NLS-1$
						+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Search LDAP User Names Service REST Service", e); //$NON-NLS-1$
		}
		return null;
	}
	
	
	/**
	 * Search all matching users.
	 *
	 * @param name the name
	 * @return the list
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 */
	public LdapUsersResponse searchAllMatchingUsers(final String name) throws UnauthorizedAccessException {
		try {
			String url = new String(this.serviceUrl + XMAdminRestClientConstants.SEARCH_LDAP_USER + "/" + name);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);

			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			ResponseEntity<LdapUsersResponse> responseList = restTemplate.exchange(url, HttpMethod.GET, requestEntity,
					new ParameterizedTypeReference<LdapUsersResponse>() {});
			HttpStatus statusCode = responseList.getStatusCode();
			if (statusCode.is2xxSuccessful()) {
				LdapUsersResponse userRespone = responseList.getBody();
				return userRespone;
			} else {
				LOGGER.error("Error while calling XMSYSTEM Search LDAP Users Service, returns the status code: " //$NON-NLS-1$
						+ statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException)e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}else if (e instanceof HttpStatusCodeException) {
					RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
				}
			} 
		} catch (Exception e) {
			LOGGER.error("Error while calling XMSYSTEM Search LDAP Users Service REST Service", e); //$NON-NLS-1$
		}
		return null;
	}
}
