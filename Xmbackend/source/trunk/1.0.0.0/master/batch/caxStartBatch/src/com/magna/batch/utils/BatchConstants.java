package com.magna.batch.utils;
/*
 *This class contains common constants 
 * @author Ardhendu.Sinha
 *
 */
public interface BatchConstants {
	
	final String MessageBundle_US_PATH = "src/resource/MessageBundle_us_US.properties";
	final String MessageBundle_DE_PATH = "src/resource/MessageBundle_de_DE.properties";

}
