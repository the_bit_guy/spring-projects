package com.magna.batch.utils;

import java.util.ArrayList;
import java.util.List;

/**
 * The Class SQLConverter.
 */
public class SQLConverter {
	public static String convertToSql(List<String> argsList) {
		try {
			String sqlQueryStr = "";
			if (argsList.contains("-type")) {
				int indexofType = argsList.indexOf("-type");
				String type = argsList.get(indexofType + 1);
				if (type.equals("base_data")) {
					int indexofSelect = argsList.indexOf("-select");
					if ((indexofSelect - indexofType) > 2) {
						return null;
					}
					String optionType = argsList.get(indexofSelect);
					// add "select" to query
					sqlQueryStr = sqlQueryStr + optionType.split("-")[1];
					int indexOfSelect = argsList.indexOf("-select");
					String objectType = argsList.get(indexOfSelect + 1).toUpperCase();
					if (objectType.equals("USER")) {
						int indexOfUser = argsList.indexOf("user");
						// query for all users
						if (indexOfUser + 1 == argsList.size()) {
							return sqlQueryStr + "  * from USERS_TBL ut ,USER_TRANSLATION_TBL utt , ICONS_TBL it "
									+ "where ut.user_id = utt.user_id AND it.icon_id = ut.icon_id";
						}
						String optionField = argsList.get(indexOfUser + 1);
						if (optionField.equals("-fields")) {
							int indexOfOptionField = argsList.indexOf(optionField);
							// get all columns provided by user
							int beginColumnIndex = indexOfOptionField + 1;
							int index = 0;
							boolean whereClausefound = false;
							boolean usertranslationTableRequired = false;
							boolean iconTableRequired = false;
							for (index = beginColumnIndex; index < argsList.size(); index++) {
								if (argsList.get(index).equals("username")) {
									sqlQueryStr = sqlQueryStr + " " + "ut.username" + ",";
									continue;
								}
								if (argsList.get(index).equals("email")) {
									sqlQueryStr = sqlQueryStr + " " + "ut." + "email_id" + ",";
									continue;
								}
								if (argsList.get(index).equals("status")) {
									sqlQueryStr = sqlQueryStr + " " + "ut." + argsList.get(index) + ",";
									continue;
								}
								if (argsList.get(index).equals("full_name")) {
									sqlQueryStr = sqlQueryStr + " " + "ut." + argsList.get(index) + ",";
									continue;
								}
								if (argsList.get(index).equals("telephone_number")) {
									sqlQueryStr = sqlQueryStr + " " + " ut." + argsList.get(index) + ",";
									continue;
								}
								if (argsList.get(index).equals("icon_id")) {
									sqlQueryStr = sqlQueryStr + " " + "ut." + argsList.get(index) + ",";
									continue;
								}
								if (argsList.get(index).equals("department")) {
									sqlQueryStr = sqlQueryStr + " " + "ut." + argsList.get(index) + ",";
									continue;
								}
								if (argsList.get(index).equals("update_date")) {
									sqlQueryStr = sqlQueryStr + " " + "ut." + argsList.get(index) + ",";
									continue;
								}
								if (argsList.get(index).equals("create_date")) {
									sqlQueryStr = sqlQueryStr + " " + "ut." + argsList.get(index) + " ,";
									continue;
								}
								if (argsList.get(index).equals("language_code")) {
									sqlQueryStr = sqlQueryStr + " " + " utt." + argsList.get(index) + " ,";
									usertranslationTableRequired = true;
									continue;
								}
								if (argsList.get(index).equals("description")) {
									sqlQueryStr = sqlQueryStr + " " + " utt." + argsList.get(index) + ",";
									usertranslationTableRequired = true;
									continue;
								}
								if (argsList.get(index).equals("remarks")) {
									sqlQueryStr = sqlQueryStr + " " + " utt." + argsList.get(index) + ",";
									usertranslationTableRequired = true;
									continue;
								}
								if (argsList.get(index).equals("icon_name")) {
									sqlQueryStr = sqlQueryStr + " " + " it." + argsList.get(index) + ",";
									iconTableRequired = true;
									continue;
								}
								if (argsList.get(index).equals("-where")) {
									whereClausefound = true;
									break;
								}
							}
							if (!whereClausefound) {

								if (iconTableRequired && usertranslationTableRequired) {
									// System.out.println(sqlQueryStr);
									sqlQueryStr = sqlQueryStr.substring(0, sqlQueryStr.length() - 1)
											+ " from users_tbl ut, icons_tbl it, USER_TRANSLATION_TBL utt where "
											+ "ut.USER_ID = utt.USER_ID and ut.ICON_ID = it.ICON_ID";
								} else if (iconTableRequired) {
									sqlQueryStr = sqlQueryStr.substring(0, sqlQueryStr.length() - 1)
											+ " from users_tbl ut, USER_TRANSLATION_TBL utt where "
											+ "ut.USER_ID = utt.USER_ID ";
								} else if (!iconTableRequired && !usertranslationTableRequired) {

									sqlQueryStr = sqlQueryStr.substring(0, sqlQueryStr.length() - 1)
											+ " from users_tbl ut";
								}
								return sqlQueryStr;
							}
							// verify query contains any condition
							int indexOfWhere = argsList.indexOf("-where");
							sqlQueryStr = sqlQueryStr.substring(0, sqlQueryStr.length() - 1);
							String query = " AND ";

							// get all name/value pair
							for (int idx = indexOfWhere + 1; idx < argsList.size(); idx++) {
								if (argsList.get(idx).contains("=")) {
									String elementName = argsList.get(idx).split("=")[0];
									String elementValue = argsList.get(idx).split("=")[1];
									// do not add comma to last name/value pair
									if (idx + 1 == argsList.size()) {
										query = query + " " + elementName + "='" + elementValue + "'";
									} else {

										query = query + " " + elementName + "='" + elementValue + "'" + " AND ";
									}
								}
							}
							sqlQueryStr = sqlQueryStr
									+ " from users_tbl ut,ICONS_TBL it,USER_TRANSLATION_TBL utt where "
									+ "ut.USER_ID = utt.USER_ID and ut.ICON_ID = it.ICON_ID" + query;
							return sqlQueryStr;
						}
						if (indexOfUser + 1 == argsList.size()) {
							return sqlQueryStr;
						}
						String option = argsList.get(indexOfUser + 1);
						if (option.equals("-where")) {
							sqlQueryStr = sqlQueryStr
									+ " * from users_tbl ut,ICONS_TBL it,USER_TRANSLATION_TBL utt where "
									+ "ut.USER_ID = utt.USER_ID and ut.ICON_ID = it.ICON_ID " + "AND";
							int indexOfWhere = argsList.indexOf("-where");
							String element = argsList.get(indexOfWhere + 1);
							if (element.contains("status=")) {
								String[] elementval = element.split("=");
								if (elementval.length != 2) {
									return null;
								}
								if ("status".equalsIgnoreCase(elementval[0])) {
									sqlQueryStr = sqlQueryStr + " " + "ut." + "status = ";
									String elementvalue = element.split("=")[1];
									sqlQueryStr = sqlQueryStr + "'" + elementvalue + "'";
									int indexOfElement = argsList.indexOf(element);
									if (indexOfElement + 1 == argsList.size()) {
										return sqlQueryStr;
									}
									String isAnd = argsList.get(indexOfElement + 1);
									if (isAnd.equalsIgnoreCase("AND")) {
										sqlQueryStr = sqlQueryStr + " " + isAnd;
										int indexOfAnd = argsList.indexOf("AND");
										element = argsList.get(indexOfAnd + 1);
									}
								}
							}
							if (element.contains("name=")) {
								sqlQueryStr = sqlQueryStr + " " + "ut." + "username = ";
								String elementval = element.split("=")[1];
								sqlQueryStr = sqlQueryStr + "'" + elementval + "'";
								int indexOfElement = argsList.indexOf(element);
								if (indexOfElement + 1 == argsList.size()) {
									return sqlQueryStr;
								}
								String optionAND = argsList.get(indexOfElement + 1).toUpperCase();
								if (optionAND.equals("AND")) {
									sqlQueryStr = sqlQueryStr + " " + optionAND;
									int indexOfAnd = argsList.indexOf("AND");
									if (indexOfAnd + 1 == argsList.size())
										return null;
									String elements = argsList.get(indexOfAnd + 1);
									if (element.contains("'"))
										return null;

									// add single quotes to element value
									String elementFirstPart = elements.substring(0, elements.indexOf("=") + 1);
									sqlQueryStr = sqlQueryStr + " " + "ut." + elementFirstPart;

									String elementvaluetpart = "\'" + elements.split("=")[1] + "\'";
									sqlQueryStr = sqlQueryStr + " " + elementvaluetpart;

									int indexOfElements = argsList.indexOf(elements);
									if (indexOfElements + 1 == argsList.size()) {
										return sqlQueryStr;
									}
								}
							}
							int indexOfelement = argsList.indexOf(element);
							String optionName = argsList.get(indexOfelement);
							if (optionName.equals("name")) {
								sqlQueryStr = sqlQueryStr + " " + "UPPER(username) ";
								int indexOfElement = argsList.indexOf(optionName);
								String optionIn = argsList.get(indexOfElement + 1).toUpperCase();
								if (optionIn.equals("IN")) {
									sqlQueryStr = sqlQueryStr + " " + optionIn + " ";
									int indexOfIn = argsList.indexOf("IN");
									

									//String elementFirstName = argsList.get(indexOfIn + 1);
									
									List<String> inParams = new ArrayList<>();
									String inElement = argsList.get(++indexOfIn);
									if (inElement.startsWith("(")) {
										if (inElement.length() > 0) {
											String param = inElement.replace("(", "").replaceAll(",", "");
											if (!BatchUtil.isEmpty(param)) {
												inParams.add(param.replace(")", ""));
											}
										} else {
											inParams.add(inElement.replaceAll(",", ""));
										}
									}
									while(!((indexOfIn + 1) == argsList.size() || inElement.endsWith(")"))) {
										inElement = argsList.get(++indexOfIn);
										if (inElement.length() > 0) {
											String param = inElement.replace(",", "").replace(")", "");
											if (!BatchUtil.isEmpty(param)) {
												inParams.add(param);
											}
										}
									}
									sqlQueryStr = sqlQueryStr + "(";
									for (int index = 0; index < inParams.size(); index++) {
										String param = inParams.get(index);
										sqlQueryStr = sqlQueryStr + " \'" + param.toUpperCase() + "\'";
										if ((index + 1) != inParams.size()) {
											sqlQueryStr = sqlQueryStr + ",";
										}
									}
									
									sqlQueryStr = sqlQueryStr + ")";
									
									String optionAnd = argsList.get(++indexOfIn);
									if (optionAnd.equals("AND")) {
										sqlQueryStr = sqlQueryStr + " " + optionAnd;
										int indexOfAnd = argsList.indexOf("AND");
										if (indexOfAnd + 1 == argsList.size())
											return null;
										element = argsList.get(indexOfAnd + 1);
										if (element.contains("'")) {
											return null;
										}
										++indexOfIn;
										// add single quotes to element value
										String elementFirstPart = element.substring(0, element.indexOf("=") + 1);
										sqlQueryStr = sqlQueryStr + " " + elementFirstPart;

										String elementvaluetpart = "\'" + element.split("=")[1] + "\'";
										sqlQueryStr = sqlQueryStr + " " + elementvaluetpart;

										int indexOfElements = argsList.indexOf(element);
										if (indexOfElements + 1 == argsList.size()) {
											// add a semicolon to end of query
											// string
											return sqlQueryStr;
										}
									}
									optionField = argsList.get(++indexOfIn);
									if (optionField.equals("-fields")) {
										int indexOfOptionField = argsList.indexOf(optionField);
										String OptionDistinct = argsList.get(indexOfOptionField + 1);
										if (OptionDistinct.equals("DISTINCT")) {
											sqlQueryStr = sqlQueryStr + " " + OptionDistinct;
											int indexOfDistinct = argsList.indexOf("DISTINCT");
											for (int index = indexOfDistinct + 1; index < argsList.size(); index++) {
												if (argsList.get(index).equals("name") || argsList.get(index).equals("username")) {
													sqlQueryStr = sqlQueryStr + " " + "username";
												} else if (argsList.get(index).equals("email")) {
													sqlQueryStr = sqlQueryStr + " " + "email_id ";
												} else if (argsList.get(index).equals("icon_id") || argsList.get(index).equals("icon_name")) {
													sqlQueryStr = sqlQueryStr + " " + "icon_name ";
												} else {
													sqlQueryStr = sqlQueryStr + " " + argsList.get(index);
												}
											}
											// place distinct after select
											// for valid sql statement
											int beginIndex = sqlQueryStr.indexOf("DISTINCT");
											String sqlQuery = "Select " + sqlQueryStr.substring(beginIndex);
											//sqlQuery = sqlQuery.substring(0, sqlQuery.length() - 2);
											int bindex = sqlQueryStr.indexOf("from");
											sqlQuery = sqlQuery + " " + sqlQueryStr.substring(bindex, beginIndex);
											return sqlQuery;
										}
									}

									// add single quote
									/*int size = elementFirstName.length();
									String firstchar = elementFirstName.substring(0, 1);
									String otherChars = elementFirstName.substring(1, size);

									String firstName = firstchar + "\'" + otherChars + "\'";
									sqlQueryStr = sqlQueryStr + " " + firstName.toUpperCase();
									int indexOfElementFirstName = argsList.indexOf(elementFirstName);
									if (indexOfElementFirstName + 1 == argsList.size()) {
										sqlQueryStr = sqlQueryStr + ")";
										return sqlQueryStr;
									}
									int indexOfElementNextName = 0;
									String Optioncomma = argsList.get(indexOfElementFirstName + 1);
									if (Optioncomma.equals("AND")) {
										sqlQueryStr = sqlQueryStr + ") " + Optioncomma;
										int indexOfAnd = argsList.indexOf("AND");
										if (indexOfAnd + 1 == argsList.size())
											return null;
										element = argsList.get(indexOfAnd + 1);
										if (element.contains("'"))
											return null;
										// add single quotes to element value
										String elementFirstPart = element.substring(0, element.indexOf("=") + 1);
										sqlQueryStr = sqlQueryStr + " " + elementFirstPart;

										String elementvaluetpart = "\'" + element.split("=")[1] + "\'";
										sqlQueryStr = sqlQueryStr + " " + elementvaluetpart;

										int indexOfElements = argsList.indexOf(element);
										if (indexOfElements + 1 == argsList.size()) {
											// add a semicolon to end of query
											// string
											return sqlQueryStr;
										}
									}*/
									/*sqlQueryStr = sqlQueryStr + " " + Optioncomma;
									int IndexOfComma = argsList.indexOf(Optioncomma);
									String elementNextName = argsList.get(IndexOfComma + 1);
									String elementAndName = elementNextName;
									if (!elementNextName.toUpperCase().equals("AND")) {
										// add single quote
										String lastChar = elementNextName.substring(elementNextName.length() - 1);
										String otherchars = elementNextName.substring(0, elementNextName.length() - 1);
										String nextName = " \'" + otherchars + "\'" + lastChar;
										sqlQueryStr = sqlQueryStr + " " + nextName.toUpperCase();
										indexOfElementNextName = argsList.indexOf(elementNextName);
										if (indexOfElementNextName + 1 == argsList.size()) {

											return sqlQueryStr;
										}
									}*/
									/*String elementAndName = argsList.get(indexOfIn + 1);
									// verify any clause exist
									String optionAnd = elementAndName.toUpperCase();
									if (optionAnd.equals("AND")) {
										sqlQueryStr = sqlQueryStr + " " + optionAnd;
										int indexOfAnd = argsList.indexOf("AND");
										if (indexOfAnd + 1 == argsList.size())
											return null;
										element = argsList.get(indexOfAnd + 1);
										if (element.contains("'"))
											return null;
										// add single quotes to element value
										String elementFirstPart = element.substring(0, element.indexOf("=") + 1);
										sqlQueryStr = sqlQueryStr + " " + elementFirstPart;

										String elementvaluetpart = "\'" + element.split("=")[1] + "\'";
										sqlQueryStr = sqlQueryStr + " " + elementvaluetpart;

										int indexOfElements = argsList.indexOf(element);
										if (indexOfElements + 1 == argsList.size()) {
											// add a semicolon to end of query
											// string
											return sqlQueryStr;
										}
									}*/
								}
								if (optionIn.equals("NOT")) {
									sqlQueryStr = sqlQueryStr + " " + optionIn;
									int indexOfNot = argsList.indexOf("NOT");
									String optionLike = argsList.get(indexOfNot + 1).toUpperCase();
									if (optionLike.equals("LIKE")) {
										sqlQueryStr = sqlQueryStr + " " + optionLike;
										int indexOfLike = argsList.indexOf(optionLike);
										String pattern = argsList.get(indexOfLike + 1);
										String patternToUpper = pattern.toUpperCase();
										// System.out.println("pattern"+pattern);
										// add single quotes to pattern to be
										// sql
										// compatible
										sqlQueryStr = sqlQueryStr + " " + "'" + patternToUpper + "'";
										int indexOfPattern = argsList.indexOf(pattern);
										if (indexOfPattern + 1 == argsList.size()) {
											return sqlQueryStr;
										}
										String optionAnd = argsList.get(indexOfPattern + 1).toUpperCase();
										if (optionAnd.equals("AND")) {
											sqlQueryStr = sqlQueryStr + " " + optionAnd;
											int indexOfAnd = argsList.indexOf("AND");
											if (indexOfAnd + 1 == argsList.size())
												return null;
											String elements = argsList.get(indexOfAnd + 1);
											if (elements.contains("'"))
												return null;
											// add single quotes to element
											// value
											String elementFirstPart = elements.substring(0, elements.indexOf("=") + 1);
											sqlQueryStr = sqlQueryStr + " " + elementFirstPart;

											String elementvaluetpart = "\'" + elements.split("=")[1] + "\'";
											sqlQueryStr = sqlQueryStr + " " + elementvaluetpart;
											int indexOfElements = argsList.indexOf(elements);
											if (indexOfElements + 1 == argsList.size()) {
												return sqlQueryStr;
											}
										}
									}
								}
								if (optionIn.equals("LIKE")) {
									sqlQueryStr = sqlQueryStr + " " + optionIn;
									int indexOfLike = argsList.indexOf("LIKE");
									String pattern = argsList.get(indexOfLike + 1);
									// add single quotes to pattern to be sql
									// compatible
									sqlQueryStr = sqlQueryStr + " " + "'" + pattern.toUpperCase() + "'";
									int indexOfPattern = argsList.indexOf(pattern);
									if (indexOfPattern + 1 == argsList.size()) {
										return sqlQueryStr;
									}
									String optionAnd = argsList.get(indexOfPattern + 1).toUpperCase();
									if (optionAnd.equals("AND")) {
										sqlQueryStr = sqlQueryStr + " " + optionAnd;
										int indexOfAnd = argsList.indexOf("AND");
										if (indexOfAnd + 1 == argsList.size())
											return null;

										String elements = argsList.get(indexOfAnd + 1);

										if (elements.contains("'"))
											return null;
										// add single quotes to element value
										String elementFirstPart = elements.substring(0, elements.indexOf("=") + 1);
										sqlQueryStr = sqlQueryStr + " " + elementFirstPart;

										String elementvaluetpart = "\'" + elements.split("=")[1] + "\'";
										sqlQueryStr = sqlQueryStr + " " + elementvaluetpart;
										int indexOfElements = argsList.indexOf(elements);
										if (indexOfElements + 1 == argsList.size()) {
											return sqlQueryStr;
										}
									}

								}
								if (optionIn.equals("ORDER")) {
									sqlQueryStr = sqlQueryStr + " " + optionIn;
									int indexOfOrderOption = argsList.indexOf(optionIn);
									String optionBy = argsList.get(indexOfOrderOption + 1).toUpperCase();
									if (optionBy.equals("BY")) {
										sqlQueryStr = sqlQueryStr + " " + optionBy;
										int indexOfOptionBy = argsList.indexOf(optionBy);
										
										// set the statement to sql compatible
										sqlQueryStr = sqlQueryStr.replace("where username", "");
										sqlQueryStr = sqlQueryStr + " " + "username";
										
										String sortOption = argsList.get(indexOfOptionBy + 1).toUpperCase();
										if (sortOption.equals("ASC") || sortOption.equals("DESC")) {
											sqlQueryStr = sqlQueryStr + " " + sortOption;
										}
									}
									return sqlQueryStr;
								}
							}
						}
						if (option.equals("ORDER")) {
							sqlQueryStr = sqlQueryStr
									+ " * from users_tbl ut,ICONS_TBL it,USER_TRANSLATION_TBL utt where "
									+ "ut.USER_ID = utt.USER_ID and ut.ICON_ID = it.ICON_ID ";
							sqlQueryStr = sqlQueryStr + " " + option;
							int indexOfOrderOption = argsList.indexOf(option);
							String optionBy = argsList.get(indexOfOrderOption + 1).toUpperCase();
							if (optionBy.equals("BY")) {
								sqlQueryStr = sqlQueryStr + " " + optionBy;
								int indexOfOptionBy = argsList.indexOf(optionBy);
								
								// set the statement to sql compatible
								sqlQueryStr = sqlQueryStr.replace("where username", "");
								sqlQueryStr = sqlQueryStr + " " + "username";
								
								String sortOption = argsList.get(indexOfOptionBy + 2).toUpperCase();
								if (sortOption.equals("ASC") || sortOption.equals("DESC")) {
									sqlQueryStr = sqlQueryStr + " " + sortOption;
								} else {
									return null;
								}
							}
							return sqlQueryStr;
						}
					}
				}
				if (type.equals("relation")) {
					int indexOfType = argsList.indexOf("relation");
					String relationType = argsList.get(indexOfType + 1).toUpperCase();
					if (relationType.equals("-USER_PROJECT")) {
						int indexOfRelationType = argsList.indexOf(relationType);
						String option = argsList.get(indexOfRelationType + 1);
						if (option.equals("-where")) {
							int indexOfOption = argsList.indexOf(option);
							String elements = argsList.get(indexOfOption + 1);
							if (elements.contains("project")) {
								String element = elements.split("=")[1];
								sqlQueryStr = "select UT.USERNAME, PT.NAME, UPRT.STATUS FROM USERS_TBL UT,"
										+ "PROJECTS_TBL PT, USER_PROJECT_REL_TBL UPRT WHERE UT.USER_ID = UPRT.USER_ID AND "
										+ " PT.PROJECT_ID = UPRT.PROJECT_ID AND PT.NAME = " + "'" + element + "'";
								return sqlQueryStr;
							}
							if (elements.contains("user")) {
								String element = elements.split("=")[1];
								sqlQueryStr = "select UT.USERNAME, PT.NAME, UPRT.STATUS FROM USERS_TBL UT,"
										+ "PROJECTS_TBL PT, USER_PROJECT_REL_TBL UPRT WHERE UT.USER_ID = UPRT.USER_ID AND "
										+ "PT.PROJECT_ID = UPRT.PROJECT_ID AND UT.USERNAME = " + "'" + element + "'";
								return sqlQueryStr;
							}
						}
					}
					if (relationType.equals("-ADMINAREA_PROJECT")) {
						String area = null;
						int indexOfRelationType = argsList.indexOf(relationType);
						String option = argsList.get(indexOfRelationType + 1);
						if (option.equals("-where")) {
							int indexOfOption = argsList.indexOf(option);
							String elements = argsList.get(indexOfOption + 1);
							int indexOfelements = argsList.indexOf(elements);
							if (elements.contains("adminarea")) {
								area = "adminarea";
								String element = argsList.get(indexOfOption + 1).split("=")[1];
								sqlQueryStr = sqlQueryStr
										+ "select AAT.NAME AS ADMIN_AREA_NAME, ST.NAME AS SITE_NAME , PT.NAME PROJECT_NAME,AAP."
										+ "STATUS FROM PROJECTS_TBL PT,SITES_TBL ST,ADMIN_AREA_PROJECT_REL_TBL AAP, "
										+ "SITE_ADMIN_AREA_REL_TBL SAART, ADMIN_AREAS_TBL AAT "
										+ "WHERE PT.PROJECT_ID = AAP.PROJECT_ID AND AAP.SITE_ADMIN_AREA_REL_ID = SAART.SITE_ADMIN_AREA_REL_ID "
										+ "AND SAART.ADMIN_AREA_ID = AAT.ADMIN_AREA_ID AND SAART.SITE_ID = ST.SITE_ID  AND AAT.NAME="
										+ "'" + element + "'";
								if (indexOfelements + 1 == argsList.size()) {
									return sqlQueryStr;
								}

							} else if (elements.contains("project")) {
								area = "project";
								String elemnt = argsList.get(indexOfOption + 1).split("=")[1];
								sqlQueryStr = sqlQueryStr
										+ "select AAT.NAME AS ADMIN_AREA_NAME, ST.NAME AS SITE_NAME , PT.NAME PROJECT_NAME,"
										+ "AAP.STATUS FROM PROJECTS_TBL PT, SITES_TBL ST,ADMIN_AREA_PROJECT_REL_TBL AAP, "
										+ "SITE_ADMIN_AREA_REL_TBL SAART, ADMIN_AREAS_TBL AAT WHERE PT.PROJECT_ID = "
										+ "AAP.PROJECT_ID AND AAP.SITE_ADMIN_AREA_REL_ID = SAART.SITE_ADMIN_AREA_REL_ID"
										+ " AND SAART.ADMIN_AREA_ID = AAT.ADMIN_AREA_ID AND SAART.SITE_ID = ST.SITE_ID AND PT.NAME= "
										+ "'" + elemnt + "'";

								if (indexOfelements + 1 == argsList.size()) {
									return sqlQueryStr;
								}
							}
							String optinAnd = argsList.get(indexOfelements + 1).toUpperCase();
							if (optinAnd.equals("AND")) {
								sqlQueryStr = sqlQueryStr + " " + "AND";
								int indexOfAnd = argsList.indexOf(optinAnd);
								sqlQueryStr = sqlQueryStr + " " + "AAP.status = ";
								/*if (area.equals("adminarea")) {
									sqlQueryStr = sqlQueryStr + " " + "AAP.status = ";
								}
								if (area.equals("project")) {
									sqlQueryStr = sqlQueryStr + " " + "AAP.status = ";
								}*/
								String elemnts = argsList.get(indexOfAnd + 1);
								if (elemnts.contains("'")) {

									return null;
								}
								String elemnt = elemnts.split("=")[1];
								sqlQueryStr = sqlQueryStr + " " + "'" + elemnt + "'";
								int indexOfelemnts = argsList.indexOf(elemnts);
								if (indexOfelemnts + 1 == argsList.size()) {
									return sqlQueryStr;
								}
							}
						}
					}
					if (relationType.equals("-USER_PROJECT_USAGE")) {
						int indexOfRelationType = argsList.indexOf("-USER_PROJECT_USAGE");
						String optionWhere = argsList.get(indexOfRelationType + 1);
						if (optionWhere.equals("-where")) {

							int indexOfOptionWhere = argsList.indexOf("-where");
							if (indexOfOptionWhere + 1 == argsList.size()) {
								return null;
							}
							String yearElement = argsList.get(indexOfOptionWhere + 1);
							if (!yearElement.contains("=")) {
								return null;
							}

							String year = argsList.get(indexOfOptionWhere + 1).split("=")[1];
							String beginYear = "'" + year + "/01/01'";
							String endYear = "'" + year + "/12/31'";
							sqlQueryStr = "select PROJECT, usemonth, count(PROJECT) from ( select distinct UHT.USER_NAME, UHT.PROJECT, TO_CHAR(UHT.LOG_TIME,'YYYY/MM') usemonth "
									+ "from USER_HISTORY_TBL UHT where UHT.LOG_TIME>=to_date(" + beginYear
									+ ",'YYYY/MM/DD') and UHT.LOG_TIME<=to_date(" + endYear
									+ ",'YYYY/MM/DD') and UHT.APPLICATION in ('*OpenProject*') and IS_USER_STATUS='false') group by PROJECT, usemonth order by PROJECT, usemonth";
							String yearElement1 = argsList.get(indexOfOptionWhere + 1);
							int indexOfYearElement = argsList.indexOf(yearElement1);
							if (indexOfYearElement + 1 == argsList.size()) {
								return sqlQueryStr;
							}

						}

					}
				}
			}

			return null;
		} catch (Exception e) {

			return null;
		}
	}
}
