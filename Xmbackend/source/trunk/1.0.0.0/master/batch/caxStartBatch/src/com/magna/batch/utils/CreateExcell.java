package com.magna.batch.utils;

import java.io.File;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;

import java.util.Map.Entry;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CreateExcell {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(CreateExcell.class);

	/**
	 * Generates Excel sheet.
	 *
	 * @param list
	 *            the list
	 */

	public static void createExcellforUserCmds(final List<Map<String, Object>> list, String type, String filepath) {

		XSSFWorkbook workbook = new XSSFWorkbook();

		XSSFSheet sheet;
		if (type.equals("userprojectusage")) {
			sheet = workbook.createSheet("projectusage");
		} else {
			sheet = workbook.createSheet("Data");
		}

		if (type.equals("users")) {

			List<String> lstr = new ArrayList<String>();
			lstr.add("USERNAME");
			lstr.add("FULL_NAME");
			lstr.add("EMAIL_ID");
			lstr.add("TELEPHONE_NUMBER");
			lstr.add("DEPARTMENT");
			lstr.add("STATUS");
			lstr.add("DESCRIPTION");
			lstr.add("REMARKS");
			lstr.add("LANGUAGE_CODE");
			lstr.add("ICON_NAME");

			int rowNum = 0;
			Row row = sheet.createRow(rowNum++);
			int colNum = 0;
			for (Object field : lstr) {

				Cell cell = row.createCell(colNum++);
				if (field instanceof String) {
					cell.setCellValue((String) field);
				} else if (field instanceof Integer) {
					cell.setCellValue((Integer) field);
				}
			}

			for (Map<String, Object> userObjectMap : list) {

				String iconVal = null;

				row = sheet.createRow(rowNum++);
				colNum = 0;
				Cell cell = null;
				Set<Entry<String, Object>> EntrySet = userObjectMap.entrySet();
				for (Entry<String, Object> entry : EntrySet) {

					switch (entry.getKey()) {
					case "USER_ID":
						continue;
					case "ICON_ID":
						continue;
					case "USER_TRANSLATION_ID":
						continue;
					case "CREATE_DATE":
						continue;
					case "UPDATE_DATE":
						continue;
					case "ICON_TYPE":
						continue;

					}

					if (!entry.getKey().equals("ICON_NAME")) {
						cell = row.createCell(colNum++);
					}

					if (!userObjectMap.containsKey("USERNAME")) {
						cell.setCellValue("null");
					}
					if ((entry.getKey().equals("USERNAME"))) {
						cell.setCellValue((String) entry.getValue());

					}

					if (!userObjectMap.containsKey("FULL_NAME")) {
						cell.setCellValue("null");
					}
					if (entry.getKey().equals("FULL_NAME")) {
						cell.setCellValue((String) entry.getValue());

					}

					if (!userObjectMap.containsKey("EMAIL_ID")) {
						cell.setCellValue("null");
					}
					if (entry.getKey().equals("EMAIL_ID")) {
						cell.setCellValue((String) entry.getValue());

					}

					if (!userObjectMap.containsKey("TELEPHONE_NUMBER")) {
						cell.setCellValue("null");
					}
					if (entry.getKey().equals("TELEPHONE_NUMBER")) {
						cell.setCellValue((String) entry.getValue());

					}

					if (!userObjectMap.containsKey("DEPARTMENT")) {
						cell.setCellValue("null");

					}
					if (entry.getKey().equals("DEPARTMENT")) {
						cell.setCellValue((String) entry.getValue());

					}

					if (!userObjectMap.containsKey("STATUS")) {
						cell.setCellValue("null");

					}
					if (entry.getKey().equals("STATUS")) {
						cell.setCellValue((String) entry.getValue());

					}
					if (!userObjectMap.containsKey("DESCRIPTION")) {
						cell.setCellValue("null");

					} else if (entry.getKey().equals("DESCRIPTION")) {
						cell.setCellValue((String) entry.getValue());

					}

					if (!userObjectMap.containsKey("REMARKS")) {
						cell.setCellValue("null");

					}
					if (entry.getKey().equals("REMARKS")) {
						cell.setCellValue((String) entry.getValue());

					}

					if (!userObjectMap.containsKey("LANGUAGE_CODE")) {
						cell.setCellValue("null");

					}
					if (entry.getKey().equals("LANGUAGE_CODE")) {
						cell.setCellValue((String) entry.getValue());

					}

					if (!userObjectMap.containsKey("ICON_NAME")) {
						cell.setCellValue("null");

					}
					if (entry.getKey().equals("ICON_NAME")) {
						// cell.setCellValue((String) entry.getValue());
						iconVal = (String) entry.getValue();

					}

				}
				cell = row.createCell(colNum++);
				cell.setCellValue(iconVal);
			}

		}
		if (type.equals("adminproject")) {

			List<String> lstr = new ArrayList<String>();
			lstr.add("ADMIN_AREA_NAME");
			lstr.add("SITE_NAME");
			lstr.add("PROJECT_NAME");
			lstr.add("ADMIN AREA PROJECT STATUS");

			int rowNum = 0;
			Row row = sheet.createRow(rowNum++);
			int colNum = 0;
			for (Object field : lstr) {

				Cell cell = row.createCell(colNum++);
				if (field instanceof String) {
					cell.setCellValue((String) field);
				} else if (field instanceof Integer) {
					cell.setCellValue((Integer) field);
				}
			}

			for (Map<String, Object> userObjectMap : list) {

				row = sheet.createRow(rowNum++);
				colNum = 0;
				Cell cell = null;
				Set<Entry<String, Object>> EntrySet = userObjectMap.entrySet();
				for (Entry<String, Object> entry : EntrySet) {

					cell = row.createCell(colNum++);

					if (!userObjectMap.containsKey("ADMIN_AREA_NAME")) {
						cell.setCellValue("null");
					}
					if ((entry.getKey().equals("ADMIN_AREA_NAME"))) {
						cell.setCellValue((String) entry.getValue());

					}

					if (!userObjectMap.containsKey("PROJECT_NAME")) {
						cell.setCellValue("null");
					}
					if (entry.getKey().equals("PROJECT_NAME")) {
						cell.setCellValue((String) entry.getValue());

					}

					if (!userObjectMap.containsKey("STATUS")) {
						cell.setCellValue("null");
					}
					if (entry.getKey().equals("STATUS")) {
						cell.setCellValue((String) entry.getValue());

					}

					if (!userObjectMap.containsKey("SITE_NAME")) {
						cell.setCellValue("null");
					}
					if (entry.getKey().equals("SITE_NAME")) {
						cell.setCellValue((String) entry.getValue());

					}

				}

			}

		}
		if (type.equals("userproject")) {

			List<String> lstr = new ArrayList<String>();
			lstr.add("USER_NAME");
			lstr.add("PROJECT_NAME");
			lstr.add("USER PROJECT RELATION STATUS");

			int rowNum = 0;
			Row row = sheet.createRow(rowNum++);
			int colNum = 0;
			for (Object field : lstr) {

				Cell cell = row.createCell(colNum++);
				if (field instanceof String) {
					cell.setCellValue((String) field);
				} else if (field instanceof Integer) {
					cell.setCellValue((Integer) field);
				}
			}

			for (Map<String, Object> userObjectMap : list) {

				row = sheet.createRow(rowNum++);
				colNum = 0;
				Cell cell = null;
				Set<Entry<String, Object>> EntrySet = userObjectMap.entrySet();
				for (Entry<String, Object> entry : EntrySet) {
					{

						cell = row.createCell(colNum++);

						if (!userObjectMap.containsKey("USERNAME")) {
							cell.setCellValue("null");
						}
						if ((entry.getKey().equals("USERNAME"))) {
							cell.setCellValue((String) entry.getValue());

						}

						if (!userObjectMap.containsKey("NAME")) {
							cell.setCellValue("null");
						}
						if (entry.getKey().equals("NAME")) {
							cell.setCellValue((String) entry.getValue());

						}

						if (!userObjectMap.containsKey("STATUS")) {
							cell.setCellValue("null");
						}
						if (entry.getKey().equals("STATUS")) {
							cell.setCellValue((String) entry.getValue());

						}

					}

				}
			}

		}
		if (type.equals("userprojectusage")) {

			String year = filepath.split("_")[1];
			filepath = filepath.split("_")[0];

			List<String> lstr = new ArrayList<String>();

			lstr.add("Project");
			for (int i = 1; i < 13; i++) {
				if (i >= 1 && i <= 9) {
					lstr.add(year + "/" + "0" + i);
				} else {
					if (i == 12) {
						lstr.add(year + "/" + i);
					} else {
						lstr.add(year + "/" + i);
					}
				}

			}

			int rowNum = 0;
			Row row = sheet.createRow(rowNum++);
			int colNum = 0;

			for (Object field : lstr) {

				Cell cell = row.createCell(colNum++);
				if (field instanceof String) {
					// System.out.println(field);
					cell.setCellValue((String) field);
				} else if (field instanceof Integer) {
					cell.setCellValue((Integer) field);
				}
			}

			// get data
			row = sheet.createRow(rowNum++);
			colNum = 0;

			String projInfo = "";
			int[] dat = new int[12];
			Arrays.fill(dat, 0);
			int month = 0;
			String prevProjName = null;
			for (Map<String, Object> userObjectMap : list) {
				Set<Entry<String, Object>> EntrySet = userObjectMap.entrySet();
				for (Entry<String, Object> entry : EntrySet) {

					if (entry.getKey().equals("PROJECT")) {
						if (projInfo.isEmpty()) {
							projInfo = (String) entry.getValue();
							prevProjName = projInfo;
							Cell cell = row.createCell(colNum++);
							cell.setCellValue(projInfo);
						}
						if (!entry.getValue().equals(prevProjName)) {
							for (int i = 0; i < 12; i++) {

								Cell cell = row.createCell(colNum++);
								cell.setCellValue(dat[i]);
							}
							Arrays.fill(dat, 0);
							row = sheet.createRow(rowNum++);
							colNum = 0;
							projInfo = "";
						}
					}

					if (entry.getKey().equals("USEMONTH")) {
						String mm = ((String) entry.getValue()).split("/")[1];
						month = Integer.valueOf(mm);
					}

					if (entry.getKey().equals("COUNT(PROJECT)")) {
						try {
							int val = (int) entry.getValue();
							dat[month - 1] = val;
						} catch (Exception e) {
							System.out.println(e.getMessage());
						}
					}

				}

			}
			colNum = 1;
			for (int i = 0; i < 12; i++) {
				Cell cell = row.createCell(colNum++);
				cell.setCellValue(dat[i]);
			}

		}

		File file = new File(filepath);
		int lastIndexOf = filepath.lastIndexOf(File.separator);
		String dirPath = filepath.substring(0, lastIndexOf);
		while (true) {
			String name = file.getPath();
			String fname = name.split("\\.")[0];
			String extn = name.split("\\.")[1];
			if (file.exists()) {
				System.out.println(file.getPath() + " exists");
				int id = 1;
				if (name.contains("_")) {
					int idfound = Integer.valueOf(fname.split("_")[1]);
					String filename = fname.split("_")[0];
					++idfound;
					filepath = filename + "_" + idfound + "." + extn;
				} else {
					filepath = fname + "_" + id + "." + extn;
				}
				file = new File(filepath);
			} else {
				break;
			}
		}

		File f = new File(dirPath);
		if (!f.exists()) {
			f.mkdirs();
		}
		try {
			FileOutputStream outputStream = new FileOutputStream(filepath);
			workbook.write(outputStream);
			workbook.close();
			System.out.println("Creating excel sheet file at : " + filepath);
		} catch (FileNotFoundException e) {
			System.out.println("ERROR: " + e.getMessage());

			// LOGGER.error("Exception occurred"+e);
		} catch (IOException e) {
			System.out.println("ERROR: " + e.getMessage());
			LOGGER.error("Exception occurred" + e);
		} catch (Exception e) {
			System.out.println("ERROR: " + e.getMessage());
			LOGGER.error("Exception occurred" + e);

		}

	}
}
