package com.magna.batch.restclient.authorization;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.magna.batch.restclient.BatchRestController;
import com.magna.batch.restclient.RestClientExpHandlerUtil;
import com.magna.batch.restclient.exception.UnauthorizedAccessException;
import com.magna.batch.utils.RestClientConstants;
import com.magna.batch.utils.XmSystemEnvProcess;
import com.magna.xmbackend.vo.user.AuthResponse;
import com.magna.xmbackend.vo.user.UserCredential;

/**
 * The Class AuthController.
 */
public class AuthController extends BatchRestController {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(AuthController.class);

	/**
	 * Instantiates a new auth controller.
	 */
	public AuthController() {
		super();
	}

	/**
	 * Authorize login.
	 * @param userCredential  the user credential
	 * @return the auth response
	 * @throws UnauthorizedAccessException the unauthorized access exception
	 */
	public AuthResponse authorizeLogin(final String userName, final String password, final String caxAppName)
			throws UnauthorizedAccessException {
		try {
			final UserCredential userCredential = new UserCredential();
			userCredential.setUsername(userName);
			userCredential.setPassword(password);
			userCredential.setAppName(caxAppName);

			String url = new String(this.serviceUrl + RestClientConstants.GET_AUTHENTICATION_RESPONSE);
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<UserCredential> requestEntity = new HttpEntity<UserCredential>(userCredential, this.headers);
			ResponseEntity<AuthResponse> responseEntity = restTemplate.exchange(url, HttpMethod.POST, requestEntity,
					AuthResponse.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			HttpHeaders headersFromBackend = responseEntity.getHeaders();
			String tkt = headersFromBackend.getFirst("TKT");
			XmSystemEnvProcess.getInstance().setTicket(tkt);
			if (statusCode.is2xxSuccessful()) {
				return responseEntity.getBody();
			} else {
				LOGGER.error("Error while calling authorizeLogin REST Service, returns the status code: ", //$NON-NLS-1$
						statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				}
			} else if (e instanceof HttpStatusCodeException) {
				RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			} else if (e instanceof ResourceAccessException) {
				throw e;
			}
		} catch (Exception e) {
			LOGGER.error("Error while calling authorizeLogin REST Service!", e); //$NON-NLS-1$
		}
		return null;
	}
}
