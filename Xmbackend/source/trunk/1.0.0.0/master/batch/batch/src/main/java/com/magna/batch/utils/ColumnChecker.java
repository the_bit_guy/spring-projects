package com.magna.batch.utils;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.cli.CommandLine;

/**
 * Class for Column checker.
 *
 * @author Roshan.Ekka
 */
public class ColumnChecker {
	
	/** Member variable 'user' for {@link List<String>}. */
	private List<String> user;
	
	/** Member variable 'show_user' for {@link List<String>}. */
	private List<String> show_user;
	
	/** Member variable 'site' for {@link List<String>}. */
	private List<String> site;
	
	/** Member variable 'show_site' for {@link List<String>}. */
	private List<String> show_site;
	
	/** Member variable 'adminarea' for {@link List<String>}. */
	private List<String> adminarea;
	
	/** Member variable 'show_adminarea' for {@link List<String>}. */
	private List<String> show_adminarea;
	
	/** Member variable 'project' for {@link List<String>}. */
	private List<String> project;
	
	/** Member variable 'show_project' for {@link List<String>}. */
	private List<String> show_project;
	
	/** Member variable 'userapp' for {@link List<String>}. */
	private List<String> userapp;
	
	/** Member variable 'show_userapp' for {@link List<String>}. */
	private List<String> show_userapp;
	
	/** Member variable 'projectapp' for {@link List<String>}. */
	private List<String> projectapp;
	
	/** Member variable 'show_projectapp' for {@link List<String>}. */
	private List<String> show_projectapp;
	
	/** Member variable 'startapp' for {@link List<String>}. */
	private List<String> startapp;
	
	/** Member variable 'show_startapp' for {@link List<String>}. */
	private List<String> show_startapp;
	
	/** Member variable 'baseapp' for {@link List<String>}. */
	private List<String> baseapp;
	
	/** Member variable 'show_baseapp' for {@link List<String>}. */
	private List<String> show_baseapp;
	
	/** Member variable 'user_project' for {@link List<String>}. */
	private List<String> user_project;
	
	/** Member variable 'show_user_project' for {@link List<String>}. */
	private List<String> show_user_project;
	
	/** Member variable 'show_adminarea_userapp' for {@link List<String>}. */
	private List<String> show_adminarea_userapp;
	
	/** Member variable 'show_adminarea_project' for {@link List<String>}. */
	private List<String> show_adminarea_project;
	
	/** Member variable 'show_adminarea_startapp' for {@link List<String>}. */
	private List<String> show_adminarea_startapp;
	
	/** Member variable 'show_user_project_usage' for {@link List<String>}. */
	private List<String> show_user_project_usage;
	
	/** Member variable 'show_site_adminarea' for {@link List<String>}. */
	private List<String> show_site_adminarea;
	
	/** Member variable 'show_adminarea_project_projectapp' for {@link List<String>}. */
	private List<String> show_adminarea_project_projectapp;
	
	/** Member variable 'show_adminarea_project_startapp' for {@link List<String>}. */
	private List<String> show_adminarea_project_startapp;
	
	/** Member variable 'show_user_startapp' for {@link List<String>}. */
	private List<String> show_user_startapp;
	
	/** Member variable 'show_user_project_projectapp' for {@link List<String>}. */
	private List<String> show_user_project_projectapp;
	
	/** Member variable 'show_user_userapp' for {@link List<String>}. */
	private List<String> show_user_userapp;
	
	/** Member variable 'show_baseapp_userapp' for {@link List<String>}. */
	private List<String> show_baseapp_userapp;
	
	/** Member variable 'show_baseapp_projectapp' for {@link List<String>}. */
	private List<String> show_baseapp_projectapp;
	
	/** Member variable 'show_baseapp_startapp' for {@link List<String>}. */
	private List<String> show_baseapp_startapp;
	
	/** Member variable 'show_object_list' for {@link List<String>}. */
	private List<String> show_object_list;
	
	/** Member variable 'show_type_list' for {@link List<String>}. */
	private List<String> show_type_list;
	
	/** Member variable 'relObjType' for {@link List<String>}. */
	private List<String> relObjType; 
	
	/** Member variable 'allType' for {@link List<String>}. */
	private List<String> allType; 
	
	/**
	 * Constructor for ColumnChecker Class.
	 */
	public ColumnChecker() {
		this.user = Arrays.asList("NAME", "STATUS", "ICON_NAME", "FULL_NAME", "EMAIL_ID", "DEPARTMENT",
				"TELEPHONE_NUMBER", "DESCRIPTION_EN", "DESCRIPTION_DE", "REMARKS_EN", "REMARKS_DE");
		
		this.show_user = Arrays.asList("NAME", "STATUS", "ICON_NAME", "FULL_NAME", "EMAIL_ID", "DEPARTMENT",
				"TELEPHONE_NUMBER", "DESCRIPTION", "REMARKS", "LANGUAGE_CODE");
		
		this.site = Arrays.asList("NAME", "STATUS", "ICON_NAME", "DESCRIPTION_EN", "DESCRIPTION_DE", "REMARKS_EN",
				"REMARKS_DE");
		
		this.show_site = Arrays.asList("NAME", "STATUS", "ICON_NAME", "DESCRIPTION", "REMARKS", "LANGUAGE_CODE");

		this.adminarea = Arrays.asList("NAME", "STATUS", "ICON_NAME", "SINGLETON_APP_TIMEOUT", "HOTLINE_CONTACT_NUMBER",
				"HOTLINE_CONTACT_EMAIL", "DESCRIPTION_EN", "DESCRIPTION_DE", "REMARKS_EN", "REMARKS_DE");
		
		this.show_adminarea = Arrays.asList("NAME", "STATUS", "ICON_NAME", "SINGLETON_APP_TIMEOUT", "HOTLINE_CONTACT_NUMBER",
				"HOTLINE_CONTACT_EMAIL", "DESCRIPTION", "REMARKS", "LANGUAGE_CODE");

		this.project = Arrays.asList("NAME", "STATUS", "ICON_NAME", "DESCRIPTION_EN", "DESCRIPTION_DE", "REMARKS_EN",
				"REMARKS_DE");
		
		this.show_project = Arrays.asList("NAME", "STATUS", "ICON_NAME", "DESCRIPTION", "REMARKS", "LANGUAGE_CODE");

		this.userapp = Arrays.asList("NAME", "NAME_EN", "NAME_DE", "STATUS", "ICON_NAME", "APPLICATION", "IS_PARENT",
				"IS_SINGLETON", "POSITION", "DESCRIPTION", "DESCRIPTION_EN", "DESCRIPTION_DE", "REMARKS_EN",
				"REMARKS_DE");
		this.show_userapp = Arrays.asList("NAME", "NAME_EN", "NAME_DE", "STATUS", "ICON_NAME", "APPLICATION", "IS_PARENT",
				"IS_SINGLETON", "POSITION", "DESCRIPTION", "REMARKS", "LANGUAGE_CODE");

		this.projectapp = Arrays.asList("NAME", "NAME_EN", "NAME_DE", "STATUS", "IS_PARENT", "IS_SINGLETON", "POSITION",
				"APPLICATION", "DESCRIPTION", "ICON_NAME", "DESCRIPTION_EN", "DESCRIPTION_DE", "REMARKS_EN",
				"REMARKS_DE");
		
		this.show_projectapp = Arrays.asList("NAME", "NAME_EN", "NAME_DE", "STATUS", "IS_PARENT", "IS_SINGLETON", "POSITION",
				"APPLICATION", "DESCRIPTION", "ICON_NAME", "REMARKS", "LANGUAGE_CODE");

		this.startapp = Arrays.asList("NAME", "STATUS", "ICON_NAME", "APPLICATION", "IS_MESSAGE",
				"START_MESSAGE_EXPIRY_DATE", "DESCRIPTION_EN", "DESCRIPTION_DE", "REMARKS_EN", "REMARKS_DE");
		
		this.show_startapp = Arrays.asList("NAME", "STATUS", "ICON_NAME", "APPLICATION", "IS_MESSAGE",
				"START_MESSAGE_EXPIRY_DATE", "DESCRIPTION", "REMARKS", "LANGUAGE_CODE");

		this.baseapp = Arrays.asList("NAME", "STATUS", "PLATFORMS", "PROGRAM", "ICON_NAME", "DESCRIPTION_EN",
				"DESCRIPTION_DE", "REMARKS_EN", "REMARKS_DE");
		
		this.show_baseapp = Arrays.asList("NAME", "STATUS", "PLATFORMS", "PROGRAM", "ICON_NAME", "DESCRIPTION", "REMARKS", "LANGUAGE_CODE");
		
		this.user_project = Arrays.asList("USER", "PROJECT");
		
		this.show_user_project = Arrays.asList("USER_NAME", "PROJECT_NAME", "STATUS", "PROJECT_EXPIRY_DAYS");
		
		this.show_adminarea_userapp = Arrays.asList("SITE_NAME", "ADMINAREA_NAME", "USERAPP_NAME", "STATUS", "REL_TYPE");
		
		this.show_adminarea_project = Arrays.asList("SITE_NAME", "ADMINAREA_NAME", "PROJECT_NAME", "STATUS");
		
		this.show_adminarea_startapp = Arrays.asList("SITE_NAME", "ADMINAREA_NAME", "STARTAPP_NAME", "STATUS");
		
		this.show_user_project_usage = Arrays.asList("YEAR");
		
		this.show_site_adminarea = Arrays.asList("SITE_NAME", "ADMINAREA_NAME", "STATUS");
		
		this.show_adminarea_project_projectapp = Arrays.asList("SITE_NAME", "ADMINAREA_NAME", "PROJECT_NAME", "PROJECTAPP_NAME", "STATUS", "REL_TYPE");
		
		this.show_adminarea_project_startapp = Arrays.asList("SITE_NAME", "ADMINAREA_NAME", "PROJECT_NAME", "STARTAPP_NAME", "STATUS");
		
		this.show_user_startapp = Arrays.asList("USER_NAME","STARTAPP_NAME", "STATUS");
		
		this.show_user_project_projectapp = Arrays.asList("USER_NAME", "SITE_NAME", "ADMINAREA_NAME", "PROJECT_NAME", "PROJECTAPP_NAME", "USER_REL_TYPE");
		
		this.show_user_userapp = Arrays.asList("SITE_NAME", "ADMINAREA_NAME", "USER_NAME", "USERAPP_NAME", "USER_REL_TYPE");
		
		this.show_baseapp_userapp = Arrays.asList("BASEAPP_NAME", "USERAPP_NAME");
		
		this.show_baseapp_projectapp = Arrays.asList("BASEAPP_NAME", "PROJECTAPP_NAME");
		
		this.show_baseapp_startapp = Arrays.asList("BASEAPP_NAME", "STARTAPP_NAME");
		
		this.show_object_list = Arrays.asList("SITE", "ADMINAREA", "PROJECT", "USER", "USERAPP", "PROJECTAPP", "STARTAPP", "BASEAPP");
		
		this.show_type_list = Arrays.asList("BASE_DATA", "RELATION");
		
		this.relObjType = Arrays.asList("-h", "-help", "-genexcel", "-GENEXCEL", "-user_project", "-USER_PROJECT",
				"-adminarea_project", "-ADMINAREA_PROJECT", "-adminarea_startapp", "-ADMINAREA_STARTAPP",
				"-user_project_usage", "-USER_PROJECT_USAGE", "-site_adminarea", "-SITE_ADMINAREA", "-adminarea_userapp",
				"-ADMINAREA_USERAPP", "-adminarea_project_projectapp", "-adminarea_project_startapp",
				"-ADMINAREA_PROJECT_STARTAPP", "-user_startapp", "-USER_STARTAPP", "-user_project_projectapp",
				"-USER_PROJECT_PROJECTAPP", "-user_userapp", "-USER_USERAPP", "-baseapp_userapp", "-BASEAPP_USERAPP",
				"-baseapp_projectapp", "-BASEAPP_PROJECTAPP", "-baseapp_startapp", "-BASEAPP_STARTAPP");
		this.allType = Arrays.asList("-h", "-help", "-genexcel", "-GENEXCEL", "-user_project", "-USER_PROJECT",
				"-adminarea_project", "-ADMINAREA_PROJECT", "-adminarea_startapp", "-ADMINAREA_STARTAPP",
				"-user_project_usage", "-USER_PROJECT_USAGE", "-site_adminarea", "-SITE_ADMINAREA", "-adminarea_userapp",
				"-ADMINAREA_USERAPP", "-adminarea_project_projectapp", "-adminarea_project_startapp",
				"-ADMINAREA_PROJECT_STARTAPP", "-user_startapp", "-USER_STARTAPP", "-user_project_projectapp",
				"-USER_PROJECT_PROJECTAPP", "-user_userapp", "-USER_USERAPP", "-baseapp_userapp", "-BASEAPP_USERAPP",
				"-baseapp_projectapp", "-BASEAPP_PROJECTAPP", "-baseapp_startapp", "-BASEAPP_STARTAPP", "-where", "-WHERE",
				"-fields", "-FIELDS","-remove", "-REMOVE", "-add", "-ADD","-delete", "-DELETE", "-update", "-UPDATE","-create", "-CREATE",
				"-select", "-SELECT", "-type", "-TYPE");
		
	}
	
	/**
	 * Gets the column.
	 *
	 * @param str {@link String}
	 * @param objType {@link ObjectType}
	 * @return the column
	 */
	public boolean getColumn(String str, ObjectType objType, final CommandLine cmd){
		boolean isValid = true;
		String mapType = objType.toString().toLowerCase();
		List<String> list = null;
		switch (mapType) {
		case "user":
			if(cmd.hasOption("create") || cmd.hasOption("update")){
				list = this.user;
			} else {
				list = this.show_user;
			}
			break;
		case "site":
			if(cmd.hasOption("create") || cmd.hasOption("update")){
				list = this.site;
			} else {
				list = this.show_site;
			}
			break;
		case "adminarea":
			if(cmd.hasOption("create") || cmd.hasOption("update")){
				list = this.adminarea;
			} else {
				list = this.show_adminarea;
			}
			break;
		case "project":
			if(cmd.hasOption("create") || cmd.hasOption("update")){
				list = this.project;
			} else {
				list = this.show_project;
			}
			break;
		case "userapp":
			if(cmd.hasOption("create") || cmd.hasOption("update")){
				list = this.userapp;
			} else {
				list = this.show_userapp;
			}
			break;
		case "projectapp":
			if(cmd.hasOption("create") || cmd.hasOption("update")){
				list = this.projectapp;
			} else {
				list = this.show_projectapp;
			}
			break;
		case "startapp":
			if(cmd.hasOption("create") || cmd.hasOption("update")){
				list = this.startapp;
			} else {
				list = this.show_startapp;
			}
			break;
		case "baseapp":
			if(cmd.hasOption("create") || cmd.hasOption("update")){
				list = this.baseapp;
			} else {
				list = this.show_baseapp;
			}
			break;
		default:
			System.out.println("ERROR: Syntax error !"+ mapType +" is not a valid object.");
			System.out.println("Valid objects are : SITE, ADMINAREA, PROJECT, USER, USERAPP, PROJECTAPP, STARTAPP, BASEAPP");
			return isValid = false;
		}
		
		if(cmd.hasOption("create") || cmd.hasOption("update")) {
			String[] split = str.split("\"");
			for (int index = 0; index < split.length; index++) {
				if(split[index].contains("=")){
					String splitSte = split[index].split("=")[0];
					String value = splitSte.toUpperCase();
					value = value.replace("'", "");
					if(split[index].startsWith("=") || split[index].endsWith("=")){
						System.out.println("ERROR: Syntax error : "+split[index]);
						isValid = false;
						return isValid;
					} else if(!list.contains(value)){
						System.out.println(splitSte +" is not a valid field !");
						System.out.println("Valid fields are : " + list);
						isValid = false;
						return isValid;
					}
				}
			}
		} else if(cmd.hasOption("fields")) {
			String[] split = str.split(" ");
			for (int index = 0; index < split.length; index++) {
				String value = split[index].toUpperCase();
				value = value.replace("'", "");
				if(!"".equalsIgnoreCase(value)){
					if(!(list.contains(value) || "DISTINCT".equalsIgnoreCase(value))){
						System.out.println(split[index] +" is not a valid field !");
						System.out.println("Valid fields are : " + list);
						isValid = false;
						return isValid;
					}
				}
			}
		}
		
		return isValid;
	}
	
	/**
	 * Gets the rel column.
	 *
	 * @param str {@link String}
	 * @param objRelType {@link ObjectRelationType}
	 * @return the rel column
	 */
	public boolean getRelColumn(String str, ObjectRelationType objRelType, final CommandLine cmd){
		boolean isValid = true;
		String mapType = objRelType.toString().toLowerCase();
		List<String> list = null;
		switch (mapType) {
		case "user_project":
			if(cmd.hasOption("add") || cmd.hasOption("remove")){
				list = this.user_project;
			} else {
				list = this.show_user_project;
			}
			break;
		case "adminarea_project":
			list = this.show_adminarea_project;
			break;
		case "adminarea_startapp":
			list = this.show_adminarea_startapp;
			break;
		case "user_project_usage":
			list = this.show_user_project_usage;
			break;
		case "site_adminarea":
			list = this.show_site_adminarea;
			break;
		case "adminarea_userapp":
			list = this.show_adminarea_userapp;
			break;
		case "adminarea_project_projectapp":
			list = this.show_adminarea_project_projectapp;
			break;
		case "adminarea_project_startapp":
			list = this.show_adminarea_project_startapp;
			break;
		case "user_startapp":
			list = this.show_user_startapp;
			break;
		case "user_project_projectapp":
			list = this.show_user_project_projectapp;
			break;
		case "user_userapp":
			list = this.show_user_userapp;
			break;
		case "baseapp_userapp":
			list = this.show_baseapp_userapp;
			break;
		case "baseapp_projectapp":
			list = this.show_baseapp_projectapp;
			break;
		case "baseapp_startapp":
			list = this.show_baseapp_startapp;
			break;
		default:
			System.out.println("ERROR: Syntax error !"+ mapType +" is not a valid object.");
			System.out.println("Valid relation objects are : USER_PROJECT, ADMINAREA_PROJECT, ADMINAREA_STARTAPP, USER_PROJECT_USAGE,"
					+ "SITE_ADMINAREA, ADMINAREA_USERAPP, ADMINAREA_PROJECT_PROJECTAPP, ADMINAREA_PROJECT_STARTAPP, USER_STARTAPP,"
					+ "  USER_PROJECT_PROJECTAPP, USER_USERAPP, BASEAPP_USERAPP, BASEAPP_PROJECTAPP, BASEAPP_STARTAPP ");
			return isValid = false;
		}
		
		if(cmd.hasOption("add") || cmd.hasOption("remove")) {
			String[] split = str.split("\"");
			for (int index = 0; index < split.length; index++) {
				if(split[index].contains("=")){
					String splitSte = split[index].split("=")[0];
					String value = splitSte.toUpperCase();
					value = value.replace("'", "");
					if(split[index].startsWith("=") || split[index].endsWith("=")){
						System.out.println("ERROR: Syntax error : "+split[index]);
						isValid = false;
						return isValid;
					} else if(!list.contains(value)){
						System.out.println(splitSte +" is not a valid field !");
						System.out.println("Valid fields are : " + list);
						isValid = false;
						return isValid;
					}
				}
			}
		} else if(cmd.hasOption("fields")) {
			String[] split = str.split(" ");
			for (int index = 0; index < split.length; index++) {
				String value = split[index].toUpperCase();
				value = value.replace("'", "");
				if(!"".equalsIgnoreCase(value)){
					if(!(list.contains(value)|| "DISTINCT".equalsIgnoreCase(value))){
						System.out.println(split[index] +" is not a valid field !");
						System.out.println("Valid fields are : " + list);
						isValid = false;
						return isValid;
					}
				}
			}
		}
		
		return isValid;
	}
	
	/**
	 * Gets the column.
	 *
	 * @param str {@link String}
	 * @param cmd {@link CommandLine}
	 * @return the column
	 */
	public boolean getColumn(String str, final CommandLine cmd){
		boolean isValid = true;
		if(!(show_object_list.contains(str.toUpperCase()))){
			System.out.println("ERROR: Syntax error !"+ str +" is not a valid object.");
			System.out.println("Valid Objects are : " + show_object_list);
			isValid = false;
			return isValid;
		}
		return isValid;
	}
	
	/**
	 * Gets the type.
	 *
	 * @param str {@link String}
	 * @param cmd {@link CommandLine}
	 * @return the type
	 */
	public boolean getType(String str, final CommandLine cmd){
		boolean isValid = true;
		if(!(show_type_list.contains(str.toUpperCase()))){
			System.out.println("ERROR: Syntax error !"+ str +" is not a valid type.");
			System.out.println("Valid types are : " + show_type_list);
			isValid = false;
			return isValid;
		}
		return isValid;
	}
	
	/**
	 * @param optValues
	 * @param isValidSyntax
	 * @param relObjType
	 * @param allType
	 * @return
	 */
	public boolean checkSyntax(String[] optValues, boolean isValidSyntax) {
		StringBuilder errStb = new StringBuilder();
		for (int index = 0; index < optValues.length; index++) {
			if(relObjType.contains(optValues[index]) && index < optValues.length-1) {
				if(!allType.contains(optValues[index+1])) {
					for (int index2 = index+1; index2 < optValues.length; index2++) {
						if(!allType.contains(optValues[index2])) {
							errStb.append(optValues[index2]).append(" ");
						} else if(allType.contains(optValues[index2])) {
							System.out.println("ERROR: Syntax Error at " +"\""+errStb.toString().trim()+"\" ! use -help option for Help");
							return isValidSyntax = false;
						}
					}
					System.out.println("ERROR: Syntax Error at " +"\""+errStb.toString().trim()+"\" ! use -help option for Help");
					return isValidSyntax = false;
				}
			} else if(index == 0) {
				if(!allType.contains(optValues[index])) {
					for (int index2 = index; index2 < optValues.length; index2++) {
						if(!allType.contains(optValues[index2])) {
							errStb.append(optValues[index2]).append(" ");
						} else if(allType.contains(optValues[index2])) {
							System.out.println("ERROR: Syntax Error at " +"\""+errStb.toString().trim()+"\" ! use -help option for Help");
							return isValidSyntax = false;
						}
					}
					System.out.println("ERROR: Syntax Error at " +"\""+errStb.toString().trim()+"\" ! use -help option for Help");
					return isValidSyntax = false;
				}
			}
		}
		return isValidSyntax;
	}
	
	/**
	 * Method for Check syntax.
	 *
	 * @param optValues {@link String[]}
	 * @param cmd {@link CommandLine}
	 * @return true, if successful
	 */
	public void checkSyntax(String[] optValues) {
		StringBuilder errStb = new StringBuilder();
		for (int index = 0; index < optValues.length; index++) {
			String replace = optValues[index];
			errStb.append(replace).append(" ");
			if(index == optValues.length-1){
					System.out.println("ERROR: Syntax Error at " +"\""+errStb.toString().trim()+"\" ! use -help option for Help");
					return ;
		}
	}
		return;
	}
}
