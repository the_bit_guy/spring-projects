package com.magna.batch.utils;

import org.apache.commons.cli.CommandLine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.piterion.security.manager.windows.TicketManager;

/**
 * The Class BatchUtil.
 * 
 * @author shashwat.anand
 */
public class BatchUtil {
	/**
	 * Logger instance
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(BatchUtil.class);
	
	/** The ticket. */
	private String ticket;
	
	/** BatchUtil static reference. */
	private static BatchUtil thisRef;
	
	/**
	 * Constructor
	 */
	private BatchUtil() {
		try {
			thisRef = this;
		} catch (Exception ex) {
			LOGGER.error("Execption ocuured in creating instance of XMSystemEnvVariables instance", ex); //$NON-NLS-1$
		}
	}

	/**
	 * Gets the single instance of BatchUtil.
	 *
	 * @return single instance of BatchUtil
	 */
	public static BatchUtil getInstance() {
		if (thisRef == null) {
			new BatchUtil();
		}
		return thisRef;
	}
	
	/**
	 * Checks if is empty.
	 *
	 * @param inputStr the input str
	 * @return true, if is empty
	 */
	public static boolean isEmpty(final String inputStr) {
		if (inputStr != null && !inputStr.isEmpty()) {
			return false;
		}
		return true;
	}
	
	/**
	 * Gets the XM backend url.
	 *
	 * @return the XM backend url
	 */
	public static String getXMBackendUrl() {
		return CaxStartBatchProps.getInstance().getProperty("XMBACKEND_SERVICE_URL");
	}
	
	/**
	 * Gets the system user name.
	 *
	 * @return the system user name
	 */
	public static String getSystemUserName() {
		return BatchEnvProcess.getInstance().getenv("XM_USER");
	}
	
	/**
	 * Gets the ticket.
	 *
	 * @return the ticket
	 */
	public String getTicket() {
		return ticket;
	}

	/**
	 * Sets the ticket.
	 *
	 * @param ticket the new ticket
	 */
	public void setTicket(String ticket) {
		this.ticket = ticket;
	}
	
	/**
	 * Gets the user ticket.
	 *
	 * @return the user ticket
	 */
	public String getUserTicket() {
		if (OSValidator.isWindows()) {
			try {
				TicketManager ticketManager = new TicketManager();
				String encryptTicket = ticketManager.generateTicket();
				return encryptTicket;
			} catch (Exception e) {
				LOGGER.error("Execption ocuured while launching the application: ", e); //$NON-NLS-1$
			}

		} else if (OSValidator.isUnixOrLinux()) {
			try {
				com.piterion.security.manager.linux.TicketManager ticketManager = new com.piterion.security.manager.linux.TicketManager();
				String encryptTicket = ticketManager.generateTicket();
				return encryptTicket;
			} catch (Exception e) {
				LOGGER.error("Execption ocuured while launching the application: ", e); //$NON-NLS-1$
			}
		}
		return null;
		
	}
	
	/**
	 * Gets the string value.
	 *
	 * @param optionValues {@link String[]}
	 * @param cmd {@link CommandLine}
	 * @param objType {@link ObjectType}
	 * @return the string value
	 */
	public String getStringValue(String[] optionValues, final CommandLine cmd, ObjectType objType) {
		StringBuilder stb = new StringBuilder();
		String tempStr = "";
		int tempIndex = 0;
		for (int index = 0; index < optionValues.length; index++) {
			if (index != 0) {
				String string = optionValues[index];
				string = string.trim();
				if (!string.contains("=") && (index + 1) < optionValues.length /*|| (optionValues.length == index+1 && (index + 1) < optionValues.length)*/) {
					if(optionValues[index-1].equals("=") || optionValues[index+1].equals("=")
							||optionValues[index-1].endsWith("=")|| optionValues[index+1].startsWith("=")){
						tempStr = tempStr.concat(string);
						if (index != 1 && tempIndex + 1 == index) {
							stb = stb.append("\"").append(tempStr).append("\"").append(" ");
							tempStr = "";
							tempIndex = 0;
						}
					} else {
						System.out.println("ERROR: Syntax error :"+string);
						return null;
					}
					continue;
				} else if (string.equals("=") || (string.endsWith("=") && !string.startsWith("="))) {
					tempIndex = index;
					tempStr = tempStr.concat(string);
					if (optionValues[index+1].endsWith("=") || optionValues[index+1].startsWith("=")) {
						if(!tempStr.endsWith("=") && !tempStr.startsWith("=")){
							stb = stb.append("\"").append(tempStr).append("\"").append(" ");
							tempStr = "";
							tempIndex = 0;
							continue;
						}else {
							System.out.println("ERROR: Syntax error : "+tempStr);
							return null;
						}
					}
					continue;
				} else if ((string.startsWith("=") && !string.endsWith("="))) {
					if(index != 1 && !optionValues[index-1].endsWith("=") && !optionValues[index-1].startsWith("=")){
						tempIndex = index;
						tempStr = tempStr.concat(string);
						if(!tempStr.endsWith("=") && !tempStr.startsWith("=")){
							stb = stb.append("\"").append(tempStr).append("\"").append(" ");
							tempStr = "";
							tempIndex = 0;
							continue;
						}else {
							System.out.println("ERROR: Syntax error : "+tempStr);
							return null;
						}
					} else {
						System.out.println("ERROR: Syntax error : "+string);
						return null;
					}
					
				} else if (string.contains("=") && !string.endsWith("=")) {
					stb = stb.append("\"").append(string).append("\"").append(" ");
				} else if (optionValues.length == index+1) {
					if(optionValues[index-1].equals("=")
							||optionValues[index-1].endsWith("=")){
						tempStr = tempStr.concat(string);
						if (index != 1 && tempIndex + 1 == index) {
							stb = stb.append("\"").append(tempStr).append("\"").append(" ");
							tempStr = "";
							tempIndex = 0;
						}
					} else {
						System.out.println("ERROR: Syntax error :"+string);
						return null;
					}
					continue;
				} else {
					System.out.println("ERROR: Syntax error : "+string);
					return null;
				}
			}
        }
		ColumnChecker columnChecker = new ColumnChecker();
		String stbStr = stb.toString().replace("'", "");
		boolean column = columnChecker.getColumn(stbStr, objType, cmd);
		if(!column){
			return null;
		}
		return stbStr;
	}
	
	/*public String getStringValue(String[] optionValues, final CommandLine cmd) {
        String strVal = "";
        for (int index = 0; index < optionValues.length; index++) {
             if (index != 0) {
                  String copyStrVal = optionValues[index];
                  copyStrVal = copyStrVal.replace("'", "");
                  if (copyStrVal.matches("[a-zA-Z]+=") || copyStrVal.matches("=[0-9a-zA-Z]+")) {
                        System.out.println("ERROR: Syntax error ! eg. status='ACTIVE' or status = 'ACTIVE'");
                        return null;
                  }
                  if (!("=".equals(copyStrVal)) && copyStrVal.contains("=")) {
                        copyStrVal = copyStrVal.trim();
                        strVal = strVal.concat("\"").concat(copyStrVal).concat("\"");
                  } else {
                        strVal = strVal.concat(copyStrVal);
                  }
             }
        }
        StringBuilder returnStb = new StringBuilder();
        String patternString = "[_a-zA-Z@.]+=[_0-9a-zA-Z@\\.]+";
        Pattern pattern = Pattern.compile(patternString);
        Matcher matcher = pattern.matcher(strVal);
        while(matcher.find()) {
            returnStb.append(strVal.substring(matcher.start(), matcher.end()));
            returnStb.append(" ");
        }
        return returnStb.toString();
  }*/
	
	/**
	 * @param objName
	 * @return
	 */
	public String toCamelCase(final String objName) {
	    if (objName==null)
	        return null;

	    final StringBuilder objStb = new StringBuilder(objName.length());

	    for (final String word : objName.split(" ")) {
	        if (!word.isEmpty()) {
	            objStb.append(word.substring(0, 1).toUpperCase());
	            objStb.append(word.substring(1).toLowerCase());
	        }
	        if (!(objStb.length()==objName.length()))
	            objStb.append(" ");
	    }
	    String string = objStb.toString();
		if(string.contains("app")){
			string = string.split("app")[0];
			string = string.concat(" ").concat("App");
	    }
	    return string;
	}
	
	/**
	 * @param start_message_expiry_date
	 * @return
	 */
	public boolean isValidDate(String start_message_expiry_date) {
		String day, month,year;
		String[] split = null;
		boolean isValid = false;
		if(start_message_expiry_date.contains("/")){
			split = start_message_expiry_date.split("/");
		} else if(start_message_expiry_date.contains("-")){
			split = start_message_expiry_date.split("-");
		} else {
			return isValid;
		}
		day = split[0];
		month = split[1];
		year = split[2];
		if(Integer.parseInt(day)<32 && Integer.parseInt(month)<13 && Integer.parseInt(year) >= 1934 && Integer.parseInt(year)<10000 ){
			isValid = true;
		}
		return isValid;
	}
	
	/**
	 * Gets the rel string value.
	 *
	 * @param optionValues {@link String[]}
	 * @param cmd {@link CommandLine}
	 * @param objRelType {@link ObjectRelationType}
	 * @return the rel string value
	 */
	public String getRelStringValue(String[] optionValues, final CommandLine cmd, ObjectRelationType objRelType) {
		StringBuilder stb = new StringBuilder();
		String tempStr = "";
		int tempIndex = 0;
		for (int index = 0; index < optionValues.length; index++) {
				String string = optionValues[index];
				string = string.trim();
				if (!string.contains("=") && (index + 1) < optionValues.length ) {
					if(index==0 || optionValues[index-1].equals("=") || optionValues[index+1].equals("=")
							||optionValues[index-1].endsWith("=")|| optionValues[index+1].startsWith("=")){
						tempStr = tempStr.concat(string);
						if (index != 1 && tempIndex + 1 == index) {
							stb = stb.append("\"").append(tempStr).append("\"").append(" ");
							tempStr = "";
							tempIndex = 0;
						}
					} else {
						System.out.println("ERROR: Syntax error :"+string);
						return null;
					}
					continue;
				} else if (string.equals("=") || (string.endsWith("=") && !string.startsWith("="))) {
					tempIndex = index;
					tempStr = tempStr.concat(string);
					if (optionValues[index+1].endsWith("=") || optionValues[index+1].startsWith("=")) {
						if(!tempStr.endsWith("=") && !tempStr.startsWith("=")){
							stb = stb.append("\"").append(tempStr).append("\"").append(" ");
							tempStr = "";
							tempIndex = 0;
							continue;
						}else {
							System.out.println("ERROR: Syntax error : "+tempStr);
							return null;
						}
					}
					continue;
				} else if ((string.startsWith("=") && !string.endsWith("="))) {
					if(index==0 || (index != 0 && !optionValues[index-1].endsWith("=") && !optionValues[index-1].startsWith("="))){
						tempIndex = index;
						tempStr = tempStr.concat(string);
						if(!tempStr.endsWith("=") && !tempStr.startsWith("=")){
							stb = stb.append("\"").append(tempStr).append("\"").append(" ");
							tempStr = "";
							tempIndex = 0;
							continue;
						}else {
							System.out.println("ERROR: Syntax error : "+tempStr);
							return null;
						}
					} else {
						System.out.println("ERROR: Syntax error : "+string);
						return null;
					}
					
				} else if (string.contains("=") && !string.endsWith("=")) {
					stb = stb.append("\"").append(string).append("\"").append(" ");
				} else if (optionValues.length == index+1) {
					if(index==0 || optionValues[index-1].equals("=")
							||optionValues[index-1].endsWith("=")){
						tempStr = tempStr.concat(string);
						if (index != 1 && tempIndex + 1 == index) {
							stb = stb.append("\"").append(tempStr).append("\"").append(" ");
							tempStr = "";
							tempIndex = 0;
						}
					} else {
						System.out.println("ERROR: Syntax error :"+string);
						return null;
					}
					continue;
				} else {
					System.out.println("ERROR: Syntax error : "+string);
					return null;
				}
        }
		ColumnChecker columnChecker = new ColumnChecker();
		String stbStr = stb.toString().replace("'", "");
		boolean column = columnChecker.getRelColumn(stbStr, objRelType, cmd);
		if(!column){
			return null;
		}
		return stbStr;
	}
	
	/**
	 * Method for Syntax checker.
	 *
	 * @param cmd {@link CommandLine}
	 * @return true, if successful
	 */
	public boolean syntaxChecker(final CommandLine cmd) {
		boolean isValidSyntax = false;
		String[] optValues;
		if(cmd.hasOption("type")) {
			optValues = cmd.getOptionValues("type");
			isValidSyntax = checkSyntax(optValues);
			if(!isValidSyntax) return isValidSyntax;
		}
		if(cmd.hasOption("select")) {
			optValues = cmd.getOptionValues("select");
			isValidSyntax = checkSyntax(optValues);
			if(!isValidSyntax) return isValidSyntax;
		}
		if(cmd.hasOption("fields")) {
			optValues = cmd.getOptionValues("fields");
			isValidSyntax = checkSyntax(optValues);
			if(!isValidSyntax) return isValidSyntax;
		}
		if(cmd.hasOption("where")) {
			optValues = cmd.getOptionValues("where");
			isValidSyntax = checkSyntax(optValues);
			if(!isValidSyntax) return isValidSyntax;
		}
		if(cmd.hasOption("add")) {
			optValues = cmd.getOptionValues("add");
			isValidSyntax = checkSyntax(optValues);
			if(!isValidSyntax) return isValidSyntax;
		}
		if(cmd.hasOption("remove")) {
			optValues = cmd.getOptionValues("remove");
			isValidSyntax = checkSyntax(optValues);
			if(!isValidSyntax) return isValidSyntax;
		}
		if(cmd.hasOption("create")) {
			optValues = cmd.getOptionValues("create");
			isValidSyntax = checkSyntax(optValues);
			if(!isValidSyntax) return isValidSyntax;
		}
		if(cmd.hasOption("update")) {
			optValues = cmd.getOptionValues("update");
			isValidSyntax = checkSyntax(optValues);
			if(!isValidSyntax) return isValidSyntax;
		}
		if(cmd.hasOption("delete")) {
			optValues = cmd.getOptionValues("delete");
			isValidSyntax = checkSyntax(optValues);
			if(!isValidSyntax) return isValidSyntax;
		}
		return isValidSyntax;
	}

	/**
	 * @param optValues
	 */
	private boolean checkSyntax(String[] optValues) {
		for (int index = 0; index < optValues.length; index++) {
			if(optValues[index].startsWith("-") || optValues[index].endsWith("-")) {
				System.out.println("ERROR: Syntax Error at " +"\""+optValues[index]+"\" ! use -help option for Help");
				return false;
			}
		}
		return true;
	}
	
	/**
	 * Method for Check options.
	 *
	 * @param optValues {@link String[]}
	 * @return true, if successful
	 */
	public boolean checkOptions(String[] optValue) {
		String[] optValues = optValue;
		boolean isValidSyntax = true;
		ColumnChecker checker = new ColumnChecker();
		isValidSyntax = checker.checkSyntax(optValues, isValidSyntax);
		return isValidSyntax;
	}
}
