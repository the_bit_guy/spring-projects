package com.magna.batch.utils;

/**
 * Enum for Object relation type.
 *
 * @author Roshan.Ekka
 */
public enum ObjectRelationType {

	/** Member variable 'site adminarea' for {@link ObjectRelationType}. */
	SITE_ADMINAREA,
	
	/** Member variable 'adminarea project' for {@link ObjectRelationType}. */
	ADMINAREA_PROJECT,
	
	/** Member variable 'adminarea userapp' for {@link ObjectRelationType}. */
	ADMINAREA_USERAPP,
	
	/** Member variable 'adminarea startapp' for {@link ObjectRelationType}. */
	ADMINAREA_STARTAPP,
	
	/** Member variable 'adminarea project projectapp' for {@link ObjectRelationType}. */
	ADMINAREA_PROJECT_PROJECTAPP,
	
	/** Member variable 'adminarea project startapp' for {@link ObjectRelationType}. */
	ADMINAREA_PROJECT_STARTAPP,
	
	/** Member variable 'user startapp' for {@link ObjectRelationType}. */
	USER_STARTAPP,
	
	/** Member variable 'user userapp' for {@link ObjectRelationType}. */
	USER_USERAPP,
	
	/** Member variable 'user project' for {@link ObjectRelationType}. */
	USER_PROJECT,
	
	/** Member variable 'user project usage' for {@link ObjectRelationType}. */
	USER_PROJECT_USAGE,
	
	/** Member variable 'user project projectapp' for {@link ObjectRelationType}. */
	USER_PROJECT_PROJECTAPP,
	
	/** Member variable 'baseapp userapp' for {@link ObjectRelationType}. */
	BASEAPP_USERAPP,
	
	/** Member variable 'baseapp projectapp' for {@link ObjectRelationType}. */
	BASEAPP_PROJECTAPP,
	
	/** Member variable 'baseapp startapp' for {@link ObjectRelationType}. */
	BASEAPP_STARTAPP;
	
	/**
	 * Gets the object relation type.
	 *
	 * @return the object relation type
	 */
	public static String[] getObjectRelationType() {
		ObjectRelationType[] values = values();
		String[] returnArray = new String[values.length];
		for (int index = 0; index < values.length; index++) {
			returnArray[index] = values[index].name();
		}
		return returnArray;
	}
	
	/**
	 * Gets the object type.
	 *
	 * @param objectType the object type
	 * @return the object type
	 */
	public static ObjectRelationType getObjectRelType(String objectType) {
        for (ObjectRelationType value : values()) {
            if (objectType.equalsIgnoreCase(value.name())) {
                return value;
            }
        }
        return null;
    }
}
