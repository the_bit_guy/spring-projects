package com.magna.batch.utils;

import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class XmSystemEnvProcess.
 * 
 * @author shashwat.anand
 */
public class BatchEnvProcess {

	/**
	 * Logger instance
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(BatchEnvProcess.class);

	/**
	 * XMSystemProperties static reference
	 */
	private static BatchEnvProcess thisRef;
	
	/** The Constant DEFAULT_LANGUAGE. */
	private static final String DEFAULT_LANGUAGE = "English";

	private Map<String, String> environment;
	
	/**
	 * Constructor
	 */
	private BatchEnvProcess() {
		try {
			thisRef = this;
		} catch (Exception ex) {
			LOGGER.error("Execption ocuured in creating instance of BatchEnvProcess instance", ex); //$NON-NLS-1$
		}
	}

	/**
	 * Gets the single instance of XmSystemEnvProcess.
	 *
	 * @return single instance of XmSystemEnvProcess
	 */
	public static BatchEnvProcess getInstance() {
		if (thisRef == null) {
			new BatchEnvProcess();
		}
		return thisRef;
	}

	/**
	 * Method for Start.
	 *
	 * @param application {@link APPLICATION}
	 */
	public void start() {
		this.environment = new HashMap<String, String>(System.getenv());
		initSystemSpecificVariables();
		initVariables();
	}

	/**
	 * Method for Inits the system specific variables.
	 */
	private void initSystemSpecificVariables() {
		this.environment.put("XM_VERSION", "1.1");
		this.environment.put("XM_USER", System.getenv("USERNAME"));
		this.environment.put("XM_HOST", getHostName());
	}

	/**
	 * Method for Initializing environmental variables, if there is no bath
	 * execution.
	 */
	private void initVariables() {
		String cadDriveLetter;
		if ((cadDriveLetter = System.getenv("CAD_DRIVE_LETTER")) != null) { //$NON-NLS-1$
			cadDriveLetter = cadDriveLetter.trim();
		}
		String xmSiteName;
		if ((xmSiteName = System.getenv("XM_SITENAME")) != null) { //$NON-NLS-1$
			xmSiteName = xmSiteName.trim();
		}
		String systemPath;
		if ((systemPath = System.getenv("SYSTEM_PATH")) != null) { //$NON-NLS-1$
			this.environment.put("Path", systemPath);
		}
		String lang;
		if ((lang = System.getenv("XM_LANG")) != null) { //$NON-NLS-1$
			this.environment.put("XM_LANG", lang);
		} else {
			this.environment.put("XM_LANG", DEFAULT_LANGUAGE);
		}
		String xmSystemInstalltionPath = "c:/cax/xmsystem/";
		if (OSValidator.isUnixOrLinux()) {
			xmSystemInstalltionPath = System.getProperty("user.home") + "/cax/xmsystem";
			//cadDriveLetter = "/run/user/1000/gvfs/smb-share:server=192.168.1.107,share=magna_share";
		}

		if (this.environment.get("XM_PATH") == null) { //$NON-NLS-1$
			this.environment.put("XM_PATH", xmSystemInstalltionPath);
		}
		if (this.environment.get("XM_CONFIG") == null) { //$NON-NLS-1$
			this.environment.put("XM_CONFIG", xmSystemInstalltionPath + "/xmsystem.properties");
		}
		if (this.environment.get("CAD_SCRIPT_DIR") == null) { //$NON-NLS-1$
			this.environment.put("CAD_SCRIPT_DIR", cadDriveLetter + "/scripts");
		}
		if (this.environment.get("XM_SITE") == null) { //$NON-NLS-1$
			this.environment.put("XM_SITE", xmSiteName);
		}
	}

	/**
	 * Gets the env.
	 *
	 * @param envVariable {@link String}
	 * @return the env
	 */
	public String getenv(String variable) {
		return this.environment.get(variable);
	}
	
	/**
	 * Gets the environment map.
	 *
	 * @return the environment map
	 */
	public Map<String, String> getEnvironmentMap() {
		return this.environment;
	}

	/**
	 * Update env variable.
	 *
	 * @param key
	 *            the key
	 * @param value
	 *            the value
	 */
	public void updateEnvVariable(String key, String value) {
		this.environment.put(key, value);
	}

	/**
	 * Gets the host name.
	 *
	 * @return the host name
	 */
	private String getHostName() {
		String xmHost = null;

		try {
			java.net.InetAddress localMachine = java.net.InetAddress.getLocalHost();
			if ((xmHost = localMachine.getHostName()) != null) {
				xmHost = xmHost.trim();
			}
		} catch (UnknownHostException e) {
			LOGGER.error("Exception occured while getting host name!");
		}
		return xmHost;
	}
}
