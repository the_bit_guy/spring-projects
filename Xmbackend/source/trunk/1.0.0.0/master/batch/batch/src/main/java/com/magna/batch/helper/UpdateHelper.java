package com.magna.batch.helper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import org.apache.commons.cli.CommandLine;

import com.magna.batch.restclient.exception.UnauthorizedAccessException;
import com.magna.batch.restclient.icons.IconController;
import com.magna.batch.restclient.obj.baseapp.BaseAppController;
import com.magna.batch.utils.BatchUtil;
import com.magna.batch.utils.ColumnChecker;
import com.magna.batch.utils.CommonConstants;
import com.magna.batch.utils.ObjectType;
import com.magna.xmbackend.exception.CannotCreateObjectException;
import com.magna.xmbackend.vo.adminArea.AdminAreaRequest;
import com.magna.xmbackend.vo.adminArea.AdminAreaTranslation;
import com.magna.xmbackend.vo.baseApplication.BaseApplicationRequest;
import com.magna.xmbackend.vo.baseApplication.BaseApplicationTransulation;
import com.magna.xmbackend.vo.enums.Status;
import com.magna.xmbackend.vo.icon.IconRespWrapper;
import com.magna.xmbackend.vo.icon.IkonRequest;
import com.magna.xmbackend.vo.jpa.site.SiteRequest;
import com.magna.xmbackend.vo.jpa.site.SiteTranslation;
import com.magna.xmbackend.vo.project.ProjectRequest;
import com.magna.xmbackend.vo.project.ProjectTranslation;
import com.magna.xmbackend.vo.projectApplication.ProjectApplicationCustomResponse;
import com.magna.xmbackend.vo.projectApplication.ProjectApplicationRequest;
import com.magna.xmbackend.vo.projectApplication.ProjectApplicationTransulation;
import com.magna.xmbackend.vo.startApplication.StartApplicationCustomResponse;
import com.magna.xmbackend.vo.startApplication.StartApplicationRequest;
import com.magna.xmbackend.vo.startApplication.StartApplicationTranslation;
import com.magna.xmbackend.vo.user.UserRequest;
import com.magna.xmbackend.vo.user.UserTranslation;
import com.magna.xmbackend.vo.userApplication.UserApplicationCustomResponse;
import com.magna.xmbackend.vo.userApplication.UserApplicationRequest;
import com.magna.xmbackend.vo.userApplication.UserApplicationTranslation;

/**
 * Class for Update helper.
 *
 * @author Roshan.Ekka
 */
public class UpdateHelper {
	
	/** Member variable 'this ref' for {@link UpdateHelper}. */
	private static UpdateHelper thisRef;
	
	/**
	 * Constructor for UpdateHelper Class.
	 */
	private UpdateHelper() {
		thisRef = this;
	}

	/**
	 * Gets the single instance of UpdateHelper.
	 *
	 * @return single instance of UpdateHelper
	 */
	public static UpdateHelper getInstance() {
		if (thisRef == null) {
			new UpdateHelper();
		}
		return thisRef;
	}
	/**
	 * @param deleteObjType
	 */
	public void updateObject(ObjectType updateObjType, final CommandLine cmd) {
		switch (updateObjType) {
		case USER:
			updateUser(cmd, updateObjType);
			break;
		case SITE:
			updateSite(cmd, updateObjType);
			break;
		case ADMINAREA:
			updateAdminArea(cmd, updateObjType);
			break;
		case PROJECT:
			updateProject(cmd, updateObjType);
			break;
		case BASEAPP:
			updateBaseApp(cmd, updateObjType);
			break;
		case USERAPP:
			updateUserApp(cmd, updateObjType);
			break;
		case PROJECTAPP:
			updateProjectApp(cmd, updateObjType);
			break;
		case STARTAPP:
			updateStartApp(cmd, updateObjType);
		default:
			break;
		}
	}
	
	/**
	 * Method for Update start app.
	 *
	 * @param cmd {@link CommandLine}
	 */
	private void updateStartApp(CommandLine cmd, ObjectType updateObjType) {
		String startAppName = null;
		startAppName = getName(cmd, updateObjType);
		if(startAppName==null) return;
		com.magna.batch.restclient.obj.startapp.StartAppController startAppController = new com.magna.batch.restclient.obj.startapp.StartAppController();
		StartApplicationRequest startAppRequest = new StartApplicationRequest();
		StartApplicationCustomResponse startAppByName = startAppController.getStartAppByName(startAppName);
		if(startAppByName==null) return;
		String statusStr = null, iconStr = null, is_message = startAppByName.getIsMessage(), application = null, dbApplication = startAppByName.getBaseAppName(), start_message_expiry_date = null,
				desc_en = null, desc_de = null, remarks_en = null, remarks_de = null,
				strVal = "";
		
		IconRespWrapper iconRespWrapper = null;
		List<StartApplicationTranslation> startAppTranslation = new ArrayList<>();
		StartApplicationTranslation startAppTranslationTbl_en = new StartApplicationTranslation();
		startAppTranslationTbl_en.setLanguageCode(com.magna.batch.utils.LANG_ENUM.ENGLISH.getLangCode());
		startAppTranslation.add(startAppTranslationTbl_en);
		StartApplicationTranslation startAppTranslationTbl_de = new StartApplicationTranslation();
		startAppTranslationTbl_de.setLanguageCode(com.magna.batch.utils.LANG_ENUM.GERMAN.getLangCode());
		startAppTranslation.add(startAppTranslationTbl_de);
		startAppRequest.setStartApplicationTranslations(startAppTranslation);
		String[] optionValues = cmd.getOptionValues("update");
		if (optionValues.length > 0) {
			strVal = BatchUtil.getInstance().getStringValue(optionValues, cmd, updateObjType);
			if(strVal==null) return;
			String[] splittedStrVal = strVal.split("\"");
			String value;
			for (int i = 0; i < splittedStrVal.length; i++) {
				value = splittedStrVal[i];
				value = value.trim();
				if(!name(value)) return;
				if (value.split("=")[0].trim().equalsIgnoreCase("STATUS") && !value.endsWith("=")) {
					if (value.contains("=")) {
						statusStr = value.split("=", 2)[1];
						statusStr = statusStr.toUpperCase();
						if(!(statusStr.equalsIgnoreCase("active") || statusStr.equalsIgnoreCase("inactive"))){
							System.out.println("ERROR: Syntax error ! Status should be ACTIVE or INACTIVE");
							return;
						}
					} else {
						System.out.println("ERROR: Syntax error !");
						return;
					}
					continue;
				} else if (value.split("=")[0].trim().equalsIgnoreCase("STATUS") && value.endsWith("=")) {
					System.out.println("ERROR: Syntax error ! Status should be ACTIVE or INACTIVE");
					return;
				} else if(value.startsWith("STATUS") || value.startsWith("status")) {
					System.out.println("ERROR: Syntax error !");
					return;
				}
				
				if (value.split("=")[0].trim().equalsIgnoreCase("ICON_NAME") && !value.endsWith("=")) {
					if (value.contains("=")) {
						iconStr = value.split("=", 2)[1];
					} else {
						System.out.println("ERROR: Syntax error !");
						return;
					}
					continue;
				} else if (value.split("=")[0].trim().equalsIgnoreCase("ICON_NAME") && value.endsWith("=")) {
					iconStr="";
					continue;
				} else if(value.startsWith("ICON_NAME") || value.startsWith("icon_name")){
					System.out.println("ERROR: Syntax error !");
					return;
				}
				
				if (value.split("=")[0].trim().equalsIgnoreCase("IS_MESSAGE") && !value.endsWith("=")) {
					is_message = value.split("=", 2)[1];
					continue;
				} else if (value.split("=")[0].trim().equalsIgnoreCase("IS_MESSAGE") && value.endsWith("=")) {
					is_message = "";
					if(is_message.equalsIgnoreCase("true") || is_message.equalsIgnoreCase("false")){
						boolean parseBoolean = Boolean.parseBoolean(is_message);
						startAppRequest.setIsMessage(parseBoolean);
					} else {
						System.out.println("ERROR: Syntax error ! is_message should be TRUE or FALSE. Eg: is_message=true");
						return;
					}
					continue;
				} else if(value.startsWith("IS_MESSAGE") || value.startsWith("is_message")){
					System.out.println("ERROR: Syntax error !");
					return;
				}
				
				if (value.split("=")[0].trim().equalsIgnoreCase("APPLICATION") && !value.endsWith("=")) {
					application = value.split("=", 2)[1];
					continue;
				} else if (value.split("=")[0].trim().equalsIgnoreCase("APPLICATION") && value.endsWith("=")) {
					application = "";
					continue;
				} else if(value.startsWith("APPLICATION") || value.startsWith("application")){
					System.out.println("ERROR: Syntax error !");
					return;
				}
				
				if (value.split("=")[0].trim().equalsIgnoreCase("START_MESSAGE_EXPIRY_DATE") && !value.endsWith("=")) {
					start_message_expiry_date = value.split("=", 2)[1];
					continue;
				} else if (value.split("=")[0].trim().equalsIgnoreCase("START_MESSAGE_EXPIRY_DATE") && value.endsWith("=")) {
					start_message_expiry_date = "";
					/*try {
						startAppRequest.setStartMsgExpiryDate(new SimpleDateFormat("DD-MON-RR HH.MI.SSXFF AM").parse(start_message_expiry_date));
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}*/
					continue;
				} else if(value.startsWith("START_MESSAGE_EXPIRY_DATE") || value.startsWith("start_message_expiry_date")){
					System.out.println("ERROR: Syntax error !");
					return;
				}
				
				if (value.split("=")[0].trim().equalsIgnoreCase("DESCRIPTION_EN") && !value.endsWith("=")) {
					if(value.contains("\"")){
						if (value.contains("=")) {
							desc_en = value.split("=\"", 2)[1];
							if(!(desc_en.length()<240)){
								System.out.println("Please provide a valid description !" +CommonConstants.Messages.DESC_LIMIT_MESSAGE);
								return;
							}
						} else {
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}else{
						if (value.contains("=")) {
							desc_en = value.split("=", 2)[1];
							if(!(desc_en.length()<240)){
								System.out.println("Please provide a valid description !" +CommonConstants.Messages.DESC_LIMIT_MESSAGE);
								return;
							}
						} else {
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}
					continue;
				} else if (value.split("=")[0].trim().equalsIgnoreCase("DESCRIPTION_EN") && value.endsWith("=")) {
					desc_en = "";
					startAppTranslationTbl_en.setDescription(desc_en);
					continue;
				} else if(value.startsWith("DESCRIPTION_EN") || value.startsWith("description_en")){
					System.out.println("ERROR: Syntax error !");
					return;
				} else if (value.split("=")[0].trim().equalsIgnoreCase("DESCRIPTION_DE") && !value.endsWith("=")) {
					if(value.contains("\"")){
						if (value.contains("=")) {
							desc_de = value.split("=\"", 2)[1];
							if(!(desc_de.length()<240)){
								System.out.println("Please provide a valid description !" +CommonConstants.Messages.DESC_LIMIT_MESSAGE);
								return;
							}
						} else {
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}else{
						if (value.contains("=")) {
							desc_de = value.split("=", 2)[1];
							if(!(desc_de.length()<240)){
								System.out.println("Please provide a valid description !" +CommonConstants.Messages.DESC_LIMIT_MESSAGE);
								return;
							}
						} else {
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}
					continue;
				} else if (value.split("=")[0].trim().equalsIgnoreCase("DESCRIPTION_DE") && value.endsWith("=")) {
					desc_de = "";
					startAppTranslationTbl_de.setDescription(desc_de);
					continue;
				} else if(value.startsWith("DESCRIPTION_DE") || value.startsWith("description_de")) {
					System.out.println("ERROR: Syntax error !");
					return;
				} 
				if (value.split("=")[0].trim().equalsIgnoreCase("REMARKS_EN") && !value.endsWith("=")) {
					if(value.contains("\"")){
						if (value.contains("=")) {
							remarks_en = value.split("=\"", 2)[1];
							if(!(remarks_en.length()<1500)){
								System.out.println("Please provide a valid remarks !" +CommonConstants.Messages.REMARKS_LIMIT_MESSAGE);
								return;
							}
						} else {
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}else{
						if (value.contains("=")) {
							remarks_en = value.split("=", 2)[1];
							if(!(remarks_en.length()<1500)){
								System.out.println("Please provide a valid remarks !" +CommonConstants.Messages.REMARKS_LIMIT_MESSAGE);
								return;
							}
						} else {
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}
					continue;
				} else if (value.split("=")[0].trim().equalsIgnoreCase("REMARKS_EN") && value.endsWith("=")) {
					remarks_en = "";
					startAppTranslationTbl_en.setRemarks(remarks_en);
					continue;
				} else if(value.startsWith("REMARKS_EN") || value.startsWith("remarks_en")){
					System.out.println("ERROR: Syntax error !");
					return;
				}
				if (value.split("=")[0].trim().equalsIgnoreCase("REMARKS_DE") && !value.endsWith("=")) {
					if(value.contains("\"")){
						if (value.contains("=")) {
							remarks_de = value.split("=\"", 2)[1];
							if(!(remarks_de.length()<1500)){
								System.out.println("Please provide a valid remarks !" +CommonConstants.Messages.REMARKS_LIMIT_MESSAGE);
								return;
							}
						} else {
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}else{
						if (value.contains("=")) {
							remarks_de = value.split("=", 2)[1];
							if(!(remarks_de.length()<1500)){
								System.out.println("Please provide a valid remarks !" +CommonConstants.Messages.REMARKS_LIMIT_MESSAGE);
								return;
							}
						} else {
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}
					continue;
				} else if (value.split("=")[0].trim().equalsIgnoreCase("REMARKS_DE") && value.endsWith("=")) {
					remarks_de = "";
					startAppTranslationTbl_de.setRemarks(remarks_de);
					continue;
				} else if(value.startsWith("REMARKS_DE") || value.startsWith("remarks_de")) {
					System.out.println("ERROR: Syntax error !");
					return;
				}
			}
			Date startMessageExpiryDate = startAppByName.getStartMessageExpiryDate();
			if(is_message!=null && is_message.equalsIgnoreCase("false")){
				if(BatchUtil.isEmpty(application) && dbApplication==null){
						System.out.println("application is required");
						return;
				} else if(!BatchUtil.isEmpty(start_message_expiry_date)){
					System.out.println("You could not assign start message expiry date, If Start Message is 'false'");
					return;
				} 
			} else if(is_message!=null && is_message.equalsIgnoreCase("true")){
				if(BatchUtil.isEmpty(start_message_expiry_date) && startMessageExpiryDate==null){
					System.out.println("start_message_expiry_date is required");
					return;
				} else if(!BatchUtil.isEmpty(application)){
					System.out.println("You could not assign Base Application, If Start Message is 'true'");
					return;
				}
			} 
			
			if(iconStr!=null){
				IconController iconController = new IconController();
				IkonRequest ikonRequest = new IkonRequest();
				ikonRequest.setIconName(iconStr);
				iconRespWrapper = iconController.getIconByName(ikonRequest);
				if(iconRespWrapper==null) return;
			}
			String baseAppId = null;
			if (!BatchUtil.isEmpty(application)) {
				BaseAppController baseAppController = new BaseAppController();
				baseAppId = baseAppController.getBaseAppByName(application);
				if(baseAppId==null) return;
				startAppRequest.setBaseAppId(baseAppId);
			}
			if (!BatchUtil.isEmpty(startAppName)) {
				startAppRequest.setName(startAppName);
			}
			if (!BatchUtil.isEmpty(is_message)) {
				if(is_message.equalsIgnoreCase("true") || is_message.equalsIgnoreCase("false")){
					boolean parseBoolean = Boolean.parseBoolean(is_message);
					startAppRequest.setIsMessage(parseBoolean);
				} else {
					System.out.println("ERROR: Syntax error ! is_message should be TRUE or FALSE. Eg: is_message=true");
					return;
				}
			}
			
			if (!BatchUtil.isEmpty(start_message_expiry_date)) {
				try {
					Date todaysDate = new Date();
					Date endDate = null;
					if(start_message_expiry_date.matches("[0-9][0-9]/[0-9][0-9]/[0-9][0-9][0-9][0-9]")){
						if(!BatchUtil.getInstance().isValidDate(start_message_expiry_date)){
							System.out.println("Please provide a valid date. Eg. start_message_expiry_date = DD-MM-YYYY or DD/MM/YYYY");
							return;
						}else {
							endDate=new SimpleDateFormat("dd/MM/yyyy").parse(start_message_expiry_date);
						}
					} else if(start_message_expiry_date.matches("[0-9][0-9]-[0-9][0-9]-[0-9][0-9][0-9][0-9]")){
						if(!BatchUtil.getInstance().isValidDate(start_message_expiry_date)){
							System.out.println("Please provide a valid date. Eg. start_message_expiry_date = DD-MM-YYYY or DD/MM/YYYY");
							return;
						} else {
							endDate=new SimpleDateFormat("dd-MM-yyyy").parse(start_message_expiry_date);
						}
					}else{
						System.out.println("ERROR: Syntax error ! Eg. start_message_expiry_date = DD-MM-YYYY or DD/MM/YYYY");
						return;
					}
					
					SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.YYYY HH:mm:ss.SSS");
					if (!(endDate.after(todaysDate) || sdf.format(endDate).equals(sdf.format(todaysDate)))) {
						System.out.println("Expiry date must be after your current date time");
						return;
					}
					startAppRequest.setStartMsgExpiryDate(endDate);
				} catch (ParseException e) {
					System.out.println("Unable to Parse start_message_expiry_date. Eg. start_message_expiry_date = DD-MM-YYYY or DD/MM/YYYY");
					return;
				}
			} 
			if (!BatchUtil.isEmpty(statusStr)) {
				startAppRequest.setStatus(statusStr);
			} 
			if (!BatchUtil.isEmpty(iconStr) && iconRespWrapper!=null) {
				startAppRequest.setIconId(iconRespWrapper.getIconId());
			} 
			if (!BatchUtil.isEmpty(desc_en)) {
				startAppTranslationTbl_en.setDescription(desc_en);
			} 
			if (!BatchUtil.isEmpty(remarks_en)) {
				startAppTranslationTbl_en.setRemarks(remarks_en);
			}
			if (!BatchUtil.isEmpty(desc_de)) {
				startAppTranslationTbl_de.setDescription(desc_de);
			}
			if (!BatchUtil.isEmpty(remarks_de)) {
				startAppTranslationTbl_de.setRemarks(remarks_de);
			}
			List<StartApplicationTranslation> startAppTranslationTblList = new ArrayList<StartApplicationTranslation>();
			startAppTranslationTblList.add(startAppTranslationTbl_en);
			startAppTranslationTblList.add(startAppTranslationTbl_de);
			startAppRequest.setStartApplicationTranslations(startAppTranslationTblList);

			Boolean updateStartApp = startAppController.updateStartApp(startAppRequest);
				if (updateStartApp == null || updateStartApp == Boolean.FALSE) {
					return;
				} else {
					System.out.println("Start Application updated Successfully");
				}
		}
	}

	/**
	 * Method for Update project app.
	 *
	 * @param cmd {@link CommandLine}
	 */
	private void updateProjectApp(CommandLine cmd, ObjectType updateObjType) {
		String projectAppName = null;
		projectAppName = getName(cmd, updateObjType);
		if(projectAppName==null) return;
		com.magna.batch.restclient.obj.projectapp.ProjectAppController projectAppController = new com.magna.batch.restclient.obj.projectapp.ProjectAppController();
		ProjectApplicationRequest projectAppRequest = new ProjectApplicationRequest();
		ProjectApplicationCustomResponse projectAppByName = projectAppController.getProjectAppByName(projectAppName);
		if(projectAppByName==null) return;
		String statusStr = null, iconStr = null, desc = null,is_parent = null, db_isParent = projectAppByName.getIsParent(), is_singleton = null, db_is_singleton = projectAppByName.getIsSingleton(),
				position = null, dbPosition = projectAppByName.getPosition(), application = null, dbApplication = projectAppByName.getBaseAppName(), name_en = null, name_de = null, desc_en = null, desc_de = null, remarks_en = null, remarks_de = null,
				strVal = "";
		IconRespWrapper iconRespWrapper = null;
		List<ProjectApplicationTransulation> projectAppTranslation = new ArrayList<>();
		ProjectApplicationTransulation projectAppTranslationTbl_en = new ProjectApplicationTransulation();
		projectAppTranslationTbl_en.setLanguageCode(com.magna.batch.utils.LANG_ENUM.ENGLISH.getLangCode());
		projectAppTranslation.add(projectAppTranslationTbl_en);
		ProjectApplicationTransulation projectAppTranslationTbl_de = new ProjectApplicationTransulation();
		projectAppTranslationTbl_de.setLanguageCode(com.magna.batch.utils.LANG_ENUM.GERMAN.getLangCode());
		projectAppTranslation.add(projectAppTranslationTbl_de);
		projectAppRequest.setProjectApplicationTransulations(projectAppTranslation);
		String[] optionValues = cmd.getOptionValues("update");
		if (optionValues.length > 0) {
			strVal = BatchUtil.getInstance().getStringValue(optionValues, cmd, updateObjType);
			if(strVal==null) return;
			String[] splittedStrVal = strVal.split("\"");
			String value;
			for (int i = 0; i < splittedStrVal.length; i++) {
				value = splittedStrVal[i];
				value = value.trim();
				
				if (value.split("=")[0].trim().equalsIgnoreCase("NAME_EN") && !value.endsWith("=")) {
					name_en = value.split("=", 2)[1];
					if(!name_en.matches(CommonConstants.RegularExpressions.ALLOWED_APP_NAME_REGEX)){
						System.out.println("Please provide a valid name !" +CommonConstants.Messages.NAME_MESSAGE);
						return;
					} else if(!(name_en.length()<30)){
						System.out.println("Please provide a valid name !" +CommonConstants.Messages.NAME_LIMIT_MESSAGE);
						return;
					}
					continue;
				} else if (value.split("=")[0].trim().equalsIgnoreCase("NAME_EN") && value.endsWith("=")) {
					name_en = "";
					projectAppTranslationTbl_en.setName(name_en);
					continue;
				} else if(value.startsWith("NAME_EN") || value.startsWith("name_en")){
					System.out.println("ERROR: Syntax error !");
					return;
				} else if (value.split("=")[0].trim().equalsIgnoreCase("NAME_DE") && !value.endsWith("=")) {
					name_de = value.split("=", 2)[1];
					if(!name_de.matches(CommonConstants.RegularExpressions.ALLOWED_APP_NAME_REGEX)){
						System.out.println("Please provide a valid name !" +CommonConstants.Messages.NAME_MESSAGE);
						return;
					} else if(!(name_de.length()<30)){
						System.out.println("Please provide a valid name !" +CommonConstants.Messages.NAME_LIMIT_MESSAGE);
						return;
					}
					continue;
				} else if (value.split("=")[0].trim().equalsIgnoreCase("NAME_DE") && value.endsWith("=")) {
					name_de = "";
					projectAppTranslationTbl_de.setName(name_de);
					continue;
				} else if(value.startsWith("NAME_DE") || value.startsWith("name_de")){
					System.out.println("ERROR: Syntax error !");
					return;
				} else if(!name(value)) return;
				
				if (value.split("=")[0].trim().equalsIgnoreCase("STATUS") && !value.endsWith("=")) {
					if (value.contains("=")) {
						statusStr = value.split("=", 2)[1];
						statusStr = statusStr.toUpperCase();
						if(!(statusStr.equalsIgnoreCase("active") || statusStr.equalsIgnoreCase("inactive"))){
							System.out.println("ERROR: Syntax error ! Status should be ACTIVE or INACTIVE");
							return;
						}
					} else {
						System.out.println("ERROR: Syntax error !");
						return;
					}
					continue;
				} else if (value.split("=")[0].trim().equalsIgnoreCase("STATUS") && value.endsWith("=")) {
					System.out.println("ERROR: Syntax error ! Status should be ACTIVE or INACTIVE");
					return;
				} else if(value.startsWith("STATUS") || value.startsWith("status")) {
					System.out.println("ERROR: Syntax error !");
					return;
				}
				
				if (value.split("=")[0].trim().equalsIgnoreCase("ICON_NAME") && !value.endsWith("=")) {
					if (value.contains("=")) {
						iconStr = value.split("=", 2)[1];
					} else {
						System.out.println("ERROR: Syntax error !");
						return;
					}
					continue;
				} else if (value.split("=")[0].trim().equalsIgnoreCase("ICON_NAME") && value.endsWith("=")) {
					iconStr="";
					continue;
				} else if(value.startsWith("ICON_NAME") || value.startsWith("icon_name")){
					System.out.println("ERROR: Syntax error !");
					return;
				}
				if (value.split("=")[0].trim().equalsIgnoreCase("DESCRIPTION_EN") && !value.endsWith("=")) {
					if(value.contains("\"")){
						if (value.contains("=")) {
							desc_en = value.split("=\"", 2)[1];
							if(!(desc_en.length()<240)){
								System.out.println("Please provide a valid description !" +CommonConstants.Messages.DESC_LIMIT_MESSAGE);
								return;
							}
						} else {
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}else{
						if (value.contains("=")) {
							desc_en = value.split("=", 2)[1];
							if(!(desc_en.length()<240)){
								System.out.println("Please provide a valid description !" +CommonConstants.Messages.DESC_LIMIT_MESSAGE);
								return;
							}
						} else {
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}
					continue;
				} else if (value.split("=")[0].trim().equalsIgnoreCase("DESCRIPTION_EN") && value.endsWith("=")) {
					desc_en = "";
					projectAppTranslationTbl_en.setDescription(desc_en);
					continue;
				} else if(value.startsWith("DESCRIPTION_EN") || value.startsWith("description_en")){
					System.out.println("ERROR: Syntax error !");
					return;
				} else if (value.split("=")[0].trim().equalsIgnoreCase("DESCRIPTION_DE") && !value.endsWith("=")) {
					if(value.contains("\"")){
						if (value.contains("=")) {
							desc_de = value.split("=\"", 2)[1];
							if(!(desc_de.length()<240)){
								System.out.println("Please provide a valid description !" +CommonConstants.Messages.DESC_LIMIT_MESSAGE);
								return;
							}
						} else {
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}else{
						if (value.contains("=")) {
							desc_de = value.split("=", 2)[1];
							if(!(desc_de.length()<240)){
								System.out.println("Please provide a valid description !" +CommonConstants.Messages.DESC_LIMIT_MESSAGE);
								return;
							}
						} else {
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}
					continue;
				} else if (value.split("=")[0].trim().equalsIgnoreCase("DESCRIPTION_DE") && value.endsWith("=")) {
					desc_de = "";
					projectAppTranslationTbl_de.setDescription(desc_de);
					continue;
				} else if(value.startsWith("DESCRIPTION_DE") || value.startsWith("description_de")) {
					System.out.println("ERROR: Syntax error !");
					return;
				} else if (value.split("=")[0].trim().equalsIgnoreCase("DESCRIPTION") && !value.endsWith("=")) {
					if(value.contains("\"")){
						if (value.contains("=")) {
							desc = value.split("=\"", 2)[1];
							if(!(desc.length()<240)){
								System.out.println("Please provide a valid description !" +CommonConstants.Messages.DESC_LIMIT_MESSAGE);
								return;
							}
						} else {
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}else{
						if (value.contains("=")) {
							desc = value.split("=", 2)[1];
							if(!(desc.length()<240)){
								System.out.println("Please provide a valid description !" +CommonConstants.Messages.DESC_LIMIT_MESSAGE);
								return;
							}
						} else {
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}
					continue;
				} else if (value.split("=")[0].trim().equalsIgnoreCase("DESCRIPTION") && value.endsWith("=")) {
					desc = "";
					projectAppRequest.setDescription(desc);
					continue;
				} else if(value.startsWith("DESCRIPTION") || value.startsWith("description")) {
					System.out.println("ERROR: Syntax error !");
					return;
				}
				
				if (value.split("=")[0].trim().equalsIgnoreCase("IS_PARENT") && !value.endsWith("=")) {
					is_parent = value.split("=", 2)[1];
					continue;
				} else if (value.split("=")[0].trim().equalsIgnoreCase("IS_PARENT") && value.endsWith("=")) {
					is_parent = "";
					projectAppRequest.setIsParent(is_parent);
					continue;
				} else if(value.startsWith("IS_PARENT") || value.startsWith("is_parent")){
					System.out.println("ERROR: Syntax error !");
					return;
				}
				
				if (value.split("=")[0].trim().equalsIgnoreCase("IS_SINGLETON") && !value.endsWith("=")) {
					is_singleton = value.split("=", 2)[1];
					continue;
				} else if (value.split("=")[0].trim().equalsIgnoreCase("IS_SINGLETON") && value.endsWith("=")) {
					is_singleton = "";
					projectAppRequest.setIsSingleton(is_singleton);
					continue;
				} else if(value.startsWith("IS_SINGLETON") || value.startsWith("is_singleton")){
					System.out.println("ERROR: Syntax error !");
					return;
				}
				
				if (value.split("=")[0].trim().equalsIgnoreCase("POSITION") && !value.endsWith("=")) {
					position = value.split("=", 2)[1];
					position.toUpperCase();
					if(!(position.equals("*ButtonTask*") || position.equals("*IconTask*") || 
							position.equals("*MenuTask*"))){
						System.out.println("\""+position+"\""+" is not a valid position value! valid position values are  : *ButtonTask*"
							+ ", *IconTask*, *MenuTask* ");
						return;
					}
					continue;
				} else if (value.split("=")[0].trim().equalsIgnoreCase("POSITION") && value.endsWith("=")) {
					System.out.println("position value should not be empty! valid position values are  : *ButtonTask*"
							+ ", *IconTask*, *MenuTask* ");
					continue;
				} else if(value.startsWith("POSITION") || value.startsWith("position")){
					System.out.println("ERROR: Syntax error !");
					return;
				}
				
				if (value.split("=")[0].trim().equalsIgnoreCase("APPLICATION") && !value.endsWith("=")) {
					application = value.split("=", 2)[1];
					continue;
				} else if (value.split("=")[0].trim().equalsIgnoreCase("APPLICATION") && value.endsWith("=")) {
					application = "";
					continue;
				} else if(value.startsWith("APPLICATION") || value.startsWith("application")){
					System.out.println("ERROR: Syntax error !");
					return;
				}
				
				if (value.split("=")[0].trim().equalsIgnoreCase("REMARKS_EN") && !value.endsWith("=")) {
					if(value.contains("\"")){
						if (value.contains("=")) {
							remarks_en = value.split("=\"", 2)[1];
							if(!(remarks_en.length()<1500)){
								System.out.println("Please provide a valid remarks !" +CommonConstants.Messages.REMARKS_LIMIT_MESSAGE);
								return;
							}
						} else {
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}else{
						if (value.contains("=")) {
							remarks_en = value.split("=", 2)[1];
							if(!(remarks_en.length()<1500)){
								System.out.println("Please provide a valid remarks !" +CommonConstants.Messages.REMARKS_LIMIT_MESSAGE);
								return;
							}
						} else {
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}
					continue;
				} else if (value.split("=")[0].trim().equalsIgnoreCase("REMARKS_EN") && value.endsWith("=")) {
					remarks_en = "";
					projectAppTranslationTbl_en.setRemarks(remarks_en);
					continue;
				} else if(value.startsWith("REMARKS_EN") || value.startsWith("remarks_en")){
					System.out.println("ERROR: Syntax error !");
					return;
				}
				if (value.split("=")[0].trim().equalsIgnoreCase("REMARKS_DE") && !value.endsWith("=")) {
					if(value.contains("\"")){
						if (value.contains("=")) {
							remarks_de = value.split("=\"", 2)[1];
							if(!(remarks_de.length()<1500)){
								System.out.println("Please provide a valid remarks !" +CommonConstants.Messages.REMARKS_LIMIT_MESSAGE);
								return;
							}
						} else {
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}else{
						if (value.contains("=")) {
							remarks_de = value.split("=", 2)[1];
							if(!(remarks_de.length()<1500)){
								System.out.println("Please provide a valid remarks !" +CommonConstants.Messages.REMARKS_LIMIT_MESSAGE);
								return;
							}
						} else {
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}
					continue;
				} else if (value.split("=")[0].trim().equalsIgnoreCase("REMARKS_DE") && value.endsWith("=")) {
					remarks_de = "";
					projectAppTranslationTbl_de.setRemarks(remarks_de);
					continue;
				} else if(value.startsWith("REMARKS_DE") || value.startsWith("remarks_de")) {
					System.out.println("ERROR: Syntax error !");
					return;
				}
			}
			
			/*if (db_isParent != null) {
				if (db_isParent.equalsIgnoreCase("false")) {
					if (is_parent != null) {
						if (dbPosition != null) {
							if (is_parent.equalsIgnoreCase("true") && !(dbPosition.equals("*ButtonTask*")
									|| dbPosition.equals("*MenuTask*") || dbPosition.equals("*IconTask*"))) {
								System.out.println("You could not assign parent, if Project Application is child");
								return;
							} else if (is_parent.equalsIgnoreCase("true") && position == null && dbPosition.equalsIgnoreCase("*IconTask*")) {
								@SuppressWarnings("resource")
								Scanner scn = new Scanner(System.in);
								boolean check = true;
								while(check){
									System.out.println("Do you want to update Application to Parent ? y/n");
									String conferm = scn.next();
									if(conferm.equalsIgnoreCase("y")){
										System.out.println("position is required");
										return;
									}else if(conferm.equalsIgnoreCase("n")){
										return;
									}else{
										System.out.println("Invalid command : valid commands are 'y' or 'n'. ");
										System.out.println();
									}//End of IF
								}//End of While
							} else if(position!=null && is_parent.equalsIgnoreCase("true")){
								if (!(position.equals("*ButtonTask*") || position.equals("*MenuTask*"))) {
									System.out.print("\"" + position + "\"" + " is not a valid position value! ");
									System.out.println(
											"If Project Application is parent, Valid positions are : *ButtonTask* and *MenuTask*. Eg: position='*MenuTask*'");
									return;
								} else if (!BatchUtil.isEmpty(dbApplication) && BatchUtil.isEmpty(application)) {
									application = dbApplication;
									if (is_singleton != null) {
										System.out.println(
												"You could not assign Singleton, if Project Application is parent");
										return;
									} else if (BatchUtil.isEmpty(position)) {
										System.out.println("position is required");
										return;
									}
								} else if (!BatchUtil.isEmpty(application)) {
									System.out.println(
											"You could not assign Base Application, if Project Application is parent");
									return;
								} else if (db_is_singleton != null && is_singleton == null) {
									is_singleton = db_is_singleton;
									if (BatchUtil.isEmpty(position)) {
										System.out.println("position is required");
										return;
									}
								} else if (is_singleton != null) {
									System.out.println("You could not assign Singleton, if Project Application is parent");
									return;
								} else if (BatchUtil.isEmpty(position)) {
									System.out.println("position is required");
									return;
								}
							} else if (is_parent.equalsIgnoreCase("false")) {
								if (!BatchUtil.isEmpty(dbApplication) && BatchUtil.isEmpty(application)) {
									application = dbApplication;
								} else if (BatchUtil.isEmpty(application)) {
									System.out.println("application is required");
									return;
								} else if (!BatchUtil.isEmpty(dbPosition) && BatchUtil.isEmpty(position)) {
									position = dbPosition;
								} else if (BatchUtil.isEmpty(position)) {
									System.out.println("position is required");
									return;
								}
							} else {
								System.out.println("You could not assign parent, if Project Application is child");
								return;
							}
						}
					}
				} else if (db_isParent.equalsIgnoreCase("true")) {
					if (!BatchUtil.isEmpty(is_parent)) {
						System.out.println("You could not update Parent, if Project Application is Parent");
						return;
					} else if (!BatchUtil.isEmpty(is_singleton)) {
						System.out.println("You could not update Singleton, if Project Application is Parent");
						return;
					} else if (!BatchUtil.isEmpty(application)) {
						System.out.println("You could not update Base Application, if Project Application is Parent");
						return;
					} else if (position == null && dbPosition != null) {
						if (dbPosition.equals("*ButtonTask*") || dbPosition.equals("*MenuTask*")) {
							position = dbPosition;
						} else {
							@SuppressWarnings("resource")
							Scanner scanner = new Scanner(System.in);
							position = scanner.next();
							System.out.print("Enter Position : Valid Positions are *ButtonTask* and *MenuTask* ");
							if (!(position.equals("*ButtonTask*") || position.equals("*MenuTask*"))) {
								System.out.print("\"" + position + "\"" + " is not a valid position value! ");
								System.out.println(
										"If Project Application is parent, Valid positions are : *ButtonTask* and *MenuTask*. Eg: position='*MenuTask*'");
								return;
							}
						}
					} else if (position != null) {
						if (!(position.equals("*ButtonTask*") || position.equals("*MenuTask*"))) {
							System.out.print("\"" + position + "\"" + " is not a valid position value! ");
							System.out.println(
									"If Project Application is parent, Valid positions are : *ButtonTask* and *MenuTask*. Eg: position='*MenuTask*'");
							return;
						}
					}
				}
			}*/
			if (db_isParent != null) {
				if (db_isParent.equalsIgnoreCase("false")) {
					if (is_parent != null) {
						if (dbPosition != null) {
							if (is_parent.equalsIgnoreCase("true") && !(dbPosition.equals("*ButtonTask*")
									|| dbPosition.equals("*MenuTask*") || dbPosition.equals("*IconTask*"))) {
								System.out.println("You could not assign parent, if Project Application is child");
								return;
							} else if (is_parent.equalsIgnoreCase("true") && position == null && dbPosition.equalsIgnoreCase("*IconTask*")) {
								@SuppressWarnings("resource")
								Scanner scn = new Scanner(System.in);
								boolean check = true;
								while(check){
									System.out.println("Do you want to update Application to Parent ? y/n");
									String conferm = scn.next();
									if(conferm.equalsIgnoreCase("y")){
										System.out.println("position is required");
										return;
									}else if(conferm.equalsIgnoreCase("n")){
										return;
									}else{
										System.out.println("Invalid command : valid commands are 'y' or 'n'. ");
										System.out.println();
									}//End of IF
								}//End of While
							} else if(dbPosition!=null && is_parent.equalsIgnoreCase("true")){
								if(position==null){
									position = dbPosition;
								}
								if (!(position.equals("*ButtonTask*") || position.equals("*MenuTask*"))) {
									System.out.print("\"" + position + "\"" + " is not a valid position value! ");
									System.out.println(
											"If Project Application is parent, Valid positions are : *ButtonTask* and *MenuTask*. Eg: position='*MenuTask*'");
									return;
								} else if (!BatchUtil.isEmpty(dbApplication) && BatchUtil.isEmpty(application)) {
									application = dbApplication;
									if (is_singleton != null) {
										System.out.println(
												"Application can not be Singleton, if Project Application is parent");
										return;
									} else if (BatchUtil.isEmpty(position)) {
										System.out.println("position is required");
										return;
									}
								} else if (!BatchUtil.isEmpty(application)) {
									System.out.println(
											"You could not assign Base Application, if Project Application is parent");
									return;
								} else if (db_is_singleton != null && is_singleton == null) {
									is_singleton = db_is_singleton;
									if (BatchUtil.isEmpty(position)) {
										System.out.println("position is required");
										return;
									}
								} else if (is_singleton != null) {
									System.out.println("Application can not be Singleton, if Project Application is parent");
									return;
								} else if (BatchUtil.isEmpty(position)) {
									System.out.println("position is required");
									return;
								}
							} else if (is_parent.equalsIgnoreCase("false")) {
								if (!BatchUtil.isEmpty(dbApplication) && BatchUtil.isEmpty(application)) {
									application = dbApplication;
								} else if (BatchUtil.isEmpty(application)) {
									System.out.println("application is required");
									return;
								} else if (!BatchUtil.isEmpty(dbPosition) && BatchUtil.isEmpty(position)) {
									position = dbPosition;
								} else if (BatchUtil.isEmpty(position)) {
									System.out.println("position is required");
									return;
								}
							} /*else {
								System.out.println("You could not assign parent, if User Application is child");
								return;
							}*/
						}
					}
				} else if (db_isParent.equalsIgnoreCase("true")) {
					if (!BatchUtil.isEmpty(is_parent)) {
						System.out.println("You could not update Parent, if Project Application is Parent");
						return;
					} else if (!BatchUtil.isEmpty(is_singleton)) {
						System.out.println("You could not update Singleton, if Project Application is Parent");
						return;
					} else if (!BatchUtil.isEmpty(application)) {
						System.out.println("You could not update Base Application, if Project Application is Parent");
						return;
					} else if (position == null && dbPosition != null) {
						if (dbPosition.equals("*ButtonTask*") || dbPosition.equals("*MenuTask*")) {
							position = dbPosition;
						} else {
							@SuppressWarnings("resource")
							Scanner scanner = new Scanner(System.in);
							position = scanner.next();
							System.out.print("Enter Position : Valid Positions are *ButtonTask* and *MenuTask* ");
							if (!(position.equals("*ButtonTask*") || position.equals("*MenuTask*"))) {
								System.out.print("\"" + position + "\"" + " is not a valid position value! ");
								System.out.println(
										"If Project Application is parent, Valid positions are : *ButtonTask* and *MenuTask*. Eg: position='*MenuTask*'");
								return;
							}
						}
					} else if (position != null) {
						if (!(position.equals("*ButtonTask*") || position.equals("*MenuTask*"))) {
							System.out.print("\"" + position + "\"" + " is not a valid position value! ");
							System.out.println(
									"If Project Application is parent, Valid positions are : *ButtonTask* and *MenuTask*. Eg: position='*MenuTask*'");
							return;
						}
					}

				}
			}
			if(iconStr!=null){
				IconController iconController = new IconController();
				IkonRequest ikonRequest = new IkonRequest();
				ikonRequest.setIconName(iconStr);
				iconRespWrapper = iconController.getIconByName(ikonRequest);
				if(iconRespWrapper==null) return;
			}
			if (db_isParent!=null && db_isParent.equalsIgnoreCase("false") && !BatchUtil.isEmpty(is_parent)) {
				if(is_parent.equalsIgnoreCase("true") || is_parent.equalsIgnoreCase("false")){
					projectAppRequest.setIsParent(is_parent);
				} else {
					System.out.println("ERROR: Syntax error ! parent should be TRUE or FALSE. Eg: is_parent=true");
					return;
				}
			}
			if (db_isParent!=null && db_isParent.equalsIgnoreCase("false") && !BatchUtil.isEmpty(is_singleton)) {
				if(is_singleton.equalsIgnoreCase("true") || is_singleton.equalsIgnoreCase("false")){
					projectAppRequest.setIsSingleton(is_singleton);
				} else{
					System.out.println("ERROR: Syntax error ! singleton should be TRUE or FALSE. Eg: is_singleton=true");
					return;
				}
			}
			if (!BatchUtil.isEmpty(position)) {
				projectAppRequest.setPosition(position);
			}
			String baseAppId = null;
			if (db_isParent!=null && !db_isParent.equalsIgnoreCase("true") && !BatchUtil.isEmpty(application)) {
				BaseAppController baseAppController = new BaseAppController();
				baseAppId = baseAppController.getBaseAppByName(application);
				if (baseAppId == null) return;
				projectAppRequest.setBaseAppId(baseAppId);
			}
			
			if (!BatchUtil.isEmpty(projectAppName)) {
				projectAppRequest.setName(projectAppName);
			}
			if (!BatchUtil.isEmpty(desc)) {
				projectAppRequest.setDescription(desc);
			}
			if (!BatchUtil.isEmpty(statusStr)) {
				projectAppRequest.setStatus(statusStr);
			}
			if (!BatchUtil.isEmpty(iconStr) && iconRespWrapper!=null) {
				projectAppRequest.setIconId(iconRespWrapper.getIconId());
			}
			if (!BatchUtil.isEmpty(name_en)) {
				projectAppTranslationTbl_en.setName(desc_en);
			}
			if (!BatchUtil.isEmpty(name_de)) {
				projectAppTranslationTbl_de.setName(name_de);
			}
			if (!BatchUtil.isEmpty(desc_en)) {
				projectAppTranslationTbl_en.setDescription(desc_en);
			}
			if (!BatchUtil.isEmpty(remarks_en)) {
				projectAppTranslationTbl_en.setRemarks(remarks_en);
			}
			if (!BatchUtil.isEmpty(desc_de)) {
				projectAppTranslationTbl_de.setDescription(desc_de);
			}
			if (!BatchUtil.isEmpty(remarks_de)) {
				projectAppTranslationTbl_de.setRemarks(remarks_de);
			}
			List<ProjectApplicationTransulation> projectAppTranslationTblList = new ArrayList<ProjectApplicationTransulation>();
			projectAppTranslationTblList.add(projectAppTranslationTbl_en);
			projectAppTranslationTblList.add(projectAppTranslationTbl_de);
			projectAppRequest.setProjectApplicationTransulations(projectAppTranslationTblList);

			Boolean updateProjectApp = projectAppController.updateProjectApp(projectAppRequest);
				if (updateProjectApp == null || updateProjectApp == Boolean.FALSE) {
					return;
				} else {
					System.out.println("Project Application updated Successfully");

				}
		}
	}

	/**
	 * Method for Update user app.
	 *
	 * @param cmd {@link CommandLine}
	 */
	private void updateUserApp(CommandLine cmd, ObjectType updateObjType) {
		String userAppName = null;
		userAppName = getName(cmd, updateObjType);
		if(userAppName==null) return;
		com.magna.batch.restclient.obj.userapp.UserAppController userAppController = new com.magna.batch.restclient.obj.userapp.UserAppController();
		UserApplicationRequest userAppRequest = new UserApplicationRequest();
		UserApplicationCustomResponse userAppByName = userAppController.getUserAppByName(userAppName);
		if(userAppByName==null) return;
		String statusStr = null, iconStr = null, desc = null, is_parent = null, db_isParent = userAppByName.getIsParent(), is_singleton = null, db_is_singleton = userAppByName.getIsSingleTon(),
				position = null, dbPosition = userAppByName.getPosition(), application = null, dbApplication = userAppByName.getBaseAppName(), name_en = null, name_de = null, desc_en = null, desc_de = null, remarks_en = null, remarks_de = null,
				strVal = "";
		IconRespWrapper iconRespWrapper = null;
		List<UserApplicationTranslation> userAppTranslation = new ArrayList<>();
		UserApplicationTranslation userAppTranslationTbl_en = new UserApplicationTranslation();
		userAppTranslationTbl_en.setLanguageCode(com.magna.batch.utils.LANG_ENUM.ENGLISH.getLangCode());
		userAppTranslation.add(userAppTranslationTbl_en);
		UserApplicationTranslation userAppTranslationTbl_de = new UserApplicationTranslation();
		userAppTranslationTbl_de.setLanguageCode(com.magna.batch.utils.LANG_ENUM.GERMAN.getLangCode());
		userAppTranslation.add(userAppTranslationTbl_de);
		userAppRequest.setUserApplicationTranslations(userAppTranslation);
		String[] optionValues = cmd.getOptionValues("update");
		if (optionValues.length > 0) {
			strVal = BatchUtil.getInstance().getStringValue(optionValues, cmd, updateObjType);
			if(strVal==null) return;
			String[] splittedStrVal = strVal.split("\"");
			String value;
			for (int i = 0; i < splittedStrVal.length; i++) {
				value = splittedStrVal[i];
				value = value.trim();
				
				if (value.split("=")[0].trim().equalsIgnoreCase("NAME_EN") && !value.endsWith("=")) {
					name_en = value.split("=", 2)[1];
					if(!name_en.matches(CommonConstants.RegularExpressions.ALLOWED_APP_NAME_REGEX)){
						System.out.println("Please provide a valid name !" +CommonConstants.Messages.NAME_MESSAGE);
						return;
					} else if(!(name_en.length()<30)){
						System.out.println("Please provide a valid name !" +CommonConstants.Messages.NAME_LIMIT_MESSAGE);
						return;
					}
					continue;
				} else if (value.split("=")[0].trim().equalsIgnoreCase("NAME_EN") && value.endsWith("=")) {
					name_en = "";
					userAppTranslationTbl_en.setName(name_en);
					continue;
				} else if(value.startsWith("NAME_EN") || value.startsWith("name_en")){
					System.out.println("ERROR: Syntax error !");
					return;
				} else if (value.split("=")[0].trim().equalsIgnoreCase("NAME_DE") && !value.endsWith("=")) {
					name_de = value.split("=", 2)[1];
					if(!name_de.matches(CommonConstants.RegularExpressions.ALLOWED_APP_NAME_REGEX)){
						System.out.println("Please provide a valid name !" +CommonConstants.Messages.NAME_MESSAGE);
						return;
					} else if(!(name_de.length()<30)){
						System.out.println("Please provide a valid name !" +CommonConstants.Messages.NAME_LIMIT_MESSAGE);
						return;
					}
					continue;
				} else if (value.split("=")[0].trim().equalsIgnoreCase("NAME_DE") && value.endsWith("=")) {
					name_de = "";
					userAppTranslationTbl_de.setName(name_de);
					continue;
				} else if(value.startsWith("NAME_DE") || value.startsWith("name_de")){
					System.out.println("ERROR: Syntax error !");
					return;
				} else if(!name(value)) return;
				
				if (value.split("=")[0].trim().equalsIgnoreCase("STATUS") && !value.endsWith("=")) {
					if (value.contains("=")) {
						statusStr = value.split("=", 2)[1];
						statusStr = statusStr.toUpperCase();
						if(!(statusStr.equalsIgnoreCase("active") || statusStr.equalsIgnoreCase("inactive"))){
							System.out.println("ERROR: Syntax error ! Status should be ACTIVE or INACTIVE");
							return;
						}
					} else {
						System.out.println("ERROR: Syntax error !");
						return;
					}
					continue;
				} else if (value.split("=")[0].trim().equalsIgnoreCase("STATUS") && value.endsWith("=")) {
					System.out.println("ERROR: Syntax error ! Status should be ACTIVE or INACTIVE");
					return;
				} else if(value.startsWith("STATUS") || value.startsWith("status")) {
					System.out.println("ERROR: Syntax error !");
					return;
				}
				
				if (value.split("=")[0].trim().equalsIgnoreCase("ICON_NAME") && !value.endsWith("=")) {
					if (value.contains("=")) {
						iconStr = value.split("=", 2)[1];
					} else {
						System.out.println("ERROR: Syntax error !");
						return;
					}
					continue;
				} else if (value.split("=")[0].trim().equalsIgnoreCase("ICON_NAME") && value.endsWith("=")) {
					iconStr="";
					continue;
				} else if(value.startsWith("ICON_NAME") || value.startsWith("icon_name")){
					System.out.println("ERROR: Syntax error !");
					return;
				}
				
				if (value.split("=")[0].trim().equalsIgnoreCase("DESCRIPTION_EN") && !value.endsWith("=")) {
					if(value.contains("\"")){
						if (value.contains("=")) {
							desc_en = value.split("=\"", 2)[1];
							if(!(desc_en.length()<240)){
								System.out.println("Please provide a valid description !" +CommonConstants.Messages.DESC_LIMIT_MESSAGE);
								return;
							}
						} else {
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}else{
						if (value.contains("=")) {
							desc_en = value.split("=", 2)[1];
							if(!(desc_en.length()<240)){
								System.out.println("Please provide a valid description !" +CommonConstants.Messages.DESC_LIMIT_MESSAGE);
								return;
							}
						} else {
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}
					continue;
				} else if (value.split("=")[0].trim().equalsIgnoreCase("DESCRIPTION_EN") && value.endsWith("=")) {
					desc_en = "";
					userAppTranslationTbl_en.setDescription(desc_en);
					continue;
				} else if(value.startsWith("DESCRIPTION_EN") || value.startsWith("description_en")){
					System.out.println("ERROR: Syntax error !");
					return;
				} else if (value.split("=")[0].trim().equalsIgnoreCase("DESCRIPTION_DE") && !value.endsWith("=")) {
					if(value.contains("\"")){
						if (value.contains("=")) {
							desc_de = value.split("=\"", 2)[1];
							if(!(desc_de.length()<240)){
								System.out.println("Please provide a valid description !" +CommonConstants.Messages.DESC_LIMIT_MESSAGE);
								return;
							}
						} else {
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}else{
						if (value.contains("=")) {
							desc_de = value.split("=", 2)[1];
							if(!(desc_de.length()<240)){
								System.out.println("Please provide a valid description !" +CommonConstants.Messages.DESC_LIMIT_MESSAGE);
								return;
							}
						} else {
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}
					continue;
				} else if (value.split("=")[0].trim().equalsIgnoreCase("DESCRIPTION_DE") && value.endsWith("=")) {
					desc_de = "";
					userAppTranslationTbl_de.setDescription(desc_de);
					continue;
				} else if(value.startsWith("DESCRIPTION_DE") || value.startsWith("description_de")) {
					System.out.println("ERROR: Syntax error !");
					return;
				} else if (value.split("=")[0].trim().equalsIgnoreCase("DESCRIPTION") && !value.endsWith("=")) {
					if(value.contains("\"")){
						if (value.contains("=")) {
							desc = value.split("=\"", 2)[1];
							if(!(desc.length()<240)){
								System.out.println("Please provide a valid description !" +CommonConstants.Messages.DESC_LIMIT_MESSAGE);
								return;
							}
						} else {
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}else{
						if (value.contains("=")) {
							desc = value.split("=", 2)[1];
							if(!(desc.length()<240)){
								System.out.println("Please provide a valid description !" +CommonConstants.Messages.DESC_LIMIT_MESSAGE);
								return;
							}
						} else {
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}
					continue;
				} else if (value.split("=")[0].trim().equalsIgnoreCase("DESCRIPTION") && value.endsWith("=")) {
					desc = "";
					userAppRequest.setDescription(desc);
					continue;
				} else if(value.startsWith("DESCRIPTION") || value.startsWith("description")) {
					System.out.println("ERROR: Syntax error !");
					return;
				}
				
				
				if (value.split("=")[0].trim().equalsIgnoreCase("IS_PARENT") && !value.endsWith("=")) {
					is_parent = value.split("=", 2)[1];
					is_parent = is_parent.toLowerCase();
					continue;
				} else if (value.split("=")[0].trim().equalsIgnoreCase("IS_PARENT") && value.endsWith("=")) {
					System.out.println("ERROR: Syntax error ! parent should be TRUE or FALSE. Eg: is_parent=true");
					return;
				} else if(value.startsWith("IS_PARENT") || value.startsWith("is_parent")){
					System.out.println("ERROR: Syntax error !");
					return;
				}
				
				if (value.split("=")[0].trim().equalsIgnoreCase("APPLICATION") && !value.endsWith("=")) {
					application = value.split("=", 2)[1];
					continue;
				} else if (value.split("=")[0].trim().equalsIgnoreCase("APPLICATION") && value.endsWith("=")) {
					application = "";
					continue;
				} else if(value.startsWith("APPLICATION") || value.startsWith("application")){
					System.out.println("ERROR: Syntax error !");
					return;
				}
				
				if (value.split("=")[0].trim().equalsIgnoreCase("IS_SINGLETON") && !value.endsWith("=")) {
					is_singleton = value.split("=", 2)[1];
					is_singleton = is_singleton.toLowerCase();
					continue;
				} else if (value.split("=")[0].trim().equalsIgnoreCase("IS_SINGLETON") && value.endsWith("=")) {
					System.out.println("ERROR: Syntax error ! singleton should be TRUE or FALSE. Eg: is_singleton=true");
					return;
				} else if(value.startsWith("IS_SINGLETON") || value.startsWith("is_singleton")){
					System.out.println("ERROR: Syntax error !");
					return;
				}
				
				if (value.split("=")[0].trim().equalsIgnoreCase("POSITION") && !value.endsWith("=")) {
					position = value.split("=", 2)[1];
					position.toUpperCase();
					if(!(position.equals("*ButtonTask*") || position.equals("*IconTask*") || 
							position.equals("*MenuTask*"))){
						System.out.println("\""+position+"\""+" is not a valid position value! valid position values are  : *ButtonTask*"
							+ ", *IconTask*, *MenuTask* ");
						return;
					}
					continue;
				} else if (value.split("=")[0].trim().equalsIgnoreCase("POSITION") && value.endsWith("=")) {
					System.out.println("position value should not be empty! valid position values are  : *ButtonTask*"
							+ ", *IconTask*, *MenuTask* ");
					continue;
				} else if(value.startsWith("POSITION") || value.startsWith("position")){
					System.out.println("ERROR: Syntax error !");
					return;
				}
				
				if (value.split("=")[0].trim().equalsIgnoreCase("REMARKS_EN") && !value.endsWith("=")) {
					if(value.contains("\"")){
						if (value.contains("=")) {
							remarks_en = value.split("=\"", 2)[1];
							if(!(remarks_en.length()<1500)){
								System.out.println("Please provide a valid remarks !" +CommonConstants.Messages.REMARKS_LIMIT_MESSAGE);
								return;
							}
						} else {
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}else{
						if (value.contains("=")) {
							remarks_en = value.split("=", 2)[1];
							if(!(remarks_en.length()<1500)){
								System.out.println("Please provide a valid remarks !" +CommonConstants.Messages.REMARKS_LIMIT_MESSAGE);
								return;
							}
						} else {
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}
					continue;
				} else if (value.split("=")[0].trim().equalsIgnoreCase("REMARKS_EN") && value.endsWith("=")) {
					remarks_en = "";
					userAppTranslationTbl_en.setRemarks(remarks_en);
					continue;
				} else if(value.startsWith("REMARKS_EN") || value.startsWith("remarks_en")){
					System.out.println("ERROR: Syntax error !");
					return;
				}
				if (value.split("=")[0].trim().equalsIgnoreCase("REMARKS_DE") && !value.endsWith("=")) {
					if(value.contains("\"")){
						if (value.contains("=")) {
							remarks_de = value.split("=\"", 2)[1];
							if(!(remarks_de.length()<1500)){
								System.out.println("Please provide a valid remarks !" +CommonConstants.Messages.REMARKS_LIMIT_MESSAGE);
								return;
							}
						} else {
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}else{
						if (value.contains("=")) {
							remarks_de = value.split("=", 2)[1];
							if(!(remarks_de.length()<1500)){
								System.out.println("Please provide a valid remarks !" +CommonConstants.Messages.REMARKS_LIMIT_MESSAGE);
								return;
							}
						} else {
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}
					continue;
				} else if (value.split("=")[0].trim().equalsIgnoreCase("REMARKS_DE") && value.endsWith("=")) {
					remarks_de = "";
					userAppTranslationTbl_de.setRemarks(remarks_de);
					continue;
				} else if(value.startsWith("REMARKS_DE") || value.startsWith("remarks_de")) {
					System.out.println("ERROR: Syntax error !");
					return;
				}
			}
			
			if (db_isParent != null) {
				if (db_isParent.equalsIgnoreCase("false")) {
					if (is_parent != null) {
						if (dbPosition != null) {
							if (is_parent.equalsIgnoreCase("true") && !(dbPosition.equals("*ButtonTask*")
									|| dbPosition.equals("*MenuTask*") || dbPosition.equals("*IconTask*"))) {
								System.out.println("You could not assign parent, if User Application is child");
								return;
							} else if (is_parent.equalsIgnoreCase("true") && position == null && dbPosition.equalsIgnoreCase("*IconTask*")) {
								@SuppressWarnings("resource")
								Scanner scn = new Scanner(System.in);
								boolean check = true;
								while(check){
									System.out.println("Do you want to update Application to Parent ? y/n");
									String conferm = scn.next();
									if(conferm.equalsIgnoreCase("y")){
										System.out.println("position is required");
										return;
									}else if(conferm.equalsIgnoreCase("n")){
										return;
									}else{
										System.out.println("Invalid command : valid commands are 'y' or 'n'. ");
										System.out.println();
									}//End of IF
								}//End of While
							} else if(dbPosition!=null && is_parent.equalsIgnoreCase("true")){
								if(position==null){
									position = dbPosition;
								}
								if (!(position.equals("*ButtonTask*") || position.equals("*MenuTask*"))) {
									System.out.print("\"" + position + "\"" + " is not a valid position value! ");
									System.out.println(
											"If User Application is parent, Valid positions are : *ButtonTask* and *MenuTask*. Eg: position='*MenuTask*'");
									return;
								} else if (!BatchUtil.isEmpty(dbApplication) && BatchUtil.isEmpty(application)) {
									application = dbApplication;
									if (is_singleton != null) {
										System.out.println(
												"Application can not be Singleton, if User Application is parent");
										return;
									} else if (BatchUtil.isEmpty(position)) {
										System.out.println("position is required");
										return;
									}
								} else if (!BatchUtil.isEmpty(application)) {
									System.out.println(
											"You could not assign Base Application, if User Application is parent");
									return;
								} else if (db_is_singleton != null && is_singleton == null) {
									is_singleton = db_is_singleton;
									if (BatchUtil.isEmpty(position)) {
										System.out.println("position is required");
										return;
									}
								} else if (is_singleton != null) {
									System.out.println("Application can not be Singleton, if User Application is parent");
									return;
								} else if (BatchUtil.isEmpty(position)) {
									System.out.println("position is required");
									return;
								}
							} else if (is_parent.equalsIgnoreCase("false")) {
								if (!BatchUtil.isEmpty(dbApplication) && BatchUtil.isEmpty(application)) {
									application = dbApplication;
								} else if (BatchUtil.isEmpty(application)) {
									System.out.println("application is required");
									return;
								} else if (!BatchUtil.isEmpty(dbPosition) && BatchUtil.isEmpty(position)) {
									position = dbPosition;
								} else if (BatchUtil.isEmpty(position)) {
									System.out.println("position is required");
									return;
								}
							} /*else {
								System.out.println("You could not assign parent, if User Application is child");
								return;
							}*/
						}
					}
				} else if (db_isParent.equalsIgnoreCase("true")) {
					if (!BatchUtil.isEmpty(is_parent)) {
						System.out.println("You could not update Parent, if User Application is Parent");
						return;
					} else if (!BatchUtil.isEmpty(is_singleton)) {
						System.out.println("You could not update Singleton, if User Application is Parent");
						return;
					} else if (!BatchUtil.isEmpty(application)) {
						System.out.println("You could not update Base Application, if User Application is Parent");
						return;
					} else if (position == null && dbPosition != null) {
						if (dbPosition.equals("*ButtonTask*") || dbPosition.equals("*MenuTask*")) {
							position = dbPosition;
						} else {
							@SuppressWarnings("resource")
							Scanner scanner = new Scanner(System.in);
							position = scanner.next();
							System.out.print("Enter Position : Valid Positions are *ButtonTask* and *MenuTask* ");
							if (!(position.equals("*ButtonTask*") || position.equals("*MenuTask*"))) {
								System.out.print("\"" + position + "\"" + " is not a valid position value! ");
								System.out.println(
										"If User Application is parent, Valid positions are : *ButtonTask* and *MenuTask*. Eg: position='*MenuTask*'");
								return;
							}
						}
					} else if (position != null) {
						if (!(position.equals("*ButtonTask*") || position.equals("*MenuTask*"))) {
							System.out.print("\"" + position + "\"" + " is not a valid position value! ");
							System.out.println(
									"If User Application is parent, Valid positions are : *ButtonTask* and *MenuTask*. Eg: position='*MenuTask*'");
							return;
						}
					}

				}
			}
			if(iconStr!=null){
				IconController iconController = new IconController();
				IkonRequest ikonRequest = new IkonRequest();
				ikonRequest.setIconName(iconStr);
				iconRespWrapper = iconController.getIconByName(ikonRequest);
				if(iconRespWrapper==null) return;
			}
			
			if (db_isParent!=null && db_isParent.equalsIgnoreCase("false") && !BatchUtil.isEmpty(is_parent)) {
				if(is_parent.equalsIgnoreCase("true") || is_parent.equalsIgnoreCase("false")){
					userAppRequest.setIsParent(is_parent);
				} else{
					System.out.println("ERROR: Syntax error ! parent should be TRUE or FALSE. Eg: is_parent=true");
					return;
				}
			}
			if (db_isParent!=null && db_isParent.equalsIgnoreCase("false") && !BatchUtil.isEmpty(is_singleton)) {
				if(is_singleton.equalsIgnoreCase("true") || is_singleton.equalsIgnoreCase("false")){
					userAppRequest.setIsSingleTon(is_singleton);
				} else{
					System.out.println("ERROR: Syntax error ! singleton should be TRUE or FALSE. Eg: is_singleton=true");
					return;
				}
			}
			if (!BatchUtil.isEmpty(position)) {
				userAppRequest.setPosition(position);
			}
			String baseAppId = null;
			if (db_isParent!=null && !db_isParent.equalsIgnoreCase("true") && !BatchUtil.isEmpty(application)) {
				BaseAppController baseAppController = new BaseAppController();
				baseAppId = baseAppController.getBaseAppByName(application);
				if (baseAppId == null) return;
				userAppRequest.setBaseAppId(baseAppId);
			}
			
			if (!BatchUtil.isEmpty(userAppName)) {
				userAppRequest.setName(userAppName);
			}
			if (!BatchUtil.isEmpty(desc)) {
				userAppRequest.setDescription(desc);
			}
			if (!BatchUtil.isEmpty(statusStr)) {
				if(statusStr.equalsIgnoreCase("active") || statusStr.equalsIgnoreCase("inactive")){
					userAppRequest.setStatus(statusStr);
				} else{
					System.out.println("ERROR: Syntax error ! Status should be ACTIVE or INACTIVE. Eg: status='ACTIVE'");
					return;
				}
			}
			if (!BatchUtil.isEmpty(iconStr) && iconRespWrapper!=null) {
				userAppRequest.setIconId(iconRespWrapper.getIconId());
			}
			if (!BatchUtil.isEmpty(name_en)) {
				userAppTranslationTbl_en.setName(desc_en);
			}
			if (!BatchUtil.isEmpty(name_de)) {
				userAppTranslationTbl_en.setName(name_de);
			}
			if (!BatchUtil.isEmpty(desc_en)) {
				userAppTranslationTbl_en.setDescription(desc_en);
			}
			if (!BatchUtil.isEmpty(remarks_en)) {
				userAppTranslationTbl_en.setRemarks(remarks_en);
			}
			if (!BatchUtil.isEmpty(desc_de)) {
				userAppTranslationTbl_de.setDescription(desc_de);
			}
			if (!BatchUtil.isEmpty(remarks_de)) {
				userAppTranslationTbl_de.setRemarks(remarks_de);
			}
			List<UserApplicationTranslation> userAppTranslationTblList = new ArrayList<UserApplicationTranslation>();
			userAppTranslationTblList.add(userAppTranslationTbl_en);
			userAppTranslationTblList.add(userAppTranslationTbl_de);
			userAppRequest.setUserApplicationTranslations(userAppTranslationTblList);

			Boolean updateUserApp = userAppController.updateUserApp(userAppRequest);
				if (updateUserApp == null || updateUserApp == Boolean.FALSE) {
					return;
				} else {
					System.out.println("User Application updated Successfully");

				}
		}
	}

	/**
	 * Method for Update base app.
	 *
	 * @param cmd {@link CommandLine}
	 */
	private void updateBaseApp(CommandLine cmd, ObjectType updateObjType) {
		com.magna.batch.restclient.obj.baseapp.BaseAppController baseAppController = new com.magna.batch.restclient.obj.baseapp.BaseAppController();
		BaseApplicationRequest baseAppRequest = new BaseApplicationRequest();
		String baseAppName = null, statusStr = null, iconStr = null, platforms = null, program = null, desc_en = null, desc_de = null, remarks_en = null, remarks_de = null,
				strVal = "";
		IconRespWrapper iconRespWrapper = null;
		List<BaseApplicationTransulation> baseAppTranslation = new ArrayList<>();
		BaseApplicationTransulation baseAppTranslationTbl_en = new BaseApplicationTransulation();
		baseAppTranslationTbl_en.setLanguageCode(com.magna.batch.utils.LANG_ENUM.ENGLISH.getLangCode());
		baseAppTranslation.add(baseAppTranslationTbl_en);
		BaseApplicationTransulation baseAppTranslationTbl_de = new BaseApplicationTransulation();
		baseAppTranslationTbl_de.setLanguageCode(com.magna.batch.utils.LANG_ENUM.GERMAN.getLangCode());
		baseAppTranslation.add(baseAppTranslationTbl_de);
		baseAppRequest.setBaseApplicationTransulations(baseAppTranslation);
		String[] optionValues = cmd.getOptionValues("update");
		if (optionValues.length > 0) {
			strVal = BatchUtil.getInstance().getStringValue(optionValues, cmd, updateObjType);
			if(strVal==null) return;
			String[] splittedStrVal = strVal.split("\"");
			String value;
			for (int i = 0; i < splittedStrVal.length; i++) {
				value = splittedStrVal[i];
				value = value.trim();
				if(!name(value)) return;
				if (value.split("=")[0].trim().equalsIgnoreCase("STATUS") && !value.endsWith("=")) {
					if (value.contains("=")) {
						statusStr = value.split("=", 2)[1];
						statusStr = statusStr.toUpperCase();
						if(!(statusStr.equalsIgnoreCase("active") || statusStr.equalsIgnoreCase("inactive"))){
							System.out.println("ERROR: Syntax error ! Status should be ACTIVE or INACTIVE");
							return;
						}
					} else {
						System.out.println("ERROR: Syntax error !");
						return;
					}
					continue;
				} else if (value.split("=")[0].trim().equalsIgnoreCase("STATUS") && value.endsWith("=")) {
					System.out.println("ERROR: Syntax error ! Status should be ACTIVE or INACTIVE");
					return;
				} else if(value.startsWith("STATUS") || value.startsWith("status")) {
					System.out.println("ERROR: Syntax error !");
					return;
				}
				if (value.split("=")[0].trim().equalsIgnoreCase("ICON_NAME") && !value.endsWith("=")) {
					if (value.contains("=")) {
						iconStr = value.split("=", 2)[1];
					} else {
						System.out.println("ERROR: Syntax error !");
						return;
					}
					continue;
				} else if (value.split("=")[0].trim().equalsIgnoreCase("ICON_NAME") && value.endsWith("=")) {
					iconStr="";
					continue;
				} else if(value.startsWith("ICON_NAME") || value.startsWith("icon_name")){
					System.out.println("ERROR: Syntax error !");
					return;
				}
				
				if (value.split("=")[0].trim().equalsIgnoreCase("PLATFORMS") && !value.endsWith("=")) {
					platforms = value.split("=", 2)[1];
					platforms = platforms.trim().toUpperCase();
					if(platforms.contains(";")){
						String[] split = platforms.split(";");
						for (int index = 0; index < split.length; index++) {
							if(!("WINDOWS64".equals(split[index]) || "WINDOWS32".equals(split[index]) || "LINUX64".equals(split[index]))){
								System.out.println("ERROR: Syntax error ! Platforms should be WINDOWS64, WINDOWS32, LINUX64 - Eg: WINDOWS64;WINDOWS32;LINUX64 ");
								return;
							}
							String string = split[index];
							for (int index2 = 0; index2 < split.length; index2++) {
								if(index!=index2){
									String string2 = split[index2];
									if(string.equalsIgnoreCase(string2)){
										System.out.println("ERROR: Syntax error ! Platform name "+ "\""+"-"+string+"\""+" should't be multiple times. ");
										return;
									}
								}
							}
						}
					} else if(!("WINDOWS64".equals(platforms) || "WINDOWS32".equals(platforms) || "LINUX64".equals(platforms))){
						System.out.println("ERROR: Syntax error ! Platforms should be WINDOWS64, WINDOWS32, LINUX64 - Eg: WINDOWS64;WINDOWS32;LINUX64 ");
						return;
					}
					continue;
				} else if (value.split("=")[0].trim().equalsIgnoreCase("PLATFORMS") && value.endsWith("=")) {
					platforms = "";
					baseAppRequest.setPlatforms(platforms);
					continue;
				} else if(value.startsWith("PLATFORMS") || value.startsWith("platforms")){
					System.out.println("ERROR: Syntax error !");
					return;
				}
				
				if (value.split("=")[0].trim().equalsIgnoreCase("PROGRAM") && !value.endsWith("=")) {
					program = value.split("=", 2)[1];
					continue;
				} else if (value.split("=")[0].trim().equalsIgnoreCase("PROGRAM") && value.endsWith("=")) {
					program = "";
					baseAppRequest.setProgram(program);
					continue;
				} else if(value.startsWith("PROGRAM") || value.startsWith("program")){
					System.out.println("ERROR: Syntax error !");
					return;
				}
				
				if (value.split("=")[0].trim().equalsIgnoreCase("DESCRIPTION_EN") && !value.endsWith("=")) {
					if(value.contains("\"")){
						if (value.contains("=")) {
							desc_en = value.split("=\"", 2)[1];
							if(!(desc_en.length()<240)){
								System.out.println("Please provide a valid description !" +CommonConstants.Messages.DESC_LIMIT_MESSAGE);
								return;
							}
						} else {
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}else{
						if (value.contains("=")) {
							desc_en = value.split("=", 2)[1];
							if(!(desc_en.length()<240)){
								System.out.println("Please provide a valid description !" +CommonConstants.Messages.DESC_LIMIT_MESSAGE);
								return;
							}
						} else {
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}
					continue;
				} else if (value.split("=")[0].trim().equalsIgnoreCase("DESCRIPTION_EN") && value.endsWith("=")) {
					desc_en = "";
					baseAppTranslationTbl_en.setDescription(desc_en);
					continue;
				} else if(value.startsWith("DESCRIPTION_EN") || value.startsWith("description_en")){
					System.out.println("ERROR: Syntax error !");
					return;
				}
				if (value.split("=")[0].trim().equalsIgnoreCase("DESCRIPTION_DE") && !value.endsWith("=")) {
					if(value.contains("\"")){
						if (value.contains("=")) {
							desc_de = value.split("=\"", 2)[1];
							if(!(desc_de.length()<240)){
								System.out.println("Please provide a valid description !" +CommonConstants.Messages.DESC_LIMIT_MESSAGE);
								return;
							}
						} else {
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}else{
						if (value.contains("=")) {
							desc_de = value.split("=", 2)[1];
							if(!(desc_de.length()<240)){
								System.out.println("Please provide a valid description !" +CommonConstants.Messages.DESC_LIMIT_MESSAGE);
								return;
							}
						} else {
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}
					continue;
				} else if (value.split("=")[0].trim().equalsIgnoreCase("DESCRIPTION_DE") && value.endsWith("=")) {
					desc_de = "";
					baseAppTranslationTbl_de.setDescription(desc_de);
					continue;
				} else if(value.startsWith("DESCRIPTION_DE") || value.startsWith("description_de")) {
					System.out.println("ERROR: Syntax error !");
					return;
				}
				if (value.split("=")[0].trim().equalsIgnoreCase("REMARKS_EN") && !value.endsWith("=")) {
					if(value.contains("\"")){
						if (value.contains("=")) {
							remarks_en = value.split("=\"", 2)[1];
							if(!(remarks_en.length()<1500)){
								System.out.println("Please provide a valid remarks !" +CommonConstants.Messages.REMARKS_LIMIT_MESSAGE);
								return;
							}
						} else {
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}else{
						if (value.contains("=")) {
							remarks_en = value.split("=", 2)[1];
							if(!(remarks_en.length()<1500)){
								System.out.println("Please provide a valid remarks !" +CommonConstants.Messages.REMARKS_LIMIT_MESSAGE);
								return;
							}
						} else {
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}
					continue;
				} else if (value.split("=")[0].trim().equalsIgnoreCase("REMARKS_EN") && value.endsWith("=")) {
					remarks_en = "";
					baseAppTranslationTbl_en.setRemarks(remarks_en);
					continue;
				} else if(value.startsWith("REMARKS_EN") || value.startsWith("remarks_en")){
					System.out.println("ERROR: Syntax error !");
					return;
				}
				if (value.split("=")[0].trim().equalsIgnoreCase("REMARKS_DE") && !value.endsWith("=")) {
					if(value.contains("\"")){
						if (value.contains("=")) {
							remarks_de = value.split("=\"", 2)[1];
							if(!(remarks_de.length()<1500)){
								System.out.println("Please provide a valid remarks !" +CommonConstants.Messages.REMARKS_LIMIT_MESSAGE);
								return;
							}
						} else {
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}else{
						if (value.contains("=")) {
							remarks_de = value.split("=", 2)[1];
							if(!(remarks_de.length()<1500)){
								System.out.println("Please provide a valid remarks !" +CommonConstants.Messages.REMARKS_LIMIT_MESSAGE);
								return;
							}
						} else {
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}
					continue;
				} else if (value.split("=")[0].trim().equalsIgnoreCase("REMARKS_DE") && value.endsWith("=")) {
					remarks_de = "";
					baseAppTranslationTbl_de.setRemarks(remarks_de);
					continue;
				} else if(value.startsWith("REMARKS_DE") || value.startsWith("remarks_de")) {
					System.out.println("ERROR: Syntax error !");
					return;
				}
			}
			
			baseAppName = getName(cmd, updateObjType);
			if(baseAppName==null) return;
			if(iconStr!=null){
				IconController iconController = new IconController();
				IkonRequest ikonRequest = new IkonRequest();
				ikonRequest.setIconName(iconStr);
				iconRespWrapper = iconController.getIconByName(ikonRequest);
				if(iconRespWrapper==null) return;
			}
			if (!BatchUtil.isEmpty(baseAppName)) {
				baseAppRequest.setName(baseAppName);
			}
			if (!BatchUtil.isEmpty(statusStr)) {
				baseAppRequest.setStatus(statusStr);
			}
			if (!BatchUtil.isEmpty(iconStr) && iconRespWrapper!=null) {
				baseAppRequest.setIconId(iconRespWrapper.getIconId());
			}
			if (!BatchUtil.isEmpty(platforms)) {
				baseAppRequest.setPlatforms(platforms);
			}
			if (!BatchUtil.isEmpty(program)) {
				baseAppRequest.setProgram(program);
			}
			if (!BatchUtil.isEmpty(desc_en)) {
				baseAppTranslationTbl_en.setDescription(desc_en);
			}
			if (!BatchUtil.isEmpty(remarks_en)) {
				baseAppTranslationTbl_en.setRemarks(remarks_en);
			}
			if (!BatchUtil.isEmpty(desc_de)) {
				baseAppTranslationTbl_de.setDescription(desc_de);
			}
			if (!BatchUtil.isEmpty(remarks_de)) {
				baseAppTranslationTbl_de.setRemarks(remarks_de);
			}
			List<BaseApplicationTransulation> baseAppTranslationTblList = new ArrayList<BaseApplicationTransulation>();
			baseAppTranslationTblList.add(baseAppTranslationTbl_en);
			baseAppTranslationTblList.add(baseAppTranslationTbl_de);
			baseAppRequest.setBaseApplicationTransulations(baseAppTranslationTblList);

			Boolean updateBaseApp = baseAppController.updateBaseApp(baseAppRequest);
				if (updateBaseApp == null || updateBaseApp == Boolean.FALSE) {
					return;
				} else {
					System.out.println("Base Application updated Successfully");
				}
		}
	}

	/**
	 * Method for Update project.
	 *
	 * @param cmd {@link CommandLine}
	 */
	private void updateProject(CommandLine cmd, ObjectType updateObjType) {
		com.magna.batch.restclient.obj.project.ProjectController userController = new com.magna.batch.restclient.obj.project.ProjectController();
		ProjectRequest projectRequest = new ProjectRequest();
		String projectName = null, statusStr = null, iconStr = null, desc_en = null, desc_de = null, remarks_en = null, remarks_de = null,
				strVal = "";
		IconRespWrapper iconRespWrapper = null;
		List<ProjectTranslation> projectTranslation = new ArrayList<>();
		ProjectTranslation projectTranslationTbl_en = new ProjectTranslation();
		projectTranslationTbl_en.setLanguageCode(com.magna.batch.utils.LANG_ENUM.ENGLISH.getLangCode());
		projectTranslation.add(projectTranslationTbl_en);
		ProjectTranslation projectTranslationTbl_de = new ProjectTranslation();
		projectTranslationTbl_de.setLanguageCode(com.magna.batch.utils.LANG_ENUM.GERMAN.getLangCode());
		projectTranslation.add(projectTranslationTbl_de);
		projectRequest.setProjectTranslations(projectTranslation);
		String[] optionValues = cmd.getOptionValues("update");
		if (optionValues.length > 0) {
			strVal = BatchUtil.getInstance().getStringValue(optionValues, cmd, updateObjType);
			if(strVal==null) return;
			String[] splittedStrVal = strVal.split("\"");
			String value;
			for (int i = 0; i < splittedStrVal.length; i++) {
				value = splittedStrVal[i];
				value = value.trim();
				if(!name(value)) return;
				if (value.split("=")[0].trim().equalsIgnoreCase("STATUS") && !value.endsWith("=")) {
					if (value.contains("=")) {
						statusStr = value.split("=", 2)[1];
						statusStr = statusStr.toUpperCase();
						if(!(statusStr.equalsIgnoreCase("active") || statusStr.equalsIgnoreCase("inactive"))){
							System.out.println("ERROR: Syntax error ! Status should be ACTIVE or INACTIVE");
							return;
						}
					} else {
						System.out.println("ERROR: Syntax error !");
						return;
					}
					continue;
				} else if (value.split("=")[0].trim().equalsIgnoreCase("STATUS") && value.endsWith("=")) {
					System.out.println("ERROR: Syntax error ! Status should be ACTIVE or INACTIVE");
					return;
				} else if(value.startsWith("STATUS") || value.startsWith("status")) {
					System.out.println("ERROR: Syntax error !");
					return;
				}
				if (value.split("=")[0].trim().equalsIgnoreCase("ICON_NAME") && !value.endsWith("=")) {
					if (value.contains("=")) {
						iconStr = value.split("=", 2)[1];
					} else {
						System.out.println("ERROR: Syntax error !");
						return;
					}
					continue;
				} else if (value.split("=")[0].trim().equalsIgnoreCase("ICON_NAME") && value.endsWith("=")) {
					iconStr="";
					continue;
				} else if(value.startsWith("ICON_NAME") || value.startsWith("icon_name")){
					System.out.println("ERROR: Syntax error !");
					return;
				}
				if (value.split("=")[0].trim().equalsIgnoreCase("DESCRIPTION_EN") && !value.endsWith("=")) {
					if(value.contains("\"")){
						if (value.contains("=")) {
							desc_en = value.split("=\"", 2)[1];
							if(!(desc_en.length()<240)){
								System.out.println("Please provide a valid description !" +CommonConstants.Messages.DESC_LIMIT_MESSAGE);
								return;
							}
						} else {
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}else{
						if (value.contains("=")) {
							desc_en = value.split("=", 2)[1];
							if(!(desc_en.length()<240)){
								System.out.println("Please provide a valid description !" +CommonConstants.Messages.DESC_LIMIT_MESSAGE);
								return;
							}
						} else {
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}
					continue;
				} else if (value.split("=")[0].trim().equalsIgnoreCase("DESCRIPTION_EN") && value.endsWith("=")) {
					desc_en = "";
					projectTranslationTbl_en.setDescription(desc_en);
					continue;
				} else if(value.startsWith("DESCRIPTION_EN") || value.startsWith("description_en")){
					System.out.println("ERROR: Syntax error !");
					return;
				}
				if (value.split("=")[0].trim().equalsIgnoreCase("DESCRIPTION_DE") && !value.endsWith("=")) {
					if(value.contains("\"")){
						if (value.contains("=")) {
							desc_de = value.split("=\"", 2)[1];
							if(!(desc_de.length()<240)){
								System.out.println("Please provide a valid description !" +CommonConstants.Messages.DESC_LIMIT_MESSAGE);
								return;
							}
						} else {
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}else{
						if (value.contains("=")) {
							desc_de = value.split("=", 2)[1];
							if(!(desc_de.length()<240)){
								System.out.println("Please provide a valid description !" +CommonConstants.Messages.DESC_LIMIT_MESSAGE);
								return;
							}
						} else {
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}
					continue;
				} else if (value.split("=")[0].trim().equalsIgnoreCase("DESCRIPTION_DE") && value.endsWith("=")) {
					desc_de = "";
					projectTranslationTbl_de.setDescription(desc_de);
					continue;
				} else if(value.startsWith("DESCRIPTION_DE") || value.startsWith("description_de")) {
					System.out.println("ERROR: Syntax error !");
					return;
				}
				if (value.split("=")[0].trim().equalsIgnoreCase("REMARKS_EN") && !value.endsWith("=")) {
					if(value.contains("\"")){
						if (value.contains("=")) {
							remarks_en = value.split("=\"", 2)[1];
							if(!(remarks_en.length()<1500)){
								System.out.println("Please provide a valid remarks !" +CommonConstants.Messages.REMARKS_LIMIT_MESSAGE);
								return;
							}
						} else {
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}else{
						if (value.contains("=")) {
							remarks_en = value.split("=", 2)[1];
							if(!(remarks_en.length()<1500)){
								System.out.println("Please provide a valid remarks !" +CommonConstants.Messages.REMARKS_LIMIT_MESSAGE);
								return;
							}
						} else {
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}
					continue;
				} else if (value.split("=")[0].trim().equalsIgnoreCase("REMARKS_EN") && value.endsWith("=")) {
					remarks_en = "";
					projectTranslationTbl_en.setRemarks(remarks_en);
					continue;
				} else if(value.startsWith("REMARKS_EN") || value.startsWith("remarks_en")){
					System.out.println("ERROR: Syntax error !");
					return;
				}
				if (value.split("=")[0].trim().equalsIgnoreCase("REMARKS_DE") && !value.endsWith("=")) {
					if(value.contains("\"")){
						if (value.contains("=")) {
							remarks_de = value.split("=\"", 2)[1];
							if(!(remarks_de.length()<1500)){
								System.out.println("Please provide a valid remarks !" +CommonConstants.Messages.REMARKS_LIMIT_MESSAGE);
								return;
							}
						} else {
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}else{
						if (value.contains("=")) {
							remarks_de = value.split("=", 2)[1];
							if(!(remarks_de.length()<1500)){
								System.out.println("Please provide a valid remarks !" +CommonConstants.Messages.REMARKS_LIMIT_MESSAGE);
								return;
							}
						} else {
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}
					continue;
				} else if (value.split("=")[0].trim().equalsIgnoreCase("REMARKS_DE") && value.endsWith("=")) {
					remarks_de = "";
					projectTranslationTbl_de.setRemarks(remarks_de);
					continue;
				} else if(value.startsWith("REMARKS_DE") || value.startsWith("remarks_de")) {
					System.out.println("ERROR: Syntax error !");
					return;
				}
			}
			projectName = getName(cmd, updateObjType);
			if(projectName==null) return;
			if(iconStr!=null){
				IconController iconController = new IconController();
				IkonRequest ikonRequest = new IkonRequest();
				ikonRequest.setIconName(iconStr);
				iconRespWrapper = iconController.getIconByName(ikonRequest);
				if(iconRespWrapper==null) return;
			}
			if (!BatchUtil.isEmpty(projectName)) {
				projectRequest.setName(projectName);
			}
			if (!BatchUtil.isEmpty(statusStr)) {
				projectRequest.setStatus(statusStr);
			}
			if (!BatchUtil.isEmpty(iconStr) && iconRespWrapper!=null) {
				projectRequest.setIconId(iconRespWrapper.getIconId());
			}
			if (!BatchUtil.isEmpty(desc_en)) {
				projectTranslationTbl_en.setDescription(desc_en);
			}
			if (!BatchUtil.isEmpty(remarks_en)) {
				projectTranslationTbl_en.setRemarks(remarks_en);
			}
			if (!BatchUtil.isEmpty(desc_de)) {
				projectTranslationTbl_de.setDescription(desc_de);
			}
			if (!BatchUtil.isEmpty(remarks_de)) {
				projectTranslationTbl_de.setRemarks(remarks_de);
			}
			List<ProjectTranslation> projectTranslationTblList = new ArrayList<ProjectTranslation>();
			projectTranslationTblList.add(projectTranslationTbl_en);
			projectTranslationTblList.add(projectTranslationTbl_de);
			projectRequest.setProjectTranslations(projectTranslationTblList);
				
			Boolean updateProject = userController.updateProject(projectRequest);
				if (updateProject == null || updateProject == Boolean.FALSE) {
					return;
				} else {
					System.out.println("Project updated Successfully");

				}
		}
	}

	/**
	 * Method for Update admin area.
	 *
	 * @param cmd {@link CommandLine}
	 */
	private void updateAdminArea(CommandLine cmd, ObjectType updateObjType) {
		com.magna.batch.restclient.obj.adminarea.AdminAreaController adminAreaController = new com.magna.batch.restclient.obj.adminarea.AdminAreaController();
		AdminAreaRequest adminAreaRequest = new AdminAreaRequest();
		String adminAreaName = null, statusStr = null, iconStr = null, singleton_app_timeout = null, hotline_contact_number = null, hotline_contact_email = null, desc_en = null, desc_de = null, remarks_en = null, remarks_de = null,
				strVal = "";
		IconRespWrapper iconRespWrapper = null;
		List<AdminAreaTranslation> adminAreaTranslation = new ArrayList<>();
		AdminAreaTranslation adminAreaTranslationTbl_en = new AdminAreaTranslation();
		adminAreaTranslationTbl_en.setLanguageCode(com.magna.batch.utils.LANG_ENUM.ENGLISH.getLangCode());
		adminAreaTranslation.add(adminAreaTranslationTbl_en);
		AdminAreaTranslation adminAreaTranslationTbl_de = new AdminAreaTranslation();
		adminAreaTranslationTbl_de.setLanguageCode(com.magna.batch.utils.LANG_ENUM.GERMAN.getLangCode());
		adminAreaTranslation.add(adminAreaTranslationTbl_de);
		adminAreaRequest.setAdminAreaTranslations(adminAreaTranslation);
		String[] optionValues = cmd.getOptionValues("update");
		if (optionValues.length > 0) {
			strVal = BatchUtil.getInstance().getStringValue(optionValues, cmd, updateObjType);
			if(strVal==null) return;
			String[] splittedStrVal = strVal.split("\"");
			String value;
			for (int i = 0; i < splittedStrVal.length; i++) {
				value = splittedStrVal[i];
				value = value.trim();
				if(!name(value)) return;
				if (value.split("=")[0].trim().equalsIgnoreCase("STATUS") && !value.endsWith("=")) {
					if (value.contains("=")) {
						statusStr = value.split("=", 2)[1];
						statusStr = statusStr.toUpperCase();
						if(!(statusStr.equalsIgnoreCase("active") || statusStr.equalsIgnoreCase("inactive"))){
							System.out.println("ERROR: Syntax error ! Status should be ACTIVE or INACTIVE");
							return;
						}
					} else {
						System.out.println("ERROR: Syntax error !");
						return;
					}
					continue;
				} else if (value.split("=")[0].trim().equalsIgnoreCase("STATUS") && value.endsWith("=")) {
					System.out.println("ERROR: Syntax error ! Status should be ACTIVE or INACTIVE");
					return;
				} else if(value.startsWith("STATUS") || value.startsWith("status")) {
					System.out.println("ERROR: Syntax error !");
					return;
				}
				if (value.split("=")[0].trim().equalsIgnoreCase("ICON_NAME") && !value.endsWith("=")) {
					if (value.contains("=")) {
						iconStr = value.split("=", 2)[1];
					} else {
						System.out.println("ERROR: Syntax error !");
						return;
					}
					continue;
				} else if (value.split("=")[0].trim().equalsIgnoreCase("ICON_NAME") && value.endsWith("=")) {
					iconStr="";
					continue;
				} else if(value.startsWith("ICON_NAME") || value.startsWith("icon_name")){
					System.out.println("ERROR: Syntax error !");
					return;
				}
				
				if (value.split("=")[0].trim().equalsIgnoreCase("SINGLETON_APP_TIMEOUT") && !value.endsWith("=")) {
					singleton_app_timeout = value.split("=", 2)[1];
					continue;
				} else if (value.split("=")[0].trim().equalsIgnoreCase("SINGLETON_APP_TIMEOUT") && value.endsWith("=")) {
					singleton_app_timeout = "";
					if(singleton_app_timeout.matches("[0-9]*")){
						long parseLong = Long.parseLong(singleton_app_timeout);
						adminAreaRequest.setSingleTonAppTimeOut(parseLong);
					} else {
						System.out.println("ERROR: Syntax Error ! singleton_app_timeout accepts only numbers.");
						return;
					}
					continue;
				} else if(value.startsWith("SINGLETON_APP_TIMEOUT") || value.startsWith("singleton_app_timeout")){
					System.out.println("ERROR: Syntax error !");
					return;
				}
				
				if (value.split("=")[0].trim().equalsIgnoreCase("HOTLINE_CONTACT_NUMBER") && !value.endsWith("=")) {
					hotline_contact_number = value.split("=", 2)[1];
					if(!(hotline_contact_number.length()<30)){
						System.out.println("Please provide a valid hotline contact number !" +CommonConstants.Messages.HOTLINE_CONTACT_LIMIT_MESSAGE);
						return;
					}
					continue;
				} else if(value.split("=")[0].trim().equalsIgnoreCase("HOTLINE_CONTACT_NUMBER") && value.endsWith("=")) {
					hotline_contact_number = "";
					adminAreaRequest.setHotlineContactNo(hotline_contact_number);
					continue;
				}  else if(value.startsWith("HOTLINE_CONTACT_NUMBER") || value.startsWith("hotline_contact_number")){
					System.out.println("ERROR: Syntax error !");
					return;
				}
				
				if (value.split("=")[0].trim().equalsIgnoreCase("HOTLINE_CONTACT_EMAIL") && !value.endsWith("=")) {
					hotline_contact_email = value.split("=", 2)[1];
					if(!hotline_contact_email.matches(CommonConstants.RegularExpressions.EMAIL_REGEX)){
						System.out.println("Please provide a valid email address ! " +CommonConstants.Messages.EMAIL_MESSAGE);
						return;
					}else if(!(hotline_contact_email.length()<30)){
						System.out.println("Please provide a valid email address !" +CommonConstants.Messages.NAME_LIMIT_MESSAGE);
						return;
					}
					continue;
				} else if(value.split("=")[0].trim().equalsIgnoreCase("HOTLINE_CONTACT_EMAIL") && value.endsWith("=")){
					hotline_contact_email = "";
					adminAreaRequest.setHotlineContactEmail(hotline_contact_email);
					continue;
				} else if(value.startsWith("HOTLINE_CONTACT_EMAIL") || value.startsWith("hotline_contact_email")){
					System.out.println("ERROR: Syntax error !");
					return;
				}
				if (value.split("=")[0].trim().equalsIgnoreCase("DESCRIPTION_EN") && !value.endsWith("=")) {
					if(value.contains("\"")){
						if (value.contains("=")) {
							desc_en = value.split("=\"", 2)[1];
							if(!(desc_en.length()<240)){
								System.out.println("Please provide a valid description !" +CommonConstants.Messages.DESC_LIMIT_MESSAGE);
								return;
							}
						} else {
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}else{
						if (value.contains("=")) {
							desc_en = value.split("=", 2)[1];
							if(!(desc_en.length()<240)){
								System.out.println("Please provide a valid description !" +CommonConstants.Messages.DESC_LIMIT_MESSAGE);
								return;
							}
						} else {
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}
					continue;
				} else if (value.split("=")[0].trim().equalsIgnoreCase("DESCRIPTION_EN") && value.endsWith("=")) {
					desc_en = "";
					adminAreaTranslationTbl_en.setDescription(desc_en);
					continue;
				} else if(value.startsWith("DESCRIPTION_EN") || value.startsWith("description_en")){
					System.out.println("ERROR: Syntax error !");
					return;
				}
				if (value.split("=")[0].trim().equalsIgnoreCase("DESCRIPTION_DE") && !value.endsWith("=")) {
					if(value.contains("\"")){
						if (value.contains("=")) {
							desc_de = value.split("=\"", 2)[1];
							if(!(desc_de.length()<240)){
								System.out.println("Please provide a valid description !" +CommonConstants.Messages.DESC_LIMIT_MESSAGE);
								return;
							}
						} else {
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}else{
						if (value.contains("=")) {
							desc_de = value.split("=", 2)[1];
							if(!(desc_de.length()<240)){
								System.out.println("Please provide a valid description !" +CommonConstants.Messages.DESC_LIMIT_MESSAGE);
								return;
							}
						} else {
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}
					continue;
				} else if (value.split("=")[0].trim().equalsIgnoreCase("DESCRIPTION_DE") && value.endsWith("=")) {
					desc_de = "";
					adminAreaTranslationTbl_de.setDescription(desc_de);
					continue;
				} else if(value.startsWith("DESCRIPTION_DE") || value.startsWith("description_de")) {
					System.out.println("ERROR: Syntax error !");
					return;
				}
				if (value.split("=")[0].trim().equalsIgnoreCase("REMARKS_EN") && !value.endsWith("=")) {
					if(value.contains("\"")){
						if (value.contains("=")) {
							remarks_en = value.split("=\"", 2)[1];
							if(!(remarks_en.length()<1500)){
								System.out.println("Please provide a valid remarks !" +CommonConstants.Messages.REMARKS_LIMIT_MESSAGE);
								return;
							}
						} else {
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}else{
						if (value.contains("=")) {
							remarks_en = value.split("=", 2)[1];
							if(!(remarks_en.length()<1500)){
								System.out.println("Please provide a valid remarks !" +CommonConstants.Messages.REMARKS_LIMIT_MESSAGE);
								return;
							}
						} else {
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}
					continue;
				} else if (value.split("=")[0].trim().equalsIgnoreCase("REMARKS_EN") && value.endsWith("=")) {
					remarks_en = "";
					adminAreaTranslationTbl_en.setRemarks(remarks_en);
					continue;
				} else if(value.startsWith("REMARKS_EN") || value.startsWith("remarks_en")){
					System.out.println("ERROR: Syntax error !");
					return;
				}
				if (value.split("=")[0].trim().equalsIgnoreCase("REMARKS_DE") && !value.endsWith("=")) {
					if(value.contains("\"")){
						if (value.contains("=")) {
							remarks_de = value.split("=\"", 2)[1];
							if(!(remarks_de.length()<1500)){
								System.out.println("Please provide a valid remarks !" +CommonConstants.Messages.REMARKS_LIMIT_MESSAGE);
								return;
							}
						} else {
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}else{
						if (value.contains("=")) {
							remarks_de = value.split("=", 2)[1];
							if(!(remarks_de.length()<1500)){
								System.out.println("Please provide a valid remarks !" +CommonConstants.Messages.REMARKS_LIMIT_MESSAGE);
								return;
							}
						} else {
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}
					continue;
				} else if (value.split("=")[0].trim().equalsIgnoreCase("REMARKS_DE") && value.endsWith("=")) {
					remarks_de = "";
					adminAreaTranslationTbl_de.setRemarks(remarks_de);
					continue;
				} else if(value.startsWith("REMARKS_DE") || value.startsWith("remarks_de")) {
					System.out.println("ERROR: Syntax error !");
					return;
				}
			}
			adminAreaName = getName(cmd, updateObjType);
			if(adminAreaName==null) return;
			if(iconStr!=null){
				IconController iconController = new IconController();
				IkonRequest ikonRequest = new IkonRequest();
				ikonRequest.setIconName(iconStr);
				iconRespWrapper = iconController.getIconByName(ikonRequest);
				if(iconRespWrapper==null) return;
			}
			if (!BatchUtil.isEmpty(adminAreaName)) {
				adminAreaRequest.setName(adminAreaName);
			}
			if (!BatchUtil.isEmpty(statusStr)) {
				adminAreaRequest.setStatus(statusStr);
			}
			if (!BatchUtil.isEmpty(iconStr) && iconRespWrapper!=null) {
				adminAreaRequest.setIconId(iconRespWrapper.getIconId());
			}
			if (!BatchUtil.isEmpty(singleton_app_timeout)) {
				if(singleton_app_timeout.matches("[0-9]*")){
					long parseLong = Long.parseLong(singleton_app_timeout);
					if(parseLong <= 7200){
						adminAreaRequest.setSingleTonAppTimeOut(parseLong);
					} else {
						System.out.println("ERROR: Syntax Error ! singleton_app_timeout limit should not exceed to 7200.");
						return;
					}
				} else {
					System.out.println("ERROR: Syntax Error ! singleton_app_timeout accepts only numbers.");
					return;
				}
			}
			if (!BatchUtil.isEmpty(hotline_contact_number)) {
				adminAreaRequest.setHotlineContactNo(hotline_contact_number);
			}
			if (!BatchUtil.isEmpty(hotline_contact_email)) {
				adminAreaRequest.setHotlineContactEmail(hotline_contact_email);
			}
			if (!BatchUtil.isEmpty(desc_en)) {
				adminAreaTranslationTbl_en.setDescription(desc_en);
			}
			if (!BatchUtil.isEmpty(remarks_en)) {
				adminAreaTranslationTbl_en.setRemarks(remarks_en);
			}
			if (!BatchUtil.isEmpty(desc_de)) {
				adminAreaTranslationTbl_de.setDescription(desc_de);
			}
			if (!BatchUtil.isEmpty(remarks_de)) {
				adminAreaTranslationTbl_de.setRemarks(remarks_de);
			}
			List<AdminAreaTranslation> adminAreaTranslationTblList = new ArrayList<AdminAreaTranslation>();
			adminAreaTranslationTblList.add(adminAreaTranslationTbl_en);
			adminAreaTranslationTblList.add(adminAreaTranslationTbl_de);
			adminAreaRequest.setAdminAreaTranslations(adminAreaTranslationTblList);
				Boolean updateAdminArea = adminAreaController.updateAdminArea(adminAreaRequest);
				if (updateAdminArea == null || updateAdminArea == Boolean.FALSE) {
					return;
				} else {
					System.out.println("Administration Area updated Successfully");
				}
		}
	}

	/**
	 * Method for Update site.
	 *
	 * @param cmd {@link CommandLine}
	 */
	private void updateSite(final CommandLine cmd, ObjectType updateObjType) {
		com.magna.batch.restclient.obj.site.SiteController siteController = new com.magna.batch.restclient.obj.site.SiteController();
		SiteRequest siteRequest = new SiteRequest();
		String siteName = null, statusStr = null, iconStr = null, desc_en = null, desc_de = null, remarks_en = null,
				remarks_de = null, strVal = "";
		IconRespWrapper iconRespWrapper = null;
		List<SiteTranslation> siteTranslation = new ArrayList<>();
		SiteTranslation siteTranslationTbl_en = new SiteTranslation();
		siteTranslationTbl_en.setLanguageCode(com.magna.batch.utils.LANG_ENUM.ENGLISH.getLangCode());
		siteTranslation.add(siteTranslationTbl_en);
		SiteTranslation siteTranslationTbl_de = new SiteTranslation();
		siteTranslationTbl_de.setLanguageCode(com.magna.batch.utils.LANG_ENUM.GERMAN.getLangCode());
		siteTranslation.add(siteTranslationTbl_de);
		siteRequest.setSiteTranslations(siteTranslation);
		String[] optionValues = cmd.getOptionValues("update");
		if (optionValues.length > 0) {
			strVal = BatchUtil.getInstance().getStringValue(optionValues, cmd, updateObjType);
			if(strVal==null) return;
			String[] splittedStrVal = strVal.split("\"");
			String value;
			for (int i = 0; i < splittedStrVal.length; i++) {
				value = splittedStrVal[i];
				value = value.trim();
				if(!name(value)) return;
				if (value.split("=")[0].trim().equalsIgnoreCase("STATUS") && !value.endsWith("=")) {
					if (value.contains("=")) {
						statusStr = value.split("=", 2)[1];
						statusStr = statusStr.toUpperCase();
						if(!(statusStr.equalsIgnoreCase("active") || statusStr.equalsIgnoreCase("inactive"))){
							System.out.println("ERROR: Syntax error ! Status should be ACTIVE or INACTIVE");
							return;
						}
					} else {
						System.out.println("ERROR: Syntax error !");
						return;
					}
					continue;
				} else if (value.split("=")[0].trim().equalsIgnoreCase("STATUS") && value.endsWith("=")) {
					System.out.println("ERROR: Syntax error ! Status should be ACTIVE or INACTIVE");
					return;
				} else if(value.startsWith("STATUS") || value.startsWith("status")) {
					System.out.println("ERROR: Syntax error !");
					return;
				}
				if (value.split("=")[0].trim().equalsIgnoreCase("ICON_NAME") && !value.endsWith("=")) {
					if (value.contains("=")) {
						iconStr = value.split("=", 2)[1];
					} else {
						System.out.println("ERROR: Syntax error !");
						return;
					}
					continue;
				} else if (value.split("=")[0].trim().equalsIgnoreCase("ICON_NAME") && value.endsWith("=")) {
					iconStr="";
					continue;
				} else if(value.startsWith("ICON_NAME") || value.startsWith("icon_name")){
					System.out.println("ERROR: Syntax error !");
					return;
				}
				if (value.split("=")[0].trim().equalsIgnoreCase("DESCRIPTION_EN") && !value.endsWith("=")) {
					if(value.contains("\"")){
						if (value.contains("=")) {
							desc_en = value.split("=\"", 2)[1];
							if(!(desc_en.length()<240)){
								System.out.println("Please provide a valid description !" +CommonConstants.Messages.DESC_LIMIT_MESSAGE);
								return;
							}
						} else {
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}else{
						if (value.contains("=")) {
							desc_en = value.split("=", 2)[1];
							if(!(desc_en.length()<240)){
								System.out.println("Please provide a valid description !" +CommonConstants.Messages.DESC_LIMIT_MESSAGE);
								return;
							}
						} else {
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}
					continue;
				} else if (value.split("=")[0].trim().equalsIgnoreCase("DESCRIPTION_EN") && value.endsWith("=")) {
					desc_en = "";
					siteTranslationTbl_en.setDescription(desc_en);
					continue;
				} else if(value.startsWith("DESCRIPTION_EN") || value.startsWith("description_en")){
					System.out.println("ERROR: Syntax error !");
					return;
				}
				if (value.split("=")[0].trim().equalsIgnoreCase("DESCRIPTION_DE") && !value.endsWith("=")) {
					if(value.contains("\"")){
						if (value.contains("=")) {
							desc_de = value.split("=\"", 2)[1];
							if(!(desc_de.length()<240)){
								System.out.println("Please provide a valid description !" +CommonConstants.Messages.DESC_LIMIT_MESSAGE);
								return;
							}
						} else {
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}else{
						if (value.contains("=")) {
							desc_de = value.split("=", 2)[1];
							if(!(desc_de.length()<240)){
								System.out.println("Please provide a valid description !" +CommonConstants.Messages.DESC_LIMIT_MESSAGE);
								return;
							}
						} else {
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}
					continue;
				} else if (value.split("=")[0].trim().equalsIgnoreCase("DESCRIPTION_DE") && value.endsWith("=")) {
					desc_de = "";
					siteTranslationTbl_de.setDescription(desc_de);
					continue;
				} else if(value.startsWith("DESCRIPTION_DE") || value.startsWith("description_de")) {
					System.out.println("ERROR: Syntax error !");
					return;
				}
				if (value.split("=")[0].trim().equalsIgnoreCase("REMARKS_EN") && !value.endsWith("=")) {
					if(value.contains("\"")){
						if (value.contains("=")) {
							remarks_en = value.split("=\"", 2)[1];
							if(!(remarks_en.length()<1500)){
								System.out.println("Please provide a valid remarks !" +CommonConstants.Messages.REMARKS_LIMIT_MESSAGE);
								return;
							}
						} else {
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}else{
						if (value.contains("=")) {
							remarks_en = value.split("=", 2)[1];
							if(!(remarks_en.length()<1500)){
								System.out.println("Please provide a valid remarks !" +CommonConstants.Messages.REMARKS_LIMIT_MESSAGE);
								return;
							}
						} else {
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}
					continue;
				} else if (value.split("=")[0].trim().equalsIgnoreCase("REMARKS_EN") && value.endsWith("=")) {
					remarks_en = "";
					siteTranslationTbl_en.setRemarks(remarks_en);
					continue;
				} else if(value.startsWith("REMARKS_EN") || value.startsWith("remarks_en")){
					System.out.println("ERROR: Syntax error !");
					return;
				}
				if (value.split("=")[0].trim().equalsIgnoreCase("REMARKS_DE") && !value.endsWith("=")) {
					if(value.contains("\"")){
						if (value.contains("=")) {
							remarks_de = value.split("=\"", 2)[1];
							if(!(remarks_de.length()<1500)){
								System.out.println("Please provide a valid remarks !" +CommonConstants.Messages.REMARKS_LIMIT_MESSAGE);
								return;
							}
						} else {
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}else{
						if (value.contains("=")) {
							remarks_de = value.split("=", 2)[1];
							if(!(remarks_de.length()<1500)){
								System.out.println("Please provide a valid remarks !" +CommonConstants.Messages.REMARKS_LIMIT_MESSAGE);
								return;
							}
						} else {
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}
					continue;
				} else if (value.split("=")[0].trim().equalsIgnoreCase("REMARKS_DE") && value.endsWith("=")) {
					remarks_de = "";
					siteTranslationTbl_de.setRemarks(remarks_de);
					continue;
				} else if(value.startsWith("REMARKS_DE") || value.startsWith("remarks_de")) {
					System.out.println("ERROR: Syntax error !");
					return;
				}
			}
			
			siteName = getName(cmd, updateObjType);
			if(siteName==null) return;
			if(iconStr!=null){
				IconController iconController = new IconController();
				IkonRequest ikonRequest = new IkonRequest();
				ikonRequest.setIconName(iconStr);
				iconRespWrapper = iconController.getIconByName(ikonRequest);
				if(iconRespWrapper==null) return;
			}
			if (!BatchUtil.isEmpty(siteName)) {
				siteRequest.setName(siteName);
			}
			if (!BatchUtil.isEmpty(statusStr)) {
				if(statusStr.equalsIgnoreCase("active") || statusStr.equalsIgnoreCase("inactive")){
					siteRequest.setStatus(statusStr.toUpperCase().equals("ACTIVE") ? Status.ACTIVE : Status.INACTIVE);
				} else{
					System.out.println("ERROR: Syntax error ! Status should be ACTIVE or INACTIVE");
					return;
				}
			}
			if (!BatchUtil.isEmpty(iconStr) && iconRespWrapper!=null) {
				siteRequest.setIconId(iconRespWrapper.getIconId());
			}
			if (!BatchUtil.isEmpty(desc_en)) {
				siteTranslationTbl_en.setDescription(desc_en);
			}
			if (!BatchUtil.isEmpty(remarks_en)) {
				siteTranslationTbl_en.setRemarks(remarks_en);
			}
			if (!BatchUtil.isEmpty(desc_de)) {
				siteTranslationTbl_de.setDescription(desc_de);
			}
			if (!BatchUtil.isEmpty(remarks_de)) {
				siteTranslationTbl_de.setRemarks(remarks_de);
			}
			List<SiteTranslation> siteTranslationTblList = new ArrayList<SiteTranslation>();
			siteTranslationTblList.add(siteTranslationTbl_en);
			siteTranslationTblList.add(siteTranslationTbl_de);
			siteRequest.setSiteTranslations(siteTranslationTblList);
				Boolean updateSite = siteController.updateSite(siteRequest);
				if (updateSite == null || updateSite == Boolean.FALSE) {
					return;
				} else {
					System.out.println("Site updated Successfully");

				}
		}
		
	}

	/**
	 * Method for Update user.
	 *
	 * @param cmd {@link CommandLine}
	 */
	private void updateUser(final CommandLine cmd, ObjectType updateObjType) {
		com.magna.batch.restclient.obj.users.UserController userController = new com.magna.batch.restclient.obj.users.UserController();
		UserRequest userRequest = new UserRequest();
		String userName = null, statusStr = null, iconStr = null, fullName = null, emailId = null, department = null,
				telephoneNumber = null, desc_en = null, desc_de = null, remarks_en = null, remarks_de = null,
				strVal = "";
		IconRespWrapper iconRespWrapper = null;
		List<UserTranslation> userTranslation = new ArrayList<>();
		UserTranslation userTranslationTbl_en = new UserTranslation();
		userTranslationTbl_en.setLanguageCode(com.magna.batch.utils.LANG_ENUM.ENGLISH.getLangCode());
		userTranslation.add(userTranslationTbl_en);
		UserTranslation userTranslationTbl_de = new UserTranslation();
		userTranslationTbl_de.setLanguageCode(com.magna.batch.utils.LANG_ENUM.GERMAN.getLangCode());
		userTranslation.add(userTranslationTbl_de);
		userRequest.setUserTranslation(userTranslation);
		String[] optionValues = cmd.getOptionValues("update");
		if (optionValues.length > 0) {
			strVal = BatchUtil.getInstance().getStringValue(optionValues, cmd, updateObjType);
			if(strVal==null) return;
			String[] splittedStrVal = strVal.split("\"");
			String value;
			for (int i = 0; i < splittedStrVal.length; i++) {
				value = splittedStrVal[i];
				value = value.trim();
				if(!name(value)) return;
				if (value.split("=")[0].trim().equalsIgnoreCase("STATUS") && !value.endsWith("=")) {
					if (value.contains("=")) {
						statusStr = value.split("=", 2)[1];
						statusStr = statusStr.toUpperCase();
						if(!(statusStr.equalsIgnoreCase("active") || statusStr.equalsIgnoreCase("inactive"))){
							System.out.println("ERROR: Syntax error ! Status should be ACTIVE or INACTIVE");
							return;
						}
					} else {
						System.out.println("ERROR: Syntax error !");
						return;
					}
					continue;
				} else if (value.split("=")[0].trim().equalsIgnoreCase("STATUS") && value.endsWith("=")) {
					System.out.println("ERROR: Syntax error ! Status should be ACTIVE or INACTIVE");
					return;
				} else if((value.startsWith("STATUS") || value.startsWith("status"))) {
					System.out.println("ERROR: Syntax error !");
					return;
				}
				if (value.split("=")[0].trim().equalsIgnoreCase("ICON_NAME") && !value.endsWith("=")) {
					if (value.contains("=")) {
						iconStr = value.split("=", 2)[1];
					} else {
						System.out.println("ERROR: Syntax error !");
						return;
					}
					continue;
				} else if (value.split("=")[0].trim().equalsIgnoreCase("ICON_NAME") && value.endsWith("=")) {
					iconStr="";
					continue;
				} else if((value.startsWith("ICON_NAME") || value.startsWith("icon_name"))){
					System.out.println("ERROR: Syntax error !");
					return;
				}
				
				if (value.split("=")[0].trim().equalsIgnoreCase("FULL_NAME") && !value.endsWith("=")) {
					if (value.contains("\"")) {
						if (value.contains("=")) {
							fullName = value.split("=\"", 2)[1];
							if(!(fullName.length()<30)){
								System.out.println("Please provide a valid full name !" +CommonConstants.Messages.NAME_LIMIT_MESSAGE);
								return;
							}
						} else {
							System.out.println("ERROR: Syntax error !");
							return;
						}
					} else {
						if (value.contains("=")) {
							fullName = value.split("=", 2)[1];
							if(!(fullName.length()<30)){
								System.out.println("Please provide a valid full name !" +CommonConstants.Messages.NAME_LIMIT_MESSAGE);
								return;
							}
						} else {
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}
					continue;
				} else if (value.split("=")[0].trim().equalsIgnoreCase("FULL_NAME") && value.endsWith("=")) {
					fullName = "";
					userRequest.setFullName(fullName);
					continue;
				} else if((value.startsWith("FULL_NAME") || value.startsWith("full_name"))){
					System.out.println("ERROR: Syntax error !");
					return;
				}
				
				if (value.split("=")[0].trim().equalsIgnoreCase("EMAIL_ID") && !value.endsWith("=")) {
					if (value.contains("=")) {
						emailId = value.split("=", 2)[1];
						if(!emailId.matches(CommonConstants.RegularExpressions.EMAIL_REGEX)){
							System.out.println("Please provide a valid email address ! " +CommonConstants.Messages.EMAIL_MESSAGE);
							return;
						}else if(!(emailId.length()<60)){
							System.out.println("Please provide a valid email address !" +CommonConstants.Messages.LIMIT_MESSAGE);
							return;
						}
					} else {
						System.out.println("ERROR: Syntax error !");
						return;
					}
					continue;
				} else if (value.split("=")[0].trim().equalsIgnoreCase("EMAIL_ID") && value.endsWith("=")) {
					emailId = "";
					userRequest.setEmail(emailId);
					continue;
				} else if((value.startsWith("EMAIL_ID") ||value.startsWith("email_id"))){
					System.out.println("ERROR: Syntax error !");
					return;
				}
				
				if (value.split("=")[0].trim().equalsIgnoreCase("DEPARTMENT") && !value.endsWith("=")) {
					if(value.contains("\"")){
						if (value.contains("=")) {
							department = value.split("=\"", 2)[1];
							if(!(department.length()<60)){
								System.out.println("Please provide a valid department name !" +CommonConstants.Messages.LIMIT_MESSAGE);
								return;
							}
						} else {
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}else{
						if (value.contains("=")) {
							department = value.split("=", 2)[1];
							if(!(department.length()<60)){
								System.out.println("Please provide a valid department name !" +CommonConstants.Messages.LIMIT_MESSAGE);
								return;
							}
						} else {
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}
					continue;
				} else if (value.split("=")[0].trim().equalsIgnoreCase("DEPARTMENT") && value.endsWith("=")) {
					department = "";
					userRequest.setDepartment(department);
					continue;
				} else if((value.startsWith("DEPARTMENT") || value.startsWith("department"))){
					System.out.println("ERROR: Syntax error !");
					return;
				}
				if (value.split("=")[0].trim().equalsIgnoreCase("TELEPHONE_NUMBER") && !value.endsWith("=")) {
					if (value.contains("=")) {
						telephoneNumber = value.split("=", 2)[1];
						if(!(telephoneNumber.length()<60)){
							System.out.println("Please provide a valid telephone number !" +CommonConstants.Messages.LIMIT_MESSAGE);
							return;
						}
					} else {
						System.out.println("ERROR: Syntax error !");
						return;
					}
					continue;
				} else if (value.split("=")[0].trim().equalsIgnoreCase("TELEPHONE_NUMBER") && value.endsWith("=")) {
					telephoneNumber = "";
					userRequest.setTelephoneNumber(telephoneNumber);
					continue;
				} else if((value.startsWith("TELEPHONE_NUMBER") || value.startsWith("telephone_number"))) {
					System.out.println("ERROR: Syntax error !");
					return;
				}
				if (value.split("=")[0].trim().equalsIgnoreCase("DESCRIPTION_EN") && !value.endsWith("=")) {
					if(value.contains("\"")){
						if (value.contains("=")) {
							desc_en = value.split("=\"", 2)[1];
							if(!(desc_en.length()<240)){
								System.out.println("Please provide a valid description !" +CommonConstants.Messages.DESC_LIMIT_MESSAGE);
								return;
							}
						} else {
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}else{
						if (value.contains("=")) {
							desc_en = value.split("=", 2)[1];
							if(!(desc_en.length()<240)){
								System.out.println("Please provide a valid description !" +CommonConstants.Messages.DESC_LIMIT_MESSAGE);
								return;
							}
						} else {
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}
					continue;
				} else if (value.split("=")[0].trim().equalsIgnoreCase("DESCRIPTION_EN") && value.endsWith("=")) {
					desc_en = "";
					userTranslationTbl_en.setDescription(desc_en);
					continue;
				} else if((value.startsWith("DESCRIPTION_EN") || value.startsWith("description_en"))){
					System.out.println("ERROR: Syntax error !");
					return;
				}
				if (value.split("=")[0].trim().equalsIgnoreCase("DESCRIPTION_DE") && !value.endsWith("=")) {
					if(value.contains("\"")){
						if (value.contains("=")) {
							desc_de = value.split("=\"", 2)[1];
							if(!(desc_de.length()<240)){
								System.out.println("Please provide a valid description !" +CommonConstants.Messages.DESC_LIMIT_MESSAGE);
								return;
							}
						} else {
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}else{
						if (value.contains("=")) {
							desc_de = value.split("=", 2)[1];
							if(!(desc_de.length()<240)){
								System.out.println("Please provide a valid description !" +CommonConstants.Messages.DESC_LIMIT_MESSAGE);
								return;
							}
						} else {
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}
					continue;
				} else if (value.split("=")[0].trim().equalsIgnoreCase("DESCRIPTION_DE") && value.endsWith("=")) {
					desc_de = "";
					userTranslationTbl_de.setDescription(desc_de);
					continue;
				} else if(value.startsWith("DESCRIPTION_DE") || value.startsWith("description_de")) {
					System.out.println("ERROR: Syntax error !");
					return;
				}
				if (value.split("=")[0].trim().equalsIgnoreCase("REMARKS_EN") && !value.endsWith("=")) {
					if(value.contains("\"")){
						if (value.contains("=")) {
							remarks_en = value.split("=\"", 2)[1];
							if(!(remarks_en.length()<1500)){
								System.out.println("Please provide a valid remarks !" +CommonConstants.Messages.REMARKS_LIMIT_MESSAGE);
								return;
							}
						} else {
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}else{
						if (value.contains("=")) {
							remarks_en = value.split("=", 2)[1];
							if(!(remarks_en.length()<1500)){
								System.out.println("Please provide a valid remarks !" +CommonConstants.Messages.REMARKS_LIMIT_MESSAGE);
								return;
							}
						} else {
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}
					continue;
				} else if (value.split("=")[0].trim().equalsIgnoreCase("REMARKS_EN") && value.endsWith("=")) {
					remarks_en = "";
					userTranslationTbl_en.setRemarks(remarks_en);
					continue;
				} else if(value.startsWith("REMARKS_EN") || value.startsWith("remarks_en")){
					System.out.println("ERROR: Syntax error !");
					return;
				}
				if (value.split("=")[0].trim().equalsIgnoreCase("REMARKS_DE") && !value.endsWith("=")) {
					if(value.contains("\"")){
						if (value.contains("=")) {
							remarks_de = value.split("=\"", 2)[1];
							if(!(remarks_de.length()<1500)){
								System.out.println("Please provide a valid remarks !" +CommonConstants.Messages.REMARKS_LIMIT_MESSAGE);
								return;
							}
						} else {
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}else{
						if (value.contains("=")) {
							remarks_de = value.split("=", 2)[1];
							if(!(remarks_de.length()<1500)){
								System.out.println("Please provide a valid remarks !" +CommonConstants.Messages.REMARKS_LIMIT_MESSAGE);
								return;
							}
						} else {
							System.out.println("ERROR: Syntax error !");
							return;
						}
					}
					continue;
				} else if (value.split("=")[0].trim().equalsIgnoreCase("REMARKS_DE") && value.endsWith("=")) {
					remarks_de = "";
					userTranslationTbl_de.setRemarks(remarks_de);
					continue;
				} else if(value.startsWith("REMARKS_DE") || value.startsWith("remarks_de")) {
					System.out.println("ERROR: Syntax error !");
					return;
				}
			}
			
			userName = getName(cmd, updateObjType);
			if(userName==null) return;
			if(iconStr!=null){
				IconController iconController = new IconController();
				IkonRequest ikonRequest = new IkonRequest();
				ikonRequest.setIconName(iconStr);
				iconRespWrapper = iconController.getIconByName(ikonRequest);
				if(iconRespWrapper==null) return;
			}
			
			if (!BatchUtil.isEmpty(userName)) {
				userRequest.setUserName(userName);
			}
			if (!BatchUtil.isEmpty(fullName)) {
				userRequest.setFullName(fullName);
			}
			if (!BatchUtil.isEmpty(emailId)) {
				userRequest.setEmail(emailId);
			}
			if (!BatchUtil.isEmpty(statusStr)) {
				if(statusStr.equalsIgnoreCase("active") || statusStr.equalsIgnoreCase("inactive")){
					userRequest.setStatus(statusStr.toUpperCase().equals("ACTIVE") ? Status.ACTIVE : Status.INACTIVE);
				} else{
					System.out.println("ERROR: Syntax error ! Status should be ACTIVE or INACTIVE");
					return;
				}
			}
			if (!BatchUtil.isEmpty(iconStr) && iconRespWrapper!=null) {
				userRequest.setIconId(iconRespWrapper.getIconId());
			}
			if (!BatchUtil.isEmpty(department)) {
				userRequest.setDepartment(department);
			}
			if (!BatchUtil.isEmpty(telephoneNumber)) {
				userRequest.setTelephoneNumber(telephoneNumber);
			}
			if (!BatchUtil.isEmpty(desc_en)) {
				userTranslationTbl_en.setDescription(desc_en);
			}
			if (!BatchUtil.isEmpty(remarks_en)) {
				userTranslationTbl_en.setRemarks(remarks_en);
			}
			if (!BatchUtil.isEmpty(desc_de)) {
				userTranslationTbl_de.setDescription(desc_de);
			}
			if (!BatchUtil.isEmpty(remarks_de)) {
				userTranslationTbl_de.setRemarks(remarks_de);
			}
			List<UserTranslation> userTranslationTblList = new ArrayList<UserTranslation>();
			userTranslationTblList.add(userTranslationTbl_en);
			userTranslationTblList.add(userTranslationTbl_de);
			userRequest.setUserTranslation(userTranslationTblList);
			try {
				Boolean updateUser = userController.updateUser(userRequest);
				if (updateUser == null || updateUser == Boolean.FALSE) {
					return;
				} else {
					System.out.println("User updated Successfully");

				}
			} catch (CannotCreateObjectException e) {
				System.out.println(e.getMessage());
				return;
			} catch (UnauthorizedAccessException e) {
				System.out.println("Permission error ! You do not have permission for this operation");
				return;
			} catch (Exception e) {
				System.out.println("ERROR: Syntax error !");
			}
		}
	}

	/**
	 * @param value
	 */
	private boolean name(String value) {
		boolean updateName = true;
		if ((value.startsWith("NAME") || value.startsWith("name")) && !value.endsWith("=")) {
			if(value.contains("\"")){
				if (value.contains("=")) {
					System.out.println("Unable to update, use caxStartAdmin to update name ! ");
					return updateName = false;
				} else {
					System.out.println("ERROR: Syntax error !");
					return updateName = false;
				}
			}else{
				if (value.contains("=")) {
					System.out.println("Unable to update, use caxStartAdmin to update name ! ");
					return updateName = false;
				} else {
					System.out.println("ERROR: Syntax error !");
					return updateName = false;
				}
			}
		} else if ((value.startsWith("NAME") || value.startsWith("name")) && value.endsWith("=")){
			System.out.println("Unable to update, use caxStartAdmin to update name ! ");
			return updateName;
		}
		return updateName;
	}

	
	/**
	 * Gets the name.
	 *
	 * @param cmd {@link CommandLine}
	 * @param objType {@link ObjectType}
	 * @return the name
	 */
	private String getName(final CommandLine cmd, ObjectType objType) {
		String name = null;
		StringBuilder stbName= new StringBuilder();
		String[] whereOptionValues = cmd.getOptionValues("where");
		if(whereOptionValues != null && whereOptionValues.length>3){
			System.out.print("ERROR: Syntax error !");
			return null;
		} else if(whereOptionValues != null && whereOptionValues.length > 0) {
			for (int index = 0; index < whereOptionValues.length ; index++) {
					String string = whereOptionValues[index];
					stbName = stbName.append(string);
				}
			name = stbName.toString().trim();
			ColumnChecker columnChecker = new ColumnChecker();
			boolean column = columnChecker.getColumn(name, objType, cmd);
			if(!column){
				return null;
			}
			if((name.startsWith("NAME") || name.startsWith("name")) && !name.endsWith("=") && name.contains("=")){
				if(name.contains("\"")){
					name = name.split("=\"")[1];
				}else{
					name = name.split("=")[1];
				}
			} else if ((name.startsWith("NAME") || name.startsWith("name")) && name.endsWith("=")){
				System.out.println(objType.toString().toLowerCase()+" name is required");
				return null;
			} else {
				System.out.print("ERROR: Syntax error !");
				return null;
			}
			name = name.replace("'", "");
			name = name.replace("\"", "");
		}
		return name;
	}
}
