package com.magna.batch.utils;
/**
 * The Interface RestClientConstants.
 */
public interface  RestClientConstants {
	
	/**  SUCCESS constant. */
	String SUCCESS = "Success";

	/** ACCEPT_HEADER constants. */
	String ACCEPT_HEADER = "Accept";

	/** APPLICATION_JSON constants. */
	String APPLICATION_JSON = "application/json";

	/** CONTENT_TYPE constants. */
	String CONTENT_TYPE = "Content-Type";

	/** GET constant. */
	String GET = "GET";

	/** POST constant. */
	String POST = "POST";

	/** PUT constant. */
	String PUT = "PUT";

	/** DELETE constant. */
	String DELETE = "DELETE";
	
	/** GET_USERS constant. */
	String GET_USERS = "/user/findAll";
	
	/** GET_USER_BY_NAME constant. */
	String GET_USER_BY_NAME = "/user/findByName";
	
	/** GET_START_APP_BY_NAME constant. */
	String GET_START_APP_BY_NAME = "/startApplication/caxBatchStartFindByName";
	
	/** GET_PROJECT_APP_BY_NAME constant. */
	String GET_PROJECT_APP_BY_NAME = "/projectApplication/caxBatchStartFindByName";
	
	/** GET_USER_APP_BY_NAME constant. */
	String GET_USER_APP_BY_NAME = "/userApplication/caxBatchStartFindByName";
	
	//String GET_START_APPLICATIONS = "/startApplication/findAll";

	/** GET_SITE_BY_NAME constant. *//*
	String GET_SITE_BY_NAME = "/site/findByName";
	
	*//** GET_ADMIN_AREA_BY_NAME constant. *//*
	String GET_ADMIN_AREA_BY_NAME = "/area/findByName";*/
	
	/** GET_USERS_BY_PROJECT_ID constant. */
	String GET_USERS_BY_PROJECT_ID = "/user/findUsersByProjectId";

	/** GET_USERS_BY_USER_APP_ID constant. */
	String GET_USERS_BY_USER_APP_ID = "/user/findUsersByUserAppId";

	/** GET_USERS_BY_PROJECT_APP_ID constant. */
	String GET_USERS_BY_PROJECT_APP_ID = "/user/findUsersByProjectAppId";

	/** GET_USERS_BY_START_APP_ID constant. */
	String GET_USERS_BY_START_APP_ID = "/user/findUsersByStartAppId";
	
	/** The create user. */
	String CREATE_USER = "/user/caxstartbatch/save";
	
	/** The create project app. */
	String CREATE_PRPJECTAPP = "/projectApplication/caxstartbatch/save";
	
	/** The create base app. */
	String CREATE_BASEAPP = "/baseApplication/caxstartbatch/save";
	
	/** The create start app. */
	String CREATE_STARTAPP = "/startApplication/caxstartbatch/save";
	
	/** The create user app. */
	String CREATE_USERAPP = "/userApplication/caxstartbatch/save";
	
	/** The create project. */
	String CREATE_PROJECT = "/project/caxstartbatch/save";
	
	/** The create adminarea. */
	String CREATE_ADMINAREA = "/area/caxstartbatch/save";
	
	/** The create site. */
	String CREATE_SITE = "/site/caxstartbatch/save";
	
	/** The update user. */
	String UPDATE_USER = "/user/caxstartbatch/update";
	
	/** The update site. */
	String UPDATE_SITE = "/site/caxstartbatch/update";
	
	/** The update adminarea. */
	String UPDATE_ADMINAREA = "/area/caxstartbatch/update";
	
	/** The update project. */
	String UPDATE_PROJECT = "/project/caxstartbatch/update";
	
	/** The update project app. */
	String UPDATE_PROJECTAPP = "/projectApplication/caxstartbatch/update";
	
	/** The update user app. */
	String UPDATE_USERAPP = "/userApplication/caxstartbatch/update";
	
	/** The update startapp. */
	String UPDATE_STARTAPP = "/startApplication/caxstartbatch/update";
	
	/** The update baseapp. */
	String UPDATE_BASEAPP = "/baseApplication/caxstartbatch/update";
	
	/** GET_ICON_BY_NAME constant. */
	String GET_ICONID_BY_NAME = "/icon/findIconIdByName";

	/** Delete user Constant. */
	String DELETE_USER = "/user/caxstartbatch/delete";
	
	/** Delete admin area Constant. */
	String DELETE_ADMIN_AREA = "/area/caxstartbatch/delete";
	
	/** Delete project Constant. */
	String DELETE_PROJECT = "/project/caxstartbatch/delete";
	
	/** Delete user app Constant. */
	String DELETE_USER_APP = "/userApplication/caxstartbatch/delete";
	
	/** Delete start app Constant. */
	String DELETE_START_APP = "/startApplication/caxstartbatch/delete";
	
	/** Delete project app Constant. */
	String DELETE_PROJECT_APP = "/projectApplication/caxstartbatch/delete";
	
	/** Delete base app Constant. */
	String DELETE_BASE_APP = "/baseApplication/caxstartbatch/delete";
	
	/** Delete site Constant. */
	String DELETE_SITE = "/site/caxstartbatch/delete";
	
	/** Get base app Constant. */
	String GET_BASE_APP_BY_NAME = "/baseApplication/findByName";
	
	/** Get base app Constant. */
	String GET_CAX_START_BATCH_BASE_APP_BY_NAME = "/baseApplication/caxStartBatchFindByName";
	
	/** The String. */
	String ADD_USER_TO_PROJECT = "/userProjectRel/caxstartbatch/save";
	
	/** The remove user from project. */
	String REMOVE_USER_FROM_PROJECT = "/userProjectRel/caxstartbatch/delete";
	
	/** The remove user from project. */
	String GET_RESULTS_FROM_QUERY = "/xmbatch/findResultSet";
	
	/** The get authentication response. */
    String GET_AUTHENTICATION_RESPONSE = "/auth/login";
}
