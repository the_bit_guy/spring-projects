package com.magna.batch.model;

/**
 * The Class UserData.
 */
public class UserData {

	/** The user name. */
	private String userName;
	
	/** The full name. */
	private String fullName;
	
	/** The email ID. */
	private String emailID;
	
	/** The ph number. */
	private String phNumber;
	
	/** The department. */
	private String department;
	
	/** The status. */
	private String status;
	
	/** The description. */
	private String description;
	
	/** The remarks. */
	private String remarks;
	
	/** The lang code. */
	private String lang_code;
	
	/** The icon name. */
	private String iconName;

	/**
	 * Instantiates a new user data.
	 */
	public UserData() {
		super();

	}

	/**
	 * Gets the user name.
	 *
	 * @return the user name
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * Sets the user name.
	 *
	 * @param userName the new user name
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * Gets the full name.
	 *
	 * @return the full name
	 */
	public String getFullName() {
		return fullName;
	}

	/**
	 * Sets the full name.
	 *
	 * @param fullName the new full name
	 */
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	/**
	 * Gets the email ID.
	 *
	 * @return the email ID
	 */
	public String getEmailID() {
		return emailID;
	}

	/**
	 * Sets the email ID.
	 *
	 * @param emailID the new email ID
	 */
	public void setEmailID(String emailID) {
		this.emailID = emailID;
	}

	/**
	 * Gets the ph number.
	 *
	 * @return the ph number
	 */
	public String getPhNumber() {
		return phNumber;
	}

	/**
	 * Sets the ph number.
	 *
	 * @param phNumber the new ph number
	 */
	public void setPhNumber(String phNumber) {
		this.phNumber = phNumber;
	}

	/**
	 * Gets the department.
	 *
	 * @return the department
	 */
	public String getDepartment() {
		return department;
	}

	/**
	 * Sets the department.
	 *
	 * @param department the new department
	 */
	public void setDepartment(String department) {
		this.department = department;
	}

	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 *
	 * @param status the new status
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description.
	 *
	 * @param description the new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Gets the remarks.
	 *
	 * @return the remarks
	 */
	public String getRemarks() {
		return remarks;
	}

	/**
	 * Sets the remarks.
	 *
	 * @param remarks the new remarks
	 */
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	/**
	 * Gets the lang code.
	 *
	 * @return the lang code
	 */
	public String getLang_code() {
		return lang_code;
	}

	/**
	 * Sets the lang code.
	 *
	 * @param lang_code the new lang code
	 */
	public void setLang_code(String lang_code) {
		this.lang_code = lang_code;
	}

	/**
	 * Gets the icon name.
	 *
	 * @return the icon name
	 */
	public String getIconName() {
		return iconName;
	}

	/**
	 * Sets the icon name.
	 *
	 * @param iconName the new icon name
	 */
	public void setIconName(String iconName) {
		this.iconName = iconName;
	}
}
