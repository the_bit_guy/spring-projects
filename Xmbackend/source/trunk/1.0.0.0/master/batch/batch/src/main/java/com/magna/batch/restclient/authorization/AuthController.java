package com.magna.batch.restclient.authorization;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.magna.batch.restclient.BatchRestController;
import com.magna.batch.restclient.exception.UnauthorizedAccessException;
import com.magna.batch.utils.BatchUtil;
import com.magna.batch.utils.RestClientConstants;
import com.magna.xmbackend.vo.user.AuthResponse;

/**
 * The Class AuthController.
 */
public class AuthController extends BatchRestController {

	/** The Constant LOGGER. */
	/*private static final Logger LOGGER = LoggerFactory.getLogger(AuthController.class);*/

	/**
	 * Instantiates a new auth controller.
	 */
	public AuthController() {
		super();
	}

	/**
	 * Method for Authorize login.
	 *
	 * @param encryptTicket {@link String}
	 * @param caxAppName {@link String}
	 * @return the auth response {@link AuthResponse}
	 */
	public AuthResponse authorizeLogin(final String encryptTicket, final String caxAppName) {
		String url = new String(this.serviceUrl + RestClientConstants.GET_AUTHENTICATION_RESPONSE);
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.set("TKT", encryptTicket);
			headers.set("APP_NAME", caxAppName);

			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);
			ResponseEntity<AuthResponse> responseEntity = restTemplate.exchange(url, HttpMethod.POST, requestEntity, AuthResponse.class);
			HttpStatus statusCode = responseEntity.getStatusCode();
			HttpHeaders headersFromBackend= responseEntity.getHeaders();
			String tkt = headersFromBackend.getFirst("TKT");
			BatchUtil.getInstance().setTicket(tkt);
			
			if (statusCode.is2xxSuccessful()) {
				return responseEntity.getBody();
			} else {
				//LOGGER.error("Error while calling authorizeLogin REST Service, returns the status code: ", //$NON-NLS-1$
						//statusCode.value());
			}
		} catch (RestClientException e) {
			if (e instanceof HttpClientErrorException) {
				HttpStatus statusCode = ((HttpClientErrorException) e).getStatusCode();
				if (statusCode.compareTo(HttpStatus.FORBIDDEN) == 0) {
					throw new UnauthorizedAccessException(e.getMessage());
				} else if (statusCode.compareTo(HttpStatus.NOT_FOUND) == 0) {
					System.out.println("ERROR: Server Not Found !");
					return null;
				}
			} else if (e instanceof HttpStatusCodeException) {
				//RestClientExpHandlerUtil.handleNoXMLObjectFoundException(LOGGER, (HttpStatusCodeException) e);
			} else if (e instanceof ResourceAccessException) {
				throw e;
			}
		} catch (IllegalArgumentException e) {
			System.out.println("ERROR: URI Error ! URI is not absolute");
			return null;
		} catch (Exception e) {
			//LOGGER.error("Error while calling XMBATCH authorizeLogin REST Service", e); //$NON-NLS-1$
			System.out.println("ERROR: Server not reachable !");
			return null;
		}
		return null;
	}
}
