package com.magna.batch.helper;

import java.io.IOException;

import org.apache.commons.cli.CommandLine;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.magna.batch.restclient.obj.adminarea.AdminAreaController;
import com.magna.batch.restclient.obj.baseapp.BaseAppController;
import com.magna.batch.restclient.obj.project.ProjectController;
import com.magna.batch.restclient.obj.projectapp.ProjectAppController;
import com.magna.batch.restclient.obj.site.SiteController;
import com.magna.batch.restclient.obj.startapp.StartAppController;
import com.magna.batch.restclient.obj.userapp.UserAppController;
import com.magna.batch.restclient.obj.users.UserController;
import com.magna.batch.restclient.rel.userproject.UserProjectRelController;
import com.magna.batch.utils.BatchUtil;
import com.magna.batch.utils.ObjectRelationType;
import com.magna.batch.utils.ObjectType;
import com.magna.xmbackend.vo.rel.UserProjectRelRequest;


/**
 * Class for Delete helper.
 *
 * @author Roshan.Ekka
 */
public class DeleteHelper {
	
	/** The Constant LOGGER. */
	//private static final Logger LOGGER = LoggerFactory.getLogger(DeleteHelper.class);
	
	/** Member variable 'this ref' for {@link DeleteHelper}. */
	private static DeleteHelper thisRef;
	

	/**
	 * Constructor for DeleteHelper Class.
	 */
	private DeleteHelper() {
		thisRef = this;
	}

	/**
	 * Gets the single instance of DeleteHelper.
	 *
	 * @return single instance of DeleteHelper
	 */
	public static DeleteHelper getInstance() {
		if (thisRef == null) {
			new DeleteHelper();
		}
		return thisRef;
	}

	/**
	 * @param deleteObjType
	 * @throws IOException 
	 * @throws JsonProcessingException 
	 */
	public void deleteObject(final ObjectType deleteObjType, final String deleteTblName, final CommandLine cmd) {
		deleteObj(cmd , deleteObjType, deleteTblName);
	}
	
	/**
	 * Method for Delete rel object.
	 *
	 * @param deleteObjType {@link ObjectType}
	 * @param deleteTblName {@link String}
	 * @param cmd {@link CommandLine}
	 * @throws JsonProcessingException the json processing exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public void deleteRelObject(final ObjectRelationType deleteRelObjType, final String deleteTblName, final CommandLine cmd){
		deleteRelObj(cmd , deleteRelObjType, deleteTblName);
	}
	
	/**
	 * Method for Delete rel obj.
	 *
	 * @param cmd {@link CommandLine}
	 * @param deleteObjType {@link ObjectType}
	 * @param deleteTblName {@link String}
	 */
	private void deleteRelObj(CommandLine cmd, ObjectRelationType deleteRelObjType, String deleteTblName) {
		Boolean deleteRelObj = false;
		String userName = null, projectName = null, relStringValue = "";
		String[] optionValues = cmd.getOptionValues("remove");
		if (optionValues.length > 0) {
			relStringValue = BatchUtil.getInstance().getRelStringValue(optionValues, cmd, deleteRelObjType);
			if(relStringValue==null)return;
			String[] splittedStrVal = relStringValue.split("\"");
			String value;
			for (int i = 0; i < splittedStrVal.length; i++) {
				value = splittedStrVal[i];
				value = value.trim();
				if (value.split("=")[0].trim().equalsIgnoreCase("USER") && !value.endsWith("=")) {
					if (value.contains("=")) {
						userName = value.split("=", 2)[1];
					} else {
						System.out.println("ERROR: Syntax error !");
						return;
					}
					continue;
				} else if (value.split("=")[0].trim().equalsIgnoreCase("USER") && value.endsWith("=")) {
					userName="";
					continue;
				} else if(value.startsWith("USER") || value.startsWith("user")){
					System.out.println("ERROR: Syntax error !");
					return;
				}
				
				if (value.split("=")[0].trim().equalsIgnoreCase("PROJECT") && !value.endsWith("=")) {
					if (value.contains("=")) {
						projectName = value.split("=", 2)[1];
					} else {
						System.out.println("ERROR: Syntax error !");
						return;
					}
					continue;
				} else if (value.split("=")[0].trim().equalsIgnoreCase("PROJECT") && value.endsWith("=")) {
					projectName="";
					continue;
				} else if(value.startsWith("PROJECT") || value.startsWith("project")){
					System.out.println("ERROR: Syntax error !");
					return;
				} 
				
			/*for (int index = 0; index < optionValues.length; index++) {
					String copyStrVal=optionValues[index];
					strValue = strValue.concat(copyStrVal);
			}
			int flag= 0;
			for(int index=0; index<strValue.length(); index++){
				char str='\'';
				char charAt = strValue.charAt(index);
				if(charAt==str){
					flag = flag+1;
					if(flag>=2 && flag%2==0){
						StringBuilder stb = new StringBuilder(strValue);
						stb.setCharAt(index, ' ');
						strValue = stb.toString().trim();
					}
				}
			}
			strValue = strValue.replace("'", "");

			String[] splittedStrVal = strValue.split(" ");
			String value;
			for (int i = 0; i < splittedStrVal.length; i++) {
				value=splittedStrVal[i];
				
				if (value.contains("user=") || value.contains("USER=")) {
					userName = value.split("=")[1];
					continue;
				}
				if (value.contains("project=")|| value.contains("PROJECT=")) {
					projectName = value.split("=")[1];
					continue;
				}*/
			}
		}
		if (BatchUtil.isEmpty(userName)) {
			System.out.println("user name is required");
			return ; // Error incorrect syntax mandatory fields not
					// provided
		} else if(BatchUtil.isEmpty(projectName) ){
			System.out.println("project name is required");
			return;
		}
		
		UserProjectRelRequest userProjectRelRequest = new UserProjectRelRequest();
		userProjectRelRequest.setUserName(userName);
		userProjectRelRequest.setProjectName(projectName);
		UserProjectRelController userProjRelCtrl = new UserProjectRelController();
		deleteRelObj = userProjRelCtrl.removeUserFromProject(userProjectRelRequest);
			if(deleteRelObj){
				System.out.println("user_project relation removed successfully");
			}
	}

	/**
	 * Method for Delete user.
	 *
	 * @param cmd {@link CommandLine}
	 * @throws JsonProcessingException the json processing exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	private void deleteObj(final CommandLine cmd, ObjectType deleteObjType, final String deleteTblName) {
		String name = "";
		Boolean isDeleted = null;
		if (cmd.hasOption("where")) {
			String[] optionValues = cmd.getOptionValues("where");
			if (optionValues.length > 0) {
				for (int index = 0; index < optionValues.length; index++) {
					String copyStrVal = optionValues[index];
					name = name.concat(copyStrVal);
				}
				int flag = 0;
				for (int index = 0; index < name.length(); index++) {
					char str = '\'';
					char charAt = name.charAt(index);
					if (charAt == str) {
						flag = flag + 1;
						if (flag >= 2 && flag % 2 == 0) {
							StringBuilder stb = new StringBuilder(name);
							stb.setCharAt(index, ' ');
							name = stb.toString().trim();
						}
					}
				}
				name = name.replace("'", "");
				name = name.replaceAll("name=", "");
				name = name.replace("NAME=", "");
			}
			String camelCaseTblName = BatchUtil.getInstance().toCamelCase(deleteTblName);
			if (!BatchUtil.isEmpty(name)) {
				switch (deleteObjType) {
				case USER:
					final UserController userController = new UserController();
					isDeleted = userController.deleteUser(name);
					break;
				case SITE:
					final SiteController siteController = new SiteController();
					isDeleted = siteController.deleteSite(name);
					break;
				case ADMINAREA:
					final AdminAreaController adminAreaController = new AdminAreaController();
					isDeleted = adminAreaController.deleteAdminArea(name);
					break;
				case PROJECT:
					final ProjectController projectController = new ProjectController();
					isDeleted = projectController.deleteProject(name);
					break;
				case USERAPP:
					final UserAppController userAppController = new UserAppController();
					isDeleted = userAppController.deleteUserApp(name);
					break;
				case BASEAPP:
					final BaseAppController baseAppController = new BaseAppController();
					isDeleted = baseAppController.deleteBaseApp(name);
					break;
				case PROJECTAPP:
					final ProjectAppController projectAppController = new ProjectAppController();
					isDeleted = projectAppController.deleteProjectApp(name);
					break;
				case STARTAPP:
					final StartAppController startAppController = new StartAppController();
					isDeleted = startAppController.deleteStartApp(name);
				default:
					break;
				}
				if (isDeleted == null || isDeleted == Boolean.FALSE) {
					return;
				} else {
					System.out.println(camelCaseTblName + " Deleted Successfully");
				}
			} else if (BatchUtil.isEmpty(name)) {
				System.out.println(camelCaseTblName + " name is required");
				return;
			}
		} else {
			System.out.println("ERROR: Syntax error !");
		}
	}
}
