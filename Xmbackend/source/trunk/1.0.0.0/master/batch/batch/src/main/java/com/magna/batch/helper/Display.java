package com.magna.batch.helper;

import java.awt.Desktop;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.apache.commons.cli.CommandLine;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.magna.batch.utils.CreateExcell;
import com.magna.batch.utils.ObjectType;

/**
 * Class for Display.
 *
 * @author Roshan.Ekka
 */
public class Display {
	
	/** Member variable 'entry size' for {@link Int}. */
	private int entrySize;
	private String separator;
	private int rowNum;
	private int colNum;
	private Cell cell = null;

	/**
	 * Constructor for Display Class.
	 *
	 * @param objectType {@link ObjectType}
	 */
	public Display() {
		this.entrySize = 1;
		this.rowNum = 0;
		this.colNum = 0;
		this.separator = System.getProperty("file.separator");
	}

	/**
	 * Method for Show on console.
	 *
	 * @param result {@link List<Map<String,Object>>}
	 */
	public void showResult(final List<Map<String, Object>> queryResultSet, final CommandLine cmd, final String tableName) {
		if (cmd.hasOption("USER_PROJECT_USAGE")) {
			String strVal = "";
			String year = null;
			if (cmd.hasOption("where")) {
				String[] optionValues = cmd.getOptionValues("where");
				if (optionValues.length > 0) {
					for (int index = 0; index < optionValues.length; index++) {
						String copyStrVal = optionValues[index];
						copyStrVal = copyStrVal.replace("'", "");
						copyStrVal = copyStrVal.replace(" ", "");
						copyStrVal = copyStrVal.trim();
						strVal = strVal.concat(copyStrVal);
					}
				}
			} else {
				return;
			}
			if (strVal.contains("year")) {
				year = strVal.split("=")[1];
			}
			File curFile = new File(".");
			String filepath = curFile.getAbsolutePath();
			int lastIndexOfDot = filepath.lastIndexOf(".");
			filepath = filepath.substring(0, lastIndexOfDot);
			filepath = filepath.concat("reports" + separator +tableName+".xlsx");
			CreateExcell.createExcellforUserCmds(separator, queryResultSet, "userprojectusage", filepath + "_"+year);
		} else if (cmd.hasOption("genexcel")) {
			generateExcel(queryResultSet, tableName, cmd);
		} else {
			showResult(queryResultSet);
		}
	}
	
	/**
	 * Method for Generate excel.
	 *
	 * @param queryResultSet {@link List<Map<String,Object>>}
	 * @param tableName {@link String}
	 */
	private void generateExcel(List<Map<String, Object>> queryResultSet, final String tableName, final CommandLine cmd) {
		XSSFWorkbook workbook = new XSSFWorkbook();
		XSSFSheet sheet;
		sheet = workbook.createSheet(tableName);
		Row row = sheet.createRow(rowNum++);
		for (Map<String, Object> resultMap : queryResultSet) {
			int size = resultMap.size();
			if(entrySize==1){
				keyToExcel(sheet, resultMap, size);
				row = valueToExcel(sheet, resultMap, size);
				entrySize++;
			}else{
				row = valueToExcel(sheet, resultMap, size);
			}
			cell = row.createCell(colNum++);
		}
		generateExcel(tableName, workbook, cmd);
	}

	/**
	 * Method for Key to excel.
	 *
	 * @param sheet {@link XSSFSheet}
	 * @param resultMap {@link Map<String,Object>}
	 * @param size {@link int}
	 */
	private void keyToExcel(XSSFSheet sheet, Map<String, Object> resultMap, int size) {
		Row row;
		row = sheet.createRow(rowNum++);
		Object[] array = resultMap.keySet().toArray(new Object[size]);
		colNum = 0;
		for (int index = 0; index < array.length; index++) {
			String value = (String) array[index];
			cell = row.createCell(colNum++);
			if (value instanceof String) {
				cell.setCellValue((String) value);
			} 
		}
	}

	/**
	 * Method for Value to excel.
	 *
	 * @param sheet {@link XSSFSheet}
	 * @param resultMap {@link Map<String,Object>}
	 * @param size {@link int}
	 * @return the row {@link Row}
	 */
	private Row valueToExcel(XSSFSheet sheet, Map<String, Object> resultMap, int size) {
		Row row;
		row = sheet.createRow(rowNum++);
		Object[] arrayValue = resultMap.values().toArray(new Object[size]);
		colNum = 0;
		for (int index = 0; index < arrayValue.length; index++) {
			String value = (String) arrayValue[index];
			cell = row.createCell(colNum++);
			if (value instanceof String) {
				cell.setCellValue((String) value);
			} 
		}
		return row;
	}

	/**
	 * @param tableName
	 * @param workbook
	 */
	private void generateExcel(final String tableName, XSSFWorkbook workbook, final CommandLine cmd) {
		File curFile = new File(".");
		String currentDirectory = curFile.getAbsolutePath();
		int lastIndexOfDot = currentDirectory.lastIndexOf(".");
		currentDirectory = currentDirectory.substring(0, lastIndexOfDot);
		currentDirectory = currentDirectory.concat("reports" + separator +tableName+".xlsx");
		String[] optionValues = cmd.getOptionValues("genexcel");
		if(optionValues!=null){
			if (optionValues.length <= 1) {
				currentDirectory = optionValues[0];
				currentDirectory = currentDirectory.replace("'", "");
				currentDirectory = currentDirectory.concat(separator+tableName+".xlsx");
			}else if(optionValues.length > 1){
				System.out.println("error in genExcel path");
				return;
			}
		}
		curFile = new File(currentDirectory);
		int lastIndexOf = currentDirectory.lastIndexOf(separator);
		String dirPath = currentDirectory.substring(0, lastIndexOf+1);
		String excelName = currentDirectory.substring(lastIndexOf+1, currentDirectory.length());
		String[] split = excelName.split("\\.");
		String name = split[0];
		String extn = split[1];
		int id = 1;
		while(curFile.exists()){
			System.out.println(currentDirectory + " exists");
			currentDirectory = dirPath + name + "_" + id + "." + extn;
			id++;
			curFile = new File(currentDirectory);
		} 
		curFile = new File(dirPath);
		if (!curFile.exists()) {
			curFile.mkdirs();
		}
		try {
			FileOutputStream outputStream = new FileOutputStream(currentDirectory);
			workbook.write(outputStream);
			workbook.close();
			System.out.println("Creating excel sheet file at : " + currentDirectory);
			Desktop.getDesktop().open(new File(currentDirectory));
		} catch (FileNotFoundException e) {
			System.out.println("ERROR: " + e.getMessage());
		} catch (IOException e) {
			System.out.println("ERROR: " + e.getMessage());
		} catch (Exception e) {
			System.out.println("ERROR: " + e.getMessage());

		}
	}

	/**
	 * Method for Show result.
	 *
	 * @param result {@link List<Map<String,Object>>}
	 */
	private void showResult(List<Map<String, Object>> result) {
		for (Map<String, Object> resultMap : result) {
			int size = resultMap.size();
			StringBuilder formatStb = new StringBuilder();
			String formatString;
			for (int resultMapSize = 0; resultMapSize < size; resultMapSize++) {
				formatStb.append(" ").append("%-35s");
			}
			formatString = formatStb.toString();
			if(entrySize==1){
				System.out.print(String.format(formatString+"\n",resultMap.keySet().toArray(new Object[size])));
				System.out.println();
				entrySize++;
			}
			
			System.out.print(String.format(formatString+"\n",resultMap.values().toArray(new Object[size])));
		}	
	}
}
