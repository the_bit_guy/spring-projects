package com.magna.batch.utils;


/**
 * Enum for Object type.
 *
 * @author Roshan.Ekka
 */
public enum ObjectType {

	/** Member variable 'site' for {@link ObjectType}. */
	SITE,
	
	/** Member variable 'administration area' for {@link ObjectType}. */
	ADMINAREA,
	
	/** Member variable 'project' for {@link ObjectType}. */
	PROJECT,
	
	/** Member variable 'user' for {@link ObjectType}. */
	USER,
	
	/** Member variable 'base application' for {@link ObjectType}. */
	BASEAPP,
	
	/** Member variable 'user application' for {@link ObjectType}. */
	USERAPP,
	
	/** Member variable 'project application' for {@link ObjectType}. */
	PROJECTAPP,
	
	/** Member variable 'start application' for {@link ObjectType}. */
	STARTAPP;
	
	/**
	 * Gets the object type.
	 *
	 * @return the object type
	 */
	public static String[] getObjectType() {
		ObjectType[] values = values();
		String[] returnArray = new String[values.length];
		for (int index = 0; index < values.length; index++) {
			returnArray[index] = values[index].name();
		}
		return returnArray;
	}

	/**
	 * Gets the object type.
	 *
	 * @param objectType {@link String}
	 * @return the object type
	 */
	public static ObjectType getObjectType(String objectType) {
        for (ObjectType value : values()) {
            if (objectType.equalsIgnoreCase(value.name())) {
                return value;
            }
        }
        return null;
    }
}
