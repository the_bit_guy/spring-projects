package com.magna.batch.model;

/**
 * Class for Site data.
 *
 * @author Roshan.Ekka
 */
public class UserProjectData {

	/** The user name. */
	private String userName;
	
	/** The project name. */
	private String projectName;
	
	/** The project exp days. */
	private String projectExpDays;
	
	/** The status. */
	private String status;
	
	/**
	 * Instantiates a new user data.
	 */
	public UserProjectData() {
		super();

	}

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @return the projectName
	 */
	public String getProjectName() {
		return projectName;
	}

	/**
	 * @param projectName the projectName to set
	 */
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	/**
	 * @return the projectExpDays
	 */
	public String getProjectExpDays() {
		return projectExpDays;
	}

	/**
	 * @param projectExpDays the projectExpDays to set
	 */
	public void setProjectExpDays(String projectExpDays) {
		this.projectExpDays = projectExpDays;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
}
