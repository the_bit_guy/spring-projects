/**
 * 
 */
package com.magna.xmbackend.audit.rel.mgr.impl;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.magna.xmbackend.audit.mgr.utils.AdminHistoryRelsUtil;
import com.magna.xmbackend.audit.rel.mgr.AdminAreaProjectAppAuditMgr;
import com.magna.xmbackend.entities.AdminAreaProjAppRelTbl;
import com.magna.xmbackend.entities.AdminAreaProjectRelTbl;
import com.magna.xmbackend.entities.AdminHistoryRelationsTbl;
import com.magna.xmbackend.entities.ProjectApplicationsTbl;
import com.magna.xmbackend.jpa.dao.ProjectApplicationJpaDao;
import com.magna.xmbackend.jpa.rel.dao.AdminAreaProjectAppRelJpaDao;
import com.magna.xmbackend.jpa.rel.dao.AdminAreaProjectRelJpaDao;
import com.magna.xmbackend.jpa.rel.dao.AdminHistoryRelationJpaDao;
import com.magna.xmbackend.vo.rel.AdminAreaProjectAppRelBatchRequest;
import com.magna.xmbackend.vo.rel.AdminAreaProjectAppRelBatchResponse;
import com.magna.xmbackend.vo.rel.AdminAreaProjectAppRelRequest;

/**
 * @author Bhabadyuti Bal
 *
 */
@Component
public class AdminAreaProjectAppAuditMgrImpl implements AdminAreaProjectAppAuditMgr {
	
	
	private static final Logger LOG = LoggerFactory.getLogger(AdminAreaProjectAppAuditMgrImpl.class);
	
	final String RELATION_NAME = "AdminAreaProjectApp";
	final String RESULT_SUCCESS = "Success";
	final String RESULT_FAILURE = "Failure";
	
	
	@Autowired
	private AdminHistoryRelsUtil adminHistoryRelsUtil;
	@Autowired
	private AdminHistoryRelationJpaDao adminHistoryRelationJpaDao;
	@Autowired
	private AdminAreaProjectRelJpaDao adminAreaProjectRelJpaDao;
	@Autowired
	private ProjectApplicationJpaDao projectAppJpaDao;
	@Autowired
	private AdminAreaProjectAppRelJpaDao adminAreaProjectAppRelJpaDao;
	
	
	@Override
	public void adminAreaProjectAppMultiSaveAuditor(
			AdminAreaProjectAppRelBatchRequest batchReq,
			AdminAreaProjectAppRelBatchResponse batchRes, HttpServletRequest httpServletRequest) {
		LOG.info(">>> adminAreaProjectAppMultiSaveAuditor");

		List<AdminAreaProjectAppRelRequest> adminAreaProjectAppRelRequests = batchReq.getAdminAreaProjectAppRelRequests();

		List<AdminAreaProjAppRelTbl> adminAreaProjectAppRelTbls = batchRes.getAdminAreaProjectAppRelTbls();
		List<Map<String, String>> statusMap = batchRes.getStatusMap();

		if (statusMap.isEmpty()) {
			// All Success
			this.createSuccessAudit(adminAreaProjectAppRelTbls, httpServletRequest);

		} else {
			//Partial Success case
			//Iterate the status map to log the failed ones
			this.createFailureAudit(statusMap, adminAreaProjectAppRelRequests, httpServletRequest);

			//Iterate the userProjectRelTbls for Success ones
			this.createSuccessAudit(adminAreaProjectAppRelTbls, httpServletRequest);
		}
		LOG.info("<<< adminAreaProjectAppMultiSaveAuditor");
		
	}

	private void createFailureAudit(List<Map<String, String>> statusMap,
			List<AdminAreaProjectAppRelRequest> adminAreaProjectAppRelRequests, HttpServletRequest httpServletRequest) {
		for (Map<String, String> map : statusMap) {
			for (AdminAreaProjectAppRelRequest adminAreaProjAppRelReq : adminAreaProjectAppRelRequests) {
				String adminAreaProjectRelId = adminAreaProjAppRelReq.getAdminAreaProjectRelId();
				String projectAppId = adminAreaProjAppRelReq.getProjectAppId();
				
				AdminAreaProjectRelTbl adminAreaProjectRelTbl = this.adminAreaProjectRelJpaDao.findOne(adminAreaProjectRelId);
				String adminAreaName = "";
				String projName = "";
				if(adminAreaProjectRelTbl != null) {
					adminAreaName = adminAreaProjectRelTbl.getSiteAdminAreaRelId().getAdminAreaId().getName();
					projName = adminAreaProjectRelTbl.getProjectId().getName();
				}
				
				ProjectApplicationsTbl projAppTbl = this.projectAppJpaDao.findOne(projectAppId);
				String projAppName = "";
				if(projAppTbl != null) {
					projAppName = projAppTbl.getName();
				}
					String message = map.get("en");
					if ( message.contains(projName) && message.contains(projAppName)) {
						AdminHistoryRelationsTbl adminHistoryRelationTbl = this.adminHistoryRelsUtil
								.makeAdminHistoryRelationTbl(httpServletRequest, RELATION_NAME,
										adminAreaName, projName,projAppName, 
										adminAreaProjAppRelReq.getRelationType(), null,
										null, message, "AdminAreaProjectApp Create", RESULT_FAILURE);
						this.adminHistoryRelationJpaDao.save(adminHistoryRelationTbl);
						break;
					}
			}
		}
		
	}

	private void createSuccessAudit(List<AdminAreaProjAppRelTbl> adminAreaProjectAppRelTbls,
			HttpServletRequest httpServletRequest) {
		
		adminAreaProjectAppRelTbls.forEach(adminAreaProjectAppRelTbl -> {
			String adminAreaName = adminAreaProjectAppRelTbl.getAdminAreaProjectRelId().getSiteAdminAreaRelId().getAdminAreaId().getName();
			String projectName = adminAreaProjectAppRelTbl.getAdminAreaProjectRelId().getProjectId().getName();
			String projAppName = adminAreaProjectAppRelTbl.getProjectApplicationId().getName();
			String relationType = adminAreaProjectAppRelTbl.getRelType();
			String status = adminAreaProjectAppRelTbl.getStatus();

			AdminHistoryRelationsTbl adminHistoryRelationTbl = this.adminHistoryRelsUtil
					.makeAdminHistoryRelationTbl(httpServletRequest, RELATION_NAME, adminAreaName,
							projectName, projAppName, relationType ,
							status, null, null, "AdminAreaProjectApp Create",
							RESULT_SUCCESS);
			this.adminHistoryRelationJpaDao.save(adminHistoryRelationTbl);
		});
		
	}

	@Override
	public void adminAreaProjectAppMultiSaveFailureAuditor(HttpServletRequest httpServletRequest, Exception ex) {
		AdminHistoryRelationsTbl adminHistoryRelationTbl = this.adminHistoryRelsUtil.makeAdminHistoryRelationTbl(
				httpServletRequest, RELATION_NAME, null, null, null, null, null, null, ex.getMessage(), "AdminAreaProjectApp Create",
				RESULT_FAILURE);
		this.adminHistoryRelationJpaDao.save(adminHistoryRelationTbl);
	}
	
	
	@Override
	public void adminAreaProjectAppUpdateFailureAuditor(HttpServletRequest httpServletRequest, Exception ex) {
		AdminHistoryRelationsTbl adminHistoryRelationTbl = this.adminHistoryRelsUtil.makeAdminHistoryRelationTbl(
				httpServletRequest, RELATION_NAME, null, null, null, null, null, null, ex.getMessage(), "AdminAreaProjectApp Update",
				RESULT_FAILURE);
		this.adminHistoryRelationJpaDao.save(adminHistoryRelationTbl);
	}

	@Override
	public void adminAreaProjectAppMultiDeleteAuditor(AdminAreaProjAppRelTbl adminAreaProjAppRelTbl,
			HttpServletRequest httpServletRequest) {

		String adminAreaName = adminAreaProjAppRelTbl.getAdminAreaProjectRelId().getSiteAdminAreaRelId().getAdminAreaId().getName();
		String projectName = adminAreaProjAppRelTbl.getAdminAreaProjectRelId().getProjectId().getName();
		String projAppName = adminAreaProjAppRelTbl.getProjectApplicationId().getName();
		String relationType = adminAreaProjAppRelTbl.getRelType();
		String status = adminAreaProjAppRelTbl.getStatus();

		AdminHistoryRelationsTbl adminHistoryRelationTbl = this.adminHistoryRelsUtil
				.makeAdminHistoryRelationTbl(httpServletRequest, RELATION_NAME, adminAreaName,
						projectName, projAppName, relationType ,
						status, null, null, "AdminAreaProjectApp Delete",
						RESULT_SUCCESS);
		this.adminHistoryRelationJpaDao.save(adminHistoryRelationTbl);
	
		
	}

	@Override
	public void adminAreaProjectAppUpdateAuditor(boolean updateStatusById, String status, String id,
			HttpServletRequest httpServletRequest) {
		if (updateStatusById) {
			AdminAreaProjAppRelTbl adminAreaProjAppRelTbl = this.adminAreaProjectAppRelJpaDao.findOne(id);
			if (adminAreaProjAppRelTbl != null) {
				String adminAreaName = adminAreaProjAppRelTbl.getAdminAreaProjectRelId().getSiteAdminAreaRelId()
						.getAdminAreaId().getName();
				String projectName = adminAreaProjAppRelTbl.getAdminAreaProjectRelId().getProjectId().getName();
				String projAppName = adminAreaProjAppRelTbl.getProjectApplicationId().getName();
				AdminHistoryRelationsTbl adminHistoryRelationTbl = this.adminHistoryRelsUtil
						.makeAdminHistoryRelationTbl(httpServletRequest, RELATION_NAME, adminAreaName,
								projectName, projAppName, null, status, null, null, "AdminAreaProjectApp status update",
								RESULT_SUCCESS);
				this.adminHistoryRelationJpaDao.save(adminHistoryRelationTbl);
			}

		}
	}

	@Override
	public void adminAreaProjectAppRelUpdateAuditor(AdminAreaProjectAppRelBatchResponse batchResp,
			HttpServletRequest httpServletRequest) {
		List<AdminAreaProjAppRelTbl> adminAreaProjectAppRelTbls = batchResp.getAdminAreaProjectAppRelTbls();
		adminAreaProjectAppRelTbls.forEach(adminAreaProjectAppRelTbl -> {
			
			String adminAreaName = adminAreaProjectAppRelTbl.getAdminAreaProjectRelId().getSiteAdminAreaRelId().getAdminAreaId().getName();
			String projectName = adminAreaProjectAppRelTbl.getAdminAreaProjectRelId().getProjectId().getName();
			String projAppName = adminAreaProjectAppRelTbl.getProjectApplicationId().getName();
			String relType = adminAreaProjectAppRelTbl.getRelType();
			String status = adminAreaProjectAppRelTbl.getStatus();
			AdminHistoryRelationsTbl adminHistoryRelationTbl = this.adminHistoryRelsUtil
					.makeAdminHistoryRelationTbl(httpServletRequest, RELATION_NAME, adminAreaName,
							projectName, projAppName, relType ,
							status, null, null, "AdminAreaProjectApp relationType update",
							RESULT_SUCCESS);
			this.adminHistoryRelationJpaDao.save(adminHistoryRelationTbl);
		});
	}

	@Override
	public void adminAreaProjectAppStatusUpdateAuditor(boolean updateStatusById, String status, String id,
			HttpServletRequest httpServletRequest) {
		if (updateStatusById) {
			AdminAreaProjAppRelTbl adminAreaProjAppRelTbl = this.adminAreaProjectAppRelJpaDao.findOne(id);
			if (adminAreaProjAppRelTbl != null) {
				String adminAreaName = adminAreaProjAppRelTbl.getAdminAreaProjectRelId().getSiteAdminAreaRelId()
						.getAdminAreaId().getName();
				String projectName = adminAreaProjAppRelTbl.getAdminAreaProjectRelId().getProjectId().getName();
				String projAppName = adminAreaProjAppRelTbl.getProjectApplicationId().getName();
				String relType = adminAreaProjAppRelTbl.getRelType();
				AdminHistoryRelationsTbl adminHistoryRelationTbl = this.adminHistoryRelsUtil
						.makeAdminHistoryRelationTbl(httpServletRequest, RELATION_NAME, adminAreaName,
								projectName, projAppName, relType, status, null, null, "AdminAreaProjectApp status update",
								RESULT_SUCCESS);
				this.adminHistoryRelationJpaDao.save(adminHistoryRelationTbl);
			}

		}
		
	}

	@Override
	public void adminAreaProjectAppStatusUpdateFailureAuditor(String id, String status, Exception ex,
			HttpServletRequest httpServletRequest) {

		AdminAreaProjAppRelTbl adminAreaProjAppRelTbl = this.adminAreaProjectAppRelJpaDao.findOne(id);
		if (adminAreaProjAppRelTbl != null) {
			String adminAreaName = adminAreaProjAppRelTbl.getAdminAreaProjectRelId().getSiteAdminAreaRelId()
					.getAdminAreaId().getName();
			String projectName = adminAreaProjAppRelTbl.getAdminAreaProjectRelId().getProjectId().getName();
			String projAppName = adminAreaProjAppRelTbl.getProjectApplicationId().getName();
			String errorMsg = ex.getMessage();
			AdminHistoryRelationsTbl adminHistoryRelationTbl = this.adminHistoryRelsUtil
					.makeAdminHistoryRelationTbl(httpServletRequest, RELATION_NAME, adminAreaName,
							projectName, projAppName, status, null, null, errorMsg, "AdminAreaProjectApp status update",
							RESULT_FAILURE);
			this.adminHistoryRelationJpaDao.save(adminHistoryRelationTbl);
		}
	}

}
