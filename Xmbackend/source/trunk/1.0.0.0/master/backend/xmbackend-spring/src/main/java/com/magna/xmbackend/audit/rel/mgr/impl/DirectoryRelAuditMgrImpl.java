/**
 * 
 */
package com.magna.xmbackend.audit.rel.mgr.impl;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.magna.xmbackend.audit.mgr.utils.AdminHistoryRelsUtil;
import com.magna.xmbackend.audit.rel.mgr.DirectoryRelAuditMgr;
import com.magna.xmbackend.entities.AdminHistoryRelationsTbl;
import com.magna.xmbackend.entities.DirectoryRefTbl;
import com.magna.xmbackend.entities.DirectoryTbl;
import com.magna.xmbackend.entities.ProjectApplicationsTbl;
import com.magna.xmbackend.entities.ProjectsTbl;
import com.magna.xmbackend.entities.UserApplicationsTbl;
import com.magna.xmbackend.entities.UsersTbl;
import com.magna.xmbackend.jpa.dao.DirectoryJpaDao;
import com.magna.xmbackend.jpa.dao.ProjectApplicationJpaDao;
import com.magna.xmbackend.jpa.dao.ProjectJpaDao;
import com.magna.xmbackend.jpa.dao.UserApplicationJpaDao;
import com.magna.xmbackend.jpa.dao.UserJpaDao;
import com.magna.xmbackend.jpa.rel.dao.AdminHistoryRelationJpaDao;
import com.magna.xmbackend.vo.directory.DirectoryRelBatchRequest;
import com.magna.xmbackend.vo.directory.DirectoryRelBatchResponse;
import com.magna.xmbackend.vo.directory.DirectoryRelRequest;
import com.magna.xmbackend.vo.enums.DirectoryObjectType;

/**
 * @author Bhabadyuti Bal
 *
 */

@Component
public class DirectoryRelAuditMgrImpl implements DirectoryRelAuditMgr {
	
	 private static final Logger LOG = LoggerFactory.getLogger(DirectoryRelAuditMgrImpl.class);
	 
	 @Autowired
	 private AdminHistoryRelationJpaDao adminHistoryRelationJpaDao;
	 @Autowired
	 private AdminHistoryRelsUtil adminHistoryRelsUtil;
	 @Autowired
	 private UserJpaDao userJpaDao;
	 @Autowired
	 private ProjectJpaDao projectJpaDao;
	 @Autowired
	 private UserApplicationJpaDao userApplicationJpaDao;
	 @Autowired
	 private ProjectApplicationJpaDao projectApplicationJpaDao;
	 @Autowired
	 private DirectoryJpaDao directoryJpaDao;

	@Override
	public void dirRelMultiSaveAuditor(DirectoryRelBatchRequest drbReq,
			DirectoryRelBatchResponse drbRes, HttpServletRequest httpServletRequest) {
		LOG.info(">>> userUserAppMultiSaveAuditor");

		List<DirectoryRelRequest> directoryRelRequests = drbReq.getDirectoryRelRequests();

		List<DirectoryRefTbl> directoryRefTbls = drbRes.getDirectoryRefTbls();
		List<Map<String, String>> statusMap = drbRes.getStatusMaps();

		if (statusMap.isEmpty()) {
			// All success
			this.createSuccessAudit(directoryRefTbls, httpServletRequest);

		} else {
			//Partial success case
			//Iterate the status map to log the failed ones
			this.createFailureAudit(statusMap, directoryRelRequests, httpServletRequest);

			//Iterate the userProjectRelTbls for success ones
			this.createSuccessAudit(directoryRefTbls, httpServletRequest);
		}
		LOG.info("<<< userProjectAppMultiSaveAuditor");
		
	}

	private void createFailureAudit(List<Map<String, String>> statusMap, List<DirectoryRelRequest> directoryRelRequests,
			HttpServletRequest httpServletRequest) {
		for (Map<String, String> map : statusMap) {
			for (DirectoryRelRequest directoryRelRequest : directoryRelRequests) {
				String objectId = directoryRelRequest.getObjectId();
				String objectType = directoryRelRequest.getObjectType().name();
				String dirId = directoryRelRequest.getDirectoryId();
				DirectoryTbl dirTbl = this.directoryJpaDao.findOne(dirId);
				String objectName = null;
				if (objectType.equals(DirectoryObjectType.USER.name())) {
					UsersTbl usersTbl = this.userJpaDao.findOne(objectId);
					objectName = usersTbl.getUsername();
				} else if (objectType.equals(DirectoryObjectType.PROJECT.name())) {
					ProjectsTbl projectsTbl = this.projectJpaDao.findOne(objectId);
					objectName = projectsTbl.getName();
				} else if (objectType.equals(DirectoryObjectType.USERAPPLICATION.name())) {
					UserApplicationsTbl userAppTbl = this.userApplicationJpaDao.findOne(objectId);
					objectName = userAppTbl.getName();
				} else if (objectType.equals(DirectoryObjectType.PROJECTAPPLICATION.name())) {
					ProjectApplicationsTbl projAppTbl = this.projectApplicationJpaDao.findOne(objectId);
					objectName = projAppTbl.getName();
				}

				String message = map.get("en");
				if (message.contains(dirTbl.getName()) && message.contains(objectName)) {
					AdminHistoryRelationsTbl adminHistoryRelationTbl = this.adminHistoryRelsUtil
							.makeAdminHistoryRelationTbl(httpServletRequest, "DirectoryRelation", dirTbl.getName(),
									objectType, objectName, null, null, null, message, "Directory Relation Create",
									"Failure");
					this.adminHistoryRelationJpaDao.save(adminHistoryRelationTbl);
					break;
				}
			}
		}

	}

	private void createSuccessAudit(List<DirectoryRefTbl> directoryRefTbls, HttpServletRequest httpServletRequest) {
		directoryRefTbls.forEach(directoryRefTbl -> {
			String objectId = directoryRefTbl.getObjectId();
			String objectType = directoryRefTbl.getObjectType();
			String dirName = directoryRefTbl.getDirectoryId().getName();
			String objectName = null;
			String dirRelType = null;
			if(objectType.equals(DirectoryObjectType.USER.name())) {
				UsersTbl usersTbl = this.userJpaDao.findOne(objectId);
				objectName = usersTbl.getUsername();
				dirRelType = DirectoryObjectType.USER.name();
			} else if(objectType.equals(DirectoryObjectType.PROJECT.name())) {
				ProjectsTbl projectsTbl = this.projectJpaDao.findOne(objectId);
				objectName = projectsTbl.getName();
				dirRelType = DirectoryObjectType.PROJECT.name();
			} else if(objectType.equals(DirectoryObjectType.USERAPPLICATION.name())) {
				UserApplicationsTbl userAppTbl = this.userApplicationJpaDao.findOne(objectId);
				objectName = userAppTbl.getName();
				dirRelType = DirectoryObjectType.USERAPPLICATION.name();
			} else if(objectType.equals(DirectoryObjectType.PROJECTAPPLICATION.name())) {
				ProjectApplicationsTbl projAppTbl = this.projectApplicationJpaDao.findOne(objectId);
				objectName = projAppTbl.getName();
				dirRelType = DirectoryObjectType.PROJECTAPPLICATION.name();
			}
			

			AdminHistoryRelationsTbl adminHistoryRelationTbl = this.adminHistoryRelsUtil
					.makeAdminHistoryRelationTbl(httpServletRequest, "DirectoryRelation", dirName,
							dirRelType, objectName, null,
							null, null, null, "Directory Relation Create",
							"Success");
			this.adminHistoryRelationJpaDao.save(adminHistoryRelationTbl);
		});
		
	}

	@Override
	public void dirRelMultiSaveFailureAuditor(HttpServletRequest httpServletRequest, Exception ex) {
		LOG.info(">>> dirRelMultiSaveFailureAuditor");
		AdminHistoryRelationsTbl adminHistoryRelationTbl = this.adminHistoryRelsUtil.makeAdminHistoryRelationTbl(
				httpServletRequest, "DirectoryRel", null, null, null, null, null, null, ex.getMessage(), "DirectoryRelation Create",
				"Failure");
		this.adminHistoryRelationJpaDao.save(adminHistoryRelationTbl);
		LOG.info("<<< dirRelMultiSaveFailureAuditor");
		
	}

	@Override
	public void directoryRelMultiDeleteSuccessAudit(DirectoryRefTbl directoryRefTbl,
			HttpServletRequest httpServletRequest) {

		String objectId = directoryRefTbl.getObjectId();
		String objectType = directoryRefTbl.getObjectType();
		String dirName = directoryRefTbl.getDirectoryId().getName();
		String objectName = null;
		String dirRelType = null;
		if(objectType.equals(DirectoryObjectType.USER.name())) {
			UsersTbl usersTbl = this.userJpaDao.findOne(objectId);
			objectName = usersTbl.getUsername();
			dirRelType = DirectoryObjectType.USER.name();
		} else if(objectType.equals(DirectoryObjectType.PROJECT.name())) {
			ProjectsTbl projectsTbl = this.projectJpaDao.findOne(objectId);
			objectName = projectsTbl.getName();
			dirRelType = DirectoryObjectType.PROJECT.name();
		} else if(objectType.equals(DirectoryObjectType.USERAPPLICATION.name())) {
			UserApplicationsTbl userAppTbl = this.userApplicationJpaDao.findOne(objectId);
			objectName = userAppTbl.getName();
			dirRelType = DirectoryObjectType.USERAPPLICATION.name();
		} else if(objectType.equals(DirectoryObjectType.PROJECTAPPLICATION.name())) {
			ProjectApplicationsTbl projAppTbl = this.projectApplicationJpaDao.findOne(objectId);
			objectName = projAppTbl.getName();
			dirRelType = DirectoryObjectType.PROJECTAPPLICATION.name();
		}
		

		AdminHistoryRelationsTbl adminHistoryRelationTbl = this.adminHistoryRelsUtil
				.makeAdminHistoryRelationTbl(httpServletRequest, "DirectoryRelation", dirName,
						dirRelType, objectName, null,
						null, null, null, "DirectoryRelation Delete",
						"Success");
		this.adminHistoryRelationJpaDao.save(adminHistoryRelationTbl);
		
	}
	 
	 

}
