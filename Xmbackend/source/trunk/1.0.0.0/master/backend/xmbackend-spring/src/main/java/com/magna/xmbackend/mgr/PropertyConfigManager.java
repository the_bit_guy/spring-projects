/**
 * 
 */
package com.magna.xmbackend.mgr;

import com.magna.xmbackend.vo.propConfig.PropertyConfigResponse;

/**
 * @author Bhabadyuti Bal
 *
 */
public interface PropertyConfigManager {

	public PropertyConfigResponse findLdapDetails();

	public PropertyConfigResponse findSmtpDetails();
	
	public PropertyConfigResponse findSingletonDetails();
	
	public PropertyConfigResponse findProjectPopupDetails();
	
	public PropertyConfigResponse findProjectExpiryDetails();

	PropertyConfigResponse updatePropertyConfigDetails(Object object);
}
