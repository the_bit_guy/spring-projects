package com.magna.xmbackend.rel.controller;

import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.magna.xmbackend.audit.rel.mgr.DirectoryRelAuditMgr;
import com.magna.xmbackend.mgr.DirectoryRelationManager;
import com.magna.xmbackend.vo.directory.DirectoryRelBatchRequest;
import com.magna.xmbackend.vo.directory.DirectoryRelBatchResponse;
import com.magna.xmbackend.vo.directory.DirectoryRelResponse;
import com.magna.xmbackend.vo.directory.DirectoryRelResponseWrapper;

/**
 * @author Bhabadyuti Bal
 *
 */
@RestController
@RequestMapping(value = "/directoryRelation")
public class DirectoryRelationController {

    private static final Logger LOG = LoggerFactory.getLogger(DirectoryRelationController.class);

    @Autowired
    DirectoryRelationManager dirRelationManager;
    @Autowired
    private DirectoryRelAuditMgr directoryRelAuditMgr;

    /**
     *
     * @param httpServletRequest
     * @param directoryRelBatchRequest
     * @return ResponseEntity
     */
    @RequestMapping(value = "/multi/save",
            method = RequestMethod.POST,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<DirectoryRelBatchResponse> batchSave(
            HttpServletRequest httpServletRequest,
            @RequestBody final DirectoryRelBatchRequest directoryRelBatchRequest) {
        LOG.info("> batchSave");
        final String userName = (String) httpServletRequest.getAttribute("userName");
        LOG.info("userName={}", userName);
        DirectoryRelBatchResponse drbr = null;
		try {
			drbr = dirRelationManager.createBatch(directoryRelBatchRequest, userName);
			this.directoryRelAuditMgr.dirRelMultiSaveAuditor(directoryRelBatchRequest, drbr, httpServletRequest);
		} catch (Exception ex) {
			this.directoryRelAuditMgr.dirRelMultiSaveFailureAuditor(httpServletRequest, ex);
		}
        LOG.info("< batchSave");
        return new ResponseEntity<>(drbr, HttpStatus.ACCEPTED);
    }

    /**
     *
     * @param httpServletRequest
     * @param directoryRefIds
     * @return Boolean
     */
    @RequestMapping(value = "/multi/delete",
            method = RequestMethod.POST,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<Boolean> multiDelete(
            HttpServletRequest httpServletRequest,
            @RequestBody final Set<String> directoryRefIds) {
        LOG.info("> multiDelete");
        final Boolean isDeleted = dirRelationManager.multiDelete(directoryRefIds, httpServletRequest);
        LOG.info("< multiDelete");
        return new ResponseEntity<>(isDeleted, HttpStatus.OK);
    }

    /**
     *
     * @param httpServletRequest
     * @param directoryId
     * @param objectType
     * @return DirectoryRelResponseWrapper
     */
    @RequestMapping(value = "/findDirectoryRelByDirIdAndObjectType/{directoryId}/{objectType}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<DirectoryRelResponseWrapper> findDirectoryRelByDirIdAndObjectType(
            HttpServletRequest httpServletRequest,
            @PathVariable String directoryId, @PathVariable String objectType) {
        LOG.info("> findDirectoryRelByDirIdAndObjectType");
        final String userName = (String) httpServletRequest.getAttribute("userName");
        LOG.info("userName={}", userName);
        final List<DirectoryRelResponse> directoryRelResponses
                = this.dirRelationManager.findDirectoryRelByDirIdAndObjectType(directoryId, objectType, userName);
        final DirectoryRelResponseWrapper drrw = new DirectoryRelResponseWrapper(directoryRelResponses);
        LOG.info("< findDirectoryRelByDirIdAndObjectType");
        return new ResponseEntity<>(drrw, HttpStatus.OK);
    }
}
