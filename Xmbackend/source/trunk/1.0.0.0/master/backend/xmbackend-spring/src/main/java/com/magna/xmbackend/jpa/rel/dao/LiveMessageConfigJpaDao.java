/**
 * 
 */
package com.magna.xmbackend.jpa.rel.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.magna.xmbackend.entities.LiveMessageConfigTbl;
import com.magna.xmbackend.entities.LiveMessageTbl;

/**
 * @author Bhabadyuti Bal
 *
 */
public interface LiveMessageConfigJpaDao extends CrudRepository<LiveMessageConfigTbl, String>{

	List<LiveMessageConfigTbl> findByLiveMessageId(LiveMessageTbl liveMessageTbl);
	
	@Query("from LiveMessageConfigTbl LMCT where LMCT.siteId=:siteId and LMCT.userId=(select UT.userId from UsersTbl UT where UT.username=:username)")
	LiveMessageConfigTbl findBySiteIdAnduserId(@Param("siteId") String siteId, @Param("username") String username);
	
	@Query("from LiveMessageConfigTbl LMCT where LMCT.siteId=:siteId and LMCT.adminAreaId is null and LMCT.projectId is null and LMCT.userId is null")
	List<LiveMessageConfigTbl> findBySiteId(@Param("siteId") String siteId);
	
	
	@Query("from LiveMessageConfigTbl LMCT where LMCT.siteId is null and LMCT.adminAreaId is null and LMCT.projectId is null and LMCT.userId=:userId")
	List<LiveMessageConfigTbl> findByUserId(@Param("userId") String userId);
	
	@Query("from LiveMessageConfigTbl LMCT where LMCT.siteId is null and LMCT.adminAreaId is null and LMCT.projectId=:projectId and LMCT.userId is null")
	List<LiveMessageConfigTbl> findByProjectId(@Param("projectId") String projectId);
	
	
	@Query("from LiveMessageConfigTbl LMCT where LMCT.siteId=:siteId and LMCT.adminAreaId=:adminAreaId  and LMCT.projectId is null and LMCT.userId is null")
	List<LiveMessageConfigTbl> findBySiteIdAndAdminAreaId(@Param("siteId") String siteId, @Param("adminAreaId") String adminAreaId);
	
	@Query("from LiveMessageConfigTbl LMCT where LMCT.siteId=:siteId and LMCT.adminAreaId=:adminAreaId and LMCT.projectId=:projectId and LMCT.userId is null")
	List<LiveMessageConfigTbl> findBySiteIdAndAdminAreaIdAndProjectId(@Param("siteId") String siteId, @Param("adminAreaId") String adminAreaId, @Param("projectId") String projectId);
	
	/*@Query("from LiveMessageConfigTbl LMCT where LMCT.siteId=:siteId and LMCT.adminAreaId is null and LMCT.projectId is null and LMCT.userId is null")
	List<LiveMessageConfigTbl> findBySiteIdAndAdminAreaIdAndProjectIdAndUserId(@Param("siteId") String siteId);*/
	
	
	//List<LiveMessageConfigTbl> findBySiteId(String siteId);
	
	List<LiveMessageConfigTbl> findBySiteIdAndUserId(String siteId, String userId);
	
	//List<LiveMessageConfigTbl> findByUserId(String userId);
	
	//List<LiveMessageConfigTbl> findByProjectId(String projectId);
	
	List<LiveMessageConfigTbl> findByProjectIdAndUserId(String projectId, String userId);
	
	//List<LiveMessageConfigTbl> findBySiteIdAndAdminAreaId(String siteId, String adminAreaId);
	
	List<LiveMessageConfigTbl> findBySiteIdAndAdminAreaIdAndUserId(String siteId, String adminAreaId, String userId);
	
	//List<LiveMessageConfigTbl> findBySiteIdAndAdminAreaIdAndProjectId(String siteId, String adminAreaId, String projectId);
	
}
