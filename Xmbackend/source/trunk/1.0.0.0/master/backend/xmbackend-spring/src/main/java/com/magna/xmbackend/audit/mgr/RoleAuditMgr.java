package com.magna.xmbackend.audit.mgr;

import com.magna.xmbackend.entities.RolesTbl;
import com.magna.xmbackend.vo.roles.RoleRequest;
import com.magna.xmbackend.vo.roles.RoleResponse;
import java.util.List;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author Admin
 */
public interface RoleAuditMgr {

    /**
     *
     * @param roleResponse
     * @param httpServletRequest
     */
    void roleCreateSuccessAuditor(final RoleResponse roleResponse,
            final HttpServletRequest httpServletRequest);

    /**
     *
     * @param roleRequestList
     * @param httpServletRequest
     */
    void roleCreateFailureAuditor(final List<RoleRequest> roleRequestList,
            final HttpServletRequest httpServletRequest);

    /**
     *
     * @param roleResponse
     * @param servletRequest
     */
    void roleUpdateSuccessAuditor(final RoleResponse roleResponse,
            final HttpServletRequest servletRequest);

    /**
     *
     * @param roleRequestList
     * @param httpServletRequest
     */
    void roleUpdateFailureAuditor(final List<RoleRequest> roleRequestList,
            final HttpServletRequest httpServletRequest);

    /**
     *
     * @param roleId
     * @param httpServletRequest
     * @param isSuccess
     * @param rolesTbl
     */
    void roleDeleteStatusAuditor(final String roleId,
            final HttpServletRequest httpServletRequest, 
            final boolean isSuccess, final RolesTbl rolesTbl);

    /**
     *
     * @param roleResponse
     * @param roleIds
     * @param httpServletRequest
     */
    void roleMultiDeleteAuditor(final RoleResponse roleResponse,
            final Set<String> roleIds, final HttpServletRequest httpServletRequest);
}
