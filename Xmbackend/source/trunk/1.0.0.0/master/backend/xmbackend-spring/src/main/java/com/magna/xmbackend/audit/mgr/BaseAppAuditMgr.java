package com.magna.xmbackend.audit.mgr;

import com.magna.xmbackend.entities.BaseApplicationsTbl;
import com.magna.xmbackend.vo.baseApplication.BaseApplicationRequest;
import com.magna.xmbackend.vo.baseApplication.BaseApplicationResponse;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author Admin
 */
public interface BaseAppAuditMgr {

    /**
     *
     * @param baseApplicationsTbl
     * @param httpServletRequest
     */
    void baseAppCreateSuccessAuditor(final BaseApplicationsTbl baseApplicationsTbl,
            final HttpServletRequest httpServletRequest);

    /**
     *
     * @param baseApplicationRequest
     * @param httpServletRequest
     */
    void baseAppCreateFailureAuditor(final BaseApplicationRequest baseApplicationRequest,
            final HttpServletRequest httpServletRequest);
    
    void baseAppCreateFailureBatchAuditor(final BaseApplicationRequest baseApplicationRequest,
            final HttpServletRequest httpServletRequest);

    /**
     *
     * @param baseApplicationsTbl
     * @param servletRequest
     */
    void baseAppUpdateSuccessAuditor(final BaseApplicationsTbl baseApplicationsTbl,
            final HttpServletRequest servletRequest);

    /**
     *
     * @param baseApplicationRequest
     * @param httpServletRequest
     */
    void baseAppUpdateFailureAuditor(final BaseApplicationRequest baseApplicationRequest,
            final HttpServletRequest httpServletRequest);

    /**
     *
     * @param status
     * @param baseAppId
     * @param httpServletRequest
     * @param isSuccess
     * @param baseApplicationsTbl
     */
    void baseAppUpdateStatusAuditor(final String status, final String baseAppId,
            final HttpServletRequest httpServletRequest,
            final boolean isSuccess, final BaseApplicationsTbl baseApplicationsTbl);

    /**
     *
     * @param baseAppId
     * @param httpServletRequest
     * @param isSuccess
     * @param baseApplicationsTbl
     */
    void baseAppDeleteStatusAuditor(final String baseAppId,
            final HttpServletRequest httpServletRequest,
            final boolean isSuccess, final BaseApplicationsTbl baseApplicationsTbl);

    /**
     *
     * @param baseApplicationResponse
     * @param baseAppIds
     * @param httpServletRequest
     */
    void baseAppMultiDeleteAuditor(final BaseApplicationResponse baseApplicationResponse,
            final Set<String> baseAppIds, final HttpServletRequest httpServletRequest);

	void baseAppStatusUpdateSuccessAuditor(String baseAppName, String status, HttpServletRequest httpServletRequest);

	void baseAppStatusUpdateFailureAuditor(String baseAppName, String status, Exception ex,
			HttpServletRequest httpServletRequest);
}
