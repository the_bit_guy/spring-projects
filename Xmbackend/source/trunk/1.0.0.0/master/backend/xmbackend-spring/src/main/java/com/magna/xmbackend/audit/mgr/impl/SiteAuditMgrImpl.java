/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.audit.mgr.impl;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.magna.xmbackend.audit.mgr.SiteAuditMgr;
import com.magna.xmbackend.audit.mgr.utils.AdminHistoryBaseObjectUtils;
import com.magna.xmbackend.entities.AdminHistoryBaseObjectsTbl;
import com.magna.xmbackend.entities.SitesTbl;
import com.magna.xmbackend.jpa.dao.AdminHistoryBaseObjectsJpaDao;
import com.magna.xmbackend.vo.jpa.site.SiteRequest;

/**
 *
 * @author dhana
 */
@Component
public class SiteAuditMgrImpl implements SiteAuditMgr {

    private static final Logger LOG
            = LoggerFactory.getLogger(SiteAuditMgrImpl.class);
    
    final String OBJECT_NAME = "SitesTbl";
    final String RESULT_SUCCESS = "Success";
    final String RESULT_FAILURE = "Failure";
    

    @Autowired
    private AdminHistoryBaseObjectsJpaDao adminHistoryBaseObjectsJpaDao;

    @Autowired
    private AdminHistoryBaseObjectUtils adminHistoryBaseObjectUtils;

    @Override
    public void siteCreateSuccessAudit(HttpServletRequest hsr, SitesTbl st) {
        LOG.info(">>>> siteCreateSuccessAudit");
        String name = st.getName();
        String changes = "Created site with name " + name;
        AdminHistoryBaseObjectsTbl ahbot
                = this.adminHistoryBaseObjectUtils
                        .makeAdminHistoryBaseObjectsTbl(hsr, changes, "",
                        		OBJECT_NAME, "site create", RESULT_SUCCESS);
        this.adminHistoryBaseObjectsJpaDao.save(ahbot);
        LOG.info("<<<< siteCreateSuccessAudit");
    }

    @Override
    public void siteCreateFailureAudit(HttpServletRequest hsr,
            SiteRequest sr, String errMsg) {
        LOG.info(">>>> siteCreateFailureAudit");
        String name = sr.getName();
        String errorMessage = "Site with name " + name
                + " already exist ";
        AdminHistoryBaseObjectsTbl ahbot
                = this.adminHistoryBaseObjectUtils
                        .makeAdminHistoryBaseObjectsTbl(hsr, "", errorMessage,
                        		OBJECT_NAME, "site create", RESULT_FAILURE);
        this.adminHistoryBaseObjectsJpaDao.save(ahbot);
        LOG.info("<<<< siteCreateFailureAudit");
    }
    
    
    @Override
    public void siteCreateFailureBatchAudit(HttpServletRequest hsr,
            SiteRequest sr) {
        LOG.info(">>>> siteCreateFailureAudit");
        String errMsg = "Site with name "+sr.getName()+" already exist";
        AdminHistoryBaseObjectsTbl ahbot
                = this.adminHistoryBaseObjectUtils
                        .makeAdminHistoryBaseObjectsTbl(hsr, "", errMsg,
                        		OBJECT_NAME, "site create", RESULT_FAILURE);
        this.adminHistoryBaseObjectsJpaDao.save(ahbot);
        LOG.info("<<<< siteCreateFailureAudit");
    }

    @Override
    public void siteDeleteSuccessAudit(HttpServletRequest hsr, String id,
            String name) {
        LOG.info(">>>> sideDeleteSuccessAudit with id {}", id);
        String changes = "Site with name " + name
                + " deleted successfully";
        AdminHistoryBaseObjectsTbl ahbot
                = this.adminHistoryBaseObjectUtils
                        .makeAdminHistoryBaseObjectsTbl(hsr,
                                changes, "", OBJECT_NAME,
                                "site delete", RESULT_SUCCESS);
        this.adminHistoryBaseObjectsJpaDao.save(ahbot);
        LOG.info("<<<< sideDeleteSuccessAudit");
    }

    @Override
    public void siteDeleteFailureAudit(HttpServletRequest hsr,
            String id, String errMsg) {
        LOG.info(">>>> sideDeleteFailureAudit with id {}", id);
        AdminHistoryBaseObjectsTbl ahbot
                = this.adminHistoryBaseObjectUtils
                        .makeAdminHistoryBaseObjectsTbl(hsr, "", errMsg,
                        		OBJECT_NAME, "site delete", RESULT_FAILURE);
        this.adminHistoryBaseObjectsJpaDao.save(ahbot);
        LOG.info("<<<< sideDeleteFailureAudit ");
    }

    @Override
    public void siteUpdateSuccessAudit(HttpServletRequest hsr, SiteRequest sr) {
        LOG.info(">>>> sideUpdateSuccessAudit");
        String name = sr.getName();
        String changes = "Site with name "+name+" updated with status "+sr.getStatus();
        AdminHistoryBaseObjectsTbl ahbot
                = this.adminHistoryBaseObjectUtils
                        .makeAdminHistoryBaseObjectsTbl(hsr, changes, "",
                        		OBJECT_NAME, "site update", RESULT_SUCCESS);
        this.adminHistoryBaseObjectsJpaDao.save(ahbot);
        LOG.info("<<<< sideUpdateSuccessAudit");
    }

    @Override
    public void siteUpdateFailureAudit(HttpServletRequest hsr, SiteRequest sr,
            String errMsg) {
        LOG.info(">>>> sideUpdateFailureAudit");
        AdminHistoryBaseObjectsTbl ahbot = this.adminHistoryBaseObjectUtils
                .makeAdminHistoryBaseObjectsTbl(hsr, "", errMsg,
                		OBJECT_NAME, "site update", RESULT_FAILURE);
        this.adminHistoryBaseObjectsJpaDao.save(ahbot);
        LOG.info("<<<< sideUpdateFailureAudit");
    }

    @Override
    public void siteUpdateSuccessAudit(HttpServletRequest hsr, String id,
            String name, String status) {
        LOG.info(">>>> siteUpdateSuccessAudit");
        String changes = "Site with name " + name
                + " updated with status "+status;
        AdminHistoryBaseObjectsTbl ahbot
                = this.adminHistoryBaseObjectUtils
                        .makeAdminHistoryBaseObjectsTbl(hsr, changes, "",
                        		OBJECT_NAME, "site update", RESULT_SUCCESS);
        this.adminHistoryBaseObjectsJpaDao.save(ahbot);
        LOG.info("<<<< siteUpdateSuccessAudit");
    }

    @Override
    public void siteUpdateFailureAudit(HttpServletRequest hsr, String id,
            String errMsg) {
        LOG.info(">>>> siteUpdateFailureAudit");
        AdminHistoryBaseObjectsTbl ahbot = this.adminHistoryBaseObjectUtils
                .makeAdminHistoryBaseObjectsTbl(hsr, "", errMsg,
                		OBJECT_NAME, "site update", RESULT_FAILURE);
        this.adminHistoryBaseObjectsJpaDao.save(ahbot);
        LOG.info("<<<< siteUpdateFailureAudit");
    }

	@Override
	public void siteStatusUpdateSuccessAuditor(String siteName, String status, HttpServletRequest httpServletRequest) {
		LOG.info(">>>> siteStatusUpdateSuccessAuditor");
		String errorMsg = "";
		final StringBuilder changeBuff = new StringBuilder();
		changeBuff.append("Site with name ").append(siteName).append(" updated ").append(" with status ")
				.append(status);
		
		AdminHistoryBaseObjectsTbl ahbot = this.adminHistoryBaseObjectUtils
				.makeAdminHistoryBaseObjectsTbl(
						httpServletRequest, changeBuff.toString(), errorMsg,
						OBJECT_NAME, "Site status update", RESULT_SUCCESS);
		this.adminHistoryBaseObjectsJpaDao.save(ahbot);
		LOG.info("<<<< siteStatusUpdateSuccessAuditor");
		
	}

	@Override
	public void siteStatusUpdateFailureAuditor(String siteName, String status, Exception ex,
			HttpServletRequest httpServletRequest) {
		LOG.info(">>>> siteStatusUpdateFailureAuditor");
		
		String errorMsg = ex.getMessage();
		final StringBuilder changeBuff = new StringBuilder();
		changeBuff.append("Site with name ").append(siteName).append(" updated ").append(" with status ")
				.append(status);

		AdminHistoryBaseObjectsTbl ahbot = this.adminHistoryBaseObjectUtils
				.makeAdminHistoryBaseObjectsTbl(
						httpServletRequest, changeBuff.toString(), errorMsg,
						OBJECT_NAME, "AdminArea status update", RESULT_FAILURE);
		this.adminHistoryBaseObjectsJpaDao.save(ahbot);
		LOG.info("<<<< siteStatusUpdateFailureAuditor");
		
	}
}
