package com.magna.xmbackend.rel.mgr.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Component;

import com.magna.xmbackend.audit.rel.mgr.UserUserAppRelAuditMgr;
import com.magna.xmbackend.entities.AdminAreaUserAppRelTbl;
import com.magna.xmbackend.entities.AdminAreasTbl;
import com.magna.xmbackend.entities.SiteAdminAreaRelTbl;
import com.magna.xmbackend.entities.UserApplicationsTbl;
import com.magna.xmbackend.entities.UserUserAppRelTbl;
import com.magna.xmbackend.entities.UsersTbl;
import com.magna.xmbackend.exception.CannotCreateRelationshipException;
import com.magna.xmbackend.exception.XMObjectNotFoundException;
import com.magna.xmbackend.jpa.dao.UserJpaDao;
import com.magna.xmbackend.jpa.rel.dao.AdminAreaUserAppRelJpaDao;
import com.magna.xmbackend.jpa.rel.dao.SiteAdminAreaRelJpaDao;
import com.magna.xmbackend.jpa.rel.dao.UserUserAppRelJpaDao;
import com.magna.xmbackend.mgr.UserApplicationMgr;
import com.magna.xmbackend.mgr.UserMgr;
import com.magna.xmbackend.rel.mgr.SiteAdminAreaRelMgr;
import com.magna.xmbackend.rel.mgr.UserUserAppRelMgr;
import com.magna.xmbackend.response.rel.useruserapp.UserUserAppRelResponseWrapper;
import com.magna.xmbackend.response.rel.useruserapp.UserUserAppRelation;
import com.magna.xmbackend.utils.MessageMaker;
import com.magna.xmbackend.utils.Validator;
import com.magna.xmbackend.vo.enums.UserRelationType;
import com.magna.xmbackend.vo.permission.ValidationRequest;
import com.magna.xmbackend.vo.rel.UserUserAppRelBatchRequest;
import com.magna.xmbackend.vo.rel.UserUserAppRelBatchResponse;
import com.magna.xmbackend.vo.rel.UserUserAppRelRequest;
import com.magna.xmbackend.vo.rel.UserUserAppRelResponse;

/**
 *
 * @author vijay
 */
@Component
public class UserUserAppRelMgrImpl implements UserUserAppRelMgr {

    private static final Logger LOG
            = LoggerFactory.getLogger(UserUserAppRelMgrImpl.class);

    @Autowired
    private UserUserAppRelJpaDao userAppRelJpaDao;

    @Autowired
    private UserApplicationMgr userApplicationMgr;

    @Autowired
    private SiteAdminAreaRelMgr siteAdminAreaRelMgr;

    @Autowired
    private MessageMaker messageMaker;

    @Autowired
    private SiteAdminAreaRelJpaDao siteAdminAreaRelJpaDao;

    @Autowired
    private Validator validator;
    
    @Autowired
    private AdminAreaUserAppRelJpaDao adminAreaUserAppRelJpaDao;
    
    @Autowired
    private UserMgr userMgr;
    
    @Autowired
    UserJpaDao userJpaDao;
    
    @Autowired
    private UserUserAppRelAuditMgr userUserAppRelAuditMgr;

    /**
     *
     * @param userUserAppRelRequest
     * @param userName
     * @return UserUserAppRelTbl
     */
    @Override
    public UserUserAppRelTbl create(final UserUserAppRelRequest userUserAppRelRequest,
            final String userName) {
    	LOG.info(">> create");
        UserUserAppRelTbl userUserAppRelTbl = null;
        final boolean hotlineRequest = userUserAppRelRequest.isHotlineRequest();
        //final boolean hotlineRequest = true;
        LOG.info("hotlineRequest={}", hotlineRequest);
        final String userId = userUserAppRelRequest.getUserId();
        final String siteAdminAreaRelId = userUserAppRelRequest.getSiteAdminAreaRelId();
        final String userAppId = userUserAppRelRequest.getUserAppId();
        final Map<String, Object> assignmentAllowedMap
                = isInActiveAssignmentsAllowed(userUserAppRelRequest, userName);
        LOG.info("assignmentAllowedMap={}", assignmentAllowedMap);
        if (assignmentAllowedMap.containsKey("isAssignmentAllowed")) {
            boolean isAssignmentAllowed = (boolean) assignmentAllowedMap.get("isAssignmentAllowed");
            if (isAssignmentAllowed) {
            	if(hotlineRequest) {
            		final boolean check = this.checkIfHotlineUserAppExistInUserAndAdminArea(userUserAppRelRequest);
            		if(check) {
            			String userRelationType = userUserAppRelRequest.getUserRelationType();
            			if (UserRelationType.ALLOWED.name().equalsIgnoreCase(userRelationType)) {
            				userRelationType = UserRelationType.FORBIDDEN.name();
            			} else {
            				userRelationType = UserRelationType.ALLOWED.name();
            			}
            			final UserUserAppRelTbl uuserAppRelTbl = this.userAppRelJpaDao.
            					findByUserIdAndSiteAdminAreaRelIdAndUserApplicationIdAndUserRelType(new UsersTbl(userId), new SiteAdminAreaRelTbl(siteAdminAreaRelId), 
            							new UserApplicationsTbl(userAppId), userRelationType);
            			if (null != uuserAppRelTbl ) {
            				final String userUserAppRelId = uuserAppRelTbl.getUserUserAppRelId();
            				this.userAppRelJpaDao.updateUserRelType(userUserAppRelRequest.getUserRelationType(), userUserAppRelId);
            			}
            		} else {
            			final UserUserAppRelTbl uuart = this.convert2Entity(userUserAppRelRequest);
                        userUserAppRelTbl = this.userAppRelJpaDao.save(uuart);
            		}
            		
            	} else {
            		this.checkIfUserAppExistInUserAndAdminArea(userUserAppRelRequest);
            		final UserUserAppRelTbl uuart = this.convert2Entity(userUserAppRelRequest);
                    userUserAppRelTbl = this.userAppRelJpaDao.save(uuart);
            	}
                
                //this.checkIfUserAppNotExistInSiteAdminArea(userUserAppRelRequest);
                
            } else {
                if (assignmentAllowedMap.containsKey("userApplicationsTbl")) {
                    final Map<String, String> userAppTransMap
                            = messageMaker.getUserAppNames((UserApplicationsTbl) assignmentAllowedMap.get("userApplicationsTbl"));
                    final Map<String, String> userTransMap
                            = messageMaker.getUserNames((UsersTbl) assignmentAllowedMap.get("usersTbl"));
                    final Map<String, String[]> i18nCodeMap = this.messageMaker.geti18nCodeMap(userTransMap, userAppTransMap);
                    LOG.debug("i18nCodeMap={}", i18nCodeMap);
                    throw new CannotCreateRelationshipException(
                            "Inactive Assignments not allowed", "UUA_ERR0001", i18nCodeMap);
                }
            }
        }
        LOG.info("<< create");
        return userUserAppRelTbl;
    }

    /**
     *
     * @param userUserAppRelRequest
     * @param userName
     * @return Map
     */
    private Map<String, Object> isInActiveAssignmentsAllowed(final UserUserAppRelRequest userUserAppRelRequest,
            final String userName) {
        final boolean isViewInactiveAllowed = isViewInActiveAllowed(userName);
        LOG.debug("isViewInactiveAllowed={}", isViewInactiveAllowed);
        final Map<String, Object> assignmentAllowedMap
                = validator.isUserUserAppInactiveAssignmentAllowed(userUserAppRelRequest,
                        isViewInactiveAllowed);
        return assignmentAllowedMap;
    }

    /**
     *
     * @param userName
     * @return boolean
     */
    private boolean isViewInActiveAllowed(final String userName) {
        final ValidationRequest validationRequest
                = validator.formSaveRelationValidationRequest(userName, "USERAPPLICATION_USER");
        LOG.info("validationRequest={}", validationRequest);
        final boolean isInactiveAssignment = validator.isViewInactiveAllowed(validationRequest);
        LOG.info("isInactiveAssignment={}", isInactiveAssignment);
        return isInactiveAssignment;
    }

    /**
     *
     * @param uarr
     */
    private void checkIfUserAppExistInUserAndAdminArea(final UserUserAppRelRequest uarr) {
        final String userAppIdIn = uarr.getUserAppId();
        final String userIdIn = uarr.getUserId();
        final String siteAARelId = uarr.getSiteAdminAreaRelId();
        LOG.debug("userAppIdIn {} and siteAdminAreaRelId {} and userIdIn {}", userAppIdIn, siteAARelId, userIdIn);
        final UserApplicationsTbl userApplicationsTbl = this.userApplicationMgr.findById(userAppIdIn);
        final SiteAdminAreaRelTbl siteAdminAreaRelTbl = this.siteAdminAreaRelMgr.findById(siteAARelId);
        final AdminAreasTbl adminAreasTbl = siteAdminAreaRelTbl.getAdminAreaId();
        if (null != userApplicationsTbl) {
            final Collection<UserUserAppRelTbl> userUserAppRelTbls = userApplicationsTbl.getUserUserAppRelTblCollection();
            if (!userUserAppRelTbls.isEmpty()) {
                for (UserUserAppRelTbl userUserAppRelTbl : userUserAppRelTbls) {
                    final UsersTbl usersTbl = userUserAppRelTbl.getUserId();
                    final String siteAARelIdFromTbl = userUserAppRelTbl.getSiteAdminAreaRelId().getSiteAdminAreaRelId();

                    String userIdFromTbl = usersTbl.getUserId();
                    if (userIdFromTbl != null && userIdFromTbl.equalsIgnoreCase(userIdIn) && siteAARelIdFromTbl.equalsIgnoreCase(siteAARelId)) {
                        //throw ex
                        //User Application 'name' is already assigned to User 'name'
                        final Map<String, String> userAppNames = this.messageMaker.getUserAppNames(userApplicationsTbl);
                        final Map<String, String> userNames = this.messageMaker.getUserNames(usersTbl);
                        final Map<String, String> adminAreas = this.messageMaker.getAdminAreaNames(adminAreasTbl);

                        final Map<String, String[]> i18nCodeMap = this.messageMaker.geti18nCodeMap(userAppNames, adminAreas, userNames);
                        throw new CannotCreateRelationshipException(
                                "Realtionship exists", "ERR0016", i18nCodeMap);
                    }
                }
            }
        }
    }
    
    
    
    private boolean checkIfHotlineUserAppExistInUserAndAdminArea(final UserUserAppRelRequest uarr) {
        final String userAppIdIn = uarr.getUserAppId();
        final String userIdIn = uarr.getUserId();
        final String siteAARelId = uarr.getSiteAdminAreaRelId();
        final String userRelationType = uarr.getUserRelationType();
        LOG.debug("userAppIdIn {} and siteAdminAreaRelId {} and userIdIn {}", userAppIdIn, siteAARelId, userIdIn);
        final UserApplicationsTbl userApplicationsTbl = this.userApplicationMgr.findById(userAppIdIn);
        if (null != userApplicationsTbl) {
            //final Collection<UserUserAppRelTbl> userUserAppRelTbls = userApplicationsTbl.getUserUserAppRelTblCollection();
            final List<UserUserAppRelTbl> userUserAppRelTbls = this.userAppRelJpaDao.findByUserIdAndSiteAdminAreaRelIdAndUserApplicationId(new UsersTbl(userIdIn), new SiteAdminAreaRelTbl(siteAARelId), new UserApplicationsTbl(userAppIdIn));
            if (!userUserAppRelTbls.isEmpty()) {
            	
                for (UserUserAppRelTbl userUserAppRelTbl : userUserAppRelTbls) {
                	String userRelType = userUserAppRelTbl.getUserRelType();
                    if (UserRelationType.ALLOWED.name().equalsIgnoreCase(userRelType)) {
                    	userRelType = UserRelationType.FORBIDDEN.name();
        			} else {
        				userRelType = UserRelationType.ALLOWED.name();
        			}
                    final UsersTbl usersTbl = userUserAppRelTbl.getUserId();
                    final String siteAARelIdFromTbl = userUserAppRelTbl.getSiteAdminAreaRelId().getSiteAdminAreaRelId();
                    //final String userRelType = userUserAppRelTbl.getUserRelType();

                    String userIdFromTbl = usersTbl.getUserId();
                    if (userIdFromTbl != null && userIdFromTbl.equalsIgnoreCase(userIdIn) && siteAARelIdFromTbl.equalsIgnoreCase(siteAARelId) && userRelationType.equals(userRelType) ) {
                        //throw ex
                        //User Application 'name' is already assigned to User 'name'
                        /*final Map<String, String> userAppNames = this.messageMaker.getUserAppNames(userApplicationsTbl);
                        final Map<String, String> userNames = this.messageMaker.getUserNames(usersTbl);
                        final Map<String, String> adminAreas = this.messageMaker.getAdminAreaNames(adminAreasTbl);

                        final Map<String, String[]> i18nCodeMap = this.messageMaker.geti18nCodeMap(userAppNames, adminAreas, userNames);
                        throw new CannotCreateRelationshipException(
                                "Realtionship exists", "ERR0016", i18nCodeMap);*/
                    	return true;
                    } else {
                    	return false;
                    }
                }
            }
        }
		return false;
    }
    

    
    @SuppressWarnings("unused")
	private void checkIfUserAppNotExistInSiteAdminArea(final UserUserAppRelRequest uarr){
    	final String userAppIdIn = uarr.getUserAppId();
    	final String userIdIn = uarr.getUserId();
    	final UsersTbl usersTbl = this.userMgr.findById(userIdIn);
    	final String siteAARelId = uarr.getSiteAdminAreaRelId();
    	final UserApplicationsTbl userApplicationsTbl = this.userApplicationMgr.findById(userAppIdIn);
    	final SiteAdminAreaRelTbl siteAdminAreaRelTbl = this.siteAdminAreaRelMgr.findById(siteAARelId);
    	final AdminAreasTbl adminAreasTbl = siteAdminAreaRelTbl.getAdminAreaId();
    	List<AdminAreaUserAppRelTbl> adminAreaUserAppRelTbls = this.adminAreaUserAppRelJpaDao.findBySiteAdminAreaRelId(siteAdminAreaRelTbl);
    	List<UserApplicationsTbl> userApplicationsTbls = null;
    	if(!adminAreaUserAppRelTbls.isEmpty()){
    		userApplicationsTbls = new ArrayList<>();
    		for (AdminAreaUserAppRelTbl adminAreaUserAppRelTbl : adminAreaUserAppRelTbls) {
    			userApplicationsTbls.add(adminAreaUserAppRelTbl.getUserApplicationId());
			}
    		if(!userApplicationsTbls.isEmpty()){
    			if(!userApplicationsTbls.contains(new UserApplicationsTbl(userAppIdIn))){
    				//throw cannot create relation exception
    				final Map<String, String> userAppNames = this.messageMaker.getUserAppNames(userApplicationsTbl);
                    final Map<String, String> userNames = this.messageMaker.getUserNames(usersTbl);
                    final Map<String, String> adminAreas = this.messageMaker.getAdminAreaNames(adminAreasTbl);
                    final Map<String, String[]> i18nCodeMap = this.messageMaker.geti18nCodeMap(userAppNames, adminAreas, userNames);
                    throw new CannotCreateRelationshipException(
                            "Invalid Relation", "ERR0018", i18nCodeMap);
    			}
    		}
    	}
    }
    
    
    /**
     *
     * @param userUserAppRelRequest
     * @return UserUserAppRelTbl
     */
    private UserUserAppRelTbl convert2Entity(final UserUserAppRelRequest userUserAppRelRequest) {
        final String id = UUID.randomUUID().toString();
        final String userAppId = userUserAppRelRequest.getUserAppId();
        final String userId = userUserAppRelRequest.getUserId();
        final String userRelationType = userUserAppRelRequest.getUserRelationType();
        final String siteAdminAreaRelId = userUserAppRelRequest.getSiteAdminAreaRelId();
        final UserUserAppRelTbl userUserAppRelTbl = new UserUserAppRelTbl(id);
        userUserAppRelTbl.setUserApplicationId(new UserApplicationsTbl(userAppId));
        userUserAppRelTbl.setUserId(new UsersTbl(userId));
        userUserAppRelTbl.setUserRelType(userRelationType);
        userUserAppRelTbl.setSiteAdminAreaRelId(new SiteAdminAreaRelTbl(siteAdminAreaRelId));
        return userUserAppRelTbl;
    }

    /**
     *
     * @param userUserAppRelBatchRequest
     * @param userName
     * @return UserUserAppRelBatchResponse
     */
    @Override
    public final UserUserAppRelBatchResponse createBatch(final UserUserAppRelBatchRequest userUserAppRelBatchRequest,
            final String userName) {
        final List<UserUserAppRelTbl> userUserAppRelTbls = new ArrayList<>();
        final List<Map<String, String>> statusMaps = new ArrayList<>();
        final List<UserUserAppRelRequest> userUserAppRelRequests
                = userUserAppRelBatchRequest.getUserUserAppRelRequests();
        for (final UserUserAppRelRequest userUserAppRelRequest : userUserAppRelRequests) {
            try {
                final UserUserAppRelTbl aasartOut = this.create(userUserAppRelRequest, userName);
                userUserAppRelTbls.add(aasartOut);
            } catch (CannotCreateRelationshipException ccre) {
                final Map<String, String> statusMap = messageMaker.extractFromException(ccre);
                statusMaps.add(statusMap);
            }
        }
        final UserUserAppRelBatchResponse userUserAppRelBatchResponse
                = new UserUserAppRelBatchResponse(userUserAppRelTbls, statusMaps);
        return userUserAppRelBatchResponse;
    }

    /**
     *
     * @param userId
     * @return UserUserAppRelResponse
     */
    @Override
    public UserUserAppRelResponse findUserUserAppRelsByUserId(final String userId) {
        LOG.info(">> findUserUserAppRelsByUserId");
        final List<UserUserAppRelTbl> userUserAppRelTblItr = this.userAppRelJpaDao.findByUserId(new UsersTbl(userId));
        if (userUserAppRelTblItr.isEmpty()) {
            throw new RuntimeException("No Relation Found");
        }
        final UserUserAppRelResponse relResponse = new UserUserAppRelResponse(userUserAppRelTblItr);
        LOG.info("<< findUserUserAppRelsByUserId");
        return relResponse;
    }

    /**
     *
     * @param userId
     * @param siteAARelId
     * @return UserUserAppRelResponse
     */
    @Override
    public UserUserAppRelResponse findUserUserAppRelsByUserIdAndSiteAARelId(final String userId,
            final String siteAARelId, final ValidationRequest validationRequest) {
        LOG.info(">> findUserUserAppRelsByUserIdAndSiteAARelId");
        final boolean isViewInactive = validator.isViewInactiveAllowed(validationRequest);
        LOG.info("isViewInactive={}", isViewInactive);
        SiteAdminAreaRelTbl siteAdminAreaRelTbl = this.siteAdminAreaRelJpaDao.findOne(siteAARelId);
        validator.validateUser(userId, isViewInactive);
        validator.validateSiteAdminArea(siteAARelId, isViewInactive);
        validator.validateAdminArea(siteAdminAreaRelTbl.getAdminAreaId().getAdminAreaId(), isViewInactive);
        validator.validateSite(siteAdminAreaRelTbl.getSiteId().getSiteId(), isViewInactive);
        
		final List<UserUserAppRelTbl> userUserAppRelTbls = this.userAppRelJpaDao.findByUserIdAndSiteAdminAreaRelId(
                new UsersTbl(userId), siteAdminAreaRelTbl);
		final List<UserUserAppRelTbl> filterUserUserAppRel = validator.filterUserUserAppRel(isViewInactive, userUserAppRelTbls);
        if (filterUserUserAppRel.isEmpty()) {
            throw new RuntimeException("No Relation Found");
        }
        final UserUserAppRelResponse relResponse = new UserUserAppRelResponse(filterUserUserAppRel);
        LOG.info("<< findUserUserAppRelsByUserIdAndSiteAARelId");
        return relResponse;
    }

    /**
     *
     * @param userId
     * @param relType
     * @return UserUserAppRelResponse
     */
    @Override
    public final UserUserAppRelResponse findUserUserAppRelsByUserIdAndRelationType(final String userId,
            final String relType) {
        List<UserUserAppRelTbl> userUserAppRelTblItr
                = this.userAppRelJpaDao.findByUserIdAndUserRelType(new UsersTbl(userId), relType);
        if (userUserAppRelTblItr.isEmpty()) {
            throw new RuntimeException("No Relation Found");
        }
        final UserUserAppRelResponse relResponse = new UserUserAppRelResponse(userUserAppRelTblItr);
        return relResponse;
    }

    /**
     *
     * @param userAppId
     * @return UserUserAppRelResponseWrapper
     */
    @Override
    public UserUserAppRelResponseWrapper findByUserAppId(final String userAppId, final ValidationRequest validationRequest) {
        LOG.info(">> findByUserAppId");
        final boolean isViewInactive = validator.isViewInactiveAllowed(validationRequest);
        LOG.info("isViewInactive={}", isViewInactive);
        validator.validateUserApp(userAppId, isViewInactive);
        final List<UserUserAppRelTbl> userUserAppRelTblItr
                = this.userAppRelJpaDao.findByUserApplicationId(new UserApplicationsTbl(userAppId));
        if (userUserAppRelTblItr.isEmpty()) {
            throw new RuntimeException("No Relation Found");
        }
        final List<UserUserAppRelation> userUserAppRelations = new ArrayList<>();
        for (final UserUserAppRelTbl userUserAppRelTbl : userUserAppRelTblItr) {
        	final UsersTbl usersTbl = userUserAppRelTbl.getUserId();
        	UsersTbl filterUserTbl = validator.filterUserResponse(isViewInactive, usersTbl);
        	if (null != filterUserTbl) {
        		final UserUserAppRelation uuar = new UserUserAppRelation();
        		uuar.setUserUserAppRelId(userUserAppRelTbl.getUserUserAppRelId());
        		uuar.setUserRelType(userUserAppRelTbl.getUserRelType());
        		uuar.setUserUserAppRelId(userUserAppRelTbl.getUserUserAppRelId());
        		uuar.setUserId(filterUserTbl);
        		userUserAppRelations.add(uuar);
        	}
        }
        final UserUserAppRelResponseWrapper relResponse = new UserUserAppRelResponseWrapper(userUserAppRelations);
        LOG.info("<< findByUserAppId");
        return relResponse;
    }

    /**
     *
     * @param id
     * @return Boolean
     */
    @Override
    public Boolean deleteById(final String id) {
        LOG.info(">> deleteById");
        boolean isDeleted = false;
        try {
        	this.userAppRelJpaDao.delete(id);
        	isDeleted = true;
        } catch (Exception e) {
        	if (e instanceof EmptyResultDataAccessException) {
        		final String[] param = {id};
        		throw new XMObjectNotFoundException("User not found", "P_ERR0022", param);
        	}
        }

        LOG.info("<< deleteById");
        return isDeleted;
    }
    
    
	@Override
	public UserUserAppRelResponse multiDelete(Set<String> ids, HttpServletRequest httpServletRequest) {
		LOG.info(">> multiDelete");
		final List<Map<String, String>> statusMaps = new ArrayList<>();
		ids.forEach(id -> {
			UserUserAppRelTbl userUserAppRelTbl = this.userAppRelJpaDao.findOne(id);
			try {
				if(userUserAppRelTbl != null) {
					this.deleteById(id);
					this.userUserAppRelAuditMgr.userUserAppMultiDeleteSuccessAudit(userUserAppRelTbl, httpServletRequest);
				} else {
					throw new XMObjectNotFoundException("Relation not found", "ERR0003");
				}
				
			} catch (XMObjectNotFoundException objectNotFound) {
				Map<String, String> statusMap = messageMaker.extractFromException(objectNotFound);
				statusMaps.add(statusMap);
			}
		});
		UserUserAppRelResponse relResponse = new UserUserAppRelResponse(statusMaps);
		LOG.info(">> multiDelete");
		return relResponse;
	}

    /**
     *
     * @param userId
     * @param siteAARelId
     * @param relType
     * @return UserUserAppRelResponse
     */
    @Override
    public UserUserAppRelResponse findUserUserAppRelsByUserIdAndSiteAARelIdAndRelType(final String userId,
            final String siteAARelId, final String relType, final ValidationRequest validationRequest) {
        LOG.info(">> findUserUserAppRelsByUserIdAndSiteAARelIdAndRelType");
    	final boolean isViewInactive = validator.isViewInactiveAllowed(validationRequest);
        LOG.info("isViewInactive={}", isViewInactive);
        SiteAdminAreaRelTbl siteAdminAreaRelTbl = this.siteAdminAreaRelJpaDao.findOne(siteAARelId);
        validator.validateUser(userId, isViewInactive);
        validator.validateSiteAdminArea(siteAARelId, isViewInactive);
        validator.validateAdminArea(siteAdminAreaRelTbl.getAdminAreaId().getAdminAreaId(), isViewInactive);
        validator.validateSite(siteAdminAreaRelTbl.getSiteId().getSiteId(), isViewInactive);
        
        final List<UserUserAppRelTbl> userUserAppRelTbls = this.userAppRelJpaDao.findByUserIdAndSiteAdminAreaRelIdAndUserRelType(
                new UsersTbl(userId), siteAdminAreaRelTbl, relType);
        final List<UserUserAppRelTbl> filterUserUserAppRel = validator.filterUserUserAppRel(isViewInactive, userUserAppRelTbls);
        if (filterUserUserAppRel.isEmpty()) {
            throw new RuntimeException("No Relation Found");
        }
        final UserUserAppRelResponse relResponse = new UserUserAppRelResponse(filterUserUserAppRel);
        LOG.info("<< findUserUserAppRelsByUserIdAndSiteAARelIdAndRelType");
        return relResponse;
    }

    /**
     *
     * @param userId
     * @param adminAreaId
     * @return UserUserAppRelResponseWrapper
     */
    @Override
    public List<UserUserAppRelation> findUserUserAppRelsByUserIdAndAAId(final String userId,
            final String adminAreaId, ValidationRequest validationRequest) {
    	final boolean isViewInactive = validator.isViewInactiveAllowed(validationRequest);
        LOG.info("isViewInactive={}", isViewInactive);
        UsersTbl validatedUser = validator.validateUser(userId, isViewInactive);
        AdminAreasTbl validatedAdminArea = validator.validateAdminArea(adminAreaId, isViewInactive);
        final List<SiteAdminAreaRelTbl> siteAdminAreaRelTbls = this.siteAdminAreaRelJpaDao.findByAdminAreaId(validatedAdminArea);
        List<SiteAdminAreaRelTbl> filterdSiteAdminAreaRel = validator.filterSiteAdminAreaRel(isViewInactive, siteAdminAreaRelTbls);
        List<UserUserAppRelation> userUserAppRelations = null;
        if (!siteAdminAreaRelTbls.isEmpty()) {
            final List<UserUserAppRelTbl> userUserAppRelTbls = this.userAppRelJpaDao.findBySiteAdminAreaRelIdInAndUserId(filterdSiteAdminAreaRel, validatedUser);
            if (!userUserAppRelTbls.isEmpty()) {
                userUserAppRelations = new ArrayList<>();
                for (UserUserAppRelTbl userUserAppRelTbl : userUserAppRelTbls) {
                	UserApplicationsTbl userApplicationsTbl = userUserAppRelTbl.getUserApplicationId();
                	UserApplicationsTbl filterUserApplicationTbl = validator.filterUserApplicationResponse(isViewInactive, userApplicationsTbl);
                	if (null != filterUserApplicationTbl) {
                		final UserUserAppRelation userUserAppRelation = new UserUserAppRelation();
                		userUserAppRelation.setUserUserAppRelId(userUserAppRelTbl.getUserUserAppRelId());
                		userUserAppRelation.setUserRelType(userUserAppRelTbl.getUserRelType());
                		userUserAppRelation.setUserId(userUserAppRelTbl.getUserId());
                		userUserAppRelation.setUserApplicationId(filterUserApplicationTbl);
                		userUserAppRelations.add(userUserAppRelation);
                	}
                }
            } else {
                throw new XMObjectNotFoundException("No Realtion Found", "U_ERR0008");
            }
        }
        //final UserUserAppRelResponseWrapper responseWrapper = new UserUserAppRelResponseWrapper(userUserAppRelations);
        return userUserAppRelations;
    }

    /**
     *
     * @param userUserAppRelBatchRequest
     * @return UserUserAppRelBatchResponse
     */
    @Override
    public UserUserAppRelBatchResponse updateUserRelTypesByIds(final UserUserAppRelBatchRequest userUserAppRelBatchRequest,
    									HttpServletRequest httpServletRequest) {
        final List<UserUserAppRelRequest> userUserAppRelRequests = userUserAppRelBatchRequest.getUserUserAppRelRequests();
        final List<UserUserAppRelTbl> userUserAppRelTbls = new ArrayList<>();
        if (userUserAppRelRequests.size() > 0) {
            userUserAppRelRequests.forEach(request -> {
                final UserUserAppRelTbl userUserAppRelTbl = this.userAppRelJpaDao.findByUserUserAppRelId(request.getId());
                userUserAppRelTbl.setUserRelType(request.getUserRelationType());
                UserUserAppRelTbl userUserAppRelOutTbl = this.userAppRelJpaDao.save(userUserAppRelTbl);
				userUserAppRelTbls.add(userUserAppRelOutTbl);
                this.userUserAppRelAuditMgr.userUserAppUpdateSuccessAudit(userUserAppRelTbl, httpServletRequest);
            });
        }
        final List<Map<String, String>> statusMaps = null;
        final UserUserAppRelBatchResponse uuarbr = new UserUserAppRelBatchResponse(userUserAppRelTbls, statusMaps);
        return uuarbr;
    }



}
