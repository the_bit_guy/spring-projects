/**
 * 
 */
package com.magna.xmbackend.audit.mgr.impl;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.magna.xmbackend.audit.mgr.IconAuditMgr;
import com.magna.xmbackend.audit.mgr.utils.AdminHistoryBaseObjectUtils;
import com.magna.xmbackend.entities.AdminHistoryBaseObjectsTbl;
import com.magna.xmbackend.entities.IconsTbl;
import com.magna.xmbackend.jpa.dao.AdminHistoryBaseObjectsJpaDao;
import com.magna.xmbackend.vo.icon.IkonRequest;

/**
 * @author Bhabadyuti Bal
 *
 */
@Component
public class IconAuditMgrImpl implements IconAuditMgr {
	
	
	private static final Logger LOG = LoggerFactory.getLogger(IconAuditMgrImpl.class);
	
	final String OBJECT_NAME = "IconTbl";
	final String RESULT_SUCCESS = "Success";
	final String RESULT_FAILURE = "Failure";
	
	@Autowired
    private AdminHistoryBaseObjectsJpaDao adminHistoryBaseObjectsJpaDao;

    @Autowired
    private AdminHistoryBaseObjectUtils adminHistoryBaseObjectUtils;

	@Override
	public void iconCreateSuccessAudit(IconsTbl it, HttpServletRequest httpServletRequest) {
		LOG.info(">> iconCreateSuccessAudit");
		String name = it.getIconName();
		final StringBuilder changeBuff = new StringBuilder();
		if(it != null) {
			changeBuff.append("Icon with name ").append(name).append(" created ");
		}
		
		AdminHistoryBaseObjectsTbl ahbot = this.adminHistoryBaseObjectUtils.makeAdminHistoryBaseObjectsTbl(httpServletRequest,
				changeBuff.toString(), "", OBJECT_NAME, "Icon Create", RESULT_SUCCESS);
		this.adminHistoryBaseObjectsJpaDao.save(ahbot);
		
		LOG.info("<< iconCreateSuccessAudit");
	}

	@Override
	public void iconCreateFailureAudit(HttpServletRequest httpServletRequest, IkonRequest ikonRequest, String errMsg) {
		LOG.info(">>>> iconCreateFailureAudit");
        String name = ikonRequest.getIconName();
        String errorMessage = "Icon creation failed for name " + name
                + "with exception " + errMsg;
        AdminHistoryBaseObjectsTbl ahbot
                = this.adminHistoryBaseObjectUtils
                        .makeAdminHistoryBaseObjectsTbl(httpServletRequest, "", errorMessage,
                        		OBJECT_NAME, "Icon Create", RESULT_FAILURE);
        this.adminHistoryBaseObjectsJpaDao.save(ahbot);
        LOG.info("<<<< iconCreateFailureAudit");
	}

	@Override
	public void iconDeleteSuccessAudit(HttpServletRequest httpServletRequest, IconsTbl iconsTbl, boolean isDeleted) {
		LOG.info(">>>> iconDeleteSuccessAudit");
		String name = iconsTbl.getIconName();
		String changes = "Icon with name " + name + " deleted";
		AdminHistoryBaseObjectsTbl ahbot = this.adminHistoryBaseObjectUtils.makeAdminHistoryBaseObjectsTbl(httpServletRequest,
				changes, "", OBJECT_NAME, "Icon Delete", RESULT_SUCCESS);
		this.adminHistoryBaseObjectsJpaDao.save(ahbot);
		
	}

	@Override
	public void iconDeleteFailureAudit(HttpServletRequest httpServletRequest, String id, String errMsg) {
		LOG.info(">>>> iconDeleteFailureAudit");
        AdminHistoryBaseObjectsTbl ahbot
                = this.adminHistoryBaseObjectUtils
                        .makeAdminHistoryBaseObjectsTbl(httpServletRequest, "", errMsg,
                        		OBJECT_NAME, "Icon Delete", RESULT_FAILURE);
        this.adminHistoryBaseObjectsJpaDao.save(ahbot);
        LOG.info("<<<< groupDeleteFailureAudit ");
		
	}

}
