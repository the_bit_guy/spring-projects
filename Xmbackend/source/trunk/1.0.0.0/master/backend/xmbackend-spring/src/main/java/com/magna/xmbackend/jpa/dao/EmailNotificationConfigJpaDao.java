/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.jpa.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.magna.xmbackend.entities.EmailNotificationConfigTbl;
import com.magna.xmbackend.entities.EmailNotificationEventTbl;
import com.magna.xmbackend.entities.EmailTemplateTbl;

@Transactional
public interface EmailNotificationConfigJpaDao extends CrudRepository<EmailNotificationConfigTbl, String> {
    
	
    List<EmailNotificationConfigTbl> findByEmailNotificationEventId(EmailNotificationEventTbl emailNotificationEventId);
    
    @Query("select enurt.status from EmailNotificationEventTbl enurt where enurt.event=:event")
    String findStatusByEvent(@Param("event") String event);
    
    //@Query("select enurt.emailTemplate from EmailNotificationEventTbl enurt where enurt.event=:event")
    /*@Query("select enct.emailTemplate from EmailNotificationEventTbl enet, EmailNotificationConfigTbl enct where "
    		+ "enet.emailNotificationEventId = enct.emailNotificationEventId and  enet.event =:event")*/
    //String findEmailTemplateByEvent(@Param("event") String event);
    
    @Query("from EmailNotificationEventTbl enet, EmailNotificationConfigTbl enct where "
    		+ "enet.emailNotificationEventId = enct.emailNotificationEventId and  enet.event =:event and enct.name=:name")
    EmailNotificationConfigTbl findByEventAndAction(@Param("event") String event, @Param("name") String actionName);
    
    @Modifying
    @Query("update EmailNotificationConfigTbl enct set enct.status=:status where enct.emailNotificationConfigId =:emailNotificationConfigId")
    int updateEmailNotificationConfiguration(@Param("status") String status, @Param("emailNotificationConfigId") String emailNotificationConfigId);

	List<EmailNotificationConfigTbl> findByEmailNotificationEventIdAndStatus(EmailNotificationEventTbl eventTbl,
			String status);

	EmailNotificationConfigTbl findByNameIgnoreCase(String notificationConfigActionName);
	
	@Modifying
	@Query("update EmailNotificationConfigTbl enct set enct.status=:status where enct.emailNotificationConfigId=:configId")
	int updateEmailNotificationConfigStatus(@Param("configId") String configId,@Param("status")  String status);

	List<EmailNotificationConfigTbl> findByEmailTemplateId(EmailTemplateTbl emailTemplateTbl);
}
