package com.magna.xmbackend.mgr;

import java.util.List;
import java.util.Set;

import com.magna.xmbackend.entities.ProjectApplicationsTbl;
import com.magna.xmbackend.vo.permission.ValidationRequest;
import com.magna.xmbackend.vo.projectApplication.ProjectApplciationMenuWrapper;
import com.magna.xmbackend.vo.projectApplication.ProjectApplicationRequest;
import com.magna.xmbackend.vo.projectApplication.ProjectApplicationResponse;
import javax.servlet.http.HttpServletRequest;

// TODO: Auto-generated Javadoc
/**
 * The Interface ProjectApplicationMgr.
 */
public interface ProjectApplicationMgr {

    /**
     * Find all.
     *
     * @param validationRequest the validation request
     * @return ProjectApplicationResponse
     */
    ProjectApplicationResponse findAll(final ValidationRequest validationRequest);

    /**
     * Find by id.
     *
     * @param id the id
     * @param validationRequest the validation request
     * @return ProjectApplicationsTbl
     */
    ProjectApplicationsTbl findById(final String id,
            final ValidationRequest validationRequest);

    /**
     * Find by id.
     *
     * @param id the id
     * @return ProjectApplicationsTbl
     */
    ProjectApplicationsTbl findById(final String id);

    /**
     * Creates the.
     *
     * @param projectApplicationRequest the project application request
     * @return ProjectApplicationsTbl
     */
    ProjectApplicationsTbl create(final ProjectApplicationRequest projectApplicationRequest);

    /**
     * Update.
     *
     * @param projectApplicationRequest the project application request
     * @return ProjectApplicationsTbl
     */
    ProjectApplicationsTbl update(final ProjectApplicationRequest projectApplicationRequest);

    /**
     * Delete by id.
     *
     * @param id the id
     * @return boolean
     */
    boolean deleteById(final String id);

    /**
     * Multi delete.
     *
     * @param projAppIds the proj app ids
     * @param httpServletRequest the http servlet request
     * @return ProjectApplicationResponse
     */
    ProjectApplicationResponse multiDelete(final Set<String> projAppIds,
            final HttpServletRequest httpServletRequest);

    /**
     * Update status by id.
     *
     * @param status the status
     * @param id the id
     * @return boolean
     */
    boolean updateStatusById(final String status, final String id);

    /**
     * Find project applications by project id.
     *
     * @param id the id
     * @param type the type
     * @param validationRequest the validation request
     * @return ProjectApplicationResponse
     */
    ProjectApplicationResponse findProjectApplicationsByProjectId(final String id,
            final String type, final ValidationRequest validationRequest);

    /**
     * Find project applications by base app id.
     *
     * @param id the id
     * @param validationRequest the validation request
     * @return ProjectApplicationResponse
     */
    ProjectApplicationResponse findProjectApplicationsByBaseAppId(final String id,
            final ValidationRequest validationRequest);

    /**
     * Find project app by position.
     *
     * @param position the position
     * @param validationRequest the validation request
     * @return ProjectApplicationResponse
     */
    ProjectApplicationResponse findProjectAppByPosition(final String position,
            final ValidationRequest validationRequest);

    /**
     * Find all project applications by status.
     *
     * @param status the status
     * @return ProjectApplicationResponse
     */
    ProjectApplicationResponse findAllProjectApplicationsByStatus(final String status);

    /**
     * Find all project applications by positions.
     *
     * @param validationRequest the validation request
     * @return ProjectApplicationResponse
     */
    ProjectApplicationResponse findAllProjectApplicationsByPositions(final ValidationRequest validationRequest);

    /**
     * Find proj app by project site AA id.
     *
     * @param siteId the site id
     * @param adminAreaId the admin area id
     * @param projectId the project id
     * @return ProjectApplicationResponse
     */
    ProjectApplicationResponse findProjAppByProjectSiteAAId(final String siteId,
            final String adminAreaId, final String projectId);

    /**
     * Find proj app by user project id.
     *
     * @param userId the user id
     * @param projectId the project id
     * @return ProjectApplicationResponse
     */
    ProjectApplicationResponse findProjAppByUserProjectId(final String userId,
            final String projectId);

    /**
     * Find proj app by project site AA id and rel type.
     *
     * @param siteId the site id
     * @param adminAreaId the admin area id
     * @param projectId the project id
     * @param relType the rel type
     * @return ProjectApplicationResponse
     */
    ProjectApplicationResponse findProjAppByProjectSiteAAIdAndRelType(final String siteId,
            final String adminAreaId, final String projectId, final String relType);

    /**
     * Find fixed proj app by user project id.
     *
     * @param userId the user id
     * @param projectId the project id
     * @return ProjectApplicationResponse
     */
    ProjectApplicationResponse findFixedProjAppByUserProjectId(final String userId,
            final String projectId);

    /**
     * Find project app by AA id project id user name.
     *
     * @param aaId the aa id
     * @param projectId the project id
     * @param userName the user name
     * @return ProjectApplicationResponse
     */
    ProjectApplicationResponse findProjectAppByAAIdProjectIdUserName(final String aaId,
            final String projectId, final String userName);

    /**
     * Find project app by AA id project id user id.
     *
     * @param aaId the aa id
     * @param projectId the project id
     * @param userId the user id
     * @return ProjectApplicationResponse
     */
    ProjectApplicationResponse findProjectAppByAAIdProjectIdUserId(final String aaId,
            final String projectId, final String userId);

    /**
     * Find project app by AA id project id user id.
     *
     * @param aaId the aa id
     * @param projectId the project id
     * @return ProjectApplicationResponse
     */
    ProjectApplicationResponse findProjectAppByAAIdProjectIdUserId(final String aaId,
            final String projectId);

    /**
     * Find project applications by AA id and proj id and user name.
     *
     * @param aaId the aa id
     * @param tkt the tkt
     * @param projectId the project id
     * @param userName the user name
     * @param validationRequest the validation request
     * @return ProjectApplciationMenuWrapper
     */
    ProjectApplciationMenuWrapper findProjectApplicationsByAAIdAndProjIdAndUserName(final String aaId, final String tkt,
            final String projectId, final String userName,
            final ValidationRequest validationRequest);

    /**
     * Find by name.
     *
     * @param name the name
     * @return the project applications tbl
     */
    ProjectApplicationsTbl findByName(final String name);
    
    
    /**
     * Find by name.
     *
     * @param name the name
     * @param validationRequest the validation request
     * @return the project applications tbl
     */
    ProjectApplicationsTbl findByName(final String name, final ValidationRequest validationRequest);

	/**
	 * Multi update.
	 *
	 * @param projectAppRequests the project app requests
	 * @param httpServletRequest the http servlet request
	 * @return the project application response
	 */
	ProjectApplicationResponse multiUpdate(List<ProjectApplicationRequest> projectAppRequests,
			HttpServletRequest httpServletRequest);

	/**
	 * Find project apps and child byd project app name.
	 *
	 * @param projectAppName the project app name
	 * @param tkt the tkt
	 * @param userName the user name
	 * @param validationRequest the validation request
	 * @return the project applciation menu wrapper
	 */
	ProjectApplciationMenuWrapper findProjectAppsAndChildBydProjectAppName(String projectAppName, String tkt,
			String userName, ValidationRequest validationRequest);

	/**
	 * Update for batch.
	 *
	 * @param projectApplicationRequest the project application request
	 * @return the project applications tbl
	 */
	ProjectApplicationsTbl updateForBatch(ProjectApplicationRequest projectApplicationRequest);
}
