package com.magna.xmbackend.mgr.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Component;

import com.magna.xmbackend.audit.mgr.SiteAuditMgr;
import com.magna.xmbackend.controller.AuthController;
import com.magna.xmbackend.entities.AdminAreaProjAppRelTbl;
import com.magna.xmbackend.entities.AdminAreaProjectRelTbl;
import com.magna.xmbackend.entities.AdminAreaStartAppRelTbl;
import com.magna.xmbackend.entities.AdminAreaUserAppRelTbl;
import com.magna.xmbackend.entities.AdminAreasTbl;
import com.magna.xmbackend.entities.IconsTbl;
import com.magna.xmbackend.entities.LanguagesTbl;
import com.magna.xmbackend.entities.ProjectApplicationsTbl;
import com.magna.xmbackend.entities.ProjectsTbl;
import com.magna.xmbackend.entities.SiteAdminAreaRelTbl;
import com.magna.xmbackend.entities.SiteTranslationTbl;
import com.magna.xmbackend.entities.SitesTbl;
import com.magna.xmbackend.entities.StartApplicationsTbl;
import com.magna.xmbackend.entities.UserApplicationsTbl;
import com.magna.xmbackend.entities.UserTkt;
import com.magna.xmbackend.exception.CannotCreateObjectException;
import com.magna.xmbackend.exception.XMObjectNotFoundException;
import com.magna.xmbackend.jpa.dao.SiteJpaDao;
import com.magna.xmbackend.jpa.dao.UserTktJpaDao;
import com.magna.xmbackend.mgr.ProjectApplicationMgr;
import com.magna.xmbackend.mgr.ProjectMgr;
import com.magna.xmbackend.mgr.SiteMgr;
import com.magna.xmbackend.mgr.StartApplicationMgr;
import com.magna.xmbackend.mgr.UserApplicationMgr;
import com.magna.xmbackend.rel.mgr.AdminAreaProjectRelMgr;
import com.magna.xmbackend.utils.MessageMaker;
import com.magna.xmbackend.utils.Validator;
import com.magna.xmbackend.vo.enums.Application;
import com.magna.xmbackend.vo.enums.Status;
import com.magna.xmbackend.vo.jpa.site.SiteAdminAreaRelIdWithAdminArea;
import com.magna.xmbackend.vo.jpa.site.SiteAdminAreaRelIdWithAdminAreaResponse;
import com.magna.xmbackend.vo.jpa.site.SiteRequest;
import com.magna.xmbackend.vo.jpa.site.SiteResponse;
import com.magna.xmbackend.vo.jpa.site.SiteTranslation;
import com.magna.xmbackend.vo.permission.ValidationRequest;
import com.magna.xmbackend.vo.project.ProjectResponse;

/**
 * The Class SiteMgrImpl.
 */
@Component
public class SiteMgrImpl implements SiteMgr {

    /** The Constant LOG. */
    private static final Logger LOG
            = LoggerFactory.getLogger(SiteMgrImpl.class);

    /** The site jpa dao. */
    @Autowired
    private SiteJpaDao siteJpaDao;

    /** The admin area project rel mgr. */
    @Autowired
    private AdminAreaProjectRelMgr adminAreaProjectRelMgr;

    /** The project mgr. */
    @Autowired
    private ProjectMgr projectMgr;

    /** The user application mgr. */
    @Autowired
    private UserApplicationMgr userApplicationMgr;

    /** The project application mgr. */
    @Autowired
    private ProjectApplicationMgr projectApplicationMgr;

    /** The start application mgr. */
    @Autowired
    private StartApplicationMgr startApplicationMgr;

    /** The validator. */
    @Autowired
    private Validator validator;

    /** The message maker. */
    @Autowired
    private MessageMaker messageMaker;
    
    /** The user tkt jpa dao. */
    @Autowired
    private UserTktJpaDao userTktJpaDao;

    /** The site audit mgr. */
    @Autowired
    private SiteAuditMgr siteAuditMgr;
    
    /** The auth controller. */
    @Autowired
    private AuthController authController;

    /**
     * Find all.
     *
     * @param validationRequest the validation request
     * @return SiteResponse
     */
    @Override
    public final SiteResponse findAll(final ValidationRequest validationRequest) {
        LOG.info(">> findAll");
        Iterable<SitesTbl> sitesTbls = this.siteJpaDao.findAll();
        final boolean isViewInactive = validator.isViewInactiveAllowed(validationRequest);
        LOG.info("findAll isViewInactive={}", isViewInactive);
        sitesTbls = validator.filterSiteResponse(isViewInactive, sitesTbls);
        final SiteResponse siteResponse = new SiteResponse(sitesTbls);
        LOG.info("<< findAll");
        return siteResponse;
    }

    /**
     * Creates the.
     *
     * @param siteRequest the site request
     * @return SitesTbl
     */
    @Override
    public final SitesTbl create(final SiteRequest siteRequest, HttpServletRequest httpServletRequest) {
        LOG.info(">> create");
        SitesTbl stOut = null;
		//try {
			final SitesTbl stIn = this.convert2Entity(siteRequest, false);
			stOut = this.siteJpaDao.save(stIn);
			//this.siteAuditMgr.siteCreateSuccessAudit(httpServletRequest, stOut);
		/*} catch (Exception ex) {
			if (ex instanceof CannotCreateObjectException) {
				this.siteAuditMgr.siteCreateFailureAudit(httpServletRequest, siteRequest, "Site already exist.");
			}
            throw ex;
		}*/
        LOG.info("<< create");
        return stOut;
    }

    /**
     * Convert 2 entity.
     *
     * @param siteRequest the site request
     * @param isUpdate the is update
     * @return SitesTbl
     */
    private SitesTbl convert2Entity(final SiteRequest siteRequest,
            final boolean isUpdate) {
        String id = UUID.randomUUID().toString();
        if (isUpdate) {
            id = siteRequest.getId();
        }

        final Date date = new Date();
        final String iconId = siteRequest.getIconId();
        final String siteName = siteRequest.getName();
        final String status = siteRequest.getStatus().toString();
        final List<SiteTranslation> translations = siteRequest.getSiteTranslations();

        if (!isUpdate
                && null != this.findSiteIdForName(siteName)) {
            final Map<String, String[]> paramMap
                    = this.messageMaker.getSiteNameWithi18nCode(translations, siteName);
            throw new CannotCreateObjectException(
                    "Site name already found", "ERR0002", paramMap);
        }

        final SitesTbl sitesTbl = new SitesTbl(id);
        sitesTbl.setName(siteName);
        sitesTbl.setIconId(new IconsTbl(iconId));
        sitesTbl.setStatus(status);
        sitesTbl.setCreateDate(date);
        sitesTbl.setUpdateDate(date);
        final List<SiteTranslationTbl> stts = new ArrayList<>();

        if(translations != null &&  translations.size() > 0) {
        	for (SiteTranslation translation : translations) {
                String transId = UUID.randomUUID().toString();
                if (isUpdate) {
                    transId = translation.getId();
                }
                final String languageCode = translation.getLanguageCode();
                final String description = translation.getDescription();
                final String remarks = translation.getRemarks();
                final SiteTranslationTbl stt = new SiteTranslationTbl(transId);
                stt.setCreateDate(date);
                stt.setUpdateDate(date);
                stt.setDescription(description);
                stt.setLanguageCode(new LanguagesTbl(languageCode));
                stt.setRemarks(remarks);
                stt.setSiteId(new SitesTbl(id));
                stts.add(stt);
            }
		} else {
			SiteTranslationTbl siteTranslationTblEn = new SiteTranslationTbl(UUID.randomUUID().toString(), date, date,
					new LanguagesTbl("en"), sitesTbl);
			SiteTranslationTbl siteTranslationTblGe = new SiteTranslationTbl(UUID.randomUUID().toString(), date, date,
					new LanguagesTbl("de"), sitesTbl);
			stts.add(siteTranslationTblEn);
			stts.add(siteTranslationTblGe);
		}

        sitesTbl.setSiteTranslationTblCollection(stts);
        return sitesTbl;
    }

    /**
     * Update.
     *
     * @param siteRequest the site request
     * @return SitesTbl
     */
    @Override
    public final SitesTbl update(final SiteRequest siteRequest) {
        LOG.info(">> update");
        final SitesTbl stIn = this.convert2Entity(siteRequest, true);
        final SitesTbl stOut = this.siteJpaDao.save(stIn);
        LOG.info("<< update");

        return stOut;
    }
    
    
    /* (non-Javadoc)
     * @see com.magna.xmbackend.mgr.SiteMgr#updateForBatch(com.magna.xmbackend.vo.jpa.site.SiteRequest)
     */
    @Override
	public SitesTbl updateForBatch(SiteRequest siteRequest, HttpServletRequest httpServletRequest) {
		
    	SitesTbl sitesTbl = this.findByNameForBatch(siteRequest.getName());
    	if (sitesTbl == null) {
    		throw new XMObjectNotFoundException("Site bot found", "S_ERR0001", new String[]{siteRequest.getName()});
    	}
		String status = (siteRequest.getStatus() == null)?(sitesTbl.getStatus()):siteRequest.getStatus().name();
		siteRequest.setId(sitesTbl.getSiteId());
		siteRequest.setStatus(Status.valueOf(status));
		
		String iconId = (siteRequest.getIconId() == null) ? (sitesTbl.getIconId().getIconId())
				: siteRequest.getIconId();

		siteRequest.setIconId(iconId);
		List<SiteTranslation> siteTranslations = siteRequest.getSiteTranslations();
		if (siteTranslations != null && siteTranslations.size() > 0) {
			for (SiteTranslation siteTranslation : siteTranslations) {
				for (SiteTranslationTbl siteTranslationTbl : sitesTbl.getSiteTranslationTblCollection()) {
					if (siteTranslationTbl.getLanguageCode().getLanguageCode()
							.equals(siteTranslation.getLanguageCode())) {
						siteTranslation.setId(siteTranslationTbl.getSiteTranslationId());
					}
				}
			}
		}
		
		SitesTbl stOut = null;
		try {
			final SitesTbl stIn = this.convert2Entity(siteRequest, true);
			stOut = this.siteJpaDao.save(stIn);
			this.siteAuditMgr.siteUpdateSuccessAudit(httpServletRequest, siteRequest);
		} catch (Exception ex) {
			String name = siteRequest.getName();
			LOG.error("Exception / Error in updating site with name {} " + "and exception msg {}", name,
					ex.getMessage());
			this.siteAuditMgr.siteUpdateFailureAudit(httpServletRequest, siteRequest, ex.getMessage());
			throw ex;
		}
		return stOut;
	}
    

    /**
     * Update by id.
     *
     * @param status the status
     * @param id the id
     * @return boolean
     */
    @Override
    public final boolean updateById(final String status, final String id) {
        LOG.info(">> updateById {}", id);
        final Date date = new Date();
        boolean isUpdated = false;
        final int out = this.siteJpaDao
                .setStatusAndUpdateDateForSiteTbl(status, date, id);
        LOG.debug("is Modified status value {}", out);
        if (out > 0) {
            isUpdated = true;
        }
        LOG.info("<< updateById");
        return isUpdated;
    }

    /**
     * Find by id.
     *
     * @param id the id
     * @return SitesTbl
     */
    @Override
    public final SitesTbl findById(final String id) {
        LOG.info(">> findById");
        final SitesTbl st = this.siteJpaDao.findOne(id);
        LOG.info("<< findById");
        return st;
    }

    /**
     * Delete by id.
     *
     * @param id the id
     * @return boolean
     */
    @Override
    public boolean deleteById(final String id) {
        LOG.info(">> deleteById");
        boolean isDeleted = false;
        try {
            this.siteJpaDao.delete(id);
            isDeleted = true;
        } catch (Exception e) {
            if (e instanceof EmptyResultDataAccessException) {
                final String[] param = {id};
                //throw from here and catch this in multidelete catch block
                throw new XMObjectNotFoundException("Object not found", "P_ERR0010", param);
            }
        }

        LOG.info("<< deleteById");
        return isDeleted;
    }

    /* (non-Javadoc)
     * @see com.magna.xmbackend.mgr.SiteMgr#multiDelete(java.util.Set, javax.servlet.http.HttpServletRequest)
     */
    @Override
    public SiteResponse multiDelete(Set<String> siteIds,
            HttpServletRequest httpServletRequest) {
        LOG.info(">> multiDelete");

        final List<Map<String, String>> statusMaps = new ArrayList<>();

        for (String siteId : siteIds) {
            String name = "";
            try {
                SitesTbl sitesTbl2Del = this.findById(siteId);
                if (null != sitesTbl2Del) {
                    name = sitesTbl2Del.getName();
                }
                this.deleteById(siteId);
                this.siteAuditMgr.siteDeleteSuccessAudit(httpServletRequest,
                            siteId, name);
            } catch (XMObjectNotFoundException objectNotFound) {
                String errorMsg;
                if (!"".equalsIgnoreCase(siteId)) {
                    errorMsg = "Site with id " + siteId + " cannot be deleted";
                } else {
                    errorMsg = "Site with id " + siteId + " and with name "
                            + name + "cannot be deleted";
                }
                this.siteAuditMgr.siteDeleteFailureAudit(httpServletRequest,
                        name, errorMsg);
                Map<String, String> statusMap
                        = messageMaker.extractFromException(objectNotFound);
                statusMaps.add(statusMap);
            }
        }

        SiteResponse siteResponse = new SiteResponse(statusMaps);

        LOG.info(">> multiDelete");
        return siteResponse;
    }

    /**
     * Find admin area by id.
     *
     * @param id the id
     * @param validationRequest the validation request
     * @return SiteAdminAreaRelIdWithAdminAreaResponse
     */
    @Override
    public SiteAdminAreaRelIdWithAdminAreaResponse findAdminAreaById(final String id,
            final ValidationRequest validationRequest) {
        LOG.info(">> findAdminAreaById {}", id);
        final boolean isViewInactive = validator.isViewInactiveAllowed(validationRequest);
        LOG.info("findAdminAreaById isViewInactive={}", isViewInactive);
        final SitesTbl foundSitesTbl = validator.validateSite(id, isViewInactive);
        final SiteAdminAreaRelIdWithAdminAreaResponse response = this.getAdminAreas(foundSitesTbl, isViewInactive);
        LOG.info("<< findAdminAreaById");
        return response;
    }

    /**
     * Gets the admin areas.
     *
     * @param sitesTbl the sites tbl
     * @param isViewInactive the is view inactive
     * @return SiteAdminAreaRelIdWithAdminAreaResponse
     */
    private SiteAdminAreaRelIdWithAdminAreaResponse getAdminAreas(final SitesTbl sitesTbl,
            final boolean isViewInactive) {
        Collection<SiteAdminAreaRelTbl> siteAdminAreaRelTbls
                = sitesTbl.getSiteAdminAreaRelTblCollection();
        if (siteAdminAreaRelTbls.isEmpty()) {
            throw new XMObjectNotFoundException("Admin Areas not available for site", "S_ERR0003");
        }
        siteAdminAreaRelTbls = validator.filterSiteAdminAreaRel(isViewInactive, siteAdminAreaRelTbls);
        final List<SiteAdminAreaRelIdWithAdminArea> siteAdminAreaRelIdWithAdminAreas = new ArrayList<>();
        for (final SiteAdminAreaRelTbl siteAdminAreaRelTbl : siteAdminAreaRelTbls) {
            final String siteAdminAreaRelId = siteAdminAreaRelTbl.getSiteAdminAreaRelId();
            AdminAreasTbl adminAreasTbl = siteAdminAreaRelTbl.getAdminAreaId();
            adminAreasTbl = validator.filterAdminAreaResponse(isViewInactive, adminAreasTbl);
            if (null != adminAreasTbl && null != adminAreasTbl.getAdminAreaId()) {
                final SiteAdminAreaRelIdWithAdminArea saariwaa = new SiteAdminAreaRelIdWithAdminArea();
                saariwaa.setAdminAreasTbl(adminAreasTbl);
                saariwaa.setSiteAdminAreaRelId(siteAdminAreaRelId);
                siteAdminAreaRelIdWithAdminAreas.add(saariwaa);
            }
        }
        final SiteAdminAreaRelIdWithAdminAreaResponse response
                = new SiteAdminAreaRelIdWithAdminAreaResponse(siteAdminAreaRelIdWithAdminAreas);
        return response;
    }

    /**
     * Find admin area by name.
     *
     * @param name the name
     * @param validationRequest the validation request
     * @return SiteAdminAreaRelIdWithAdminAreaResponse
     */
    @Override
    public final SiteAdminAreaRelIdWithAdminAreaResponse findAdminAreaByName(final String name,
            final ValidationRequest validationRequest) {
        LOG.info(">> findAdminAreaByName {}", name);
        final String siteId = this.findSiteIdForName(name);
        if (null == siteId) {
            final String[] param = {name};
            throw new XMObjectNotFoundException("Given Site name not found", "S_ERR0001", param);
        }
        final boolean isViewInactive = validator.isViewInactiveAllowed(validationRequest);
        LOG.info("findAdminAreaByName isViewInactive={}", isViewInactive);
        final SitesTbl sitesTbl = validator.validateSite(siteId, isViewInactive);
        final SiteAdminAreaRelIdWithAdminAreaResponse response = this.getAdminAreas(sitesTbl, isViewInactive);
        LOG.info("<< findAdminAreaByName");
        return response;
    }

    /**
     * Find by name.
     *
     * @param name the name
     * @param tkt the tkt
     * @param validationRequest the validation request
     * @return SitesTbl
     */
    @Override
    public final SitesTbl findByName(final String name, final String tkt,
            final ValidationRequest validationRequest) {
        LOG.info(">> findByName {}", name);
        final String siteId = findSiteIdForName(name);
        if (null == siteId) {
            final String[] param = {name};
            throw new XMObjectNotFoundException("Given Site name not found", "S_ERR0001", param);
        }
        boolean isViewInactive = validator.isViewInactiveAllowed(validationRequest);
        LOG.info("findByName isViewInactive={}", isViewInactive);
        if (!isViewInactive) {
            final UserTkt userTktTbl = this.userTktJpaDao.findByTktAndApplicationName(this.authController.hash(tkt),
                    Application.CAX_ADMIN_MENU.name());
            if (userTktTbl != null) {
                validationRequest.setUserName(userTktTbl.getUsername());
                isViewInactive = validator.isViewInactiveAllowed(validationRequest);
            }
        }
        final SitesTbl sitesTblByName = validator.validateSite(siteId, isViewInactive);
        LOG.info("<< findByName");
        return sitesTblByName;
    }

    /**
     * Find by name.
     *
     * @param name the name
     * @return SitesTbl
     */
    private SitesTbl findByName(final String name) {
        LOG.info(">> findByName {}", name);
        String siteId;
        siteId = findSiteIdForName(name);
        if (null == siteId) {
            final String[] param = {name};
            throw new XMObjectNotFoundException("Given Site name not found", "S_ERR0001", param);
        }
        final SitesTbl sitesTblByName = this.findById(siteId);
        LOG.info("<< findByName");
        return sitesTblByName;
    }

    /**
     * Find site id for name.
     *
     * @param name the name
     * @return Site Name
     */
    private String findSiteIdForName(final String name) {
        SitesTbl sitesTbl = this.siteJpaDao.findByNameIgnoreCase(name);
        String siteId = null;
        if (sitesTbl != null) {
            siteId = sitesTbl.getSiteId();
        }
        LOG.info(">> findSiteIdForName - Site id for name is {} - {}",
                name, siteId);
        return siteId;
    }

    /**
     * Find project by id.
     *
     * @param id the id
     * @param validationRequest the validation request
     * @return ProjectResponse
     */
    @Override
    public final ProjectResponse findProjectById(final String id,
            final ValidationRequest validationRequest) {
        ProjectResponse projectResponse = null;
        LOG.info(">> findProjectById {}", id);
        final boolean isViewInactive = validator.isViewInactiveAllowed(validationRequest);
        LOG.info("findProjectById isViewInactive={}", isViewInactive);
        final SitesTbl sitesTbl = validator.validateSite(id, isViewInactive);
        if (null != sitesTbl) {
            projectResponse = this.getProjects(sitesTbl, isViewInactive);
        } else {
            final String[] param = {id};
            throw new XMObjectNotFoundException("Given SiteId not found", "S_ERR0002", param);
        }
        LOG.info("<< findProjectById");
        return projectResponse;
    }

    /**
     * Gets the projects.
     *
     * @param sitesTbl the sites tbl
     * @param isViewInactive the is view inactive
     * @return ProjectResponse
     */
    private ProjectResponse getProjects(final SitesTbl sitesTbl,
            final boolean isViewInactive) {
        ProjectResponse projectResponse;
        Collection<SiteAdminAreaRelTbl> siteAdminAreaRelTbls = sitesTbl.getSiteAdminAreaRelTblCollection();
        if (siteAdminAreaRelTbls.isEmpty()) {
            throw new XMObjectNotFoundException("Admin Areas not available for site", "S_ERR0003");
        }
        // Filter site admin area relation by isViewInactive allowed
        siteAdminAreaRelTbls = validator.filterSiteAdminAreaRel(isViewInactive, siteAdminAreaRelTbls);
        final List<ProjectsTbl> projectsTbls = new ArrayList<>();
        for (SiteAdminAreaRelTbl siteAdminAreaRelTbl : siteAdminAreaRelTbls) {
            AdminAreasTbl filterAdminAreaTbl = validator.filterAdminAreaResponse(isViewInactive, siteAdminAreaRelTbl.getAdminAreaId());
            if (null != filterAdminAreaTbl) {
                Iterable<AdminAreaProjectRelTbl> adminAreaProjectRelTbls = adminAreaProjectRelMgr.findAdminAreaProjectRelTblsBySiteAdminAreaRelId(siteAdminAreaRelTbl);
                if (null != adminAreaProjectRelTbls) {
                    // Filter admin area project relation by isViewInactive allowed
                    adminAreaProjectRelTbls = validator.filterAdminAreaProjectRel(isViewInactive, adminAreaProjectRelTbls);
                    for (AdminAreaProjectRelTbl adminAreaProjectRelTbl
                            : adminAreaProjectRelTbls) {
                        ProjectsTbl projectsTbl = adminAreaProjectRelTbl.getProjectId();
                        LOG.info("projectsTbl={}", projectsTbl.getProjectId());
                        projectsTbl = validator.filterProjectResponse(isViewInactive, projectsTbl);
                        if (null != projectsTbl && null != projectsTbl.getProjectId()) {
                            projectsTbls.add(projectsTbl);
                        }
                    }
                }
            }
        }
        if (projectsTbls.isEmpty()) {
            throw new XMObjectNotFoundException("No project relationship found for the site", "S_ERR0004");
        }
        projectResponse = new ProjectResponse(projectsTbls);
        return projectResponse;
    }

    /**
     * Find project by name.
     *
     * @param name the name
     * @param validationRequest the validation request
     * @return ProjectResponse
     */
    @Override
    public final ProjectResponse findProjectByName(final String name,
            final ValidationRequest validationRequest) {
        LOG.info(">> findProjectByName {}", name);
        SitesTbl sitesTbl = this.findByName(name);
        if (null == sitesTbl) {
            final String[] param = {name};
            throw new XMObjectNotFoundException("Given Site name not found", "S_ERR0001", param);
        }
        final boolean isViewInactive = validator.isViewInactiveAllowed(validationRequest);
        LOG.info("findProjectByName isViewInactive={}", isViewInactive);
        sitesTbl = validator.validateSite(sitesTbl.getSiteId(), isViewInactive);
        final ProjectResponse projectResponse = this.getProjects(sitesTbl, isViewInactive);
        LOG.info("<< findProjectByName");
        return projectResponse;
    }

    /**
     * Find site by project id.
     *
     * @param projectId the project id
     * @return SiteResponse
     */
    @Override
    public final SiteResponse findSiteByProjectId(final String projectId) {
        SiteResponse siteResponse = null;
        LOG.info(">> findSiteByProjectId {}", projectId);
        final ProjectsTbl projectsTbl = this.projectMgr.findById(projectId);
        if (null != projectsTbl) {
            final Collection<AdminAreaProjectRelTbl> adminAreaProjectRelTblCollection = projectsTbl.getAdminAreaProjectRelTblCollection();
            if (!adminAreaProjectRelTblCollection.isEmpty()) {
                final Collection<SitesTbl> sitesTbls = new ArrayList<>();
                for (AdminAreaProjectRelTbl adminAreaProjectRelTbl : adminAreaProjectRelTblCollection) {
                    final SiteAdminAreaRelTbl siteAdminAreaRelTbl = adminAreaProjectRelTbl.getSiteAdminAreaRelId();
                    if (null != siteAdminAreaRelTbl) {
                        final SitesTbl sitesTbl = siteAdminAreaRelTbl.getSiteId();
                        sitesTbls.add(sitesTbl);
                    }
                }
                if (sitesTbls.isEmpty()) {
                    throw new XMObjectNotFoundException("No Admin Area Project relationship found for the Site Admin Area", "S_ERR0005");
                }
                siteResponse = new SiteResponse(sitesTbls);
            } else {
                throw new XMObjectNotFoundException("No Admin Area Project relationship found for the project", "S_ERR0006");
            }
        } else {
            final String[] param = {projectId};
            throw new XMObjectNotFoundException("No Projects found", "S_ERR0007", param);
        }
        LOG.info("<< findSiteByProjectId");
        return siteResponse;
    }

    /**
     * Find sites by user app id.
     *
     * @param userAppId the user app id
     * @return SiteResponse
     */
    @Override
    public final SiteResponse findSitesByUserAppId(final String userAppId) {
        LOG.info(">> findSitesByUserAppId {}", userAppId);
        SiteResponse siteResponse = null;
        final UserApplicationsTbl userApplicationsTbl = this.userApplicationMgr.findById(userAppId);
        if (null != userApplicationsTbl) {
            Collection<AdminAreaUserAppRelTbl> adminAreaUserAppRelTblCollection = userApplicationsTbl.getAdminAreaUserAppRelTblCollection();
            if (!adminAreaUserAppRelTblCollection.isEmpty()) {
                Collection<SitesTbl> sitesTbls = new ArrayList<>();
                for (AdminAreaUserAppRelTbl adminAreaUserAppRelTbl : adminAreaUserAppRelTblCollection) {
                    final SiteAdminAreaRelTbl siteAdminAreaRelTbl = adminAreaUserAppRelTbl.getSiteAdminAreaRelId();
                    if (null != siteAdminAreaRelTbl) {
                        final SitesTbl sitesTbl = siteAdminAreaRelTbl.getSiteId();
                        sitesTbls.add(sitesTbl);
                    }
                }
                if (sitesTbls.isEmpty()) {
                    throw new XMObjectNotFoundException("No Admin Area User App relationship found for the Site Admin Area", "S_ERR0008");
                }
                siteResponse = new SiteResponse(sitesTbls);
            } else {
                throw new XMObjectNotFoundException("No Admin Area User App relationship found for the User App", "S_ERR0009");
            }
        } else {
            final String[] param = {userAppId};
            throw new XMObjectNotFoundException("No User App found", "S_ERR0010", param);
        }
        LOG.info("<< findSitesByUserAppId");

        return siteResponse;
    }

    /**
     * Find sites by project app id.
     *
     * @param projectAppId the project app id
     * @return SiteResponse
     */
    @Override
    public final SiteResponse findSitesByProjectAppId(final String projectAppId) {
        SiteResponse siteResponse = null;
        LOG.info(">> findSitesByProjectAppId {}", projectAppId);
        final ProjectApplicationsTbl projectApplicationsTbl = this.projectApplicationMgr.findById(projectAppId);
        if (null != projectApplicationsTbl) {
            final Collection<AdminAreaProjAppRelTbl> adminAreaProjAppRelTblCollection = projectApplicationsTbl.getAdminAreaProjAppRelTblCollection();
            if (!adminAreaProjAppRelTblCollection.isEmpty()) {
                final Collection<SitesTbl> sitesTbls = new ArrayList<>();
                for (AdminAreaProjAppRelTbl adminAreaProjAppRelTbl : adminAreaProjAppRelTblCollection) {
                    final AdminAreaProjectRelTbl adminAreaProjectRelTbl = adminAreaProjAppRelTbl.getAdminAreaProjectRelId();
                    if (null != adminAreaProjectRelTbl) {
                        final SiteAdminAreaRelTbl siteAdminAreaRelTbl = adminAreaProjectRelTbl.getSiteAdminAreaRelId();
                        if (null != siteAdminAreaRelTbl) {
                            final SitesTbl sitesTbl = siteAdminAreaRelTbl.getSiteId();
                            sitesTbls.add(sitesTbl);
                        }
                    } else {
                        throw new XMObjectNotFoundException("No Admin Area Project App relationship found for the Admin Area Project Relation", "S_ERR0014");
                    }
                }
                if (sitesTbls.isEmpty()) {
                    throw new XMObjectNotFoundException("No Admin Area Project relationship found for the Site Admin Area", "S_ERR0015");
                }
                siteResponse = new SiteResponse(sitesTbls);
            } else {
                throw new XMObjectNotFoundException("No Admin Area Proj App relationship found for the Project App", "S_ERR0016");
            }
        } else {
            final String[] param = {projectAppId};
            throw new XMObjectNotFoundException("No Project App found", "S_ERR0017", param);
        }
        LOG.info("<< findSitesByProjectAppId ");
        return siteResponse;
    }

    /**
     * Find sites by start app id.
     *
     * @param startAppId the start app id
     * @return SiteResponse
     */
    @Override
    public final SiteResponse findSitesByStartAppId(final String startAppId) {
        SiteResponse siteResponse = null;
        LOG.info(">> findSitesByStartAppId {}", startAppId);
        final StartApplicationsTbl startApplicationsTbl = this.startApplicationMgr.findById(startAppId);
        if (null != startApplicationsTbl) {
            final Collection<AdminAreaStartAppRelTbl> adminAreaStartAppRelTblCollection = startApplicationsTbl.getAdminAreaStartAppRelTblCollection();
            if (!adminAreaStartAppRelTblCollection.isEmpty()) {
                final Collection<SitesTbl> sitesTbls = new ArrayList<>();
                for (AdminAreaStartAppRelTbl adminAreaStartAppRelTbl : adminAreaStartAppRelTblCollection) {
                    final SiteAdminAreaRelTbl siteAdminAreaRelTbl = adminAreaStartAppRelTbl.getSiteAdminAreaRelId();
                    if (null != siteAdminAreaRelTbl) {
                        final SitesTbl sitesTbl = siteAdminAreaRelTbl.getSiteId();
                        sitesTbls.add(sitesTbl);
                    }
                }
                if (sitesTbls.isEmpty()) {
                    throw new XMObjectNotFoundException("No Admin Area Start App relationship found for the Site Admin Area", "S_ERR0011");
                }
                siteResponse = new SiteResponse(sitesTbls);
            } else {
                throw new XMObjectNotFoundException("No Admin Area Start App relationship found for the Start App", "S_ERR0012");
            }
        } else {
            final String[] param = {startAppId};
            throw new XMObjectNotFoundException("No Start App found", "S_ERR0013", param);
        }
        LOG.info("<< findSitesByStartAppId ");
        return siteResponse;
    }

    /**
     * Find sites by admin area id.
     *
     * @param adminAreaId the admin area id
     * @param validationRequest the validation request
     * @return SiteResponse
     */
    @Override
    public final SiteResponse findSitesByAdminAreaId(final String adminAreaId,
            final ValidationRequest validationRequest) {
        SiteResponse siteResponse = null;
        LOG.info(">> findSitesByAdminAreaId {}", adminAreaId);
        final boolean isViewInactive = validator.isViewInactiveAllowed(validationRequest);
        LOG.info("findSitesByAdminAreaId isViewInactive={}", isViewInactive);
        final AdminAreasTbl adminAreasTbl = validator.validateAdminArea(adminAreaId, isViewInactive);
        if (null != adminAreasTbl) {
            Collection<SiteAdminAreaRelTbl> siteAdminAreaRelTblCollection = adminAreasTbl.getSiteAdminAreaRelTblCollection();
            final Collection<SitesTbl> sitesTbls = new ArrayList<>();
            siteAdminAreaRelTblCollection = validator.filterSiteAdminAreaRel(isViewInactive, siteAdminAreaRelTblCollection);
            for (final SiteAdminAreaRelTbl siteAdminAreaRelTbl : siteAdminAreaRelTblCollection) {
                SitesTbl sitesTbl = siteAdminAreaRelTbl.getSiteId();
                sitesTbl = validator.filterSiteResponse(isViewInactive, sitesTbl);
                if (null != sitesTbl && null != sitesTbl.getSiteId()) {
                    sitesTbls.add(sitesTbl);
                }
            }
            siteResponse = new SiteResponse(sitesTbls);
        }
        LOG.info("<< findSitesByAdminAreaId ");
        return siteResponse;
    }

	/* (non-Javadoc)
	 * @see com.magna.xmbackend.mgr.SiteMgr#multiUpdate(java.util.List, javax.servlet.http.HttpServletRequest)
	 */
	@Override
	public SiteResponse multiUpdate(List<SiteRequest> siteRequests, HttpServletRequest httpServletRequest) {
		LOG.info(">> multiUpdate");
		List<String> siteNames = new ArrayList<>();
		siteRequests.forEach(siteRequest -> {
			String siteName = siteRequest.getName();
			String status = siteRequest.getStatus().name();
			try {
				boolean updateById = this.updateById(status, siteRequest.getId());
				if (updateById) {
					// success & audit it
					this.siteAuditMgr.siteStatusUpdateSuccessAuditor(siteName, status, httpServletRequest);
				} else {
					siteNames.add(siteName);
				}
			} catch (Exception ex) {
				this.siteAuditMgr.siteStatusUpdateFailureAuditor(siteName, status, ex, httpServletRequest);
			}

		});
		SiteResponse siteResponse = new SiteResponse();
		siteResponse.setStatusUpdatationFailedList(siteNames);
		LOG.info("<< multiUpdate");
		return siteResponse;
	}

	/* (non-Javadoc)
	 * @see com.magna.xmbackend.mgr.SiteMgr#findByNameForBatch(java.lang.String)
	 */
	@Override
	public SitesTbl findByNameForBatch(String name) {
		LOG.info(">> findByNameForBatch");
		SitesTbl sitesTbl = this.siteJpaDao.findByNameIgnoreCase(name);
		if (null == sitesTbl) {
			String[] param = { name };
			throw new XMObjectNotFoundException("Site not found", "P_ERR0036", param);
		}
		LOG.info("<< findByNameForBatch");
		return sitesTbl;
	}

}
