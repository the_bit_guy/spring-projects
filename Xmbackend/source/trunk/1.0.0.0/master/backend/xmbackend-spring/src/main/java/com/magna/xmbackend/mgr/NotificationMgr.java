/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.mgr;

import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import com.magna.xmbackend.entities.EmailNotificationEventTbl;
import com.magna.xmbackend.entities.EmailNotifyToUserRelTbl;
import com.magna.xmbackend.entities.EmailTemplateTbl;
import com.magna.xmbackend.vo.notification.EmailTemplateRequest;
import com.magna.xmbackend.vo.notification.EmailTemplateResponse;
import com.magna.xmbackend.vo.notification.NotificationConfigResponse;
import com.magna.xmbackend.vo.notification.NotificationRequest;
import com.magna.xmbackend.vo.notification.NotificationResponse;

/**
 *
 * @author dhana
 */
public interface NotificationMgr {

    Iterable<EmailNotifyToUserRelTbl> findAllPendingNotification();

    List<NotificationConfigResponse> findAllNotificationConfig();
    
    NotificationConfigResponse createNotification(NotificationRequest notificationRequest);
    
    NotificationConfigResponse updateNotification(NotificationRequest notificationRequest);
    
    List<NotificationConfigResponse> findByEvent(String event);

	boolean updateNotificationStatus(HttpServletRequest httpServletRequest, String id, String status);
	
	EmailTemplateTbl saveOrUpdateEmailTemplate(EmailTemplateRequest emailTemplateRequest, boolean isUpdate);

	boolean deleteEmailTemplate(String templateId);

	List<EmailTemplateTbl> findAll();
	
	List<EmailNotificationEventTbl> findAllEvents();

	boolean deleteNotificationConfig(HttpServletRequest httpServletRequest, String notifConfigId);
	
	NotificationResponse multiDeleteNotificationConfig(HttpServletRequest httpServletRequest, Set<String> notifConfigIds);

	EmailTemplateResponse multiDeleteEmailTemplate(HttpServletRequest httpServletRequest, Set<String> templateIds);

	boolean updateNotificationConfigStatus(HttpServletRequest httpServletRequest, String configId, String status);
	
}
