/**
 * 
 */
package com.magna.xmbackend.mgr;

import com.magna.xmbackend.entities.UserSettingsTbl;
import com.magna.xmbackend.vo.userSettings.UserSettingsRequest;

/**
 * @author Bhabadyuti Bal
 *
 */
public interface UserSettingsMgr {

	UserSettingsTbl findUserSettingsBySiteId(final String userName, final String siteId);
	
	
	UserSettingsTbl saveOrUpdateProject(final String userName, final UserSettingsRequest userSettingsRequest, final String tkt);
	
	
	UserSettingsTbl saveOrUpdateSettings(final String userName, final UserSettingsRequest userSettingsRequest, final String tkt);
}
