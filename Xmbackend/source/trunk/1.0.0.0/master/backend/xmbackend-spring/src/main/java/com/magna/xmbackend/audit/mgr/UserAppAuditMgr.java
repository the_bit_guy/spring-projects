package com.magna.xmbackend.audit.mgr;

import com.magna.xmbackend.entities.UserApplicationsTbl;
import com.magna.xmbackend.vo.userApplication.UserApplicationRequest;
import com.magna.xmbackend.vo.userApplication.UserApplicationResponse;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author Admin
 */
public interface UserAppAuditMgr {

    /**
     *
     * @param userApplicationsTbl
     * @param httpServletRequest
     */
    void userAppCreateSuccessAuditor(final UserApplicationsTbl userApplicationsTbl,
            final HttpServletRequest httpServletRequest);

    /**
     *
     * @param userApplicationRequest
     * @param httpServletRequest
     */
    void userAppCreateFailureAuditor(final UserApplicationRequest userApplicationRequest,
            final HttpServletRequest httpServletRequest);

    /**
     *
     * @param userApplicationsTbl
     * @param servletRequest
     */
    void userAppUpdateSuccessAuditor(final UserApplicationsTbl userApplicationsTbl,
            final HttpServletRequest servletRequest);

    /**
     *
     * @param userApplicationRequest
     * @param httpServletRequest
     */
    void userAppUpdateFailureAuditor(final UserApplicationRequest userApplicationRequest,
            final HttpServletRequest httpServletRequest);

    /**
     *
     * @param status
     * @param userAppId
     * @param httpServletRequest
     * @param isSuccess
     * @param uat
     */
    void userAppUpdateStatusAuditor(final String status, final String userAppId,
            final HttpServletRequest httpServletRequest,
            final boolean isSuccess, final UserApplicationsTbl uat);

    /**
     *
     * @param userAppId
     * @param httpServletRequest
     * @param isSuccess
     * @param uat
     */
    void userAppDeleteStatusAuditor(final String userAppId,
            final HttpServletRequest httpServletRequest,
            final boolean isSuccess, final UserApplicationsTbl uat);

    /**
     *
     * @param userApplicationResponse
     * @param userAppIds
     * @param httpServletRequest
     */
    void userAppMultiDeleteAuditor(final UserApplicationResponse userApplicationResponse,
            final Set<String> userAppIds, final HttpServletRequest httpServletRequest);

	void userAppStatusUpdateSuccessAuditor(String userAppName, String status, HttpServletRequest httpServletRequest);

	void userAppStatusUpdateFailureAuditor(String userAppName, String status, Exception ex,
			HttpServletRequest httpServletRequest);

	void userAppDeleteFailureAuditor(String name, HttpServletRequest httpServletRequest);

	void userAppCreateFailureBatchAuditor(UserApplicationRequest userApplicationRequest,
			HttpServletRequest httpServletRequest);
}
