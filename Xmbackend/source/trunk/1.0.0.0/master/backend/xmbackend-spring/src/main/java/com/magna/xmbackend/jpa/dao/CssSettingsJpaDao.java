/**
 * 
 */
package com.magna.xmbackend.jpa.dao;

import java.util.Optional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.magna.xmbackend.entities.CssSettingsTbl;
import com.magna.xmbackend.entities.UsersTbl;

// TODO: Auto-generated Javadoc
/**
 * The Interface CssSettingsJpaDao.
 *
 * @author Bhabadyuti Bal
 */
@Transactional
public interface CssSettingsJpaDao extends CrudRepository<CssSettingsTbl, String>{

	
	/**
	 * Find by user.
	 *
	 * @param usersTbl the users tbl
	 * @return the css settings tbl
	 */
	@Query("from CssSettingsTbl cst where cst.userId = :usersTbl")
	Optional<CssSettingsTbl> findByUser(@Param("usersTbl") UsersTbl usersTbl);

	/**
	 * Delete by user.
	 *
	 * @param usersTbl the users tbl
	 */
	@Modifying
	@Query("delete from CssSettingsTbl cst where cst.userId = :usersTbl")
	void deleteByUser(@Param("usersTbl") UsersTbl usersTbl);

	/**
	 * Update applied status.
	 *
	 * @param usersTbl the users tbl
	 * @param isApplied the is applied
	 * @return the int
	 */
	@Modifying
	@Query("update CssSettingsTbl cst set cst.isApplied = :isApplied where cst.userId = :usersTbl" )
	int updateAppliedStatus(@Param("usersTbl") UsersTbl usersTbl, @Param("isApplied") String isApplied);

}
