package com.magna.xmbackend.audit.mgr.impl;

import com.magna.xmbackend.audit.mgr.StartAppAuditMgr;
import com.magna.xmbackend.audit.mgr.utils.AdminHistoryBaseObjectUtils;
import com.magna.xmbackend.entities.AdminHistoryBaseObjectsTbl;
import com.magna.xmbackend.entities.StartApplicationsTbl;
import com.magna.xmbackend.jpa.dao.AdminHistoryBaseObjectsJpaDao;
import com.magna.xmbackend.vo.startApplication.StartApplicationRequest;
import com.magna.xmbackend.vo.startApplication.StartApplicationResponse;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author Admin
 */
@Component
public class StartAppAuditMgrImpl implements StartAppAuditMgr {

    private static final Logger LOG
            = LoggerFactory.getLogger(StartAppAuditMgrImpl.class);

    final String OBJECT_NAME = "StartApplicationsTbl";
    final String OPERATION_CREATE = "Start App Create";
    final String OPERATION_UPDATE = "Start App Update";
    final String OPERATION_DELETE = "Start App Delete";
    final String OPERATION_MULTI_DELETE = "Start App Multi Delete";
    final String RESULT_SUCCESS = "Success";
    final String RESULT_FAILURE = "Failure";

    @Autowired
    private AdminHistoryBaseObjectsJpaDao adminHistoryBaseObjectsJpaDao;

    @Autowired
    private AdminHistoryBaseObjectUtils adminHistoryBaseObjectUtils;

    /**
     *
     * @param startApplicationsTbl
     * @param httpServletRequest
     */
    @Override
    public void startAppCreateSuccessAuditor(final StartApplicationsTbl startApplicationsTbl,
            final HttpServletRequest httpServletRequest) {
        LOG.info(">>>> startAppCreateSuccessAuditor");
        final String name = startApplicationsTbl.getName();
        final StringBuilder changeBuff = new StringBuilder();
        changeBuff.append("Start Application created with ")
                .append(" name").append(name);
        final String errorMessage = "";
        final AdminHistoryBaseObjectsTbl ahbot = this.adminHistoryBaseObjectUtils
                .makeAdminHistoryBaseObjectsTbl(
                        httpServletRequest, changeBuff.toString(), errorMessage,
                        OBJECT_NAME, OPERATION_CREATE, RESULT_SUCCESS);
        this.adminHistoryBaseObjectsJpaDao.save(ahbot);
        LOG.info("<<<< startAppCreateSuccessAuditor");
    }

    /**
     *
     * @param startApplicationRequest
     * @param httpServletRequest
     */
    @Override
    public void startAppCreateFailureAuditor(final StartApplicationRequest startApplicationRequest,
            final HttpServletRequest httpServletRequest) {
        LOG.info(">>>> startAppCreateFailureAuditor");
        final String name = startApplicationRequest.getName();
        final StringBuilder errorBuff = new StringBuilder();
        errorBuff.append("Start Application creation failed for name")
                .append(name);
        final String changes = "";
        final AdminHistoryBaseObjectsTbl ahbot = this.adminHistoryBaseObjectUtils
                .makeAdminHistoryBaseObjectsTbl(
                        httpServletRequest, changes, errorBuff.toString(),
                        OBJECT_NAME, OPERATION_CREATE, RESULT_FAILURE);
        this.adminHistoryBaseObjectsJpaDao.save(ahbot);
        LOG.info("<<<< startAppCreateFailureAuditor");
    }
    
    
    
    @Override
    public void startAppCreateFailureBatchAuditor(final StartApplicationRequest startApplicationRequest,
            final HttpServletRequest httpServletRequest) {
        LOG.info(">>>> startAppCreateFailureAuditor");
        final String name = startApplicationRequest.getName();
        final StringBuilder errorBuff = new StringBuilder();
        errorBuff.append("Start Application with name ")
                .append(name).append(" already exist");
        final String changes = "";
        final AdminHistoryBaseObjectsTbl ahbot = this.adminHistoryBaseObjectUtils
                .makeAdminHistoryBaseObjectsTbl(
                        httpServletRequest, changes, errorBuff.toString(),
                        OBJECT_NAME, OPERATION_CREATE, RESULT_FAILURE);
        this.adminHistoryBaseObjectsJpaDao.save(ahbot);
        LOG.info("<<<< startAppCreateFailureAuditor");
    }

    /**
     *
     * @param startApplicationsTbl
     * @param httpServletRequest
     */
    @Override
    public void startAppUpdateSuccessAuditor(final StartApplicationsTbl startApplicationsTbl,
            final HttpServletRequest httpServletRequest) {
        LOG.info(">>>> startAppUpdateSuccessAuditor");
        final String name = startApplicationsTbl.getName();
        final StringBuilder changeBuff = new StringBuilder();
        changeBuff.append("Start Application updated with ")
                .append(" name").append(name);
        final String errorMessage = "";
        final AdminHistoryBaseObjectsTbl ahbot = this.adminHistoryBaseObjectUtils
                .makeAdminHistoryBaseObjectsTbl(
                        httpServletRequest, changeBuff.toString(), errorMessage,
                        OBJECT_NAME, OPERATION_UPDATE, RESULT_SUCCESS);
        this.adminHistoryBaseObjectsJpaDao.save(ahbot);
        LOG.info("<<<< startAppUpdateSuccessAuditor");
    }

    /**
     *
     * @param startApplicationRequest
     * @param httpServletRequest
     */
    @Override
    public void startAppUpdateFailureAuditor(final StartApplicationRequest startApplicationRequest,
            final HttpServletRequest httpServletRequest) {
        LOG.info(">>>> startAppUpdateFailureAuditor");
        final String name = startApplicationRequest.getName();
        final StringBuilder errorBuff = new StringBuilder();
        errorBuff.append("Start Application update failed for name")
                .append(name);
        String changes = "";
        final AdminHistoryBaseObjectsTbl ahbot = this.adminHistoryBaseObjectUtils
                .makeAdminHistoryBaseObjectsTbl(
                        httpServletRequest, changes, errorBuff.toString(),
                        OBJECT_NAME, OPERATION_UPDATE, RESULT_FAILURE);
        this.adminHistoryBaseObjectsJpaDao.save(ahbot);
        LOG.info("<<<< startAppUpdateFailureAuditor");
    }

    /**
     *
     * @param status
     * @param startAppId
     * @param httpServletRequest
     * @param isSuccess
     * @param sat
     */
    @Override
    public void startAppUpdateStatusAuditor(final String status, final String startAppId,
            final HttpServletRequest httpServletRequest, 
            final boolean isSuccess, final StartApplicationsTbl sat) {
        LOG.info(">>>> startAppUpdateStatusAuditor::isSuccess {}", isSuccess);
        final StringBuilder changeBuff = new StringBuilder();
        final StringBuilder errorBuff = new StringBuilder();
        String result = "failed";
        String startAppName = "";
         if (null != sat) {
            startAppName = sat.getName();
        }
        if (isSuccess) {
            changeBuff.append("Start Application updated ")
                    .append(" with name:")
                    .append(startAppName).append(" Success");
            result = "Success";
        } else {
            errorBuff.append("Start Application updation with name ")
                    .append(startAppName).append(" failed");
        }
        final AdminHistoryBaseObjectsTbl ahbot = this.adminHistoryBaseObjectUtils
                .makeAdminHistoryBaseObjectsTbl(
                        httpServletRequest, changeBuff.toString(),
                        errorBuff.toString(), OBJECT_NAME, OPERATION_UPDATE,
                        result);
        this.adminHistoryBaseObjectsJpaDao.save(ahbot);
        LOG.info("<<<< startAppUpdateStatusAuditor::isSuccess");
    }

    /**
     *
     * @param startAppId
     * @param httpServletRequest
     * @param isSuccess
     * @param sat
     */
    @Override
    public void startAppDeleteStatusAuditor(final String startAppId,
            final HttpServletRequest httpServletRequest,
            final boolean isSuccess, final StartApplicationsTbl sat) {
        LOG.info(">>>> startAppDeleteStatusAuditor::isSuccess {}", isSuccess);
        final StringBuilder changeBuff = new StringBuilder();
        final StringBuilder errorBuff = new StringBuilder();
        String result = "Failed";
        String startAppName = "";
        if (null != sat) {
            startAppName = sat.getName();
        }
        if (isSuccess) {
            changeBuff.append("Start Application with ")
                    .append(" name ").append(startAppName).append(" deletion success");
            result = "Success";
        } else {
            errorBuff.append("Start Application with ")
                    .append(" name ").append(startAppName).append(" failed");
        }
        final AdminHistoryBaseObjectsTbl ahbot = this.adminHistoryBaseObjectUtils
                .makeAdminHistoryBaseObjectsTbl(
                        httpServletRequest, changeBuff.toString(), errorBuff.toString(),
                        OBJECT_NAME, OPERATION_DELETE, result);
        this.adminHistoryBaseObjectsJpaDao.save(ahbot);
        LOG.info("<<<< startAppDeleteStatusAuditor");
    }

    /**
     *
     * @param startApplicationResponse
     * @param startAppIds
     * @param httpServletRequest
     */
    @Override
    public void startAppMultiDeleteAuditor(final StartApplicationResponse startApplicationResponse,
            final Set<String> startAppIds, final HttpServletRequest httpServletRequest) {
        LOG.info(">>>> startAppMultiDeleteAuditor");
        final List<Map<String, String>> statusMaps = startApplicationResponse.getStatusMaps();
        String result = "Failure";
        final StringBuilder errorMessageBuff = new StringBuilder();
        if (statusMaps.isEmpty()) {
            result = "Success";
        } else {
            statusMaps.forEach(statusMap -> {
                statusMap.forEach((key, value) -> {
                    errorMessageBuff.append(" ").append(key).append(":").append(value);
                });
            });
        }
        final StringBuilder changeBuff = new StringBuilder();
        changeBuff.append("Given StartApp Ids:").append(String.join(" ", startAppIds));
        if (!statusMaps.isEmpty() && (startAppIds.size() == statusMaps.size())) {
            result = "Failure";
        } else if (!statusMaps.isEmpty() && (startAppIds.size() > statusMaps.size())) {
            result = "Partial Success";
        }
        final AdminHistoryBaseObjectsTbl ahbot
                = this.adminHistoryBaseObjectUtils.makeAdminHistoryBaseObjectsTbl(httpServletRequest,
                        changeBuff.toString(), errorMessageBuff.toString(),
                        OBJECT_NAME, OPERATION_MULTI_DELETE, result);
        this.adminHistoryBaseObjectsJpaDao.save(ahbot);
        LOG.info("<<<< startAppMultiDeleteAuditor");
    }

	@Override
	public void startAppStatusUpdateSuccessAuditor(String startAppName, String status,
			HttpServletRequest httpServletRequest) {
		LOG.info(">>>> startAppStatusUpdateSuccessAuditor");
		String errorMsg = "";
		final StringBuilder changeBuff = new StringBuilder();
		changeBuff.append("StartApp with name ").append(startAppName).append(" updated ").append(" with status ")
				.append(status);

		AdminHistoryBaseObjectsTbl ahbot = this.adminHistoryBaseObjectUtils
				.makeAdminHistoryBaseObjectsTbl(
						httpServletRequest, changeBuff.toString(), errorMsg,
						OBJECT_NAME, "StartApp status update", RESULT_SUCCESS);
		this.adminHistoryBaseObjectsJpaDao.save(ahbot);
		LOG.info("<<<< startAppStatusUpdateSuccessAuditor");
		
	}

	@Override
	public void startAppStatusUpdateFailureAuditor(String startAppName, String status, Exception ex,
			HttpServletRequest httpServletRequest) {
		LOG.info(">>>> startAppStatusUpdateFailureAuditor");
		
		String errorMsg = ex.getMessage();
		final StringBuilder changeBuff = new StringBuilder();
		changeBuff.append("StartApp with name ").append(startAppName).append(" updated ").append(" with status ")
				.append(status);

		AdminHistoryBaseObjectsTbl ahbot = this.adminHistoryBaseObjectUtils
				.makeAdminHistoryBaseObjectsTbl(
						httpServletRequest, changeBuff.toString(), errorMsg,
						OBJECT_NAME, "StartApp status update", RESULT_FAILURE);
		this.adminHistoryBaseObjectsJpaDao.save(ahbot);
		LOG.info("<<<< startAppStatusUpdateFailureAuditor");
		
	}
}
