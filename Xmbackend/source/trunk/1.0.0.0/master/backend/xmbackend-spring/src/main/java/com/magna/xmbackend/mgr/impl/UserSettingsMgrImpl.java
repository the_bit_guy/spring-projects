/**
 * 
 */
package com.magna.xmbackend.mgr.impl;

import java.util.Date;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.magna.xmbackend.entities.ProjectsTbl;
import com.magna.xmbackend.entities.SitesTbl;
import com.magna.xmbackend.entities.UserSettingsTbl;
import com.magna.xmbackend.entities.UserTkt;
import com.magna.xmbackend.entities.UsersTbl;
import com.magna.xmbackend.exception.XMObjectNotFoundException;
import com.magna.xmbackend.jpa.dao.ProjectJpaDao;
import com.magna.xmbackend.jpa.dao.UserJpaDao;
import com.magna.xmbackend.jpa.dao.UserSettingsJpaDao;
import com.magna.xmbackend.jpa.dao.UserTktJpaDao;
import com.magna.xmbackend.mgr.UserSettingsMgr;
import com.magna.xmbackend.vo.enums.Application;
import com.magna.xmbackend.vo.userSettings.UserSettingsRequest;

/**
 * @author Bhabadyuti Bal
 *
 */
@Component
public class UserSettingsMgrImpl implements UserSettingsMgr {
	
	@Autowired
	private UserSettingsJpaDao userSettingsJpaDao;
	@Autowired
	private UserJpaDao userJpaDao;
	@Autowired
	private ProjectJpaDao projectJpaDao;
	@Autowired
	UserTktJpaDao userTktJpaDao;
	
	private static final Logger LOG = LoggerFactory.getLogger(UserSettingsMgrImpl.class);

	@Override
	public UserSettingsTbl findUserSettingsBySiteId(String userName, String siteId) {
		final String userId = this.userJpaDao.findUserIdByUserName(userName);
		UserSettingsTbl userSettingsTbl = this.userSettingsJpaDao.findByUserIdAndSiteId(new UsersTbl(userId), new SitesTbl(siteId));
		if(null == userSettingsTbl){
			String[] param = {siteId};
			throw new XMObjectNotFoundException("User Settings Not Found", "US_ERR0001", param);
		}
		return userSettingsTbl;
	}

	@Override
	public UserSettingsTbl saveOrUpdateProject(final String userName, final UserSettingsRequest userSettingsRequest, 
			 final String tkt) {
		LOG.info(">> saveOrUpdateProject");
		UserSettingsTbl userSettingsTblOut = null;
		final String userId = this.userJpaDao.findUserIdByUserName(userName);
		final String siteId = userSettingsRequest.getSiteId();
		final String projectId = userSettingsRequest.getProjectId();
		UserTkt userTktTbl = userTktJpaDao.findByTktAndApplicationName(tkt, Application.CAX_START_ADMIN.name());
		if(userTktTbl == null){
			UserSettingsTbl userSettingsTbl = this.userSettingsJpaDao.findByUserIdAndSiteId(new UsersTbl(userId), new SitesTbl(siteId));
			if(null != userSettingsTbl){
				//update project
				final int isUpdated = this.userSettingsJpaDao.updateProject(new ProjectsTbl(projectId), userSettingsTbl.getUserSettingsId());
				if(isUpdated > 0){
					userSettingsTblOut = this.userSettingsJpaDao.findOne(userSettingsTbl.getUserSettingsId());
				}
			} else {
				//create new entry
				final UserSettingsTbl userSettingsTblIn = this.convert2Entity(userName, userSettingsRequest);
				userSettingsTblOut = this.userSettingsJpaDao.save(userSettingsTblIn);
			}
			
		}
		LOG.info("<< saveOrUpdateProject");
		return userSettingsTblOut;
	}
	
	
	@Override
	public UserSettingsTbl saveOrUpdateSettings(String userName, UserSettingsRequest userSettingsRequest, String tkt) {

		LOG.info(">> saveOrUpdateSettings");
		UserSettingsTbl userSettingsTblOut = null;
		final String userId = this.userJpaDao.findUserIdByUserName(userName);
		final String siteId = userSettingsRequest.getSiteId();
		final String settings = userSettingsRequest.getSettings();
		UserTkt userTktTbl = userTktJpaDao.findByTktAndApplicationName(tkt, Application.CAX_START_ADMIN.name());
		if(userTktTbl == null){
			UserSettingsTbl userSettingsTbl = this.userSettingsJpaDao.findByUserIdAndSiteId(new UsersTbl(userId), new SitesTbl(siteId));
			if(null != userSettingsTbl){
				//update settings
				final int isUpdated = this.userSettingsJpaDao.updateSetting(settings, userSettingsTbl.getUserSettingsId());
				if(isUpdated > 0){
					userSettingsTblOut = this.userSettingsJpaDao.findOne(userSettingsTbl.getUserSettingsId());
				}
			} else {
				//create new entry
				final UserSettingsTbl userSettingsTblIn = this.convert2Entity(userName, userSettingsRequest);
				userSettingsTblOut = this.userSettingsJpaDao.save(userSettingsTblIn);
			}
			
		}
		LOG.info("<< saveOrUpdateSettings");
		return userSettingsTblOut;
	
	}

	private UserSettingsTbl convert2Entity(String userName, UserSettingsRequest userSettingsRequest) {

		final String userId = this.userJpaDao.findUserIdByUserName(userName);
		final String siteId = userSettingsRequest.getSiteId();
		final String projectId = userSettingsRequest.getProjectId();
		final String settings = userSettingsRequest.getSettings();
		Date date = new Date();
		String id = UUID.randomUUID().toString();
		UserSettingsTbl usrSettingsTbl = new UserSettingsTbl(id);
		usrSettingsTbl.setUserId(new UsersTbl(userId));
		usrSettingsTbl.setSiteId(new SitesTbl(siteId));
		ProjectsTbl projectsTbl = projectJpaDao.findByProjectId(projectId);
		usrSettingsTbl.setProjectId(projectsTbl);
		usrSettingsTbl.setSettings(settings);
		usrSettingsTbl.setCreateDate(date);
		usrSettingsTbl.setUpdateDate(date);
		return usrSettingsTbl;
	}

	
}
