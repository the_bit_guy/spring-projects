/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.mgr.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Component;

import com.magna.xmbackend.entities.EmailNotificationConfigTbl;
import com.magna.xmbackend.entities.EmailNotificationEventTbl;
import com.magna.xmbackend.entities.EmailNotifyToUserRelTbl;
import com.magna.xmbackend.entities.EmailTemplateTbl;
import com.magna.xmbackend.entities.UsersTbl;
import com.magna.xmbackend.exception.CannotCreateObjectException;
import com.magna.xmbackend.exception.CannotCreateRelationshipException;
import com.magna.xmbackend.exception.XMObjectNotFoundException;
import com.magna.xmbackend.jpa.dao.EmailNotificationConfigJpaDao;
import com.magna.xmbackend.jpa.dao.EmailNotificationEventsJpaDao;
import com.magna.xmbackend.jpa.dao.EmailTemplateJpaDao;
import com.magna.xmbackend.jpa.dao.UserJpaDao;
import com.magna.xmbackend.jpa.rel.dao.EmailNotifyToUserRelJpaDao;
import com.magna.xmbackend.mgr.NotificationMgr;
import com.magna.xmbackend.utils.MailSupport;
import com.magna.xmbackend.utils.MessageMaker;
import com.magna.xmbackend.vo.notification.EmailTemplateRequest;
import com.magna.xmbackend.vo.notification.EmailTemplateResponse;
import com.magna.xmbackend.vo.notification.NotificationConfigResponse;
import com.magna.xmbackend.vo.notification.NotificationRequest;
import com.magna.xmbackend.vo.notification.NotificationResponse;

@Component
public class NotificationMgrImpl implements NotificationMgr {

	private static final Logger LOG = LoggerFactory.getLogger(NotificationMgrImpl.class);
    @Autowired
    private EmailNotificationConfigJpaDao emailNotificationConfigJpaDao;
    @Autowired
    private EmailNotifyToUserRelJpaDao emailNotifyToUserRelJpaDao;
    @Autowired
    private MessageMaker messageMaker;
    @Autowired
    private MailSupport mailSupport;
    @Autowired
    private EmailNotificationEventsJpaDao eventsJpaDao;
    @Autowired
    private EmailTemplateJpaDao emailTemplateJpaDao;
    @Autowired
    private SessionFactory sessionFactory;
    @Autowired
    private UserJpaDao userJpaDao;
    @Autowired
    private EmailNotificationEventsJpaDao notificationEventsJpaDao;
    
    
    
    @Override
    public Iterable<EmailNotifyToUserRelTbl> findAllPendingNotification() {
        LOG.info(">> findAllPendingNotification");
        Iterable<EmailNotifyToUserRelTbl> enturts
                = this.emailNotifyToUserRelJpaDao.findAll();
        LOG.info("<< findAllPendingNotification");
        return enturts;
    }

    @Override
	public List<NotificationConfigResponse> findAllNotificationConfig() {
		LOG.info(">> findAllNotificationConfig");
		List<NotificationConfigResponse> configResponses = new ArrayList<>();
		Iterable<EmailNotificationConfigTbl> findAll = this.emailNotificationConfigJpaDao.findAll();
		for (EmailNotificationConfigTbl emailNotificationConfigTbl : findAll) {
			NotificationConfigResponse configResponse = new NotificationConfigResponse();
			configResponse.setNotifEventId(emailNotificationConfigTbl.getEmailNotificationEventId().getEmailNotificationEventId());
			configResponse.setNotifEventName(emailNotificationConfigTbl.getEmailNotificationEventId().getEvent());
			configResponse.setNotifEventDescription(emailNotificationConfigTbl.getEmailNotificationEventId().getDescription());
			configResponse.setNotifConfigName(emailNotificationConfigTbl.getName());
			configResponse.setNotifConfigId(emailNotificationConfigTbl.getEmailNotificationConfigId());
			
			EmailTemplateTbl emailTemplateTbl = emailNotificationConfigTbl.getEmailTemplateId();
			configResponse.setNotifConfigEmailTemplateId(emailTemplateTbl.getEmailTemplateId());
			configResponse.setNotifConfigEmailTemplateMsg(emailTemplateTbl.getMessage());
			configResponse.setNotifConfigEmailTemplateName(emailTemplateTbl.getName());
			configResponse.setNotifConfigEmailTemplateSubject(emailTemplateTbl.getSubject());
			String toUsers = emailNotificationConfigTbl.getEmailNotifyToUserId().getToUsers();
			String templateVeriables = emailTemplateTbl.getTemplateVeriables();
			if (templateVeriables != null) {
				List<String> asList = Arrays.asList(templateVeriables.split(","));
				configResponse.setNotifConfigEmailTemplateVariables(new HashSet<>(asList));
			}
			
			Set<String> listOfToUsernames = null;
			if(toUsers != null) {
				listOfToUsernames = new HashSet<>();
				listOfToUsernames = this.getListOfUsernames(toUsers);
			}
			configResponse.setNotifConfigToUsers(listOfToUsernames);
			
			String ccUsers = emailNotificationConfigTbl.getEmailNotifyToUserId().getCcUsers();
			Set<String> listOfCcUsernames = null;
			if(ccUsers != null) {
				listOfCcUsernames = new HashSet<>();
				listOfCcUsernames = this.getListOfUsernames(ccUsers);
			}
			configResponse.setNotifConfigCcUsers(listOfCcUsernames);
			configResponse.setNotifConfigStatus(emailNotificationConfigTbl.getStatus());
			configResponse.setNotifEventStatus(emailNotificationConfigTbl.getEmailNotificationEventId().getStatus());
			configResponses.add(configResponse);
		}
		LOG.info("<< findAllNotificationConfig");
		return configResponses;
	}

    private Set<String> getListOfUsernames(final String usernames) {
    	Set<String> notifConfigToUsers = new HashSet<>();
    	String[] unames = usernames.split(";");
    	for (String username : unames) {
    		UsersTbl usersTbl = this.userJpaDao.findByUsername(username);
    		if(usersTbl != null) {
    			notifConfigToUsers.add(usersTbl.getUsername());
    		}
    	}
    	return notifConfigToUsers;
    }
    
    
    @Override
	public NotificationConfigResponse createNotification(NotificationRequest notificationRequest) {
		LOG.info(">> createNotification");
		EmailNotificationConfigTbl convert2Entity = this.convert2Entity(notificationRequest, false);
		EmailNotificationConfigTbl emailNotificationConfigTbl = this.emailNotificationConfigJpaDao.save(convert2Entity);
		EmailNotifyToUserRelTbl emailNotifyToUserRelTbl = this.emailNotifyToUserRelJpaDao.save(convert2Entity.getEmailNotifyToUserId());
		/*List<EmailNotificationConfigTbl> configTbls = new ArrayList<>();
		configTbls.add(outEntitiy);
		NotificationResponse response = new NotificationResponse(configTbls);
		response.setDescription(outEntitiy.getEmailNotificationEventId().getDescription());
		response.setEventStatus(outEntitiy.getEmailNotificationEventId().getStatus());*/
		NotificationConfigResponse configResponse = this.createNotificationResponse(emailNotificationConfigTbl, emailNotifyToUserRelTbl);
		
		LOG.info("<< createNotification");
		return configResponse;
	}
    
    private NotificationConfigResponse createNotificationResponse(EmailNotificationConfigTbl emailNotificationConfigTbl,
    		EmailNotifyToUserRelTbl emailNotifyToUserRelTbl ) {
    	Set<String> toUsersSet = null;
		Set<String> ccUsersSet = null;
    	String toUsers = emailNotifyToUserRelTbl.getToUsers();
    	if(toUsers != null) {
    		List<String> toUsersList = Arrays.asList(toUsers.split("\\s*;\\s*"));
    		toUsersSet = new HashSet<>(toUsersList);
			//toUsersSet = this.getUsernamesFromUserIds(toUsersList);
    	}
    	
		String ccUsers = emailNotifyToUserRelTbl.getCcUsers();
		if(ccUsers != null) {
			List<String> ccUsersList = Arrays.asList(ccUsers.split("\\s*;\\s*"));
			ccUsersSet = new HashSet<>(ccUsersList);
			//ccUsersSet = this.getUsernamesFromUserIds(ccUsersList);
		}
		
		NotificationConfigResponse configResponse = new NotificationConfigResponse();
		
		configResponse.setNotifConfigToUsers(toUsersSet);
		configResponse.setNotifConfigCcUsers(ccUsersSet);
		
		EmailTemplateTbl emailTemplateId = emailNotificationConfigTbl.getEmailTemplateId();
		String templateVeriables = emailTemplateId.getTemplateVeriables();
		Set<String> variablesSet = null;
		if (templateVeriables != null) {
			variablesSet = new HashSet<>();
			variablesSet = new TreeSet<>(Arrays.asList(templateVeriables));
		}
		
		configResponse.setNotifConfigEmailTemplateId(emailTemplateId.getEmailTemplateId());
		configResponse.setNotifConfigEmailTemplateMsg(emailTemplateId.getMessageTemplate());
		configResponse.setNotifConfigEmailTemplateName(emailTemplateId.getName());
		configResponse.setNotifConfigEmailTemplateSubject(emailTemplateId.getSubject());
		configResponse.setNotifConfigEmailTemplateVariables(variablesSet);
		configResponse.setNotifConfigId(emailNotificationConfigTbl.getEmailNotificationConfigId());
		configResponse.setNotifConfigName(emailNotificationConfigTbl.getName());
		configResponse.setNotifConfigStatus(emailNotificationConfigTbl.getStatus());
		
		EmailNotificationEventTbl emailNotificationEventId = emailNotificationConfigTbl.getEmailNotificationEventId();
		configResponse.setNotifEventDescription(emailNotificationEventId.getDescription());
		configResponse.setNotifEventId(emailNotificationEventId.getEmailNotificationEventId());
		configResponse.setNotifEventName(emailNotificationEventId.getEvent());
		configResponse.setNotifEventStatus(emailNotificationEventId.getStatus());
		configResponse.setNotifConfigEmailTemplateSubject(emailTemplateId.getSubject());
		
		return configResponse;
	}

	//new
    private EmailNotificationConfigTbl convert2Entity(NotificationRequest notificationRequest, boolean isUpdate) {
    	LOG.info(">> convert2Entity");
    	String notificationConfigActionName = notificationRequest.getNotificationConfigActionName();
    	if(!isUpdate) {
    		if(this.emailNotificationConfigJpaDao.findByNameIgnoreCase(notificationConfigActionName) != null) {
    			String[] name = {notificationConfigActionName};
    			throw new CannotCreateRelationshipException("Relation already exist", "ERR0024", name);
    		}
    	}
    	List<String> usersToNotify = notificationRequest.getUsersToNotify();
    	List<String> ccUsersToNotify = notificationRequest.getCcUsersToNotify();
    	
    	String notificationEventId = notificationRequest.getNotificationEventId();
    	String notificationConfigId = notificationRequest.getNotificationConfigId();
    	String notificationConfigStatus = notificationRequest.getNotificationActionStatus().name();
    	String emailTemplateId = notificationRequest.getEmailTemplateId();
    	String toUserIds = null;
    	if(usersToNotify != null && usersToNotify.size() > 0) {
    		//toUserIds = this.getUserIdsFromUsernames(usersToNotify).toString();
    		toUserIds = String.join(";", usersToNotify);
    	}
    	
    	String ccUserIds = null;
    	if(ccUsersToNotify != null && ccUsersToNotify.size() > 0) {
    		//ccUserIds = this.getUserIdsFromUsernames(ccUsersToNotify).toString();
    		ccUserIds = String.join(";", ccUsersToNotify);
    	}
    	
    	EmailNotificationConfigTbl notificationConfigTbl = null;
    	EmailTemplateTbl emailTemplateTbl = null;
    	EmailNotificationEventTbl emailNotificationEventTbl = null;
    	String uuid = UUID.randomUUID().toString().toUpperCase();
    	if(isUpdate) {
    		notificationConfigTbl = this.emailNotificationConfigJpaDao.findOne(notificationConfigId);
    		if(notificationConfigTbl == null) {
    			throw new XMObjectNotFoundException("No configuration found", "ERR0022");
    		}
    		emailTemplateTbl = this.emailTemplateJpaDao.findOne(emailTemplateId);
    		emailNotificationEventTbl = this.notificationEventsJpaDao.findOne(notificationEventId);
    	} else {
    		notificationConfigTbl = new EmailNotificationConfigTbl(uuid);
    		emailTemplateTbl = new EmailTemplateTbl(emailTemplateId);
    		emailNotificationEventTbl = new EmailNotificationEventTbl(notificationEventId);
    	}
    	
    	
    	notificationConfigTbl.setName(notificationConfigActionName);
    	notificationConfigTbl.setStatus(notificationConfigStatus);
    	notificationConfigTbl.setEmailNotificationEventId(emailNotificationEventTbl);
    	notificationConfigTbl.setEmailTemplateId(emailTemplateTbl);
		Date date = new Date();
		if (!isUpdate) {
			notificationConfigTbl.setCreateDate(date);
		}
		notificationConfigTbl.setUpdateDate(date);
		
		String emailNotifyUserId = UUID.randomUUID().toString().toUpperCase();
		if(isUpdate) {
			EmailNotifyToUserRelTbl emailNotifyToUserId = notificationConfigTbl.getEmailNotifyToUserId();
			if(emailNotifyToUserId != null)
				emailNotifyUserId = emailNotifyToUserId.getEmailNotifyToUserRelId();
		}
		EmailNotifyToUserRelTbl emailNotifyToUserRelTbl = new EmailNotifyToUserRelTbl(emailNotifyUserId);
		emailNotifyToUserRelTbl.setCcUsers(ccUserIds);
		emailNotifyToUserRelTbl.setToUsers(toUserIds);
		emailNotifyToUserRelTbl.setEmailNotificationConfigId(notificationConfigTbl);
		emailNotifyToUserRelTbl.setCreateDate(date);
		emailNotifyToUserRelTbl.setUpdateDate(date);
		
		notificationConfigTbl.setEmailNotifyToUserId(emailNotifyToUserRelTbl);
    	LOG.info("<< convert2Entity");
    	return notificationConfigTbl;
    }
    
    private StringBuilder getUserIdsFromUsernames(List<String> usersToNotify) {
    	StringBuilder userIds = new StringBuilder();
    	usersToNotify.forEach(username -> {
    		UsersTbl usersTbl = this.userJpaDao.findByUsername(username);
    		userIds.append(usersTbl.getUserId()).append(";");
    	});
    	return userIds;
    }
    

	/*private EmailNotificationConfigTbl convert2Entity(NotificationRequest notificationRequest, boolean isUpdate) {
		String notificationEvent = notificationRequest.getNotificationEvent();
		String notificationConfigId = notificationRequest.getNotificationConfigId();
		List<String> usersToNotify = notificationRequest.getUsersToNotify();
		List<String> ccUsersToNotify = notificationRequest.getCcUsersToNotify();
		
		if(!isUpdate) {
			notificationConfigId = UUID.randomUUID().toString();
		}
		//EmailNotificationConfigTbl configTbl = this.emailNotificationConfigJpaDao.findByEvent(notificationEvent);
		EmailNotificationEventTbl eventTbl = this.eventsJpaDao.findByEvent(notificationEvent);
		if (null == eventTbl) {
			LOG.error("Notification Event {} not supported", notificationEvent);
			// todo: throw err
		}
		

		Date date = new Date();

		String actionName = notificationRequest.getNotificationConfigActionName();
		EmailNotificationConfigTbl notifConfigTbl = this.emailNotificationConfigJpaDao.findByEventAndAction( notificationEvent, actionName );
		if (!isUpdate && notifConfigTbl != null ) {
			throw new CannotCreateObjectException("Notification config already found", "ERR0002", new String[]{actionName});
		}
		String message = notificationRequest.getMessage();
		String template = this.mailSupport.textToThymleafTemplate(message);
		String subject = notificationRequest.getSubject();
		String status = notificationRequest.getNotificationActionStatus().name();
		
		EmailNotificationConfigTbl configTbl = new EmailNotificationConfigTbl(notificationConfigId);
		configTbl.setName(actionName);
		configTbl.setEmailNotificationEventId(eventTbl);
		configTbl.setUpdateDate(date);
		configTbl.setCreateDate(date);
		configTbl.setStatus(status);
		
		Collection<EmailNotifyToUserRelTbl> emailNotifyToUserRelTbls = new ArrayList<>();
		List<String> invalidUserNames = new ArrayList<>();
		StringBuilder ccUsers = new StringBuilder();
		for(String ccUser : ccUsersToNotify) {
			UsersTbl usersTbl = this.userMgr.findByName(ccUser);
			if(usersTbl != null)
				ccUsers.append(usersTbl.getUserId()).append(";");
			else
				invalidUserNames.add(ccUser);
		}
		
		for (String user : usersToNotify) {
			UsersTbl usersTbl = this.userMgr.findByName(user);
			if (usersTbl != null) {
				
				Iterable<EmailNotifyToUserRelTbl> relTbls = null;
				//Iterable<EmailNotifyToUserRelTbl> relTbls = this.emailNotifyToUserRelJpaDao.findByUserId(usersTbl);
				String uuid = UUID.randomUUID().toString();
				for (EmailNotifyToUserRelTbl relTbl : relTbls) {
					EmailNotificationConfigTbl emailNotificationConfigId = relTbl.getEmailNotificationConfigId();
					String eventGot = emailNotificationConfigId.getEmailNotificationEventId().getEvent();
					if (notificationEvent.equalsIgnoreCase(eventGot)) {
						uuid = relTbl.getEmailNotifyToUserRelId();
						break;
					}
				}
				EmailNotifyToUserRelTbl emailNotifyToUserRelTbl = new EmailNotifyToUserRelTbl(uuid, date, date);

				// todo: check for user existence
				emailNotifyToUserRelTbl.setCreateDate(date);
				emailNotifyToUserRelTbl.setUpdateDate(date);
				emailNotifyToUserRelTbl.setEmailNotificationConfigId(configTbl);
				emailNotifyToUserRelTbls.add(emailNotifyToUserRelTbl);
			} else {
				invalidUserNames.add(user);
			}
		}
		//configTbl.setEmailNotifyToUserRelTblCollection(emailNotifyToUserRelTbls);
		
		if(invalidUserNames.size() > 0){
			String commaSeparatedUsernames = String.join(",", invalidUserNames);
			String[] param = {commaSeparatedUsernames};
			throw new XMObjectNotFoundException("No User Found", "USR_ERR0001", param);
		}	
		return configTbl;
	}
*/
    
    @Override
    public NotificationConfigResponse updateNotification(NotificationRequest notificationRequest) {
    	LOG.info(">> updateNotification");
    	EmailNotificationConfigTbl convert2Entity = this.convert2Entity(notificationRequest, true);
		EmailNotificationConfigTbl emailNotificationConfigTbl = this.emailNotificationConfigJpaDao.save(convert2Entity);
		EmailNotifyToUserRelTbl emailNotifyToUserRelTbl = this.emailNotifyToUserRelJpaDao.save(convert2Entity.getEmailNotifyToUserId());
		NotificationConfigResponse configResponse = this.createNotificationResponse(emailNotificationConfigTbl, emailNotifyToUserRelTbl);
    	LOG.info("<< updateNotification");
    	return configResponse;
    }
    
    
    /*@Override
    public NotificationResponse updateNotification(NotificationRequest notificationRequest) {
    	LOG.info(">> updateNotification");
    	
        final String event = notificationRequest.getNotificationEvent();
        EmailNotificationEventTbl eventTbl = this.eventsJpaDao.findByEvent(event);
    	final List<Map<String, String>> statusMaps = new ArrayList<>();
    	List<EmailNotificationConfigTbl> configTbls = new ArrayList<>();
    	EmailNotificationConfigTbl emailNotificationConfigTbl = null;
    	NotificationResponse notificationResponse = null;
		if (notificationRequest.getUsersToNotify().size() > 0) {
			try {
				List<EmailNotifyToUserRelTbl> emailNotifyToUserRelTbls = this.emailNotifyToUserRelJpaDao
						.findByEmailNotificationConfigId(
								new EmailNotificationConfigTbl(notificationRequest.getNotificationConfigId()));
				this.emailNotifyToUserRelJpaDao.delete(emailNotifyToUserRelTbls);
				emailNotificationConfigTbl = this.convert2Entity(notificationRequest, true);
			} catch (XMObjectNotFoundException objectNotFoundException) {
				Map<String, String> statusMap = messageMaker.extractFromException(objectNotFoundException);
				statusMaps.add(statusMap);
			}
			EmailNotificationConfigTbl outEntitiy = this.emailNotificationConfigJpaDao.save(emailNotificationConfigTbl);
			//Iterable<EmailNotifyToUserRelTbl> outEntities = this.emailNotifyToUserRelJpaDao.save(convert2EntityCollection);
			//write to thymleaf template
			Set<EmailNotificationConfigTbl> emailNotifConfigTbl = new HashSet<>();
			for (EmailNotifyToUserRelTbl emailNotifyToUserRelTbl : outEntities) {
				emailNotifConfigTbl.add(emailNotifyToUserRelTbl.getEmailNotificationConfigId());
			}
			if(emailNotifConfigTbl.size() == 1) {
				EmailNotificationConfigTbl emailNotifTbl = emailNotifConfigTbl.iterator().next();
				String notificationMsg = emailNotifTbl.getMessage();
				String template = this.textToThymleafTemplate(notificationMsg);
				LOG.debug("template {}", template);
				//update to EMAIL_TEMPLATE column
				int updatedVal = this.emailNotificationConfigJpaDao.updateEmailTemplate(template, emailNotifTbl.getEmailNotificationConfigId());
				if (updatedVal > 0) {
					LOG.info("Email Template updated!");
				}
			}
			configTbls.add(outEntitiy);
			notificationResponse = new NotificationResponse(configTbls, statusMaps);
			notificationResponse.setDescription(outEntitiy.getEmailNotificationEventId().getDescription());
			notificationResponse.setEventStatus(outEntitiy.getEmailNotificationEventId().getStatus());
		}else{
			List<EmailNotifyToUserRelTbl> emailNotifyToUserRelTbls = this.emailNotifyToUserRelJpaDao
					.findByEmailNotificationConfigId(
							new EmailNotificationConfigTbl(notificationRequest.getNotificationConfigId()));
			this.emailNotifyToUserRelJpaDao.delete(emailNotifyToUserRelTbls);
			Date date = new Date();
	        String message = notificationRequest.getMessage();
	        String subject = notificationRequest.getSubject();
	        //EmailNotificationConfigTbl configTbl = this.emailNotificationConfigJpaDao.findByEvent(notificationEvent);
	        EmailNotificationConfigTbl configTbl = new EmailNotificationConfigTbl(eventTbl);
			configTbl.setUpdateDate(date);
            
            String event2 = configTbl.getEmailNotificationEventId().getEvent();
            if (event2.equals(NotificationEventType.USER_PROJECT_RELATION_EXPIRY.name()) ||
            		event2.equals(NotificationEventType.USER_PROJECT_RELATION_GRACE_EXPIRY.name())) {
            	//configTbl.setEmailTemplate(this.mailSupport.textToThymleafTemplate(message));
	        }
            emailNotificationConfigTbl = this.emailNotificationConfigJpaDao.save(configTbl);
            List<EmailNotificationConfigTbl> list = new ArrayList<>();
            list.add(emailNotificationConfigTbl);
            notificationResponse = new NotificationResponse(list);
            notificationResponse.setDescription(emailNotificationConfigTbl.getEmailNotificationEventId().getDescription());
			notificationResponse.setEventStatus(emailNotificationConfigTbl.getEmailNotificationEventId().getStatus());
		}
        LOG.info("<< updateNotification");
        return null;
    }*/
    

    @Override
    public List<NotificationConfigResponse> findByEvent(String event) {
		LOG.info(">> findAllNotificationConfig");
		List<NotificationConfigResponse> configResponses = new ArrayList<>();
		EmailNotificationEventTbl emailNotificationEventTbl = this.eventsJpaDao.findByEvent(event);
		List<EmailNotificationConfigTbl> findAll = this.emailNotificationConfigJpaDao.findByEmailNotificationEventId(emailNotificationEventTbl);
		for (EmailNotificationConfigTbl emailNotificationConfigTbl : findAll) {
			NotificationConfigResponse configResponse = new NotificationConfigResponse();
			configResponse.setNotifEventId(emailNotificationConfigTbl.getEmailNotificationEventId().getEmailNotificationEventId());
			configResponse.setNotifEventName(emailNotificationConfigTbl.getEmailNotificationEventId().getEvent());
			configResponse.setNotifEventDescription(emailNotificationConfigTbl.getEmailNotificationEventId().getDescription());
			configResponse.setNotifConfigName(emailNotificationConfigTbl.getName());
			configResponse.setNotifConfigId(emailNotificationConfigTbl.getEmailNotificationConfigId());
			
			configResponse.setNotifConfigEmailTemplateId(emailNotificationConfigTbl.getEmailTemplateId().getEmailTemplateId());
			configResponse.setNotifConfigEmailTemplateMsg(emailNotificationConfigTbl.getEmailTemplateId().getMessage());
			configResponse.setNotifConfigEmailTemplateName(emailNotificationConfigTbl.getEmailTemplateId().getName());
			configResponse.setNotifConfigEmailTemplateSubject(emailNotificationConfigTbl.getEmailTemplateId().getSubject());
			String toUsers = emailNotificationConfigTbl.getEmailNotifyToUserId().getToUsers();
			Set<String> listOfToUsernames = null;
			if(toUsers != null) {
				listOfToUsernames = new HashSet<>();
				listOfToUsernames = this.getListOfUsernames(toUsers);
			}
			configResponse.setNotifConfigToUsers(listOfToUsernames);
			
			String ccUsers = emailNotificationConfigTbl.getEmailNotifyToUserId().getCcUsers();
			Set<String> listOfCcUsernames = null;
			if(ccUsers != null) {
				listOfCcUsernames = new HashSet<>();
				listOfCcUsernames = this.getListOfUsernames(ccUsers);
			}
			configResponse.setNotifConfigCcUsers(listOfCcUsernames);
			configResponse.setNotifConfigStatus(emailNotificationConfigTbl.getStatus());
			configResponse.setNotifEventStatus(emailNotificationConfigTbl.getEmailNotificationEventId().getStatus());
			configResponses.add(configResponse);
		}
		LOG.info("<< findAllNotificationConfig");
		return configResponses;
	}

	@Override
	public boolean updateNotificationStatus(HttpServletRequest httpServletRequest, String event, String status) {
		LOG.info(">> updateNotificationStatus");
		boolean isUpdated = false;
		int updateEventStatus = this.eventsJpaDao.updateEventStatus(status, event);
		if(updateEventStatus > 0) {
			isUpdated = true;
		}
		LOG.info("<< updateNotificationStatus");
		return isUpdated;
	}

	@Override
	public EmailTemplateTbl saveOrUpdateEmailTemplate(EmailTemplateRequest emailTemplateRequest, boolean isUpdate) {
		String uuid = UUID.randomUUID().toString().toUpperCase();
		Date date = new Date();
		String name = emailTemplateRequest.getName();
		if(!isUpdate && this.emailTemplateJpaDao.findByName(name) != null) {
			throw new CannotCreateObjectException("Template already exist", "ERR0002", new String[]{name});
		}
		String message = emailTemplateRequest.getMessage();
		String subject = emailTemplateRequest.getSubject();
		TreeSet<String> templateVariables = emailTemplateRequest.getTemplateVariables();
		String variables = this.mailSupport.getTemplateVariables(templateVariables);
		String template = this.mailSupport.textToThymleafTemplate(message, templateVariables);
		
		EmailTemplateTbl emailTemplateTbl = null;
		if(isUpdate) {
			uuid = emailTemplateRequest.getEmailTempalteId().toUpperCase();
			emailTemplateTbl = this.emailTemplateJpaDao.findOne(uuid);
		} else {
			emailTemplateTbl = new EmailTemplateTbl(uuid);
		}
		emailTemplateTbl.setName(name);
		emailTemplateTbl.setSubject(subject);
		emailTemplateTbl.setMessage(message);
		emailTemplateTbl.setMessageTemplate(template);
		emailTemplateTbl.setTemplateVeriables(variables);
		
		if(!isUpdate)
			emailTemplateTbl.setCreateDate(date);
		emailTemplateTbl.setUpdatedDate(date);
		EmailTemplateTbl savedEntity = this.emailTemplateJpaDao.save(emailTemplateTbl);
		return savedEntity;
	}

	@Override
	public boolean deleteEmailTemplate(String templateId) {
		boolean isUpdated = false;
		try {
			EmailTemplateTbl emailTemplateTbl = this.emailTemplateJpaDao.findOne(templateId);
			List<EmailNotificationConfigTbl> configTbls = this.emailNotificationConfigJpaDao.findByEmailTemplateId(emailTemplateTbl);
			if(configTbls.size() > 0) {
				String templateName = emailTemplateTbl.getName();
				throw new XMObjectNotFoundException("Cannot remove template", "ERR0027", new String[]{templateName});
			}
			this.emailTemplateJpaDao.delete(templateId);
			isUpdated = true;
		} catch (Exception e) {
			LOG.error("Exception occured while delting notification template {}", e.getMessage());
			if (e instanceof EmptyResultDataAccessException) {
                final String[] param = {templateId};
                //throw from here and catch this in multidelete catch block
                throw new XMObjectNotFoundException("Object not found", "P_ERR0038", param);
            }
		}
		return isUpdated;
	}
	
	
	@Override
	public EmailTemplateResponse multiDeleteEmailTemplate(HttpServletRequest httpServletRequest,
			Set<String> templateIds) {
		LOG.info(">> multiDeleteEmailTemplate");
		final List<Map<String, String>> statusMaps = new ArrayList<>();
		for (String templateId : templateIds) {
			try {
				this.deleteEmailTemplate(templateId);
			} catch (XMObjectNotFoundException objectNotFound) {
				Map<String, String> statusMap = messageMaker.extractFromException(objectNotFound);
				statusMaps.add(statusMap);
			}
		}
		EmailTemplateResponse emailTemplateResponse = new EmailTemplateResponse(statusMaps);
		LOG.info("<< multiDeleteEmailTemplate");
		return emailTemplateResponse;
	}

	@Override
	public List<EmailTemplateTbl> findAll() {
		LOG.info(">> findAll");
		Session session = this.sessionFactory.openSession();
		Criteria criteria = session.createCriteria(EmailTemplateTbl.class);
		List<EmailTemplateTbl> emailTemplateTbls = criteria.list();
		session.flush();
		LOG.info("<< findAll");
		return emailTemplateTbls;
	}
	
	
    private Set<String> getUsernamesFromUserIds(List<String> usersIds) {
    	Set<String> userNames = new HashSet<>();
    	usersIds.forEach(userId -> {
    		UsersTbl usersTbl = this.userJpaDao.findByUserId(userId);
    		userNames.add(usersTbl.getUsername());
    	});
    	return userNames;
    }

	@Override
	public boolean deleteNotificationConfig(HttpServletRequest httpServletRequest, String notifConfigId) {
		LOG.info(">> deleteNotificationConfig");
		boolean isDeleted = false;
		try {
			this.emailNotificationConfigJpaDao.delete(notifConfigId);
			isDeleted = true;
		} catch (Exception e) {
			LOG.error("Exception occured while delting notification configuration {}", e.getMessage());
			if (e instanceof EmptyResultDataAccessException) {
                final String[] param = {notifConfigId};
                //throw from here and catch this in multidelete catch block
                throw new XMObjectNotFoundException("Object not found", "P_ERR0037", param);
            }
		}
		LOG.info(">> deleteNotificationConfig");
		return isDeleted;
	}

	@Override
	public NotificationResponse multiDeleteNotificationConfig(HttpServletRequest httpServletRequest, Set<String> notifConfigIds) {
		LOG.info(">> multi deleteNotificationConfig");
		final List<Map<String, String>> statusMaps = new ArrayList<>();
		for (String notifConfigId : notifConfigIds) {
			try {
				this.deleteNotificationConfig(httpServletRequest, notifConfigId);
			} catch (XMObjectNotFoundException objectNotFound) {
				Map<String, String> statusMap = messageMaker.extractFromException(objectNotFound);
				statusMaps.add(statusMap);
			}
		}
		NotificationResponse notificationResponse = new NotificationResponse();
		notificationResponse.setStatusMaps(statusMaps);
		LOG.info("<< multi deleteNotificationConfig");
		return notificationResponse;
	}

	@Override
	public List<EmailNotificationEventTbl> findAllEvents() {
		LOG.info(">> findAllEvents");
		List<EmailNotificationEventTbl> eventTbls = this.eventsJpaDao.findAll();
		LOG.info("<< findAllEvents");
		return eventTbls;
	}

	@Override
	public boolean updateNotificationConfigStatus(HttpServletRequest httpServletRequest, String configId,
			String status) {
		LOG.info(">> updateNotificationConfigStatus");
		boolean isUpdated = false;
		try {
			int update = this.emailNotificationConfigJpaDao.updateEmailNotificationConfigStatus(configId, status);
			if(update > 0) {
				isUpdated = true;
			}
		} catch (Exception e) {
			LOG.error("Error/Exception occured while updating status {}", e.getMessage());
		}
		LOG.info("<< updateNotificationConfigStatus");
		return isUpdated;
	}

}
