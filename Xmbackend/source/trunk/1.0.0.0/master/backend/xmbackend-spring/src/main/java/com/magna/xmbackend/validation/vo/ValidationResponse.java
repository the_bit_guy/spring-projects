/**
 * 
 */
package com.magna.xmbackend.validation.vo;

import java.util.List;
import java.util.Map;

/**
 * @author Bhabadyuti Bal
 *
 */
public class ValidationResponse {
	
	private boolean permissionCheck;
	private List<Map<String, String>> statusMaps;
	
	
	public ValidationResponse(boolean permissionCheck, List<Map<String, String>> statusMaps) {
		this.permissionCheck = permissionCheck;
		this.statusMaps = statusMaps;
	}


	/**
	 * @return the permissionCheck
	 */
	public boolean isPermissionCheck() {
		return permissionCheck;
	}


	/**
	 * @param permissionCheck the permissionCheck to set
	 */
	public void setPermissionCheck(boolean permissionCheck) {
		this.permissionCheck = permissionCheck;
	}


	/**
	 * @return the statusMaps
	 */
	public List<Map<String, String>> getStatusMaps() {
		return statusMaps;
	}


	/**
	 * @param statusMaps the statusMaps to set
	 */
	public void setStatusMaps(List<Map<String, String>> statusMaps) {
		this.statusMaps = statusMaps;
	}
	

}
