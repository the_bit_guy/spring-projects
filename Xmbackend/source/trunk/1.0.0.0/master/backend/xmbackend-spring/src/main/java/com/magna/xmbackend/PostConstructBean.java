/**
 * 
 */
package com.magna.xmbackend;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.magna.xmbackend.vo.enums.IDateTimeFormatConst;

/**
 * @author Bhabadyuti Bal
 *
 */
@Component
public class PostConstructBean {

	private static final Logger LOG = LoggerFactory.getLogger(PostConstructBean.class);
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	
	@PostConstruct
	@Transactional
	public void alterDbSessionDate() {
		this.jdbcTemplate.execute("ALTER SESSION SET NLS_TIMESTAMP_FORMAT = " + IDateTimeFormatConst.DB_DATE_TIME_FORMAT );
		LOG.info("DB date format altered...");
	}
	
}
