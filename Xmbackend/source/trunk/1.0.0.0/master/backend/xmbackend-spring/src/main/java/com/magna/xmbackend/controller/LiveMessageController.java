/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.controller;

import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.magna.xmbackend.audit.mgr.LiveMessageAuditMgr;
import com.magna.xmbackend.entities.LiveMessageTbl;
import com.magna.xmbackend.exception.XMObjectNotFoundException;
import com.magna.xmbackend.mgr.LiveMessageMgr;
import com.magna.xmbackend.vo.liveMessage.LiveMessageCreateRequest;
import com.magna.xmbackend.vo.liveMessage.LiveMessageResponse;
import com.magna.xmbackend.vo.liveMessage.LiveMessageResponseWrapper;
import com.magna.xmbackend.vo.liveMessage.LiveMessageStatusRequest;
import com.magna.xmbackend.vo.liveMessage.LiveMessageStatusResponseWrapper;

/**
 *
 * @author dhana
 */
@RestController
@RequestMapping(value = "/liveMessage")
public class LiveMessageController {

    private static final Logger LOG
            = LoggerFactory.getLogger(LiveMessageController.class);

    @Autowired
    private LiveMessageMgr liveMessageMgr;
    @Autowired
    private LiveMessageAuditMgr liveMessageAuditMgr;

    @RequestMapping(value = "/save",
            method = RequestMethod.POST,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<LiveMessageTbl> save(
            HttpServletRequest httpServletRequest,
            @RequestBody @Valid LiveMessageCreateRequest liveMessageCreateRequest) {
        LOG.info("> save");
        LiveMessageTbl lmt = null;
		try {
			lmt = this.liveMessageMgr.createLiveMessage(liveMessageCreateRequest);
			this.liveMessageAuditMgr.lmCreateSuccessAudit(httpServletRequest, lmt);
		} catch (Exception ex) {
			String lmName = liveMessageCreateRequest.getLiveMessageName();
			LOG.error("Exception / Error occured while creating live message with name {}", lmName);
			this.liveMessageAuditMgr.lmCreateFailureAudit(httpServletRequest, liveMessageCreateRequest, ex.getMessage().split("with ErrorCode")[0]);
			LOG.error("Exception / Error occured while creating live message with name {}", ex.getMessage());
		}

        LOG.info("< save");
        return new ResponseEntity<>(lmt, HttpStatus.ACCEPTED);
    }
    
    
    @RequestMapping(value = "/findAll",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<LiveMessageResponseWrapper> findAll(
            HttpServletRequest httpServletRequest ) {
        LOG.info("> findAll");
        LiveMessageResponseWrapper responseWrapper = this.liveMessageMgr.findAll();
        LOG.info("< findAll");
        return new ResponseEntity<>(responseWrapper, HttpStatus.OK);
    }
    
    
    @RequestMapping(value = "/update",
            method = RequestMethod.PUT,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<Boolean> update(
            HttpServletRequest httpServletRequest, @RequestBody LiveMessageCreateRequest liveMessageCreateRequest ) {
        LOG.info("> update");
        Boolean isUpdated = null;
        String id = liveMessageCreateRequest.getLiveMessageId();
		String name = liveMessageCreateRequest.getLiveMessageName();
		try {
			isUpdated = this.liveMessageMgr.updateLiveMessage(liveMessageCreateRequest);
			
			this.liveMessageAuditMgr.lmUpdateSuccessAudit(httpServletRequest, id, name);
		} catch (Exception ex) {
			LOG.error("Exception / Error in updating live msg with name {} "
                    + "and exception msg {}", name, ex.getMessage());
			String errMsg = "Error in updating live msg with name " + name + " : " + ex.getMessage().split("with ErrorCode")[0];
			this.liveMessageAuditMgr.lmUpdateFailureAudit(httpServletRequest, liveMessageCreateRequest, errMsg);
			throw ex;
		}
        LOG.info("< update");
        return new ResponseEntity<>(isUpdated, HttpStatus.OK);
    }
    
    
    @RequestMapping(value = "/delteById/{liveMsgId}",
            method = RequestMethod.DELETE,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<Boolean> delteById(
            HttpServletRequest httpServletRequest, @PathVariable String liveMsgId ) {
        LOG.info("> delteByMsgId");
        Boolean isUpdated = false;
        String name = "";
        final LiveMessageTbl messageTbl = this.liveMessageMgr.findById(liveMsgId);
		try {
			if(null != messageTbl) {
				isUpdated = this.liveMessageMgr.delteById(liveMsgId);
				this.liveMessageAuditMgr.lmDeleteSuccessAudit(httpServletRequest, liveMsgId, messageTbl.getLiveMessageName());
			} else {
				throw new XMObjectNotFoundException("Live Msg not found with id "+liveMsgId, "ERR0022");
			}
			
			
		} catch (Exception ex) {
			LOG.error("Exception / Error in deleting live msg with id " + liveMsgId);
			String reason = (ex instanceof XMObjectNotFoundException)?(reason = ex.getMessage().split("with ErrorCode")[0]):ex.getMessage();
			String errMsg = "Error in deleting liveMsg with id " + liveMsgId + " and name " + name +" reason" +" : " + reason;
			this.liveMessageAuditMgr.lmDeleteFailureAudit(httpServletRequest, liveMsgId, errMsg);
			throw ex;
		}
        LOG.info("< delteByMsgId");
        return new ResponseEntity<>(isUpdated, HttpStatus.OK);
    }
    
    
    @RequestMapping(value = "/multiDelete",
            method = RequestMethod.DELETE,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<LiveMessageResponse> multiDelete(
            HttpServletRequest httpServletRequest, @RequestBody Set<String> ids ) {
        LOG.info("> multiDelete");
        final LiveMessageResponse liveMessageResponse = this.liveMessageMgr.multiDelete(ids, httpServletRequest);
        LOG.info("< multiDelete");
        return new ResponseEntity<>(liveMessageResponse, HttpStatus.OK);
    }
    
    
    @RequestMapping(value = "/findUnreadLiveMessages",
            method = RequestMethod.POST,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<LiveMessageStatusResponseWrapper> findUnreadLiveMessages(
            HttpServletRequest httpServletRequest,
            @RequestBody LiveMessageStatusRequest liveMessageStatusRequest ) {
        LOG.info("> findLiveMessagesBySiteAndUser");
        final String userName = (String)httpServletRequest.getAttribute("userName");
        final String tkt = httpServletRequest.getHeader("TKT");
        LiveMessageStatusResponseWrapper lmt = this.liveMessageMgr.findLiveMessagesByStatus(liveMessageStatusRequest, userName, tkt);

        LOG.info("< findLiveMessagesBySiteAndUser");
        return new ResponseEntity<>(lmt, HttpStatus.ACCEPTED);
    }
    
    
    @RequestMapping(value = "/updateLiveMessageStatus",
            method = RequestMethod.POST,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<Boolean> updateLiveMessageStatus(
            HttpServletRequest httpServletRequest,
            @RequestBody LiveMessageStatusRequest liveMessageStatusRequest ) {
        LOG.info("> updateLiveMessageStatus");
        final String userName = (String)httpServletRequest.getAttribute("userName");
        final String tkt = httpServletRequest.getHeader("TKT");
        boolean isUpdated = false;
		try {
			String liveMsgStatId = liveMessageStatusRequest.getLiveMessageStatusId();
			String liveMessageId = liveMessageStatusRequest.getLiveMessageId();
			isUpdated = this.liveMessageMgr.updateLiveMessageStatus(liveMessageStatusRequest, userName, tkt);
			LiveMessageTbl liveMsgTbl = this.liveMessageMgr.findById(liveMessageId);
			this.liveMessageAuditMgr.lmStatusUpdateSuccessAudit(httpServletRequest, liveMsgStatId, liveMsgTbl.getLiveMessageName());
		} catch (Exception ex) {
			this.liveMessageAuditMgr.lmStatusUpdateFailureAudit(httpServletRequest, liveMessageStatusRequest, ex.getMessage());
		}
        LOG.info("< updateLiveMessageStatus");
        return new ResponseEntity<>(isUpdated, HttpStatus.ACCEPTED);
    }
    
}
