/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.controller;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.magna.xmbackend.entities.UserTkt;
import com.magna.xmbackend.jpa.dao.UserTktJpaDao;
import com.magna.xmbackend.mgr.AppAuthorizationMgr;
import com.magna.xmbackend.mgr.LdapMgr;
import com.magna.xmbackend.vo.enums.Application;
import com.magna.xmbackend.vo.enums.Status;
import com.magna.xmbackend.vo.user.AuthResponse;
import com.piterion.security.manager.TicketManager;
import com.piterion.security.manager.UserDetails;

/**
 *
 * @author dhana
 */
@RestController
@RequestMapping(value = "/auth")
public class AuthController {

	private static final Logger LOG = LoggerFactory.getLogger(AuthController.class);

	@Autowired
	private LdapMgr ldapMgr;

	@Autowired
	private UserTktJpaDao userTktJpaDao;

	@Autowired
	private AppAuthorizationMgr appAuthorizationMgr;

	@RequestMapping(value = "/login", method = RequestMethod.POST, consumes = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_FORM_URLENCODED_VALUE }, produces = {
					MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE,
					MediaType.APPLICATION_FORM_URLENCODED_VALUE })
	public @ResponseBody final ResponseEntity<AuthResponse> login(HttpServletRequest httpServletRequest,
			HttpServletResponse httpServletResponse) throws Exception {
		LOG.info("> login");
		AuthResponse authResponse;
		HttpHeaders headers = new HttpHeaders();
		final String encryTkt = httpServletRequest.getHeader("TKT");
		final String applicationName = httpServletRequest.getHeader("APP_NAME");
		if(encryTkt == null) {
			authResponse = new AuthResponse("TKT not provided");
			ResponseEntity<AuthResponse> responseEntity = new ResponseEntity<>(authResponse, headers, HttpStatus.OK);
			return responseEntity;
		}
		// need to pass the encrypted tkt
		UserDetails userDetails = null;
		try {
			TicketManager ticketManager = new TicketManager();
			userDetails = ticketManager.validateTicket(encryTkt);
		} catch (Exception e) {
			authResponse = new AuthResponse("Invalid TKT");
			ResponseEntity<AuthResponse> responseEntity = new ResponseEntity<>(authResponse, headers, HttpStatus.OK);
			return responseEntity;
		}
		if (userDetails == null) {
			authResponse = new AuthResponse("Invalid TKT");
			ResponseEntity<AuthResponse> responseEntity = new ResponseEntity<>(authResponse, headers, HttpStatus.OK);
			return responseEntity;
		}
		String username = userDetails.getUsername();
		String machinename = userDetails.getMachinename();

		// boolean isAuthorized = false;
		String[] ldapResp = new String[2];
		long ldapRespTime = 0L;
		//1. CAX_START_MENU Check. If user opens XMenu this check will happen, else for other application GoTo -> B
		if (applicationName.equals(Application.CAX_START_MENU.name())) {
			boolean isCAXUser = this.appAuthorizationMgr.isXMSystemUser(username);

			if (!isCAXUser) {
				authResponse = new AuthResponse(username, isCAXUser, "Invalid CAXMenu user.", ldapRespTime);
				return new ResponseEntity<>(authResponse, HttpStatus.OK);
			} else {
				boolean isCAXUserActive = this.appAuthorizationMgr.isXMSystemUserActive(username);
				if (!isCAXUserActive) {
					authResponse = new AuthResponse(username, isCAXUserActive,
							"User account deactive! Please contact administrator.", ldapRespTime);
					return new ResponseEntity<>(authResponse, HttpStatus.OK);
				}
				if (applicationName.equals(Application.CAX_START_BATCH.name())) {
					boolean isHavingPermission = this.appAuthorizationMgr
							.checkXMSystemUserPermissionForAdminHotlineBatchAccess(username, applicationName);
					if (!isHavingPermission) {
						authResponse = new AuthResponse(username, isHavingPermission, "Invalid CAXMenu User.",
								ldapRespTime);
						return new ResponseEntity<>(authResponse, HttpStatus.OK);
					}
				}
			}
		} else {
			//2. CAX_START_BATCH & CAX_START_ADMIN Check. Check if user is in AD or not
			ldapResp = this.ldapMgr.isUsernameValid(username);
			boolean isLDAPUser = Boolean.valueOf(ldapResp[0]);
			ldapRespTime = Long.valueOf(ldapResp[1]);
			//if not return else GoTo -> B
			if (!isLDAPUser) {
				authResponse = new AuthResponse(username, isLDAPUser, ldapResp[2], ldapRespTime);
				return new ResponseEntity<>(authResponse, HttpStatus.OK);
			} else {
				//3. Check if the ldap user is a XMSystem user
				boolean isCAXUser = this.appAuthorizationMgr.isXMSystemUser(username);
				//if not return else GoTo -> C
				if (!isCAXUser) {
					authResponse = new AuthResponse(username, isCAXUser, "Invalid XMSystem user", ldapRespTime);
					return new ResponseEntity<>(authResponse, HttpStatus.OK);
				} else {
					//4. Check if the XMSystem user is active, if not return with message else GoTo -> D
					boolean isCAXUserActive = this.appAuthorizationMgr.isXMSystemUserActive(username);
					if (!isCAXUserActive && !applicationName.equals(Application.CAX_START_BATCH.name())) {
						authResponse = new AuthResponse(username, isCAXUserActive,
								"User account deactivated! Please contact administrator", ldapRespTime);
						return new ResponseEntity<>(authResponse, HttpStatus.OK);
					} else {
						//5. Check if the user is  
						boolean isAdmin = this.appAuthorizationMgr
								.checkXMSystemUserPermissionForAdminHotlineBatchAccess(username, applicationName);
						if (!isAdmin) {
							authResponse = new AuthResponse(username, isAdmin, "You are not authorized to access current application",
									ldapRespTime);
							return new ResponseEntity<>(authResponse, HttpStatus.OK);
						}
					}
				}
			}
		}

		String userTktId = String.valueOf(UUID.randomUUID());
		Date createdDate = new Date();
		Date updatedDate = new Date();

		String tkt = "TKT" + userTktId;
		// tkt & username encryption using SHA2
		String hashedTkt = hash(tkt);
		/*String hashedUsername = hash(username);
		String hashedMachinename = hash(machinename);
		String hashedAppName = hash(applicationName);*/

		// check if user with tkt exists. If exist then get it and if not exist
		// create new ticket
		// UserTkt userTktFound = this.userTktJpaDao.findByUsername(username);
		UserTkt userTktFound = this.userTktJpaDao.findByUsernameAndApplicationNameAndHostName(username,
				applicationName, machinename);

		if (null != userTktFound) {
			userTktFound.setTkt(hashedTkt);
			userTktFound.setCreateDate(userTktFound.getCreateDate());
			userTktFound.setUpdateDate(updatedDate);
			this.userTktJpaDao.save(userTktFound);
		} else {
			UserTkt userTkt = new UserTkt(userTktId, username, applicationName, hashedTkt, machinename,
					createdDate, updatedDate);
			this.userTktJpaDao.save(userTkt);
		}

		headers.add("Content-Type", "application/json; charset=UTF-8");
		headers.add("TKT", tkt);

		authResponse = new AuthResponse(username, true, Status.VALID_USER.name(), ldapRespTime);
		ResponseEntity<AuthResponse> responseEntity = new ResponseEntity<>(authResponse, headers, HttpStatus.OK);

		LOG.info("< login");
		return responseEntity;
	}

	public String hash(String param)  {
		String bytesToHex = null;
		try {
			MessageDigest digest = MessageDigest.getInstance("SHA-256");
			byte[] encodedhash = digest.digest(param.getBytes(StandardCharsets.UTF_8));
			bytesToHex = bytesToHex(encodedhash);
		} catch (NoSuchAlgorithmException e) {
			LOG.error("Error/ Exception occured while encryption - {}", e.getCause());
		}
		return bytesToHex;
	}

	private static String bytesToHex(byte[] hash) {
		StringBuffer hexString = new StringBuffer();
		for (int i = 0; i < hash.length; i++) {
			String hex = Integer.toHexString(0xff & hash[i]);
			if (hex.length() == 1)
				hexString.append('0');
			hexString.append(hex);
		}
		return hexString.toString();
	}

}
