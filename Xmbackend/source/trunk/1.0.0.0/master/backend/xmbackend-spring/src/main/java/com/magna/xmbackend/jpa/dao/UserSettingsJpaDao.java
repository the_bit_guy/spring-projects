/**
 * 
 */
package com.magna.xmbackend.jpa.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.magna.xmbackend.entities.ProjectsTbl;
import com.magna.xmbackend.entities.SitesTbl;
import com.magna.xmbackend.entities.UserSettingsTbl;
import com.magna.xmbackend.entities.UsersTbl;

/**
 * @author Bhabadyuti Bal
 *
 */
@Transactional
public interface UserSettingsJpaDao extends CrudRepository<UserSettingsTbl, String>{
	
	List<UserSettingsTbl> findBySiteId(SitesTbl siteId);

	UserSettingsTbl findByUserIdAndSiteId(UsersTbl usersTbl, SitesTbl sitesTbl);
	
	@Modifying
	@Query("update UserSettingsTbl ust set ust.projectId =:projectId where ust.userSettingsId =:userSettingsId")
	int updateProject(@Param("projectId") ProjectsTbl projectId, @Param("userSettingsId") String userSettingsId);
	
	
	@Modifying
	@Query("update UserSettingsTbl ust set ust.settings =:settings where ust.userSettingsId =:userSettingsId")
	int updateSetting(@Param("settings") String settings, @Param("userSettingsId") String userSettingsId);
	
}
