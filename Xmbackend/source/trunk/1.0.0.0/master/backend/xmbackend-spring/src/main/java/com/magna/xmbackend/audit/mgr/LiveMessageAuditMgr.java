/**
 * 
 */
package com.magna.xmbackend.audit.mgr;

import javax.servlet.http.HttpServletRequest;

import com.magna.xmbackend.entities.LiveMessageTbl;
import com.magna.xmbackend.vo.liveMessage.LiveMessageCreateRequest;
import com.magna.xmbackend.vo.liveMessage.LiveMessageStatusRequest;

/**
 * @author Bhabadyuti Bal
 *
 */
public interface LiveMessageAuditMgr {

	void lmCreateSuccessAudit(HttpServletRequest httpServletRequest, LiveMessageTbl lmt);

	void lmCreateFailureAudit(HttpServletRequest httpServletRequest, LiveMessageCreateRequest lmcr, String errMsg);

	void lmDeleteFailureAudit(HttpServletRequest hsr, String id, String errMsg);

	void lmDeleteSuccessAudit(HttpServletRequest hsr, String id, String name);

	void lmUpdateSuccessAudit(HttpServletRequest hsr, String id, String name);
	
	void lmStatusUpdateSuccessAudit(HttpServletRequest hsr, String id, String liveMsgName);
	
	void lmUpdateFailureAudit(HttpServletRequest hsr, LiveMessageCreateRequest lmcr, String errMsg);

	void lmStatusUpdateFailureAudit(HttpServletRequest httpServletRequest,
			LiveMessageStatusRequest liveMessageStatusRequest, String message);
	
	

}
