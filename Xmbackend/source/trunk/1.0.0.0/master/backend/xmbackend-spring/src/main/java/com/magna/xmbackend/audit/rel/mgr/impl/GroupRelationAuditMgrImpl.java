/**
 * 
 */
package com.magna.xmbackend.audit.rel.mgr.impl;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.magna.xmbackend.audit.mgr.utils.AdminHistoryRelsUtil;
import com.magna.xmbackend.audit.rel.mgr.GroupRelationAuditMgr;
import com.magna.xmbackend.entities.AdminHistoryRelationsTbl;
import com.magna.xmbackend.entities.GroupRefTbl;
import com.magna.xmbackend.entities.GroupsTbl;
import com.magna.xmbackend.entities.ProjectApplicationsTbl;
import com.magna.xmbackend.entities.ProjectsTbl;
import com.magna.xmbackend.entities.UserApplicationsTbl;
import com.magna.xmbackend.entities.UsersTbl;
import com.magna.xmbackend.jpa.dao.GroupsJpaDao;
import com.magna.xmbackend.jpa.dao.ProjectApplicationJpaDao;
import com.magna.xmbackend.jpa.dao.ProjectJpaDao;
import com.magna.xmbackend.jpa.dao.UserApplicationJpaDao;
import com.magna.xmbackend.jpa.dao.UserJpaDao;
import com.magna.xmbackend.jpa.rel.dao.AdminHistoryRelationJpaDao;
import com.magna.xmbackend.jpa.rel.dao.GroupRelJpaDao;
import com.magna.xmbackend.vo.enums.Groups;
import com.magna.xmbackend.vo.group.GroupRelBatchRequest;
import com.magna.xmbackend.vo.group.GroupRelBatchResponse;
import com.magna.xmbackend.vo.group.GroupRelRequest;
import com.magna.xmbackend.vo.group.GroupRelResponse;

/**
 * @author Bhabadyuti Bal
 *
 */
@Component
public class GroupRelationAuditMgrImpl implements GroupRelationAuditMgr {
	
	private static final Logger LOG
    = LoggerFactory.getLogger(GroupRelationAuditMgrImpl.class);
	
	
	 @Autowired
	 private AdminHistoryRelationJpaDao adminHistoryRelationJpaDao;
	 @Autowired
	 private AdminHistoryRelsUtil adminHistoryRelsUtil;
	 @Autowired
	 private UserJpaDao userJpaDao;
	 @Autowired
	 private ProjectJpaDao projectJpaDao;
	 @Autowired
	 private UserApplicationJpaDao userApplicationJpaDao;
	 @Autowired
	 private ProjectApplicationJpaDao projectApplicationJpaDao;
	 @Autowired
	 private GroupsJpaDao groupsJpaDao;
	 @Autowired
	 private GroupRelJpaDao groupRelJpaDao;
	

	@Override
	public void groupRelMultiSaveAuditor(GroupRelBatchRequest batchReq, GroupRelBatchResponse batchRes,
			HttpServletRequest httpServletRequest) {
		LOG.info(">>> userUserAppMultiSaveAuditor");

		List<GroupRelRequest> groupRelBatchRequests = batchReq.getGroupRelBatchRequest();

		List<GroupRelResponse> groupRelResponses = batchRes.getGroupRelResponses();
		List<Map<String, String>> statusMap = batchRes.getStatusMaps();

		if (statusMap.isEmpty()) {
			// All success
			this.createSuccessAudit(groupRelResponses, httpServletRequest);

		} else {
			//Partial success case
			//Iterate the status map to log the failed ones
			this.createFailureAudit(statusMap, groupRelBatchRequests, httpServletRequest);

			//Iterate the userProjectRelTbls for success ones
			this.createSuccessAudit(groupRelResponses, httpServletRequest);
		}
		LOG.info("<<< userProjectAppMultiSaveAuditor");
		
	}

	private void createSuccessAudit(List<GroupRelResponse> groupRelResponses, HttpServletRequest httpServletRequest) {
		groupRelResponses.forEach(groupRelResponse -> {
			String groupRefId = groupRelResponse.getGroupRefId();
			GroupsTbl groupsTbl = this.groupRelJpaDao.findOne(groupRefId).getGroupId();
			String groupName = groupsTbl.getName();
			String groupType = groupsTbl.getGroupType();
			String objectName = null;
			if(groupType.equals(Groups.USER.name())) {
				objectName = groupRelResponse.getUsersTbl().getUsername();
			} else if(groupType.equals(Groups.PROJECT.name())) {
				objectName = groupRelResponse.getProjectsTbl().getName();
			} else if(groupType.equals(Groups.USERAPPLICATION.name())) {
				objectName = groupRelResponse.getUserApplicationsTbl().getName();
			} else if(groupType.equals(Groups.PROJECTAPPLICATION.name())) {
				objectName = groupRelResponse.getProjectApplicationsTbl().getName();
			}
			

			AdminHistoryRelationsTbl adminHistoryRelationTbl = this.adminHistoryRelsUtil
					.makeAdminHistoryRelationTbl(httpServletRequest, "GroupRelation", groupName,
							groupType, objectName, null,
							null, null, null, "GroupRelation Create",
							"Success");
			this.adminHistoryRelationJpaDao.save(adminHistoryRelationTbl);
		});
		
	}

	private void createFailureAudit(List<Map<String, String>> statusMap, List<GroupRelRequest> groupRelBatchRequests,
			HttpServletRequest httpServletRequest) {
		
		for (Map<String, String> map : statusMap) {
			for(GroupRelRequest groupRelRequest : groupRelBatchRequests) {
				String groupId = groupRelRequest.getGroupId();
				String objectId = groupRelRequest.getObjectId();
				
				String objectName = null;
				
				GroupsTbl groupsTbl = this.groupsJpaDao.findOne(groupId);
				String grpName = groupsTbl.getName();
				String groupType = groupsTbl.getGroupType();
				if(groupType.equals(Groups.USER.name())) {
					UsersTbl usersTbl = this.userJpaDao.findOne(objectId);
					objectName = usersTbl.getUsername();
				} else if(groupType.equals(Groups.PROJECT.name())) {
					ProjectsTbl projectsTbl = this.projectJpaDao.findOne(objectId);
					objectName = projectsTbl.getName();
				} else if(groupType.equals(Groups.USERAPPLICATION.name())) {
					UserApplicationsTbl userApplicationsTbl = this.userApplicationJpaDao.findOne(objectId);
					objectName = userApplicationsTbl.getName();
				} else if(groupType.equals(Groups.PROJECTAPPLICATION.name())) {
					ProjectApplicationsTbl projectApplicationsTbl = this.projectApplicationJpaDao.findOne(objectId);
					objectName = projectApplicationsTbl.getName();
				}
				
				String message = map.get("en");
				if (message.contains(grpName) && message.contains(objectName)) {
					AdminHistoryRelationsTbl adminHistoryRelationTbl = this.adminHistoryRelsUtil
							.makeAdminHistoryRelationTbl(httpServletRequest, "GroupRelation", grpName,
									groupType, objectName, null, null, null, message, "GroupRelation Create",
									"Failure");
					this.adminHistoryRelationJpaDao.save(adminHistoryRelationTbl);
					break;
				}
				
			}
		}
		
		
		
		
		
	}

	@Override
	public void groupRelMultiSaveFailureAuditor(HttpServletRequest httpServletRequest, Exception ex) {
		LOG.info(">>> groupRelMultiSaveFailureAuditor");
		AdminHistoryRelationsTbl adminHistoryRelationTbl = this.adminHistoryRelsUtil.makeAdminHistoryRelationTbl(
				httpServletRequest, "GroupRelation", null, null, null, null, null, null, ex.getMessage(), "GroupRelation Create",
				"Failure");
		this.adminHistoryRelationJpaDao.save(adminHistoryRelationTbl);
		LOG.info("<<< groupRelMultiSaveFailureAuditor");
		
	}

	@Override
	public void groupRelationMultiDeleteSuccessAudit(GroupRefTbl groupRefTbl, HttpServletRequest httpServletRequest) {

		GroupsTbl groupsTbl = groupRefTbl.getGroupId();
		String groupName = groupsTbl.getName();
		String groupType = groupsTbl.getGroupType();
		String objectName = null;
		String objectId = groupRefTbl.getObjectId();
		if(groupType.equals(Groups.USER.name())) {
			UsersTbl usersTbl = this.userJpaDao.findOne(objectId);
			objectName = usersTbl.getUsername();
		} else if(groupType.equals(Groups.PROJECT.name())) {
			ProjectsTbl projectsTbl = this.projectJpaDao.findOne(objectId);
			objectName = projectsTbl.getName();
		} else if(groupType.equals(Groups.USERAPPLICATION.name())) {
			UserApplicationsTbl userApplicationsTbl = this.userApplicationJpaDao.findOne(objectId);
			objectName = userApplicationsTbl.getName();
		} else if(groupType.equals(Groups.PROJECTAPPLICATION.name())) {
			ProjectApplicationsTbl projectApplicationsTbl = this.projectApplicationJpaDao.findOne(objectId);
			objectName = projectApplicationsTbl.getName();
		}
		

		AdminHistoryRelationsTbl adminHistoryRelationTbl = this.adminHistoryRelsUtil
				.makeAdminHistoryRelationTbl(httpServletRequest, "GroupRelation", groupName,
						groupType, objectName, null,
						null, null, null, "GroupRelation Delete",
						"Success");
		this.adminHistoryRelationJpaDao.save(adminHistoryRelationTbl);
	
		
	}
	
	

}
