/**
 * 
 */
package com.magna.xmbackend.audit.rel.mgr.impl;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.magna.xmbackend.audit.mgr.utils.AdminHistoryRelsUtil;
import com.magna.xmbackend.audit.rel.mgr.AdminAreaProjectAuditMgr;
import com.magna.xmbackend.entities.AdminAreaProjectRelTbl;
import com.magna.xmbackend.entities.AdminHistoryRelationsTbl;
import com.magna.xmbackend.entities.ProjectsTbl;
import com.magna.xmbackend.entities.SiteAdminAreaRelTbl;
import com.magna.xmbackend.jpa.dao.ProjectJpaDao;
import com.magna.xmbackend.jpa.rel.dao.AdminAreaProjectRelJpaDao;
import com.magna.xmbackend.jpa.rel.dao.AdminHistoryRelationJpaDao;
import com.magna.xmbackend.jpa.rel.dao.SiteAdminAreaRelJpaDao;
import com.magna.xmbackend.vo.rel.AdminAreaProjectRelBatchRequest;
import com.magna.xmbackend.vo.rel.AdminAreaProjectRelBatchResponse;
import com.magna.xmbackend.vo.rel.AdminAreaProjectRelRequest;

/**
 * @author Bhabadyuti Bal
 *
 */
@Component
public class AdminAreaProjectAudiMgrImpl implements AdminAreaProjectAuditMgr {
	
	private static final Logger LOG = LoggerFactory.getLogger(AdminAreaProjectAudiMgrImpl.class);
	
	final String RELATION_NAME = "AdminAreaProject";
	final String RESULT_SUCCESS = "Success";
	final String RESULT_FAILURE = "Failure";
	
	@Autowired
	private AdminHistoryRelsUtil adminHistoryRelsUtil;
	@Autowired
	private AdminHistoryRelationJpaDao adminHistoryRelationJpaDao;
	@Autowired
	private SiteAdminAreaRelJpaDao siteAdminAreaRelJpaDao;
	@Autowired
	private ProjectJpaDao projectJpaDao;
	@Autowired
	private AdminAreaProjectRelJpaDao adminAreaProjectRelJpaDao;

	@Override
	public void adminAreaProjectMultiSaveFailureAuditor(HttpServletRequest httpServletRequest, Exception ex) {
		AdminHistoryRelationsTbl adminHistoryRelationTbl = this.adminHistoryRelsUtil.makeAdminHistoryRelationTbl(
				httpServletRequest, RELATION_NAME, null, null, null, null, null, null, ex.getMessage(), "AdminAreaProject Create",
				RESULT_FAILURE);
		this.adminHistoryRelationJpaDao.save(adminHistoryRelationTbl);
		
	}

	@Override
	public void adminAreaProjectMultiSaveAuditor(AdminAreaProjectRelBatchRequest aaprbReq,
			AdminAreaProjectRelBatchResponse aaprbRes, HttpServletRequest httpServletRequest) {

		LOG.info(">>> userUserAppMultiSaveAuditor");

		List<AdminAreaProjectRelRequest> adminAreaProjectRelRequests = aaprbReq.getAdminAreaProjectRelRequests();

		List<AdminAreaProjectRelTbl> adminAreaProjectRelTbls = aaprbRes.getAdminAreaProjectRelTbls();
		List<Map<String, String>> statusMap = aaprbRes.getStatusMap();

		if (statusMap.isEmpty()) {
			// All success
			this.createSuccessAudit(adminAreaProjectRelTbls, httpServletRequest);

		} else {
			//Partial success case
			// iterate the status map to log the failed ones
			this.createFailureAudit(statusMap, adminAreaProjectRelRequests, httpServletRequest);

			// iterate the userProjectRelTbls for success ones
			this.createSuccessAudit(adminAreaProjectRelTbls, httpServletRequest);
		}
		LOG.info("<<< userProjectAppMultiSaveAuditor");
	
		
	}

	private void createFailureAudit(List<Map<String, String>> statusMap,
			List<AdminAreaProjectRelRequest> adminAreaProjectRelRequests, HttpServletRequest httpServletRequest) {
		for (Map<String, String> map : statusMap) {
			for (AdminAreaProjectRelRequest adminAreaProjRelReq : adminAreaProjectRelRequests) {
				String siteAdminAreaRelId = adminAreaProjRelReq.getSiteAdminAreaRelId();
				String projectId = adminAreaProjRelReq.getProjectId();
				
				SiteAdminAreaRelTbl siteAdminAreaRelTbl = this.siteAdminAreaRelJpaDao.findOne(siteAdminAreaRelId);
				ProjectsTbl projectsTbl = this.projectJpaDao.findOne(projectId);
				
				
				if (siteAdminAreaRelTbl != null && projectsTbl != null) {
					String message = map.get("en");
					String projectName = projectsTbl.getName();
					String adminAreaName = siteAdminAreaRelTbl.getAdminAreaId().getName();
					if (message.contains(projectName) && message.contains(adminAreaName)) {
						AdminHistoryRelationsTbl adminHistoryRelationTbl = this.adminHistoryRelsUtil
								.makeAdminHistoryRelationTbl(httpServletRequest, RELATION_NAME,
										adminAreaName, projectName,null, 
										null, adminAreaProjRelReq.getStatus(),
										null, message, "AdminAreaProject Create", RESULT_FAILURE);
						this.adminHistoryRelationJpaDao.save(adminHistoryRelationTbl);
						break;
					}
				}
			}
		}
	}

	private void createSuccessAudit(List<AdminAreaProjectRelTbl> adminAreaProjectRelTbls,
			HttpServletRequest httpServletRequest) {
		adminAreaProjectRelTbls.forEach(adminAreaProjectRelTbl -> {
			String adminAreaName = adminAreaProjectRelTbl.getSiteAdminAreaRelId().getAdminAreaId().getName();
			String projectName = adminAreaProjectRelTbl.getProjectId().getName();

			AdminHistoryRelationsTbl adminHistoryRelationTbl = this.adminHistoryRelsUtil
					.makeAdminHistoryRelationTbl(httpServletRequest, RELATION_NAME, adminAreaName,
							projectName, null, null,
							adminAreaProjectRelTbl.getStatus(), null, null, "AdminAreaProject Create",
							RESULT_SUCCESS);
			this.adminHistoryRelationJpaDao.save(adminHistoryRelationTbl);
		});
	}

	@Override
	public void userProjectMultiDeleteSuccessAudit(AdminAreaProjectRelTbl adminAreaProjectRelTbl,
			HttpServletRequest httpServletRequest) {

		String adminAreaName = adminAreaProjectRelTbl.getSiteAdminAreaRelId().getAdminAreaId().getName();
		String projectName = adminAreaProjectRelTbl.getProjectId().getName();

		AdminHistoryRelationsTbl adminHistoryRelationTbl = this.adminHistoryRelsUtil
				.makeAdminHistoryRelationTbl(httpServletRequest, RELATION_NAME, adminAreaName,
						projectName, null, null,
						adminAreaProjectRelTbl.getStatus(), null, null, "AdminAreaProject Delete",
						RESULT_SUCCESS);
		this.adminHistoryRelationJpaDao.save(adminHistoryRelationTbl);
	
		
	}

	@Override
	public void adminAreaProjectStatusUpdateAuditor(boolean updateStatusById, String status, String id,
			HttpServletRequest httpServletRequest) {
		if (updateStatusById) {
			AdminAreaProjectRelTbl adminAreaProjectRelTbl = this.adminAreaProjectRelJpaDao.findOne(id);
			if (adminAreaProjectRelTbl != null) {
				String adminAreaName = adminAreaProjectRelTbl.getSiteAdminAreaRelId().getAdminAreaId().getName();
				String projectName = adminAreaProjectRelTbl.getProjectId().getName();
				AdminHistoryRelationsTbl adminHistoryRelationTbl = this.adminHistoryRelsUtil
						.makeAdminHistoryRelationTbl(httpServletRequest, RELATION_NAME, adminAreaName,
								projectName, null, null, status, null, null, "AdminAreaProject status update",
								RESULT_SUCCESS);
				this.adminHistoryRelationJpaDao.save(adminHistoryRelationTbl);
			}
		}
	}

	
	@Override
	public void adminAreaProjectStatusUpdateFailureAuditor(String id, String status, Exception ex,
			HttpServletRequest httpServletRequest) {
		
		AdminAreaProjectRelTbl adminAreaProjectRelTbl = this.adminAreaProjectRelJpaDao.findOne(id);
		if (adminAreaProjectRelTbl != null) {
			String adminAreaName = adminAreaProjectRelTbl.getSiteAdminAreaRelId().getAdminAreaId().getName();
			String projectName = adminAreaProjectRelTbl.getProjectId().getName();
			String errorMsg = ex.getMessage();
			AdminHistoryRelationsTbl adminHistoryRelationTbl = this.adminHistoryRelsUtil
					.makeAdminHistoryRelationTbl(httpServletRequest, RELATION_NAME, adminAreaName,
							projectName, null, null, status, null, errorMsg, "AdminAreaProject status update",
							RESULT_FAILURE);
			this.adminHistoryRelationJpaDao.save(adminHistoryRelationTbl);
		}
	}

}
