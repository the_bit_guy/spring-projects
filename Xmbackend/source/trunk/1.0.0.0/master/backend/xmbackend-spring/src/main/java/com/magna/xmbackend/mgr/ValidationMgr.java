package com.magna.xmbackend.mgr;

import com.magna.xmbackend.validation.vo.ValidationResponse;
import com.magna.xmbackend.vo.permission.ValidationRequest;
import com.magna.xmbackend.vo.rel.UserProjectRelRequest;

/**
 *
 * @author vijay
 */
public interface ValidationMgr {

    /**
     *
     * @param validationRequest
     * @return boolean
     */
    boolean isObjectAccessAllowed(ValidationRequest validationRequest);

    /**
     * clear the cache in permission filter
     *
     * @return boolean
     */
    boolean clearCache();
    
    
    //ValidationResponse checkHotlinePermissions(final String username);
    
    
    ValidationResponse checkHotlinePermissions(final String username, final String adminAreaId);
    
    
    void isUserProjectAssignmentAllowed(final UserProjectRelRequest userProjectRelRequest, final String userName);
    
    void isUserProjectRemoveAllowed(final UserProjectRelRequest userProjectRelRequest, final String userName);
    
    void isUserProjectActDeactAllowed(final UserProjectRelRequest userProjectRelRequest, final String userName);

}
