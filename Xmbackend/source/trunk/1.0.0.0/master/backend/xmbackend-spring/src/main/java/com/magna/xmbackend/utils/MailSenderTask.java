/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.utils;

import java.time.Duration;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.magna.xmbackend.entities.EmailNotificationConfigTbl;
import com.magna.xmbackend.entities.EmailNotificationEventTbl;
import com.magna.xmbackend.entities.MailQueueTbl;
import com.magna.xmbackend.entities.PropertyConfigTbl;
import com.magna.xmbackend.entities.UserProjectRelTbl;
import com.magna.xmbackend.entities.UsersTbl;
import com.magna.xmbackend.jpa.dao.EmailNotificationConfigJpaDao;
import com.magna.xmbackend.jpa.dao.EmailNotificationEventsJpaDao;
import com.magna.xmbackend.jpa.dao.MailQueueJpaDao;
import com.magna.xmbackend.jpa.dao.ProjectJpaDao;
import com.magna.xmbackend.jpa.dao.PropertyConfigJpaDao;
import com.magna.xmbackend.jpa.dao.UserHistoryJpaDao;
import com.magna.xmbackend.jpa.dao.UserJpaDao;
import com.magna.xmbackend.jpa.rel.dao.UserProjectRelJpaDao;
import com.magna.xmbackend.mail.mgr.UserProjectRelPreNotification;
import com.magna.xmbackend.vo.enums.ConfigCategory;
import com.magna.xmbackend.vo.enums.NotificationEventType;
import com.magna.xmbackend.vo.enums.ProjectExpiryKey;
import com.magna.xmbackend.vo.enums.Status;

/**
 *
 * @author dhana
 */
@Component
public class MailSenderTask {

	private static final Logger LOG = LoggerFactory.getLogger(MailSenderTask.class);

    @Autowired
    private MailQueueJpaDao mailQueueJpaDao;
    @Autowired
    private UserProjectRelJpaDao userProjectRelJpaDao;
    @Autowired
    private UserHistoryJpaDao userHistoryJpaDao;
    @Autowired
    private ProjectJpaDao projectJpaDao;
    @Autowired
    private SendMail sendMail;
    @Autowired
    private PropertyConfigJpaDao propertyConfigJpaDao;
    @Autowired
    private UserProjectRelPreNotification userProjectRelPreNotification;
    @Autowired
    private UserJpaDao userJpaDao;
    @Autowired
    private EmailNotificationEventsJpaDao eventsJpaDao;
    @Autowired
    private EmailNotificationConfigJpaDao configJpaDao;
    
    

    private final String STAGE_NEW = "new";
    private final String STAGE_INPROGRESS = "inprogress";
    private final String STAGE_SUCCESS = "success";
    private final String STAGE_FAILURE = "failure";

    @Value("${cron.mail.failure.retry.count}")
    private int rtcCount;

    @Scheduled(cron = "${cron.expression}")
    public void newMailSendTask() {
        //get the records from mailQ with stage as new
		Iterable<MailQueueTbl> mailQueueTblsWithStageNew = this.mailQueueJpaDao.findByStage(STAGE_NEW);
        mailQueueTblsWithStageNew.forEach(mailQueueTblStageNew -> {
            mailQueueTblStageNew.setStage(STAGE_INPROGRESS);
        });
        Iterable<MailQueueTbl> mailQueueTblStageInPorgress
                = this.mailQueueJpaDao.save(mailQueueTblsWithStageNew);
        mailQueueTblStageInPorgress = this.mailQueueJpaDao.findByStage(STAGE_INPROGRESS);
        this.inProgressMailTaskProcess(mailQueueTblStageInPorgress);
    }

    @Scheduled(cron = "${cron.expression}")
	public void failureMailSendTask() {
		// get the records from mailQ with stage as failure
		Iterable<MailQueueTbl> mailQueueTblsWithStageF = this.mailQueueJpaDao.findByStage(STAGE_FAILURE);
		mailQueueTblsWithStageF.forEach(mailQueueTblStageF -> {

			String rtcFaceLess = mailQueueTblStageF.getReTryCount();
			if (rtcFaceLess != null) {
				int rtcValue = Integer.parseInt(rtcFaceLess);
				if (rtcValue < rtcCount) {
					rtcValue++;
					mailQueueTblStageF.setStage(STAGE_INPROGRESS);
					mailQueueTblStageF.setReTryCount(Integer.toString(rtcValue));
					MailQueueTbl mailQueueTblStageIP = this.mailQueueJpaDao.save(mailQueueTblStageF);
					this.doSingleMailTaskProcess(mailQueueTblStageIP);
				}
			}
		});
	}
    

    private void inProgressMailTaskProcess(
            Iterable<MailQueueTbl> mailQueueTblStageInPorgress) {
        mailQueueTblStageInPorgress.forEach((mailQueueTblStageIP) -> {
            this.doSingleMailTaskProcess(mailQueueTblStageIP);
        });

    }

	private void doSingleMailTaskProcess(MailQueueTbl mailQueueTblStageIP) {
		String subject = mailQueueTblStageIP.getSubject();
		String toMail = mailQueueTblStageIP.getToMail();
		String ccMail = mailQueueTblStageIP.getCcMail(); //has to be as comma separated emails
		String textMessage = mailQueueTblStageIP.getTextMessage();
		try {
			if (toMail != null) {
				this.sendMail.sendEmail(toMail, ccMail, subject, textMessage);
				mailQueueTblStageIP.setStage(STAGE_SUCCESS);
				this.mailQueueJpaDao.save(mailQueueTblStageIP);
			}
		} catch (Exception ex) {
			LOG.debug("ex {}", ex);
			mailQueueTblStageIP.setStage(STAGE_FAILURE);
			mailQueueTblStageIP.setRemarks(ex.getMessage());
			this.mailQueueJpaDao.save(mailQueueTblStageIP);
		}
	}
	
	//@Scheduled(cron = "${cron.expression.projectExpiry}")
	private void projectExpiryNotificationSendTask() {
		//get ACTIVE USER_PROJECT_RELATION_EXPIRY event
		EmailNotificationEventTbl eventTbl = this.eventsJpaDao.findByEventAndStatus(NotificationEventType.USER_PROJECT_RELATION_EXPIRY.name(), Status.ACTIVE.name());
		if (eventTbl != null) {
			//get the ACTIVE configurations or ACTIONs for USER_PROJECT_RELATION_EXPIRY event
			List<EmailNotificationConfigTbl> emailNotificationEventTbls = this.configJpaDao.findByEmailNotificationEventIdAndStatus(eventTbl, Status.ACTIVE.name());
			if(emailNotificationEventTbls.size() > 0 && emailNotificationEventTbls != null) {
				//get all USER_PROJECT_RELATIONs
				Iterable<UserProjectRelTbl> userProjectRelTbls = this.userProjectRelJpaDao.findByStatus(Status.ACTIVE.name());
				for (UserProjectRelTbl userProjectRelTbl : userProjectRelTbls) {
					//for each U-P relations all the configurations/actions has to be executed
					for (EmailNotificationConfigTbl configTbl : emailNotificationEventTbls) {
						long globalExpiryDays = Long.valueOf(userProjectRelTbl.getProjectExpiryDays());
						if (globalExpiryDays > 0) {
							UsersTbl usersTbl = this.userJpaDao.findOne(userProjectRelTbl.getUserId().getUserId());
							if (usersTbl.getEmailId() != null) {
								//if (usersTbl.getUsername().equals("vishakha.c")) {
								String projectName = this.projectJpaDao.findOne(userProjectRelTbl.getProjectId().getProjectId()).getName();
								// the expiry days value has to be >0. If 0 will be
								// considered as NoExpiry.
								Date maxLogTime = this.userHistoryJpaDao.findMaxLogTimeByProject(projectName, usersTbl.getUsername());
								PropertyConfigTbl propertyConfigTbl = this.propertyConfigJpaDao.findByCategoryAndProperty(
												ConfigCategory.PROJECTEXPIRYDAYS.name(), ProjectExpiryKey.DATA_MIGRATION_DATE.name());
								if (propertyConfigTbl == null) {
									LOG.error("DATA_MIGRATION_DATE not provided");
									return;
								}
								String value = propertyConfigTbl.getValue();
								LocalDate dataMigDate = LocalDate.parse(value, DateTimeFormatter.ISO_DATE);
								if (maxLogTime != null) {
									LocalDate lastUsedDate = maxLogTime.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
									if (lastUsedDate.isAfter(dataMigDate)) {
										long projectUsageGap = Duration.between(dataMigDate.atStartOfDay(), lastUsedDate.atStartOfDay()).toDays();
										LOG.debug("projectUsageGap --- {}", projectUsageGap);
										this.processProjectExpiryNotification(userProjectRelTbl, configTbl, projectUsageGap, globalExpiryDays);
									}
								} else {
									// if there is no entry for any user-project access in
									// history then get the updated date and use as lastUsedDate
									LocalDate lastUsedDate = userProjectRelTbl.getUpdateDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
									if (lastUsedDate.isAfter(dataMigDate)) {
										long projectUsageGap = Duration.between(dataMigDate.atStartOfDay(), lastUsedDate.atStartOfDay()).toDays();
										this.processProjectExpiryNotification(userProjectRelTbl, configTbl, projectUsageGap, globalExpiryDays);
									}
								}
							//}
						}
						}
					}
				}
			}
		}
	}
	
	private void processProjectExpiryNotification(UserProjectRelTbl userProjectRelTbl, EmailNotificationConfigTbl configTbl, long projectUsageGap, long globalExpiryDays){
		long gracePeriod = Long.valueOf(this.propertyConfigJpaDao
				.findByCategoryAndProperty("PROJECTEXPIRYDAYS", "PROJECT_EXPIRY_GRACE_PERIOD").getValue());
		if (projectUsageGap > globalExpiryDays) {
			// grace period check
			if (projectUsageGap > (globalExpiryDays + Long.valueOf(gracePeriod))) {
				if (userProjectRelTbl.getExpiryGraceNotifMailFlag().equals("false")) {
					// send notification mail for grace period. Grace period also expired
					this.userProjectRelPreNotification.postToQueueForProjectExpiry(userProjectRelTbl,
							configTbl, projectUsageGap, gracePeriod);
				}
			} else {
				if (userProjectRelTbl.getExpiryNotifMailFlag().equals("false")) {
					// send only expiry notification mail, to provide a grace period
					this.userProjectRelPreNotification.postToQueueForProjectExpiry(userProjectRelTbl,
							configTbl, projectUsageGap, gracePeriod);
				}
			}
		}
	}
}
