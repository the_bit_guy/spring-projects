package com.magna.xmbackend.audit.rel.mgr.impl;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.magna.xmbackend.audit.mgr.utils.AdminHistoryRelsUtil;
import com.magna.xmbackend.audit.rel.mgr.SiteAdminAreaAuditMgr;
import com.magna.xmbackend.entities.AdminAreasTbl;
import com.magna.xmbackend.entities.AdminHistoryRelationsTbl;
import com.magna.xmbackend.entities.SiteAdminAreaRelTbl;
import com.magna.xmbackend.entities.SitesTbl;
import com.magna.xmbackend.exception.CannotCreateRelationshipException;
import com.magna.xmbackend.jpa.dao.AdminAreaJpaDao;
import com.magna.xmbackend.jpa.dao.SiteJpaDao;
import com.magna.xmbackend.jpa.rel.dao.AdminHistoryRelationJpaDao;
import com.magna.xmbackend.jpa.rel.dao.SiteAdminAreaRelJpaDao;
import com.magna.xmbackend.vo.rel.SiteAdminAreaRelBatchRequest;
import com.magna.xmbackend.vo.rel.SiteAdminAreaRelBatchResponse;
import com.magna.xmbackend.vo.rel.SiteAdminAreaRelRequest;
import com.magna.xmbackend.vo.rel.SiteAdminAreaRelResponse;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.regex.Pattern;

/**
 *
 * @author dhana
 */
@Component
public class SiteAdminAreaAuditMgrImpl implements SiteAdminAreaAuditMgr {

    private static final Logger LOG
            = LoggerFactory.getLogger(SiteAdminAreaAuditMgrImpl.class);
    
    final String RELATION_NAME = "SiteAdminArea";
    final String RESULT_SUCCESS = "Success";
    final String RESULT_FAILURE = "Failure";

    @Autowired
    private AdminHistoryRelationJpaDao adminHistoryRelationJpaDao;

    @Autowired
    private SiteJpaDao siteJpaDao;

    @Autowired
    private AdminAreaJpaDao adminAreaJpaDao;

    @Autowired
    private SiteAdminAreaRelJpaDao siteAdminAreaRelJpaDao;
    
	@Autowired
	private AdminHistoryRelsUtil adminHistoryRelsUtil;
	
    /**
     *
     * @param siteAdminAreaRelTbl
     * @param servletRequest
     * @param relationName
     * @param operation
     * @param result
     */
    @Override
    public void siteAdminAreaAuditor(final SiteAdminAreaRelTbl siteAdminAreaRelTbl,
            final HttpServletRequest servletRequest,
            final String relationName, final String operation,
            final String result) {
        LOG.info(">>>> siteAdminAreaCreateAuditor");
        final String siteAdminAreaRelId = siteAdminAreaRelTbl.getSiteAdminAreaRelId();
        final String adminAreaName = siteAdminAreaRelTbl.getAdminAreaId().getName();
        final String siteName = siteAdminAreaRelTbl.getSiteId().getName();
        final String status = siteAdminAreaRelTbl.getStatus();
        LOG.debug("siteAdminAreaRelId {} with adminAreaName {} and siteName {} of status {} created",
                siteAdminAreaRelId, adminAreaName, siteName, status);

        final AdminHistoryRelationsTbl adminHistoryRelationsTbl
                = this.makeAdminHistoryRelationTbl(servletRequest,
                        relationName, siteName, adminAreaName, status, "",
                        operation, result);
        this.adminHistoryRelationJpaDao.save(adminHistoryRelationsTbl);
        LOG.info("<<<< siteAdminAreaCreateAuditor");
    }

    /**
     *
     * @param siteAdminAreaRelRequest
     * @param ccre
     * @param servletRequest
     */
    @Override
    public void siteAdminAreaCreateFailAuditor(final SiteAdminAreaRelRequest siteAdminAreaRelRequest,
            final CannotCreateRelationshipException ccre, final HttpServletRequest servletRequest) {
        LOG.info(">>>> siteAdminAreaCreateFailAuditor");
        final Map<String, String[]> paramMap = ccre.getParamMap();
        final String[] names = paramMap.get("en");
        final String adminAreaName = names[1];
        final String siteName = names[0];
        final String status = siteAdminAreaRelRequest.getStatus();
        AdminHistoryRelationsTbl adminHistoryRelationsTbl
                = this.makeAdminHistoryRelationTbl(servletRequest,
                        "SiteAdminArea", siteName, adminAreaName, status, ccre.getMessage(),
                        "SiteAdminArea Create", "Failure");
        adminHistoryRelationsTbl
                = this.adminHistoryRelationJpaDao.save(adminHistoryRelationsTbl);
        LOG.debug("adminHistoryRelationsTbl={}", adminHistoryRelationsTbl);
        LOG.info("<<<< siteAdminAreaCreateFailAuditor");
    }

    /**
     *
     * @param siteAdminAreaRelBatchRequest
     * @param saarbr
     * @param httpServletRequest
     */
    @Override
    public void siteAdminAreaMultiSaveAuditor(final SiteAdminAreaRelBatchRequest siteAdminAreaRelBatchRequest,
            SiteAdminAreaRelBatchResponse saarbr, final HttpServletRequest httpServletRequest) {
        LOG.info(">>>> siteAdminAreaMultiSaveAuditor");
        final List<Map<String, String>> statusMaps = saarbr.getStatusMap();
        final List<SiteAdminAreaRelRequest> siteAdminAreaRelRequests
                = siteAdminAreaRelBatchRequest.getSiteAdminAreaRelRequests();
        final List<SiteAdminAreaRelTbl> siteAdminAreaRelTbls = saarbr.getSiteAdminAreaRelTbls();
        // If status map is empty, then all are success, save it in db
        if (statusMaps.isEmpty()) {
            logSuccessScenario(siteAdminAreaRelTbls, httpServletRequest);
        } else {
            if (null != siteAdminAreaRelTbls) {
                final int responseSize = siteAdminAreaRelTbls.size();
                // if response size is 0, then it a complete failure scenario
                // save the failure messages
                if (responseSize == 0) {
                    findAndLogMessage(siteAdminAreaRelRequests, statusMaps, httpServletRequest);
                } else {
                    // Partial success and Partial failure scenario
                    // First find the partial failure scenario and log it
                    final List<SiteAdminAreaRelRequest> requests = new ArrayList<>();
                    siteAdminAreaRelRequests.forEach(siteAdminAreaRelRequest -> {
                        // Find from the response, which request is failed
                        final String reqAdminAreaId = siteAdminAreaRelRequest.getAdminAreaId();
                        final String reqSiteId = siteAdminAreaRelRequest.getSiteId();
                        final Optional<SiteAdminAreaRelTbl> siteAdminAreaRelTbl
                                = siteAdminAreaRelTbls.stream().filter(saa
                                        -> saa.getAdminAreaId().getAdminAreaId().equalsIgnoreCase(reqAdminAreaId)
                                && saa.getSiteId().getSiteId().equalsIgnoreCase(reqSiteId)).findFirst();
                        // If any request is failed, save it in db
                        if (!siteAdminAreaRelTbl.isPresent()) {
                            requests.add(siteAdminAreaRelRequest);
                        }
                    });
                    // log the partial failure scenario
                    if (!requests.isEmpty()) {
                        findAndLogMessage(requests, statusMaps, httpServletRequest);
                    }
                    // Log the partial success scenario
                    logSuccessScenario(siteAdminAreaRelTbls, httpServletRequest);
                }
            }
        }
        LOG.info("<<<< siteAdminAreaMultiSaveAuditor");
    }

    /**
     *
     * @param siteAdminAreaRelResponse
     * @param siteAdminAreaIds
     * @param httpServletRequest
     */
    @Override
    public void siteAdminAreaMultiDeleteAuditor(
            final SiteAdminAreaRelResponse siteAdminAreaRelResponse,
            final Set<String> siteAdminAreaIds,
            final HttpServletRequest httpServletRequest) {
        LOG.info(">>>> siteAdminAreaMultiDeleteAuditor");
        final List<Map<String, String>> statusMaps = siteAdminAreaRelResponse.getStatusMaps();
        String result = "Failure";
        StringBuilder errorMessage = new StringBuilder();
        if (statusMaps.isEmpty()) {
            result = "Success";
        } else {
            statusMaps.forEach(statusMap -> {
                statusMap.forEach((key, value) -> {
                    errorMessage.append(" ").append(key).append(":").append(value);
                });
            });
        }
        final StringBuilder siteAdminAreaIdsBuff = new StringBuilder();
        siteAdminAreaIdsBuff.append("Given SiteAdminArea Ids:").append(String.join(" ", siteAdminAreaIds));
        LOG.info("siteAdminAreaIdsBuff={}", siteAdminAreaIdsBuff);
        if (!statusMaps.isEmpty() && (siteAdminAreaIds.size() == statusMaps.size())) {
            result = "Failure";
        } else if (!statusMaps.isEmpty() && (siteAdminAreaIds.size() > statusMaps.size())) {
            result = "Partial Success";
        }
        // As error message column value is 1024, this will work if it doesnt cross the defined limit
        // Need to revisit and modify accordingly
        AdminHistoryRelationsTbl adminHistoryRelationsTbl
                = this.makeAdminHistoryRelationTbl(httpServletRequest,
                        "SiteAdminArea", "", "", "", errorMessage.toString(),
                        "SiteAdminArea Delete", result);
        adminHistoryRelationsTbl
                = this.adminHistoryRelationJpaDao.save(adminHistoryRelationsTbl);
        LOG.info("adminHistoryRelationsTbl={}", adminHistoryRelationsTbl);
        LOG.info("<<<< siteAdminAreaMultiDeleteAuditor");
    }

    /**
     *
     * @param siteAdminAreaId
     * @param httpServletRequest
     */
    @Override
    public void siteAdminAreaDeleteSuccessAuditor(final String siteAdminAreaId,
            final HttpServletRequest httpServletRequest,
            final SiteAdminAreaRelTbl siteAdminAreaRelTbl) {
        LOG.info(">>>> siteAdminAreaDeleteSuccessAuditor");
        String[] names = retrieveNames(siteAdminAreaRelTbl);
        final StringBuilder siteAdminAreaIdBuff = new StringBuilder();
        siteAdminAreaIdBuff.append("SiteAdminArea Deleted with id:").append(siteAdminAreaId);
        // Logging the information in error message column. Need to revisit
        AdminHistoryRelationsTbl adminHistoryRelationsTbl
                = this.makeAdminHistoryRelationTbl(httpServletRequest,
                        "SiteAdminArea", names[0], names[1], names[2],
                        siteAdminAreaIdBuff.toString(),
                        "SiteAdminArea Delete", "Success");
        adminHistoryRelationsTbl
                = this.adminHistoryRelationJpaDao.save(adminHistoryRelationsTbl);
        LOG.debug("adminHistoryRelationsTbl={}", adminHistoryRelationsTbl);
        LOG.info("<<<< siteAdminAreaDeleteSuccessAuditor");
    }

    /**
     *
     * @param siteAdminAreaId
     * @param errMsg
     * @param httpServletRequest
     */
    @Override
    public void siteAdminAreaUpdateFailAuditor(final String siteAdminAreaId,
            final String errMsg, final HttpServletRequest httpServletRequest) {
        LOG.info(">>>> siteAdminAreaUpdateFailAuditor for siteAdminAreaId={}", siteAdminAreaId);
        final StringBuilder siteAdminAreaIdBuff = new StringBuilder();
        siteAdminAreaIdBuff.append("SiteAdminArea update failed for id:").append(siteAdminAreaId);
        // Logging the information in error message column. Need to revisit
        AdminHistoryRelationsTbl adminHistoryRelationsTbl
                = this.makeAdminHistoryRelationTbl(httpServletRequest,
                        "SiteAdminArea", "", "", "", siteAdminAreaIdBuff.toString(),
                        "SiteAdminArea Update", "Failure");
        adminHistoryRelationsTbl
                = this.adminHistoryRelationJpaDao.save(adminHistoryRelationsTbl);
        LOG.debug("adminHistoryRelationsTbl={}", adminHistoryRelationsTbl);
        LOG.info("<<<< siteAdminAreaUpdateFailAuditor");
    }

    /**
     *
     * @param siteAdminAreaId
     * @param httpServletRequest
     */
    @Override
    public void siteAdminAreaUpdateSuccessAuditor(final String siteAdminAreaId,
            final HttpServletRequest httpServletRequest) {
        LOG.info(">>>> siteAdminAreaUpdateSuccessAuditor for Id={}", siteAdminAreaId);
        final SiteAdminAreaRelTbl siteAdminAreaRelTbl = siteAdminAreaRelJpaDao.findOne(siteAdminAreaId);
        final String siteName = siteAdminAreaRelTbl.getSiteId().getName();
        final String adminAreaName = siteAdminAreaRelTbl.getAdminAreaId().getName();
        final String status = siteAdminAreaRelTbl.getStatus();
        AdminHistoryRelationsTbl adminHistoryRelationsTbl
                = this.makeAdminHistoryRelationTbl(httpServletRequest,
                        "SiteAdminArea", siteName, adminAreaName,
                        status, "", "SiteAdminArea Update", "Success");
        adminHistoryRelationsTbl
                = this.adminHistoryRelationJpaDao.save(adminHistoryRelationsTbl);
        LOG.info("adminHistoryRelationsTbl={}", adminHistoryRelationsTbl);
        LOG.info("<<<< siteAdminAreaUpdateSuccessAuditor");
    }

    /**
     *
     * @param siteAdminAreaRelTbls
     * @param httpServletRequest
     */
    private void logSuccessScenario(final List<SiteAdminAreaRelTbl> siteAdminAreaRelTbls,
            final HttpServletRequest httpServletRequest) {
        siteAdminAreaRelTbls.forEach(siteAdminAreaRelTbl -> {
            siteAdminAreaAuditor(siteAdminAreaRelTbl, httpServletRequest,
                    "SiteAdminArea", "SiteAdminArea Create", "Success");
        });
    }

    /**
     *
     * @param siteAdminAreaRelRequests
     * @param statusMaps
     * @param httpServletRequest
     */
    private void findAndLogMessage(final List<SiteAdminAreaRelRequest> siteAdminAreaRelRequests,
            final List<Map<String, String>> statusMaps,
            final HttpServletRequest httpServletRequest) {
        siteAdminAreaRelRequests.forEach(siteAdminAreaRelRequest -> {
            final String[] message = retrieveNames(siteAdminAreaRelRequest, statusMaps);
            AdminHistoryRelationsTbl adminHistoryRelationsTbl
                    = this.makeAdminHistoryRelationTbl(httpServletRequest,
                            "SiteAdminArea", message[0], message[1],
                            message[2], message[3],
                            "SiteAdminArea Create", "Failure");
            adminHistoryRelationsTbl
                    = this.adminHistoryRelationJpaDao.save(adminHistoryRelationsTbl);
            LOG.info("adminHistoryRelationsTbl={}", adminHistoryRelationsTbl);
        });
    }

    /**
     *
     * @param siteAdminAreaRelRequest
     * @return String
     */
    private String[] retrieveNames(final SiteAdminAreaRelRequest siteAdminAreaRelRequest,
            final List<Map<String, String>> statusMaps) {
        final String[] names = new String[4];
        String siteName = "";
        String adminAreaName = "";
        String message = "";
        final String siteId = siteAdminAreaRelRequest.getSiteId();
        final String adminAreaId = siteAdminAreaRelRequest.getAdminAreaId();
        final String status = siteAdminAreaRelRequest.getStatus();
        final SitesTbl sitesTbl = siteJpaDao.findOne(siteId);
        final AdminAreasTbl adminAreasTbl = adminAreaJpaDao.findOne(adminAreaId);
        if (null != sitesTbl) {
            siteName = sitesTbl.getName();
        }
        if (null != adminAreasTbl) {
            adminAreaName = adminAreasTbl.getName();
        }
        if (!siteName.trim().isEmpty() && !adminAreaName.trim().isEmpty()) {
            for (final Map<String, String> statusMap : statusMaps) {
                LOG.info("statusMap{}", statusMap);
                message = statusMap.get("en");
                final Pattern pattern = Pattern.compile("[\\'\"]");
                final String[] result = pattern.split(message);
                final List<String> resultList = Arrays.asList(result);
                if (resultList.contains(siteName) && resultList.contains(adminAreaName)) {
                    break;
                }
            }
        }
        names[0] = siteName;
        names[1] = adminAreaName;
        names[2] = status;
        names[3] = message;
        return names;
    }

    /**
     *
     * @param siteAdminAreaRelRequest
     * @return String
     */
    private String[] retrieveNames(final SiteAdminAreaRelTbl siteAdminAreaRelTbl) {
        final String[] names = new String[3];
        String siteName = "";
        String adminAreaName = "";
        String status = "";
        if (null != siteAdminAreaRelTbl) {
            siteName = siteAdminAreaRelTbl.getSiteId().getName();
            adminAreaName = siteAdminAreaRelTbl.getAdminAreaId().getName();
            status = siteAdminAreaRelTbl.getStatus();
        }
        names[0] = siteName;
        names[1] = adminAreaName;
        names[2] = status;
        return names;
    }

    /**
     *
     * @param siteAdminAreaId
     * @param errMsg
     * @param httpServletRequest
     */
    @Override
    public void siteAdminAreaDeleteFailureAuditor(final String siteAdminAreaId,
            final String errMsg, final HttpServletRequest httpServletRequest,
            final SiteAdminAreaRelTbl siteAdminAreaRelTbl) {
        LOG.info(">>>> siteAdminAreaDeleteFailureAuditor");
        String[] names = retrieveNames(siteAdminAreaRelTbl);
        AdminHistoryRelationsTbl adminHistoryRelationsTbl
                = this.makeAdminHistoryRelationTbl(httpServletRequest,
                        "SiteAdminArea", names[0], names[1], names[2],
                        errMsg.concat(" ").concat(siteAdminAreaId),
                        "SiteAdminArea Delete", "Failure");
        adminHistoryRelationsTbl
                = this.adminHistoryRelationJpaDao.save(adminHistoryRelationsTbl);
        LOG.info("adminHistoryRelationsTbl={}", adminHistoryRelationsTbl);
        LOG.info("<<<< siteAdminAreaDeleteFailureAuditor");
    }

    /**
     *
     * @param servletRequest
     * @param relationName
     * @param relationObjectName1
     * @param relationObjectName2
     * @param status
     * @param errorMessage
     * @param operation
     * @param result
     * @return AdminHistoryRelationsTbl
     */
    private AdminHistoryRelationsTbl makeAdminHistoryRelationTbl(
            final HttpServletRequest servletRequest, final String relationName,
            final String relationObjectName1,
            final String relationObjectName2,
            final String status,
            final String errorMessage,
            final String operation,
            final String result) {
        final Date date = new Date();
        final AdminHistoryRelationsTbl adminHistoryRelationsTbl
                = new AdminHistoryRelationsTbl();
        switch (relationName) {
            case "SiteAdminArea":
                adminHistoryRelationsTbl.setSite(relationObjectName1);
                adminHistoryRelationsTbl.setAdminArea(relationObjectName2);
                adminHistoryRelationsTbl.setStatus(status);
                break;
            default:
                break;
        }
        adminHistoryRelationsTbl.setAdminName(servletRequest.getHeader("USER_NAME"));
        adminHistoryRelationsTbl.setApiRequestPath(servletRequest.getServletPath());
        adminHistoryRelationsTbl.setErrorMessage(errorMessage);
        adminHistoryRelationsTbl.setLogTime(date);
        adminHistoryRelationsTbl.setOperation(operation);
        adminHistoryRelationsTbl.setResult(result);
        return adminHistoryRelationsTbl;
    }

	@Override
	public void siteAdminAreaMultiDeleteSuccessAudit(SiteAdminAreaRelTbl siteAdminAreaRelTbl,
			HttpServletRequest httpServletRequest) {
		String adminAreaName = siteAdminAreaRelTbl.getAdminAreaId().getName();
		String siteName = siteAdminAreaRelTbl.getSiteId().getName();
		String status = siteAdminAreaRelTbl.getStatus();
		
		AdminHistoryRelationsTbl adminHistoryRelationTbl = this.adminHistoryRelsUtil
				.makeAdminHistoryRelationTbl(httpServletRequest, RELATION_NAME, siteName,
						adminAreaName, null, null,
						status, null, null, "SiteAdminAreaRelation Delete",
						"Success");
		this.adminHistoryRelationJpaDao.save(adminHistoryRelationTbl);
		
	}

	@Override
	public void siteAdminAreaStatusUpdateAuditor(boolean updateStatusById, String status, String id,
			HttpServletRequest httpServletRequest) {
		if (updateStatusById) {
			SiteAdminAreaRelTbl siteAdminAreaRelTbl = this.siteAdminAreaRelJpaDao.findOne(id);
			if (siteAdminAreaRelTbl != null) {
				String siteName = siteAdminAreaRelTbl.getSiteId().getName();
				String adminAreaName = siteAdminAreaRelTbl.getAdminAreaId().getName();
				AdminHistoryRelationsTbl adminHistoryRelationTbl = this.adminHistoryRelsUtil
						.makeAdminHistoryRelationTbl(httpServletRequest, RELATION_NAME, siteName,
								adminAreaName, null, null, status, null, null, "SiteAdminArea status update",
								RESULT_SUCCESS);
				this.adminHistoryRelationJpaDao.save(adminHistoryRelationTbl);
			}

		}
		
	}

	@Override
	public void siteAdminAreaStatusUpdateFailureAuditor(String id, String status, Exception ex,
			HttpServletRequest httpServletRequest) {
		SiteAdminAreaRelTbl siteAdminAreaRelTbl = this.siteAdminAreaRelJpaDao.findOne(id);
		if (siteAdminAreaRelTbl != null) {
			String siteName = siteAdminAreaRelTbl.getSiteId().getName();
			String adminAreaName = siteAdminAreaRelTbl.getAdminAreaId().getName();
			String errorMsg = ex.getMessage();
			AdminHistoryRelationsTbl adminHistoryRelationTbl = this.adminHistoryRelsUtil
					.makeAdminHistoryRelationTbl(httpServletRequest, RELATION_NAME, siteName,
							adminAreaName, null, null, status, null, errorMsg, "SiteAdminArea status update",
							RESULT_FAILURE);
			this.adminHistoryRelationJpaDao.save(adminHistoryRelationTbl);
		}
	}
}
