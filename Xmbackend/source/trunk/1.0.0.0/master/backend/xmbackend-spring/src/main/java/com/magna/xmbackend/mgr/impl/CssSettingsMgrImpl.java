/**
 * 
 */
package com.magna.xmbackend.mgr.impl;

import java.util.Date;
import java.util.Optional;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.magna.xmbackend.entities.CssSettingsTbl;
import com.magna.xmbackend.entities.UsersTbl;
import com.magna.xmbackend.exception.XMObjectNotFoundException;
import com.magna.xmbackend.jpa.dao.CssSettingsJpaDao;
import com.magna.xmbackend.jpa.dao.UserJpaDao;
import com.magna.xmbackend.mgr.CssSettingsMgr;
import com.magna.xmbackend.vo.css.CssSettingsRequest;

/**
 * @author Bhabadyuti Bal
 *
 */
@Service
public class CssSettingsMgrImpl implements CssSettingsMgr {
	
	private static final Logger LOG = LoggerFactory.getLogger(CssSettingsMgrImpl.class);
	
	@Autowired
	private CssSettingsJpaDao cssSettingsJpaDao;
	@Autowired
	private UserJpaDao userJpaDao;

	
	private CssSettingsTbl convert2Entity(final CssSettingsRequest cssSettingsRequest, String userName){
		
		String fileContent = cssSettingsRequest.getFileContent();
		String fileName = cssSettingsRequest.getFileName();
		boolean applied = cssSettingsRequest.isApplied();
		Date date = new Date();
		String cssSettingsId = UUID.randomUUID().toString().toUpperCase();
		CssSettingsTbl cssSettingsTbl = null;
		try {
			UsersTbl usersTbl = this.userJpaDao.findByUsernameIgnoreCase(userName);
			if(usersTbl == null) {
				throw new XMObjectNotFoundException("User not found", "P_ERR0008");
			}
			Optional<CssSettingsTbl> findByUserId = this.cssSettingsJpaDao.findByUser(usersTbl);
			if (findByUserId.isPresent()) {
				cssSettingsId = findByUserId.get().getCssSettingsId();
			}
			cssSettingsTbl = new CssSettingsTbl(cssSettingsId);
			cssSettingsTbl.setUserId(usersTbl);
			cssSettingsTbl.setCssContent(fileContent);
			cssSettingsTbl.setFileName(fileName);
			cssSettingsTbl.setIsApplied(String.valueOf(applied));
			cssSettingsTbl.setCreateDate(date);
			cssSettingsTbl.setUpdateDate(date);
		} catch (Exception e) {
			LOG.error("Exception / Error occured in css settings convert2Entity - {}", e.getCause());
		}
		
		return cssSettingsTbl;
	}

	@Override
	public CssSettingsTbl findCssSettingsForUser(String userName) {
		LOG.info(">> findCssSettingsForUser");
		UsersTbl usersTbl = this.userJpaDao.findByUsernameIgnoreCase(userName);
		if(usersTbl == null) {
			throw new XMObjectNotFoundException("User not found", "P_ERR0008");
		}
		Optional<CssSettingsTbl> findByUserId = this.cssSettingsJpaDao.findByUser(usersTbl);
		if (findByUserId.isPresent()) {
			return findByUserId.get();
		}
		LOG.info("<< findCssSettingsForUser");
		return null;
	}

	@Override
	public boolean deleteCssSettings(String userName) {
		LOG.info(">> deleteCssSettings");
		boolean isDeleted = false;
		try {
			UsersTbl usersTbl = this.userJpaDao.findByUsernameIgnoreCase(userName);
			if(usersTbl == null) {
				throw new XMObjectNotFoundException("User not found", "P_ERR0008");
			}
			this.cssSettingsJpaDao.deleteByUser(usersTbl);
			isDeleted = true;
		} catch (Exception e) {
			LOG.error("Exception / Error occured while deleting css settings - {}", e.getCause());
		}
		LOG.info("<< deleteCssSettings");
		return isDeleted;
	}

	@Override
	public boolean createOrUpdate(CssSettingsRequest cssSettingsRequest, String userName) {
		LOG.info(">> createOrUpdate");
		boolean isSaved = false;
		try {
			CssSettingsTbl convert2Entity = this.convert2Entity(cssSettingsRequest, userName);
			this.cssSettingsJpaDao.save(convert2Entity);
			isSaved = true;
		} catch (Exception e) {
			LOG.error("Exception / Error occured while creating css settings - {}", e.getCause());
		}
		LOG.info("<< createOrUpdate");
		return isSaved;
	}

	@Override
	public boolean updateCssSettings(String userName, String isApplied) {
		LOG.info(">> updateCssSettings");
		boolean isUpdated = false;
		UsersTbl usersTbl = this.userJpaDao.findByUsernameIgnoreCase(userName);
		if(usersTbl == null) {
			throw new XMObjectNotFoundException("User not found", "P_ERR0008");
		}
		int appliedStatus = this.cssSettingsJpaDao.updateAppliedStatus(usersTbl, isApplied);
		if (appliedStatus > 0) {
			isUpdated = true;
		}
		LOG.info(">> updateCssSettings");
		return isUpdated;
	}
	

}
