package com.magna.xmbackend.controller;

import com.magna.xmbackend.audit.mgr.RoleAuditMgr;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.magna.xmbackend.entities.RolesTbl;
import com.magna.xmbackend.mgr.RoleMgr;
import com.magna.xmbackend.vo.roles.RoleRequest;
import com.magna.xmbackend.vo.roles.RoleResponse;

@RestController
@RequestMapping(value = "/role")
public class RoleController {

    private static final Logger LOG
            = LoggerFactory.getLogger(RoleController.class);

    @Autowired
    private RoleMgr roleMgr;

    @Autowired
    RoleAuditMgr roleAuditMgr;

    /**
     *
     * @param httpServletRequest
     * @param roleRequestList
     * @return RolesTbl
     */
    @RequestMapping(value = "/save",
            method = RequestMethod.POST,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<RoleResponse> save(
            HttpServletRequest httpServletRequest,
            @RequestBody List<RoleRequest> roleRequestList) {
        RoleResponse roleResponse = null;
        LOG.info("> save");
        try {
            final List<RolesTbl> rolesTbls = this.roleMgr.create(roleRequestList);
            roleResponse = new RoleResponse(rolesTbls);
            this.roleAuditMgr.roleCreateSuccessAuditor(roleResponse,
                    httpServletRequest);
        } catch (Exception ex) {
            LOG.error("Exception / Error occured in creating");
            this.roleAuditMgr.roleCreateFailureAuditor(
                    roleRequestList, httpServletRequest);
            throw ex;
        }
        LOG.info("< save");
        return new ResponseEntity<>(roleResponse, HttpStatus.ACCEPTED);
    }

    /**
     *
     * @param httpServletRequest
     * @param roleRequestList
     * @return RolesTbl
     */
    @RequestMapping(value = "/update",
            method = RequestMethod.POST,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<RoleResponse> update(
            HttpServletRequest httpServletRequest,
            @RequestBody List<RoleRequest> roleRequestList) {
        RoleResponse roleResponse = null;
        LOG.info("> update");
        try {
            final List<RolesTbl> rolesTbls = this.roleMgr.update(roleRequestList);
            roleResponse = new RoleResponse(rolesTbls);
            this.roleAuditMgr.roleUpdateSuccessAuditor(roleResponse,
                    httpServletRequest);
        } catch (Exception ex) {
            LOG.error("Exception / Error occured in update");
            this.roleAuditMgr.roleUpdateFailureAuditor(
                    roleRequestList, httpServletRequest);
            throw ex;
        }
        LOG.info("< update");
        return new ResponseEntity<>(roleResponse, HttpStatus.ACCEPTED);
    }

    /**
     *
     * @param httpServletRequest
     * @param roleId
     * @return RolesTbl
     */
    @RequestMapping(value = "/find/{roleId}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<RolesTbl> findById(
            HttpServletRequest httpServletRequest,
            @PathVariable String roleId) {
        LOG.info("> findById");
        final RolesTbl rolesTbl = this.roleMgr.findById(roleId);
        LOG.info("< findById");
        return new ResponseEntity<>(rolesTbl, HttpStatus.OK);
    }

    /**
     *
     * @param httpServletRequest
     * @return RoleResponse
     */
    @RequestMapping(value = "/findAll",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<RoleResponse> findAll(
            HttpServletRequest httpServletRequest) {
        LOG.info("> findAll");
        final RoleResponse roleResponse = this.roleMgr.findAll();
        LOG.info("< findAll");
        return new ResponseEntity<>(roleResponse, HttpStatus.OK);
    }

    /**
     *
     * @param httpServletRequest
     * @param roleId
     * @return boolean
     */
    @RequestMapping(value = "/delete/{roleId}",
            method = RequestMethod.DELETE,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<Boolean> delete(
            HttpServletRequest httpServletRequest,
            @PathVariable String roleId) {
        RolesTbl rolesTbl = null;
        boolean isDeleted = false;
        LOG.info("> delete");
        try {
            rolesTbl = this.roleMgr.findById(roleId);
            isDeleted = this.roleMgr.deleteById(roleId);
            this.roleAuditMgr.roleDeleteStatusAuditor(roleId,
                    httpServletRequest, isDeleted, rolesTbl);
        } catch (Exception ex) {
            LOG.error("Exception / error occured in deleting role={}", roleId);
            this.roleAuditMgr.roleDeleteStatusAuditor(roleId,
                    httpServletRequest, isDeleted, rolesTbl);
            throw ex;
        }
        LOG.info("< delete");
        return new ResponseEntity<>(isDeleted, HttpStatus.ACCEPTED);
    }

    /**
     *
     * @param httpServletRequest
     * @param ids
     * @return RoleResponse
     */
    @RequestMapping(value = "/multiDelete",
            method = RequestMethod.DELETE,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<RoleResponse> multiDelete(
            HttpServletRequest httpServletRequest,
            @RequestBody Set<String> ids) {
        LOG.info("> multiDelete");
        final RoleResponse roleResponse = this.roleMgr.multiDelete(ids, httpServletRequest);
        //this.roleAuditMgr.roleMultiDeleteAuditor(roleResponse, ids, httpServletRequest);
        LOG.info("< multiDelete");
        return new ResponseEntity<>(roleResponse, HttpStatus.ACCEPTED);
    }

}
