/**
 * 
 */
package com.magna.xmbackend.utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import com.magna.xmbackend.exception.XMObjectNotFoundException;
import com.magna.xmbackend.jpa.dao.EmailTemplateJpaDao;
import com.magna.xmbackend.vo.enums.NotificationVariables;

/**
 * @author Bhabadyuti Bal
 *
 */
@Component
public class MailSupport {

	private static final Logger LOG = LoggerFactory.getLogger(MailSupport.class);
	
	@Autowired
    private ResourceLoader resourceLoader;
	@Autowired
    private TemplateEngine templateEngine;
	@Autowired
	private EmailTemplateJpaDao emailTemplateJpaDao;

	public void createDynamicTemplate(final String event) {
		File file = null;
		BufferedWriter writer = null;
		try {
			String emailTemplate = this.emailTemplateJpaDao.findMessageTemplateByEvent(event);
			/*String content = IOUtils
					.toString(resourceLoader.getResource("classpath:templates/common_template.html").getInputStream());*/
			file = resourceLoader.getResource("classpath:templates/common_template.html").getFile();
			if (file.exists()) {
				file.delete();
			}
			file.createNewFile();
			FileOutputStream fileOutput = new FileOutputStream(file);
			writer = new BufferedWriter(new OutputStreamWriter(fileOutput));
			writer.write(emailTemplate);
			String content1 = new String(Files.readAllBytes(Paths.get(file.getPath())));
			LOG.debug("content1=== {}", content1);
			writer.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public void createDynamicTemplateForDirectSendMail(final String emailTemplate) {
		File file = null;
		BufferedWriter writer = null;
		try {
			/*String content = IOUtils
					.toString(resourceLoader.getResource("classpath:templates/common_template.html").getInputStream());*/
			file = resourceLoader.getResource("classpath:templates/common_template.html").getFile();
			if (file.exists()) {
				file.delete();
			}
			file.createNewFile();
			FileOutputStream fileOutput = new FileOutputStream(file);
			// String(Files.readAllBytes(Paths.get(file.getPath())));
			writer = new BufferedWriter(new OutputStreamWriter(fileOutput));
			writer.write(emailTemplate);
			writer.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String textToHTML(String content) {
		if (content == null) {
			return null;
		}
		int length = content.length();
		boolean prevSlashR = false;
		StringBuffer out = new StringBuffer();
		for (int i = 0; i < length; i++) {
			char ch = content.charAt(i);
			switch (ch) {
			case '\r':
				if (prevSlashR) {
					out.append("<br/>");
				}
				prevSlashR = true;
				break;
			case '\n':
				prevSlashR = false;
				out.append("<br/>");
				break;
			case '"':
				if (prevSlashR) {
					out.append("<br/>");
					prevSlashR = false;
				}
				out.append("&quot;");
				break;
			case '<':
				if (prevSlashR) {
					out.append("<br>");
					prevSlashR = false;
				}
				out.append("&lt;");
				break;
			case '>':
				if (prevSlashR) {
					out.append("<br/>");
					prevSlashR = false;
				}
				out.append("&gt;");
				break;
			case '&':
				if (prevSlashR) {
					out.append("<br/>");
					prevSlashR = false;
				}
				out.append("&amp;");
				break;
			default:
				if (prevSlashR) {
					out.append("<br/>");
					prevSlashR = false;
				}
				out.append(ch);
				break;
			}
		}
		return out.toString();
	}
	
	public String textToThymleafTemplate(final String notificationMsg, Set<String> templateVariables) {

		String remarks_en = "remarks_en";
		String remarks_de = "remarks_de";
		String remarksEn = "remarksEn";
		String remarksDe = "remarksDe";
		StringBuffer sb = new StringBuffer();
		sb.append("<html xmlns:th=\"http://www.thymeleaf.org\">");
		sb.append("<head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" /></head>");
		sb.append("<body>");
		sb.append("<div>");
		String htmlContent = null;
		// %% need to be changed to ${} format
		htmlContent = this.textToHTML(notificationMsg);
		for(String var : templateVariables) {
			if(notificationMsg.contains(var)) {
				String varWithoutPercent = var.replace("%", "");
				//if %remarks% variable is there in the input template then add en & de formats
				if(var.equals(NotificationVariables.REMARKS.toString())) {
					String remarksEnTemplate = "<div th:if=\"\\$\\{"+remarksEn+"} == true\"><u>Remarks_EN</u><span th:text=\"\\$\\{"+remarks_en+"}\">InputText</span></div>";
					String remarksDeTemplate = "<div th:if=\"\\$\\{"+remarksDe+"} == true\"><u>Remarks_DE</u><span th:text=\"\\$\\{"+remarks_de+"}\">InputText</span></div>";
					String remarks = remarksEnTemplate.concat(remarksDeTemplate);
					htmlContent = htmlContent.replaceAll(var, remarks);
				} else {
					htmlContent = htmlContent.replaceAll(var, "<span th:text=\"\\$\\{"+varWithoutPercent+"}\">InputText</span>");
				}
			}
		}
		sb.append(htmlContent);
		/*htmlContent = htmlContent.replaceAll("\\$", "<span th:text=\"\\$");
		htmlContent = htmlContent.replaceAll("}", "}\">InputText</span>");
		sb.append(htmlContent);*/
		sb.append("</div>");
		sb.append("<div>");
		//sb.append("<br/><br/><br/>");
		//String remarks_en = "<p th:if=\"\\${INCLUDE_REMARKS} == 'true'\" th:text=\"\\${REMARKS_en}\">REMARKS</p>";
		//sb.append(remarks_en.replace("\\", ""));
		//sb.append("<br/><br/>");
		//String remarks_de = "<p th:if=\"\\${INCLUDE_REMARKS} == 'true'\" th:text=\"\\${REMARKS_de}\">REMARKS</p>";
		//sb.append(remarks_de.replace("\\", ""));
		sb.append("</div>");
		sb.append("</body>");
		sb.append("</html>");
		String htmlMockTemplate = sb.toString();
		this.validateTemplate(htmlMockTemplate);
		return htmlMockTemplate;
	}
	
	public String getTemplateVariables(final Set<String> templateVariables) {
	    	StringBuilder variables = new StringBuilder();
	    	templateVariables.forEach(templateVariable -> {
	    		variables.append(templateVariable).append(",");
	    	});
		return variables.toString();
	}
	
	public void validateTemplate(final String template) {
		this.createDynamicTemplateForDirectSendMail(template);
		this.templateEngine.clearTemplateCache();
		Context ctx = new Context();
		try {
			this.templateEngine.process("common_template", ctx);
		} catch (Exception e) {
			if (e instanceof org.thymeleaf.exceptions.TemplateInputException) {
				throw new XMObjectNotFoundException("Format Exception", "PARSE_ERR001");
			}
			LOG.info("Error/Exception occurred while template processing {}", e.getCause());
		}
		this.templateEngine.clearTemplateCache();
	}
}
