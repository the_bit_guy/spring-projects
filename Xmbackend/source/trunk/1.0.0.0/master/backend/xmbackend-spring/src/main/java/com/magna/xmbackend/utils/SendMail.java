/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.utils;

import java.util.Properties;

import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.magna.xmbackend.entities.PropertyConfigTbl;
import com.magna.xmbackend.jpa.dao.PropertyConfigJpaDao;

/**
 *
 * @author dhana
 */
@Component
public class SendMail {

    @Autowired
    private PropertyConfigJpaDao propertyConfigJpaDao;

    private static final Logger LOG
            = LoggerFactory.getLogger(SendMail.class);

    private String getSMTPValue(String key) {
        PropertyConfigTbl propertyConfigTbl
                = this.propertyConfigJpaDao
                        .findByCategoryAndProperty("SMTP", key);
        String value = propertyConfigTbl.getValue();
        return value;
    }

    public void sendEmail(String toMail, String ccMail,
            String subject, String txtMsg) throws Exception {
        LOG.info(">>> sendEmail >>>");
        //SMTP_EMAIL_ID
        final String fromMailId = this.getSMTPValue("SMTP_EMAIL_ID");
        //SMTP_PASSWORD
        final String password = this.getSMTPValue("SMTP_PASSWORD");
        //SMTP_SERVER_NAME
        String host = this.getSMTPValue("SMTP_SERVER_NAME");

        /*Properties props = new Properties();
        props.put("mail.smtp.auth", "false");
        //props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", host);*/
        
        Properties props = new Properties();
        props.put("mail.smtp.host", host);
        //SMTP_PORT_NUMBER
        props.put("mail.smtp.port", this.getSMTPValue("SMTP_PORT_NUMBER"));
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        //props.put("mail.smtp.ssl.trust", host);

        // Get the Session object.
        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(fromMailId, password);
            }
        });
        //Session session = Session.getDefaultInstance(props, null);
        //Session session = Session.getInstance(props);

		try {
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(fromMailId));
			if (toMail != null) {
				String[] emails = toMail.split(";");
				Address[] to = new Address[emails.length];
				int counter = 0;
				for(String email : emails) {
					to[counter] = new InternetAddress(email.trim());
					counter++;
				}
				message.setRecipients(Message.RecipientType.TO, to);
			}
			
			if(ccMail != null) {
				String[] emails = ccMail.split(";");
				Address[] cc = new Address[emails.length];
				int counter = 0;
				for(String email : emails) {
					cc[counter] = new InternetAddress(email.trim());
					counter++;
				}
				message.setRecipients(Message.RecipientType.CC, cc);
			}
			message.setSubject(subject);
			// message.setContent(txtMsg, "text/html");
			message.setContent(txtMsg, "text/html; charset=utf-8");
			// Send message
			Transport transport = session.getTransport("smtp");
			transport.connect();
			Transport.send(message);

			LOG.debug("Sent message successfully....");

		} catch (MessagingException e) {
            throw new RuntimeException(e);
        }

        LOG.info("<<< sendEmail <<<");

    }

}
