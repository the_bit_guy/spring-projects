package com.magna.xmbackend.controller;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.magna.xmbackend.mgr.ValidationMgr;
import com.magna.xmbackend.validation.vo.ValidationResponse;
import com.magna.xmbackend.vo.permission.ValidationRequest;

@RestController
@RequestMapping(value = "/validation")
public class ValidationController {

    private static final Logger LOG
            = LoggerFactory.getLogger(ValidationController.class);

    @Autowired
    private ValidationMgr validationMgr;
    
    

    /**
     *
     * @param httpServletRequest
     * @param validationRequest
     * @return Boolean
     */
    @RequestMapping(value = "/accessAllowed",
            method = RequestMethod.POST,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<Boolean> accessAllowed(
            HttpServletRequest httpServletRequest,
            @RequestBody ValidationRequest validationRequest) {
        LOG.info("> accessAllowed");
        final String userName = (String) httpServletRequest.getAttribute("userName");
        LOG.info("userName={}", userName);
        validationRequest.setUserName(userName);
        boolean isAccessAllowed = this.validationMgr.isObjectAccessAllowed(validationRequest);
        LOG.info("< accessAllowed");
        return new ResponseEntity<>(isAccessAllowed, HttpStatus.OK);
    }

    /**
     *
     * @param httpServletRequest
     * @return Boolean
     */
    @RequestMapping(value = "/cache",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<Boolean> clearCache(
            HttpServletRequest httpServletRequest) {
        LOG.info("> clearCache");
        boolean isClearCache = this.validationMgr.clearCache();
        LOG.info("< clearCache");
        return new ResponseEntity<>(isClearCache, HttpStatus.OK);
    }
    
    
	@RequestMapping(value = "/checkHotlinePermissions",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
	public @ResponseBody final ResponseEntity<Boolean> checkPermissions(HttpServletRequest httpServletRequest){
		LOG.info("> checkPermissions");
		String userName = (String)httpServletRequest.getAttribute("userName");
		String adminAreaId = (String)httpServletRequest.getAttribute("adminAreaId");
		ValidationResponse vr = this.validationMgr.checkHotlinePermissions(userName, adminAreaId);
		LOG.info("< checkPermissions");
		return new ResponseEntity<>(vr.isPermissionCheck(), HttpStatus.OK);
	}
}
