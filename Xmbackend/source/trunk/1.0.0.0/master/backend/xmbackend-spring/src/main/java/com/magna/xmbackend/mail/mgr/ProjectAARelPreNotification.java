/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.mail.mgr;

import com.magna.xmbackend.entities.AdminAreaProjectRelTbl;

/**
 *
 * @author dhana
 */
public interface ProjectAARelPreNotification {

    void postToQueue(AdminAreaProjectRelTbl adminAreaProjectRelTbl, String event);
}
