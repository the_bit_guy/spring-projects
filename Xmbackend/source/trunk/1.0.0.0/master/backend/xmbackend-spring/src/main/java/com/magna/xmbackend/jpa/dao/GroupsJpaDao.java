/**
 * 
 */
package com.magna.xmbackend.jpa.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.repository.CrudRepository;

import com.magna.xmbackend.entities.GroupsTbl;

/**
 * @author Bhabadyuti Bal
 *
 */
@Transactional
public interface GroupsJpaDao extends CrudRepository<GroupsTbl, String>{

	GroupsTbl findByNameIgnoreCaseAndGroupType(String name, String groupType);
	
	List<GroupsTbl> findByGroupType(String groupType);
}
