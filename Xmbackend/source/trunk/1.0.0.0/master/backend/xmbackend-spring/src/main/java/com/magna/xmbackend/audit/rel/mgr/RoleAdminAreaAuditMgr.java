/**
 * 
 */
package com.magna.xmbackend.audit.rel.mgr;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.magna.xmbackend.entities.RoleAdminAreaRelTbl;
import com.magna.xmbackend.vo.rel.RoleAdminAreaRelRequest;
import com.magna.xmbackend.vo.rel.RoleAdminAreaRelResponse;

/**
 * @author Bhabadyuti Bal
 *
 */
public interface RoleAdminAreaAuditMgr {

	void roleAdminAreaCreateSuccessAudit(RoleAdminAreaRelResponse roleAdminAreaRelResponse,
			List<RoleAdminAreaRelRequest> roleAdminAreaRelRequestList, HttpServletRequest httpServletRequest);

	void roleAdminAreaMultiDeleteSuccess(RoleAdminAreaRelTbl roleAdminAreaRelTbl,
			HttpServletRequest httpServletRequest);

}
