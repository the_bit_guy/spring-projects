/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.jpa.dao;

import com.magna.xmbackend.entities.BaseAppTranslationTbl;
import com.magna.xmbackend.entities.BaseApplicationsTbl;
import com.magna.xmbackend.entities.LanguagesTbl;
import javax.transaction.Transactional;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author dana
 */
@Transactional
public interface BaseAppTranslationJpaDao extends CrudRepository<BaseAppTranslationTbl, String> {
//    @Query("SELECT bat.baseAppTranslationId FROM BaseAppTranslationTbl bat WHERE bat.baseApplicationsTbl = :baseApplicationsTbl AND bat.languagesTbl = :languagesTbl")
//    String getIdFromBaseAppTranslationTbl(@Param("baseApplicationsTbl") BaseApplicationsTbl baseApplicationsTbl, @Param("languagesTbl") LanguagesTbl languagesTbl);

    BaseAppTranslationTbl findByBaseApplicationIdAndLanguageCode(BaseApplicationsTbl baseApplicationId, LanguagesTbl languageCode);
}
