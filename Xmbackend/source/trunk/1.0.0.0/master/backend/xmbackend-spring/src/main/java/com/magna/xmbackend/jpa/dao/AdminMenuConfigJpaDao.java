/**
 * 
 */
package com.magna.xmbackend.jpa.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.repository.CrudRepository;

import com.magna.xmbackend.entities.AdminMenuConfigTbl;

/**
 * @author Bhabadyuti Bal
 *
 */
@Transactional
public interface AdminMenuConfigJpaDao extends CrudRepository<AdminMenuConfigTbl, String>{

	List<AdminMenuConfigTbl> findByObjectType(String objectType);
	
	AdminMenuConfigTbl findByObjectTypeAndObjectId(String objectType, String objectId);
	
	List<AdminMenuConfigTbl> findByObjectTypeIn(List<String> objectTypes);
	
	List<AdminMenuConfigTbl> findByObjectId(String objectId);
}
