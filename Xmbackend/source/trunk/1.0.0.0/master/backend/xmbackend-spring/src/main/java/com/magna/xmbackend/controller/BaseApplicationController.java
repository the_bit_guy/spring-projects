package com.magna.xmbackend.controller;

import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.HandlerMapping;

import com.magna.xmbackend.audit.mgr.BaseAppAuditMgr;
import com.magna.xmbackend.entities.BaseApplicationsTbl;
import com.magna.xmbackend.exception.CannotCreateObjectException;
import com.magna.xmbackend.exception.XMObjectNotFoundException;
import com.magna.xmbackend.mgr.BaseApplicationMgr;
import com.magna.xmbackend.utils.Validator;
import com.magna.xmbackend.vo.baseApplication.BaseApplicationCustomResponse;
import com.magna.xmbackend.vo.baseApplication.BaseApplicationRequest;
import com.magna.xmbackend.vo.baseApplication.BaseApplicationResponse;
import com.magna.xmbackend.vo.permission.ValidationRequest;

@RestController
@RequestMapping(value = "/baseApplication")
public class BaseApplicationController {

    private static final Logger LOG
            = LoggerFactory.getLogger(BaseApplicationController.class);

    @Autowired
    private BaseApplicationMgr baseApplicationMgr;

    @Autowired
    private Validator validator;

    @Autowired
    BaseAppAuditMgr baseAppAuditMgr;

    /**
     *
     * @param httpServletRequest
     * @return BaseApplicationResponse
     */
    @RequestMapping(value = "/findAll",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<BaseApplicationResponse> findAll(
            HttpServletRequest httpServletRequest) {
        LOG.info("> findAll");
        final ValidationRequest validationRequest
                = validator.formObjectValidationRequest(httpServletRequest, "BASEAPPLICATION");
        final BaseApplicationResponse baseApplicationResponse
                = baseApplicationMgr.findAll(validationRequest);
        LOG.info("< findAll");
        return new ResponseEntity<>(baseApplicationResponse, HttpStatus.OK);
    }

    /**
     *
     * @param httpServletRequest
     * @param id
     * @return BaseApplicationsTbl
     */
    @RequestMapping(value = "/find/{id}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<BaseApplicationsTbl> findById(
            HttpServletRequest httpServletRequest, @PathVariable String id) {
        LOG.info("> findById");
        final BaseApplicationsTbl baseApplicationsTbl
                = baseApplicationMgr.findById(id);
        LOG.info("< findById");
        return new ResponseEntity<>(baseApplicationsTbl, HttpStatus.OK);
    }

    
    
    @RequestMapping(value = "/findByName/{name}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<BaseApplicationsTbl> findByName(
            HttpServletRequest httpServletRequest, @PathVariable String name) {
        LOG.info("> findByName");
		final BaseApplicationsTbl baseApplicationsTbl = baseApplicationMgr.findByName(name);
        LOG.info("< findByName");
        return new ResponseEntity<>(baseApplicationsTbl, HttpStatus.OK);
    }
    
    
    @RequestMapping(value = "/caxStartBatchFindByName/**",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<BaseApplicationCustomResponse> caxStartBatchFindByName(
            HttpServletRequest httpServletRequest) {
        LOG.info("> findByName");
        String pattern = (String)httpServletRequest.getAttribute(HandlerMapping.BEST_MATCHING_PATTERN_ATTRIBUTE);
        String baseAppName = new AntPathMatcher().extractPathWithinPattern(pattern, 
          httpServletRequest.getServletPath());
		final BaseApplicationsTbl baseApplicationsTbl = baseApplicationMgr.findByName(baseAppName);
		BaseApplicationCustomResponse customResponse = new BaseApplicationCustomResponse();
		customResponse.setBaseApplicationId(baseApplicationsTbl.getBaseApplicationId());
		customResponse.setName(baseApplicationsTbl.getName());
		customResponse.setStatus(baseApplicationsTbl.getStatus());
		
        LOG.info("< findByName");
        return new ResponseEntity<>(customResponse, HttpStatus.OK);
    }
    
    /**
     *
     * @param httpServletRequest
     * @param baseApplicationRequest
     * @return BaseApplicationsTbl
     */
    @RequestMapping(value = "/save",
            method = RequestMethod.POST,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<BaseApplicationsTbl> save(
            HttpServletRequest httpServletRequest,
            @RequestBody BaseApplicationRequest baseApplicationRequest) {
        BaseApplicationsTbl baseApplicationsTbl = null;
        LOG.info("> save");
        try {
            baseApplicationsTbl
                    = baseApplicationMgr.create(baseApplicationRequest);
            this.baseAppAuditMgr.baseAppCreateSuccessAuditor(baseApplicationsTbl,
                    httpServletRequest);
        } catch (Exception ex) {
            final String name = baseApplicationRequest.getName();
            LOG.error("Exception / Error occured in creating "
                    + "Base App with name {}", name);
            this.baseAppAuditMgr.baseAppCreateFailureAuditor(
                    baseApplicationRequest, httpServletRequest);
            throw ex;
        }
        LOG.info("< save");
        return new ResponseEntity<>(baseApplicationsTbl, HttpStatus.ACCEPTED);
    }
    
    /**
     * 
     * @param httpServletRequest
     * @param baseApplicationRequest
     * @return
     */
    @RequestMapping(value = "/caxstartbatch/save", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_FORM_URLENCODED_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE,
					MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_FORM_URLENCODED_VALUE })
	public @ResponseBody final ResponseEntity<Boolean> saveFromCaxStartBatch(HttpServletRequest httpServletRequest,
			@Valid @RequestBody BaseApplicationRequest baseApplicationRequest) {
		LOG.info("> save");
		BaseApplicationsTbl baseApplicationsTbl = null;
		try {
			baseApplicationsTbl = this.baseApplicationMgr.create(baseApplicationRequest);
		} catch (Exception e) {
			if (e instanceof CannotCreateObjectException) {
				this.baseAppAuditMgr.baseAppCreateFailureBatchAuditor(baseApplicationRequest, httpServletRequest);
			}
			throw e;
		}
		LOG.info("< save");
		if (baseApplicationsTbl != null) {
			this.baseAppAuditMgr.baseAppCreateSuccessAuditor(baseApplicationsTbl, httpServletRequest);
			return new ResponseEntity<>(Boolean.TRUE, HttpStatus.ACCEPTED);
		} else {
			this.baseAppAuditMgr.baseAppCreateFailureBatchAuditor(baseApplicationRequest, httpServletRequest);
			return new ResponseEntity<>(Boolean.FALSE, HttpStatus.ACCEPTED);
		}
	}
    
    
    /**
     *
     * @param httpServletRequest
     * @param baseApplicationRequest
     * @return BaseApplicationsTbl
     */
    @RequestMapping(value = "/update",
            method = RequestMethod.POST,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<BaseApplicationsTbl> update(
            HttpServletRequest httpServletRequest,
            @RequestBody BaseApplicationRequest baseApplicationRequest) {
        BaseApplicationsTbl baseApplicationsTbl = null;
        LOG.info("> update");
        try {
            baseApplicationsTbl
                    = baseApplicationMgr.update(baseApplicationRequest);
            this.baseAppAuditMgr.baseAppUpdateSuccessAuditor(baseApplicationsTbl, httpServletRequest);
        } catch (Exception ex) {
            final String id = baseApplicationRequest.getId();
            LOG.error("Exception / Error occured in updating "
                    + "Base App with id {}", id);
            this.baseAppAuditMgr.baseAppUpdateFailureAuditor(baseApplicationRequest,
                    httpServletRequest);
            throw ex;
        }
        LOG.info("< update");
        return new ResponseEntity<>(baseApplicationsTbl, HttpStatus.ACCEPTED);
    }
    
    @RequestMapping(value = "/caxstartbatch/update", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_FORM_URLENCODED_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE,
					MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_FORM_URLENCODED_VALUE })
	public @ResponseBody final ResponseEntity<Boolean> updateFromCaxStartBatch(HttpServletRequest httpServletRequest,
			@RequestBody BaseApplicationRequest baseApplicationRequest) {
		LOG.info("> update");
		final BaseApplicationsTbl baseAppTbl = this.baseApplicationMgr.updateForBatch(baseApplicationRequest);
		LOG.info("< update");
		if (baseAppTbl != null) {
			this.baseAppAuditMgr.baseAppStatusUpdateSuccessAuditor(baseAppTbl.getName(), baseAppTbl.getStatus(), httpServletRequest);
			return new ResponseEntity<>(Boolean.TRUE, HttpStatus.ACCEPTED);
		} else {
			this.baseAppAuditMgr.baseAppUpdateFailureAuditor(baseApplicationRequest,
                    httpServletRequest);
			return new ResponseEntity<>(Boolean.FALSE, HttpStatus.ACCEPTED);
		}
	}
    
    

    /**
     *
     * @param httpServletRequest
     * @param status
     * @param id
     * @return Boolean
     */
    @RequestMapping(value = "/updateStatus/{status}/{id}",
            method = RequestMethod.POST,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<Boolean> update(
            HttpServletRequest httpServletRequest,
            @PathVariable String status, @PathVariable String id) {
        boolean updateStatusById = false;
        BaseApplicationsTbl baseApplicationsTbl = null;
        LOG.info("> updateStatus");
        try {
            baseApplicationsTbl = baseApplicationMgr.findById(id);
            if (null != baseApplicationsTbl) {
                updateStatusById = baseApplicationMgr.updateStatusById(status, id);
                this.baseAppAuditMgr.baseAppUpdateStatusAuditor(status, id,
                        httpServletRequest, updateStatusById, baseApplicationsTbl);
            } else {
                throw new XMObjectNotFoundException("Object not found",
                        "ERR0003");
            }
        } catch (Exception ex) {
            LOG.info("Exception / Error in updating base app status for id {}",
                    id);
            this.baseAppAuditMgr.baseAppUpdateStatusAuditor(status, id,
                    httpServletRequest, updateStatusById, baseApplicationsTbl);
            throw ex;
        }
        LOG.info("< updateStatus");
        return new ResponseEntity<>(updateStatusById, HttpStatus.ACCEPTED);
    }

    
    
	@RequestMapping(value = "/multiStatusUpdate",
			method = RequestMethod.POST,
			consumes = {MediaType.APPLICATION_JSON_VALUE,
				MediaType.APPLICATION_XML_VALUE,
				MediaType.APPLICATION_FORM_URLENCODED_VALUE},
			produces = {MediaType.APPLICATION_JSON_VALUE,
				MediaType.APPLICATION_XML_VALUE,
				MediaType.APPLICATION_FORM_URLENCODED_VALUE}
	)
	public @ResponseBody
	final ResponseEntity<BaseApplicationResponse> multiUpdate(
			HttpServletRequest httpServletRequest,
			@RequestBody List<BaseApplicationRequest> baseAppRequests) {
		LOG.info("> multiUpdate");
		BaseApplicationResponse baseApplicationResponse = this.baseApplicationMgr.multiUpdate(baseAppRequests, httpServletRequest);
		LOG.info("< multiUpdate");
		return new ResponseEntity<>(baseApplicationResponse, HttpStatus.ACCEPTED);
	}
    
    
    /**
     *
     * @param httpServletRequest
     * @param id
     * @return Boolean
     */
    @RequestMapping(value = "/delete/{id}",
            method = RequestMethod.DELETE,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<Boolean> deleteById(
            HttpServletRequest httpServletRequest,
            @PathVariable String id) {
        boolean isDeleted = false;
        BaseApplicationsTbl baseApplicationsTbl = null;
        LOG.info("> deleteIconById >>{} and {}", id);
        try {
            baseApplicationsTbl = baseApplicationMgr.findById(id);
            isDeleted = baseApplicationMgr.deleteById(id);
            this.baseAppAuditMgr.baseAppDeleteStatusAuditor(id,
                    httpServletRequest, isDeleted, baseApplicationsTbl);
        } catch (Exception ex) {
            LOG.error("Exception / error occured in deleting base app={}", id);
            this.baseAppAuditMgr.baseAppDeleteStatusAuditor(id,
                    httpServletRequest, isDeleted, baseApplicationsTbl);
            throw ex;
        }
        final ResponseEntity<Boolean> responseEntity
                = new ResponseEntity<>(isDeleted, HttpStatus.OK);
        LOG.info("< deleteById");
        return responseEntity;
    }
    
    
	@RequestMapping(value = "/caxstartbatch/delete", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_FORM_URLENCODED_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE,
					MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_FORM_URLENCODED_VALUE })
	public @ResponseBody final ResponseEntity<Boolean> deleteByName(HttpServletRequest httpServletRequest,
			@RequestBody String name) {
		LOG.info("> delete");
		BaseApplicationsTbl baseApplicationsTbl = this.baseApplicationMgr.findByName(name);
		boolean isDeleted = false;
		isDeleted = this.baseApplicationMgr.deleteById(baseApplicationsTbl.getBaseApplicationId());
		if (isDeleted) {
			this.baseAppAuditMgr.baseAppDeleteStatusAuditor(baseApplicationsTbl.getBaseApplicationId(), httpServletRequest, isDeleted, baseApplicationsTbl);
		} else {
			this.baseAppAuditMgr.baseAppDeleteStatusAuditor(baseApplicationsTbl.getBaseApplicationId(),
	                httpServletRequest, isDeleted, baseApplicationsTbl);
		}
		LOG.info("< delete");
		return new ResponseEntity<>(isDeleted, HttpStatus.ACCEPTED);
	}

    /**
     *
     * @param httpServletRequest
     * @param ids
     * @return BaseApplicationResponse
     */
    @RequestMapping(value = "/multiDelete",
            method = RequestMethod.DELETE,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<BaseApplicationResponse> multiDelete(
            HttpServletRequest httpServletRequest,
            @RequestBody Set<String> ids) {
        LOG.info("> multiDelete >>{} and {}", ids);
        final BaseApplicationResponse baseApplicationResponse
                = baseApplicationMgr.multiDelete(ids, httpServletRequest);
//        this.baseAppAuditMgr.baseAppMultiDeleteAuditor(baseApplicationResponse, ids, httpServletRequest);
        final ResponseEntity<BaseApplicationResponse> responseEntity
                = new ResponseEntity<>(baseApplicationResponse, HttpStatus.OK);
        LOG.info("< multiDelete");
        return responseEntity;
    }
}
