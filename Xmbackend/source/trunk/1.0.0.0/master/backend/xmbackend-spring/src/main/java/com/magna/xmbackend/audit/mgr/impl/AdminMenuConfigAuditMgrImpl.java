/**
 * 
 */
package com.magna.xmbackend.audit.mgr.impl;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.magna.xmbackend.audit.mgr.AdminMenuConfigAuditMgr;
import com.magna.xmbackend.audit.mgr.utils.AdminHistoryRelsUtil;
import com.magna.xmbackend.entities.AdminHistoryRelationsTbl;
import com.magna.xmbackend.entities.AdminMenuConfigTbl;
import com.magna.xmbackend.entities.ProjectApplicationsTbl;
import com.magna.xmbackend.entities.UserApplicationsTbl;
import com.magna.xmbackend.entities.UsersTbl;
import com.magna.xmbackend.jpa.dao.ProjectApplicationJpaDao;
import com.magna.xmbackend.jpa.dao.UserApplicationJpaDao;
import com.magna.xmbackend.jpa.dao.UserJpaDao;
import com.magna.xmbackend.jpa.rel.dao.AdminHistoryRelationJpaDao;
import com.magna.xmbackend.vo.adminMenu.AdminMenuConfigRequest;
import com.magna.xmbackend.vo.adminMenu.AdminMenuConfigResponse;
import com.magna.xmbackend.vo.enums.AdminMenuConfig;

/**
 * @author Bhabadyuti Bal
 *
 */
@Component
public class AdminMenuConfigAuditMgrImpl implements AdminMenuConfigAuditMgr {
	
	private static final Logger LOG = LoggerFactory.getLogger(AdminMenuConfigAuditMgrImpl.class);
	
	final String OBJECT_NAME = "AdminAreasTbl";
	final String RESULT_SUCCESS = "Success";
	final String RESULT_FAILURE = "Failure";
	
	@Autowired
	private AdminHistoryRelationJpaDao adminHistoryRelationJpaDao;
	@Autowired
	private AdminHistoryRelsUtil adminHistoryRelsUtil;
    @Autowired
    private UserJpaDao userJpaDao;
    @Autowired
    private UserApplicationJpaDao userApplicationJpaDao;
    @Autowired
    private ProjectApplicationJpaDao projectApplicationJpaDao;
    

	@Override
	public void amcCreateSuccessAudit(HttpServletRequest httpServletRequest, List<AdminMenuConfigRequest> adminMenuConfigRequestList,AdminMenuConfigResponse response) {
		LOG.info(">>> amcCreateSuccessAudit");


		Iterable<AdminMenuConfigTbl> adminMenuConfigTbls = response.getAdminMenuConfigTbls();
		List<Map<String, String>> statusMap = response.getStatusMaps();

		if (statusMap.isEmpty()) {
			// All success
			this.createSuccessAudit(adminMenuConfigTbls, httpServletRequest);

		} else {
			//Partial success case
			//Iterate the status map to log the failed ones
			this.createFailureAudit(statusMap, adminMenuConfigRequestList, httpServletRequest);

			//Iterate the userProjectRelTbls for success ones
			this.createSuccessAudit(adminMenuConfigTbls, httpServletRequest);
		}
		LOG.info("<<< amcCreateSuccessAudit");
		
	}


	private void createFailureAudit(List<Map<String, String>> statusMap,
			List<AdminMenuConfigRequest> adminMenuConfigRequestList, HttpServletRequest httpServletRequest) {
		
		statusMap.forEach(map -> {
			adminMenuConfigRequestList.forEach(aamcr -> {
				String message = map.get("en");
				String objectId = aamcr.getObjectId();
				String objectType = aamcr.getObjectType().name();
				if(objectType.equals(AdminMenuConfig.USER.name())){
					UsersTbl usersTbl = this.userJpaDao.findOne(objectId);
					String username = usersTbl.getUsername();
					AdminHistoryRelationsTbl adminHistoryRelationTbl = this.adminHistoryRelsUtil
							.makeAdminHistoryRelationTbl(httpServletRequest, "AdminMenuConfig", username,
									AdminMenuConfig.USER.name(), null, null,
									null, null, message, "User Menu Config Create",
									RESULT_FAILURE);
					this.adminHistoryRelationJpaDao.save(adminHistoryRelationTbl);
				} else if(objectType.equals(AdminMenuConfig.USERAPPLICATION.name())){
					UserApplicationsTbl userApplicationsTbl = this.userApplicationJpaDao.findOne(objectId);
					String userAppName = userApplicationsTbl.getName();
					AdminHistoryRelationsTbl adminHistoryRelationTbl = this.adminHistoryRelsUtil
							.makeAdminHistoryRelationTbl(httpServletRequest, "AdminMenuConfig", userAppName,
									AdminMenuConfig.USERAPPLICATION.name(), null, null,
									null, null, message, "UserApp Menu Config Create",
									RESULT_FAILURE);
					this.adminHistoryRelationJpaDao.save(adminHistoryRelationTbl);
				} else if(objectType.equals(AdminMenuConfig.PROJECTAPPLICATION.name())){
					ProjectApplicationsTbl projectApplicationsTbl = this.projectApplicationJpaDao.findOne(objectId);
					String projAppName = projectApplicationsTbl.getName();
					AdminHistoryRelationsTbl adminHistoryRelationTbl = this.adminHistoryRelsUtil
							.makeAdminHistoryRelationTbl(httpServletRequest, "AdminMenuConfig", projAppName,
									AdminMenuConfig.PROJECTAPPLICATION.name(), null, null,
									null, null, message, "ProjectApp Menu Config Create",
									RESULT_FAILURE);
					this.adminHistoryRelationJpaDao.save(adminHistoryRelationTbl);
				}
				
			});
		});
	}


	private void createSuccessAudit(Iterable<AdminMenuConfigTbl> adminMenuConfigTbls,
			HttpServletRequest httpServletRequest) {
		adminMenuConfigTbls.forEach(adminMenuConfigTbl -> {
			String objectType = adminMenuConfigTbl.getObjectType();
			String objectId = adminMenuConfigTbl.getObjectId();
			if(objectType.equals(AdminMenuConfig.USER.name())){
				UsersTbl usersTbl = this.userJpaDao.findOne(objectId);
				String username = usersTbl.getUsername();
				AdminHistoryRelationsTbl adminHistoryRelationTbl = this.adminHistoryRelsUtil
						.makeAdminHistoryRelationTbl(httpServletRequest, "AdminMenuConfig", username,
								AdminMenuConfig.USER.name(), null, null,
								null, null, null, "User Menu Config create",
								RESULT_SUCCESS);
				this.adminHistoryRelationJpaDao.save(adminHistoryRelationTbl);
			} else if(objectType.equals(AdminMenuConfig.USERAPPLICATION.name())){
				UserApplicationsTbl userApplicationsTbl = this.userApplicationJpaDao.findOne(objectId);
				String userAppName = userApplicationsTbl.getName();
				AdminHistoryRelationsTbl adminHistoryRelationTbl = this.adminHistoryRelsUtil
						.makeAdminHistoryRelationTbl(httpServletRequest, "AdminMenuConfig", userAppName,
								AdminMenuConfig.USERAPPLICATION.name(), null, null,
								null, null, null, "UserApp Menu Config create",
								RESULT_SUCCESS);
				this.adminHistoryRelationJpaDao.save(adminHistoryRelationTbl);
			} else if(objectType.equals(AdminMenuConfig.PROJECTAPPLICATION.name())){
				ProjectApplicationsTbl projectApplicationsTbl = this.projectApplicationJpaDao.findOne(objectId);
				String projAppName = projectApplicationsTbl.getName();
				AdminHistoryRelationsTbl adminHistoryRelationTbl = this.adminHistoryRelsUtil
						.makeAdminHistoryRelationTbl(httpServletRequest, "AdminMenuConfig", projAppName,
								AdminMenuConfig.PROJECTAPPLICATION.name(), null, null,
								null, null, null, "ProjectApp Menu Config create",
								RESULT_SUCCESS);
				this.adminHistoryRelationJpaDao.save(adminHistoryRelationTbl);
			}
			
			
		});
		
	}





	@Override
	public void amcUpdateFailureAudit(HttpServletRequest hsr, AdminMenuConfigRequest amcr, String errMsg) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void amcMultiDeleteAuditor(AdminMenuConfigTbl adminMenuConfigTbl,
			HttpServletRequest httpServletRequest) {
		

		String objectType = adminMenuConfigTbl.getObjectType();
		String objectId = adminMenuConfigTbl.getObjectId();
		if(objectType.equals(AdminMenuConfig.USER.name())){
			UsersTbl usersTbl = this.userJpaDao.findOne(objectId);
			String username = usersTbl.getUsername();
			AdminHistoryRelationsTbl adminHistoryRelationTbl = this.adminHistoryRelsUtil
					.makeAdminHistoryRelationTbl(httpServletRequest, "AdminMenuConfig", username,
							AdminMenuConfig.USER.name(), null, null,
							null, null, null, "User Menu Config delete",
							RESULT_SUCCESS);
			this.adminHistoryRelationJpaDao.save(adminHistoryRelationTbl);
		} else if(objectType.equals(AdminMenuConfig.USERAPPLICATION.name())){
			UserApplicationsTbl userApplicationsTbl = this.userApplicationJpaDao.findOne(objectId);
			String userAppName = userApplicationsTbl.getName();
			AdminHistoryRelationsTbl adminHistoryRelationTbl = this.adminHistoryRelsUtil
					.makeAdminHistoryRelationTbl(httpServletRequest, "AdminMenuConfig", userAppName,
							AdminMenuConfig.USERAPPLICATION.name(), null, null,
							null, null, null, "UserApp Menu Config delete",
							RESULT_SUCCESS);
			this.adminHistoryRelationJpaDao.save(adminHistoryRelationTbl);
		} else if(objectType.equals(AdminMenuConfig.PROJECTAPPLICATION.name())){
			ProjectApplicationsTbl projectApplicationsTbl = this.projectApplicationJpaDao.findOne(objectId);
			String projAppName = projectApplicationsTbl.getName();
			AdminHistoryRelationsTbl adminHistoryRelationTbl = this.adminHistoryRelsUtil
					.makeAdminHistoryRelationTbl(httpServletRequest, "AdminMenuConfig", projAppName,
							AdminMenuConfig.PROJECTAPPLICATION.name(), null, null,
							null, null, null, "ProjectApp Menu Config delete",
							RESULT_SUCCESS);
			this.adminHistoryRelationJpaDao.save(adminHistoryRelationTbl);
		}
		
		
	
		
	}


}
