package com.magna.xmbackend.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.magna.xmbackend.mgr.AdminHistoryMgr;
import com.magna.xmbackend.vo.adminHistory.AdminHistoryRequest;
import com.magna.xmbackend.vo.adminHistory.AdminHistoryResponse;

/**
 * The Class AdminHistoryController.
 * 
 * @author shashwat.anand
 */
@RestController
@RequestMapping(value = "/adminHistory")
public class AdminHistoryController {
	
	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(AdminHistoryController.class);

	/** The admin history mgr. */
	@Autowired
	private AdminHistoryMgr adminHistoryMgr;

	/**
	 * Find admin base obj history.
	 *
	 * @param httpServletRequest the http servlet request
	 * @param adminHistoryRequest the admin history request
	 * @return the response entity
	 */
	@RequestMapping(value = "/findAdminBaseHistory", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_FORM_URLENCODED_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE,
					MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_FORM_URLENCODED_VALUE })
	public @ResponseBody final ResponseEntity<AdminHistoryResponse> findAdminBaseObjHistory(
			HttpServletRequest httpServletRequest, @RequestBody AdminHistoryRequest adminHistoryRequest) {
		LOG.info("> findUserStatus");
		List<Map<String, Object>> resultSet = this.adminHistoryMgr.findAdminBaseObjHistory(adminHistoryRequest);
		final AdminHistoryResponse response = new AdminHistoryResponse(resultSet);
		LOG.info("< findUserStatus");
		return new ResponseEntity<>(response, HttpStatus.ACCEPTED);
	}

	/**
	 * Find admin rel history.
	 *
	 * @param httpServletRequest the http servlet request
	 * @param adminHistoryRequest the admin history request
	 * @return the response entity
	 */
	@RequestMapping(value = "/findAdminRelHistory", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_FORM_URLENCODED_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE,
					MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_FORM_URLENCODED_VALUE })
	public @ResponseBody final ResponseEntity<AdminHistoryResponse> findAdminRelHistory(
			HttpServletRequest httpServletRequest, @RequestBody AdminHistoryRequest adminHistoryRequest) {
		LOG.info("> findUserStatus");
		List<Map<String, Object>> resultSet = this.adminHistoryMgr.findAdminRelHistory(adminHistoryRequest);
		final AdminHistoryResponse response = new AdminHistoryResponse(resultSet);
		LOG.info("< findUserStatus");
		return new ResponseEntity<>(response, HttpStatus.ACCEPTED);
	}
}
