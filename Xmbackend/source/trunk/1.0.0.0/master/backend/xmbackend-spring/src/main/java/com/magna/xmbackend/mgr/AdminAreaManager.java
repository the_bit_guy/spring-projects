package com.magna.xmbackend.mgr;

import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import com.magna.xmbackend.entities.AdminAreasTbl;
import com.magna.xmbackend.vo.adminArea.AdminAreaRequest;
import com.magna.xmbackend.vo.adminArea.AdminAreaResponse;
import com.magna.xmbackend.vo.permission.ValidationRequest;

// TODO: Auto-generated Javadoc
/**
 * The Interface AdminAreaManager.
 *
 * @author dhana
 */
public interface AdminAreaManager {

    /**
     * Find all.
     *
     * @param validationRequest the validation request
     * @return AdminAreaResponse
     */
    AdminAreaResponse findAll(final ValidationRequest validationRequest);

    /**
     * Find by id.
     *
     * @param id the id
     * @return AdminAreasTbl
     */
    AdminAreasTbl findById(String id);

    /**
     * Save.
     *
     * @param adminAreaRequest the admin area request
     * @return AdminAreasTbl
     */
    AdminAreasTbl save(final AdminAreaRequest adminAreaRequest);

    /**
     * Update.
     *
     * @param adminAreaRequest the admin area request
     * @return AdminAreasTbl
     */
    AdminAreasTbl update(final AdminAreaRequest adminAreaRequest);
    
    /**
     * Multi update.
     *
     * @param adminAreaRequests the admin area requests
     * @param servletRequest the servlet request
     * @return AdminAreasTbl
     */
    AdminAreaResponse multiUpdate(final List<AdminAreaRequest> adminAreaRequests, HttpServletRequest servletRequest);

    /**
     * Update status by id.
     *
     * @param status the status
     * @param id the id
     * @return boolean
     */
    boolean updateStatusById(final String status, final String id);

    /**
     * Delete by id.
     *
     * @param id the id
     * @return boolean
     */
    public boolean deleteById(final String id);

    /**
     * Multi delete.
     *
     * @param aaIds the aa ids
     * @param httpServletRequest the http servlet request
     * @return AdminAreaResponse
     */
    AdminAreaResponse multiDelete(final Set<String> aaIds,
            final HttpServletRequest httpServletRequest);

    /**
     * Find AA by project id.
     *
     * @param id the id
     * @return AdminAreaResponse
     */
    AdminAreaResponse findAAByProjectId(final String id);

    /**
     * Find AA by user app id.
     *
     * @param id the id
     * @return AdminAreaResponse
     */
    AdminAreaResponse findAAByUserAppId(final String id);

    /**
     * Find AA by user project app id.
     *
     * @param id the id
     * @return AdminAreaResponse
     */
    AdminAreaResponse findAAByUserProjectAppId(final String id);

    /**
     * Find AA by start app id.
     *
     * @param id the id
     * @return AdminAreaResponse
     */
    AdminAreaResponse findAAByStartAppId(final String id);

    /**
     * Find by name.
     *
     * @param name the name
     * @return AdminAreasTbl
     */
    AdminAreasTbl findByName(final String name);

    /**
     * Find AA by project id and site id.
     *
     * @param projectId the project id
     * @param tkt the tkt
     * @param siteId the site id
     * @param validationRequest the validation request
     * @return the admin area response
     */
    AdminAreaResponse findAAByProjectIdAndSiteId(final String projectId, final String tkt,
            final String siteId, final ValidationRequest validationRequest);

    /**
     * Find AA by start app id and site id.
     *
     * @param startAppId the start app id
     * @param siteId the site id
     * @return the admin area response
     */
    AdminAreaResponse findAAByStartAppIdAndSiteId(final String startAppId,
            final String siteId);

    /**
     * Find AA by user app id and site id.
     *
     * @param userAppId the user app id
     * @param siteId the site id
     * @return the admin area response
     */
    AdminAreaResponse findAAByUserAppIdAndSiteId(final String userAppId,
            final String siteId);

    /**
     * Find AA by project app id and site id.
     *
     * @param projectAppId the project app id
     * @param siteId the site id
     * @return the admin area response
     */
    AdminAreaResponse findAAByProjectAppIdAndSiteId(final String projectAppId,
            final String siteId);

    
	/**
	 * Find andmin areas by site id.
	 *
	 * @param siteId the site id
	 * @return the admin area response
	 */
	AdminAreaResponse findAndminAreasBySiteId(final String siteId, ValidationRequest validationRequest);

	/**
	 * Update for batch.
	 *
	 * @param adminAreaRequest the admin area request
	 * @return the admin areas tbl
	 */
	AdminAreasTbl updateForBatch(AdminAreaRequest adminAreaRequest, HttpServletRequest httpServletRequest);
}
