package com.magna.xmbackend.rel.controller;

import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotNull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.magna.xmbackend.audit.rel.mgr.UserUserAppRelAuditMgr;
import com.magna.xmbackend.entities.UserUserAppRelTbl;
import com.magna.xmbackend.jpa.rel.dao.UserUserAppRelJpaDao;
import com.magna.xmbackend.rel.mgr.UserUserAppRelMgr;
import com.magna.xmbackend.response.rel.useruserapp.UserUserAppRelResponseWrapper;
import com.magna.xmbackend.response.rel.useruserapp.UserUserAppRelation;
import com.magna.xmbackend.utils.Validator;
import com.magna.xmbackend.vo.permission.ValidationRequest;
import com.magna.xmbackend.vo.rel.UserUserAppRelBatchRequest;
import com.magna.xmbackend.vo.rel.UserUserAppRelBatchResponse;
import com.magna.xmbackend.vo.rel.UserUserAppRelRequest;
import com.magna.xmbackend.vo.rel.UserUserAppRelResponse;

/**
 *
 * @author dhana
 */
@RestController
@RequestMapping(value = "/userUserAppRel")
public class UserUserAppRelController {

    private static final Logger LOG = LoggerFactory.getLogger(UserUserAppRelController.class);

    @Autowired
    private UserUserAppRelMgr userUserAppRelMgr;
    
    @Autowired
    private Validator validator;
    @Autowired
    private UserUserAppRelAuditMgr userUserAppRelAuditMgr;
    @Autowired
    private UserUserAppRelJpaDao userUserAppRelJpaDao;
    /**
     *
     * @param httpServletRequest
     * @param userUserAppRelRequest
     * @return UserUserAppRelTbl
     */
    @RequestMapping(value = "/save",
            method = RequestMethod.POST,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<UserUserAppRelTbl> save(
            HttpServletRequest httpServletRequest,
            @RequestBody final UserUserAppRelRequest userUserAppRelRequest) {
        LOG.info("> save");
        final String userName = (String) httpServletRequest.getAttribute("userName");
        LOG.info("userName={}", userName);
        final UserUserAppRelTbl uuart
                = this.userUserAppRelMgr.create(userUserAppRelRequest, userName);
        LOG.info("< save");
        return new ResponseEntity<>(uuart, HttpStatus.ACCEPTED);
    }

    /**
     *
     * @param httpServletRequest
     * @param userUserAppRelBatchRequest
     * @return UserUserAppRelBatchResponse
     */
    @RequestMapping(value = "/multi/save",
            method = RequestMethod.POST,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<UserUserAppRelBatchResponse> batchSave(
            HttpServletRequest httpServletRequest,
            @RequestBody final UserUserAppRelBatchRequest userUserAppRelBatchRequest) {
        LOG.info("> batchSave");
        final String userName = (String) httpServletRequest.getAttribute("userName");
        LOG.info("userName={}", userName);
        UserUserAppRelBatchResponse usarbr = null;
		try {
			usarbr = userUserAppRelMgr.createBatch(userUserAppRelBatchRequest, userName);
			this.userUserAppRelAuditMgr.userUserAppMultiSaveAuditor(userUserAppRelBatchRequest, usarbr, httpServletRequest);
		} catch (Exception ex) {
			this.userUserAppRelAuditMgr.userUserMultiSaveFailureAuditor(httpServletRequest, ex);
		}
        LOG.info("< batchSave");
        return new ResponseEntity<>(usarbr, HttpStatus.ACCEPTED);
    }

    /**
     *
     * @param httpServletRequest
     * @param id
     * @return UserUserAppRelResponse
     */
    /*@RequestMapping(value = "/findUserUserAppRelsByUserId/{id}",
           method = RequestMethod.GET,
           consumes = {MediaType.APPLICATION_JSON_VALUE,
               MediaType.APPLICATION_XML_VALUE,
               MediaType.APPLICATION_FORM_URLENCODED_VALUE},
           produces = {MediaType.APPLICATION_JSON_VALUE,
               MediaType.APPLICATION_XML_VALUE,
               MediaType.APPLICATION_FORM_URLENCODED_VALUE}
   )*/
    public @ResponseBody
    final ResponseEntity<UserUserAppRelResponse> findUserUserAppRelsByUserId(
            HttpServletRequest httpServletRequest,
            @PathVariable String id) {
        LOG.info("> findUserUserAppRelsByUserId");
        final UserUserAppRelResponse userUserAppRelResponse = userUserAppRelMgr.findUserUserAppRelsByUserId(id);
        LOG.info("< findUserUserAppRelsByUserId");
        return new ResponseEntity<>(userUserAppRelResponse, HttpStatus.OK);
    }

    /**
     *
     * @param httpServletRequest
     * @param id
     * @param siteAARelId
     * @return UserUserAppRelResponse
     */
    @RequestMapping(value = "/findByUserIdSiteAARelId/{id}/{siteAARelId}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<UserUserAppRelResponse> findUserUserAppRelsByUserIdSiteAARelId(
            HttpServletRequest httpServletRequest,
            @PathVariable String id, @PathVariable String siteAARelId) {
        LOG.info("> findUserUserAppRelsByUserIdAndSiteAARelId");
        final ValidationRequest validationRequest = validator.formObjectValidationRequest(httpServletRequest, "USER");
        final UserUserAppRelResponse userUserAppRelResponse = userUserAppRelMgr.findUserUserAppRelsByUserIdAndSiteAARelId(id, siteAARelId, validationRequest);
        LOG.info("< findUserUserAppRelsByUserIdAndSiteAARelId");
        return new ResponseEntity<>(userUserAppRelResponse, HttpStatus.OK);
    }

    /**
     *
     * @param httpServletRequest
     * @param id
     * @return UserUserAppRelResponse
     */
    /*@RequestMapping(value = "/findUserUserAppRelsByUserId/{id}/{relType}",
           method = RequestMethod.GET,
           consumes = {MediaType.APPLICATION_JSON_VALUE,
               MediaType.APPLICATION_XML_VALUE,
               MediaType.APPLICATION_FORM_URLENCODED_VALUE},
           produces = {MediaType.APPLICATION_JSON_VALUE,
               MediaType.APPLICATION_XML_VALUE,
               MediaType.APPLICATION_FORM_URLENCODED_VALUE}
   )*/
    public @ResponseBody
    final ResponseEntity<UserUserAppRelResponse> findUserUserAppRelsByUserIdAndRelType(
            HttpServletRequest httpServletRequest,
            @PathVariable String id, @PathVariable String relType) {
        LOG.info("> findUserUserAppRelsByUserId");
        final UserUserAppRelResponse userUserAppRelResponse = userUserAppRelMgr.findUserUserAppRelsByUserIdAndRelationType(id, relType);
        LOG.info("< findUserUserAppRelsByUserId");
        return new ResponseEntity<>(userUserAppRelResponse, HttpStatus.OK);
    }

    /**
     *
     * @param httpServletRequest
     * @param id
     * @param siteAARelId
     * @param relType
     * @return UserUserAppRelResponse
     */
    @RequestMapping(value = "/findByUserIdSiteAARelIdRelType/{id}/{siteAARelId}/{relType}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<UserUserAppRelResponse> findUserUserAppRelsByUserIdAndSiteAARelIdAndRelType(
            HttpServletRequest httpServletRequest,
            @PathVariable String id, @PathVariable String siteAARelId, @PathVariable String relType) {
        LOG.info("> findUserUserAppRelsByUserId");
        final ValidationRequest validationRequest = validator.formObjectValidationRequest(httpServletRequest, "USERAPPLICATION");
        final UserUserAppRelResponse userUserAppRelResponse = userUserAppRelMgr.findUserUserAppRelsByUserIdAndSiteAARelIdAndRelType(id, siteAARelId, relType, validationRequest);
        LOG.info("< findUserUserAppRelsByUserId");
        return new ResponseEntity<>(userUserAppRelResponse, HttpStatus.OK);
    }

    /**
     *
     * @param httpServletRequest
     * @param id
     * @return UserUserAppRelResponse
     */
    @RequestMapping(value = "/findUserUserAppRelsByUserAppId/{id}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<UserUserAppRelResponseWrapper> findUserUserAppRelsByUserAppId(
            HttpServletRequest httpServletRequest,
            @PathVariable String id) {
        LOG.info("> findUserUserAppRelsByUserId");
        final ValidationRequest validationRequest = validator.formObjectValidationRequest(httpServletRequest, "USERAPPLICATION");
        final UserUserAppRelResponseWrapper userUserAppRelResponse = userUserAppRelMgr.findByUserAppId(id, validationRequest);
        LOG.info("< findUserUserAppRelsByUserId");
        return new ResponseEntity<>(userUserAppRelResponse, HttpStatus.OK);
    }

    /**
     *
     * @param httpServletRequest
     * @param id
     * @return boolean
     */
    @RequestMapping(value = "/delete/{id}",
            method = RequestMethod.DELETE,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<Boolean> delete(
            HttpServletRequest httpServletRequest,
            @NotNull @PathVariable String id) {
        LOG.info("> delete");
        Boolean isDeleted = false;
		try {
			UserUserAppRelTbl userUserAppRelTbl = this.userUserAppRelJpaDao.findOne(id);
			isDeleted = userUserAppRelMgr.deleteById(id);
			this.userUserAppRelAuditMgr.userUserAppDeleteSuccessAudit(httpServletRequest, userUserAppRelTbl, isDeleted );
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        LOG.info("< delete");
        return new ResponseEntity<>(isDeleted, HttpStatus.OK);
    }
    
    
    
    @RequestMapping(value = "/multiDelete",
            method = RequestMethod.DELETE,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<UserUserAppRelResponse> multiDelete(
            HttpServletRequest httpServletRequest,
            @RequestBody Set<String> ids) {
        LOG.info("> multiDelete");
        UserUserAppRelResponse relResponse = null;
		try {
			relResponse = userUserAppRelMgr.multiDelete(ids, httpServletRequest);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        LOG.info("< multiDelete");
        return new ResponseEntity<>(relResponse, HttpStatus.OK);
    }

    /**
     *
     * @param httpServletRequest
     * @param userId
     * @param aaId
     * @return UserUserAppRelResponseWrapper
     */
    @RequestMapping(value = "/findByUserIdAAId/{userId}/{aaId}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<UserUserAppRelResponseWrapper> findUserUserAppRelsByUserIdAndAAId(
            HttpServletRequest httpServletRequest,
            @PathVariable String userId, @PathVariable String aaId) {
        LOG.info("> findUserUserAppRelsByUserIdAndSiteAARelId");
        final ValidationRequest validationRequest = validator.formObjectValidationRequest(httpServletRequest, "USERAPPLICATION");
        final List<UserUserAppRelation> userUserAppRelations = userUserAppRelMgr.findUserUserAppRelsByUserIdAndAAId(userId, aaId, validationRequest);
        UserUserAppRelResponseWrapper responseWrapper = new UserUserAppRelResponseWrapper(userUserAppRelations);
        LOG.info("< findUserUserAppRelsByUserIdAndSiteAARelId");
        return new ResponseEntity<>(responseWrapper, HttpStatus.OK);
    }

    /**
     *
     * @param httpServletRequest
     * @param userUserAppRelBatchRequest
     * @return UserUserAppRelBatchResponse
     */
    @RequestMapping(value = "/updateUserRelTypesByIds",
            method = RequestMethod.POST,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<UserUserAppRelBatchResponse> updateUserRelTypesByIds(
            HttpServletRequest httpServletRequest,
            @RequestBody final UserUserAppRelBatchRequest userUserAppRelBatchRequest) {
        LOG.info("> updateUserRelTypesByIds");
        final UserUserAppRelBatchResponse response = userUserAppRelMgr.updateUserRelTypesByIds(userUserAppRelBatchRequest, httpServletRequest);
        LOG.info("< updateUserRelTypesByIds");
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

}
