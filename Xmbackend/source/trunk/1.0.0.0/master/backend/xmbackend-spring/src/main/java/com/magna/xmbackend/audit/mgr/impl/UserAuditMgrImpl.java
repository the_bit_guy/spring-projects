/**
 * 
 */
package com.magna.xmbackend.audit.mgr.impl;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.magna.xmbackend.audit.mgr.UserAuditMgr;
import com.magna.xmbackend.audit.mgr.utils.AdminHistoryBaseObjectUtils;
import com.magna.xmbackend.entities.AdminHistoryBaseObjectsTbl;
import com.magna.xmbackend.entities.UsersTbl;
import com.magna.xmbackend.jpa.dao.AdminHistoryBaseObjectsJpaDao;
import com.magna.xmbackend.vo.user.UserRequest;
import com.magna.xmbackend.vo.user.UserResponse;

/**
 * @author Bhabadyuti Bal
 *
 */
@Component
public class UserAuditMgrImpl implements UserAuditMgr {
	
	
	private static final Logger LOG = LoggerFactory.getLogger(UserAuditMgrImpl.class);
	
	
	final String OBJECT_NAME = "UserTbl";
	final String RESULT_SUCCESS = "Success";
	final String RESULT_FAILURE = "Failure";
	
	@Autowired
    private AdminHistoryBaseObjectsJpaDao adminHistoryBaseObjectsJpaDao;

    @Autowired
    private AdminHistoryBaseObjectUtils adminHistoryBaseObjectUtils;
	
	

	@Override
	public void userMultiSaveSuccessAuditor(HttpServletRequest httpServletRequest, List<UserRequest> userRequests, UserResponse response) {
		Iterable<UsersTbl> userTbls = response.getUserTbls();
		List<Map<String, String>> statusMaps = response.getStatusMaps();
		
		if (statusMaps.isEmpty()) {
			// All Success
			this.createSuccessAudit(userTbls, httpServletRequest);

		} else {
			//Partial Success case
			//Iterate the status map to log the failed ones
			this.createFailureAudit(statusMaps, userRequests, httpServletRequest);

			//Iterate the userProjectRelTbls for Success ones
			this.createSuccessAudit(userTbls, httpServletRequest);
		}
		
	}

	private void createFailureAudit(List<Map<String, String>> statusMaps, List<UserRequest> userRequests,
			HttpServletRequest httpServletRequest) {
		
		statusMaps.forEach(map -> {
			userRequests.forEach(userRequest -> {
				String userName = userRequest.getUserName();
				String message = map.get("en");
				if(message.contains(userName)){
			        String changes = "";
			        String errorMessage = message;
			        String operation = "user import";
			        AdminHistoryBaseObjectsTbl ahbot = this.adminHistoryBaseObjectUtils
			                .makeAdminHistoryBaseObjectsTbl(
			                		httpServletRequest, changes, errorMessage,
			                		OBJECT_NAME, operation, "Failure");
			        this.adminHistoryBaseObjectsJpaDao.save(ahbot);
				}
			});
		});
	}

	private void createSuccessAudit(Iterable<UsersTbl> userTbls, HttpServletRequest httpServletRequest) {
		userTbls.forEach(userTbl -> {
			String userName = userTbl.getUsername();
	        String changes = "User created with name " + userName;
	        String errorMessage = "";
	        String operation = "user import";
	        AdminHistoryBaseObjectsTbl ahbot = this.adminHistoryBaseObjectUtils
	                .makeAdminHistoryBaseObjectsTbl(
	                		httpServletRequest, changes, errorMessage,
	                		OBJECT_NAME, operation, "Success");
	        this.adminHistoryBaseObjectsJpaDao.save(ahbot);
		});
		
	}

	@Override
	public void userMultiSaveFailureAuditor(HttpServletRequest httpServletRequest, Exception ex) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void userDeleteSuccessAudit(HttpServletRequest httpServletRequest, String userId, String name) {
		String changes = "User with name " + name
                + " deleted successfully";
        AdminHistoryBaseObjectsTbl ahbot
                = this.adminHistoryBaseObjectUtils
                        .makeAdminHistoryBaseObjectsTbl(httpServletRequest,
                                changes, "", OBJECT_NAME,
                                "user delete", "Success");
        this.adminHistoryBaseObjectsJpaDao.save(ahbot);
		
	}

	@Override
	public void userUpdateSuccessAudit(HttpServletRequest hsr, UserRequest ur) {
		LOG.info(">>>> userUpdateSuccessAudit");
        String name = ur.getUserName();
        String changes = "User with name "+name+" got updated";
        AdminHistoryBaseObjectsTbl ahbot
                = this.adminHistoryBaseObjectUtils
                        .makeAdminHistoryBaseObjectsTbl(hsr, changes, "",
                        		OBJECT_NAME, "user update", "Success");
        this.adminHistoryBaseObjectsJpaDao.save(ahbot);
        LOG.info("<<<< userUpdateSuccessAudit");
		
	}

	@Override
	public void userUpdateFailureAudit(HttpServletRequest hsr, UserRequest userRequest, String errMsg) {
		LOG.info(">>>> userUpdateFailureAudit");
        AdminHistoryBaseObjectsTbl ahbot = this.adminHistoryBaseObjectUtils
                .makeAdminHistoryBaseObjectsTbl(hsr, "", errMsg,
                		OBJECT_NAME, "user update", "Failure");
        this.adminHistoryBaseObjectsJpaDao.save(ahbot);
        LOG.info("<<<< userUpdateFailureAudit");
		
	}

	@Override
	public void userStatusUpdateSuccessAuditor(String userName, String status, HttpServletRequest httpServletRequest) {
		LOG.info(">>>> userStatusUpdateSuccessAuditor");
		String errorMsg = "";
		final StringBuilder changeBuff = new StringBuilder();
		changeBuff.append("User with name ").append(userName).append(" updated ").append(" with status ")
				.append(status);

		AdminHistoryBaseObjectsTbl ahbot = this.adminHistoryBaseObjectUtils
				.makeAdminHistoryBaseObjectsTbl(
						httpServletRequest, changeBuff.toString(), errorMsg,
						OBJECT_NAME, "User status update", RESULT_SUCCESS);
		this.adminHistoryBaseObjectsJpaDao.save(ahbot);
		LOG.info("<<<< userStatusUpdateSuccessAuditor");
	}

	@Override
	public void userStatusUpdateFailureAuditor(String userName, String status, Exception ex,
			HttpServletRequest httpServletRequest) {
		LOG.info(">>>> userStatusUpdateFailureAuditor");
		
		String errorMsg = ex.getMessage();
		final StringBuilder changeBuff = new StringBuilder();
		changeBuff.append("User with name ").append(userName).append(" updated ").append(" with status ")
				.append(status);

		AdminHistoryBaseObjectsTbl ahbot = this.adminHistoryBaseObjectUtils
				.makeAdminHistoryBaseObjectsTbl(
						httpServletRequest, changeBuff.toString(), errorMsg,
						OBJECT_NAME, "User status update", RESULT_FAILURE);
		this.adminHistoryBaseObjectsJpaDao.save(ahbot);
		LOG.info("<<<< userStatusUpdateFailureAuditor");
	}

	@Override
	public void userDeleteFailureAudit(HttpServletRequest httpServletRequest, String userId, String name) {
		String changes = "User with name " + name
                + " deletion failed";
        AdminHistoryBaseObjectsTbl ahbot
                = this.adminHistoryBaseObjectUtils
                        .makeAdminHistoryBaseObjectsTbl(httpServletRequest,
                                changes, "", OBJECT_NAME,
                                "user delete", "Failure");
        this.adminHistoryBaseObjectsJpaDao.save(ahbot);
		
	}

	@Override
	public void createSuccessAuditForBatch(UsersTbl userTbl, HttpServletRequest httpServletRequest) {
		String userName = userTbl.getUsername();
        String changes = "User created with name " + userName;
        String errorMessage = "";
        String operation = "user import";
        AdminHistoryBaseObjectsTbl ahbot = this.adminHistoryBaseObjectUtils
                .makeAdminHistoryBaseObjectsTbl(
                		httpServletRequest, changes, errorMessage,
                		OBJECT_NAME, operation, "Success");
        this.adminHistoryBaseObjectsJpaDao.save(ahbot);
		
	}

	@Override
	public void createFailureAuditForBatch(UserRequest userRequest, HttpServletRequest httpServletRequest) {

		String userName = userRequest.getUserName();
        String errorMessage = "User creation with name " + userName +" failed";
        String changes = "";
        String operation = "user create";
        AdminHistoryBaseObjectsTbl ahbot = this.adminHistoryBaseObjectUtils
                .makeAdminHistoryBaseObjectsTbl(
                		httpServletRequest, changes, errorMessage,
                		OBJECT_NAME, operation, "Failure");
        this.adminHistoryBaseObjectsJpaDao.save(ahbot);
	
	}

}
