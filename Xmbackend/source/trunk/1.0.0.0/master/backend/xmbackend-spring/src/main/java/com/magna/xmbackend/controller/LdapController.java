/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.magna.xmbackend.mgr.LdapMgr;
import com.magna.xmbackend.vo.ldap.LdapUser;
import com.magna.xmbackend.vo.ldap.LdapUsersResponse;

/**
 *
 * @author dhana
 */
@RestController
@RequestMapping(value = "/ldap")
public class LdapController {

    private static final Logger LOG
            = LoggerFactory.getLogger(LdapController.class);

    @Autowired
    private LdapMgr ldapMgr;

    @RequestMapping(value = "/all/userNames",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<List<String>> findAllLdapUserNames(
            HttpServletRequest httpServletRequest) {
        LOG.info("> findAllLdapUserNames");
        List<String> ldapPersons = this.ldapMgr.getAllUserNames();
        LOG.info("< findAllLdapUserNames");
        return new ResponseEntity<>(ldapPersons, HttpStatus.OK);
    }

    @RequestMapping(value = "/all/ous",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<List<String>> findAllOUNames(
            HttpServletRequest httpServletRequest) {
        LOG.info("> findAllOUNames");
        List<String> ous = this.ldapMgr.getAllOUNames();
        LOG.info("< findAllOUNames");
        return new ResponseEntity<>(ous, HttpStatus.OK);
    }

    @RequestMapping(value = "/all/ldapUsers",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<List<LdapUser>> findAllLdapUsers(
            HttpServletRequest httpServletRequest) {
        LOG.info("> findAllLdapUsers");
        List<LdapUser> allLdapUsers = this.ldapMgr.getAllLdapUsers();
        LOG.info("< findAllLdapUsers");
        return new ResponseEntity<>(allLdapUsers, HttpStatus.OK);
    }

    @RequestMapping(value = "/ldapUserDetail/{userName}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<LdapUser> findLdapUserDetail(
            HttpServletRequest httpServletRequest,
            @PathVariable(name = "userName") String userName) {
        LOG.info("> findLdapUserDetail");
        LdapUser ldapUserDetails = this.ldapMgr.getLdapUserDetails(userName);
        LOG.info("< findLdapUserDetail");
        return new ResponseEntity<>(ldapUserDetails, HttpStatus.OK);
    }
    
    
    @RequestMapping(value = "/search/{name}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<List<String>> searchAllMatchingUserNames(
            HttpServletRequest httpServletRequest, @PathVariable String name) {
        LOG.info("> searchAllMatchingUsers");
        List<String> ldapPersons = this.ldapMgr.getAllUserNamesByAnyName(name);
        LOG.info("< searchAllMatchingUsers");
        return new ResponseEntity<>(ldapPersons, HttpStatus.OK);
    }
    
    @RequestMapping(value = "/searchUsers/{name}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<LdapUsersResponse> searchAllMatchingUsers(
            HttpServletRequest httpServletRequest, @PathVariable String name) {
        LOG.info("> searchAllMatchingUsers");
        LdapUsersResponse usersResponse = this.ldapMgr.getAllUsersByAnyName(name);
        LOG.info("< searchAllMatchingUsers");
        return new ResponseEntity<>(usersResponse, HttpStatus.OK);
    }

}
