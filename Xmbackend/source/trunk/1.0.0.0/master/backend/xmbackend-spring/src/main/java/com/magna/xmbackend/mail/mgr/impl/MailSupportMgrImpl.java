/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.mail.mgr.impl;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import com.magna.xmbackend.entities.EmailNotificationConfigTbl;
import com.magna.xmbackend.entities.EmailNotificationEventTbl;
import com.magna.xmbackend.entities.EmailNotifyToUserRelTbl;
import com.magna.xmbackend.entities.EmailTemplateTbl;
import com.magna.xmbackend.entities.MailQueueTbl;
import com.magna.xmbackend.entities.PropertyConfigTbl;
import com.magna.xmbackend.entities.UsersTbl;
import com.magna.xmbackend.jpa.dao.EmailNotificationConfigJpaDao;
import com.magna.xmbackend.jpa.dao.EmailNotificationEventsJpaDao;
import com.magna.xmbackend.jpa.dao.EmailTemplateJpaDao;
import com.magna.xmbackend.jpa.dao.PropertyConfigJpaDao;
import com.magna.xmbackend.jpa.dao.UserJpaDao;
import com.magna.xmbackend.mail.mgr.MailSupportMgr;
import com.magna.xmbackend.utils.MailSupport;
import com.magna.xmbackend.vo.enums.NotificationVariables;
import com.magna.xmbackend.vo.enums.Status;

/**
 *
 * @author dhana
 */
@Component
public class MailSupportMgrImpl implements MailSupportMgr {

	private static final Logger LOG = LoggerFactory.getLogger(MailSupportMgrImpl.class);

	@Autowired
	private TemplateEngine templateEngine;
	@Autowired
	private PropertyConfigJpaDao propertyConfigJpaDao;
	@Autowired
	private EmailNotificationConfigJpaDao emailNotificationConfigJpaDao;
	@Autowired
	private MailSupport mailSupport;
	@Autowired
	private EmailNotificationEventsJpaDao eventsJpaDao;
	@Autowired
	private EmailTemplateJpaDao templateJpaDao;
	@Autowired
	private UserJpaDao userJpaDao;

	private static final String STAGE_NEW = "new";


	// private static final String DIRECT_MAIL_SEND_TEMPLATE =
	// "DIRECT_MAIL_SEND_TEMPLATE";
	
	private String getSMTPEmailId() {
        PropertyConfigTbl propertyConfigTbl = this.propertyConfigJpaDao.findByCategoryAndProperty("SMTP", "SMTP_EMAIL_ID");
        String value = propertyConfigTbl.getValue();
        return value;
    }

	@Override
	public List<UserEventConfigData> getUserEventConfigData(String event) {

		List<UserEventConfigData> configDatas = new ArrayList<>();
		//for the single event how many configuration available, processing has to be done for each notification configuration 
		EmailNotificationEventTbl eventTbl = this.eventsJpaDao.findByEvent(event);
		//find all configuration for the event
		List<EmailNotificationConfigTbl> configTbls = this.emailNotificationConfigJpaDao.findByEmailNotificationEventId(eventTbl);
		//iterate through the configurations and create List<UserEventConfigData>
		for (EmailNotificationConfigTbl configTbl : configTbls) {
			if (configTbl.getStatus().equals(Status.ACTIVE.name())) {
				//String includeRemarks = configTbl.getIncludeRemarks();
				EmailTemplateTbl emailTemplateTbl = this.templateJpaDao.findOne(configTbl.getEmailTemplateId().getEmailTemplateId());
				String messageTemplate = emailTemplateTbl.getMessageTemplate();
				//String projectExpiryNoticePeriod = configTbl.getProjectExpiryNoticePeriod();
				//String sendToAssignedUser = configTbl.getSendToAssignedUser();
				String subject = emailTemplateTbl.getSubject();
				UserEventConfigData userEventConfigData = new UserEventConfigData();
				String templateVeriables = emailTemplateTbl.getTemplateVeriables();
				if(templateVeriables != null) {
					boolean contains = templateVeriables.contains(NotificationVariables.REMARKS.toString());
					if(contains) {
						userEventConfigData.setIncludeRemarks(contains);
					}
				}

				EmailNotifyToUserRelTbl emailNotifyToUserRelTbl = configTbl.getEmailNotifyToUserId();
				String toUserIds = null;
				String ccUserIds = null;
				if(emailNotifyToUserRelTbl != null) {
					toUserIds = emailNotifyToUserRelTbl.getToUsers();
					ccUserIds = emailNotifyToUserRelTbl.getCcUsers();
					String toEmailIds = this.getEmailIds(toUserIds);
					String ccEmailIds = null;
					if(ccUserIds != null) {
						ccEmailIds = this.getEmailIds(ccUserIds);
					}
					
					
					userEventConfigData.setEventType(event);
					userEventConfigData.setFromEmailId(this.getSMTPEmailId());
					userEventConfigData.setMessage(messageTemplate);
					userEventConfigData.setRetryCount("0");
					userEventConfigData.setStage(STAGE_NEW);
					userEventConfigData.setSubject(subject);
					userEventConfigData.setToEmailIds(toEmailIds);
					userEventConfigData.setCcEmailIds(ccEmailIds);
					
					configDatas.add(userEventConfigData);
				}
			}
		}

		return configDatas;
	}

	
	/**
	 * @param toUserIds
	 * @return String of user email id's
	 */
	private String getEmailIds(String toUsers) {
		StringBuilder sb = new StringBuilder();
		List<String> usernames = Arrays.asList(toUsers.split("\\s*;\\s*"));
		usernames.forEach(
				username -> {
							UsersTbl usersTbl = this.userJpaDao.findByUsernameIgnoreCase(username);
							//handle exception 
							if (usersTbl != null) {
								String emailId = usersTbl.getEmailId();
								if(emailId == null) {
									LOG.warn("username {} email id is null and so notification will not be sent", usersTbl.getUsername());
								}
								sb.append(emailId).append(";");
							}
							
						}
				);
		return sb.toString();
	}

	@Override
	public Collection<MailQueueTbl> convert2Entity(List<UserEventConfigData> userEventConfigDatas) {
		Collection<MailQueueTbl> mailQueueTbls = new ArrayList<>();
		Date date = new Date();
		for (UserEventConfigData userEventConfigData : userEventConfigDatas) {
			String eventType = userEventConfigData.getEventType();
			String fromEmailId = this.getSMTPEmailId();
			String retryCount = userEventConfigData.getRetryCount();
			String stage = userEventConfigData.getStage();
			String subject = userEventConfigData.getSubject();
			String toEmailIds = userEventConfigData.getToEmailIds();
			String ccEmailIds = userEventConfigData.getCcEmailIds();
			String username = "USERNAME";
			String textMessage = userEventConfigData.getTextMessage().trim();
			try {
				if (textMessage != null) {
					textMessage = java.net.URLDecoder.decode(textMessage, "ISO-8859-1");
				}
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			
			String uuid = UUID.randomUUID().toString();
			MailQueueTbl mailQueueTbl = new MailQueueTbl(uuid, username, toEmailIds, ccEmailIds, fromEmailId, subject, textMessage, eventType, stage, retryCount, date, date);
			mailQueueTbl.setFromMail(fromEmailId);
			mailQueueTbl.setReTryCount(retryCount);
			mailQueueTbl.setDelaySend("false");

			mailQueueTbls.add(mailQueueTbl);
		}
		return mailQueueTbls;
	}

	@Override
	public String bindProjectTemplate2Data(String userName, String projectName, String assignedUserName, String message, String event) {
		this.mailSupport.createDynamicTemplate(event);
		Context ctx = new Context();
		ctx.setVariable("userName", userName);
		ctx.setVariable("projectName", projectName);
		ctx.setVariable("assignedUserName", assignedUserName);
		// ctx.setVariable("message", message);
		this.templateEngine.clearTemplateCache();
		final String htmlContent = this.templateEngine.process("common_template", ctx);
		this.templateEngine.clearTemplateCache();
		LOG.debug("htmlContent {}", htmlContent);

		return htmlContent;
	}

	@Override
	public String bindProjectTemplateToData(String userName, String projectName, String projExpPeriod,
			String gracePeriod, String event) {
		this.mailSupport.createDynamicTemplate(event);

		Context ctx = new Context();
		ctx.setVariable("userName", userName);
		ctx.setVariable("projExpDays", projExpPeriod);
		ctx.setVariable("projectName", projectName);
		this.templateEngine.clearTemplateCache();
		final String htmlContent = this.templateEngine.process("common_template", ctx);
		this.templateEngine.clearTemplateCache();
		LOG.debug("htmlContent {}", htmlContent);

		return htmlContent;
	}

	@Override
	public void bindTemplate2Data(List<UserEventConfigData> userEventConfigDatas, Map<String, String> eventAttributes,
			String event) {
		userEventConfigDatas.forEach((userEventConfigData) -> {
			Context ctx = new Context();

			eventAttributes.keySet().forEach(attrib -> {
				String value = eventAttributes.get(attrib);
				ctx.setVariable(attrib, value);
			});
			ctx.setVariable("userName", userEventConfigData.getUsername());
			// ctx.setVariable("FULLNAME", userEventConfigData.getFullName());
			// ctx.setVariable("MESSAGE", userEventConfigData.getMessage());
			boolean includeRemarks = userEventConfigData.isIncludeRemarks();
			if (includeRemarks) {
				ctx.setVariable("remarks", userEventConfigData.getMessage());
			}
			//writing the event message template to common_template
			this.mailSupport.createDynamicTemplate(event);

			this.templateEngine.clearTemplateCache();
			final String htmlContent = this.templateEngine.process(NotificationVariables.COMMON_MAIL_TEMPLATE.name(), ctx);
			this.templateEngine.clearTemplateCache();
			LOG.debug("htmlContent {}", htmlContent);
			userEventConfigData.setTextMessage(htmlContent);
		});
	}

	@Override
	public String bindSendMailDirectTemplate2Data(Map<String, String> eventAttributes) {
		LOG.info(">>> bindSendMailDirectTemplate2Data");

		String emailTemplate = this.mailSupport.textToThymleafTemplate(eventAttributes.get("MESSAGE"), null);
		//writing the event message template to common_template
		this.mailSupport.createDynamicTemplateForDirectSendMail(emailTemplate);

		Context ctx = new Context();
		eventAttributes.keySet().forEach(attrib -> {
			String value = eventAttributes.get(attrib);
			ctx.setVariable(attrib, value);
		});

		// DIRECT_MAIL_SEND_TEMPLATE
		this.templateEngine.clearTemplateCache();
		final String htmlContent = this.templateEngine.process(NotificationVariables.COMMON_MAIL_TEMPLATE.name(), ctx);
		LOG.debug("htmlContent {}", htmlContent);
		LOG.info("<<< bindSendMailDirectTemplate2Data");
		this.templateEngine.clearTemplateCache();
		return htmlContent;
	}

/*	private String getSMTPValue(String key) {
		PropertyConfigTbl propertyConfigTbl = this.propertyConfigJpaDao.findByCategoryAndProperty("SMTP", key);
		String value = propertyConfigTbl.getValue();
		return value;
	}*/
}
