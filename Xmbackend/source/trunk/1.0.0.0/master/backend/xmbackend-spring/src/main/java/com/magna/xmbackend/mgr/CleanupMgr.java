/**
 * 
 */
package com.magna.xmbackend.mgr;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Bhabadyuti Bal
 *
 */
public interface CleanupMgr {

	void removeRelationObjects(HttpServletRequest servletRequest, String parseRequestPath);

	void removeObjectFromDirectoryRelations(String objectId);

	void removeObjectFromGroupRelations(String objectId);

	void removeObjectFromAdminMenuConfig(String objectId);

}
