package com.magna.xmbackend.rel.controller;

import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.magna.xmbackend.audit.rel.mgr.AdminAreaProjectAuditMgr;
import com.magna.xmbackend.entities.AdminAreaProjectRelTbl;
import com.magna.xmbackend.rel.mgr.AdminAreaProjectRelMgr;
import com.magna.xmbackend.response.rel.adminareaproject.AdminAreaProjectRelResponseWrapper;
import com.magna.xmbackend.response.rel.adminareaproject.AdminAreaProjectResponse;
import com.magna.xmbackend.response.rel.adminareaproject.AdminAreaProjectResponseWrapper;
import com.magna.xmbackend.utils.Validator;
import com.magna.xmbackend.vo.permission.ValidationRequest;
import com.magna.xmbackend.vo.rel.AdminAreaProjectRelBatchRequest;
import com.magna.xmbackend.vo.rel.AdminAreaProjectRelBatchResponse;
import com.magna.xmbackend.vo.rel.AdminAreaProjectRelIdWithProjectAppRelResponse;
import com.magna.xmbackend.vo.rel.AdminAreaProjectRelIdWithProjectStartAppRelResponse;
import com.magna.xmbackend.vo.rel.AdminAreaProjectRelRequest;
import com.magna.xmbackend.vo.rel.AdminAreaProjectRelResponse;

/**
 *
 * @author vijay
 */
@RestController
@RequestMapping(value = "/adminAreaProjectRel")
public class AdminAreaProjectRelController {

    private static final Logger LOG
            = LoggerFactory.getLogger(AdminAreaProjectRelController.class);
    @Autowired
    private AdminAreaProjectRelMgr adminAreaProjectRelMgr;

    @Autowired
    private Validator validator;
    
    @Autowired
    private AdminAreaProjectAuditMgr adminAreaProjectAuditMgr;
    

    /**
     *
     * @param httpServletRequest
     * @param adminAreaProjectRelRequest
     * @return AdminAreaProjectRelTbl
     */
    @RequestMapping(value = "/save",
            method = RequestMethod.POST,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<AdminAreaProjectRelTbl> save(
            HttpServletRequest httpServletRequest,
            @RequestBody final AdminAreaProjectRelRequest adminAreaProjectRelRequest) {
        LOG.info("> save");
        final String userName = (String) httpServletRequest.getAttribute("userName");
        LOG.info("userName={}", userName);
        final AdminAreaProjectRelTbl aaparOut
                = adminAreaProjectRelMgr.create(adminAreaProjectRelRequest, userName);
        LOG.info("< save");
        return new ResponseEntity<>(aaparOut, HttpStatus.ACCEPTED);
    }

    /**
     *
     * @param httpServletRequest
     * @param adminAreaProjectRelBatchRequest
     * @return ResponseEntity
     */
    @RequestMapping(value = "/multi/save",
            method = RequestMethod.POST,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<AdminAreaProjectRelBatchResponse> batchSave(
            HttpServletRequest httpServletRequest,
            @RequestBody final AdminAreaProjectRelBatchRequest adminAreaProjectRelBatchRequest) {
        LOG.info("> batchSave");
        final String userName = (String) httpServletRequest.getAttribute("userName");
        LOG.info("userName={}", userName);
        AdminAreaProjectRelBatchResponse aaprbr = null;
		try {
			aaprbr = adminAreaProjectRelMgr.createBatch(adminAreaProjectRelBatchRequest, userName);
			this.adminAreaProjectAuditMgr.adminAreaProjectMultiSaveAuditor(adminAreaProjectRelBatchRequest, aaprbr, httpServletRequest);
		} catch (Exception ex) {
			this.adminAreaProjectAuditMgr.adminAreaProjectMultiSaveFailureAuditor(httpServletRequest, ex);
		}
        LOG.info("< batchSave");
        return new ResponseEntity<>(aaprbr, HttpStatus.ACCEPTED);
    }

    /**
     *
     * @param httpServletRequest
     * @return ResponseEntity
     */
    @RequestMapping(value = "/findAll",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<AdminAreaProjectRelResponse> findAll(HttpServletRequest httpServletRequest) {
        LOG.info("> findAll");
        final AdminAreaProjectRelResponse adminAreaProjectRelResponse = adminAreaProjectRelMgr.findAll();
        LOG.info("< findAll");
        return new ResponseEntity<>(adminAreaProjectRelResponse, HttpStatus.OK);
    }

    /**
     *
     * @param httpServletRequest
     * @param id
     * @return AdminAreaProjectRelTbl
     */
    @RequestMapping(value = "/find/{id}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<AdminAreaProjectRelTbl> find(
            HttpServletRequest httpServletRequest,
            @PathVariable String id) {
        LOG.info("> find {}", id);
        final AdminAreaProjectRelTbl adminAreaProjectRelTbl = adminAreaProjectRelMgr.findById(id);
        LOG.info("< find");
        return new ResponseEntity<>(adminAreaProjectRelTbl, HttpStatus.OK);
    }

    /**
     *
     * @param httpServletRequest
     * @param id
     * @return Boolean
     */
    @RequestMapping(value = "/delete/{id}",
            method = RequestMethod.DELETE,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<Boolean> deleteById(
            HttpServletRequest httpServletRequest,
            @PathVariable String id) {
        LOG.info("> deleteById >> {}", id);
        final boolean isDeleted = adminAreaProjectRelMgr.delete(id);
        final ResponseEntity<Boolean> responseEntity
                = new ResponseEntity<>(isDeleted, HttpStatus.OK);
        LOG.info("< deleteById");
        return responseEntity;
    }

    @RequestMapping(value = "/multiDelete",
            method = RequestMethod.DELETE,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<AdminAreaProjectRelResponse> multiDelete(
            HttpServletRequest httpServletRequest,
            @RequestBody Set<String> ids) {
        LOG.info("> multiDelete >> {}", ids);
        final AdminAreaProjectRelResponse adminAreaProjectRelResponse = adminAreaProjectRelMgr.multiDelete(ids, httpServletRequest);
        final ResponseEntity<AdminAreaProjectRelResponse> responseEntity
                = new ResponseEntity<>(adminAreaProjectRelResponse, HttpStatus.OK);
        LOG.info("< multiDelete");
        return responseEntity;
    }

    /**
     *
     * @param httpServletRequest
     * @param status
     * @param id
     * @return Boolean
     */
    @RequestMapping(value = "/updateStatus/{status}/{id}",
            method = RequestMethod.POST,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<Boolean> update(
            HttpServletRequest httpServletRequest,
            @PathVariable String status, @PathVariable String id) {
        LOG.info("> updateStatus");
        final boolean updateStatusById = this.adminAreaProjectRelMgr.updateStatusById(status, id, httpServletRequest);
        LOG.info("< updateStatus");
        return new ResponseEntity<>(updateStatusById, HttpStatus.ACCEPTED);
    }
    
    /**
     * 
     * @param httpServletRequest
     * @param adminAreaProjAppRequests
     * @return
     */
    @RequestMapping(value = "/multiStatusUpdate",
    		method = RequestMethod.POST,
    		consumes = {MediaType.APPLICATION_JSON_VALUE,
    			MediaType.APPLICATION_XML_VALUE,
    			MediaType.APPLICATION_FORM_URLENCODED_VALUE},
    		produces = {MediaType.APPLICATION_JSON_VALUE,
    			MediaType.APPLICATION_XML_VALUE,
    			MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<AdminAreaProjectResponseWrapper> multiUpdate(
    		HttpServletRequest httpServletRequest,
    		@RequestBody List<AdminAreaProjectRelRequest> adminAreaProjRequests) {
    	LOG.info("> multiUpdate");
    	AdminAreaProjectResponseWrapper adminAreaProjResponse = this.adminAreaProjectRelMgr.multiUpdate(adminAreaProjRequests, httpServletRequest);
    	LOG.info("< multiUpdate");
    	return new ResponseEntity<>(adminAreaProjResponse, HttpStatus.ACCEPTED);
    }
    
    

    /**
     *
     * @param httpServletRequest
     * @param id
     * @return AdminAreaProjectRelIdWithProjectAppRelResponse
     */
    @RequestMapping(value = "/findAllProjectAppsByAdminAreaProjectRelId/{id}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<AdminAreaProjectRelIdWithProjectAppRelResponse>
            findAllProjectAppsByAdminAreaProjectRelId(
                    HttpServletRequest httpServletRequest,
                    @PathVariable String id) {
        LOG.info("> findAllProjectAppsByAdminAreaProjectRelId {}", id);
        final ValidationRequest validationRequest
                = validator.formObjectValidationRequest(httpServletRequest, "AREA");
        final AdminAreaProjectRelIdWithProjectAppRelResponse response
                = this.adminAreaProjectRelMgr.findAllProjectAppsByAdminAreaProjectRelId(id, validationRequest);
        LOG.info("< findAllProjectAppsByAdminAreaProjectRelId");
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    /**
     *
     * @param httpServletRequest
     * @param id
     * @param relType
     * @return AdminAreaProjectRelIdWithProjectAppRelResponse
     */
    @RequestMapping(value = "/findAllProjectAppsByAdminAreaProjectRelId/{id}/{relType}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<AdminAreaProjectRelIdWithProjectAppRelResponse>
            findAllProjectAppsByAdminAreaProjectRelId(
                    HttpServletRequest httpServletRequest,
                    @PathVariable String id, @PathVariable String relType) {
        LOG.info("> findAllProjectAppsByAdminAreaProjectRelId {} {}",
                id, relType);
        final ValidationRequest validationRequest
                = validator.formObjectValidationRequest(httpServletRequest, "AREA");
        final AdminAreaProjectRelIdWithProjectAppRelResponse response
                = this.adminAreaProjectRelMgr.findAllProjectAppsByAdminAreaProjectRelId(id,
                        relType, validationRequest);
        LOG.info("< findAllProjectAppsByAdminAreaProjectRelId");
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    /**
     *
     * @param httpServletRequest
     * @param id
     * @return AdminAreaProjectRelIdWithProjectStartAppRelResponse
     */
    @RequestMapping(value = "/findAllStartAppsByAdminAreaProjectRelId/{id}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<AdminAreaProjectRelIdWithProjectStartAppRelResponse>
            findAllStartAppsByAdminAreaProjectRelId(
                    HttpServletRequest httpServletRequest,
                    @PathVariable String id) {
        LOG.info("> findAllStartAppsByAdminAreaProjectRelId {}", id);
        final ValidationRequest validationRequest
                = validator.formObjectValidationRequest(httpServletRequest, "AREA");
        final AdminAreaProjectRelIdWithProjectStartAppRelResponse response
                = this.adminAreaProjectRelMgr.findAllStartAppsByAdminAreaProjectRelId(id,
                        validationRequest);
        LOG.info("< findAllStartAppsByAdminAreaProjectRelId");
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    /**
     *
     * @param httpServletRequest
     * @param projectId
     * @return AdminAreaProjectResponseWrapper
     */
    @RequestMapping(value = "/findByProject/{projectId}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<AdminAreaProjectResponseWrapper> findByProject(
            HttpServletRequest httpServletRequest,
            @PathVariable String projectId) {
        LOG.info("> findByProject {}", projectId);
        final ValidationRequest validationRequest
                = validator.formObjectValidationRequest(httpServletRequest, "AREA");
        List<AdminAreaProjectResponse> adminAreaProjectResponseList
                = adminAreaProjectRelMgr.findByProjectId(projectId, validationRequest);
        LOG.info("< findByProject");
        return new ResponseEntity<>(new AdminAreaProjectResponseWrapper(adminAreaProjectResponseList), HttpStatus.OK);
    }

    /**
     *
     * @param httpServletRequest
     * @param siteAdminAreaProjId
     * @param projAppId
     * @return AdminAreaProjectRelResponseWrapper
     */
    @RequestMapping(value = "/findBySiteAdminAreaProjectAppId/{siteAdminAreaProjId}/{projAppId}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<AdminAreaProjectRelResponseWrapper> findBySAARProjectAppId(
            HttpServletRequest httpServletRequest,
            @PathVariable String siteAdminAreaProjId, @PathVariable String projAppId) {
        LOG.info("> findBySAARProjectAppId");
        final ValidationRequest validationRequest
                = validator.formObjectValidationRequest(httpServletRequest, "AREA");
        AdminAreaProjectRelResponseWrapper adminAreaProjectRelResponseWrapper
                = adminAreaProjectRelMgr.findBySAARProjectAppId(siteAdminAreaProjId,
                        projAppId, validationRequest);
        LOG.info("< findBySAARProjectAppId");
        return new ResponseEntity<>(adminAreaProjectRelResponseWrapper, HttpStatus.OK);
    }

    /**
     * Find by site admin area rel id and project id.
     *
     * @param httpServletRequest the http servlet request
     * @param siteAdminAreaRelId the site admin area rel id
     * @param projectId the project id
     * @return the response entity
     */
    @RequestMapping(value = "/findBySiteAdminAreaRelIdAndProjectId/{siteAdminAreaRelId}/{projectId}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<AdminAreaProjectRelTbl> findBySiteAdminAreaRelIdAndProjectId(
            HttpServletRequest httpServletRequest,
            @PathVariable String siteAdminAreaRelId, @PathVariable String projectId) {
        LOG.info("> findBySiteAdminAreaRelIdAndProjectId");
        final ValidationRequest validationRequest
                = validator.formObjectValidationRequest(httpServletRequest, "AREA");
        AdminAreaProjectRelTbl adminAreaProjectRelTbl
                = adminAreaProjectRelMgr.findBySiteAdminAreaRelIdAndProjectId(siteAdminAreaRelId,
                        projectId, validationRequest);
        LOG.info("< findBySiteAdminAreaRelIdAndProjectId");
        return new ResponseEntity<>(adminAreaProjectRelTbl, HttpStatus.OK);
    }
}
