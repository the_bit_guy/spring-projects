package com.magna.xmbackend.mgr;

import java.util.List;
import java.util.Set;

import com.magna.xmbackend.entities.UserApplicationsTbl;
import com.magna.xmbackend.vo.permission.ValidationRequest;
import com.magna.xmbackend.vo.userApplication.UserApplicationMenuWrapper;
import com.magna.xmbackend.vo.userApplication.UserApplicationRequest;
import com.magna.xmbackend.vo.userApplication.UserApplicationResponse;
import javax.servlet.http.HttpServletRequest;

// TODO: Auto-generated Javadoc
/**
 * The Interface UserApplicationMgr.
 *
 * @author dhana
 */
public interface UserApplicationMgr {

    /**
     * Find all.
     *
     * @param validationRequest the validation request
     * @return UserApplicationResponse
     */
    UserApplicationResponse findAll(final ValidationRequest validationRequest);

    /**
     * Find by id.
     *
     * @param id the id
     * @param validationRequest the validation request
     * @return UserApplicationsTbl
     */
    UserApplicationsTbl findById(final String id,
            final ValidationRequest validationRequest);

    /**
     * Find by id.
     *
     * @param id the id
     * @return UserApplicationsTbl
     */
    UserApplicationsTbl findById(final String id);

    /**
     * Creates the or update.
     *
     * @param userApplicationRequest the user application request
     * @param isUpdate the is update
     * @return UserApplicationsTbl
     */
    UserApplicationsTbl createOrUpdate(
            final UserApplicationRequest userApplicationRequest,
            final boolean isUpdate);

    /**
     * Delete.
     *
     * @param id the id
     * @return true, if successful
     */
    boolean delete(final String id);

    /**
     * Multi delete.
     *
     * @param ids the ids
     * @param httpServletRequest the http servlet request
     * @return UserApplicationResponse
     */
    UserApplicationResponse multiDelete(final Set<String> ids,
            final HttpServletRequest httpServletRequest);

    /**
     * Update status by id.
     *
     * @param status the status
     * @param id the id
     * @return true, if successful
     */
    boolean updateStatusById(final String status, final String id);

    /**
     * Find user applications by base app id.
     *
     * @param id the id
     * @param validationRequest the validation request
     * @return UserApplicationResponse
     */
    UserApplicationResponse findUserApplicationsByBaseAppId(final String id,
            final ValidationRequest validationRequest);

    /**
     * Find user app by position.
     *
     * @param position the position
     * @param validationRequest the validation request
     * @return UserApplicationResponse
     */
    UserApplicationResponse findUserAppByPosition(final String position,
            final ValidationRequest validationRequest);

    /**
     * Find all user app by status.
     *
     * @param status the status
     * @return UserApplicationResponse
     */
    UserApplicationResponse findAllUserAppByStatus(final String status);

    /**
     * Find all user app by positions.
     *
     * @param validationRequest the validation request
     * @return UserApplicationResponse
     */
    UserApplicationResponse findAllUserAppByPositions(final ValidationRequest validationRequest);

    /**
     * Find by name.
     *
     * @param name the name
     * @return the user applications tbl
     */
    UserApplicationsTbl findByName(final String name);

    /**
     * Find by user name and language code.
     *
     * @param name the name
     * @param languageCode the language code
     * @return the user applications tbl
     */
    UserApplicationsTbl findByUserNameAndLanguageCode(final String name,
            final String languageCode);

    /**
     * Find user applications by user id.
     *
     * @param id the id
     * @return UserApplicationResponse
     */
    UserApplicationResponse findUserApplicationsByUserId(final String id);

    /**
     * Find all user apps by AA id and active.
     *
     * @param adminAreaId the admin area id
     * @return UserApplicationResponse
     */
    UserApplicationResponse findAllUserAppsByAAIdAndActive(final String adminAreaId);

    /**
     * Gets the all user apps by user AA id.
     *
     * @param userId the user id
     * @param adminAreaId the admin area id
     * @return UserApplicationResponse
     */
    UserApplicationResponse getAllUserAppsByUserAAId(final String userId,
            final String adminAreaId);

    /**
     * Find user app by AA id and user name.
     *
     * @param aaId the aa id
     * @param tkt the tkt
     * @param userName the user name
     * @param validationRequest the validation request
     * @return UserApplicationMenuWrapper
     */
    UserApplicationMenuWrapper findUserAppByAAIdAndUserName(final String aaId, final String tkt,
            final String userName, final ValidationRequest validationRequest);

    
	/**
	 * Multi update.
	 *
	 * @param userApplicationRequests the user application requests
	 * @param httpServletRequest the http servlet request
	 * @return the user application response
	 */
	UserApplicationResponse multiUpdate(List<UserApplicationRequest> userApplicationRequests,
			HttpServletRequest httpServletRequest);

	/**
	 * Find user app and child by name.
	 *
	 * @param userAppName the user app name
	 * @param tkt the tkt
	 * @param userName the user name
	 * @param validationRequest the validation request
	 * @return the user application menu wrapper
	 */
	UserApplicationMenuWrapper findUserAppAndChildByName(String userAppName, String tkt, String userName,
			ValidationRequest validationRequest);

	/**
	 * Update for batch.
	 *
	 * @param userApplicationRequest the user application request
	 * @return the user applications tbl
	 */
	UserApplicationsTbl updateForBatch(UserApplicationRequest userApplicationRequest);

}
