/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.controller;

import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.magna.xmbackend.entities.EmailNotificationEventTbl;
import com.magna.xmbackend.entities.EmailNotifyToUserRelTbl;
import com.magna.xmbackend.entities.EmailTemplateTbl;
import com.magna.xmbackend.mail.mgr.SendMailMgr;
import com.magna.xmbackend.mgr.NotificationMgr;
import com.magna.xmbackend.vo.enums.Status;
import com.magna.xmbackend.vo.notification.EmailTemplateRequest;
import com.magna.xmbackend.vo.notification.EmailTemplateResponse;
import com.magna.xmbackend.vo.notification.NotificationConfigResponse;
import com.magna.xmbackend.vo.notification.NotificationEventResponse;
import com.magna.xmbackend.vo.notification.NotificationRequest;
import com.magna.xmbackend.vo.notification.NotificationResponse;
import com.magna.xmbackend.vo.notification.SendMailRequest;
import com.magna.xmbackend.vo.notification.SendMailResponse;

@RestController
@RequestMapping(value = "/notify")
public class NotificationController {

    private static final Logger LOG
            = LoggerFactory.getLogger(NotificationController.class);

    @Autowired
    private NotificationMgr notificationMgr;

    @Autowired
    private SendMailMgr sendMailMgr;

    @RequestMapping(value = "/findAll/pendingNotification",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<Iterable<EmailNotifyToUserRelTbl>> findAllPendingNotification(
            HttpServletRequest httpServletRequest) {
        LOG.info("> findAllPendingNotification");
        Iterable<EmailNotifyToUserRelTbl> enturts
                = this.notificationMgr.findAllPendingNotification();
        LOG.info("< findAllPendingNotification");
        return new ResponseEntity<>(enturts, HttpStatus.OK);
    }

    @RequestMapping(value = "/findAll/config",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<List<NotificationConfigResponse>> findAllNotificationConfig(
			HttpServletRequest httpServletRequest) {
		LOG.info("> findAllNotificationConfig");
		List<NotificationConfigResponse> notificationConfigResponses = this.notificationMgr.findAllNotificationConfig();
		LOG.info("< findAllNotificationConfig");
		return new ResponseEntity<>(notificationConfigResponses, HttpStatus.OK);
	}

    /**
     * 
     * @param httpServletRequest
     * @param notificationRequest
     * @return
     * 
     */
    @RequestMapping(value = "/save",
            method = RequestMethod.POST,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<NotificationConfigResponse> save(
            HttpServletRequest httpServletRequest,
			@RequestBody @Valid NotificationRequest notificationRequest) {
		LOG.info("> save");
		NotificationConfigResponse nr = this.notificationMgr.createNotification(notificationRequest);
		LOG.info("< save");
		return new ResponseEntity<>(nr, HttpStatus.ACCEPTED);
	}
    
    
    @DeleteMapping(value="/deleteNotificationConfig/{notifConfigId}")
    public HttpEntity<Boolean> deleteNotificationConfiguration(HttpServletRequest httpServletRequest, 
    		@PathVariable String notifConfigId) {
    	LOG.info("> deleteNotificationConfiguration");
    	boolean isDeleted = this.notificationMgr.deleteNotificationConfig(httpServletRequest, notifConfigId);
    	LOG.info("< deleteNotificationConfiguration");
    	return new ResponseEntity<>(isDeleted, HttpStatus.OK);
    }
    
    
    @PostMapping(value="/configs/multiDelete")
    public HttpEntity<NotificationResponse> deleteNotificationConfiguration(HttpServletRequest httpServletRequest, 
    		@RequestBody Set<String> notifConfigIds) {
    	LOG.info("> multi deleteNotificationConfiguration");
    	NotificationResponse response = this.notificationMgr.multiDeleteNotificationConfig(httpServletRequest, notifConfigIds);
    	LOG.info("< multi deleteNotificationConfiguration");
    	return new ResponseEntity<>(response, HttpStatus.OK);
    }
    

    @RequestMapping(value = "/updateNotificationConfig",
            method = RequestMethod.PUT,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<NotificationConfigResponse> updateNotification(
            HttpServletRequest httpServletRequest,
			@RequestBody @Valid NotificationRequest notificationRequest) {
		LOG.info("> updateNotification");
		NotificationConfigResponse ncr = this.notificationMgr.updateNotification(notificationRequest);
		LOG.info("< updateNotification");
		return new ResponseEntity<>(ncr, HttpStatus.OK);
	}
    
    
    @RequestMapping(value = "/updateNotificationEventStatus/{event}/{status}",
            method = RequestMethod.PUT,
            /*consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},*/
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<Boolean> updateNotificationStatus(
            HttpServletRequest httpServletRequest, @PathVariable String event,
            @PathVariable String status) {
		LOG.info("> updateNotification");
		boolean isUpdated = this.notificationMgr.updateNotificationStatus(httpServletRequest, event, status);
		LOG.info("< updateNotification");
		return new ResponseEntity<>(isUpdated, HttpStatus.OK);
	}
    
    
    @RequestMapping(value = "/updateNotificationConfigStatus/{configId}/{status}",
            method = RequestMethod.PUT,
            /*consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},*/
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<Boolean> updateNotificationConfigStatus(
            HttpServletRequest httpServletRequest, @PathVariable String configId,
            @PathVariable String status) {
		LOG.info("> updateNotification");
		boolean isUpdated = this.notificationMgr.updateNotificationConfigStatus(httpServletRequest, configId, status);
		LOG.info("< updateNotification");
		return new ResponseEntity<>(isUpdated, HttpStatus.OK);
	}
    

    @RequestMapping(value = "/findByEvent/{event}",
            method = RequestMethod.GET,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<List<NotificationConfigResponse>> findByEvent(
            HttpServletRequest httpServletRequest, @PathVariable String event) {
    	LOG.info("> findByEvent");
		List<NotificationConfigResponse> notificationConfigResponses = this.notificationMgr.findByEvent(event);
		LOG.info("< findByEvent");
		return new ResponseEntity<>(notificationConfigResponses, HttpStatus.OK);
    }

    @RequestMapping(value = "/sendMail",
            method = RequestMethod.POST,
            consumes = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE,
                MediaType.APPLICATION_XML_VALUE,
                MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public @ResponseBody
    final ResponseEntity<SendMailResponse> sendMail(
            HttpServletRequest httpServletRequest,
            @RequestBody SendMailRequest sendMailRequest) {
        LOG.info("> sendMail");
        SendMailResponse ack = null;
        if (sendMailRequest.getStatus().name().equals(Status.ACTIVE.name())) {
        	ack = this.sendMailMgr.sendMail(sendMailRequest);
        }
        LOG.info("< sendMail");
        return new ResponseEntity<>(ack, HttpStatus.ACCEPTED);
    }
    
    
    @PostMapping(value="/saveTemplate")
    public @ResponseBody
    final ResponseEntity<EmailTemplateTbl> saveEmailTemplate(HttpServletRequest httpServletRequest, 
    		@RequestBody @Valid EmailTemplateRequest templateRequest) {
    	LOG.info("> saveEmailTemplate");
    	EmailTemplateTbl saveEmailTemplate = this.notificationMgr.saveOrUpdateEmailTemplate(templateRequest, false);
    	LOG.info("< saveEmailTemplate");
    	return new ResponseEntity<>(saveEmailTemplate, HttpStatus.OK);
    }
    
    
    @PostMapping(value="/updateTemplate")
    public @ResponseBody
    final ResponseEntity<EmailTemplateTbl> updateEmailTemplate(HttpServletRequest httpServletRequest, 
    		@RequestBody @Valid EmailTemplateRequest templateRequest) {
    	LOG.info("> updateTemplate");
    	EmailTemplateTbl saveEmailTemplate = this.notificationMgr.saveOrUpdateEmailTemplate(templateRequest, true);
    	LOG.info("< updateTemplate");
    	return new ResponseEntity<>(saveEmailTemplate, HttpStatus.OK);
    }
    
    
    @DeleteMapping(value="/deleteTemplate/{templateId}")
    public @ResponseBody
    final ResponseEntity<Boolean> deleteEmailTemplate(HttpServletRequest httpServletRequest, 
    		@PathVariable("templateId") @Valid @NotNull String templateId) {
    	LOG.info("> deleteTemplate");
    	boolean isDeleted = this.notificationMgr.deleteEmailTemplate(templateId);
    	LOG.info("< deleteTemplate");
    	return new ResponseEntity<>(isDeleted, HttpStatus.OK);
    }
    
    
    @PostMapping(value="/templates/multiDelete")
    public @ResponseBody
    final ResponseEntity<EmailTemplateResponse> multiDeleteEmailTemplate(HttpServletRequest httpServletRequest, 
    		@RequestBody @Valid @NotNull Set<String> templateIds) {
    	LOG.info("> multi deleteTemplate");
    	EmailTemplateResponse response = this.notificationMgr.multiDeleteEmailTemplate(httpServletRequest, templateIds);
    	LOG.info("< multi deleteTemplate");
    	return new ResponseEntity<>(response, HttpStatus.OK);
    }
    
    @GetMapping(value="/findAllTemplates")
    public @ResponseBody
    final ResponseEntity<EmailTemplateResponse> findAll(HttpServletRequest httpServletRequest) {
    	LOG.info("> findAll");
    	Iterable<EmailTemplateTbl> emailTemplateTbls = this.notificationMgr.findAll();
    	EmailTemplateResponse templateResponse = new EmailTemplateResponse(emailTemplateTbls);
    	LOG.info("< findAll");
    	return new ResponseEntity<EmailTemplateResponse>(templateResponse, HttpStatus.OK);
    }
    
    
    @GetMapping(value="/findAllEvents")
    public @ResponseBody
    final ResponseEntity<NotificationEventResponse> findAllEvents(HttpServletRequest httpServletRequest) {
    	LOG.info("> findAllEvents");
    	List<EmailNotificationEventTbl> notificationEventTbls = this.notificationMgr.findAllEvents();
    	NotificationEventResponse notificationEventResponse = new NotificationEventResponse(notificationEventTbls);
    	LOG.info("< findAllEvents");
    	return new ResponseEntity<NotificationEventResponse>(notificationEventResponse, HttpStatus.OK);
    }

}
