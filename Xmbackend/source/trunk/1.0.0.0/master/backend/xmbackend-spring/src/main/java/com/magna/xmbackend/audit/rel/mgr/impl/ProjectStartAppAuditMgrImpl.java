/**
 * 
 */
package com.magna.xmbackend.audit.rel.mgr.impl;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.magna.xmbackend.audit.mgr.utils.AdminHistoryRelsUtil;
import com.magna.xmbackend.audit.rel.mgr.ProjectStartAppAuditMgr;
import com.magna.xmbackend.entities.AdminAreaProjectRelTbl;
import com.magna.xmbackend.entities.AdminHistoryRelationsTbl;
import com.magna.xmbackend.entities.ProjectStartAppRelTbl;
import com.magna.xmbackend.entities.StartApplicationsTbl;
import com.magna.xmbackend.jpa.dao.StartApplicationJpaDao;
import com.magna.xmbackend.jpa.rel.dao.AdminAreaProjectRelJpaDao;
import com.magna.xmbackend.jpa.rel.dao.AdminHistoryRelationJpaDao;
import com.magna.xmbackend.jpa.rel.dao.ProjectStartAppRelJpaDao;
import com.magna.xmbackend.vo.rel.ProjectStartAppRelBatchRequest;
import com.magna.xmbackend.vo.rel.ProjectStartAppRelBatchResponse;
import com.magna.xmbackend.vo.rel.ProjectStartAppRelRequest;

/**
 * @author Bhabadyuti Bal
 *
 */
@Component
public class ProjectStartAppAuditMgrImpl implements ProjectStartAppAuditMgr {
	
	private static Logger LOG = LoggerFactory.getLogger(ProjectStartAppAuditMgrImpl.class);
	
	final String RELATION_NAME = "ProjectStartApp";
	final String RESULT_SUCCESS = "Success";
	final String RESULT_FAILURE = "Failure";
	
	@Autowired
	private AdminHistoryRelationJpaDao adminHistoryRelationJpaDao;
	@Autowired
	private AdminHistoryRelsUtil adminHistoryRelsUtil;
	@Autowired
	private StartApplicationJpaDao startApplicationJpaDao;
	@Autowired
	private AdminAreaProjectRelJpaDao adminAreaProjectRelJpaDao;
	@Autowired
	private ProjectStartAppRelJpaDao projectStartAppRelJpaDao;

	
	
	@Override
	public void projectStartAppMultiSaveAuditor(ProjectStartAppRelBatchRequest batchReq,
			ProjectStartAppRelBatchResponse batchRes, HttpServletRequest httpServletRequest) {
		LOG.info(">>> userStartAppMultiSaveAuditor");

		List<ProjectStartAppRelRequest> projectStartAppRelRequests = batchReq.getProjectStartAppRelRequests();

		List<ProjectStartAppRelTbl> projectStartAppRelTbls = batchRes.getProjectStartAppRelTbls();
		List<Map<String, String>> statusMap = batchRes.getStatusMap();

		if (statusMap.isEmpty()) {
			// All success
			this.createSuccessAudit(projectStartAppRelTbls, httpServletRequest);

		} else {
			//Partial success case
			// iterate the status map to log the failed ones
			this.createFailureAudit(statusMap, projectStartAppRelRequests, httpServletRequest);

			// iterate the userProjectRelTbls for success ones
			this.createSuccessAudit(projectStartAppRelTbls, httpServletRequest);
		}
		LOG.info("<<< userStartAppMultiSaveAuditor");
		
		
	}

	private void createFailureAudit(List<Map<String, String>> statusMap,
			List<ProjectStartAppRelRequest> projectStartAppRelRequests, HttpServletRequest httpServletRequest) {


		for (Map<String, String> map : statusMap) {
			for (ProjectStartAppRelRequest projStartAppRelReq : projectStartAppRelRequests) {
				String startAppId = projStartAppRelReq.getStartAppId();
				String adminAreaProjRelId = projStartAppRelReq.getAdminAreaProjectRelId();
				String status = projStartAppRelReq.getStatus();
				
				StartApplicationsTbl startAppTbl = this.startApplicationJpaDao.findOne(startAppId);
				AdminAreaProjectRelTbl adminAreaProjectRelTbl = this.adminAreaProjectRelJpaDao.findOne(adminAreaProjRelId);
				
				
				if (startAppTbl != null && adminAreaProjectRelTbl != null) {
					String message = map.get("en");
					String startAppName = startAppTbl.getName();
					String projName = adminAreaProjectRelTbl.getProjectId().getName();
					if (message.contains(projName) && message.contains(startAppName)) {
						AdminHistoryRelationsTbl adminHistoryRelationTbl = this.adminHistoryRelsUtil
								.makeAdminHistoryRelationTbl(httpServletRequest, RELATION_NAME,
										projName, startAppName,null, 
										null, status,
										null, message, "ProjectStartApp Create", RESULT_FAILURE);
						this.adminHistoryRelationJpaDao.save(adminHistoryRelationTbl);
						break;
					}
				}
			}
		}
	
		
	
		
	}

	private void createSuccessAudit(List<ProjectStartAppRelTbl> projectStartAppRelTbls,
			HttpServletRequest httpServletRequest) {
		projectStartAppRelTbls.forEach(projectStartAppRelTbl -> {
			String startAppName = projectStartAppRelTbl.getStartApplicationId().getName();
			String projectName = projectStartAppRelTbl.getAdminAreaProjectRelId().getProjectId().getName();
			String status = projectStartAppRelTbl.getStatus();
			AdminHistoryRelationsTbl adminHistoryRelationTbl = this.adminHistoryRelsUtil
					.makeAdminHistoryRelationTbl(httpServletRequest, RELATION_NAME, projectName,
							startAppName, null, null,
							status, null, null, "ProjectStartApp Create",
							RESULT_SUCCESS);
			this.adminHistoryRelationJpaDao.save(adminHistoryRelationTbl);
		});
		
	}

	@Override
	public void projectStartAppMultiSaveFailureAuditor(HttpServletRequest httpServletRequest, Exception ex) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void projectStartAppMultiDeleteSuccessAudit(ProjectStartAppRelTbl projectStartAppRelTbl,
			HttpServletRequest httpServletRequest) {
		

		String startAppName = projectStartAppRelTbl.getStartApplicationId().getName();
		String projectName = projectStartAppRelTbl.getAdminAreaProjectRelId().getProjectId().getName();
		String status = projectStartAppRelTbl.getStatus();
		AdminHistoryRelationsTbl adminHistoryRelationTbl = this.adminHistoryRelsUtil
				.makeAdminHistoryRelationTbl(httpServletRequest, RELATION_NAME, projectName,
						startAppName, null, null,
						status, null, null, "ProjectStartApp Delete",
						RESULT_SUCCESS);
		this.adminHistoryRelationJpaDao.save(adminHistoryRelationTbl);
	
	}

	@Override
	public void projectStartAppStatusUpdateAuditor(boolean updateStatusById, String status, String id,
			HttpServletRequest httpServletRequest) {
		if (updateStatusById) {
			ProjectStartAppRelTbl projectStartAppRelTbl = this.projectStartAppRelJpaDao.findOne(id);
			if (projectStartAppRelTbl != null) {
				String projectName = projectStartAppRelTbl.getAdminAreaProjectRelId().getProjectId().getName();
				String startAppName = projectStartAppRelTbl.getStartApplicationId().getName();
				AdminHistoryRelationsTbl adminHistoryRelationTbl = this.adminHistoryRelsUtil
						.makeAdminHistoryRelationTbl(httpServletRequest, RELATION_NAME, projectName, startAppName,
								null, null, status, null, null, "ProjectStartApp status update",
								RESULT_SUCCESS);
				this.adminHistoryRelationJpaDao.save(adminHistoryRelationTbl);
			}

		}

	}

	@Override
	public void projectStartAppStatusUpdateFailureAuditor(String id, String status, Exception ex,
			HttpServletRequest httpServletRequest) {
		ProjectStartAppRelTbl projectStartAppRelTbl = this.projectStartAppRelJpaDao.findOne(id);
		if (projectStartAppRelTbl != null) {
			String projectName = projectStartAppRelTbl.getAdminAreaProjectRelId().getProjectId().getName();
			String startAppName = projectStartAppRelTbl.getStartApplicationId().getName();
			String errorMsg = ex.getMessage();
			AdminHistoryRelationsTbl adminHistoryRelationTbl = this.adminHistoryRelsUtil
					.makeAdminHistoryRelationTbl(httpServletRequest, RELATION_NAME, projectName,
							startAppName, null, null, status, null, errorMsg, "ProjectStartApp status update",
							RESULT_FAILURE);
			this.adminHistoryRelationJpaDao.save(adminHistoryRelationTbl);
		}
	}
	
	

}
