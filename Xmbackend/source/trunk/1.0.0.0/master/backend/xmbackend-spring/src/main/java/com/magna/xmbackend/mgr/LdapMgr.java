/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.mgr;

import java.util.List;

import com.magna.xmbackend.vo.ldap.LdapUser;
import com.magna.xmbackend.vo.ldap.LdapUsersResponse;
import com.magna.xmbackend.vo.user.UserCredential;

/**
 *
 * @author dhana
 */
public interface LdapMgr {

    List<String> getAllUserNames();

    List<String> getAllOUNames();
    
    List<LdapUser> getAllLdapUsers();
    
    LdapUser getLdapUserDetails(String userName);
    
    List<String> getAllUserNamesByAnyName(String name);
    
    LdapUsersResponse getAllUsersByAnyName(String name);
    
    String[] isUserValid(UserCredential userCredential);
    
    String[] isUsernameValid(String username);
}
