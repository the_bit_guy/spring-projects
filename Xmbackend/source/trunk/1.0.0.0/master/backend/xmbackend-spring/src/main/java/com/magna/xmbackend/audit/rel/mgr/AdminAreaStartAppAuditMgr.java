/**
 * 
 */
package com.magna.xmbackend.audit.rel.mgr;

import javax.servlet.http.HttpServletRequest;

import com.magna.xmbackend.entities.AdminAreaStartAppRelTbl;
import com.magna.xmbackend.vo.rel.AdminAreaStartAppRelBatchRequest;
import com.magna.xmbackend.vo.rel.AdminAreaStartAppRelBatchResponse;

/**
 * @author Bhabadyuti Bal
 *
 */
public interface AdminAreaStartAppAuditMgr {

	void adminAreaStartAppMultiSaveAuditor(AdminAreaStartAppRelBatchRequest adminAreaStartAppRelBatchRequest,
			AdminAreaStartAppRelBatchResponse aasarbr, HttpServletRequest httpServletRequest);

	void adminAreaStartAppMultiSaveFailureAuditor(HttpServletRequest httpServletRequest, Exception ex);

	void adminAreaStartAppMultiDeleteSuccessAudit(AdminAreaStartAppRelTbl adminAreaStartAppRelTbl,
			HttpServletRequest httpServletRequest);

	void adminAreaProjectAppStatusUpdateAuditor(boolean updateStatusById, String status, String id,
			HttpServletRequest httpServletRequest);

	void adminAreaProjectAppStatusUpdateFailureAuditor(String id, String status, Exception ex,
			HttpServletRequest httpServletRequest);

}
