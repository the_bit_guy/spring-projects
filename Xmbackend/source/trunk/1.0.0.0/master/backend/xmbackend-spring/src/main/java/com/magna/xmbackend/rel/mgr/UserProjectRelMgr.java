package com.magna.xmbackend.rel.mgr;

import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import com.magna.xmbackend.entities.UserProjectRelTbl;
import com.magna.xmbackend.vo.permission.ValidationRequest;
import com.magna.xmbackend.vo.rel.UserProjectRelBatchRequest;
import com.magna.xmbackend.vo.rel.UserProjectRelBatchResponse;
import com.magna.xmbackend.vo.rel.UserProjectRelRequest;
import com.magna.xmbackend.vo.rel.UserProjectRelResponse;

/**
 *
 * @author vijay
 */
public interface UserProjectRelMgr {

    /**
     *
     * @return UserProjectRelResponse
     */
    UserProjectRelResponse findAll(final ValidationRequest validationRequest);

    /**
     *
     * @param id
     * @return UserProjectRelTbl
     */
    UserProjectRelTbl findById(String id);

    /**
     *
     * @param userProjectRelRequest
     * @param userName
     * @return UserProjectRelTbl
     */
    UserProjectRelTbl create(final UserProjectRelRequest userProjectRelRequest,
            final String userName, final boolean isSuperAdmin);

    /**
     *
     * @param userProjectRelBatchRequest
     * @param userName
     * @return UserProjectRelBatchResponse
     */
    UserProjectRelBatchResponse createBatch(final UserProjectRelBatchRequest userProjectRelBatchRequest,
            final String userName);

    /**
     *
     * @param id
     * @return boolean
     */
    //boolean delete(final String id);
    boolean delete(final String id, final String userName);
    
    
   // UserProjectRelResponse multiDelete(final Set<String> ids, HttpServletRequest httpServletRequest);
    UserProjectRelResponse multiDelete(final Set<String> ids, HttpServletRequest httpServletRequest, String userName);

    /**
     *
     * @param status
     * @param id
     * @return boolean
     */
    //boolean updateStatusById(final String status, final String id);
    boolean updateStatusById(final String status, final String id, String userName);

    /**
     *
     * @param userId
     * @return UserProjectRelResponse
     */
    UserProjectRelResponse findUserProjectRelationByUserId(final String userId, final ValidationRequest validationRequest);

    /**
     *
     * @param projectId
     * @return UserProjectRelResponse
     */
    UserProjectRelResponse findUserProjectRelationByProjectId(final String projectId, final ValidationRequest validationRequest);

    /**
     * 
     * @param projectId
     * @return UserProjectRelResponse
     */
    UserProjectRelResponse findUserProjectRelationByProjectId(final String projectId);
    /**
     *
     * @param userId
     * @param projectId
     * @return UserProjectExpiryDaysResponse
     */
    String findProjectExpiryDaysByUserProjectId(final String userId,
            final String projectId, final ValidationRequest validationRequest);

    /**
     * Update project expiry days by user project id.
     *
     * @param userProjRelId the user proj rel id
     * @param expDays the exp days
     * @param userName the user name
     * @return boolean
     */
    /*boolean updateProjectExpiryDaysByUserProjectId(final String userProjRelId,
            final String expDays);*/
    boolean updateProjectExpiryDaysByUserProjectId(final String userProjRelId,
            final String expDays, final String userName);
    
    
    UserProjectRelTbl findUserProjectRelByUserIdProjectId(final String userId,
            final String projectId);

	/*UserProjectRelBatchResponse multiUpdate(List<UserProjectRelRequest> userProjectRelRequests,
			HttpServletRequest httpServletRequest);*/
	UserProjectRelBatchResponse multiUpdate(List<UserProjectRelRequest> userProjectRelRequests,
			HttpServletRequest httpServletRequest, String userName);

}
