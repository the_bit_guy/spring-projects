package com.magna.xmbackend.rel.mgr.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Component;

import com.magna.xmbackend.audit.rel.mgr.AdminAreaProjectAuditMgr;
import com.magna.xmbackend.entities.AdminAreaProjAppRelTbl;
import com.magna.xmbackend.entities.AdminAreaProjectRelTbl;
import com.magna.xmbackend.entities.AdminAreaTranslationTbl;
import com.magna.xmbackend.entities.AdminAreasTbl;
import com.magna.xmbackend.entities.LanguagesTbl;
import com.magna.xmbackend.entities.ProjectApplicationsTbl;
import com.magna.xmbackend.entities.ProjectStartAppRelTbl;
import com.magna.xmbackend.entities.ProjectTranslationTbl;
import com.magna.xmbackend.entities.ProjectsTbl;
import com.magna.xmbackend.entities.SiteAdminAreaRelTbl;
import com.magna.xmbackend.entities.StartApplicationsTbl;
import com.magna.xmbackend.exception.CannotCreateRelationshipException;
import com.magna.xmbackend.exception.XMObjectNotFoundException;
import com.magna.xmbackend.jpa.dao.EmailNotificationEventsJpaDao;
import com.magna.xmbackend.jpa.rel.dao.AdminAreaProjectAppRelJpaDao;
import com.magna.xmbackend.jpa.rel.dao.AdminAreaProjectRelJpaDao;
import com.magna.xmbackend.jpa.rel.dao.SiteAdminAreaRelJpaDao;
import com.magna.xmbackend.mail.mgr.ProjectAARelPreNotification;
import com.magna.xmbackend.mgr.ProjectMgr;
import com.magna.xmbackend.rel.mgr.AdminAreaProjectAppRelMgr;
import com.magna.xmbackend.rel.mgr.AdminAreaProjectRelMgr;
import com.magna.xmbackend.rel.mgr.SiteAdminAreaRelMgr;
import com.magna.xmbackend.response.rel.adminareaproject.AdminAreaProjectRelResponseWrapper;
import com.magna.xmbackend.response.rel.adminareaproject.AdminAreaProjectRelationResponse;
import com.magna.xmbackend.response.rel.adminareaproject.AdminAreaProjectResponse;
import com.magna.xmbackend.response.rel.adminareaproject.AdminAreaProjectResponseWrapper;
import com.magna.xmbackend.utils.MessageMaker;
import com.magna.xmbackend.utils.Validator;
import com.magna.xmbackend.vo.enums.NotificationEventType;
import com.magna.xmbackend.vo.enums.Status;
import com.magna.xmbackend.vo.permission.ValidationRequest;
import com.magna.xmbackend.vo.rel.AdminAreaProjectRelBatchRequest;
import com.magna.xmbackend.vo.rel.AdminAreaProjectRelBatchResponse;
import com.magna.xmbackend.vo.rel.AdminAreaProjectRelIdWithProjectAppRel;
import com.magna.xmbackend.vo.rel.AdminAreaProjectRelIdWithProjectAppRelResponse;
import com.magna.xmbackend.vo.rel.AdminAreaProjectRelIdWithProjectStartAppRel;
import com.magna.xmbackend.vo.rel.AdminAreaProjectRelIdWithProjectStartAppRelResponse;
import com.magna.xmbackend.vo.rel.AdminAreaProjectRelRequest;
import com.magna.xmbackend.vo.rel.AdminAreaProjectRelResponse;

/**
 *
 * @author vijay
 */
@Component
public class AdminAreaProjectRelMgrImpl implements AdminAreaProjectRelMgr {

    private static final Logger LOG
            = LoggerFactory.getLogger(AdminAreaProjectRelMgrImpl.class);

    @Autowired
    private AdminAreaProjectRelJpaDao adminAreaProjectRelJpaDao;

    @Autowired
    private SiteAdminAreaRelJpaDao siteAdminAreaRelJpaDao;

    @Autowired
    private AdminAreaProjectAppRelMgr adminAreaProjectAppRelMgr;

    @Autowired
    private SiteAdminAreaRelMgr siteAdminAreaRelMgr;
    
    @Autowired
    private AdminAreaProjectAppRelJpaDao adminAreaProjectAppRelJpaDao;

    @Autowired
    private ProjectMgr projectMgr;

    @Autowired
    private MessageMaker messageMaker;

    @Autowired
    private ProjectAARelPreNotification projectAARelPreNotification;

    @Autowired
    private Validator validator;
    
    @Autowired
    private AdminAreaProjectAuditMgr adminAreaProjectAuditMgr;
    
    @Autowired
    private EmailNotificationEventsJpaDao eventsJpaDao;


    /**
     *
     * @return AdminAreaProjectAppRelResponse
     */
    @Override
    public final AdminAreaProjectRelResponse findAll() {
        final Iterable<AdminAreaProjectRelTbl> adminAreaProjectRelTbls = adminAreaProjectRelJpaDao.findAll();
        final AdminAreaProjectRelResponse adminAreaProjectRelResponse = new AdminAreaProjectRelResponse(adminAreaProjectRelTbls);
        return adminAreaProjectRelResponse;
    }

    /**
     *
     * @param id
     * @return AdminAreaProjectRelTbl
     */
    @Override
    public final AdminAreaProjectRelTbl findById(String id) {
        LOG.info(">> findById {}", id);
        final AdminAreaProjectRelTbl adminAreaProjectRelTbl = this.adminAreaProjectRelJpaDao.findOne(id);
        LOG.info("<< findById");
        return adminAreaProjectRelTbl;
    }

    /**
     *
     * @param adminAreaProjectRelRequest
     * @param userName
     * @return AdminAreaProjectRelTbl
     */
    @Override
    public final AdminAreaProjectRelTbl create(final AdminAreaProjectRelRequest adminAreaProjectRelRequest,
            final String userName) {
        AdminAreaProjectRelTbl aaparOut = null;
        LOG.info(">> create");
        final Map<String, Object> assignmentAllowedMap
                = isInActiveAssignmentsAllowed(adminAreaProjectRelRequest, userName);
        LOG.info("assignmentAllowedMap={}", assignmentAllowedMap);
        if (assignmentAllowedMap.containsKey("isAssignmentAllowed")) {
            boolean isAssignmentAllowed = (boolean) assignmentAllowedMap.get("isAssignmentAllowed");
            if (isAssignmentAllowed) {
                this.checkIfProjectIdRelated2AAInSiteAARelId(adminAreaProjectRelRequest);
                this.checkIfProjectExistInOtherAAOfSameSite(adminAreaProjectRelRequest);
                final AdminAreaProjectRelTbl aaparIn = this.convert2Entity(adminAreaProjectRelRequest);
                aaparOut = adminAreaProjectRelJpaDao.save(aaparIn);
                //send mail
                String staatus = this.eventsJpaDao.findStatusByEvent(NotificationEventType.PROJECT_ADMIN_AREA_RELATION_ASSIGN.name());
                if (staatus.equals(Status.ACTIVE.name())) {
                	this.projectAARelPreNotification.postToQueue(aaparOut, NotificationEventType.PROJECT_ADMIN_AREA_RELATION_ASSIGN.name());;
                }
				
            } else {
                if (assignmentAllowedMap.containsKey("projectsTbl")) {
                    final Map<String, String> adminAreaTransMap
                            = messageMaker.getAdminAreaNames((AdminAreasTbl) assignmentAllowedMap.get("adminAreasTbl"));
                    final Map<String, String> projectTransMap
                            = messageMaker.getProjectNames((ProjectsTbl) assignmentAllowedMap.get("projectsTbl"));
                    final Map<String, String[]> paramMap = messageMaker.geti18nCodeMap(adminAreaTransMap, projectTransMap);
                    LOG.debug("paramMap={}", paramMap);
                    throw new CannotCreateRelationshipException("Inactive Assignments not allowed", "AAP_ERR0001", paramMap);
                }
            }
        }
        LOG.info("<< create");
        return aaparOut;
    }

    /**
     *
     * @param userStartAppRelRequest
     * @param userName
     * @return Map
     */
    private Map<String, Object> isInActiveAssignmentsAllowed(final AdminAreaProjectRelRequest adminAreaProjectRelRequest,
            final String userName) {
        final boolean isViewInactiveAllowed = isViewInActiveAllowed(userName);
        LOG.debug("isViewInactiveAllowed={}", isViewInactiveAllowed);
        final Map<String, Object> assignmentAllowedMap
                = validator.isProjectAdminAreaInactiveAssignmentAllowed(adminAreaProjectRelRequest,
                        isViewInactiveAllowed);
        return assignmentAllowedMap;
    }

    /**
     *
     * @param userName
     * @return boolean
     */
    private boolean isViewInActiveAllowed(final String userName) {
        final ValidationRequest validationRequest
                = validator.formSaveRelationValidationRequest(userName, "PROJECT_ADMINAREA");
        LOG.info("validationRequest={}", validationRequest);
        final boolean isInactiveAssignment = validator.isViewInactiveAllowed(validationRequest);
        LOG.info("isInactiveAssignment={}", isInactiveAssignment);
        return isInactiveAssignment;
    }

    /**
     *
     * @param adminAreaProjectRelRequest
     */
    private void checkIfProjectExistInOtherAAOfSameSite(final AdminAreaProjectRelRequest adminAreaProjectRelRequest) {
        LOG.debug(">> checkIfProjectExistInOtherAAOfSameSite");
        final String siteAdminAreaRelIdIn = adminAreaProjectRelRequest.getSiteAdminAreaRelId();
        final String projectIdIn = adminAreaProjectRelRequest.getProjectId();
        List<AdminAreaProjectRelTbl> adminAreaProjectRelTbls = this.adminAreaProjectRelJpaDao
                .findByProjectId(new ProjectsTbl(projectIdIn));
        SiteAdminAreaRelTbl siteAdminAreaRelTbl = siteAdminAreaRelMgr.findById(siteAdminAreaRelIdIn);
        String siteId = siteAdminAreaRelTbl.getSiteId().getSiteId();
        if (siteId != null) {
            for (AdminAreaProjectRelTbl areaProjectRelTbl : adminAreaProjectRelTbls) {
                if (siteId.equals(areaProjectRelTbl.getSiteAdminAreaRelId().getSiteId().getSiteId())) {
                    final Map<String, String[]> paramMap = this.getProjectNameById(projectIdIn);
                    LOG.debug("paramMap={}", paramMap);
                    throw new CannotCreateRelationshipException("Realtionship exists", "ERR0004", paramMap);
                }
            }
        }
        LOG.debug("<< checkIfProjectExistInOtherAAOfSameSite");
    }

    /**
     * Project can be assigned to only one administration area per site
     *
     * @param adminAreaProjectRelRequest
     */
    private void checkIfProjectIdRelated2AAInSiteAARelId(final AdminAreaProjectRelRequest adminAreaProjectRelRequest) {
        LOG.debug(">> isAdminAreaProjectRelCreationValid");
        final String siteAdminAreaRelIdIn = adminAreaProjectRelRequest.getSiteAdminAreaRelId();
        final String projectIdIn = adminAreaProjectRelRequest.getProjectId();
        final SiteAdminAreaRelTbl siteAdminAreaRelTbl = this.siteAdminAreaRelJpaDao.findOne(siteAdminAreaRelIdIn);
        final AdminAreasTbl adminAreasTbl = siteAdminAreaRelTbl.getAdminAreaId();
        final Map<String, String> adminAreaTransMap = getAdminAreaTransNames(adminAreasTbl);

        final Collection<AdminAreaProjectRelTbl> adminAreaProjectRelTblCollection = siteAdminAreaRelTbl
                .getAdminAreaProjectRelTblCollection();
        for (AdminAreaProjectRelTbl adminAreaProjectRelTbl : adminAreaProjectRelTblCollection) {
            final ProjectsTbl projectsTbl = adminAreaProjectRelTbl.getProjectId();
            final String projectIdGotFromTbl = projectsTbl.getProjectId();
            if (projectIdGotFromTbl.equalsIgnoreCase(projectIdIn)) {
                // already project id is assigned to one of the adminAreas in
                // the site rel
                // throw ex
                // get the projectName for the paramMap formation(both in en and
                // de)
                // final Map<String, String[]> paramMap =
                // this.getProjectNameById(projectIdIn); //commented
                final Map<String, String[]> paramMap = getProjectNameAndAAName(projectIdIn, adminAreaTransMap);
                LOG.debug("paramMap={}", paramMap);
                // throw new CannotCreateRelationshipException("Realtionship
                // exists", "ERR0004", paramMap);
                throw new CannotCreateRelationshipException("Realtionship exists", "ERR0013", paramMap);
            }
        }
        LOG.debug("<< isAdminAreaProjectRelCreationValid");
    }

    /**
     *
     * @param projectId
     * @param aaTransMap
     * @return Map
     */
    private Map<String, String[]> getProjectNameAndAAName(final String projectId,
            final Map<String, String> aaTransMap) {
        LOG.info(">> getProjectNameAndAAName ");
        final Map<String, String[]> paramMap = new HashMap<>();
        final ProjectsTbl projectsTbl = this.projectMgr.findById(projectId);
        final Collection<ProjectTranslationTbl> projectTranslationTblCollection = projectsTbl
                .getProjectTranslationTblCollection();
        final String name = projectsTbl.getName();
        for (ProjectTranslationTbl projectTranslationTbl : projectTranslationTblCollection) {
            final LanguagesTbl languagesTbl = projectTranslationTbl.getLanguageCode();
            final String languageCode = languagesTbl.getLanguageCode();
            final String adminAreaName = aaTransMap.get(languageCode);
            final String[] params = {"'" + name + "'", "'" + adminAreaName + "'"};
            paramMap.put(languageCode, params);
        }
        LOG.info("<< getProjectNameAndAAName ");
        return paramMap;
    }

    /**
     *
     * @param adminAreasTbl
     * @return Map
     */
    private Map<String, String> getAdminAreaTransNames(AdminAreasTbl adminAreasTbl) {
        final Map<String, String> projectTransMap = new HashMap<>();
        final String adminAreaName = adminAreasTbl.getName();
        final Collection<AdminAreaTranslationTbl> adminAreaTranslationTbls = adminAreasTbl.getAdminAreaTranslationTblCollection();
        for (AdminAreaTranslationTbl adminAreaTranslationTbl : adminAreaTranslationTbls) {
            final String languageCode = adminAreaTranslationTbl.getLanguageCode().getLanguageCode();
            projectTransMap.put(languageCode.toLowerCase(), adminAreaName);
        }
        return projectTransMap;
    }

    /**
     *
     * @param projectId
     * @return Map
     */
    private Map<String, String[]> getProjectNameById(final String projectId) {
        final Map<String, String[]> paramMap = new HashMap<>();
        final ProjectsTbl projectsTbl = this.projectMgr.findById(projectId);
        final String name = projectsTbl.getName();
        final Collection<ProjectTranslationTbl> projectTranslationTblCollection = projectsTbl
                .getProjectTranslationTblCollection();
        for (ProjectTranslationTbl projectTranslationTbl : projectTranslationTblCollection) {
            final LanguagesTbl languagesTbl = projectTranslationTbl.getLanguageCode();
            final String languageCode = languagesTbl.getLanguageCode();
            final String[] params = {name};
            paramMap.put(languageCode, params);
        }
        return paramMap;
    }

    /**
     *
     * @param status
     * @param id
     * @return boolean
     */
    @Override
    public final boolean updateStatusById(final String status,
            final String id, HttpServletRequest httpServletRequest) {
        LOG.info(">> updateStatusById");
        boolean isUpdated = false;
        final int out = this.adminAreaProjectRelJpaDao.setStatusForAdminAreaProjectRelTbl(status, id);
        LOG.debug("is Modified status value {}", out);
        if (out > 0) {
            isUpdated = true;
            this.adminAreaProjectAuditMgr.adminAreaProjectStatusUpdateAuditor(isUpdated, status, id, httpServletRequest);
        }
        LOG.info("<< updateStatusById");
        return isUpdated;
    }

    /**
     *
     * @param id
     * @return boolean
     */
    @Override
	public final boolean delete(final String id) {
		LOG.info(">> delete {}", id);
		AdminAreaProjectRelTbl adminAreaProjectRelTbl = this.findById(id);
		boolean isDeleted = false;
		try {
			this.adminAreaProjectRelJpaDao.delete(id);
			isDeleted = true;
		} catch (Exception e) {
			if (e instanceof EmptyResultDataAccessException) {
				final String[] param = { id };
				throw new XMObjectNotFoundException("User not found", "P_ERR0030", param);
			}
		}
		// send mail
		String staatus = this.eventsJpaDao.findStatusByEvent(NotificationEventType.PROJECT_ADMIN_AREA_RELATION_REMOVE.name());
		if (staatus.equals(Status.ACTIVE.name())) {
			this.projectAARelPreNotification.postToQueue(adminAreaProjectRelTbl, NotificationEventType.PROJECT_ADMIN_AREA_RELATION_REMOVE.name());
        }
		
		LOG.info("<< deleteById");
		return isDeleted;
	}

    @Override
    public AdminAreaProjectRelResponse multiDelete(Set<String> ids, HttpServletRequest httpServletRequest) {
        LOG.info(">> multiDelete");
        final List<Map<String, String>> statusMaps = new ArrayList<>();
        ids.forEach(id -> {
        	AdminAreaProjectRelTbl adminAreaProjectRelTbl = this.adminAreaProjectRelJpaDao.findOne(id);
            try {
            		this.delete(id);
            		this.adminAreaProjectAuditMgr.userProjectMultiDeleteSuccessAudit(adminAreaProjectRelTbl, httpServletRequest);
            } catch (XMObjectNotFoundException objectNotFound) {
                Map<String, String> statusMap = messageMaker.extractFromException(objectNotFound);
                statusMaps.add(statusMap);
            }
        });
        AdminAreaProjectRelResponse relResponse = new AdminAreaProjectRelResponse(statusMaps);
        LOG.info(">> multiDelete");
        return relResponse;
    }

    /**
     *
     * @param adminAreaProjectRelRequest
     * @return AdminAreaProjectRelTbl
     */
    private AdminAreaProjectRelTbl convert2Entity(final AdminAreaProjectRelRequest adminAreaProjectRelRequest) {
        final String id = UUID.randomUUID().toString();
        final String status = adminAreaProjectRelRequest.getStatus();
        final String siteAdminAreaRelId = adminAreaProjectRelRequest.getSiteAdminAreaRelId();
        final String projectId = adminAreaProjectRelRequest.getProjectId();
        final AdminAreaProjectRelTbl adminAreaProjectRelTbl = new AdminAreaProjectRelTbl(id);
        adminAreaProjectRelTbl.setSiteAdminAreaRelId(new SiteAdminAreaRelTbl(siteAdminAreaRelId));
        adminAreaProjectRelTbl.setStatus(status);
        adminAreaProjectRelTbl.setProjectId(new ProjectsTbl(projectId));
        return adminAreaProjectRelTbl;
    }

    /**
     *
     * @param siteAdminAreaRelTbl
     * @return AdminAreaProjectRelTbl
     */
    @Override
    public final Iterable<AdminAreaProjectRelTbl> findAdminAreaProjectRelTblsBySiteAdminAreaRelId(final SiteAdminAreaRelTbl siteAdminAreaRelTbl) {
        LOG.info(">> findAdminAreaProjectRelTblsBySiteAdminAreaRelId {}", siteAdminAreaRelTbl);
        final Iterable<AdminAreaProjectRelTbl> aaprts = this.adminAreaProjectRelJpaDao.getAdminAreaProjectRelTblsBySiteAdminAreaRelId(siteAdminAreaRelTbl);
        LOG.info("<< findAdminAreaProjectRelTblsBySiteAdminAreaRelId");
        return aaprts;
    }

    /**
     *
     * @param id
     * @param validationRequest
     * @return AdminAreaProjectRelIdWithProjectAppRelResponse
     */
    @Override
    public AdminAreaProjectRelIdWithProjectAppRelResponse
            findAllProjectAppsByAdminAreaProjectRelId(final String id,
                    final ValidationRequest validationRequest) {
        LOG.info(">> findAllProjectAppsByAdminAreaProjectRelId {}", id);
        final boolean isViewInactive = validator.isViewInactiveAllowed(validationRequest);
        LOG.info("findAllProjectAppsByAdminAreaProjectRelId isViewInactive={}", isViewInactive);
        final AdminAreaProjectRelTbl adminAreaProjectRelTbl = validator.validateAdminAreaProject(id, isViewInactive);
        validator.validateProject(adminAreaProjectRelTbl.getProjectId().getProjectId(), isViewInactive);
        SiteAdminAreaRelTbl siteAdminAreaRelTbl = adminAreaProjectRelTbl.getSiteAdminAreaRelId();
        validator.validateSiteAdminArea(siteAdminAreaRelTbl.getSiteAdminAreaRelId(), isViewInactive);
        validator.validateAdminArea(siteAdminAreaRelTbl.getAdminAreaId().getAdminAreaId(), isViewInactive);
        validator.validateSite(siteAdminAreaRelTbl.getSiteId().getSiteId(), isViewInactive);
        final AdminAreaProjectRelIdWithProjectAppRelResponse response = this.getAllProjectApps(adminAreaProjectRelTbl, isViewInactive);
        LOG.info(">> findAllProjectAppsByAdminAreaProjectRelId");
        return response;
    }

    /**
     *
     * @param adminAreaProjectRelTbl
     * @param isViewInActive
     * @return AdminAreaProjectRelIdWithProjectAppRelResponse
     */
    private AdminAreaProjectRelIdWithProjectAppRelResponse getAllProjectApps(final AdminAreaProjectRelTbl adminAreaProjectRelTbl,
            final boolean isViewInActive) {
        Collection<AdminAreaProjAppRelTbl> adminAreaProjAppRelTbls
                = adminAreaProjectRelTbl.getAdminAreaProjAppRelTblCollection();
        if (adminAreaProjAppRelTbls.isEmpty()) {
            throw new XMObjectNotFoundException("Admin Area Project App Relation not found", "ERR0003");
        }
        validateAdminAreaProjectRel(adminAreaProjectRelTbl, isViewInActive);
        adminAreaProjAppRelTbls = validator.filterAdminAreaProjAppRel(isViewInActive, adminAreaProjAppRelTbls);
        final List<AdminAreaProjectRelIdWithProjectAppRel> adminAreaProjectRelIdWithProjectAppRels = new ArrayList<>();
        for (final AdminAreaProjAppRelTbl adminAreaProjAppRelTbl : adminAreaProjAppRelTbls) {
        	final ProjectApplicationsTbl projectApplicationsTbl = adminAreaProjAppRelTbl.getProjectApplicationId();
        	final ProjectApplicationsTbl filterProjectApplicationTbl = validator.filterProjectApplicationResponse(isViewInActive, projectApplicationsTbl);
        	if (null != filterProjectApplicationTbl) {
        		final AdminAreaProjectRelIdWithProjectAppRel aapriwpar = new AdminAreaProjectRelIdWithProjectAppRel();
                aapriwpar.setAdminAreaProjectAppRelId(adminAreaProjAppRelTbl.getAdminAreaProjAppRelId());
                aapriwpar.setAdminAreaProjectRelId(adminAreaProjAppRelTbl.getAdminAreaProjectRelId());
                aapriwpar.setProjectApplicationsTbl(filterProjectApplicationTbl);
                aapriwpar.setStatus(adminAreaProjAppRelTbl.getStatus());
                aapriwpar.setRelationType(adminAreaProjAppRelTbl.getRelType());
                adminAreaProjectRelIdWithProjectAppRels.add(aapriwpar);
        	}
        }
        final AdminAreaProjectRelIdWithProjectAppRelResponse saariwaasarr = new AdminAreaProjectRelIdWithProjectAppRelResponse();
        saariwaasarr.setAdminAreaProjectRelIdWithProjectAppRels(adminAreaProjectRelIdWithProjectAppRels);
        return saariwaasarr;
    }

    /**
     *
     * @param id
     * @param relType
     * @param validationRequest
     * @return SiteAdminAreaRelIdWithAdminAreaUsrAppRelResponse
     */
    @Override
    public AdminAreaProjectRelIdWithProjectAppRelResponse
            findAllProjectAppsByAdminAreaProjectRelId(final String id,
                    final String relType,
                    final ValidationRequest validationRequest) {
        LOG.info(">> findAllProjectAppsByAdminAreaProjectRelId {} {}", id, relType);
        final boolean isInactiveAssignment = validator.isViewInactiveAllowed(validationRequest);
        LOG.info("findAllProjectAppsByAdminAreaProjectRelId isInactiveAssignment={}", isInactiveAssignment);
        final AdminAreaProjectRelTbl adminAreaProjectRelTbl = validator.validateAdminAreaProject(id, isInactiveAssignment);
        final AdminAreaProjectRelIdWithProjectAppRelResponse response
                = this.getAllProjectApps(adminAreaProjectRelTbl, relType, isInactiveAssignment);
        LOG.info(">> findAllProjectAppsByAdminAreaProjectRelId");
        return response;
    }

    /**
     *
     * @param adminAreaProjectRelTbl
     * @param relType
     * @param isViewInActive
     * @return AdminAreaProjectRelIdWithProjectAppRelResponse
     */
    private AdminAreaProjectRelIdWithProjectAppRelResponse getAllProjectApps(final AdminAreaProjectRelTbl adminAreaProjectRelTbl,
            final String relType, final boolean isViewInActive) {
        Iterable<AdminAreaProjAppRelTbl> adminAreaProjAppRelTbls
                = adminAreaProjectAppRelMgr.getAdminAreaProjAppOnRelType(adminAreaProjectRelTbl, relType);
        adminAreaProjAppRelTbls = validator.filterAdminAreaProjAppRel(isViewInActive, adminAreaProjAppRelTbls);
        validateAdminAreaProjectRel(adminAreaProjectRelTbl, isViewInActive);
        final List<AdminAreaProjectRelIdWithProjectAppRel> adminAreaProjectRelIdWithProjectAppRels = new ArrayList<>();
        for (final AdminAreaProjAppRelTbl adminAreaProjAppRelTbl : adminAreaProjAppRelTbls) {
        	final ProjectApplicationsTbl projectApplicationsTbl = adminAreaProjAppRelTbl.getProjectApplicationId();
        	final ProjectApplicationsTbl filterProjectApplicationTbl = validator.filterProjectApplicationResponse(isViewInActive, projectApplicationsTbl);
        	if (null != filterProjectApplicationTbl) {
        		final AdminAreaProjectRelIdWithProjectAppRel aapriwpar = new AdminAreaProjectRelIdWithProjectAppRel();
                aapriwpar.setAdminAreaProjectAppRelId(adminAreaProjAppRelTbl.getAdminAreaProjAppRelId());
                aapriwpar.setAdminAreaProjectRelId(adminAreaProjAppRelTbl.getAdminAreaProjectRelId());
    			aapriwpar.setProjectApplicationsTbl(filterProjectApplicationTbl);
                aapriwpar.setStatus(adminAreaProjAppRelTbl.getStatus());
                aapriwpar.setRelationType(adminAreaProjAppRelTbl.getRelType());
                adminAreaProjectRelIdWithProjectAppRels.add(aapriwpar);
        	}
        }
        if (adminAreaProjectRelIdWithProjectAppRels.isEmpty()) {
            throw new XMObjectNotFoundException("Admin Area Project App Relation not found", "ERR0003");
        }
        final AdminAreaProjectRelIdWithProjectAppRelResponse aapriwparr = new AdminAreaProjectRelIdWithProjectAppRelResponse();
        aapriwparr.setAdminAreaProjectRelIdWithProjectAppRels(adminAreaProjectRelIdWithProjectAppRels);
        return aapriwparr;
    }

    /**
     *
     * @param id
     * @param validationRequest
     * @return AdminAreaProjectRelIdWithProjectStartAppRelResponse
     */
    @Override
    public AdminAreaProjectRelIdWithProjectStartAppRelResponse
            findAllStartAppsByAdminAreaProjectRelId(final String id,
                    final ValidationRequest validationRequest) {
        LOG.info(">> findAllStartAppsByAdminAreaProjectRelId {}", id);
        final boolean isViewInActive = validator.isViewInactiveAllowed(validationRequest);
        LOG.info("findAllStartAppsByAdminAreaProjectRelId isInactiveAssignment={}", isViewInActive);
        final AdminAreaProjectRelTbl adminAreaProjectRelTbl = validator.validateAdminAreaProject(id, isViewInActive);
        validator.validateProject(adminAreaProjectRelTbl.getProjectId().getProjectId(), isViewInActive);
        final AdminAreaProjectRelIdWithProjectStartAppRelResponse response
                = this.getAllStartApps(adminAreaProjectRelTbl, isViewInActive);
        LOG.info(">> findAllStartAppsByAdminAreaProjectRelId");
        return response;
    }

    /**
     *
     * @param adminAreaProjectRelTbl
     * @param isViewInActive
     * @return AdminAreaProjectRelIdWithProjectStartAppRelResponse
     */
    private AdminAreaProjectRelIdWithProjectStartAppRelResponse getAllStartApps(final AdminAreaProjectRelTbl adminAreaProjectRelTbl,
            final boolean isViewInActive) {
        Collection<ProjectStartAppRelTbl> projectStartAppRelTbls
                = adminAreaProjectRelTbl.getProjectStartAppRelTblCollection();
        projectStartAppRelTbls = validator.filterProjectStartAppRel(isViewInActive, projectStartAppRelTbls);
        if (projectStartAppRelTbls.isEmpty()) {
            throw new XMObjectNotFoundException("No Admin Area Project Relation Found", "AA_ERR0009");
        }
        validateAdminAreaProjectRel(adminAreaProjectRelTbl, isViewInActive);
        final List<AdminAreaProjectRelIdWithProjectStartAppRel> adminAreaProjectRelIdWithProjectStartAppRel = new ArrayList<>();
        for (final ProjectStartAppRelTbl projectStartAppRelTbl : projectStartAppRelTbls) {
        	final StartApplicationsTbl startApplicationsTbl = projectStartAppRelTbl.getStartApplicationId();
        	final StartApplicationsTbl filterStartApplicationTbl = validator.filterStartApplicationResponse(isViewInActive, startApplicationsTbl);
        	if (null != filterStartApplicationTbl) {
        		final AdminAreaProjectRelIdWithProjectStartAppRel aapriwpsar = new AdminAreaProjectRelIdWithProjectStartAppRel();
                aapriwpsar.setAdminAreaProjectRelId(projectStartAppRelTbl.getAdminAreaProjectRelId());
                aapriwpsar.setProjectStartAppRelId(projectStartAppRelTbl.getProjectStartAppRelId());
    			aapriwpsar.setStartApplicationsTbl(filterStartApplicationTbl);
                aapriwpsar.setStatus(projectStartAppRelTbl.getStatus());
                adminAreaProjectRelIdWithProjectStartAppRel.add(aapriwpsar);
        	}
        }
        final AdminAreaProjectRelIdWithProjectStartAppRelResponse aapriwpsarr = new AdminAreaProjectRelIdWithProjectStartAppRelResponse();
        aapriwpsarr.setAdminAreaProjectRelIdWithProjectStartAppRels(adminAreaProjectRelIdWithProjectStartAppRel);
        return aapriwpsarr;
    }

    /**
     *
     * @param adminAreaProjectRelBatchRequest
     * @return AdminAreaProjectRelBatchResponse
     */
    @Override
    public final AdminAreaProjectRelBatchResponse createBatch(final AdminAreaProjectRelBatchRequest adminAreaProjectRelBatchRequest,
            final String userName) {
        final List<AdminAreaProjectRelTbl> adminAreaProjectRelTbls = new ArrayList<>();
        final List<Map<String, String>> statusMaps = new ArrayList<>();
        final List<AdminAreaProjectRelRequest> adminAreaProjectRelRequests
                = adminAreaProjectRelBatchRequest.getAdminAreaProjectRelRequests();
        for (final AdminAreaProjectRelRequest adminAreaProjectRelRequest : adminAreaProjectRelRequests) {
            try {
                final AdminAreaProjectRelTbl aaprtOut = this.create(adminAreaProjectRelRequest, userName);
                adminAreaProjectRelTbls.add(aaprtOut);
            } catch (CannotCreateRelationshipException ccre) {
                final Map<String, String> statusMap = messageMaker.extractFromException(ccre);
                statusMaps.add(statusMap);
            }
        }
        final AdminAreaProjectRelBatchResponse adminAreaProjectRelBatchResponse
                = new AdminAreaProjectRelBatchResponse(adminAreaProjectRelTbls, statusMaps);
        return adminAreaProjectRelBatchResponse;
    }

    /**
     *
     * @param projectId
     * @param validationRequest
     * @return AdminAreaProjectResponse
     */
    @Override
    public List<AdminAreaProjectResponse> findByProjectId(final String projectId,
            final ValidationRequest validationRequest) {
        List<AdminAreaProjectResponse> adminAreaProjectResponsees = new ArrayList<>();
        LOG.info(">> findByProjectId {}", projectId);
        final boolean isInactiveAssignment = validator.isViewInactiveAllowed(validationRequest);
        LOG.info("findByProjectId isInactiveAssignment={}", isInactiveAssignment);
        final ProjectsTbl projectsTbl = validator.validateProject(projectId, isInactiveAssignment);
        Collection<AdminAreaProjectRelTbl> adminAreaProjectRelTblCollection = projectsTbl.getAdminAreaProjectRelTblCollection();
        adminAreaProjectRelTblCollection = validator.filterAdminAreaProjectRel(isInactiveAssignment, adminAreaProjectRelTblCollection);
        if (adminAreaProjectRelTblCollection.isEmpty()) {
            throw new XMObjectNotFoundException("No Admin Area Project Relation Found", "ERR0003");
        }
        for (AdminAreaProjectRelTbl adminAreaProjectRelTbl : adminAreaProjectRelTblCollection) {
            try {
                validateAdminAreaProjectRel(adminAreaProjectRelTbl, isInactiveAssignment);
                final SiteAdminAreaRelTbl siteAdminAreaRelTbl = adminAreaProjectRelTbl.getSiteAdminAreaRelId();
                final AdminAreasTbl adminAreasTbl = siteAdminAreaRelTbl.getAdminAreaId();
                adminAreaProjectResponsees.add(new AdminAreaProjectResponse(adminAreaProjectRelTbl, adminAreasTbl));
            } catch (XMObjectNotFoundException e) {
                LOG.info("XMObjectNotFoundException", e);
            }
        }
        LOG.info("<< findByProjectId");
        return adminAreaProjectResponsees;
    }

    /**
     *
     * @param adminAreaProjectRelTbl
     * @param isViewInactive
     */
    private void validateAdminAreaProjectRel(final AdminAreaProjectRelTbl adminAreaProjectRelTbl,
            final boolean isViewInactive) {
        final String siteAdminAreaRelId = adminAreaProjectRelTbl.getSiteAdminAreaRelId().getSiteAdminAreaRelId();
        final String siteId = adminAreaProjectRelTbl.getSiteAdminAreaRelId().getSiteId().getSiteId();
        final String adminAreaId = adminAreaProjectRelTbl.getSiteAdminAreaRelId().getAdminAreaId().getAdminAreaId();
        validator.validateSiteAdminArea(siteAdminAreaRelId, isViewInactive);
        validator.validateSite(siteId, isViewInactive);
        validator.validateAdminArea(adminAreaId, isViewInactive);
    }

    /**
     *
     * @param siteAdminAreaProjId
     * @param projAppId
     * @param validationRequest
     * @return AdminAreaProjectRelResponseWrapper
     */
    @Override
    public AdminAreaProjectRelResponseWrapper findBySAARProjectAppId(final String siteAdminAreaProjId,
            final String projAppId, final ValidationRequest validationRequest) {
        final boolean isInactiveAssignment = validator.isViewInactiveAllowed(validationRequest);
        LOG.info("findBySAARProjectAppId isInactiveAssignment={}", isInactiveAssignment);
        final SiteAdminAreaRelTbl siteAdminAreaRelTbl = validator.validateSiteAdminArea(siteAdminAreaProjId, isInactiveAssignment);
        final ProjectApplicationsTbl projectApplicationsTbl = validator.validateProjectApp(projAppId, isInactiveAssignment);
        LOG.info("siteAdminAreaRelTbl={} projectApplicationsTbl={}",
                siteAdminAreaRelTbl, projectApplicationsTbl);
        List<AdminAreaProjectRelTbl> adminAreaProjectRelTbls
                = this.adminAreaProjectRelJpaDao.findBySiteAdminAreaRelIdAndProjectAppId(siteAdminAreaProjId, projAppId);
        adminAreaProjectRelTbls = validator.filterAdminAreaProjectRel(isInactiveAssignment, adminAreaProjectRelTbls);
        if (adminAreaProjectRelTbls.isEmpty()) {
            throw new XMObjectNotFoundException("No Admin Area Project Relation Found", "ERR0003");
        }
        final List<AdminAreaProjectRelationResponse> adminAreaProjRelResps = new ArrayList<>();
        for (final AdminAreaProjectRelTbl adminAreaProjectRelTbl : adminAreaProjectRelTbls) {
            try {
                validateAdminAreaProjectRel(adminAreaProjectRelTbl, isInactiveAssignment);
                final AdminAreaProjectRelationResponse aaprr = new AdminAreaProjectRelationResponse();
                final String projectId = adminAreaProjectRelTbl.getProjectId().getProjectId();
                validator.validateProject(projectId, isInactiveAssignment);
                aaprr.setAdminAreaProjectRelId(adminAreaProjectRelTbl.getAdminAreaProjectRelId());
                AdminAreaProjAppRelTbl adminAreaProjAppRelTbl = this.adminAreaProjectAppRelJpaDao.findByAdminAreaProjectRelIdAndProjectApplicationId(adminAreaProjectRelTbl, projectApplicationsTbl);
                aaprr.setStatus(adminAreaProjAppRelTbl.getStatus());
                aaprr.setProjectId(adminAreaProjectRelTbl.getProjectId());
                aaprr.setSiteAdminAreaRelId(adminAreaProjectRelTbl.getSiteAdminAreaRelId());
                adminAreaProjRelResps.add(aaprr);
            } catch (XMObjectNotFoundException e) {
                LOG.info("XMObjectNotFoundException", e);
            }
        }
        final AdminAreaProjectRelResponseWrapper adminAreaProjectRelResponseWrapper = new AdminAreaProjectRelResponseWrapper(adminAreaProjRelResps);
        return adminAreaProjectRelResponseWrapper;
    }

    /**
     *
     * @param siteAdminAreaRelId
     * @param projectId
     * @param validationRequest
     * @return AdminAreaProjectRelTbl
     */
    @Override
    public AdminAreaProjectRelTbl findBySiteAdminAreaRelIdAndProjectId(final String siteAdminAreaRelId,
            final String projectId, final ValidationRequest validationRequest) {
        AdminAreaProjectRelTbl adminAreaProjectRelTbl = null;
        LOG.info("<< findBySiteAdminAreaRelIdAndProjectId");
        final boolean isInactiveAssignment = validator.isViewInactiveAllowed(validationRequest);
        LOG.info("findBySiteAdminAreaRelIdAndProjectId isInactiveAssignment={}", isInactiveAssignment);
        final SiteAdminAreaRelTbl siteAdminAreaRelTbl = validator.validateSiteAdminArea(siteAdminAreaRelId, isInactiveAssignment);
        final String siteId = siteAdminAreaRelTbl.getSiteId().getSiteId();
        final String adminAreaId = siteAdminAreaRelTbl.getAdminAreaId().getAdminAreaId();
        validator.validateSite(siteId, isInactiveAssignment);
        validator.validateAdminArea(adminAreaId, isInactiveAssignment);
        final ProjectsTbl projectsTbl = validator.validateProject(projectId, isInactiveAssignment);
        adminAreaProjectRelTbl = adminAreaProjectRelJpaDao.findBySiteAdminAreaRelIdAndProjectId(siteAdminAreaRelTbl, projectsTbl);
        if (null != adminAreaProjectRelTbl) {
            adminAreaProjectRelTbl = validator.filterAdminAreaProjectRel(isInactiveAssignment, adminAreaProjectRelTbl);
            if (null == adminAreaProjectRelTbl) {
                throw new XMObjectNotFoundException("No Active Admin Area Project Relation Found", "ERR0003");
            }
        } else {
            throw new XMObjectNotFoundException("No Admin Area Project Relation Found", "ERR0003");
        }
        LOG.info(">> findBySiteAdminAreaRelIdAndProjectId");
        return adminAreaProjectRelTbl;
    }

    /**
     *
     * @param siteAdminAreaRelId
     * @param projectId
     * @return AdminAreaProjectRelTbl
     */
    @Override
    public AdminAreaProjectRelTbl findBySiteAdminAreaRelIdAndProjectId(final String siteAdminAreaRelId,
            final String projectId) {
        LOG.info("<< findBySiteAdminAreaRelIdAndProjectId");
        final AdminAreaProjectRelTbl adminAreaProjectRelTbl
                = findBySiteAdminAreaRelIdAndProjectId(siteAdminAreaRelId,
                        projectId, null);
        LOG.info(">> findBySiteAdminAreaRelIdAndProjectId");
        return adminAreaProjectRelTbl;
    }

	@Override
	public AdminAreaProjectResponseWrapper multiUpdate(List<AdminAreaProjectRelRequest> adminAreaProjRequests,
			HttpServletRequest httpServletRequest) {
		LOG.info(">> multiUpdate");
		List<String> relIds = new ArrayList<>();
		adminAreaProjRequests.forEach(adminAreaProjRequest -> {
			String id = adminAreaProjRequest.getId();
			String status = adminAreaProjRequest.getStatus();
			try {
				boolean updateStatusById = this.updateStatusById(status, id, httpServletRequest);
				if (updateStatusById) {
					// success & audit it
					this.adminAreaProjectAuditMgr.adminAreaProjectStatusUpdateAuditor(updateStatusById, status, id, httpServletRequest);
				} else {
					relIds.add(id);
				}
			} catch (Exception ex) {
				this.adminAreaProjectAuditMgr.adminAreaProjectStatusUpdateFailureAuditor(id, status, ex, httpServletRequest);
			}

		});
		AdminAreaProjectResponseWrapper adminAreaProjectRelResponse = new AdminAreaProjectResponseWrapper();
		adminAreaProjectRelResponse.setStatusUpdatationFailedList(relIds);
		LOG.info("<< multiUpdate");
		return adminAreaProjectRelResponse;
	}

}
