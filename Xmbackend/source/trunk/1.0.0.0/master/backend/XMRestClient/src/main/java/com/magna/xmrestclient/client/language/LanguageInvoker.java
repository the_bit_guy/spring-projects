/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmrestclient.client.language;

import com.magna.xmbackend.vo.icon.Icon;
import com.magna.xmbackend.vo.icon.IconResponse;
import com.magna.xmbackend.vo.language.Language;
import com.magna.xmbackend.vo.language.LanguageResponse;
import java.util.List;
import javax.ws.rs.ClientErrorException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author dhana
 */
public class LanguageInvoker {

    private final WebTarget webTarget;
    private final Client client;
    private static final String BASE_URI
            = "http://localhost:8888/XMBackend-1.0.0.M2";

    public LanguageInvoker() {
        client = javax.ws.rs.client.ClientBuilder.newClient();
        webTarget = client.target(BASE_URI).path("language");
    }

    public void getAllLanguages() throws ClientErrorException {
        Response response = webTarget.path("fetchAll")
                .request().header("Content-Type", MediaType.APPLICATION_JSON)
                .get();
        int status = response.getStatus();
        System.out.println("before status check::" + status);
        if (status == 200) {
            LanguageResponse languageResponse = response.readEntity(LanguageResponse.class);
            List<Language> languages = languageResponse.getLanguages();
            for (Language language : languages) {
                String languageCode = language.getLanguageCode();
                String languageName = language.getLanguageName();
                System.out.println("languageCode = " + languageCode);
                System.out.println("languageName = " + languageName);
            }

        } else {
            System.out.println("status::" + status);
        }
    }

}
