/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmrestclient.client.site;

import com.magna.xmbackend.vo.icon.Icon;
import com.magna.xmbackend.vo.icon.IconResponse;
import com.magna.xmbackend.vo.site.Site;
import com.magna.xmbackend.vo.site.SiteDetail;
import com.magna.xmbackend.vo.site.SiteObject;
import com.magna.xmbackend.vo.site.SiteResponse;
import com.magna.xmbackend.vo.site.SiteResponseObject;
import java.util.List;
import javax.ws.rs.ClientErrorException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author dhana
 */
public class SiteInvoker {

    private final WebTarget webTarget;
    private final Client client;
    private static final String BASE_URI
            = "http://localhost:8888/XMBackend-1.0.0.M2";

    public SiteInvoker() {
        client = javax.ws.rs.client.ClientBuilder.newClient();
        webTarget = client.target(BASE_URI).path("site");
    }

    public void getAllSites() throws ClientErrorException {
        Response response = webTarget.path("fetchAll")
                .request().header("Content-Type", MediaType.APPLICATION_JSON)
                .get();
        int status = response.getStatus();
        System.out.println("before status check::" + status);
        if (status == 200) {
            SiteResponseObject siteResponseObject = response.readEntity(SiteResponseObject.class);
            List<SiteResponse> siteResponseList = siteResponseObject.getSiteResponseList();
            for (SiteResponse siteResponse : siteResponseList) {
                SiteObject siteObject = siteResponse.getSiteObject();
                Site site = siteObject.getSite();
                String id = site.getId();
                System.out.println("siteId = " + id);
                List<SiteDetail> siteDetailsList = siteObject.getSiteDetailsList();
                for (SiteDetail siteDetail : siteDetailsList) {
                    String siteName = siteDetail.getSiteName();
                    System.out.println("siteName = " + siteName);
                }
            }

        } else {
            System.out.println("status::" + status);
        }
    }
}
