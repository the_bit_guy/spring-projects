/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmrestclient.client;

import com.magna.xmbackend.vo.BaseObject;
import com.magna.xmbackend.vo.icon.Icon;
import com.magna.xmbackend.vo.ResponseObject;
import javax.json.JsonObject;
import javax.ws.rs.ClientErrorException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author dhana
 */
public class XMAdminIconClient {

    private final WebTarget webTarget;
    private final Client client;
    private static final String BASE_URI
            = "http://localhost:8888/XMBackend-1.0.0.M2";

    public XMAdminIconClient() {
        client = javax.ws.rs.client.ClientBuilder.newClient();
        webTarget = client.target(BASE_URI).path("admin");
    }

    public void createIconObject(Icon icon) throws ClientErrorException {
        Response response = webTarget.path("create/icon").request()
                .post(Entity.entity(icon, MediaType.APPLICATION_JSON));
        int status = response.getStatus();
        System.out.println("before status check::" + status);
        if (status == 200) {
            ResponseObject responseObject = response.readEntity(ResponseObject.class);
            BaseObject baseObject = responseObject.getBaseObject();
            String id = baseObject.getId();
            System.out.println("baseObject.getId() is " + id);
        } else {
            System.out.println("status::" + status);
        }
    }

    public void getIconByName(String name) throws ClientErrorException {
        Response response = webTarget.path("fetch/icon/byName/" + name)
                .request().header("Content-Type", MediaType.APPLICATION_JSON)
                .get();
        int status = response.getStatus();
        System.out.println("before status check::" + status);
        if (status == 200) {
            ResponseObject responseObject = response.readEntity(ResponseObject.class);
            BaseObject baseObject = responseObject.getBaseObject();
            String id = baseObject.getId();
            System.out.println("baseObject.getId() is " + id);
        } else {
            System.out.println("status::" + status);
        }
    }

    public void close() {
        client.close();
    }
}
