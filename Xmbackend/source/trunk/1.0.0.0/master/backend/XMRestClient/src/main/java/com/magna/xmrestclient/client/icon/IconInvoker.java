/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmrestclient.client.icon;

import com.magna.xmbackend.vo.icon.Icon;
import com.magna.xmbackend.vo.icon.IconResponse;
import java.util.List;
import javax.ws.rs.ClientErrorException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author dhana
 */
public class IconInvoker {

    private final WebTarget webTarget;
    private final Client client;
    private static final String BASE_URI
            = "http://localhost:8888/XMBackend-1.0.0.M2";

    public IconInvoker() {
        client = javax.ws.rs.client.ClientBuilder.newClient();
        webTarget = client.target(BASE_URI).path("icon");
    }

    public void getIconByName(String name) throws ClientErrorException {
        Response response = webTarget.path("fetchByName")
                .request()
                .post(Entity.entity(name, MediaType.APPLICATION_JSON));
        int status = response.getStatus();
        System.out.println("before status check::" + status);
        if (status == 200) {
            IconResponse iconResponse = response.readEntity(IconResponse.class);
            List<Icon> icons = iconResponse.getIcons();
            Icon icon = icons.get(0);
            System.out.println("icon is " + icon);
        } else {
            System.out.println("status::" + status);
        }
    }

    public void getIconByID(String id) throws ClientErrorException {
        Response response = webTarget.path("fetchById/" + id)
                .request().header("Content-Type", MediaType.APPLICATION_JSON)
                .get();
        int status = response.getStatus();
        System.out.println("before status check::" + status);
        if (status == 200) {
            IconResponse iconResponse = response.readEntity(IconResponse.class);
            List<Icon> icons = iconResponse.getIcons();
            Icon icon = icons.get(0);
            System.out.println("icon is " + icon);
        } else {
            System.out.println("status::" + status);
        }
    }
}
