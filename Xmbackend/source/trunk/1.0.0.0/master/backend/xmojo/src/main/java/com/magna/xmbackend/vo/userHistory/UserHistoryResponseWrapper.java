/**
 * 
 */
package com.magna.xmbackend.vo.userHistory;

import java.util.List;
import java.util.Map;

/**
 * @author Bhabadyuti Bal
 *
 */
public class UserHistoryResponseWrapper {
	
	private List<Map<String, Object>> queryResultSet;
	
	public UserHistoryResponseWrapper() {
	}

	/**
	 * @param queryResultSet
	 */
	public UserHistoryResponseWrapper(List<Map<String, Object>> queryResultSet) {
		this.queryResultSet = queryResultSet;
	}

	/**
	 * @return the queryResultSet
	 */
	public List<Map<String, Object>> getQueryResultSet() {
		return queryResultSet;
	}

	/**
	 * @param queryResultSet the queryResultSet to set
	 */
	public void setQueryResultSet(List<Map<String, Object>> queryResultSet) {
		this.queryResultSet = queryResultSet;
	}

}
