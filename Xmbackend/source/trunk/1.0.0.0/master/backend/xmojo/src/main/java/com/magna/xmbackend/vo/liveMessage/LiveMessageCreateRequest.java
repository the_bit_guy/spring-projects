/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.vo.liveMessage;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 *
 * @author dhana
 */
public class LiveMessageCreateRequest implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;


    private String liveMessageId;
    @NotNull(message = "live message name cannot be null")
    private String liveMessageName;
    @NotNull(message = "live message popup field is required")
    private String ispopup;
    @NotNull(message = "subject is required")
    private String subject;
	private String message;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date startDatetime;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endDatetime;

    private Set<LiveMessageConfigCreateRequest> liveMessageConfigCreateRequest;
    private List<LiveMessageTranslation> liveMsgTranslations;
    
    public LiveMessageCreateRequest() {
    }
    
    /**
	 * @return the subject
	 */
	public String getSubject() {
		return subject;
	}

	/**
	 * @param subject the subject to set
	 */
	public void setSubject(String subject) {
		this.subject = subject;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * @return the liveMessageId
	 */
	public String getLiveMessageId() {
		return liveMessageId;
	}

	/**
	 * @param liveMessageId the liveMessageId to set
	 */
	public void setLiveMessageId(String liveMessageId) {
		this.liveMessageId = liveMessageId;
	}

    /**
     * @return the liveMessageName
     */
    public String getLiveMessageName() {
        return liveMessageName;
    }

    /**
     * @param liveMessageName the liveMessageName to set
     */
    public void setLiveMessageName(String liveMessageName) {
        this.liveMessageName = liveMessageName;
    }

    /**
     * @return the ispopup
     */
    public String getIspopup() {
        return ispopup;
    }

    /**
     * @param ispopup the ispopup to set
     */
    public void setIspopup(String ispopup) {
        this.ispopup = ispopup;
    }

    /**
     * @return the startDatetime
     */
    public Date getStartDatetime() {
        return startDatetime;
    }

    /**
     * @param startDatetime the startDatetime to set
     */
    public void setStartDatetime(Date startDatetime) {
        this.startDatetime = startDatetime;
    }

    /**
     * @return the endDatetime
     */
    public Date getEndDatetime() {
        return endDatetime;
    }

    /**
     * @param endDatetime the endDatetime to set
     */
    public void setEndDatetime(Date endDatetime) {
        this.endDatetime = endDatetime;
    }

    /**
     * @return the liveMessageConfigCreateRequest
     */
    public Set<LiveMessageConfigCreateRequest> getLiveMessageConfigCreateRequest() {
        return liveMessageConfigCreateRequest;
    }

    /**
     * @param liveMessageConfigCreateRequest the liveMessageConfigCreateRequest
     * to set
     */
    public void setLiveMessageConfigCreateRequest(Set<LiveMessageConfigCreateRequest> liveMessageConfigCreateRequest) {
        this.liveMessageConfigCreateRequest = liveMessageConfigCreateRequest;
    }

	/**
	 * @return the liveMsgTranslations
	 */
	public List<LiveMessageTranslation> getLiveMsgTranslations() {
		return liveMsgTranslations;
	}

	/**
	 * @param liveMsgTranslations the liveMsgTranslations to set
	 */
	public void setLiveMsgTranslations(List<LiveMessageTranslation> liveMsgTranslations) {
		this.liveMsgTranslations = liveMsgTranslations;
	}

}
