/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.vo;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author dhana
 */
public class AdminAreaResponse extends BaseObject implements Serializable {

    private List<AdminAreaPojo> adminAreaPojos;
    private Message message;

    public AdminAreaResponse() {
    }

    public AdminAreaResponse(List<AdminAreaPojo> adminAreaPojos, Message message) {
        this.adminAreaPojos = adminAreaPojos;
        this.message = message;
    }

    public AdminAreaResponse(List<AdminAreaPojo> adminAreaPojos, Message message, XMObject xMObject) {
        super(xMObject);
        this.adminAreaPojos = adminAreaPojos;
        this.message = message;
    }

    /**
     * @return the adminAreaPojos
     */
    public List<AdminAreaPojo> getAdminAreaPojos() {
        return adminAreaPojos;
    }

    /**
     * @param adminAreaPojos the adminAreaPojos to set
     */
    public void setAdminAreaPojos(List<AdminAreaPojo> adminAreaPojos) {
        this.adminAreaPojos = adminAreaPojos;
    }

    /**
     * @return the message
     */
    public Message getMessage() {
        return message;
    }

    /**
     * @param message the message to set
     */
    public void setMessage(Message message) {
        this.message = message;
    }

}
