/**
 * 
 */
package com.magna.xmbackend.vo.notification;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import com.magna.xmbackend.entities.EmailTemplateTbl;

/**
 * @author Bhabadyuti Bal
 *
 */
public class EmailTemplateResponse implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	Iterable<EmailTemplateTbl> emailTemplateTbls;
	private List<Map<String, String>> statusMaps;
	
	public EmailTemplateResponse() {
	}
	
	
	public EmailTemplateResponse(List<Map<String, String>> statusMaps) {
		super();
		this.statusMaps = statusMaps;
	}

	public List<Map<String, String>> getStatusMaps() {
		return statusMaps;
	}


	public void setStatusMaps(List<Map<String, String>> statusMaps) {
		this.statusMaps = statusMaps;
	}


	public EmailTemplateResponse(Iterable<EmailTemplateTbl> emailTemplateTbls) {
		this.emailTemplateTbls = emailTemplateTbls;
	}

	public Iterable<EmailTemplateTbl> getEmailTemplateTbls() {
		return emailTemplateTbls;
	}

	public void setEmailTemplateTbls(Iterable<EmailTemplateTbl> emailTemplateTbls) {
		this.emailTemplateTbls = emailTemplateTbls;
	}
	
	

}
