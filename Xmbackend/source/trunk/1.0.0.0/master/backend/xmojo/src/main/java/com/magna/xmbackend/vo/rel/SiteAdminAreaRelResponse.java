/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.vo.rel;

import java.util.List;
import java.util.Map;

import com.magna.xmbackend.entities.SiteAdminAreaRelTbl;

/**
 *
 * @author dhana
 */
public class SiteAdminAreaRelResponse {
    private Iterable<SiteAdminAreaRelTbl> siteAdminAreaRelTbls;
    private List<Map<String, String>> statusMaps;

    public SiteAdminAreaRelResponse() {
    }

    /**
	 * @return the statusMaps
	 */
	public List<Map<String, String>> getStatusMaps() {
		return statusMaps;
	}

	/**
	 * @param statusMaps the statusMaps to set
	 */
	public void setStatusMaps(List<Map<String, String>> statusMaps) {
		this.statusMaps = statusMaps;
	}

	/**
	 * @param statusMaps
	 */
	public SiteAdminAreaRelResponse(List<Map<String, String>> statusMaps) {
		this.statusMaps = statusMaps;
	}

	public SiteAdminAreaRelResponse(Iterable<SiteAdminAreaRelTbl> siteAdminAreaRelTbls) {
        this.siteAdminAreaRelTbls = siteAdminAreaRelTbls;
    }

    /**
     * @return the siteAdminAreaRelTbls
     */
    public Iterable<SiteAdminAreaRelTbl> getSiteAdminAreaRelTbls() {
        return siteAdminAreaRelTbls;
    }

    /**
     * @param siteAdminAreaRelTbls the siteAdminAreaRelTbls to set
     */
    public void setSiteAdminAreaRelTbls(Iterable<SiteAdminAreaRelTbl> siteAdminAreaRelTbls) {
        this.siteAdminAreaRelTbls = siteAdminAreaRelTbls;
    }
    
}
