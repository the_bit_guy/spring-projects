/**
 * 
 */
package com.magna.xmbackend.vo.propConfig;

import java.util.EnumMap;

import com.magna.xmbackend.vo.enums.LdapKey;

/**
 * @author Bhabadyuti Bal
 *
 */
public class LdapConfigRequest {

	private EnumMap<LdapKey, String> ldapKeyValue;

	public LdapConfigRequest() {
	}

	/**
	 * @param ldapKeyValue
	 */
	public LdapConfigRequest(EnumMap<LdapKey, String> ldapKeyValue) {
		this.ldapKeyValue = ldapKeyValue;
	}

	/**
	 * @return the ldapKeyValue
	 */
	public EnumMap<LdapKey, String> getLdapKeyValue() {
		return ldapKeyValue;
	}

	/**
	 * @param ldapKeyValue the ldapKeyValue to set
	 */
	public void setLdapKeyValue(EnumMap<LdapKey, String> ldapKeyValue) {
		this.ldapKeyValue = ldapKeyValue;
	}

	
}
