package com.magna.xmbackend.vo.adminHistory;

/**
 * The Class AdminBaseHistoryRequest.
 */
public class AdminHistoryRequest {
	private String queryCondition;
	private int queryLimit;

	/**
	 * @return the queryCondition
	 */
	public String getQueryCondition() {
		return queryCondition;
	}

	/**
	 * @param queryCondition
	 *            the queryCondition to set
	 */
	public void setQueryCondition(String queryCondition) {
		this.queryCondition = queryCondition;
	}

	/**
	 * @return the queryLimit
	 */
	public int getQueryLimit() {
		return queryLimit;
	}

	/**
	 * @param queryLimit
	 *            the queryLimit to set
	 */
	public void setQueryLimit(int queryLimit) {
		this.queryLimit = queryLimit;
	}
}
