package com.magna.xmbackend.vo.rel;

import com.magna.xmbackend.entities.AdminAreaProjectRelTbl;
import java.util.List;
import java.util.Map;

/**
 *
 * @author dhana
 */
public class AdminAreaProjectRelBatchResponse {

    private List<AdminAreaProjectRelTbl> adminAreaProjectRelTbls;
    private List<Map<String, String>> statusMaps;

    public AdminAreaProjectRelBatchResponse() {
    }

    /**
     *
     * @param adminAreaProjectRelTbls
     * @param statusMaps
     */
    public AdminAreaProjectRelBatchResponse(List<AdminAreaProjectRelTbl> adminAreaProjectRelTbls,
            List<Map<String, String>> statusMaps) {
        this.adminAreaProjectRelTbls = adminAreaProjectRelTbls;
        this.statusMaps = statusMaps;
    }

    /**
     * @return the siteAdminAreaRelTbls
     */
    public final List<AdminAreaProjectRelTbl> getAdminAreaProjectRelTbls() {
        return adminAreaProjectRelTbls;
    }

    /**
     * @param siteAdminAreaRelTbls the siteAdminAreaRelTbls to set
     */
    public final void setAdminAreaProjectRelTbls(final List<AdminAreaProjectRelTbl> siteAdminAreaRelTbls) {
        this.adminAreaProjectRelTbls = siteAdminAreaRelTbls;
    }

    /**
     * @return the statusMaps
     */
    public final List<Map<String, String>> getStatusMap() {
        return statusMaps;
    }

    /**
     * @param statusMaps the statusMap to set
     */
    public final void setStatusMap(final List<Map<String, String>> statusMaps) {
        this.statusMaps = statusMaps;
    }

}
