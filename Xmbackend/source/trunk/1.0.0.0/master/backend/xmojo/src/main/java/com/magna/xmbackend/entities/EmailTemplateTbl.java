/**
 * 
 */
package com.magna.xmbackend.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonBackReference;

/**
 * @author Bhabadyuti Bal
 *
 */
@Entity
@Table(name="EMAIL_TEMPLATE_TBL")
public class EmailTemplateTbl implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
    @Basic(optional = false)
    @Column(name = "EMAIL_TEMPLATE_ID")
	private String emailTemplateId;
	@Column(name = "NAME")
	private String name;
	@Column(name = "SUBJECT")
	private String subject;
	@Column(name = "MESSAGE")
	private String message;
	@Column(name = "MESSAGE_TEMPLATE")
	private String messageTemplate;
	@Column(name = "TEMPLATE_VARIABLES")
	private String templateVeriables;
	@Column(name = "CREATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
	private Date createDate;
	@Column(name = "UPDATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
	private Date updatedDate;
	
	@JsonBackReference
	@OneToMany(mappedBy = "emailTemplateId", cascade = CascadeType.ALL)
	private Collection<EmailNotificationConfigTbl> emailNotifConfigCollection;
	

	public EmailTemplateTbl() {
	}
	
	public Collection<EmailNotificationConfigTbl> getEmailNotifConfigCollection() {
		return emailNotifConfigCollection;
	}

	public void setEmailNotifConfigCollection(Collection<EmailNotificationConfigTbl> emailNotifConfigCollection) {
		this.emailNotifConfigCollection = emailNotifConfigCollection;
	}
	
	public EmailTemplateTbl(String emailTemplateId) {
		this.emailTemplateId = emailTemplateId;
	}


	public String getEmailTemplateId() {
		return emailTemplateId;
	}


	public void setEmailTemplateId(String emailTemplateId) {
		this.emailTemplateId = emailTemplateId;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getSubject() {
		return subject;
	}


	public void setSubject(String subject) {
		this.subject = subject;
	}


	public String getMessage() {
		return message;
	}


	public void setMessage(String message) {
		this.message = message;
	}


	public String getMessageTemplate() {
		return messageTemplate;
	}


	public void setMessageTemplate(String messageTemplate) {
		this.messageTemplate = messageTemplate;
	}


	public String getTemplateVeriables() {
		return templateVeriables;
	}


	public void setTemplateVeriables(String templateVeriables) {
		this.templateVeriables = templateVeriables;
	}


	public Date getCreateDate() {
		return createDate;
	}


	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}


	public Date getUpdatedDate() {
		return updatedDate;
	}


	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	
	
}
