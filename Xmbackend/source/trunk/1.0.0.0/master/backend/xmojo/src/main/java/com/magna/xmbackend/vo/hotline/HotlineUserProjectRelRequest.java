/**
 * 
 */
package com.magna.xmbackend.vo.hotline;

/**
 * @author Bhabadyuti Bal
 *
 */
public class HotlineUserProjectRelRequest {

	private String userId;
	private String projectId;
	private String relationStatus;
	
	public HotlineUserProjectRelRequest() {
	}

	/**
	 * @param userId
	 * @param projectId
	 */
	public HotlineUserProjectRelRequest(String userId, String projectId) {
		this.userId = userId;
		this.projectId = projectId;
	}

	/**
	 * @return the userId
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * @return the projectId
	 */
	public String getProjectId() {
		return projectId;
	}

	/**
	 * @param projectId the projectId to set
	 */
	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	/**
	 * @return the relationStatus
	 */
	public String getRelationStatus() {
		return relationStatus;
	}

	/**
	 * @param relationStatus the relationStatus to set
	 */
	public void setRelationStatus(String relationStatus) {
		this.relationStatus = relationStatus;
	}
	
}
