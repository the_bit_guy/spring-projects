/**
 * 
 */
package com.magna.xmbackend.vo.adminMenu;

import java.util.List;

/**
 * @author Bhabadyuti Bal
 *
 */
public class AdminMenuConfigCustomeResponseWrapper {

	private List<AdminMenuConfigCustomeResponse> adminMenuConfigCustomeResponses;
	
	
	/**
	 * @param adminMenuConfigCustomeResponses
	 */
	public AdminMenuConfigCustomeResponseWrapper(List<AdminMenuConfigCustomeResponse> adminMenuConfigCustomeResponses) {
		this.adminMenuConfigCustomeResponses = adminMenuConfigCustomeResponses;
	}

	public AdminMenuConfigCustomeResponseWrapper() {
	}

	/**
	 * @return the adminMenuConfigCustomeResponses
	 */
	public List<AdminMenuConfigCustomeResponse> getAdminMenuConfigCustomeResponses() {
		return adminMenuConfigCustomeResponses;
	}

	/**
	 * @param adminMenuConfigCustomeResponses the adminMenuConfigCustomeResponses to set
	 */
	public void setAdminMenuConfigCustomeResponses(List<AdminMenuConfigCustomeResponse> adminMenuConfigCustomeResponses) {
		this.adminMenuConfigCustomeResponses = adminMenuConfigCustomeResponses;
	}
	
	
}
