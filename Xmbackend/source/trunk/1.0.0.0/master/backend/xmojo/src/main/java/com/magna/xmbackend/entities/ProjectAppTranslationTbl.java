/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.entities;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonBackReference;

/**
 *
 * @author Admin
 */
@Entity
@Table(name = "PROJECT_APP_TRANSLATION_TBL")
@NamedQueries({
    @NamedQuery(name = "ProjectAppTranslationTbl.findAll", query = "SELECT p FROM ProjectAppTranslationTbl p")})
public class ProjectAppTranslationTbl implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "PROJECT_APP_TRANSLATION_ID")
    private String projectAppTranslationId;
    @Column(name = "NAME")
    private String name;
    @Column(name = "DESCRIPTION")
    private String description;
    @Column(name = "REMARKS")
    private String remarks;
    @Basic(optional = false)
    @Column(name = "CREATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Basic(optional = false)
    @Column(name = "UPDATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;
    @JoinColumn(name = "LANGUAGE_CODE", referencedColumnName = "LANGUAGE_CODE")
    @ManyToOne(optional = false)
    private LanguagesTbl languageCode;
    @JsonBackReference
    @JoinColumn(name = "PROJECT_APPLICATION_ID", referencedColumnName = "PROJECT_APPLICATION_ID")
    @ManyToOne
    private ProjectApplicationsTbl projectApplicationId;

    public ProjectAppTranslationTbl() {
    }

    public ProjectAppTranslationTbl(String projectAppTranslationId) {
        this.projectAppTranslationId = projectAppTranslationId;
    }

    public ProjectAppTranslationTbl(String projectAppTranslationId, Date createDate, Date updateDate) {
        this.projectAppTranslationId = projectAppTranslationId;
        this.createDate = createDate;
        this.updateDate = updateDate;
    }

    public ProjectAppTranslationTbl(String projectAppTranslationId, Date createDate, Date updateDate,
			LanguagesTbl languageCode, ProjectApplicationsTbl projectApplicationId) {
		this.projectAppTranslationId = projectAppTranslationId;
		this.createDate = createDate;
		this.updateDate = updateDate;
		this.languageCode = languageCode;
		this.projectApplicationId = projectApplicationId;
	}

	public String getProjectAppTranslationId() {
        return projectAppTranslationId;
    }

    public void setProjectAppTranslationId(String projectAppTranslationId) {
        this.projectAppTranslationId = projectAppTranslationId;
    }

    public String getName() {
    	String decName = null;
    	try {
    		if (name != null) {
    			decName = java.net.URLDecoder.decode(name, "ISO-8859-1");
    		}
    	} catch (UnsupportedEncodingException e) {
    		e.printStackTrace();
    	}
    	return decName;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
    	String decDescription = null;
		try {
			if (description != null) {
				decDescription = java.net.URLDecoder.decode(description, "ISO-8859-1");
			}
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
        return decDescription;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRemarks() {
    	String decRemarks = null;
		try {
			if (remarks != null) {
				decRemarks = java.net.URLDecoder.decode(remarks, "ISO-8859-1");
			}
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
        return decRemarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public LanguagesTbl getLanguageCode() {
        return languageCode;
    }

    public void setLanguageCode(LanguagesTbl languageCode) {
        this.languageCode = languageCode;
    }

    public ProjectApplicationsTbl getProjectApplicationId() {
        return projectApplicationId;
    }

    public void setProjectApplicationId(ProjectApplicationsTbl projectApplicationId) {
        this.projectApplicationId = projectApplicationId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (projectAppTranslationId != null ? projectAppTranslationId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProjectAppTranslationTbl)) {
            return false;
        }
        ProjectAppTranslationTbl other = (ProjectAppTranslationTbl) object;
        if ((this.projectAppTranslationId == null && other.projectAppTranslationId != null) || (this.projectAppTranslationId != null && !this.projectAppTranslationId.equals(other.projectAppTranslationId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.magna.xmbackend.entities.ProjectAppTranslationTbl[ projectAppTranslationId=" + projectAppTranslationId + " ]";
    }
    
}
