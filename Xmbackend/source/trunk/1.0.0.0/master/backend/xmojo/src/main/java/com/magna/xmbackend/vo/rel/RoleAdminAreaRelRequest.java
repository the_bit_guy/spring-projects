package com.magna.xmbackend.vo.rel;

import java.util.Date;

/**
 *
 * @author vijay
 */
public class RoleAdminAreaRelRequest {

    private String roleId;
    private String adminAreaId;
    private Date createDate;
    private Date updateDate;

    public RoleAdminAreaRelRequest() {
    }

    /**
     *
     * @param adminAreaId
     * @param roleId
     * @param createDate
     * @param updateDate
     */
    public RoleAdminAreaRelRequest(String adminAreaId, String roleId,
            Date createDate, Date updateDate) {
        this.adminAreaId = adminAreaId;
        this.roleId = roleId;
        this.createDate = createDate;
        this.updateDate = updateDate;
    }

    /**
     * @return the roleId
     */
    public final String getRoleId() {
        return roleId;
    }

    /**
     * @param roleId the roleId to set
     */
    public final void setRoleId(final String roleId) {
        this.roleId = roleId;
    }

    /**
     * @return the createDate
     */
    public final Date getCreateDate() {
        return createDate;
    }

    /**
     * @param createDate the createDate to set
     */
    public final void setCreateDate(final Date createDate) {
        this.createDate = createDate;
    }

    /**
     * @return the updateDate
     */
    public final Date getUpdateDate() {
        return updateDate;
    }

    /**
     * @param updateDate the updateDate to set
     */
    public final void setUpdateDate(final Date updateDate) {
        this.updateDate = updateDate;
    }

    /**
     * @return the adminAreaId
     */
    public String getAdminAreaId() {
        return adminAreaId;
    }

    /**
     * @param adminAreaId the adminAreaId to set
     */
    public void setAdminAreaId(String adminAreaId) {
        this.adminAreaId = adminAreaId;
    }

    /**
     *
     * @return object as string
     */
    @Override
    public final String toString() {
        return new StringBuilder().append("RoleAdminAreaRelRequest{")
                .append(", roleId=").append(getRoleId())
                .append(", adminAreaId=").append(getAdminAreaId())
                .append(", createDate=").append(getCreateDate())
                .append(", updateDate=").append(getUpdateDate())
                .append("}").toString();
    }
}
