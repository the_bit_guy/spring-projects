/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.entities;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonBackReference;

/**
 *
 * @author Admin
 */
@Entity
@Table(name = "DIRECTORY_TBL")
@NamedQueries({
    @NamedQuery(name = "DirectoryTbl.findAll", query = "SELECT d FROM DirectoryTbl d")})
public class DirectoryTbl implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "DIRECTORY_ID")
    private String directoryId;
    @Basic(optional = false)
    @Column(name = "NAME")
    private String name;
    @Basic(optional = false)
    @Column(name = "CREATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Basic(optional = false)
    @Column(name = "UPDATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "directoryId")
    private Collection<DirectoryTranslationTbl> directoryTranslationTblCollection;
    @JsonBackReference
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "directoryId")
    private Collection<DirectoryRefTbl> directoryRefTblCollection;
    @JoinColumn(name = "ICON_ID", referencedColumnName = "ICON_ID")
    @ManyToOne
    private IconsTbl iconId;

    public DirectoryTbl() {
    }

    public DirectoryTbl(String directoryId) {
        this.directoryId = directoryId;
    }

    public DirectoryTbl(String directoryId, String name, Date createDate, Date updateDate) {
        this.directoryId = directoryId;
        this.name = name;
        this.createDate = createDate;
        this.updateDate = updateDate;
    }

    public String getDirectoryId() {
        return directoryId;
    }

    public void setDirectoryId(String directoryId) {
        this.directoryId = directoryId;
    }

    public String getName() {
    	String decName = null;
		try {
			if (name != null) {
				decName = java.net.URLDecoder.decode(name, "ISO-8859-1");
			}
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
        return decName;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public Collection<DirectoryTranslationTbl> getDirectoryTranslationTblCollection() {
        return directoryTranslationTblCollection;
    }

    public void setDirectoryTranslationTblCollection(Collection<DirectoryTranslationTbl> directoryTranslationTblCollection) {
        this.directoryTranslationTblCollection = directoryTranslationTblCollection;
    }

    public Collection<DirectoryRefTbl> getDirectoryRefTblCollection() {
        return directoryRefTblCollection;
    }

    public void setDirectoryRefTblCollection(Collection<DirectoryRefTbl> directoryRefTblCollection) {
        this.directoryRefTblCollection = directoryRefTblCollection;
    }

    public IconsTbl getIconId() {
        return iconId;
    }

    public void setIconId(IconsTbl iconId) {
        this.iconId = iconId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (directoryId != null ? directoryId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DirectoryTbl)) {
            return false;
        }
        DirectoryTbl other = (DirectoryTbl) object;
        if ((this.directoryId == null && other.directoryId != null) || (this.directoryId != null && !this.directoryId.equals(other.directoryId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.magna.xmbackend.entities.DirectoryTbl[ directoryId=" + directoryId + " ]";
    }
    
}
