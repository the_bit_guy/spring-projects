/**
 * 
 */
package com.magna.xmbackend.response.rel.adminareauserapp;

import java.util.List;

/**
 * @author Bhabadyuti Bal
 *
 */
public class AdminAreaUserAppRelWrapper {

	List<AdminAreaUserAppRelation> adminAreaUserAppRels;
	
	public AdminAreaUserAppRelWrapper() {
	}
	
	

	/**
	 * @param adminAreaUserAppRels
	 */
	public AdminAreaUserAppRelWrapper(List<AdminAreaUserAppRelation> adminAreaUserAppRels) {
		this.adminAreaUserAppRels = adminAreaUserAppRels;
	}



	/**
	 * @return the adminAreaUserAppRels
	 */
	public List<AdminAreaUserAppRelation> getAdminAreaUserAppRels() {
		return adminAreaUserAppRels;
	}

	/**
	 * @param adminAreaUserAppRels the adminAreaUserAppRels to set
	 */
	public void setAdminAreaUserAppRels(List<AdminAreaUserAppRelation> adminAreaUserAppRels) {
		this.adminAreaUserAppRels = adminAreaUserAppRels;
	}
	
	
}
