/**
 * 
 */
package com.magna.xmbackend.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonBackReference;

// TODO: Auto-generated Javadoc
/**
 * The Class LiveMessageTranslationTbl.
 *
 * @author Bhabadyuti Bal
 */
@Entity
@Table(name = "LIVE_MESSAGE_TRANSLATION_TBL")
@NamedQueries({
@NamedQuery(name = "LiveMessageTranslationTbl.findAll", query = "SELECT l FROM LiveMessageTranslationTbl l")})
public class LiveMessageTranslationTbl implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The id. */
	@Id
    @Basic(optional = false)
    @Column(name = "LIVE_MESSAGE_TRANSLATION_ID")
	private String id;
	
	/** The live message id. */
	@JsonBackReference
	@JoinColumn(name = "LIVE_MESSAGE_ID", referencedColumnName = "LIVE_MESSAGE_ID")
	@ManyToOne
	private LiveMessageTbl liveMessageId;
	
	/** The subject. */
	@Basic(optional = false)
    @Column(name = "SUBJECT")
    private String subject;
    
    /** The message. */
    @Column(name = "MESSAGE")
    private String message;
	
	/** The create date. */
	@Basic(optional = false)
    @Column(name = "CREATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    
    /** The update date. */
    @Basic(optional = false)
    @Column(name = "UPDATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;
    
    /** The language code. */
    @JoinColumn(name = "LANGUAGE_CODE", referencedColumnName = "LANGUAGE_CODE")
    @ManyToOne(optional = false)
    private LanguagesTbl languageCode;
    
    /**
     * Instantiates a new live message translation tbl.
     */
    public LiveMessageTranslationTbl() {
	}
    
	/**
	 * Instantiates a new live message translation tbl.
	 *
	 * @param id the id
	 */
	public LiveMessageTranslationTbl(String id) {
		this.id = id;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	
	/**
	 * Sets the id.
	 *
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	
	/**
	 * Gets the live message id.
	 *
	 * @return the liveMessageId
	 */
	public LiveMessageTbl getLiveMessageId() {
		return liveMessageId;
	}
	
	/**
	 * Sets the live message id.
	 *
	 * @param liveMessageId the liveMessageId to set
	 */
	public void setLiveMessageId(LiveMessageTbl liveMessageId) {
		this.liveMessageId = liveMessageId;
	}
	
	/**
	 * Gets the subject.
	 *
	 * @return the subject
	 */
	public String getSubject() {
		return subject;
	}
	
	/**
	 * Sets the subject.
	 *
	 * @param subject the subject to set
	 */
	public void setSubject(String subject) {
		this.subject = subject;
	}
	
	/**
	 * Gets the message.
	 *
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}
	
	/**
	 * Sets the message.
	 *
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}
	
	/**
	 * Gets the creates the date.
	 *
	 * @return the createDate
	 */
	public Date getCreateDate() {
		return createDate;
	}
	
	/**
	 * Sets the creates the date.
	 *
	 * @param createDate the createDate to set
	 */
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	
	/**
	 * Gets the update date.
	 *
	 * @return the updateDate
	 */
	public Date getUpdateDate() {
		return updateDate;
	}
	
	/**
	 * Sets the update date.
	 *
	 * @param updateDate the updateDate to set
	 */
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	
	/**
	 * Gets the language code.
	 *
	 * @return the languageCode
	 */
	public LanguagesTbl getLanguageCode() {
		return languageCode;
	}
	
	/**
	 * Sets the language code.
	 *
	 * @param languageCode the languageCode to set
	 */
	public void setLanguageCode(LanguagesTbl languageCode) {
		this.languageCode = languageCode;
	}

}
