/**
 * 
 */
package com.magna.xmbackend.response.rel.useruserapp;

import java.util.List;

/**
 * @author Bhabadyuti Bal
 *
 */
public class UserUserAppRelResponseWrapper {

	private List<UserUserAppRelation> userUserAppRelations;
	
	public UserUserAppRelResponseWrapper() {
	}

	/**
	 * @param userUserAppRelations
	 */
	public UserUserAppRelResponseWrapper(List<UserUserAppRelation> userUserAppRelations) {
		this.userUserAppRelations = userUserAppRelations;
	}

	/**
	 * @return the userUserAppRelations
	 */
	public List<UserUserAppRelation> getUserUserAppRelations() {
		return userUserAppRelations;
	}

	/**
	 * @param userUserAppRelations the userUserAppRelations to set
	 */
	public void setUserUserAppRelations(List<UserUserAppRelation> userUserAppRelations) {
		this.userUserAppRelations = userUserAppRelations;
	}
	
	
}
