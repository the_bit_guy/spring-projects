/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Admin
 */
@Entity
@Table(name = "LIVE_MESSAGE_STATUS_TBL")
@NamedQueries({
    @NamedQuery(name = "LiveMessageStatusTbl.findAll", query = "SELECT l FROM LiveMessageStatusTbl l")})
public class LiveMessageStatusTbl implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "LIVE_MESSAGE_STATUS_ID")
    private String liveMessageStatusId;
    @Column(name = "STATUS")
    private String status;
    @Basic(optional = false)
    @Column(name = "CREATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Basic(optional = false)
    @Column(name = "UPDATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;
    @JoinColumn(name = "LIVE_MESSAGE_ID", referencedColumnName = "LIVE_MESSAGE_ID")
    @ManyToOne(optional = false)
    private LiveMessageTbl liveMessageId;
    @JoinColumn(name = "USER_ID", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private UsersTbl userId;

    public LiveMessageStatusTbl() {
    }

    public LiveMessageStatusTbl(String liveMessageStatusId) {
        this.liveMessageStatusId = liveMessageStatusId;
    }

    public LiveMessageStatusTbl(String liveMessageStatusId, Date createDate, Date updateDate) {
        this.liveMessageStatusId = liveMessageStatusId;
        this.createDate = createDate;
        this.updateDate = updateDate;
    }

    public String getLiveMessageStatusId() {
        return liveMessageStatusId;
    }

    public void setLiveMessageStatusId(String liveMessageStatusId) {
        this.liveMessageStatusId = liveMessageStatusId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public LiveMessageTbl getLiveMessageId() {
        return liveMessageId;
    }

    public void setLiveMessageId(LiveMessageTbl liveMessageId) {
        this.liveMessageId = liveMessageId;
    }

    public UsersTbl getUserId() {
        return userId;
    }

    public void setUserId(UsersTbl userId) {
        this.userId = userId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (liveMessageStatusId != null ? liveMessageStatusId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LiveMessageStatusTbl)) {
            return false;
        }
        LiveMessageStatusTbl other = (LiveMessageStatusTbl) object;
        if ((this.liveMessageStatusId == null && other.liveMessageStatusId != null) || (this.liveMessageStatusId != null && !this.liveMessageStatusId.equals(other.liveMessageStatusId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.magna.xmbackend.entities.LiveMessageStatusTbl[ liveMessageStatusId=" + liveMessageStatusId + " ]";
    }
    
}
