package com.magna.xmbackend.vo.site;

import com.magna.xmbackend.vo.BaseObject;
import com.magna.xmbackend.vo.Message;
import java.io.Serializable;
import java.util.List;

public class SiteRequestObject extends BaseObject implements Serializable {

    private List<SiteObject> siteObjectList;
    private Message message;

    /**
     *
     * @return siteObjectList
     */
    public final List<SiteObject> getSiteObjectList() {
        return siteObjectList;
    }

    /**
     *
     * @param siteObjectList
     */
    public final void setSiteObjectList(final List<SiteObject> siteObjectList) {
        this.siteObjectList = siteObjectList;
    }

    /**
     * @return the message
     */
    public final Message getMessage() {
        return message;
    }

    /**
     * @param message the message to set
     */
    public final void setMessage(final Message message) {
        this.message = message;
    }

    /**
     *
     * @return object as string
     */
    @Override
    public final String toString() {
        return new StringBuilder().append("SiteRequest{")
                .append("siteObjectList=").append(siteObjectList)
                .append("message=").append(message)
                .append("}").toString();
    }

}
