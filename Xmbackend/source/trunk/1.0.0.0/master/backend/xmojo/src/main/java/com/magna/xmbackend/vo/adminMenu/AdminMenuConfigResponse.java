/**
 * 
 */
package com.magna.xmbackend.vo.adminMenu;

import java.util.List;
import java.util.Map;

import com.magna.xmbackend.entities.AdminMenuConfigTbl;

/**
 * @author Bhabadyuti Bal
 *
 */
public class AdminMenuConfigResponse {
	
	Iterable<AdminMenuConfigTbl> adminMenuConfigTbls;
	private List<Map<String, String>> statusMaps;
	
	public AdminMenuConfigResponse() {
	}

	/**
	 * @param statusMaps
	 */
	public AdminMenuConfigResponse(List<Map<String, String>> statusMaps) {
		this.statusMaps = statusMaps;
	}

	public AdminMenuConfigResponse(Iterable<AdminMenuConfigTbl> adminMenuConfigTbls) {
		this.adminMenuConfigTbls = adminMenuConfigTbls;
	}
	

	/**
	 * @return the statusMaps
	 */
	public List<Map<String, String>> getStatusMaps() {
		return statusMaps;
	}

	/**
	 * @param statusMaps the statusMaps to set
	 */
	public void setStatusMaps(List<Map<String, String>> statusMaps) {
		this.statusMaps = statusMaps;
	}

	/**
	 * @param adminMenuConfigTbls
	 * @param statusMaps
	 */
	public AdminMenuConfigResponse(Iterable<AdminMenuConfigTbl> adminMenuConfigTbls,
			List<Map<String, String>> statusMaps) {
		this.adminMenuConfigTbls = adminMenuConfigTbls;
		this.statusMaps = statusMaps;
	}

	/**
	 * @return the adminMenuConfigTbls
	 */
	public Iterable<AdminMenuConfigTbl> getAdminMenuConfigTbls() {
		return adminMenuConfigTbls;
	}

	/**
	 * @param adminMenuConfigTbls the adminMenuConfigTbls to set
	 */
	public void setAdminMenuConfigTbls(Iterable<AdminMenuConfigTbl> adminMenuConfigTbls) {
		this.adminMenuConfigTbls = adminMenuConfigTbls;
	}
	
	
}
