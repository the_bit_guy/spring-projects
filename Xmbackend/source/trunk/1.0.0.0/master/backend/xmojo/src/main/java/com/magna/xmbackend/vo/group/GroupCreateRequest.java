/**
 * 
 */
package com.magna.xmbackend.vo.group;

import java.util.List;

import com.magna.xmbackend.vo.CreationInfo;

/**
 * @author Bhabadyuti Bal
 *
 */
public class GroupCreateRequest {

	private String id;
	private String groupName;
	private String groupType;
	private String iconId;
    private CreationInfo creationInfo;
    List<GroupTranslationRequest> groupTranslationReqs;
    /**
	 * @return the groupType
	 */
	public String getGroupType() {
		return groupType;
	}

	/**
	 * @param groupType the groupType to set
	 */
	public void setGroupType(String groupType) {
		this.groupType = groupType;
	}

    public GroupCreateRequest() {
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the iconId
	 */
	public String getIconId() {
		return iconId;
	}

	/**
	 * @param iconId the iconId to set
	 */
	public void setIconId(String iconId) {
		this.iconId = iconId;
	}

	/**
	 * @return the creationInfo
	 */
	public CreationInfo getCreationInfo() {
		return creationInfo;
	}

	/**
	 * @param creationInfo the creationInfo to set
	 */
	public void setCreationInfo(CreationInfo creationInfo) {
		this.creationInfo = creationInfo;
	}

	/**
	 * @return the groupTranslationReqs
	 */
	public List<GroupTranslationRequest> getGroupTranslationReqs() {
		return groupTranslationReqs;
	}

	/**
	 * @param groupTranslationReqs the groupTranslationReqs to set
	 */
	public void setGroupTranslationReqs(List<GroupTranslationRequest> groupTranslationReqs) {
		this.groupTranslationReqs = groupTranslationReqs;
	}

	/**
	 * @return the groupName
	 */
	public String getGroupName() {
		return groupName;
	}

	/**
	 * @param groupName the groupName to set
	 */
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
    
	
}
