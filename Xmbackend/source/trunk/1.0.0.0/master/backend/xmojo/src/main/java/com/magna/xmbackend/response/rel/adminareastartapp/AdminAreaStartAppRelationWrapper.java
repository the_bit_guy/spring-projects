/**
 * 
 */
package com.magna.xmbackend.response.rel.adminareastartapp;

import java.util.List;

/**
 * @author Bhabadyuti Bal
 *
 */
public class AdminAreaStartAppRelationWrapper {

	List<AdminAreaStartAppRelation> adminAreaStartAppRelations;
	
	public AdminAreaStartAppRelationWrapper() {
	}

	/**
	 * @param adminAreaStartAppRelations
	 */
	public AdminAreaStartAppRelationWrapper(List<AdminAreaStartAppRelation> adminAreaStartAppRelations) {
		this.adminAreaStartAppRelations = adminAreaStartAppRelations;
	}

	/**
	 * @return the adminAreaStartAppRelations
	 */
	public List<AdminAreaStartAppRelation> getAdminAreaStartAppRelations() {
		return adminAreaStartAppRelations;
	}

	/**
	 * @param adminAreaStartAppRelations the adminAreaStartAppRelations to set
	 */
	public void setAdminAreaStartAppRelations(List<AdminAreaStartAppRelation> adminAreaStartAppRelations) {
		this.adminAreaStartAppRelations = adminAreaStartAppRelations;
	}
	
	
	
	
}
