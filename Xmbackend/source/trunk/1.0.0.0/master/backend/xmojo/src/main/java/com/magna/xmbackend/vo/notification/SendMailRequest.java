/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.vo.notification;

import java.util.List;

import com.magna.xmbackend.vo.CreationInfo;
import com.magna.xmbackend.vo.enums.Status;

/**
 *
 * @author dhana
 */
public class SendMailRequest {

    //list of userNames's
    private List<String> usersToNotify;
    List<String> ccUsersToNotify;
    private String event;
    private String subject;
    private String message;
    private Status status;

    private CreationInfo creationInfo;

    public SendMailRequest() {
    }

    public SendMailRequest(List<String> usersToNotify, List<String> ccUsersTotify, String subject,
            String message, CreationInfo creationInfo) {
        this.usersToNotify = usersToNotify;
        this.ccUsersToNotify = ccUsersTotify;
        this.subject = subject;
        this.message = message;
        this.creationInfo = creationInfo;
    }

    /**
     * @return the usersToNotify
     */
    public List<String> getUsersToNotify() {
        return usersToNotify;
    }

    /**
     * @param usersToNotify the usersToNotify to set
     */
    public void setUsersToNotify(List<String> usersToNotify) {
        this.usersToNotify = usersToNotify;
    }

    /**
     * @return the subject
     */
    public String getSubject() {
        return subject;
    }

    /**
     * @param subject the subject to set
     */
    public void setSubject(String subject) {
        this.subject = subject;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * @return the creationInfo
     */
    public CreationInfo getCreationInfo() {
        return creationInfo;
    }

    /**
     * @param creationInfo the creationInfo to set
     */
    public void setCreationInfo(CreationInfo creationInfo) {
        this.creationInfo = creationInfo;
    }

	/**
	 * @return the status
	 */
	public Status getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(Status status) {
		this.status = status;
	}

	/**
	 * @return the ccUsersToNotify
	 */
	public List<String> getCcUsersToNotify() {
		return ccUsersToNotify;
	}

	/**
	 * @param ccUsersToNotify the ccUsersToNotify to set
	 */
	public void setCcUsersToNotify(List<String> ccUsersToNotify) {
		this.ccUsersToNotify = ccUsersToNotify;
	}

	/**
	 * @return the event
	 */
	public String getEvent() {
		return event;
	}

	/**
	 * @param event the event to set
	 */
	public void setEvent(String event) {
		this.event = event;
	}

}
