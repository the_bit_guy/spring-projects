package com.magna.xmbackend.vo.permission;

import com.magna.xmbackend.entities.PermissionTbl;

/**
 *
 * @author vijay
 */
public class PermissionResponse {

    private Iterable<PermissionTbl> permissionTbls;

    public PermissionResponse() {
    }

    /**
     *
     * @param permissionTbls
     */
    public PermissionResponse(Iterable<PermissionTbl> permissionTbls) {
        this.permissionTbls = permissionTbls;
    }

    /**
     *
     * @param permissionTbls
     */
    public final void setPermissionTbls(final Iterable<PermissionTbl> permissionTbls) {
        this.permissionTbls = permissionTbls;
    }

    /**
     *
     * @return permissionTbls
     */
    public final Iterable<PermissionTbl> getPermissionTbls() {
        return permissionTbls;
    }

}
