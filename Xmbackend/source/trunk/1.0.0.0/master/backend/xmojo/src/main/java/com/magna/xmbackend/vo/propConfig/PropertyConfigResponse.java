/**
 * 
 */
package com.magna.xmbackend.vo.propConfig;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import com.magna.xmbackend.entities.PropertyConfigTbl;

/**
 * @author Bhabadyuti Bal
 *
 */
public class PropertyConfigResponse {

	Iterable<PropertyConfigTbl> propertyConfigTbls;
	
	public PropertyConfigResponse() {
	}

	/**
	 * @param propertyConfigTbls
	 */
	public PropertyConfigResponse(Iterable<PropertyConfigTbl> propertyConfigTbls) {
		this.propertyConfigTbls = propertyConfigTbls;
	}

	/**
	 * @return the propertyConfigTbls
	 */
	public Iterable<PropertyConfigTbl> getPropertyConfigTbls() {
		return propertyConfigTbls;
	}

	/**
	 * @param propertyConfigTbls the propertyConfigTbls to set
	 */
	public void setPropertyConfigTbls(Iterable<PropertyConfigTbl> propertyConfigTbls) {
		this.propertyConfigTbls = propertyConfigTbls;
	}
	
	
}
