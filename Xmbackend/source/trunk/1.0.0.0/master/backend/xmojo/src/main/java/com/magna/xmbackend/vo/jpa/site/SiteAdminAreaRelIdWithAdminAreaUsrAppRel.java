package com.magna.xmbackend.vo.jpa.site;

import com.magna.xmbackend.entities.SiteAdminAreaRelTbl;
import com.magna.xmbackend.entities.UserApplicationsTbl;

/**
 *
 * @author vijay
 */
public class SiteAdminAreaRelIdWithAdminAreaUsrAppRel {

    private String adminAreaUsrAppRelId;
    private SiteAdminAreaRelTbl siteAdminAreaRelId;
    private UserApplicationsTbl userApplicationsTbl;
    private String status;
    private String relationType;

    public SiteAdminAreaRelIdWithAdminAreaUsrAppRel() {
    }

    /**
     *
     * @param siteAdminAreaRelId
     * @param projectsTbl
     * @param adminAreaProjRelId
     * @param status
     * @param relationType
     */
    public SiteAdminAreaRelIdWithAdminAreaUsrAppRel(SiteAdminAreaRelTbl siteAdminAreaRelId,
            UserApplicationsTbl projectsTbl, String adminAreaProjRelId,
            String status, String relationType) {
        this.siteAdminAreaRelId = siteAdminAreaRelId;
        this.userApplicationsTbl = projectsTbl;
        this.adminAreaUsrAppRelId = adminAreaProjRelId;
        this.status = status;
        this.relationType = relationType;
    }

    /**
     * @return the siteAdminAreaRelId
     */
    public final SiteAdminAreaRelTbl getSiteAdminAreaRelId() {
        return siteAdminAreaRelId;
    }

    /**
     * @param siteAdminAreaRelId the siteAdminAreaRelId to set
     */
    public final void setSiteAdminAreaRelId(final SiteAdminAreaRelTbl siteAdminAreaRelId) {
        this.siteAdminAreaRelId = siteAdminAreaRelId;
    }

    /**
     * @return the userApplicationsTbl
     */
    public final UserApplicationsTbl getUserApplicationsTbl() {
        return userApplicationsTbl;
    }

    /**
     * @param userApplicationsTbl the userApplicationsTbl to set
     */
    public final void setUserApplicationsTbl(final UserApplicationsTbl userApplicationsTbl) {
        this.userApplicationsTbl = userApplicationsTbl;
    }

    /**
     * @return the adminAreaUsrAppRelId
     */
    public final String getAdminAreaUsrAppRelId() {
        return adminAreaUsrAppRelId;
    }

    /**
     * @param adminAreaUsrAppRelId the adminAreaUsrAppRelId to set
     */
    public final void setAdminAreaUsrAppRelId(final String adminAreaUsrAppRelId) {
        this.adminAreaUsrAppRelId = adminAreaUsrAppRelId;
    }

    /**
     * @return the status
     */
    public final String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public final void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the relationType
     */
    public final String getRelationType() {
        return relationType;
    }

    /**
     * @param relationType the relationType to set
     */
    public final void setRelationType(final String relationType) {
        this.relationType = relationType;
    }
}
