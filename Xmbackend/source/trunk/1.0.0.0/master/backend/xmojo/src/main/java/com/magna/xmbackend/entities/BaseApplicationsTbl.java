/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonBackReference;

/**
 *
 * @author Admin
 */
@Entity
@Table(name = "BASE_APPLICATIONS_TBL")
@NamedQueries({
    @NamedQuery(name = "BaseApplicationsTbl.findAll", query = "SELECT b FROM BaseApplicationsTbl b")})
public class BaseApplicationsTbl implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "BASE_APPLICATION_ID")
    private String baseApplicationId;
    @Basic(optional = false)
    @Column(name = "NAME")
    private String name;
    @Basic(optional = false)
    @Column(name = "STATUS")
    private String status;
    @Column(name = "PLATFORMS")
    private String platforms;
    @Column(name = "PROGRAM")
    private String program;
    @Basic(optional = false)
    @Column(name = "CREATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Basic(optional = false)
    @Column(name = "UPDATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "baseApplicationId")
    private Collection<BaseAppTranslationTbl> baseAppTranslationTblCollection;
    @JsonBackReference
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "baseApplicationId")
    private Collection<UserApplicationsTbl> userApplicationsTblCollection;
    @JoinColumn(name = "ICON_ID", referencedColumnName = "ICON_ID")
    @ManyToOne
    private IconsTbl iconId;
    @JsonBackReference
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "baseApplicationId")
    private Collection<ProjectApplicationsTbl> projectApplicationsTblCollection;
    @JsonBackReference
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "baseApplicationId")
    private Collection<StartApplicationsTbl> startApplicationsTblCollection;

    public BaseApplicationsTbl() {
    }

    public BaseApplicationsTbl(String baseApplicationId) {
        this.baseApplicationId = baseApplicationId;
    }

    public BaseApplicationsTbl(String baseApplicationId, String name, String status, Date createDate, Date updateDate) {
        this.baseApplicationId = baseApplicationId;
        this.name = name;
        this.status = status;
        this.createDate = createDate;
        this.updateDate = updateDate;
    }

    public String getBaseApplicationId() {
        return baseApplicationId;
    }

    public void setBaseApplicationId(String baseApplicationId) {
        this.baseApplicationId = baseApplicationId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPlatforms() {
        return platforms;
    }

    public void setPlatforms(String platforms) {
        this.platforms = platforms;
    }

    public String getProgram() {
        return program;
    }

    public void setProgram(String program) {
        this.program = program;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public Collection<BaseAppTranslationTbl> getBaseAppTranslationTblCollection() {
        return baseAppTranslationTblCollection;
    }

    public void setBaseAppTranslationTblCollection(Collection<BaseAppTranslationTbl> baseAppTranslationTblCollection) {
        this.baseAppTranslationTblCollection = baseAppTranslationTblCollection;
    }

    public Collection<UserApplicationsTbl> getUserApplicationsTblCollection() {
        return userApplicationsTblCollection;
    }

    public void setUserApplicationsTblCollection(Collection<UserApplicationsTbl> userApplicationsTblCollection) {
        this.userApplicationsTblCollection = userApplicationsTblCollection;
    }

    public IconsTbl getIconId() {
        return iconId;
    }

    public void setIconId(IconsTbl iconId) {
        this.iconId = iconId;
    }

    public Collection<ProjectApplicationsTbl> getProjectApplicationsTblCollection() {
        return projectApplicationsTblCollection;
    }

    public void setProjectApplicationsTblCollection(Collection<ProjectApplicationsTbl> projectApplicationsTblCollection) {
        this.projectApplicationsTblCollection = projectApplicationsTblCollection;
    }

    public Collection<StartApplicationsTbl> getStartApplicationsTblCollection() {
        return startApplicationsTblCollection;
    }

    public void setStartApplicationsTblCollection(Collection<StartApplicationsTbl> startApplicationsTblCollection) {
        this.startApplicationsTblCollection = startApplicationsTblCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (baseApplicationId != null ? baseApplicationId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BaseApplicationsTbl)) {
            return false;
        }
        BaseApplicationsTbl other = (BaseApplicationsTbl) object;
        if ((this.baseApplicationId == null && other.baseApplicationId != null) || (this.baseApplicationId != null && !this.baseApplicationId.equals(other.baseApplicationId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.magna.xmbackend.entities.BaseApplicationsTbl[ baseApplicationId=" + baseApplicationId + " ]";
    }
    
}
