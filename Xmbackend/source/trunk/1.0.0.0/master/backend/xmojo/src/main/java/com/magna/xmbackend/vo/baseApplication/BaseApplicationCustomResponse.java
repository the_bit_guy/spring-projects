/**
 * 
 */
package com.magna.xmbackend.vo.baseApplication;

/**
 * @author Bhabadyuti Bal
 *
 */
public class BaseApplicationCustomResponse {
	
	private String baseApplicationId;
    private String name;
    private String status;
    private String platforms;
    private String program;
    
    public BaseApplicationCustomResponse() {
	}

	public String getBaseApplicationId() {
		return baseApplicationId;
	}

	public void setBaseApplicationId(String baseApplicationId) {
		this.baseApplicationId = baseApplicationId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getPlatforms() {
		return platforms;
	}

	public void setPlatforms(String platforms) {
		this.platforms = platforms;
	}

	public String getProgram() {
		return program;
	}

	public void setProgram(String program) {
		this.program = program;
	}
    
    

}
