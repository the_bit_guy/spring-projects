package com.magna.xmbackend.vo.permission;

import com.magna.xmbackend.entities.PermissionApiMappingTbl;

/**
 *
 * @author Admin
 */
public class PermissionApiResponse {

    private Iterable<PermissionApiMappingTbl> permissionApiMappingTbls;

    public PermissionApiResponse() {
    }

    /**
     *
     * @param permissionApiMappingTbls
     */
    public PermissionApiResponse(Iterable<PermissionApiMappingTbl> permissionApiMappingTbls) {
        this.permissionApiMappingTbls = permissionApiMappingTbls;
    }

    /**
     * @return the permissionApiMappingTbls
     */
    public Iterable<PermissionApiMappingTbl> getPermissionApiMappingTbls() {
        return permissionApiMappingTbls;
    }

    /**
     * @param permissionApiMappingTbls the permissionApiMappingTbls to set
     */
    public void setPermissionApiMappingTbls(Iterable<PermissionApiMappingTbl> permissionApiMappingTbls) {
        this.permissionApiMappingTbls = permissionApiMappingTbls;
    }

}
