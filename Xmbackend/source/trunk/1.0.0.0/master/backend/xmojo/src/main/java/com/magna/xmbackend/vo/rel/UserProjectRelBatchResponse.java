package com.magna.xmbackend.vo.rel;

import com.magna.xmbackend.entities.UserProjectRelTbl;
import java.util.List;
import java.util.Map;

/**
 *
 * @author vijay
 */
public class UserProjectRelBatchResponse {

    private List<UserProjectRelTbl> userProjectRelTbls;
    private List<Map<String, String>> statusMaps;
    private List<String> statusUpdatationFailedList;
    private List<String> statusUpdatationSuccessList;
    private List<String> userProjectRelId;

    /**
     * Instantiates a new user project rel batch response.
     */
    public UserProjectRelBatchResponse() {
    }
    
    /**
     *
     * @param userProjectRelTbls
     * @param statusMaps
     */
    public UserProjectRelBatchResponse(List<UserProjectRelTbl> userProjectRelTbls,
            List<Map<String, String>> statusMaps) {
        this.userProjectRelTbls = userProjectRelTbls;
        this.statusMaps = statusMaps;
    }
    
    public UserProjectRelBatchResponse(List<String> userProjectRelId, List<Map<String, String>> statusMaps, List<String> statusUpdatationSuccessList) {
    	this.userProjectRelId = userProjectRelId;
        this.statusMaps = statusMaps;
        this.statusUpdatationSuccessList = statusUpdatationSuccessList;
    }

    /**
     * @return the userProjectRelTbls
     */
    public final List<UserProjectRelTbl> getUserProjectRelTbls() {
        return userProjectRelTbls;
    }

    /**
     * @param userProjectRelTbls the userProjectRelTbls to set
     */
    public final void setUserProjectRelTbls(final List<UserProjectRelTbl> userProjectRelTbls) {
        this.userProjectRelTbls = userProjectRelTbls;
    }

    /**
     * @return the statusMaps
     */
    public final List<Map<String, String>> getStatusMap() {
        return statusMaps;
    }

    /**
     * @param statusMaps the statusMap to set
     */
    public final void setStatusMap(final List<Map<String, String>> statusMaps) {
        this.statusMaps = statusMaps;
    }

	/**
	 * @return the statusUpdatationFailedList
	 */
	public List<String> getStatusUpdatationFailedList() {
		return statusUpdatationFailedList;
	}

	/**
	 * @param statusUpdatationFailedList the statusUpdatationFailedList to set
	 */
	public void setStatusUpdatationFailedList(List<String> statusUpdatationFailedList) {
		this.statusUpdatationFailedList = statusUpdatationFailedList;
	}

	public List<String> getStatusUpdatationSuccessList() {
		return statusUpdatationSuccessList;
	}

	public void setStatusUpdatationSuccessList(List<String> statusUpdatationSuccessList) {
		this.statusUpdatationSuccessList = statusUpdatationSuccessList;
	}

	public List<String> getUserProjectRelId() {
		return userProjectRelId;
	}

	public void setUserProjectRelId(List<String> userProjectRelId) {
		this.userProjectRelId = userProjectRelId;
	}

}
