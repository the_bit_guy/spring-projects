/**
 * 
 */
package com.magna.xmbackend.response.rel.adminareastartapp;

import com.magna.xmbackend.entities.SiteAdminAreaRelTbl;
import com.magna.xmbackend.entities.StartApplicationsTbl;

/**
 * @author Bhabadyuti Bal
 *
 */
public class AdminAreaStartAppRelation {

	private String adminAreaStartAppRelId;
    private String status;
    private SiteAdminAreaRelTbl siteAdminAreaRelId;
    private StartApplicationsTbl startApplicationId;
    
    public AdminAreaStartAppRelation() {
	}

	/**
	 * @param adminAreaStartAppRelId
	 * @param status
	 * @param siteAdminAreaRelId
	 * @param startApplicationId
	 */
	public AdminAreaStartAppRelation(String adminAreaStartAppRelId, String status,
			SiteAdminAreaRelTbl siteAdminAreaRelId, StartApplicationsTbl startApplicationId) {
		this.adminAreaStartAppRelId = adminAreaStartAppRelId;
		this.status = status;
		this.siteAdminAreaRelId = siteAdminAreaRelId;
		this.startApplicationId = startApplicationId;
	}

	/**
	 * @return the adminAreaStartAppRelId
	 */
	public String getAdminAreaStartAppRelId() {
		return adminAreaStartAppRelId;
	}

	/**
	 * @param adminAreaStartAppRelId the adminAreaStartAppRelId to set
	 */
	public void setAdminAreaStartAppRelId(String adminAreaStartAppRelId) {
		this.adminAreaStartAppRelId = adminAreaStartAppRelId;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the siteAdminAreaRelId
	 */
	public SiteAdminAreaRelTbl getSiteAdminAreaRelId() {
		return siteAdminAreaRelId;
	}

	/**
	 * @param siteAdminAreaRelId the siteAdminAreaRelId to set
	 */
	public void setSiteAdminAreaRelId(SiteAdminAreaRelTbl siteAdminAreaRelId) {
		this.siteAdminAreaRelId = siteAdminAreaRelId;
	}

	/**
	 * @return the startApplicationId
	 */
	public StartApplicationsTbl getStartApplicationId() {
		return startApplicationId;
	}

	/**
	 * @param startApplicationId the startApplicationId to set
	 */
	public void setStartApplicationId(StartApplicationsTbl startApplicationId) {
		this.startApplicationId = startApplicationId;
	}
    
    
}
