package com.magna.xmbackend.vo.rel;

import com.magna.xmbackend.entities.UserUserAppRelTbl;
import java.util.List;
import java.util.Map;

/**
 *
 * @author vijay
 */
public class UserUserAppRelBatchResponse {

    private List<UserUserAppRelTbl> userUserAppRelTbls;
    private List<Map<String, String>> statusMaps;

    public UserUserAppRelBatchResponse() {
    }

    /**
     *
     * @param userUserAppRelTbls
     * @param statusMaps
     */
    public UserUserAppRelBatchResponse(List<UserUserAppRelTbl> userUserAppRelTbls,
            List<Map<String, String>> statusMaps) {
        this.userUserAppRelTbls = userUserAppRelTbls;
        this.statusMaps = statusMaps;
    }

    /**
     * @return the userUserAppRelTbls
     */
    public final List<UserUserAppRelTbl> getUserUserAppRelTbls() {
        return userUserAppRelTbls;
    }

    /**
     * @param userUserAppRelTbls the userUserAppRelTbls to set
     */
    public final void setUserUserAppRelTbls(final List<UserUserAppRelTbl> userUserAppRelTbls) {
        this.userUserAppRelTbls = userUserAppRelTbls;
    }

    /**
     * @return the statusMaps
     */
    public final List<Map<String, String>> getStatusMap() {
        return statusMaps;
    }

    /**
     * @param statusMaps the statusMap to set
     */
    public final void setStatusMap(final List<Map<String, String>> statusMaps) {
        this.statusMaps = statusMaps;
    }

}
