package com.magna.xmbackend.vo;

import java.io.Serializable;

public class BaseObject implements Serializable {

    private XMObject xMObject;
    private String id;

    public BaseObject() {
    }

    /**
     *
     * @param xMObject
     */
    public BaseObject(XMObject xMObject) {
        this.xMObject = xMObject;
    }

    /**
     * @return the xMObject
     */
    public final XMObject getxMObject() {
        return xMObject;
    }

    /**
     * @param xMObject the xMObject to set
     */
    public final void setxMObject(final XMObject xMObject) {
        this.xMObject = xMObject;
    }

    /**
     *
     * @return id
     */
    public final String getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public final void setId(final String id) {
        this.id = id;
    }

    /**
     *
     * @return object as string
     */
    @Override
    public String toString() {
        return new StringBuilder().append("BaseObject{")
                .append("xMObject=").append(xMObject)
                .append("id=").append(id).append("}").toString();
    }

}
