/**
 * 
 */
package com.magna.xmbackend.vo.notification;

import java.io.Serializable;
import java.util.Set;

/**
 * @author Bhabadyuti Bal
 *
 */
public class NotificationConfigResponse implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String notifEventName;
	private String notifEventId;
	private String notifEventStatus;
	private String notifEventDescription;
	private String notifConfigName;
	private String notifConfigStatus;
	private String notifConfigId;
	private String notifConfigEmailTemplateName;
	private String notifConfigEmailTemplateId;
	private String notifConfigEmailTemplateSubject;
	private String notifConfigEmailTemplateMsg;
	private Set<String> notifConfigEmailTemplateVariables;
	private Set<String> notifConfigToUsers;
	private Set<String> notifConfigCcUsers;
	
	public NotificationConfigResponse() {
	}
	
	
	public String getNotifEventName() {
		return notifEventName;
	}
	public void setNotifEventName(String notifEventName) {
		this.notifEventName = notifEventName;
	}
	public String getNotifEventId() {
		return notifEventId;
	}
	public void setNotifEventId(String notifEventId) {
		this.notifEventId = notifEventId;
	}
	public String getNotifEventStatus() {
		return notifEventStatus;
	}
	public void setNotifEventStatus(String notifEventStatus) {
		this.notifEventStatus = notifEventStatus;
	}
	public String getNotifEventDescription() {
		return notifEventDescription;
	}
	public void setNotifEventDescription(String notifEventDescription) {
		this.notifEventDescription = notifEventDescription;
	}
	public String getNotifConfigName() {
		return notifConfigName;
	}
	public void setNotifConfigName(String notifConfigName) {
		this.notifConfigName = notifConfigName;
	}
	public String getNotifConfigStatus() {
		return notifConfigStatus;
	}
	public void setNotifConfigStatus(String notifConfigStatus) {
		this.notifConfigStatus = notifConfigStatus;
	}
	public String getNotifConfigId() {
		return notifConfigId;
	}
	public void setNotifConfigId(String notifConfigId) {
		this.notifConfigId = notifConfigId;
	}
	public String getNotifConfigEmailTemplateName() {
		return notifConfigEmailTemplateName;
	}
	public void setNotifConfigEmailTemplateName(String notifConfigEmailTemplateName) {
		this.notifConfigEmailTemplateName = notifConfigEmailTemplateName;
	}
	public String getNotifConfigEmailTemplateId() {
		return notifConfigEmailTemplateId;
	}
	public void setNotifConfigEmailTemplateId(String notifConfigEmailTemplateId) {
		this.notifConfigEmailTemplateId = notifConfigEmailTemplateId;
	}
	public String getNotifConfigEmailTemplateSubject() {
		return notifConfigEmailTemplateSubject;
	}
	public void setNotifConfigEmailTemplateSubject(String notifConfigEmailTemplateSubject) {
		this.notifConfigEmailTemplateSubject = notifConfigEmailTemplateSubject;
	}
	public String getNotifConfigEmailTemplateMsg() {
		return notifConfigEmailTemplateMsg;
	}
	public void setNotifConfigEmailTemplateMsg(String notifConfigEmailTemplateMsg) {
		this.notifConfigEmailTemplateMsg = notifConfigEmailTemplateMsg;
	}
	public Set<String> getNotifConfigEmailTemplateVariables() {
		return notifConfigEmailTemplateVariables;
	}
	public void setNotifConfigEmailTemplateVariables(Set<String> notifConfigEmailTemplateVariables) {
		this.notifConfigEmailTemplateVariables = notifConfigEmailTemplateVariables;
	}
	public Set<String> getNotifConfigToUsers() {
		return notifConfigToUsers;
	}
	public void setNotifConfigToUsers(Set<String> notifConfigToUsers) {
		this.notifConfigToUsers = notifConfigToUsers;
	}
	public Set<String> getNotifConfigCcUsers() {
		return notifConfigCcUsers;
	}
	public void setNotifConfigCcUsers(Set<String> notifConfigCcUsers) {
		this.notifConfigCcUsers = notifConfigCcUsers;
	}
	
}
