/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonBackReference;

/**
 *
 * @author Admin
 */
@Entity
@Table(name = "USER_APPLICATIONS_TBL")
@NamedQueries({
    @NamedQuery(name = "UserApplicationsTbl.findAll", query = "SELECT u FROM UserApplicationsTbl u")})
public class UserApplicationsTbl implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "USER_APPLICATION_ID")
    private String userApplicationId;
    @Basic(optional = false)
    @Column(name = "NAME")
    private String name;
    @Column(name = "DESCRIPTION")
    private String description;
    @Basic(optional = false)
    @Column(name = "STATUS")
    private String status;
    @Column(name = "IS_PARENT")
    private String isParent;
    @Column(name = "IS_SINGLETON")
    private String isSingleton;
    @Column(name = "POSITION")
    private String position;
    @Basic(optional = false)
    @Column(name = "CREATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Basic(optional = false)
    @Column(name = "UPDATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;
    @JoinColumn(name = "BASE_APPLICATION_ID", referencedColumnName = "BASE_APPLICATION_ID")
    @ManyToOne
    private BaseApplicationsTbl baseApplicationId;
    @JoinColumn(name = "ICON_ID", referencedColumnName = "ICON_ID")
    @ManyToOne
    private IconsTbl iconId;
    @JsonBackReference
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userApplicationId")
    private Collection<AdminAreaUserAppRelTbl> adminAreaUserAppRelTblCollection;
    @JsonBackReference
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userApplicationId")
    private Collection<UserUserAppRelTbl> userUserAppRelTblCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userApplicationId")
    private Collection<UserAppTranslationTbl> userAppTranslationTblCollection;

    public UserApplicationsTbl() {
    }

    public UserApplicationsTbl(String userApplicationId) {
        this.userApplicationId = userApplicationId;
    }

    public UserApplicationsTbl(String userApplicationId, String name, String status, Date createDate, Date updateDate) {
        this.userApplicationId = userApplicationId;
        this.name = name;
        this.status = status;
        this.createDate = createDate;
        this.updateDate = updateDate;
    }

    public String getUserApplicationId() {
        return userApplicationId;
    }

    public void setUserApplicationId(String userApplicationId) {
        this.userApplicationId = userApplicationId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getIsParent() {
        return isParent;
    }

    public void setIsParent(String isParent) {
        this.isParent = isParent;
    }

    public String getIsSingleton() {
        return isSingleton;
    }

    public void setIsSingleton(String isSingleton) {
        this.isSingleton = isSingleton;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public BaseApplicationsTbl getBaseApplicationId() {
        return baseApplicationId;
    }

    public void setBaseApplicationId(BaseApplicationsTbl baseApplicationId) {
        this.baseApplicationId = baseApplicationId;
    }

    public IconsTbl getIconId() {
        return iconId;
    }

    public void setIconId(IconsTbl iconId) {
        this.iconId = iconId;
    }

    public Collection<AdminAreaUserAppRelTbl> getAdminAreaUserAppRelTblCollection() {
        return adminAreaUserAppRelTblCollection;
    }

    public void setAdminAreaUserAppRelTblCollection(Collection<AdminAreaUserAppRelTbl> adminAreaUserAppRelTblCollection) {
        this.adminAreaUserAppRelTblCollection = adminAreaUserAppRelTblCollection;
    }

    public Collection<UserUserAppRelTbl> getUserUserAppRelTblCollection() {
        return userUserAppRelTblCollection;
    }

    public void setUserUserAppRelTblCollection(Collection<UserUserAppRelTbl> userUserAppRelTblCollection) {
        this.userUserAppRelTblCollection = userUserAppRelTblCollection;
    }

    public Collection<UserAppTranslationTbl> getUserAppTranslationTblCollection() {
        return userAppTranslationTblCollection;
    }

    public void setUserAppTranslationTblCollection(Collection<UserAppTranslationTbl> userAppTranslationTblCollection) {
        this.userAppTranslationTblCollection = userAppTranslationTblCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (userApplicationId != null ? userApplicationId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserApplicationsTbl)) {
            return false;
        }
        UserApplicationsTbl other = (UserApplicationsTbl) object;
        if ((this.userApplicationId == null && other.userApplicationId != null) || (this.userApplicationId != null && !this.userApplicationId.equals(other.userApplicationId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.magna.xmbackend.entities.UserApplicationsTbl[ userApplicationId=" + userApplicationId + " ]";
    }
    
}
