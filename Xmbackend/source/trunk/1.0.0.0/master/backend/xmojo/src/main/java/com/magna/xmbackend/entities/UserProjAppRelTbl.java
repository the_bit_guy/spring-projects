/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author Admin
 */
@Entity
@Table(name = "USER_PROJ_APP_REL_TBL")
@NamedQueries({
    @NamedQuery(name = "UserProjAppRelTbl.findAll", query = "SELECT u FROM UserProjAppRelTbl u")})
public class UserProjAppRelTbl implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "USER_PROJ_APP_REL_ID")
    private String userProjAppRelId;
    @Basic(optional = false)
    @Column(name = "USER_REL_TYPE")
    private String userRelType;
    @JoinColumn(name = "ADMIN_AREA_PROJECT_REL_ID", referencedColumnName = "ADMIN_AREA_PROJECT_REL_ID")
    @ManyToOne(optional = false)
    private AdminAreaProjectRelTbl adminAreaProjectRelId;
    @JoinColumn(name = "PROJECT_APPLICATION_ID", referencedColumnName = "PROJECT_APPLICATION_ID")
    @ManyToOne(optional = false)
    private ProjectApplicationsTbl projectApplicationId;
    @JoinColumn(name = "USER_PROJECT_REL_ID", referencedColumnName = "USER_PROJECT_REL_ID")
    @ManyToOne(optional = false)
    private UserProjectRelTbl userProjectRelId;

    public UserProjAppRelTbl() {
    }

    public UserProjAppRelTbl(String userProjAppRelId) {
        this.userProjAppRelId = userProjAppRelId;
    }

    public UserProjAppRelTbl(String userProjAppRelId, String userRelType) {
        this.userProjAppRelId = userProjAppRelId;
        this.userRelType = userRelType;
    }

    public String getUserProjAppRelId() {
        return userProjAppRelId;
    }

    public void setUserProjAppRelId(String userProjAppRelId) {
        this.userProjAppRelId = userProjAppRelId;
    }

    public String getUserRelType() {
        return userRelType;
    }

    public void setUserRelType(String userRelType) {
        this.userRelType = userRelType;
    }

    public AdminAreaProjectRelTbl getAdminAreaProjectRelId() {
        return adminAreaProjectRelId;
    }

    public void setAdminAreaProjectRelId(AdminAreaProjectRelTbl adminAreaProjectRelId) {
        this.adminAreaProjectRelId = adminAreaProjectRelId;
    }

    public ProjectApplicationsTbl getProjectApplicationId() {
        return projectApplicationId;
    }

    public void setProjectApplicationId(ProjectApplicationsTbl projectApplicationId) {
        this.projectApplicationId = projectApplicationId;
    }

    public UserProjectRelTbl getUserProjectRelId() {
        return userProjectRelId;
    }

    public void setUserProjectRelId(UserProjectRelTbl userProjectRelId) {
        this.userProjectRelId = userProjectRelId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (userProjAppRelId != null ? userProjAppRelId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserProjAppRelTbl)) {
            return false;
        }
        UserProjAppRelTbl other = (UserProjAppRelTbl) object;
        if ((this.userProjAppRelId == null && other.userProjAppRelId != null) || (this.userProjAppRelId != null && !this.userProjAppRelId.equals(other.userProjAppRelId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.magna.xmbackend.entities.UserProjAppRelTbl[ userProjAppRelId=" + userProjAppRelId + " ]";
    }
    
}
