/**
 * 
 */
package com.magna.xmbackend.vo.userSettings;

/**
 * @author Bhabadyuti Bal
 *
 */
public class UserSettingsRequest {

	private String userSettingsId;
	private String siteId;
	private String projectId;
	private String settings;
	
	public UserSettingsRequest() {
	}

	/**
	 * 
	 * @param userSettingsId
	 * @param siteId
	 * @param projectId
	 * @param settings
	 */
	public UserSettingsRequest(String userSettingsId, String siteId, String projectId, String settings) {
		this.userSettingsId = userSettingsId;
		this.siteId = siteId;
		this.projectId = projectId;
		this.settings = settings;
	}

	/**
	 * @return the userSettingsId
	 */
	public String getUserSettingsId() {
		return userSettingsId;
	}

	/**
	 * @param userSettingsId the userSettingsId to set
	 */
	public void setUserSettingsId(String userSettingsId) {
		this.userSettingsId = userSettingsId;
	}

	/**
	 * @return the siteId
	 */
	public String getSiteId() {
		return siteId;
	}

	/**
	 * @param siteId the siteId to set
	 */
	public void setSiteId(String siteId) {
		this.siteId = siteId;
	}

	/**
	 * @return the projectId
	 */
	public String getProjectId() {
		return projectId;
	}

	/**
	 * @param projectId the projectId to set
	 */
	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	/**
	 * @return the settings
	 */
	public String getSettings() {
		return settings;
	}

	/**
	 * @param settings the settings to set
	 */
	public void setSettings(String settings) {
		this.settings = settings;
	}
	
}
