/**
 * 
 */
package com.magna.xmbackend.vo.css;

/**
 * @author Bhabadyuti Bal
 *
 */
public class CssSettingsRequest {
	
	private String id;
	private boolean isApplied;
	private String fileName;
	private String fileContent;
	
	
	/**
	 * @return the isApplied
	 */
	public boolean isApplied() {
		return isApplied;
	}
	/**
	 * @param isApplied the isApplied to set
	 */
	public void setApplied(boolean isApplied) {
		this.isApplied = isApplied;
	}
	/**
	 * @return the file
	 */
	/*public File getFile() {
		return file;
	}
	*//**
	 * @param file the file to set
	 * @throws IOException 
	 *//*
	public void setFile(File file) throws IOException {
			this.fileName = file.getName();
			this.file = file;
			this.fileContent = FileUtils.readFileToString(file);
	}*/
	/**
	 * @return the fileContent
	 */
	public String getFileContent() {
		return fileContent;
	}
	/**
	 * @param fileContent the fileContent to set
	 */
	public void setFileContent(String fileContent) {
		this.fileContent = fileContent;
	}
	/**
	 * @return the fileName
	 */
	public String getFileName() {
		return fileName;
	}
	/**
	 * @param fileName the fileName to set
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	
}
