/**
 * 
 */
package com.magna.xmbackend.exception;

import java.util.Map;

/**
 * @author Bhabadyuti Bal
 *
 */
public class CannotDeleteRelationException extends RuntimeException {

	private String errCode;
	private String[] param;

	public CannotDeleteRelationException() {
		super();
	}

	/**
	 * @param errCode
	 * @param param
	 */
	public CannotDeleteRelationException(String errCode, String[] param) {
		this.errCode = errCode;
		this.param = param;
	}

	/**
	 *
	 * @param message
	 * @param errCode
	 * @param param
	 */
	public CannotDeleteRelationException(String message, String errCode, String[] param) {
		super(message);
		this.errCode = errCode;
		this.param = param;
	}

	/**
	 *
	 * @return String
	 */
	@Override
	public String toString() {
		return super.toString();
	}

	/**
	 *
	 * @return String
	 */
	@Override
	public String getMessage() {
		return super.getMessage() + " with ErrorCode :" + this.errCode;
	}

	/**
	 * @return the errCode
	 */
	public String getErrCode() {
		return errCode;
	}

	/**
	 * @return the param
	 */
	public String[] getParam() {
		return param;
	}

}
