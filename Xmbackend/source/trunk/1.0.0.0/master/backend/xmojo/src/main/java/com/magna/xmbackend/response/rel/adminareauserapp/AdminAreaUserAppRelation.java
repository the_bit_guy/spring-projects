/**
 * 
 */
package com.magna.xmbackend.response.rel.adminareauserapp;

import com.magna.xmbackend.entities.SiteAdminAreaRelTbl;
import com.magna.xmbackend.entities.UserApplicationsTbl;

/**
 * @author Bhabadyuti Bal
 *
 */
public class AdminAreaUserAppRelation {

	private String adminAreaUserAppRelId;
	private String status;
	private String relType;
	private SiteAdminAreaRelTbl siteAdminAreaRelId;
	private UserApplicationsTbl userApplicationId;
	
	public AdminAreaUserAppRelation() {
	}

	/**
	 * @param adminAreaUserAppRelId
	 * @param status
	 * @param relType
	 * @param siteAdminAreaRelId
	 * @param userApplicationId
	 */
	public AdminAreaUserAppRelation(String adminAreaUserAppRelId, String status, String relType,
			SiteAdminAreaRelTbl siteAdminAreaRelId, UserApplicationsTbl userApplicationId) {
		this.adminAreaUserAppRelId = adminAreaUserAppRelId;
		this.status = status;
		this.relType = relType;
		this.siteAdminAreaRelId = siteAdminAreaRelId;
		this.userApplicationId = userApplicationId;
	}

	/**
	 * @return the adminAreaUserAppRelId
	 */
	public String getAdminAreaUserAppRelId() {
		return adminAreaUserAppRelId;
	}

	/**
	 * @param adminAreaUserAppRelId the adminAreaUserAppRelId to set
	 */
	public void setAdminAreaUserAppRelId(String adminAreaUserAppRelId) {
		this.adminAreaUserAppRelId = adminAreaUserAppRelId;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the relType
	 */
	public String getRelType() {
		return relType;
	}

	/**
	 * @param relType the relType to set
	 */
	public void setRelType(String relType) {
		this.relType = relType;
	}

	/**
	 * @return the siteAdminAreaRelId
	 */
	public SiteAdminAreaRelTbl getSiteAdminAreaRelId() {
		return siteAdminAreaRelId;
	}

	/**
	 * @param siteAdminAreaRelId the siteAdminAreaRelId to set
	 */
	public void setSiteAdminAreaRelId(SiteAdminAreaRelTbl siteAdminAreaRelId) {
		this.siteAdminAreaRelId = siteAdminAreaRelId;
	}

	/**
	 * @return the userApplicationId
	 */
	public UserApplicationsTbl getUserApplicationId() {
		return userApplicationId;
	}

	/**
	 * @param userApplicationId the userApplicationId to set
	 */
	public void setUserApplicationId(UserApplicationsTbl userApplicationId) {
		this.userApplicationId = userApplicationId;
	}
}
