/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author Admin
 */
@Entity
@Table(name = "ADMIN_AREA_PROJ_APP_REL_TBL")
@NamedQueries({
    @NamedQuery(name = "AdminAreaProjAppRelTbl.findAll", query = "SELECT a FROM AdminAreaProjAppRelTbl a")})
public class AdminAreaProjAppRelTbl implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "ADMIN_AREA_PROJ_APP_REL_ID")
    private String adminAreaProjAppRelId;
    @Basic(optional = false)
    @Column(name = "STATUS")
    private String status;
    @Basic(optional = false)
    @Column(name = "REL_TYPE")
    private String relType;
    @JoinColumn(name = "ADMIN_AREA_PROJECT_REL_ID", referencedColumnName = "ADMIN_AREA_PROJECT_REL_ID")
    @ManyToOne(optional = false)
    private AdminAreaProjectRelTbl adminAreaProjectRelId;
    @JoinColumn(name = "PROJECT_APPLICATION_ID", referencedColumnName = "PROJECT_APPLICATION_ID")
    @ManyToOne(optional = false)
    private ProjectApplicationsTbl projectApplicationId;

    public AdminAreaProjAppRelTbl() {
    }

    public AdminAreaProjAppRelTbl(String adminAreaProjAppRelId) {
        this.adminAreaProjAppRelId = adminAreaProjAppRelId;
    }

    public AdminAreaProjAppRelTbl(String adminAreaProjAppRelId, String status, String relType) {
        this.adminAreaProjAppRelId = adminAreaProjAppRelId;
        this.status = status;
        this.relType = relType;
    }

    public String getAdminAreaProjAppRelId() {
        return adminAreaProjAppRelId;
    }

    public void setAdminAreaProjAppRelId(String adminAreaProjAppRelId) {
        this.adminAreaProjAppRelId = adminAreaProjAppRelId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRelType() {
        return relType;
    }

    public void setRelType(String relType) {
        this.relType = relType;
    }

    public AdminAreaProjectRelTbl getAdminAreaProjectRelId() {
        return adminAreaProjectRelId;
    }

    public void setAdminAreaProjectRelId(AdminAreaProjectRelTbl adminAreaProjectRelId) {
        this.adminAreaProjectRelId = adminAreaProjectRelId;
    }

    public ProjectApplicationsTbl getProjectApplicationId() {
        return projectApplicationId;
    }

    public void setProjectApplicationId(ProjectApplicationsTbl projectApplicationId) {
        this.projectApplicationId = projectApplicationId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (adminAreaProjAppRelId != null ? adminAreaProjAppRelId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AdminAreaProjAppRelTbl)) {
            return false;
        }
        AdminAreaProjAppRelTbl other = (AdminAreaProjAppRelTbl) object;
        if ((this.adminAreaProjAppRelId == null && other.adminAreaProjAppRelId != null) || (this.adminAreaProjAppRelId != null && !this.adminAreaProjAppRelId.equals(other.adminAreaProjAppRelId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.magna.xmbackend.entities.AdminAreaProjAppRelTbl[ adminAreaProjAppRelId=" + adminAreaProjAppRelId + " ]";
    }
    
}
