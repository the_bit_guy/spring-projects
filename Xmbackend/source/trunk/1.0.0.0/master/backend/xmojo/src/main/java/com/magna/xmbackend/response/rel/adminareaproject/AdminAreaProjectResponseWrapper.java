package com.magna.xmbackend.response.rel.adminareaproject;

import java.util.List;

/**
 *
 * @author dhana
 */
public class AdminAreaProjectResponseWrapper {

    private List<AdminAreaProjectResponse> adminAreaProjectResponses;
    private List<String> statusUpdatationFailedList;
    
    public AdminAreaProjectResponseWrapper() {
    }
    
    public AdminAreaProjectResponseWrapper(List<AdminAreaProjectResponse> adminAreaProjectResponses) {
    	this.adminAreaProjectResponses = adminAreaProjectResponses;
    }

	/**
	 * @return the adminAreaProjectResponses
	 */
	public List<AdminAreaProjectResponse> getAdminAreaProjectResponses() {
		return adminAreaProjectResponses;
	}

	/**
	 * @param adminAreaProjectResponses the adminAreaProjectResponses to set
	 */
	public void setAdminAreaProjectResponses(List<AdminAreaProjectResponse> adminAreaProjectResponses) {
		this.adminAreaProjectResponses = adminAreaProjectResponses;
	}

	/**
	 * @return the statusUpdatationFailedList
	 */
	public List<String> getStatusUpdatationFailedList() {
		return statusUpdatationFailedList;
	}

	/**
	 * @param statusUpdatationFailedList the statusUpdatationFailedList to set
	 */
	public void setStatusUpdatationFailedList(List<String> statusUpdatationFailedList) {
		this.statusUpdatationFailedList = statusUpdatationFailedList;
	}
	
}
