package com.magna.xmbackend.vo.rel;

import com.magna.xmbackend.entities.UserProjAppRelTbl;
import java.util.List;
import java.util.Map;

/**
 *
 * @author dhana
 */
public class UserProjectAppRelBatchResponse {

    private List<UserProjAppRelTbl> userProjAppRelTbls;
    private List<Map<String, String>> statusMaps;

    public UserProjectAppRelBatchResponse() {
    }

    /**
     *
     * @param userProjAppRelTbls
     * @param statusMaps
     */
    public UserProjectAppRelBatchResponse(List<UserProjAppRelTbl> userProjAppRelTbls,
            List<Map<String, String>> statusMaps) {
        this.userProjAppRelTbls = userProjAppRelTbls;
        this.statusMaps = statusMaps;
    }

    /**
     * @return the userProjAppRelTbls
     */
    public final List<UserProjAppRelTbl> getUserProjectAppRelTbls() {
        return userProjAppRelTbls;
    }

    /**
     * @param userProjAppRelTbls the userProjAppRelTbls to set
     */
    public final void setUserProjectAppRelTbls(final List<UserProjAppRelTbl> userProjAppRelTbls) {
        this.userProjAppRelTbls = userProjAppRelTbls;
    }

    /**
     * @return the statusMaps
     */
    public final List<Map<String, String>> getStatusMap() {
        return statusMaps;
    }

    /**
     * @param statusMaps the statusMap to set
     */
    public final void setStatusMap(final List<Map<String, String>> statusMaps) {
        this.statusMaps = statusMaps;
    }

}
