package com.magna.xmbackend.vo.enums;

/**
 *
 * @author Admin
 */
public enum ApplicationRelationType {
    NOTFIXED, FIXED, PROTECTED
}
