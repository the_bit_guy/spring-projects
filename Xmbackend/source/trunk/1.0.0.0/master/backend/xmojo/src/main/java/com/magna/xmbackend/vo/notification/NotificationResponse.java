/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.vo.notification;

import java.util.List;
import java.util.Map;

import com.magna.xmbackend.entities.EmailNotificationConfigTbl;

/**
 *
 * @author dhana
 */
public class NotificationResponse {

	private String description;
	private String eventStatus;
	private List<EmailNotificationConfigTbl> emailNotificationConfigTbls;
    private List<Map<String, String>> statusMaps;

    public NotificationResponse() {
	}
    
    
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}


	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}


	/**
	 * @return the eventStatus
	 */
	public String getEventStatus() {
		return eventStatus;
	}


	/**
	 * @param eventStatus the eventStatus to set
	 */
	public void setEventStatus(String eventStatus) {
		this.eventStatus = eventStatus;
	}


	/**
	 * @param emailNotificationConfigTbls
	 * @param statusMaps
	 */
	public NotificationResponse(List<EmailNotificationConfigTbl> emailNotificationConfigTbls,
			List<Map<String, String>> statusMaps) {
		super();
		this.emailNotificationConfigTbls = emailNotificationConfigTbls;
		this.statusMaps = statusMaps;
	}
	
	/**
	 * @param emailNotificationConfigTbls
	 */
	public NotificationResponse(List<EmailNotificationConfigTbl> emailNotificationConfigTbls) {
		super();
		this.emailNotificationConfigTbls = emailNotificationConfigTbls;
	}



	/**
	 * @return the emailNotificationConfigTbls
	 */
	public List<EmailNotificationConfigTbl> getEmailNotificationConfigTbls() {
		return emailNotificationConfigTbls;
	}

	/**
	 * @param emailNotificationConfigTbls the emailNotificationConfigTbls to set
	 */
	public void setEmailNotificationConfigTbls(List<EmailNotificationConfigTbl> emailNotificationConfigTbls) {
		this.emailNotificationConfigTbls = emailNotificationConfigTbls;
	}

	/**
	 * @return the statusMaps
	 */
	public List<Map<String, String>> getStatusMaps() {
		return statusMaps;
	}

	/**
	 * @param statusMaps the statusMaps to set
	 */
	public void setStatusMaps(List<Map<String, String>> statusMaps) {
		this.statusMaps = statusMaps;
	}
    
    
    
}
