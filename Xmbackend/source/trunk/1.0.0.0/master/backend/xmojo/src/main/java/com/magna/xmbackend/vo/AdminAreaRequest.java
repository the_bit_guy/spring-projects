/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.vo;

import java.io.Serializable;
import java.util.List;

public class AdminAreaRequest extends BaseObject implements Serializable {

    private List<AdminAreaPojo> adminAreaPojos;

    public AdminAreaRequest() {
    }

    public AdminAreaRequest(List<AdminAreaPojo> adminAreaPojos) {
        this.adminAreaPojos = adminAreaPojos;
    }

    public List<AdminAreaPojo> getAdminAreaPojos() {
        return adminAreaPojos;
    }

    public void setAdminAreaPojos(List<AdminAreaPojo> adminAreaPojos) {
        this.adminAreaPojos = adminAreaPojos;
    }

}
