/**
 * 
 */
package com.magna.xmbackend.vo.rearrange;

import com.magna.xmbackend.entities.RearrangeApplicationsTbl;

/**
 * @author Bhabadyuti Bal
 *
 */
public class RearrangeApplicationsResponse {

	private RearrangeApplicationsTbl rearrangeApplicationsTbl;
	private String isRearrangeSettingsAvailable = "false";
	
	public RearrangeApplicationsResponse() {
	}
	
	public RearrangeApplicationsResponse(RearrangeApplicationsTbl rearrangeApplicationsTbl) {
		super();
		this.rearrangeApplicationsTbl = rearrangeApplicationsTbl;
	}

	/**
	 * @return the rearrangeApplicationsTbl
	 */
	public RearrangeApplicationsTbl getRearrangeApplicationsTbl() {
		return rearrangeApplicationsTbl;
	}

	/**
	 * @param rearrangeApplicationsTbl the rearrangeApplicationsTbl to set
	 */
	public void setRearrangeApplicationsTbl(RearrangeApplicationsTbl rearrangeApplicationsTbl) {
		this.rearrangeApplicationsTbl = rearrangeApplicationsTbl;
	}

	/**
	 * @return the isRearrangeSettingsAvailable
	 */
	public String getIsRearrangeSettingsAvailable() {
		return isRearrangeSettingsAvailable;
	}

	/**
	 * @param isRearrangeSettingsAvailable the isRearrangeSettingsAvailable to set
	 */
	public void setIsRearrangeSettingsAvailable(String isRearrangeSettingsAvailable) {
		this.isRearrangeSettingsAvailable = isRearrangeSettingsAvailable;
	}
	
	
}
