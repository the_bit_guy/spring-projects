/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

/**
 *
 * @author Admin
 */
@Entity
@Table(name = "SITE_ADMIN_AREA_REL_TBL")
@NamedQueries({
    @NamedQuery(name = "SiteAdminAreaRelTbl.findAll", query = "SELECT s FROM SiteAdminAreaRelTbl s")})
public class SiteAdminAreaRelTbl implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "SITE_ADMIN_AREA_REL_ID")
    private String siteAdminAreaRelId;
    @Basic(optional = false)
    @Column(name = "STATUS")
    private String status;
    @JsonBackReference
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "siteAdminAreaRelId")
    private Collection<AdminAreaProjectRelTbl> adminAreaProjectRelTblCollection;
    @JsonBackReference
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "siteAdminAreaRelId")
    private Collection<AdminAreaUserAppRelTbl> adminAreaUserAppRelTblCollection;
    @JoinColumn(name = "ADMIN_AREA_ID", referencedColumnName = "ADMIN_AREA_ID")
    @ManyToOne(optional = false)
    private AdminAreasTbl adminAreaId;
    @JoinColumn(name = "SITE_ID", referencedColumnName = "SITE_ID")
    @ManyToOne(optional = false)
    private SitesTbl siteId;
    @JsonBackReference
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "siteAdminAreaRelId")
    private Collection<UserUserAppRelTbl> userUserAppRelTblCollection;
    @JsonBackReference
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "siteAdminAreaRelId")
    private Collection<AdminAreaStartAppRelTbl> adminAreaStartAppRelTblCollection;

    public SiteAdminAreaRelTbl() {
    }

    public SiteAdminAreaRelTbl(String siteAdminAreaRelId) {
        this.siteAdminAreaRelId = siteAdminAreaRelId;
    }

    public SiteAdminAreaRelTbl(String siteAdminAreaRelId, String status) {
        this.siteAdminAreaRelId = siteAdminAreaRelId;
        this.status = status;
    }

    public String getSiteAdminAreaRelId() {
        return siteAdminAreaRelId;
    }

    public void setSiteAdminAreaRelId(String siteAdminAreaRelId) {
        this.siteAdminAreaRelId = siteAdminAreaRelId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Collection<AdminAreaProjectRelTbl> getAdminAreaProjectRelTblCollection() {
        return adminAreaProjectRelTblCollection;
    }

    public void setAdminAreaProjectRelTblCollection(Collection<AdminAreaProjectRelTbl> adminAreaProjectRelTblCollection) {
        this.adminAreaProjectRelTblCollection = adminAreaProjectRelTblCollection;
    }

    public Collection<AdminAreaUserAppRelTbl> getAdminAreaUserAppRelTblCollection() {
        return adminAreaUserAppRelTblCollection;
    }

    public void setAdminAreaUserAppRelTblCollection(Collection<AdminAreaUserAppRelTbl> adminAreaUserAppRelTblCollection) {
        this.adminAreaUserAppRelTblCollection = adminAreaUserAppRelTblCollection;
    }

    public AdminAreasTbl getAdminAreaId() {
        return adminAreaId;
    }

    public void setAdminAreaId(AdminAreasTbl adminAreaId) {
        this.adminAreaId = adminAreaId;
    }

    public SitesTbl getSiteId() {
        return siteId;
    }

    public void setSiteId(SitesTbl siteId) {
        this.siteId = siteId;
    }

    public Collection<UserUserAppRelTbl> getUserUserAppRelTblCollection() {
        return userUserAppRelTblCollection;
    }

    public void setUserUserAppRelTblCollection(Collection<UserUserAppRelTbl> userUserAppRelTblCollection) {
        this.userUserAppRelTblCollection = userUserAppRelTblCollection;
    }

    public Collection<AdminAreaStartAppRelTbl> getAdminAreaStartAppRelTblCollection() {
        return adminAreaStartAppRelTblCollection;
    }

    public void setAdminAreaStartAppRelTblCollection(Collection<AdminAreaStartAppRelTbl> adminAreaStartAppRelTblCollection) {
        this.adminAreaStartAppRelTblCollection = adminAreaStartAppRelTblCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (siteAdminAreaRelId != null ? siteAdminAreaRelId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SiteAdminAreaRelTbl)) {
            return false;
        }
        SiteAdminAreaRelTbl other = (SiteAdminAreaRelTbl) object;
        if ((this.siteAdminAreaRelId == null && other.siteAdminAreaRelId != null) || (this.siteAdminAreaRelId != null && !this.siteAdminAreaRelId.equals(other.siteAdminAreaRelId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.magna.xmbackend.entities.SiteAdminAreaRelTbl[ siteAdminAreaRelId=" + siteAdminAreaRelId + " ]";
    }
    
}
