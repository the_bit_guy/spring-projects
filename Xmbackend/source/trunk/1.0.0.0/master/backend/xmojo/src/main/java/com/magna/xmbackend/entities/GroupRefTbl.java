/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.magna.xmbackend.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Admin
 */
@Entity
@Table(name = "GROUP_REF_TBL")
@NamedQueries({
    @NamedQuery(name = "GroupRefTbl.findAll", query = "SELECT g FROM GroupRefTbl g")})
public class GroupRefTbl implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "GROUP_REF_ID")
    private String groupRefId;
    @Column(name = "OBJECT_ID")
    private String objectId;
    @Basic(optional = false)
    @Column(name = "CREATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Basic(optional = false)
    @Column(name = "UPDATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;
    @JoinColumn(name = "GROUP_ID", referencedColumnName = "GROUP_ID")
    @ManyToOne
    private GroupsTbl groupId;

    public GroupRefTbl() {
    }

    public GroupRefTbl(String groupRefId) {
        this.groupRefId = groupRefId;
    }

    public GroupRefTbl(String groupRefId, Date createDate, Date updateDate) {
        this.groupRefId = groupRefId;
        this.createDate = createDate;
        this.updateDate = updateDate;
    }

    public String getGroupRefId() {
        return groupRefId;
    }

    public void setGroupRefId(String groupRefId) {
        this.groupRefId = groupRefId;
    }

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public GroupsTbl getGroupId() {
        return groupId;
    }

    public void setGroupId(GroupsTbl groupId) {
        this.groupId = groupId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (groupRefId != null ? groupRefId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GroupRefTbl)) {
            return false;
        }
        GroupRefTbl other = (GroupRefTbl) object;
        if ((this.groupRefId == null && other.groupRefId != null) || (this.groupRefId != null && !this.groupRefId.equals(other.groupRefId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.magna.xmbackend.entities.GroupRefTbl[ groupRefId=" + groupRefId + " ]";
    }
    
}
