package com.piterion.security.manager.linux;

import java.security.Key;
import java.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

// TODO: Auto-generated Javadoc
/**
 * The Class TicketManager.
 */
public class TicketManager {

	/** The delimiter. */
	private String delimiter = "@@@@@@@@@@@@@@@";

	/** The algo. */
	private final String ALGO = "AES";

	/** The key. */
	private final String KEY = "o84A7AZG9%#O$1pe";

	/**
	 * Generate ticket.
	 *
	 * @return the string
	 * @throws Exception
	 *             the exception
	 */
	public String generateTicket() throws Exception {
		String ticket = null;
		String username = null;
		String machinename = getHostName();
		UnixLogin unixLogin = new UnixLogin();
		username = unixLogin.getUsername();
		if (username != null && machinename != null) {
			ticket = encrypt(username + delimiter + machinename);
		}

		return ticket;
	}

	/**
	 * Encrypt.
	 *
	 * @param Data
	 *            the data
	 * @return the string
	 * @throws Exception
	 *             the exception
	 */
	public String encrypt(String Data) throws Exception {
		String encryptedValue = null;
		Key key = generateKey();
		Cipher c = Cipher.getInstance(ALGO);
		c.init(Cipher.ENCRYPT_MODE, key);
		byte[] encVal = c.doFinal(Data.getBytes());
		encryptedValue = Base64.getEncoder().encodeToString(encVal);
		return encryptedValue;
	}

	/**
	 * Generate key.
	 *
	 * @return the key
	 * @throws Exception
	 *             the exception
	 */
	private Key generateKey() throws Exception {
		Key key = new SecretKeySpec(KEY.getBytes(), ALGO);
		return key;
	}

	/**
	 * Gets the host name.
	 *
	 * @return the host name
	 * @throws Exception
	 *             the exception
	 */
	private String getHostName() throws Exception {
		String hostname = null;
		java.net.InetAddress localMachine = java.net.InetAddress.getLocalHost();
		if ((hostname = localMachine.getHostName()) != null) {
			hostname = hostname.trim();
		}
		return hostname;
	}
}
