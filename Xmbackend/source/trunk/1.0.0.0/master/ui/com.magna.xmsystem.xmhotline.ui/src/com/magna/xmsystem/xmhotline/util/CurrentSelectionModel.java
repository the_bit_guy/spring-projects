package com.magna.xmsystem.xmhotline.util;

/**
 * The Class CurrentSelectionModel.
 * 
 * @author shashwat.anand
 */
public class CurrentSelectionModel {
	
	/** The current site combo index. */
	private int currentSiteComboIndex;
	
	/** The current site name. */
	private String currentSiteName;
	
	/** The current site id. */
	private String currentSiteId;
	
	/** The site filter text. */
	private String siteFilterText;
	
	/** The current admin area combo index. */
	private int currentAdminAreaComboIndex;
	
	/** The current admin area name. */
	private String currentAdminAreaName;
	
	/** The current admin area id. */
	private String currentAdminAreaId;
	
	/** The admin area filter text. */
	private String adminAreaFilterText;
	
	/** The current user combo index. */
	private int currentUserComboIndex;
	
	/** The current user name. */
	private String currentUserName;
	
	/** The current user id. */
	private String currentUserId;
	
	/** The user filter text. */
	private String userFilterText;
	
	/** The current project combo index. */
	private int currentProjectComboIndex;
	
	/** The current project id. */
	private String currentProjectId;
	
	/** The current project name. */
	private String currentProjectName;
	
	/** The project filter text. */
	private String projectFilterText;

	/**
	 * Gets the current site combo index.
	 *
	 * @return the currentSiteComboIndex
	 */
	public int getCurrentSiteComboIndex() {
		return currentSiteComboIndex;
	}

	/**
	 * Sets the current site combo index.
	 *
	 * @param currentSiteComboIndex  the currentSiteComboIndex to set
	 */
	public void setCurrentSiteComboIndex(int currentSiteComboIndex) {
		this.currentSiteComboIndex = currentSiteComboIndex;
	}

	/**
	 * Gets the curent site name.
	 *
	 * @return the currentSiteName
	 */
	public String getCurrentSiteName() {
		return currentSiteName;
	}

	/**
	 * Sets the curent site name.
	 *
	 * @param curentSiteName  the currentSiteName to set
	 */
	public void setCurentSiteName(String currentSiteName) {
		this.currentSiteName = currentSiteName;
	}

	/**
	 * Gets the current site id.
	 *
	 * @return the currentSiteId
	 */
	public String getCurrentSiteId() {
		return currentSiteId;
	}

	/**
	 * Sets the current site id.
	 *
	 * @param currentSiteId   the currentSiteId to set
	 */
	public void setCurrentSiteId(String currentSiteId) {
		this.currentSiteId = currentSiteId;
	}

	/**
	 * Gets the site filter text.
	 *
	 * @return the siteFilterText
	 */
	public String getSiteFilterText() {
		return siteFilterText;
	}

	/**
	 * Sets the site filter text.
	 *
	 * @param siteFilterText    the siteFilterText to set
	 */
	public void setSiteFilterText(String siteFilterText) {
		this.siteFilterText = siteFilterText;
	}

	/**
	 * Gets the current admin area combo index.
	 *
	 * @return the currentAdminAreaComboIndex
	 */
	public int getCurrentAdminAreaComboIndex() {
		return currentAdminAreaComboIndex;
	}

	/**
	 * Sets the current admin area combo index.
	 *
	 * @param currentAdminAreaComboIndex   the currentAdminAreaComboIndex to set
	 */
	public void setCurrentAdminAreaComboIndex(int currentAdminAreaComboIndex) {
		this.currentAdminAreaComboIndex = currentAdminAreaComboIndex;
	}

	/**
	 * Gets the current admin area name.
	 *
	 * @return the currentAdminAreaName
	 */
	public String getCurrentAdminAreaName() {
		return currentAdminAreaName;
	}

	/**
	 * Sets the current admin area name.
	 *
	 * @param currentAdminAreaName   the currentAdminAreaName to set
	 */
	public void setCurrentAdminAreaName(String currentAdminAreaName) {
		this.currentAdminAreaName = currentAdminAreaName;
	}

	/**
	 * Gets the current admin area id.
	 *
	 * @return the currentAdminAreaId
	 */
	public String getCurrentAdminAreaId() {
		return currentAdminAreaId;
	}

	/**
	 * Sets the current admin area id.
	 *
	 * @param currentAdminAreaId the currentAdminAreaId to set
	 */
	public void setCurrentAdminAreaId(String currentAdminAreaId) {
		this.currentAdminAreaId = currentAdminAreaId;
	}

	/**
	 * Gets the admin area filter text.
	 *
	 * @return the adminAreaFilterText
	 */
	public String getAdminAreaFilterText() {
		return adminAreaFilterText;
	}

	/**
	 * Sets the admin area filter text.
	 *
	 * @param adminAreaFilterText   the adminAreaFilterText to set
	 */
	public void setAdminAreaFilterText(String adminAreaFilterText) {
		this.adminAreaFilterText = adminAreaFilterText;
	}

	/**
	 * Gets the current user combo index.
	 *
	 * @return the currentUserComboIndex
	 */
	public int getCurrentUserComboIndex() {
		return currentUserComboIndex;
	}

	/**
	 * Sets the current user combo index.
	 *
	 * @param currentUserComboIndex   the currentUserComboIndex to set
	 */
	public void setCurrentUserComboIndex(int currentUserComboIndex) {
		this.currentUserComboIndex = currentUserComboIndex;
	}

	/**
	 * Gets the current user name.
	 *
	 * @return the currentUserName
	 */
	public String getCurrentUserName() {
		return currentUserName;
	}

	/**
	 * Sets the current user name.
	 *
	 * @param currentUserName   the currentUserName to set
	 */
	public void setCurrentUserName(String currentUserName) {
		this.currentUserName = currentUserName;
	}

	/**
	 * Gets the current user id.
	 *
	 * @return the currentUserId
	 */
	public String getCurrentUserId() {
		return currentUserId;
	}

	/**
	 * Sets the current user id.
	 *
	 * @param currentUserId   the currentUserId to set
	 */
	public void setCurrentUserId(String currentUserId) {
		this.currentUserId = currentUserId;
	}

	/**
	 * Gets the user filter text.
	 *
	 * @return the userFilterText
	 */
	public String getUserFilterText() {
		return userFilterText;
	}

	/**
	 * Sets the user filter text.
	 *
	 * @param userFilterText  the userFilterText to set
	 */
	public void setUserFilterText(String userFilterText) {
		this.userFilterText = userFilterText;
	}

	/**
	 * Gets the current project combo index.
	 *
	 * @return the currentProjectComboIndex
	 */
	public int getCurrentProjectComboIndex() {
		return currentProjectComboIndex;
	}

	/**
	 * Sets the current project combo index.
	 *
	 * @param currentProjectComboIndex  the currentProjectComboIndex to set
	 */
	public void setCurrentProjectComboIndex(int currentProjectComboIndex) {
		this.currentProjectComboIndex = currentProjectComboIndex;
	}

	/**
	 * Gets the current project id.
	 *
	 * @return the currentProjectId
	 */
	public String getCurrentProjectId() {
		return currentProjectId;
	}

	/**
	 * Sets the current project id.
	 *
	 * @param currentProjectId  the currentProjectId to set
	 */
	public void setCurrentProjectId(String currentProjectId) {
		this.currentProjectId = currentProjectId;
	}

	/**
	 * Gets the current project name.
	 *
	 * @return the currentProjectName
	 */
	public String getCurrentProjectName() {
		return currentProjectName;
	}

	/**
	 * Sets the current project name.
	 *
	 * @param currentProjectName  the currentProjectName to set
	 */
	public void setCurrentProjectName(String currentProjectName) {
		this.currentProjectName = currentProjectName;
	}

	/**
	 * Gets the project filter text.
	 *
	 * @return the projectFilterText
	 */
	public String getProjectFilterText() {
		return projectFilterText;
	}

	/**
	 * Sets the project filter text.
	 *
	 * @param projectFilterText the projectFilterText to set
	 */
	public void setProjectFilterText(String projectFilterText) {
		this.projectFilterText = projectFilterText;
	}
}
