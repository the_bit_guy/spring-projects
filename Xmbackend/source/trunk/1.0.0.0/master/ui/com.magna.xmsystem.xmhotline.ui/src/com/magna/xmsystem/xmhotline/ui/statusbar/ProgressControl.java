package com.magna.xmsystem.xmhotline.ui.statusbar;

import java.util.Objects;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.jobs.IJobChangeEvent;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.core.runtime.jobs.JobChangeAdapter;
import org.eclipse.core.runtime.jobs.ProgressProvider;
import org.eclipse.e4.ui.di.UISynchronize;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.ProgressBar;

/**
 * Process Control class for any jobs running in XmAdmin
 * @author shashwat.anand
 *
 */
public class ProgressControl {
	private final UISynchronize sync;

	private ProgressBar progressBar;
	private XMsystemProgressMonitor monitor;

	private Composite parentComposite;

	private Label progressBarLabel;

	@Inject
	public ProgressControl(UISynchronize sync, Composite parent) {
		this.sync = Objects.requireNonNull(sync);
		this.parentComposite = parent;
	}

	@PostConstruct
	public void createControls(Composite parent) {
		Composite progressBarComposite = new Composite(parent, SWT.NONE);
		GridLayoutFactory.fillDefaults().numColumns(2).equalWidth(false).spacing(0, 0).margins(0, 0).extendedMargins(0, 0, 0, 0).applyTo(progressBarComposite);
		GridDataFactory.fillDefaults().align(SWT.RIGHT, SWT.FILL).grab(true, true).applyTo(progressBarComposite);
		this.progressBarLabel = new Label(progressBarComposite, SWT.NONE);
		this.progressBarLabel.setVisible(false);
		this.progressBar = new ProgressBar(progressBarComposite, SWT.SMOOTH);
		this.progressBar.setVisible(false);

		this.monitor = new XMsystemProgressMonitor();

		Job.getJobManager().setProgressProvider(new ProgressProvider() {
			@Override
			public IProgressMonitor createMonitor(Job job) {
				return monitor.addJob(job);
			}
		});
	}
	
	/**
	 * Creates the label in status bar
	 * 
	 * @param parent
	 *            {@link Composite}
	 * @return {@link Label}
	 */
	/*private Label createLabel(final Composite parent) {
		final Composite body = new Composite(parent, SWT.NONE);
		GridDataFactory.fillDefaults().align(SWT.FILL, SWT.FILL).grab(true, true).applyTo(body);
		GridLayoutFactory.fillDefaults().margins(0, 0).spacing(0, 0).applyTo(body);
		final Label label = new Label(body, SWT.NONE);
		GridDataFactory.fillDefaults().align(SWT.RIGHT, SWT.FILL).indent(5, 0).grab(true, true).applyTo(label);
		return label;
	}*/
	
	/**
	 * @return the progressBar
	 */
	public ProgressBar getProgressBar() {
		return progressBar;
	}

	/**
	 * @return the parentComposite
	 */
	public Composite getParentComposite() {
		return parentComposite;
	}

	private final class XMsystemProgressMonitor extends NullProgressMonitor {
		private long runningTasks = 0L;

		@Override
		public void beginTask(final String name, final int totalWork) {
			sync.syncExec(new Runnable() {

				@Override
				public void run() {
					progressBarLabel.setText(name + "   ");
					progressBarLabel.setVisible(true);
					progressBar.setVisible(true);
					progressBarLabel.getParent().requestLayout();
					progressBarLabel.getParent().layout(true, true);
					progressBarLabel.getParent().update();
					if (runningTasks <= 0) {
						// --- no task is running at the moment ---
						progressBar.setSelection(0);
						progressBar.setMaximum(totalWork);

					} else {
						// --- other tasks are running ---
						/*progressBar.setVisible(true);
						progressBar.requestLayout();
						progressBar.redraw();
						progressBar.getParent().update();*/
						progressBar.setMaximum(progressBar.getMaximum() + totalWork);
					}

					runningTasks++;
					progressBar.setToolTipText("Currently running: " + runningTasks + "\nLast task: " + name);
				}
			});
		}

		public IProgressMonitor addJob(Job job) {
			if (job != null) {
				job.addJobChangeListener(new JobChangeAdapter() {
					@Override
					public void done(IJobChangeEvent event) {
						sync.syncExec(new Runnable() {

							@Override
							public void run() {
								runningTasks--;
								if (runningTasks > 0) {
									// --- some tasks are still running ---
									progressBar.setToolTipText("Currently running: " + runningTasks);
								} else {
									// --- all tasks are done (a reset of
									// selection could also be done) ---
									progressBar.setToolTipText("Currently no background progress running.");
									progressBar.setVisible(false);									
									progressBarLabel.setVisible(false);
								}
							}
						});

						// clean-up
						event.getJob().removeJobChangeListener(this);
					}
				});
			}
			return this;
		}

		@Override
		public void worked(final int work) {
			sync.syncExec(new Runnable() {

				@Override
				public void run() {
					progressBar.setSelection(progressBar.getSelection() + work);
				}
			});
		}
		
		@Override
		public void done() {
			sync.syncExec(new Runnable() {

				@Override
				public void run() {
					progressBar.setSelection(0);
					/*progressBar.setVisible(false);
					progressBar.requestLayout();
					progressBar.redraw();
					progressBar.getParent().update();*/
				}
			});
		}
		
	}
}
