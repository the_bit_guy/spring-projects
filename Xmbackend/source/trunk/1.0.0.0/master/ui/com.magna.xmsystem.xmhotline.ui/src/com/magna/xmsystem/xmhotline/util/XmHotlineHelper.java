package com.magna.xmsystem.xmhotline.util;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magna.xmbackend.entities.AdminAreaProjAppRelTbl;
import com.magna.xmbackend.entities.AdminAreasTbl;
import com.magna.xmbackend.entities.IconsTbl;
import com.magna.xmbackend.entities.ProjectApplicationsTbl;
import com.magna.xmbackend.entities.ProjectsTbl;
import com.magna.xmbackend.entities.SiteAdminAreaRelTbl;
import com.magna.xmbackend.entities.SitesTbl;
import com.magna.xmbackend.entities.UserApplicationsTbl;
import com.magna.xmbackend.entities.UserProjAppRelTbl;
import com.magna.xmbackend.entities.UsersTbl;
import com.magna.xmbackend.response.rel.adminareauserapp.AdminAreaUserAppRelWrapper;
import com.magna.xmbackend.response.rel.adminareauserapp.AdminAreaUserAppRelation;
import com.magna.xmbackend.response.rel.useruserapp.UserUserAppRelResponseWrapper;
import com.magna.xmbackend.response.rel.useruserapp.UserUserAppRelation;
import com.magna.xmbackend.vo.enums.ApplicationRelationType;
import com.magna.xmbackend.vo.enums.Status;
import com.magna.xmbackend.vo.jpa.site.SiteResponse;
import com.magna.xmbackend.vo.project.ProjectResponse;
import com.magna.xmbackend.vo.rel.AdminAreaProjectAppRelResponse;
import com.magna.xmbackend.vo.rel.SiteAdminAreaRelResponse;
import com.magna.xmbackend.vo.rel.UserProjectAppRelResponse;
import com.magna.xmsystem.dependencies.customExceptions.UnauthorizedAccessException;
import com.magna.xmsystem.dependencies.utils.LANG_ENUM;
import com.magna.xmsystem.dependencies.utils.XMSystemUtil;
import com.magna.xmsystem.ui.controls.widgets.CustomMessageDialog;
import com.magna.xmsystem.ui.controls.widgets.MagnaCustomCombo;
import com.magna.xmsystem.xmadmin.restclient.adminArea.AdminAreaController;
import com.magna.xmsystem.xmadmin.restclient.project.ProjectController;
import com.magna.xmsystem.xmadmin.restclient.relation.AdminAreaProjectAppRelController;
import com.magna.xmsystem.xmadmin.restclient.relation.AdminAreaUserAppRelController;
import com.magna.xmsystem.xmadmin.restclient.relation.SiteAdminAreaRelController;
import com.magna.xmsystem.xmadmin.restclient.relation.UserProjectAppRelController;
import com.magna.xmsystem.xmadmin.restclient.relation.UserUserAppRelController;
import com.magna.xmsystem.xmadmin.restclient.site.SiteController;
import com.magna.xmsystem.xmadmin.restclient.users.UserController;
import com.magna.xmsystem.xmhotline.model.AdminAreaModel;
import com.magna.xmsystem.xmhotline.model.Icon;
import com.magna.xmsystem.xmhotline.model.MainModel;
import com.magna.xmsystem.xmhotline.model.ProjectApplicationModel;
import com.magna.xmsystem.xmhotline.model.ProjectModel;
import com.magna.xmsystem.xmhotline.model.SiteModel;
import com.magna.xmsystem.xmhotline.model.UserApplicationModel;
import com.magna.xmsystem.xmhotline.model.UserModel;
import com.magna.xmsystem.xmhotline.model.interfaces.IXmHotlineModel;
import com.magna.xmsystem.xmhotline.ui.message.Message;

/**
 * Class to get all the data from service.
 */
public class XmHotlineHelper {

	/** Member variable Constant Log4j 'LOGGER'. */
	private static final Logger LOGGER = LoggerFactory.getLogger(XmHotlineHelper.class);

	/** Member variable 'this ref' for {@link XmHotlineHelper}. */
	private static XmHotlineHelper xmHotlineHelper;

	/** Member variable 'site controller' for {@link SiteController}. */
	private SiteController siteController;

	/**
	 * Member variable 'admin areaController' for {@link AdminAreaController}.
	 */
	private AdminAreaController adminAreaController;

	/** Member variable 'project controller' for {@link ProjectController}. */
	private ProjectController projectController;

	/** Member variable 'user controller' for {@link UserController}. */
	private UserController userController;

	/**
	 * Member variable 'adminAreaUserAppRel controller' for
	 * {@link AdminAreaUserAppRelController}.
	 */
	private AdminAreaUserAppRelController adminAreaUserAppRelController;

	/**
	 * Member variable 'adminAreaProjectAppRel controller' for
	 * {@link AdminAreaProjectAppRelController}.
	 */
	private AdminAreaProjectAppRelController adminAreaProjectAppRelController;

	/**
	 * Member variable 'userProjectAppRelController' for
	 * {@link UserProjectAppRelController}.
	 */
	private UserProjectAppRelController userProjectAppRelController;

	/**
	 * Member variable 'userUserAppRelController' for
	 * {@link UserUserAppRelController}.
	 */
	private UserUserAppRelController userUserAppRelController;

	/** The main model. */
	private MainModel mainModel;

	/** The site map list. */
	private SortedMap<String, IXmHotlineModel> siteMap;

	/** The admin area list. */
	private SortedMap<String, IXmHotlineModel> adminAreaMap;

	/** The users map. */
	private SortedMap<String, IXmHotlineModel> usersMap;

	/** The project map. */
	private SortedMap<String, IXmHotlineModel> projectMap;

	/** Member variable 'projectByUserIdList' for {@link List}. */
	//private List<IXmHotlineModel> projectByUserIdList;

	/** Member variable 'userApplList' for {@link List}. */
	private List<IXmHotlineModel> userApplList;

	/** Member variable 'projectApplList' for {@link List}. */
	private List<IXmHotlineModel> projectApplList;

	/** The project combo items. */
	private Map<String, Object> projectComboItems;

	/** The admin area combo items. */
	private Map<String, Object> adminAreaComboItems;

	/** The user combo items. */
	private Map<String, Object> userComboItems;

	/** The site combo items. */
	private Map<String, Object> siteComboItems;

	/**
	 *  Default constructor.
	 */
	private XmHotlineHelper() {
		siteController = new SiteController();
		adminAreaController = new AdminAreaController();
		projectController = new ProjectController();
		userController = new UserController();
		adminAreaUserAppRelController = new AdminAreaUserAppRelController();
		adminAreaProjectAppRelController = new AdminAreaProjectAppRelController();
		userProjectAppRelController = new UserProjectAppRelController();
		userUserAppRelController = new UserUserAppRelController();

		mainModel = new MainModel();

		this.siteMap = new TreeMap<>();
		this.adminAreaMap = new TreeMap<>();
		this.usersMap = new TreeMap<>();
		this.projectMap = new TreeMap<>();
		userApplList = new ArrayList<IXmHotlineModel>();
		projectApplList = new ArrayList<IXmHotlineModel>();

		updateSiteFromService();
		updateAdminAreaFromService();
		updateUserMapFromService();
		updateProjectsFromService();
	}

	/**
	 * Gets the single instance of XmHotlineHelper.
	 *
	 * @return XmHotlineHelper.
	 */
	public static XmHotlineHelper getInstance() {
		if (xmHotlineHelper == null) {
			xmHotlineHelper = new XmHotlineHelper();
		}
		return xmHotlineHelper;
	}
	
	/**
	 * Update site from service.
	 */
	public void updateSiteFromService() {
		final File iconFolder = getIconFolder();
		try {
			this.siteMap.clear();
			List<SitesTbl> siteResponseList = siteController.getAllSites(true);
			for (SitesTbl sitesTblVo : siteResponseList) {
				final String siteId = sitesTblVo.getSiteId();
				final IconsTbl iconTbl = sitesTblVo.getIconId();
				final boolean status = com.magna.xmbackend.vo.enums.Status.ACTIVE.name().equals(sitesTblVo.getStatus()) ? true : false;
				Icon icon = new Icon(iconTbl.getIconId(), iconTbl.getIconName(),
						iconFolder + File.separator + iconTbl.getIconName(), iconTbl.getIconType());
				String name = XmHotlineUtil.getInstance().getSiteName(sitesTblVo);
				this.siteMap.put(name, new SiteModel(siteId, name, status, getImage(icon, status)));
			}
		} catch (Exception e) {
			LOGGER.error("Exeception while getting site objects! " + e);
		}
	}

	/**
	 * Gets all admin area from the service.
	 * 
	 * @return admin area list
	 */
	public void updateAdminAreaFromService() {
		final File iconFolder = getIconFolder();
		if (iconFolder == null) {
			return;
		} else {
			try {
				this.adminAreaMap.clear();
				List<AdminAreasTbl> adminAreaResponseList = adminAreaController.getAllAdminAreas(true);
				if(adminAreaResponseList != null) {
					for (AdminAreasTbl adminAreasTblVo : adminAreaResponseList) {
						final String adminAreaId = adminAreasTblVo.getAdminAreaId();
						final IconsTbl iconTbl = adminAreasTblVo.getIconId();
						final boolean status = com.magna.xmbackend.vo.enums.Status.ACTIVE.name().equals(adminAreasTblVo.getStatus()) ? true : false;
						final Icon icon = new Icon(iconTbl.getIconId(), iconTbl.getIconName(),
								iconFolder + File.separator + iconTbl.getIconName(), iconTbl.getIconType());
						final String name = XmHotlineUtil.getInstance().getAdminAreaName(adminAreasTblVo);
						this.adminAreaMap.put(name, new AdminAreaModel(adminAreaId, name, status, getImage(icon, status)));
					}
				}
			} catch (final UnauthorizedAccessException e) {
				LOGGER.error("Exeception while getting Adminstration Areas objects! " + e);
			}
		}
	}
	
	/**
	 * Gets the admin area by tbl.
	 *
	 * @param adminAreasTbls the admin areas tbls
	 * @return the admin area by tbl
	 */
	public AdminAreaModel getAdminAreaByTbl(final AdminAreasTbl adminAreasTbls) {
		final File iconFolder = getIconFolder();
		if (iconFolder == null) {
			return null;
		} else {
			try {
				if(adminAreasTbls != null) {
					final String adminAreaId = adminAreasTbls.getAdminAreaId();
					final IconsTbl iconTbl = adminAreasTbls.getIconId();
					final boolean status = com.magna.xmbackend.vo.enums.Status.ACTIVE.name()
							.equals(adminAreasTbls.getStatus()) ? true : false;
					final Icon icon = new Icon(iconTbl.getIconId(), iconTbl.getIconName(),
							iconFolder + File.separator + iconTbl.getIconName(), iconTbl.getIconType());
					final String name = XmHotlineUtil.getInstance().getAdminAreaName(adminAreasTbls);
					
					return new AdminAreaModel(adminAreaId, name, status, getImage(icon, status));
				}
			} catch (final UnauthorizedAccessException e) {
				LOGGER.error("Exeception while getting Adminstration Areas objects by AA table! " + e);
			}
		}
		
		return null;
	}

	/**
	 * Gets the projects from service.
	 *
	 * @return project list
	 */
	public void updateProjectsFromService() {
		final File iconFolder = getIconFolder();
		if (iconFolder == null) {
			return;
		} else {
			try {
				this.projectMap.clear();
				final List<ProjectsTbl> projectResponseList = projectController.getAllProjects(true);
				if(projectResponseList != null) {
					for (ProjectsTbl projectsTblVo : projectResponseList) {
						final String projectId = projectsTblVo.getProjectId();
						final IconsTbl iconTbl = projectsTblVo.getIconId();
						final Icon icon = new Icon(iconTbl.getIconId(), iconTbl.getIconName(),
								iconFolder + File.separator + iconTbl.getIconName(), iconTbl.getIconType());
						final String projectName = XmHotlineUtil.getInstance().getProjectName(projectsTblVo);
						boolean status = projectsTblVo.getStatus().equals(Status.ACTIVE.name()) ? true : false;
						Map<LANG_ENUM, String> projectAppDescMap = XmHotlineUtil.getInstance().getProjectDescMap(projectsTblVo);
						this.projectMap.put(projectName, new ProjectModel(projectId, projectName, icon, status, projectAppDescMap));
					}
				}
			} catch (Exception e) {
				LOGGER.error("Exeception while getting  Project objects! " + e);
			}
		}
	}

	/**
	 * gets icon folder.
	 *
	 * @return Folder object
	 */
	private File getIconFolder() {
		final File iconFolder = XMSystemUtil.getXMSystemServerIconsFolder();
		if (iconFolder == null) {
			CustomMessageDialog.openError(Display.getDefault().getActiveShell(), "Error",
					"Icon folder is not reachable");
			return null;
		}
		return iconFolder;
	}

	/**
	 * Gets the projects from service.
	 *
	 * @param userId the user id
	 * @return project list
	 */
	public List<IXmHotlineModel> getProjectsByUserId(final String userId, final String siteId) {
		List<IXmHotlineModel> projectList = new ArrayList<IXmHotlineModel>();
		try {
			final ProjectResponse projectResponse = projectController.getProjectsByUserIdRel(userId, siteId);
			if (projectResponse == null) {
				return null;
			}
			final Iterable<ProjectsTbl> projectsTbls = projectResponse.getProjectsTbls();
			projectList.addAll(getFilteredProjectModel(projectsTbls));
		} catch (Exception e) {
			LOGGER.error("Exeception while getting  Project objects! " + e);
		}
		return projectList;
	}

	/**
	 * Gets the filtered project model.
	 *
	 * @param projectsTbls the projects tbls
	 * @return the filtered project model
	 */
	private Collection<? extends IXmHotlineModel> getFilteredProjectModel(Iterable<ProjectsTbl> projectsTbls) {
		IXmHotlineModel proObj;
		List<IXmHotlineModel> projectList = new ArrayList<>();
		if (projectsTbls != null) {
			XmHotlineUtil instance = XmHotlineUtil.getInstance();
			for (ProjectsTbl projectsTbl : projectsTbls) {
				String projectName = instance.getProjectName(projectsTbl);
				if ((proObj = this.projectMap.get(projectName)) != null) {
					if(proObj instanceof ProjectModel){
						boolean modelStatus = ((ProjectModel)proObj).isStatus();
						boolean projectTblstatus = projectsTbl.getStatus().equals(Status.ACTIVE.name()) ? true : false;
						if(projectTblstatus != modelStatus){
							((ProjectModel)proObj).setStatus(projectTblstatus);
						}
					}
					projectList.add(proObj);
				}
			}
		}
		return projectList;
	}

	/**
	 * Gets projects by site id.
	 * 
	 * @param siteId
	 *            site id
	 * @return project list
	 */
	public List<IXmHotlineModel> getProjetsBySiteId(final String siteId) {
		final ProjectResponse projectsBySiteId = projectController.getProjectsBySiteIdRel(siteId);
		if (projectsBySiteId == null) {
			return null;
		}
		final List<IXmHotlineModel> projectList = new ArrayList<IXmHotlineModel>();
		Iterable<ProjectsTbl> projectsTbls = projectsBySiteId.getProjectsTbls();
		projectList.addAll(getFilteredProjectModel(projectsTbls));
		Collections.sort(projectList, (IXmHotlineModel p1, IXmHotlineModel p2) -> ((ProjectModel) p1).getProjectName()
				.compareTo(((ProjectModel) p2).getProjectName()));
		return projectList;
	}

	/**
	 * Gets projects by admin area id.
	 * 
	 * @param adminAreaId
	 *            admin area
	 * @return project list
	 */
	public List<IXmHotlineModel> getProjetsByAdminAreaId(final String adminAreaId) {
		ProjectResponse projectsByAAIdRel = projectController.getProjectsByAAIdRel(adminAreaId);
		List<IXmHotlineModel> projectList = new ArrayList<IXmHotlineModel>();
		if(projectsByAAIdRel != null){
			Iterable<ProjectsTbl> projectsTbls = projectsByAAIdRel.getProjectsTbls();
			projectList.addAll(getFilteredProjectModel(projectsTbls));
		}
		Collections.sort(projectList, (IXmHotlineModel p1, IXmHotlineModel p2) -> ((ProjectModel) p1).getProjectName()
				.compareTo(((ProjectModel) p2).getProjectName()));
		return projectList;
	}
	
	/**
	 * Gets the user by admin area by id.
	 *
	 * @param adminAreaId the admin area id
	 * @return the user by admin area by id
	 */
	public List<IXmHotlineModel> getUserByAdminAreaById(final String adminAreaId) {
		List<IXmHotlineModel> userList = new ArrayList<>();
		List<UsersTbl> usersByAdminArea = userController.getUsersByAdminArea(adminAreaId);
		if (usersByAdminArea == null) {
			return null;
		}
		for (UsersTbl usersTbl : usersByAdminArea) {
			String username = usersTbl.getUsername();
			UserModel userModel = (UserModel) this.usersMap.get(username);
			if (userModel != null) {
				userList.add(userModel);
			}
		}
		Collections.sort(userList, (final IXmHotlineModel u1, final IXmHotlineModel u2) -> {
			if (u1 != null && u2 != null) {
				return ((UserModel) u1).getUserName().compareTo(((UserModel) u2).getUserName());
			}
			return 0;
		});
		return userList;
	}
	
	/**
	 * Gets the all users list.
	 *
	 * @return the all users list
	 */
	public List<IXmHotlineModel> getAllUsersList() {
		List<IXmHotlineModel> userList = new ArrayList<>();
		List<UsersTbl> usersByAdminArea = userController.getAllUsers(false);
		if (usersByAdminArea == null) {
			return null;
		}
		for (UsersTbl usersTbl : usersByAdminArea) {
			String username = usersTbl.getUsername();
			UserModel userModel = (UserModel) this.usersMap.get(username);
			if(userModel != null){
				userList.add(userModel);
			}
		}
		Collections.sort(userList, (final IXmHotlineModel u1, final IXmHotlineModel u2) -> {
			if (u1 != null && u2 != null) {
				return ((UserModel) u1).getUserName().compareTo(((UserModel) u2).getUserName());
			}
			return 0;
		});
		return userList;
	}
	
	/**
	 * Gets the all users list by user tbl.
	 *
	 * @param usersByAdminArea the users by admin area
	 * @return the all users list by user tbl
	 */
	public List<IXmHotlineModel> getAllUsersListByUserTbl(final List<UsersTbl> usersByAdminArea) {
		List<IXmHotlineModel> userList = new ArrayList<>();
		if (usersByAdminArea == null) {
			return null;
		}
		for (UsersTbl usersTbl : usersByAdminArea) {
			String username = usersTbl.getUsername();
			UserModel userModel = (UserModel) this.usersMap.get(username);
			if(userModel != null){
				userList.add(userModel);
			}
		}
		Collections.sort(userList, (final IXmHotlineModel u1, final IXmHotlineModel u2) -> {
			if (u1 != null && u2 != null) {
				return ((UserModel) u1).getUserName().compareTo(((UserModel) u2).getUserName());
			}
			return 0;
		});
		return userList;
	}

	/**
	 * Gets user application by admin area id.
	 *
	 * @param aaId            admin area id
	 * @param userId the user id
	 * @return user application list
	 */
	public List<IXmHotlineModel> getUserApplicationByAAId(final String aaId, final String userId) {
		final File iconFolder = getIconFolder();
		List<IXmHotlineModel> userApplList = new ArrayList<IXmHotlineModel>();
		AdminAreaUserAppRelWrapper adminAreaUserAppRelationsByAdminAreaId = adminAreaUserAppRelController
				.getUserAppRelationsByAAId(aaId);
		if (adminAreaUserAppRelationsByAdminAreaId != null) {
			final List<AdminAreaUserAppRelation> adminAreaUserAppRels = adminAreaUserAppRelationsByAdminAreaId
					.getAdminAreaUserAppRels();
			for (AdminAreaUserAppRelation adminAreaUserAppRelation : adminAreaUserAppRels) {
				
				UserApplicationsTbl userApplicationsTbl = adminAreaUserAppRelation.getUserApplicationId();
				String userAppId = userApplicationsTbl.getUserApplicationId();
				IconsTbl iconsTbl = userApplicationsTbl.getIconId();
				String reltype = adminAreaUserAppRelation.getRelType();
				boolean status = userApplicationsTbl.getStatus().equals(Status.ACTIVE.name()) ? true : false;
				String name = userApplicationsTbl.getName();
				String desc = userApplicationsTbl.getDescription();
				Icon icon = new Icon(iconsTbl.getIconId(), iconsTbl.getIconName(),
						iconFolder + File.separator + iconsTbl.getIconName(), iconsTbl.getIconType());
				Map<LANG_ENUM, String> userAppNameMap = XmHotlineUtil.getInstance().getUserAppNameMap(userApplicationsTbl);
				Map<LANG_ENUM, String> userAppDescMap = XmHotlineUtil.getInstance().getUserAppDescMap(userApplicationsTbl);
				
				userApplList.add(new UserApplicationModel(userAppId, name, desc, reltype, status, getImage(icon, status), aaId, userId, userAppNameMap, userAppDescMap));
			}
			
			final XmHotlineUtil instance = XmHotlineUtil.getInstance();
			final LANG_ENUM currentLocale = instance.getCurrentLocaleEnum();
			Collections.sort(userApplList, (IXmHotlineModel o1, IXmHotlineModel o2) -> {
				final UserApplicationModel ua1 = (UserApplicationModel)o1;
				final UserApplicationModel ua2 = (UserApplicationModel)o2;
				String name1 = ua1.getUserAppNameMap().get(currentLocale);
				name1 = !(XMSystemUtil.isEmpty(name1)) ? name1 : ua1.getUserAppInternalName();
				
				String name2 = ua2.getUserAppNameMap().get(currentLocale);
				name2 = !(XMSystemUtil.isEmpty(name2)) ? name2 : ua2.getUserAppInternalName();
				return name1.compareTo(name2);
			});
		}
		return userApplList;
	}

	/**
	 * Gets project application by aa id and project id.
	 *
	 * @param aaId            admin area id
	 * @param projId the proj id
	 * @param userId the user id
	 * @return project application list
	 */
	public List<IXmHotlineModel> getProjectApplicationByAAIdandProjectId(final String aaId, final String projId, String userId) {
		final File iconFolder = getIconFolder();
		List<IXmHotlineModel> projApplList = new ArrayList<IXmHotlineModel>();
		AdminAreaProjectAppRelResponse aaProjAppRelByAAIdAndProjId = adminAreaProjectAppRelController
				.getProjAppRelationsByAAIdAndProjectId(aaId, projId);
		if (aaProjAppRelByAAIdAndProjId != null) {
			Iterable<AdminAreaProjAppRelTbl> adminAreaProjAppRelTbls = aaProjAppRelByAAIdAndProjId
					.getAdminAreaProjAppRelTbls();
			for (AdminAreaProjAppRelTbl adminAreaProjAppRelTbl : adminAreaProjAppRelTbls) {
				ProjectApplicationsTbl projectApplicationsTbl = adminAreaProjAppRelTbl.getProjectApplicationId();
				String id = projectApplicationsTbl.getProjectApplicationId();
				String relType = adminAreaProjAppRelTbl.getRelType();
				IconsTbl iconsTbl = projectApplicationsTbl.getIconId();
				Icon icon = new Icon(iconsTbl.getIconId(), iconsTbl.getIconName(),
						iconFolder + File.separator + iconsTbl.getIconName(), iconsTbl.getIconType());
				boolean status = projectApplicationsTbl.getStatus().equals(Status.ACTIVE.name()) ? true : false;
				XmHotlineUtil instance = XmHotlineUtil.getInstance();
				String name = projectApplicationsTbl.getName();
				String desc = projectApplicationsTbl.getDescription();
				Map<LANG_ENUM, String> projectAppNameMap = instance.getProjectAppNameMap(projectApplicationsTbl);
				Map<LANG_ENUM, String> projectAppDescMap = instance.getProjectAppDescMap(projectApplicationsTbl);
				projApplList.add(new ProjectApplicationModel(id, name, desc, relType, status, 
						getImage(icon, status), aaId, userId, projId, projectAppNameMap, projectAppDescMap));
			}
		}
		
		XmHotlineUtil instance = XmHotlineUtil.getInstance();
		final LANG_ENUM currentLocale = instance.getCurrentLocaleEnum();
		Collections.sort(projApplList, (IXmHotlineModel o1, IXmHotlineModel o2) -> {
			ProjectApplicationModel pa1 = (ProjectApplicationModel)o1;
			ProjectApplicationModel pa2 = (ProjectApplicationModel)o2;
			String name1 = pa1.getProjectAppNameMap().get(currentLocale);
			name1 = !(XMSystemUtil.isEmpty(name1)) ? name1 : pa1.getProjectAppInternalName();
			
			String name2 = pa2.getProjectAppNameMap().get(currentLocale);
			name2 = !(XMSystemUtil.isEmpty(name2)) ? name2 : pa2.getProjectAppInternalName();
			return name1.compareTo(name2);
		});
		return projApplList;
	}
	
	/**
	 * Get User projectApp relations by user id, project id and admin area id.
	 * 
	 * @param userId
	 *            user id
	 * @param projId
	 *            project id
	 * @param aaId
	 *            admin area id
	 * @return userProjectApplication relation
	 */
	public Iterable<UserProjAppRelTbl> getUserProjAppRelsByUserProjAAId(final String userId, final String projId, final String aaId) {
		UserProjectAppRelResponse userProjAppRelsByUserProjAAId = userProjectAppRelController.getUserProjAppRelsByUserProjAAId(userId, projId, aaId);
		if (userProjAppRelsByUserProjAAId != null) {
			return userProjAppRelsByUserProjAAId.getUserProjAppRelTbls();
		}
		return null;			
	}
	
	/**
	 * Get User userApplication relations by user id and admin area id.
	 * 
	 * @param userId
	 *            user id
	 * @param aaId
	 *            admin area id
	 * @return userUserApplication relation
	 */
	public List<UserUserAppRelation> getUserUserAppRelsByUserIdAAId(final String userId, final String aaId) {
		UserUserAppRelResponseWrapper userUserAppRelsByUserIdAAId = userUserAppRelController.getUserUserAppRelsByUserIdAAId(userId, aaId);
		if (userUserAppRelsByUserIdAAId != null) {
			return userUserAppRelsByUserIdAAId.getUserUserAppRelations();
		}
		return null;
	}	

	/**
	 * Update user map from service.
	 */
	public List<UsersTbl> updateUserMapFromService() {
		final File iconFolder = getIconFolder();
		try {
			this.usersMap.clear();
			List<UsersTbl> userResponseList = userController.getAllUsers(true);
			if(userResponseList != null) {
				for (UsersTbl usersTblVo : userResponseList) {
					String id = usersTblVo.getUserId();
					String userName = usersTblVo.getUsername();
					boolean status = usersTblVo.getStatus().equals(Status.ACTIVE.name()) ? true : false;
					IconsTbl iconTbl = usersTblVo.getIconId();
					Icon icon = new Icon(iconTbl.getIconId(), iconTbl.getIconName(),
							iconFolder + File.separator + iconTbl.getIconName(), iconTbl.getIconType());
					this.usersMap.put(userName, new UserModel(id, userName, status, getImage(icon, status)));
				}
				
				return userResponseList;
			}
		} catch (Exception e) {
			LOGGER.error("Exeception while getting User objects! " + e);
		}
		
		return null;
	}

	/**
	 * sets site to site combo.
	 * 
	 * @param siteFilterCombo
	 *            site filter combo
	 */
	public void setSitesToCombo(final MagnaCustomCombo siteFilterCombo) {
		siteComboItems = new TreeMap<>();
		final Set<String> siteNameSet = this.siteMap.keySet();
		if (siteNameSet != null && siteNameSet.size() > 0) {
			for (String siteName : siteNameSet) {
				SiteModel siteModel = (SiteModel) this.siteMap.get(siteName);
				Image image = siteModel.getImage();
				String siteNameFromModel = siteModel.getSiteName();
				if (image != null && !XMSystemUtil.isEmpty(siteNameFromModel)) {
					siteComboItems.put(siteNameFromModel, image);
				}
			}
			siteFilterCombo.setItemsWithImages(siteComboItems);
		} else {
			siteFilterCombo.getTable().removeAll();
		}
	}
	
	/**
	 * Sets the user to combo.
	 *
	 * @param userFilterCombo the user filter combo
	 * @param userList the user list
	 */
	public void setUserToCombo(final MagnaCustomCombo userFilterCombo, final Collection<IXmHotlineModel> userList) {
		userComboItems = new TreeMap<>();
		if(userList != null && userList.size() > 0){
			for (IXmHotlineModel user : userList) {
				UserModel userModel = (UserModel) user;
				Image image = userModel.getImage();
				String userName = userModel.getUserName();
				if(image != null && !XMSystemUtil.isEmpty(userName)){
					userComboItems.put(userName, image);
				}
			}
			userFilterCombo.setItemsWithImages(userComboItems);
		} else {
			userFilterCombo.getTable().removeAll();
		}
	}

	/**
	 * sets Administration Area to admin area combo.
	 * 
	 * @param adminAreaFilterCombo
	 *            admin area filter combo
	 */
	public void setAdminAreasToCombo(final MagnaCustomCombo adminAreaFilterCombo, final Collection<IXmHotlineModel> adminAreaList) {
		adminAreaComboItems = new TreeMap<>();
		if (adminAreaList != null && adminAreaList.size() > 0) {
			final Set<String> adminAreaNameSet = this.adminAreaMap.keySet();
			if (adminAreaNameSet != null && adminAreaNameSet.size() > 0) {
				for (String adminAreaName : adminAreaNameSet) {
					final AdminAreaModel adminAreaModel = (AdminAreaModel) this.adminAreaMap.get(adminAreaName);
					final Image image = adminAreaModel.getImage();
					final String adminAreaNameFromModel = adminAreaModel.getAdminAreaName();
					if (image != null && !XMSystemUtil.isEmpty(adminAreaNameFromModel)) {
						adminAreaComboItems.put(adminAreaNameFromModel, image);
					}
				}
			}
			adminAreaFilterCombo.setItemsWithImages(adminAreaComboItems);
		} else {
			adminAreaFilterCombo.getTable().removeAll();
		}
		
	}

	/**
	 * set project to project filter combo.
	 *
	 * @param projectFilterCombo            project filter combo
	 * @param projectList the project list
	 */
	public void setProjectToCombo(final MagnaCustomCombo projectFilterCombo, final Collection<IXmHotlineModel> projectList) {
		int selectionIndex = projectFilterCombo.getSelectionIndex();
		projectComboItems = new TreeMap<>();
		IXmHotlineModel proObj;
		Image image = null;
		if (projectList != null && projectList.size() > 0) {
			for (IXmHotlineModel project : projectList) {
				ProjectModel projectModel = (ProjectModel) project;
				String projectName = projectModel.getProjectName();
				if ((proObj = this.projectMap.get(projectName)) != null) {
					if (proObj instanceof ProjectModel) {
						boolean modelStatus = ((ProjectModel) proObj).isStatus();
						boolean status = projectModel.isStatus();
						if (modelStatus != status) {
							image = getImage(((ProjectModel) proObj).getIcon(), ((ProjectModel) proObj).isStatus());
						} else {
							image = getImage(projectModel.getIcon(), projectModel.isStatus());
						}
					}
				}
				if (image != null && !XMSystemUtil.isEmpty(projectName)) {
					projectComboItems.put(projectName, image);
				}
			}
			projectFilterCombo.setItemsWithImages(projectComboItems);
			if (selectionIndex != -1 && projectList.size() <= selectionIndex) {
				projectFilterCombo.select(0);
				projectFilterCombo.notifyListeners(SWT.Selection, new Event());
			}
		} else {
			projectFilterCombo.getTable().removeAll();
		}
	}
	
	/**
	 * Gets the site admin area rel id.
	 *
	 * @param adminAreaId the admin area id
	 * @return the site admin area rel id
	 */
	public String getSiteAdminAreaRelId(final String adminAreaId) {
		SiteAdminAreaRelController controller = new SiteAdminAreaRelController();
		SiteAdminAreaRelResponse siteAdminAreaRelResponse = controller.getSiteAdminAreaRelByAAId(adminAreaId);
		if (siteAdminAreaRelResponse != null) {
			Iterable<SiteAdminAreaRelTbl> siteAdminAreaRelTbls = siteAdminAreaRelResponse.getSiteAdminAreaRelTbls();
			for (SiteAdminAreaRelTbl siteAdminAreaRelTbl : siteAdminAreaRelTbls) {
				String siteAdminAreaRelId = siteAdminAreaRelTbl.getSiteAdminAreaRelId();
				if (!XMSystemUtil.isEmpty(siteAdminAreaRelId)) {
					return siteAdminAreaRelId;
				}
			}
		}
		return null;
	}

	/**
	 * Gets the project combo items.
	 *
	 * @return the projectComboItems
	 */
	public final Map<String, Object> getProjectComboItems() {
		return projectComboItems;
	}

	/**
	 * Sets the project combo items.
	 *
	 * @param projectComboItems            the projectComboItems to set
	 */
	public final void setProjectComboItems(Map<String, Object> projectComboItems) {
		this.projectComboItems = projectComboItems;
	}

	/**
	 * Gets the admin area combo items.
	 *
	 * @return the adminAreaComboItems
	 */
	public final Map<String, Object> getAdminAreaComboItems() {
		return adminAreaComboItems;
	}

	/**
	 * Sets the admin area combo items.
	 *
	 * @param adminAreaComboItems            the adminAreaComboItems to set
	 */
	public final void setAdminAreaComboItems(Map<String, Object> adminAreaComboItems) {
		this.adminAreaComboItems = adminAreaComboItems;
	}

	/**
	 * Gets the user combo items.
	 *
	 * @return the userComboItems
	 */
	public final Map<String, Object> getUserComboItems() {
		return userComboItems;
	}

	/**
	 * Sets the user combo items.
	 *
	 * @param userComboItems            the userComboItems to set
	 */
	public final void setUserComboItems(Map<String, Object> userComboItems) {
		this.userComboItems = userComboItems;
	}

	/**
	 * Gets the site combo items.
	 *
	 * @return the siteComboItems
	 */
	public final Map<String, Object> getSiteComboItems() {
		return siteComboItems;
	}

	/**
	 * Sets the site combo items.
	 *
	 * @param siteComboItems            the siteComboItems to set
	 */
	public final void setSiteComboItems(Map<String, Object> siteComboItems) {
		this.siteComboItems = siteComboItems;
	}

	/**
	 * Gets the user appl list.
	 *
	 * @return the userApplList
	 */
	public final List<IXmHotlineModel> getUserApplList() {
		return userApplList;
	}

	/**
	 * Sets the user appl list.
	 *
	 * @param userApplList            the userApplList to set
	 */
	public final void setUserApplList(List<IXmHotlineModel> userApplList) {
		this.userApplList = userApplList;
	}

	/**
	 * Gets the project appl list.
	 *
	 * @return the projectApplList
	 */
	public final List<IXmHotlineModel> getProjectApplList() {
		return projectApplList;
	}

	/**
	 * Sets the project appl list.
	 *
	 * @param projectApplList            the projectApplList to set
	 */
	public final void setProjectApplList(List<IXmHotlineModel> projectApplList) {
		this.projectApplList = projectApplList;
	}

	/**
	 * Gets the image.
	 *
	 * @param icon the icon
	 * @param status the status
	 * @return the image
	 */
	public Image getImage(final Icon icon, final boolean status) {
		Image image = null;
		if (icon == null) {
			LOGGER.error("Icon not avialable!!");
		} else {
			image = XMSystemUtil.getInstance().getImage(this.getClass(), icon.getIconPath(), status, true);
		}
		return image;
	}

	/**
	 * Gets the site controller.
	 *
	 * @return the siteController
	 */
	public final SiteController getSiteController() {
		return siteController;
	}

	/**
	 * Sets the site controller.
	 *
	 * @param siteController            the siteController to set
	 */
	public final void setSiteController(final SiteController siteController) {
		this.siteController = siteController;
	}

	/**
	 * Gets the admin area controller.
	 *
	 * @return the adminAreaController
	 */
	public final AdminAreaController getAdminAreaController() {
		return adminAreaController;
	}

	/**
	 * Sets the admin area controller.
	 *
	 * @param adminAreaController            the adminAreaController to set
	 */
	public final void setAdminAreaController(final AdminAreaController adminAreaController) {
		this.adminAreaController = adminAreaController;
	}

	/**
	 * Gets the project controller.
	 *
	 * @return the projectController
	 */
	public ProjectController getProjectController() {
		return projectController;
	}

	/**
	 * Sets the project controller.
	 *
	 * @param porjectController            the porjectController to set
	 */
	public final void setProjectController(final ProjectController porjectController) {
		this.projectController = porjectController;
	}

	/**
	 * Gets the user controller.
	 *
	 * @return the userController
	 */
	public final UserController getUserController() {
		return userController;
	}

	/**
	 * Sets the user controller.
	 *
	 * @param userController            the userController to set
	 */
	public final void setUserController(final UserController userController) {
		this.userController = userController;
	}

	/**
	 * Gets the admin area user app rel controller.
	 *
	 * @return the adminAreaUserAppRelController
	 */
	public final AdminAreaUserAppRelController getAdminAreaUserAppRelController() {
		return adminAreaUserAppRelController;
	}

	/**
	 * Gets the admin area project app rel controller.
	 *
	 * @return the adminAreaProjectAppRelController
	 */
	public final AdminAreaProjectAppRelController getAdminAreaProjectAppRelController() {
		return adminAreaProjectAppRelController;
	}

	/**
	 * Gets the main model.
	 *
	 * @return the mainModel
	 */
	public final MainModel getMainModel() {
		return mainModel;
	}

	/**
	 * Gets the site map.
	 *
	 * @return the siteMap
	 */
	public SortedMap<String, IXmHotlineModel> getSiteMap() {
		return siteMap;
	}

	/**
	 * Gets the admin area map.
	 *
	 * @return the adminAreaMap
	 */
	public SortedMap<String, IXmHotlineModel> getAdminAreaMap() {
		return adminAreaMap;
	}

	/**
	 * Gets the users map.
	 *
	 * @return the usersMap
	 */
	public SortedMap<String, IXmHotlineModel> getUsersMap() {
		return usersMap;
	}

	/**
	 * Gets the project map.
	 *
	 * @return the projectMap
	 */
	public SortedMap<String, IXmHotlineModel> getProjectMap() {
		return projectMap;
	}

	/**
	 * Make question str.
	 *
	 * @param userName the user name
	 * @param messages 
	 * @return the string
	 */
	public String makeQuestionStr(final String userName, final Message messages) {
		XmHotlineUtil instance = XmHotlineUtil.getInstance();
		final LANG_ENUM currentLocale = instance.getCurrentLocaleEnum();
		StringBuilder stb = new StringBuilder();
		List<ProjectModel> projectUserRelationAddList = mainModel.getProjectUserRelationAddList();
		List<ProjectModel> projectUserRelationDeleteList = mainModel.getProjectUserRelationDeleteList();
		
		List<UserApplicationModel> userAppUserRelationAddList = mainModel.getUserAppUserRelationAddList();
		List<UserApplicationModel> userAppUserRelationDeleteList = mainModel.getUserAppUserRelationDeleteList();
		
		List<ProjectApplicationModel> projectAppUserRelationAddList = mainModel.getProjectAppUserRelationAddList();
		List<ProjectApplicationModel> projectAppUserRelationDeleteList = mainModel.getProjectAppUserRelationDeleteList();
		if (!projectUserRelationAddList.isEmpty()) {
			for (ProjectModel projectModel : projectUserRelationAddList) {
				String projectName = projectModel.getProjectName();
				stb.append(messages.addProjectUserRelMsgPart + " '" + projectName + "' " + messages.andUserMsgPart + " '" + userName + "'").append("\n");
			}
		}
		if (!projectUserRelationDeleteList.isEmpty()) {
			for (ProjectModel projectModel : projectUserRelationDeleteList) {
				String projectName = projectModel.getProjectName();
				stb.append(messages.deleteProjectUserRelMsgPart + " '" + projectName + "' " + messages.andUserMsgPart + " '" + userName + "'").append("\n");
			}
		}
		if (!userAppUserRelationAddList.isEmpty()) {
			for (UserApplicationModel userApplicationModel : userAppUserRelationAddList) {
				
				String userAppName = ((UserApplicationModel) userApplicationModel).getUserAppNameMap().get(currentLocale);
				userAppName = !(XMSystemUtil.isEmpty(userAppName)) ? userAppName
						: ((UserApplicationModel) userApplicationModel).getUserAppInternalName();
				
				if (ApplicationRelationType.FIXED.name().equals(userApplicationModel.getRelType())) {
					stb.append(messages.updateUserAppMsgPart + " '" + userAppName + "' " + messages.forUserMsgPart + " '" + userName + "'").append("\n");
				} else if (ApplicationRelationType.NOTFIXED.name().equals(userApplicationModel.getRelType())
						|| ApplicationRelationType.PROTECTED.name().equals(userApplicationModel.getRelType())) {
					stb.append(messages.updateUserAppMsgPart + " '" + userAppName + "' " + messages.forUserMsgPart + " '" + userName + "'").append("\n");
				}
			}
		}
		if (!userAppUserRelationDeleteList.isEmpty()) {
			for (UserApplicationModel userApplicationModel : userAppUserRelationDeleteList) {
				String userAppName = ((UserApplicationModel) userApplicationModel).getUserAppNameMap().get(currentLocale);
				userAppName = !(XMSystemUtil.isEmpty(userAppName)) ? userAppName
						: ((UserApplicationModel) userApplicationModel).getUserAppInternalName();
				if (ApplicationRelationType.FIXED.name().equals(userApplicationModel.getRelType())) {
					stb.append(messages.updateUserAppMsgPart + " '" + userAppName + "' " + messages.forUserMsgPart + " '" + userName + "'").append("\n");
				} else if (ApplicationRelationType.NOTFIXED.name().equals(userApplicationModel.getRelType())
						|| ApplicationRelationType.PROTECTED.name().equals(userApplicationModel.getRelType())) {
					stb.append(messages.updateUserAppMsgPart + " '" + userAppName + "' " + messages.forUserMsgPart + " '" + userName + "'").append("\n");
				}
			}
		}
		if (!projectAppUserRelationAddList.isEmpty()) {
			for (ProjectApplicationModel projectApplicationModel : projectAppUserRelationAddList) {
				String projectAppName = ((ProjectApplicationModel) projectApplicationModel).getProjectAppNameMap().get(currentLocale);
				projectAppName = !(XMSystemUtil.isEmpty(projectAppName)) ? projectAppName
						: ((ProjectApplicationModel) projectApplicationModel).getProjectAppInternalName();
				if (ApplicationRelationType.FIXED.name().equals(projectApplicationModel.getRelType())) {
					stb.append(messages.updateProjectAppMsgPart + " '" + projectAppName + "' " + messages.forUserMsgPart + " '" + userName + "'").append("\n");
				} else if (ApplicationRelationType.NOTFIXED.name().equals(projectApplicationModel.getRelType())
						|| ApplicationRelationType.PROTECTED.name().equals(projectApplicationModel.getRelType())) {
					stb.append(messages.updateProjectAppMsgPart + " '" + projectAppName + "' " + messages.forUserMsgPart + " '" + userName + "'").append("\n");
				}
			}
		}
		if (!projectAppUserRelationDeleteList.isEmpty()) {
			for (ProjectApplicationModel projectApplicationModel : projectAppUserRelationDeleteList) {
				String projectAppName = ((ProjectApplicationModel) projectApplicationModel).getProjectAppNameMap().get(currentLocale);
				projectAppName = !(XMSystemUtil.isEmpty(projectAppName)) ? projectAppName
						: ((ProjectApplicationModel) projectApplicationModel).getProjectAppInternalName();
				if (ApplicationRelationType.FIXED.name().equals(projectApplicationModel.getRelType())) {
					stb.append(messages.updateProjectAppMsgPart + " '" + projectAppName + "' " + messages.forUserMsgPart + " '" + userName + "'").append("\n");
				} else if (ApplicationRelationType.NOTFIXED.name().equals(projectApplicationModel.getRelType())
						|| ApplicationRelationType.PROTECTED.name().equals(projectApplicationModel.getRelType())) {
					stb.append(messages.updateProjectAppMsgPart + " '" + projectAppName + "' " + messages.forUserMsgPart + " '" + userName + "'").append("\n");
				}
			}
		}
		return stb.toString();
	}
	
	/**
	 * Gets the admin area by site and project id.
	 *
	 * @param siteId the site id
	 * @param projectId the project id
	 * @return the admin area by site and project id
	 */
	public List<AdminAreasTbl> getAdminAreaBySiteAndProjectId(final String siteId, final String projectId) {
		List<AdminAreasTbl> adminAreaTbl = new ArrayList<>();
		try {
			adminAreaTbl = adminAreaController.getAAByProjectIdAndSiteId(projectId, siteId);
		} catch (Exception e) {
			LOGGER.error("Exeception while getting AA by site and project Id objects! " + e);
		}

		return adminAreaTbl;
	}
	
	/**
	 * Checks if is admin area site relation exist.
	 *
	 * @param adminAreaId the admin area id
	 * @return true, if is admin area site relation exist
	 */
	public boolean isAdminAreaSiteRelationExist(final String adminAreaId) {
		if (adminAreaId != null) {
			try {
				SiteResponse sitesByAAId = siteController.getSitesByAAId(adminAreaId);
				Iterable<SitesTbl> sitesTbls = sitesByAAId.getSitesTbls();
				List<SitesTbl> siteTblList = new ArrayList<>();
				for (SitesTbl sitesTbl : sitesTbls) {
					siteTblList.add(sitesTbl);
				}

				if (!siteTblList.isEmpty()) {
					return true;
				}
			} catch (Exception e) {
				LOGGER.error("Exeception while getting site by AA Id ! " + e);
			}

		}

		return false;
	}
	
	/**
	 * Checks if is project admin area relation exist.
	 *
	 * @param projectId the project id
	 * @param siteId the site id
	 * @return true, if is project admin area relation exist
	 */
	public boolean isProjectAdminAreaRelationExist(final String projectId, final String siteId) {
		if (projectId != null && siteId != null) {
			try {
				List<AdminAreasTbl> adminAreaTbl = adminAreaController.getAAByProjectIdAndSiteId(projectId, siteId);
				if (adminAreaTbl != null) {
					return true;
				}
			} catch (Exception e) {
				LOGGER.error("Exeception while getting AA by site and project Id objects! " + e);
			}
		}

		return false;
	}
	
	/**
	 * Checks if is project user relation exist.
	 *
	 * @param projectId the project id
	 * @param userId the user id
	 * @return true, if is project user relation exist
	 */
	public boolean isProjectUserRelationExist(final String projectId, final String userId) {
		if (projectId != null && userId != null) {
			try {
				ProjectResponse projectsByUserId = projectController.getProjectsByUserId(userId);
				if (projectsByUserId != null) {
					Iterable<ProjectsTbl> projectsTbls = projectsByUserId.getProjectsTbls();
					for (ProjectsTbl projectsTbl : projectsTbls) {
						String projectIdFromTbl = projectsTbl.getProjectId();
						if (projectIdFromTbl.equals(projectId)) {
							return true;
						}
					}
				}
			} catch (Exception e) {
				LOGGER.error("Exeception while getting project from user Id ! " + e);
			}

		}

		return false;
	}
}
