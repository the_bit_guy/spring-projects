
package com.magna.xmsystem.xmhotline.model;

import java.util.Map;

import org.eclipse.swt.graphics.Image;

import com.magna.xmsystem.dependencies.utils.LANG_ENUM;
import com.magna.xmsystem.xmhotline.model.interfaces.IXmHotlineModel;

/**
 * This class contains User Application id, name, type(FIXED, NOT_FIXED, PROTECTED), status and Image.
 */
public class UserApplicationModel implements IXmHotlineModel {

	/** Member variable 'userApplicationId' for {@link UserApplicationModel}. */
	private String userAppId;
	
	/** The user app internal name. */
	private String userAppInternalName;
	
	/** The user app internal desc. */
	private String userAppInternalDesc;
	
	/** Member variable 'userApplType' for {@link UserApplicationModel}. */
	private String relType;
	
	/** Member variable 'status' for {@link UserApplicationModel}. */
	private boolean status;
	
	/** Member variable 'image' for {@link UserApplicationModel}. */
	private Image image;
	
	/** The admin area id. */
	private String adminAreaId;
	
	/** The user user app rel id. */
	private String userUserAppRelId;

	/** The user id. */
	private String userId;
	
	/** The user app desc map. */
	private Map<LANG_ENUM, String> userAppDescMap;

	/** The user app name map. */
	private Map<LANG_ENUM, String> userAppNameMap;
	
	/**
	 * Constructor.
	 *
	 * @param userAppId the user app id
	 * @param userAppInternalName the user app name
	 * @param userAppInternalDesc  the user app desc
	 * @param relType the rel type
	 * @param status the status
	 * @param image the image
	 * @param adminAreaId the admin area id
	 * @param userId the user id
	 * @param userAppNameMap the user app name map
	 * @param userAppDescMap the user app desc map
	 */
	public UserApplicationModel(final String userAppId, final String userAppInternalName, final String userAppInternalDesc,
			final String relType, final boolean status, final Image image, final String adminAreaId, String userId, 
			final Map<LANG_ENUM, String> userAppNameMap, final Map<LANG_ENUM, String> userAppDescMap) {
		this.userAppId = userAppId;
		this.userAppInternalName = userAppInternalName;
		this.userAppInternalDesc = userAppInternalDesc;
		this.relType = relType;
		this.status = status;
		this.image = image;
		this.adminAreaId = adminAreaId;
		this.userId = userId;
		this.userAppNameMap = userAppNameMap;
		this.userAppDescMap = userAppDescMap;
	}

	/**
	 * Gets the user app id.
	 *
	 * @return the id
	 */
	public final String getUserAppId() {
		return userAppId;
	}

	/**
	 * Sets the user app id.
	 *
	 * @param userAppId the new user app id
	 */
	public final void setUserAppId(final String userAppId) {
		this.userAppId = userAppId;
	}

	/**
	 * @return the userAppInternalName
	 */
	public String getUserAppInternalName() {
		return userAppInternalName;
	}

	/**
	 * @param userAppInternalName the userAppInternalName to set
	 */
	public void setUserAppInternalName(String userAppInternalName) {
		this.userAppInternalName = userAppInternalName;
	}

	/**
	 * @return the userAppInternalDesc
	 */
	public String getUserAppInternalDesc() {
		return userAppInternalDesc;
	}

	/**
	 * @param userAppInternalDesc the userAppInternalDesc to set
	 */
	public void setUserAppInternalDesc(String userAppInternalDesc) {
		this.userAppInternalDesc = userAppInternalDesc;
	}

	/**
	 * Gets the rel type.
	 *
	 * @return the relType
	 */
	public final String getRelType() {
		return relType;
	}

	/**
	 * Sets the rel type.
	 *
	 * @param relType the relType to set
	 */
	public final void setRelType(final String relType) {
		this.relType = relType;
	}

	/**
	 * Checks if is status.
	 *
	 * @return the status
	 */
	public final boolean isStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 *
	 * @param status the status to set
	 */
	public final void setStatus(final boolean status) {
		this.status = status;
	}

	/**
	 * Gets the image.
	 *
	 * @return the image
	 */
	public final Image getImage() {
		return image;
	}

	/**
	 * Sets the image.
	 *
	 * @param image the image to set
	 */
	public final void setImage(final Image image) {
		this.image = image;
	}

	/**
	 * Gets the admin area id.
	 *
	 * @return the adminAreaId
	 */
	public String getAdminAreaId() {
		return adminAreaId;
	}

	/**
	 * Sets the admin area id.
	 *
	 * @param adminAreaId the adminAreaId to set
	 */
	public void setAdminAreaId(String adminAreaId) {
		this.adminAreaId = adminAreaId;
	}

	/**
	 * Gets the user user app rel id.
	 *
	 * @return the userUserAppRelId
	 */
	public String getUserUserAppRelId() {
		return userUserAppRelId;
	}

	/**
	 * Sets the user user app rel id.
	 *
	 * @param userUserAppRelId the userUserAppRelId to set
	 */
	public void setUserUserAppRelId(String userUserAppRelId) {
		this.userUserAppRelId = userUserAppRelId;
	}
	
	/**
	 * Gets the user id.
	 *
	 * @return the user id
	 */
	public String getUserId() {
		return this.userId;
	}

	/**
	 * Sets the user id.
	 *
	 * @param userId the new user id
	 */
	public void setUserId(final String userId) {
		this.userId = userId;
	}

	/**
	 * Gets the user app desc map.
	 *
	 * @return the userAppDescMap
	 */
	public Map<LANG_ENUM, String> getUserAppDescMap() {
		return userAppDescMap;
	}

	/**
	 * Sets the user app desc map.
	 *
	 * @param userAppDescMap the userAppDescMap to set
	 */
	public void setUserAppDescMap(Map<LANG_ENUM, String> userAppDescMap) {
		this.userAppDescMap = userAppDescMap;
	}

	/**
	 * Gets the user app name map.
	 *
	 * @return the userAppNameMap
	 */
	public Map<LANG_ENUM, String> getUserAppNameMap() {
		return userAppNameMap;
	}

	/**
	 * Sets the user app name map.
	 *
	 * @param userAppNameMap the userAppNameMap to set
	 */
	public void setUserAppNameMap(Map<LANG_ENUM, String> userAppNameMap) {
		this.userAppNameMap = userAppNameMap;
	}	
}
