package com.magna.xmsystem.xmhotline.ui.modelprocessor;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.ui.menu.MHandledItem;
import org.eclipse.e4.ui.model.application.ui.menu.MMenu;
import org.eclipse.e4.ui.model.application.ui.menu.MMenuElement;
import org.eclipse.e4.ui.workbench.modeling.EModelService;

import com.magna.xmsystem.dependencies.utils.LANG_ENUM;
import com.magna.xmsystem.xmhotline.util.XmHotlineUtil;

public class LangMenuProcessor {
	// the menu is injected based on the parameter
	/** The menu. */
	// defined in the extension point
	@Inject
	@Named("com.magna.xmsystem.xmhotline.ui.menu.language")
	private MMenu menu;

	@Execute
	public void execute(EModelService modelService) {
		final LANG_ENUM currentLocaleEnum = XmHotlineUtil.getInstance().getCurrentLocaleEnum();
		if (currentLocaleEnum == null)
			return;
		final List<MMenuElement> children = menu.getChildren();
		for (MMenuElement muiElement : children) {
			if ("com.magna.xmsystem.xmhotline.ui.handledmenuitem.english".equals(muiElement.getElementId())
					&& currentLocaleEnum == LANG_ENUM.ENGLISH) {
				((MHandledItem) muiElement).setSelected(true);
			} else if ("com.magna.xmsystem.xmhotline.ui.handledmenuitem.german".equals(muiElement.getElementId())
					&& currentLocaleEnum == LANG_ENUM.GERMAN) {
				((MHandledItem) muiElement).setSelected(true);
			}
		}
	}

}
