package com.magna.xmsystem.xmhotline.ui.parts;

import java.util.List;

import org.eclipse.e4.core.services.events.IEventBroker;
import org.eclipse.jface.resource.ColorRegistry;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.events.MouseTrackAdapter;
import org.eclipse.swt.events.MouseTrackListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magna.xmbackend.vo.enums.ApplicationRelationType;
import com.magna.xmsystem.dependencies.utils.LANG_ENUM;
import com.magna.xmsystem.dependencies.utils.XMSystemUtil;
import com.magna.xmsystem.xmhotline.model.MainModel;
import com.magna.xmsystem.xmhotline.model.ProjectApplicationModel;
import com.magna.xmsystem.xmhotline.model.ProjectModel;
import com.magna.xmsystem.xmhotline.model.UserApplicationModel;
import com.magna.xmsystem.xmhotline.model.interfaces.IXmHotlineModel;
import com.magna.xmsystem.xmhotline.ui.message.MessageRegistry;
import com.magna.xmsystem.xmhotline.util.Constants;
import com.magna.xmsystem.xmhotline.util.XmHotlineHelper;
import com.magna.xmsystem.xmhotline.util.XmHotlineUtil;

/**
 * class for create control XMHotline.
 * 
 * @author shashwat.anand
 */
public class CustomLabelComposite extends Composite {
	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(CustomLabelComposite.class);
	
	/** The default BG color. */
	final Color defaultBGColor;

	/** The default FG color. */
	final Color defaultFGColor;
	
	/** The mouse hover BGFG over color. */
	final Color defaultMouseEnterBGColor;

	/** The mouse hover FG over color. */
	final Color defaultMouseEnterFGColor;
	
	/** The check. */
	private boolean check;
	
	/** The image lbl. */
	private CLabel imageLbl;
	
	/** The check btn. */
	private Button checkBtn;
	
	/** The check btn. *//*
	private Label dummyLbl;*/
	
	/** The model. */
	private IXmHotlineModel model;

	/** The registry. */
	private MessageRegistry registry;

	/** The event broker. */
	private IEventBroker eventBroker;
	
	/** The xm hotline helper. */
	protected XmHotlineHelper xmHotlineHelper;
	
	/**
	 * Instantiates a new custom label composite.
	 *
	 * @param parent the parent
	 * @param model the model
	 * @param registry the registry
	 * @param eventBroker the event broker
	 */
	public CustomLabelComposite(final Composite parent, final IXmHotlineModel model, final MessageRegistry registry, final IEventBroker eventBroker) {
		super(parent, SWT.BORDER);
		this.model = model;
		this.registry = registry;
		this.eventBroker = eventBroker;
		this.xmHotlineHelper = XmHotlineHelper.getInstance();
		
		this.defaultBGColor = getSavedColor(255, 255, 255);
		this.defaultFGColor = getSavedColor(0, 0, 0);
		this.defaultMouseEnterBGColor = getSavedColor(0, 197, 205);
		this.defaultMouseEnterFGColor = getSavedColor(255, 255, 255);
		
		initGui();
		initListeners();
	}
	
	/**
	 * Gets the saved color.
	 *
	 * @param r {@link int}
	 * @param g {@link int}
	 * @param b {@link int}
	 * @return the saved color
	 */
	private Color getSavedColor(int r, int g, int b) {
		final String colorString = "COLOR:" + r + "-" + g + "-" + b;
		ColorRegistry colorRegistry = JFaceResources.getColorRegistry();
		if (!colorRegistry.hasValueFor(colorString)) {
			colorRegistry.put(colorString, new RGB(r, g, b));
		}
		return colorRegistry.get(colorString);
	}
	
	/* (non-Javadoc)
	 * @see org.eclipse.swt.widgets.Control#setBackground(org.eclipse.swt.graphics.Color)
	 */
	@Override
	public void setBackground(final Color color) {
		super.setBackground(color);
		if(this.imageLbl != null && !this.imageLbl.isDisposed()) {
			this.imageLbl.setBackground(color);
		}
		if(this.checkBtn != null && !this.checkBtn.isDisposed()) {
			this.checkBtn.setBackground(color);
		}	
	}

	/* (non-Javadoc)
	 * @see org.eclipse.swt.widgets.Control#setForeground(org.eclipse.swt.graphics.Color)
	 */
	@Override
	public void setForeground(final Color color) {
		super.setForeground(color);
		if(this.imageLbl != null && !this.imageLbl.isDisposed()) {
			this.imageLbl.setForeground(color);
		}
		if(this.checkBtn != null && !this.checkBtn.isDisposed()) {
			this.checkBtn.setForeground(color);
		}
	}
	
	@Override
	public void dispose() {
		super.dispose();
		if(this.imageLbl != null && !this.imageLbl.isDisposed()) {
			this.imageLbl.dispose();
		}
		if(this.checkBtn != null && !this.checkBtn.isDisposed()) {
			this.checkBtn.dispose();
		}
	}
	
	@Override
	public void setEnabled(boolean enabled) {
		super.setEnabled(enabled);
		this.imageLbl.setEnabled(enabled);
		this.checkBtn.setEnabled(enabled);		
	}

	/* (non-Javadoc)
	 * @see org.eclipse.swt.widgets.Control#addMouseListener(org.eclipse.swt.events.MouseListener)
	 */
	@Override
	public void addMouseListener(MouseListener listener) {
		super.addMouseListener(listener);
		imageLbl.addMouseListener(listener);
		checkBtn.addMouseListener(listener);
	}

	/* (non-Javadoc)
	 * @see org.eclipse.swt.widgets.Control#addMouseTrackListener(org.eclipse.swt.events.MouseTrackListener)
	 */
	@Override
	public void addMouseTrackListener(MouseTrackListener listener) {
		super.addMouseTrackListener(listener);
		imageLbl.addMouseTrackListener(listener);
		checkBtn.addMouseTrackListener(listener);
	}
	
	@Override
	public void setToolTipText(String string) {
		super.setToolTipText(string);
		if(this.imageLbl != null && !this.imageLbl.isDisposed()) {
			this.imageLbl.setToolTipText(string);
		}
		if(this.checkBtn != null && !this.checkBtn.isDisposed()) {
			this.checkBtn.setToolTipText(string);
		}
	}
	
	private void initListeners() {
		this.addMouseTrackListener(new MouseTrackAdapter() {
            
            /**
             * Mouse enter.
             *
             * @param event the event
             */
            @Override
            public void mouseEnter(final MouseEvent event) {
            	if(checkBtn.isEnabled()) {
            		setBackground(defaultMouseEnterBGColor);
                	setForeground(defaultMouseEnterFGColor);
            	}
            	
            	if(model != null) {
            		String statusString = null;
            		if (model instanceof ProjectModel) {
            			statusString = ((ProjectModel) model).getProjectName();
            		} else if (model instanceof ProjectApplicationModel) {
            			statusString = ((ProjectApplicationModel) model).getProjectAppInternalName();
            		} else if (model instanceof UserApplicationModel) {
            			statusString = ((UserApplicationModel) model).getUserAppInternalName();
            		}
            		if (!XMSystemUtil.isEmpty(statusString)) {
            			eventBroker.send(Constants.STATUSBAR, statusString);
            		} else {
            			eventBroker.send(Constants.STATUSBAR, "CAXHotline");
            		}
            	}
            }
            
            @Override
			public void mouseExit(MouseEvent e) {
            	if(checkBtn.isEnabled()) {
                	setBackground(defaultBGColor);
                	setForeground(defaultFGColor);
            	}
			}
		});
		
		this.checkBtn.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(final SelectionEvent event) {
				Button checkBoxWidget = (Button)event.widget;
				boolean selection = checkBoxWidget.getSelection();
				final CustomLabelComposite widget = (CustomLabelComposite) checkBoxWidget.getParent();
				IXmHotlineModel model = widget.getModel();
				final MainModel mainModel = XmHotlineHelper.getInstance().getMainModel();
				if (model instanceof ProjectModel) {
					processForProjectUserRelation(selection, model, mainModel);
					if(selection){
						LOGGER.info("Project '" + ((ProjectModel) model).getProjectName() + "' got selected" );
					} else {
						LOGGER.info("Project '" + ((ProjectModel) model).getProjectName() + "' got deselected" );
					}
				} else if (model instanceof UserApplicationModel) {
					processForUserAppAdminAreaRelation(selection, model, mainModel);
					if(selection){
						LOGGER.info("UserApplication '" + ((UserApplicationModel) model).getUserAppInternalName() + "' got selected" );
					} else {
						LOGGER.info("UserApplication '" + ((UserApplicationModel) model).getUserAppInternalName() + "' got deselected" );
					}
				} else if (model instanceof ProjectApplicationModel) {
					processForProjectAppUserAdminAreaRel(selection, model, mainModel);
					if(selection){
						LOGGER.info("ProjectApplication '" + ((ProjectApplicationModel) model).getProjectAppInternalName() + "' got selected" );
					} else {
						LOGGER.info("ProjectApplication '" + ((ProjectApplicationModel) model).getProjectAppInternalName() + "' got deselected" );
					}
				}
			}
		});
	}

	/**
	 * method for initialize Graphical user interface.
	 */
	private void initGui() {
		try {
			final GridLayout widgetContainerLayout = new GridLayout(3, false);
			widgetContainerLayout.verticalSpacing = 0;
			widgetContainerLayout.horizontalSpacing = 1;
			widgetContainerLayout.marginRight = 30;
			widgetContainerLayout.marginLeft = 1;
			widgetContainerLayout.marginTop = 0;
			widgetContainerLayout.marginBottom = 0;
			widgetContainerLayout.marginWidth = 0;
			widgetContainerLayout.marginHeight = 0;
			this.setLayout(widgetContainerLayout);
			this.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
			this.imageLbl = new CLabel(this, SWT.CENTER);
			this.imageLbl.setTopMargin(0);
			this.imageLbl.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 2));	
			this.checkBtn = new Button(this, SWT.CHECK | SWT.CENTER);
			GridData gridData = new GridData(SWT.RIGHT, SWT.CENTER, false, true);
			this.checkBtn.setLayoutData(gridData);
			
			setBackground(defaultBGColor);
			setForeground(defaultFGColor);
			
			setNameAndImage();
			this.checkBtn.setSelection(check);
		} catch (Exception e) {
			LOGGER.error("Unable to create UI elements", e);
		}
	}

	/**
	 * set name and image to control.
	 */
	private void setNameAndImage() {
		if (model instanceof ProjectModel) {
			ProjectModel project = (ProjectModel) model;
			Image projectImage = xmHotlineHelper.getImage(project.getIcon(), project.isStatus());
			this.imageLbl.setImage(projectImage);
			this.imageLbl.setText("\t" + project.getProjectName().trim());
			/*registry.register((text) -> updateWidgetToolTipText(text), message -> {
				return registerMessageToLabelToolTip(this.imageLbl, project);
			});*/
			this.setEnabled(false);
		} else if (model instanceof UserApplicationModel) {
			UserApplicationModel userApp = (UserApplicationModel) model;					
			if (userApp.getRelType().equalsIgnoreCase(ApplicationRelationType.FIXED.name())) {
				check = true;
			}
			this.imageLbl.setImage(userApp.getImage());
			registry.register((text) -> updateWidgetText(text, this.imageLbl), message -> {
				return registerMessageToLabel(this.imageLbl, userApp);
			});
			registry.register((text) -> updateWidgetToolTipText(text), message -> {
				return registerMessageToLabelToolTip(this.imageLbl, userApp);
			});
			//this.imageLbl.setText("\t" + userApp.getUserAppName());
		} else if (model instanceof ProjectApplicationModel) {
			ProjectApplicationModel projectAppl = (ProjectApplicationModel) model;
			if (projectAppl.getRelType().equalsIgnoreCase(ApplicationRelationType.FIXED.name())) {
				check = true;
			}
			this.imageLbl.setImage(projectAppl.getImage());
			registry.register((text) -> updateWidgetText(text, this.imageLbl), message -> {
				return registerMessageToLabel(this.imageLbl, projectAppl);
			});
			registry.register((text) -> updateWidgetToolTipText(text), message -> {
				return registerMessageToLabelToolTip(this.imageLbl, projectAppl);
			});
			//this.imageLbl.setText("\t" + projectAppl.getProjectAppName());
		}
	}
	
	/**
	 * Update widget text.
	 *
	 * @param message the message
	 * @param control the control
	 */
	private void updateWidgetText(final String message, final CLabel control) {
		if (control != null && !control.isDisposed()) {
			control.setText("\t" + message.trim());
			control.getParent().layout();
			control.getParent().getParent().update();
		}
	}
	
	public void updateWidgetImage(final Image image) {
		if (image != null && !image.isDisposed()) {
			imageLbl.setImage(image);
			
			imageLbl.getParent().layout();
			imageLbl.getParent().getParent().update();
		}
	}
	
	/**
	 * Update widget tool tip text.
	 *
	 * @param message the message
	 * @param control the control
	 */
	private void updateWidgetToolTipText(final String message) {
		if (this != null && !this.isDisposed()) {
			this.setToolTipText(message);
			this.getParent().layout();
			this.getParent().getParent().update();
		}
	}
		
	/**
	 * Register message to label.
	 * @param label the label
	 * @param model the model
	 * @return the string
	 */
	private String registerMessageToLabel(final CLabel label, IXmHotlineModel model) {
		if (label != null && !label.isDisposed()) {
			XmHotlineUtil instance = XmHotlineUtil.getInstance();
			final LANG_ENUM currentLocale = instance.getCurrentLocaleEnum();
			if (model instanceof UserApplicationModel) {
				String name = ((UserApplicationModel) model).getUserAppNameMap().get(currentLocale);
				name = !(XMSystemUtil.isEmpty(name)) ? name : ((UserApplicationModel) model).getUserAppInternalName();
				return name;
			} else if (model instanceof ProjectApplicationModel) {
				String name = ((ProjectApplicationModel) model).getProjectAppNameMap().get(currentLocale);
				name = !(XMSystemUtil.isEmpty(name)) ? name
						: ((ProjectApplicationModel) model).getProjectAppInternalName();
				return name;
			}
		}
		return "";
	}
	
	/**
	 * Register message to label tool tip.
	 *
	 * @param label the label
	 * @param model the model
	 * @return the string
	 */
	private String registerMessageToLabelToolTip(final CLabel label, final IXmHotlineModel model) {
		if (label != null && !label.isDisposed()) {
			XmHotlineUtil instance = XmHotlineUtil.getInstance();
			final LANG_ENUM currentLocale = instance.getCurrentLocaleEnum();
			final LANG_ENUM defaultLocale = instance.getDefaultLocaleEnum();
			if (model instanceof ProjectModel) {
				String name = ((ProjectModel) model).getProjectDescMap().get(currentLocale);
				name = !(XMSystemUtil.isEmpty(name)) ? name
						: ((ProjectModel) model).getProjectDescMap().get(defaultLocale);
				name = !(XMSystemUtil.isEmpty(name)) ? name : ((ProjectModel) model).getProjectName();
				return name;
			} else if (model instanceof ProjectApplicationModel) {
				String name = ((ProjectApplicationModel) model).getProjectAppDescMap().get(currentLocale);
				/*
				 * name = !(XMSystemUtil.isEmpty(name)) ? name :
				 * ((ProjectApplicationModel)
				 * model).getProjectAppDescMap().get(defaultLocale);
				 */
				name = !(XMSystemUtil.isEmpty(name)) ? name
						: ((ProjectApplicationModel) model).getProjectAppInternalDesc();
				name = !(XMSystemUtil.isEmpty(name)) ? name
						: ((ProjectApplicationModel) model).getProjectAppInternalName();

				return name;
			} else if (model instanceof UserApplicationModel) {
				String name = ((UserApplicationModel) model).getUserAppDescMap().get(currentLocale);
				name = !(XMSystemUtil.isEmpty(name)) ? name : ((UserApplicationModel) model).getUserAppInternalDesc();
				name = !(XMSystemUtil.isEmpty(name)) ? name : ((UserApplicationModel) model).getUserAppInternalName();
				return name;
			} else if (model instanceof ProjectApplicationModel) {
				String name = ((ProjectApplicationModel) model).getProjectAppDescMap().get(currentLocale);
				/*
				 * name = !(XMSystemUtil.isEmpty(name)) ? name :
				 * ((ProjectApplicationModel)
				 * model).getProjectAppDescMap().get(defaultLocale);
				 */
				name = !(XMSystemUtil.isEmpty(name)) ? name
						: ((ProjectApplicationModel) model).getProjectAppInternalDesc();
				name = !(XMSystemUtil.isEmpty(name)) ? name
						: ((ProjectApplicationModel) model).getProjectAppInternalName();

				return name;
			}
		}
		return "";
	}
	
	/**
	 * Checks if is check.
	 *
	 * @return the check
	 */
	public final boolean isCheck() {
		return check;
	}

	/**
	 * Sets the selection.
	 *
	 * @param check the check to set
	 */
	public final void setSelection(final boolean check) {
		this.check = check;
		this.checkBtn.setSelection(check);
	}
	
	/**
	 * Gets the model.
	 *
	 * @return the model
	 */
	public final IXmHotlineModel getModel() {
		return model;
	}

	/**
	 * Sets the model.
	 *
	 * @param model the model to set
	 */
	public final void setModel(IXmHotlineModel model) {
		this.model = model;
	}
	
	/**
	 * Process for project user relation.
	 *
	 * @param selection the selection
	 * @param model the model
	 * @param mainModel the main model
	 */
	private void processForProjectUserRelation(final boolean selection, final IXmHotlineModel model, final MainModel mainModel) {
		final List<ProjectModel> projectUserRelationAddList = mainModel.getProjectUserRelationAddList();
		final List<ProjectModel> projectUserRelationDeleteList = mainModel.getProjectUserRelationDeleteList();
		ProjectModel removeProjectModel = null;
		if (selection) {
			// add relation
			for (final ProjectModel projectModel : projectUserRelationDeleteList) {
				if (projectModel.getProjectId().equals(((ProjectModel)model).getProjectId())) {
					removeProjectModel = projectModel;
					break;
				}
			}
			if (removeProjectModel != null) {
				projectUserRelationDeleteList.remove(removeProjectModel);
			} else {
				projectUserRelationAddList.add((ProjectModel) model);
			}
		} else {
			// remove relation
			for (final ProjectModel projectModel : projectUserRelationAddList) {
				if (projectModel.getProjectId().equals(((ProjectModel)model).getProjectId())) {
					removeProjectModel = projectModel;
					break;
				}
			}
			if (removeProjectModel != null) {
				projectUserRelationAddList.remove(removeProjectModel);
			} else {
				projectUserRelationDeleteList.add((ProjectModel) model);
			}
		}
	}
	
	/**
	 * Process for user app admin area relation.
	 *
	 * @param selection the selection
	 * @param model the model
	 * @param mainModel the main model
	 */
	private void processForUserAppAdminAreaRelation(final boolean selection, final IXmHotlineModel model, final MainModel mainModel) {
		List<UserApplicationModel> userAppUserRelationAddList = mainModel.getUserAppUserRelationAddList();
		List<UserApplicationModel> userAppUserRelationDeleteList = mainModel.getUserAppUserRelationDeleteList();
		UserApplicationModel removeUserAppModel = null;
		if (selection) {
			// add relation
			for (final UserApplicationModel userApplicationModel : userAppUserRelationDeleteList) {
				if (userApplicationModel.getUserAppId().equals(((UserApplicationModel)model).getUserAppId())) {
					removeUserAppModel = userApplicationModel;
					break;
				}
			}
			if (removeUserAppModel != null) {
				userAppUserRelationDeleteList.remove(removeUserAppModel);
			} else {
				userAppUserRelationAddList.add((UserApplicationModel) model);
			}
		} else {
			// remove relation
			for (final UserApplicationModel userApplicationModel : userAppUserRelationAddList) {
				if (userApplicationModel.getUserAppId().equals(((UserApplicationModel)model).getUserAppId())) {
					removeUserAppModel = userApplicationModel;
					break;
				}
			}
			if (removeUserAppModel != null) {
				userAppUserRelationAddList.remove(removeUserAppModel);
			} else {
				userAppUserRelationDeleteList.add((UserApplicationModel) model);
			}
		}
	}
	
	/**
	 * Process for project app user admin area rel.
	 *
	 * @param selection the selection
	 * @param model the model
	 * @param mainModel the main model
	 */
	private void processForProjectAppUserAdminAreaRel(final boolean selection, final IXmHotlineModel model,
			final MainModel mainModel) {
		List<ProjectApplicationModel> projectAppUserRelationAddList = mainModel.getProjectAppUserRelationAddList();
		List<ProjectApplicationModel> projectAppUserRelationDeleteList = mainModel.getProjectAppUserRelationDeleteList();
		ProjectApplicationModel removeProjectAppModel = null;
		if (selection) {
			// add relation
			for (ProjectApplicationModel projectApplicationModel : projectAppUserRelationDeleteList) {
				if (projectApplicationModel.getProjectAppId().equals(((ProjectApplicationModel) model).getProjectAppId())) {
					removeProjectAppModel = projectApplicationModel;
					break;
				}
			}
			if (removeProjectAppModel != null) {
				projectAppUserRelationDeleteList.remove(removeProjectAppModel);
			} else {
				projectAppUserRelationAddList.add((ProjectApplicationModel) model);
			}
		} else {
			// remove relation
			for (final ProjectApplicationModel projectApplicationModel : projectAppUserRelationAddList) {
				if (projectApplicationModel.getProjectAppId().equals(((ProjectApplicationModel)model).getProjectAppId())) {
					removeProjectAppModel = projectApplicationModel;
					break;
				}
			}
			if (removeProjectAppModel != null) {
				projectAppUserRelationAddList.remove(removeProjectAppModel);
			} else {
				projectAppUserRelationDeleteList.add((ProjectApplicationModel) model);
			}
		}
	}
}
