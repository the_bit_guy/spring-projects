package com.magna.xmsystem.xmhotline.ui.message;

import javax.inject.Inject;

import org.eclipse.e4.core.di.annotations.Creatable;
import org.eclipse.e4.core.services.nls.BaseMessageRegistry;
import org.eclipse.e4.core.services.nls.Translation;

/**
 * Class for Message Registry.
 *
 * @author shashwat.anand
 */
@Creatable
public class MessageRegistry extends BaseMessageRegistry<Message> {

	/* (non-Javadoc)
	 * @see org.eclipse.e4.core.services.nls.BaseMessageRegistry#updateMessages(java.lang.Object)
	 */
	@Inject
	@Override
	public void updateMessages(@Translation Message messages) {
		super.updateMessages(messages);
	}
}
