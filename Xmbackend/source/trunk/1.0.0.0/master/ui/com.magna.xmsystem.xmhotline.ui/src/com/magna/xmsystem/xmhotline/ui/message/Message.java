package com.magna.xmsystem.xmhotline.ui.message;

import java.text.MessageFormat;

import javax.annotation.PostConstruct;

/**
 * Message Class which contains all translation message
 * 
 * @author shashwat.anand
 *
 */
public class Message {
	public String siteFilterGroup;
	public String adminAreaFilterGroup;
	public String userFilterGroup;
	public String projectFilterGroup;
	public String showAllButton;
	public String filterLabel;
	public String projectInfoGroup;
	public String fixUaserAppInfoGroup;
	public String nonFixUaserAppInfoGroup;
	public String fixProjectAppInfoGroup;
	public String nonFixProjectAppInfoGroup;
	public String saveButton;
	public String resetButton;
	public String protectedUserAppInfoGroup;
	public String protectedProjectAppInfoGroup;
	public String windowtitle;
	public String notModified;
	public String cancelMsg;
	public String saveInfo;
	public String savedSuccessfullyMsg;
	public String saveFailMsg;
	public String errorDialogTitile;
	public String unauthorizedLdapAccessMessage;
	public String unauthorizedUserAccessMessage;
	public String serverNotReachable;
	public String aboutDialogTitle;
	public String exitDialogTitle;
	public String exitDialogMessage;
	public String loginDialogUserName;
	public String loginDialogPassword;
	public String loginDialogLoginBtn;
	public String loginDialogCancelBtn;
	public String iconNotReachableMsg;
	public String notAssignedToSiteMsg;
	public String adminAreaSelectionMsg;
	public String projNotFoundMsg;
	public String projNotFoundForSiteMsg;
	public String userAppNotFoundForAAMsg;
	public String selectUserMsg;
	public String undefProjAppMsg;
	public String projAppNotFoundForAAMsg;
	public String andProjMsg;
	public String filterComboMsg;
	public String projUserPerMsg;
	public String relAlreadyExistsMsg;
	public String projUserDelMsg;
	public String permissionErrorMsg;
	public String selectSiteMsg;
	public String savePermissionMsg;
	public String projectUserAddSuccessMsg;
	public String projectUserAddErrorMsg;
	public String relAlreadyExistMsg;
	public String projectUserRemoveSuccessMsg;
	public String projectUserRemoveErrorMsg;
	public String userAppAddSuccessMsg;
	public String userAppAddErrorMsg;
	public String userAppRemoveSuccessMsg;
	public String userAppRemoveErrorMsg;
	public String projectAppAddSuccessMsg;
	public String projectAppAddErrorMsg;
	public String projectAppRemoveSuccessMsg;
	public String projectAppRemoveErrorMsg;
	public String addProjectUserRelMsgPart;
	public String deleteProjectUserRelMsgPart;
	public String andUserMsgPart;
	public String forUserMsgPart;
	public String workspaceLockTitle;
	public String workspaceLockMsg;
	public String iconErrorTitle;
	public String iconErrorMsg;
	public String relationCreated;
	public String updateUserAppMsgPart;
	public String updateProjectAppMsgPart;
	public String inCorrectServerURI;

	/**
	 * Method initialize the messages from resource bundles
	 */
	@PostConstruct
	public void format() {
		iconNotReachableMsg = MessageFormat.format(iconNotReachableMsg, "ResourceBundles"); //$NON-NLS-1$
		notAssignedToSiteMsg = MessageFormat.format(notAssignedToSiteMsg, "ResourceBundles"); //$NON-NLS-1$
		adminAreaSelectionMsg = MessageFormat.format(adminAreaSelectionMsg, "ResourceBundles"); //$NON-NLS-1$
		projNotFoundMsg = MessageFormat.format(projNotFoundMsg, "ResourceBundles"); //$NON-NLS-1$
		projNotFoundForSiteMsg = MessageFormat.format(projNotFoundForSiteMsg, "ResourceBundles"); //$NON-NLS-1$
		userAppNotFoundForAAMsg = MessageFormat.format(userAppNotFoundForAAMsg, "ResourceBundles"); //$NON-NLS-1$
		selectUserMsg = MessageFormat.format(selectUserMsg, "ResourceBundles"); //$NON-NLS-1$
		undefProjAppMsg = MessageFormat.format(undefProjAppMsg, "ResourceBundles"); //$NON-NLS-1$
		projAppNotFoundForAAMsg = MessageFormat.format(projAppNotFoundForAAMsg, "ResourceBundles"); //$NON-NLS-1$
		andProjMsg = MessageFormat.format(andProjMsg, "ResourceBundles"); //$NON-NLS-1$
		filterComboMsg = MessageFormat.format(filterComboMsg, "ResourceBundles"); //$NON-NLS-1$
		projUserPerMsg = MessageFormat.format(projUserPerMsg, "ResourceBundles"); //$NON-NLS-1$
		relAlreadyExistsMsg = MessageFormat.format(relAlreadyExistsMsg, "ResourceBundles"); //$NON-NLS-1$
		projUserDelMsg = MessageFormat.format(projUserDelMsg, "ResourceBundles"); //$NON-NLS-1$
		permissionErrorMsg = MessageFormat.format(permissionErrorMsg, "ResourceBundles"); //$NON-NLS-1$
		selectSiteMsg = MessageFormat.format(selectSiteMsg, "ResourceBundles"); //$NON-NLS-1$
		saveFailMsg = MessageFormat.format(saveFailMsg, "ResourceBundles"); //$NON-NLS-1$
		savedSuccessfullyMsg = MessageFormat.format(savedSuccessfullyMsg, "ResourceBundles"); //$NON-NLS-1$
		saveInfo = MessageFormat.format(saveInfo, "ResourceBundles"); //$NON-NLS-1$
		cancelMsg = MessageFormat.format(cancelMsg, "ResourceBundles"); //$NON-NLS-1$
		notModified = MessageFormat.format(notModified, "ResourceBundles"); //$NON-NLS-1$
		windowtitle = MessageFormat.format(windowtitle, "ResourceBundles"); //$NON-NLS-1$
		siteFilterGroup = MessageFormat.format(siteFilterGroup, "ResourceBundles"); //$NON-NLS-1$
		adminAreaFilterGroup = MessageFormat.format(adminAreaFilterGroup, "ResourceBundles"); //$NON-NLS-1$
		userFilterGroup = MessageFormat.format(userFilterGroup, "ResourceBundles"); //$NON-NLS-1$
		projectFilterGroup = MessageFormat.format(projectFilterGroup, "ResourceBundles"); //$NON-NLS-1$
		showAllButton = MessageFormat.format(showAllButton, "ResourceBundles"); //$NON-NLS-1$
		filterLabel = MessageFormat.format(filterLabel, "ResourceBundles"); //$NON-NLS-1$
		projectInfoGroup = MessageFormat.format(projectInfoGroup, "ResourceBundles"); //$NON-NLS-1$
		fixUaserAppInfoGroup = MessageFormat.format(fixUaserAppInfoGroup, "ResourceBundles"); //$NON-NLS-1$
		nonFixUaserAppInfoGroup = MessageFormat.format(nonFixUaserAppInfoGroup, "ResourceBundles"); //$NON-NLS-1$
		fixProjectAppInfoGroup = MessageFormat.format(fixProjectAppInfoGroup, "ResourceBundles"); //$NON-NLS-1$
		nonFixProjectAppInfoGroup = MessageFormat.format(nonFixProjectAppInfoGroup, "ResourceBundles"); //$NON-NLS-1$
		protectedUserAppInfoGroup = MessageFormat.format(protectedUserAppInfoGroup, "ResourceBundles"); //$NON-NLS-1$
		protectedProjectAppInfoGroup = MessageFormat.format(protectedProjectAppInfoGroup, "ResourceBundles"); //$NON-NLS-1$
		saveButton = MessageFormat.format(saveButton, "ResourceBundles"); //$NON-NLS-1$
		resetButton = MessageFormat.format(resetButton, "ResourceBundles"); //$NON-NLS-1$

		errorDialogTitile = MessageFormat.format(errorDialogTitile, "ResourceBundles"); //$NON-NLS-1$
		unauthorizedLdapAccessMessage = MessageFormat.format(unauthorizedLdapAccessMessage, "ResourceBundles"); //$NON-NLS-1$
		unauthorizedUserAccessMessage = MessageFormat.format(unauthorizedUserAccessMessage, "ResourceBundles"); //$NON-NLS-1$
		serverNotReachable =  MessageFormat.format(serverNotReachable, "ResourceBundles"); // $NON-NLS-1$
		
		aboutDialogTitle = MessageFormat.format(aboutDialogTitle, "ResourceBundles"); // $NON-NLS-1$
		exitDialogTitle =  MessageFormat.format(exitDialogTitle, "ResourceBundles"); // $NON-NLS-1$
		exitDialogMessage = MessageFormat.format(exitDialogMessage, "ResourceBundles"); // $NON-NLS-1$
		
		loginDialogUserName = MessageFormat.format(loginDialogUserName, "ResourceBundles"); // $NON-NLS-1$
		loginDialogPassword = MessageFormat.format(loginDialogPassword, "ResourceBundles"); // $NON-NLS-1$
		loginDialogLoginBtn =  MessageFormat.format(loginDialogLoginBtn, "ResourceBundles"); // $NON-NLS-1$
		loginDialogCancelBtn = MessageFormat.format(loginDialogCancelBtn, "ResourceBundles"); // $NON-NLS-1$
		
		savePermissionMsg = MessageFormat.format(savePermissionMsg, "ResourceBundles"); // $NON-NLS-1$
		
		relationCreated = MessageFormat.format(relationCreated, "ResourceBundles"); // $NON-NLS-1$
		projectUserAddSuccessMsg = MessageFormat.format(projectUserAddSuccessMsg, "ResourceBundles"); // $NON-NLS-1$
		projectUserAddErrorMsg = MessageFormat.format(projectUserAddErrorMsg, "ResourceBundles"); // $NON-NLS-1$
		relAlreadyExistMsg = MessageFormat.format(relAlreadyExistMsg, "ResourceBundles"); // $NON-NLS-1$
		projectUserRemoveSuccessMsg = MessageFormat.format(projectUserRemoveSuccessMsg, "ResourceBundles"); // $NON-NLS-1$
		projectUserRemoveErrorMsg = MessageFormat.format(projectUserRemoveErrorMsg, "ResourceBundles"); // $NON-NLS-1$
		userAppAddSuccessMsg = MessageFormat.format(userAppAddSuccessMsg, "ResourceBundles"); // $NON-NLS-1$
		userAppAddErrorMsg = MessageFormat.format(userAppAddErrorMsg, "ResourceBundles"); // $NON-NLS-1$
		userAppRemoveSuccessMsg = MessageFormat.format(userAppRemoveSuccessMsg, "ResourceBundles"); // $NON-NLS-1$
		userAppRemoveErrorMsg = MessageFormat.format(userAppRemoveErrorMsg, "ResourceBundles"); // $NON-NLS-1$
		projectAppAddSuccessMsg = MessageFormat.format(projectAppAddSuccessMsg, "ResourceBundles"); // $NON-NLS-1$
		projectAppAddErrorMsg = MessageFormat.format(projectAppAddErrorMsg, "ResourceBundles"); // $NON-NLS-1$
		projectAppRemoveSuccessMsg = MessageFormat.format(projectAppRemoveSuccessMsg, "ResourceBundles"); // $NON-NLS-1$
		projectAppRemoveErrorMsg = MessageFormat.format(projectAppRemoveErrorMsg, "ResourceBundles"); // $NON-NLS-1$
		
		addProjectUserRelMsgPart = MessageFormat.format(addProjectUserRelMsgPart, "ResourceBundles"); // $NON-NLS-1$
		deleteProjectUserRelMsgPart = MessageFormat.format(deleteProjectUserRelMsgPart, "ResourceBundles"); // $NON-NLS-1$
		andUserMsgPart = MessageFormat.format(andUserMsgPart, "ResourceBundles"); // $NON-NLS-1$
		forUserMsgPart = MessageFormat.format(forUserMsgPart, "ResourceBundles"); // $NON-NLS-1$
		
		workspaceLockTitle =  MessageFormat.format(workspaceLockTitle, "ResourceBundles"); // $NON-NLS-1$
		workspaceLockMsg = MessageFormat.format(workspaceLockMsg, "ResourceBundles"); // $NON-NLS-1$
		
		iconErrorTitle = MessageFormat.format(iconErrorTitle, "ResourceBundles");// $NON-NLS-1$
		iconErrorMsg = MessageFormat.format(iconErrorMsg, "ResourceBundles");// $NON-NLS-1$ 
		
		updateUserAppMsgPart = MessageFormat.format(updateUserAppMsgPart, "ResourceBundles");// $NON-NLS-1$
		updateProjectAppMsgPart = MessageFormat.format(updateProjectAppMsgPart, "ResourceBundles");// $NON-NLS-1$
		
		inCorrectServerURI = MessageFormat.format(inCorrectServerURI, "ResourceBundles");// $NON-NLS-1$
	}
}
