package com.magna.xmsystem.xmhotline.model;

import org.eclipse.swt.graphics.Image;

import com.magna.xmsystem.xmhotline.model.interfaces.IXmHotlineModel;

/**
 * This class contains Administrator Area id, name and image.
 */
public class AdminAreaModel implements IXmHotlineModel {
	
	/** Member variable 'AdminAreaId' for {@link AdminAreaModel}. */
	private String adminAreaId;
	
	/** Member variable 'AdminAreaIdName' for {@link AdminAreaModel}. */
	private String adminAreaName;
	
	/** Member variable 'image' for {@link AdminAreaModel}. */
	private Image image;

	/** The status. */
	private boolean status;
	
	/**
	 * Instantiates a new admin area model.
	 *
	 * @param adminAreaId the admin area id
	 * @param adminAreaName the admin area name
	 * @param status the status
	 * @param image the image
	 */
	public AdminAreaModel(final String adminAreaId, final String adminAreaName, final boolean status, final Image image) {
		this.adminAreaId = adminAreaId;
		this.adminAreaName = adminAreaName;
		this.status = status;
		this.image = image;
	}

	/**
	 * Gets the admin area id.
	 *
	 * @return the adminAreaId
	 */
	public final String getAdminAreaId() {
		return adminAreaId;
	}

	/**
	 * Sets the admin area id.
	 *
	 * @param adminAreaId the adminAreaId to set
	 */
	public final void setAdminAreaId(final String adminAreaId) {
		this.adminAreaId = adminAreaId;
	}

	/**
	 * @return the status
	 */
	public boolean isStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(boolean status) {
		this.status = status;
	}

	/**
	 * Gets the admin area name.
	 *
	 * @return the adminAreaName
	 */
	public final String getAdminAreaName() {
		return adminAreaName;
	}

	/**
	 * Sets the admin area name.
	 *
	 * @param adminAreaName the adminAreaName to set
	 */
	public final void setAdminAreaName(final String adminAreaName) {
		this.adminAreaName = adminAreaName;
	}

	/**
	 * Gets the image.
	 *
	 * @return the image
	 */
	public final Image getImage() {
		return image;
	}

	/**
	 * Sets the image.
	 *
	 * @param image the image to set
	 */
	public final void setImage(final Image image) {
		this.image = image;
	}

	
}
