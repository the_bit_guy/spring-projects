/*
 * 
 */
package com.magna.xmsystem.xmadmin.ui.userreviewdialogs;

import java.util.List;

import org.eclipse.jface.viewers.ITreeContentProvider;

import com.magna.xmbackend.vo.ldap.LdapUser;

// TODO: Auto-generated Javadoc
/**
 * The Class UserReviewViewerContentProvider.
 */
public class UserReviewViewerContentProvider implements ITreeContentProvider {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.viewers.ITreeContentProvider#getChildren(java.lang.
	 * Object)
	 */
	@Override
	public Object[] getChildren(final Object parentElement) {
		return new Object[] {};
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.viewers.ITreeContentProvider#getElements(java.lang.
	 * Object)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Object[] getElements(final Object inputElement) {
		/*if (inputElement instanceof LdapUsers) {
			return ((LdapUsers) inputElement).getLdapUsers().toArray();
		}else if (inputElement instanceof LdapUserProperties) {
			return ((LdapUserProperties) inputElement).getLdapUserProperties().toArray();
		}*/
		//return new Object[] {};
		
		return ((List<LdapUser>) inputElement).toArray();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.ITreeContentProvider#getParent(java.lang.
	 * Object)
	 */
	@Override
	public Object getParent(final Object arg0) {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.viewers.ITreeContentProvider#hasChildren(java.lang.
	 * Object)
	 */
	@Override
	public boolean hasChildren(final Object element) {
		return getChildren(element).length > 0;
	}

}

