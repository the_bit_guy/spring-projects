package com.magna.xmsystem.xmadmin.ui.parts;

/**
 * The Interface IEditablePart.
 * 
 * @author shashwat.anand
 */
public interface IEditablePart {

	/**
	 * Checks if is dirty.
	 *
	 * @return true, if is dirty
	 */
	boolean isDirty();

	void save();

	void setDirty(boolean dirty);
	
	void discard();
}
