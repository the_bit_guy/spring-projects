package com.magna.xmsystem.xmadmin.ui.welcomepage;

import java.util.ArrayList;
import java.util.Collections;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.eclipse.e4.core.services.nls.Translation;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.jface.viewers.IBaseLabelProvider;
import org.eclipse.jface.viewers.IContentProvider;
import org.eclipse.jface.viewers.ITreeSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;

import com.magna.xmsystem.dependencies.utils.XMSystemUtil;
import com.magna.xmsystem.xmadmin.message.Message;
import com.magna.xmsystem.xmadmin.message.MessageRegistry;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.AdminTreeContentProvider;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.AdminTreeLabelProvider;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.AdminTreeviewer;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.util.XMAdminUtil;

/**
 * Class for Info display page.
 *
 * @author archita.patel
 */
public class InfoDisplayPage extends Composite {

	/** Member variable 'info display label' for {@link Label}. */
	private Label infoDisplayLabel;

	/** Member variable 'main compsite' for {@link Composite}. */
	private Composite mainCompsite;

	/** Member variable for {@link MessageRegistry}. */
	@Inject
	private MessageRegistry registry;

	/** Member variable for messages. */
	@Inject
	@Translation
	private Message messages;

	/**
	 * Constructor for InfoDisplayPage Class.
	 *
	 * @param parent
	 *            {@link Composite}
	 * @param registry
	 *            {@link Message}
	 * @param style
	 *            {@link int}
	 */
	@Inject
	public InfoDisplayPage(Composite parent) {
		super(parent, SWT.NONE);
	}

	/**
	 * Method for Inits the gui.
	 */
	@PostConstruct
	private void initGui() {

		GridLayoutFactory.fillDefaults().applyTo(this);
		this.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		mainCompsite = new Composite(this, SWT.BORDER);
		mainCompsite.setLayout(new GridLayout(1, false));
		mainCompsite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		mainCompsite.setBackground(Display.getDefault().getSystemColor(SWT.COLOR_WHITE));
		updateContent();
	}

	/**
	 * Method for Update content.
	 */
	public void updateContent() {
		if (this.infoDisplayLabel != null && !this.infoDisplayLabel.isDisposed()) {
			this.infoDisplayLabel.dispose();
		}
		this.infoDisplayLabel = new Label(mainCompsite, SWT.WRAP);
		GridDataFactory.fillDefaults().grab(true, true).align(SWT.CENTER, SWT.CENTER).applyTo(infoDisplayLabel);
		registerMessages(registry);
	}

	/**
	 * Method for Update label.
	 *
	 * @param object
	 *            {@link Object}
	 */
	private void updateLabel(final Object object, final String text) {
		if (object == null) {
			this.infoDisplayLabel.setData("org.eclipse.e4.ui.css.CssClassName", "WelcomeTextLabel");
			this.infoDisplayLabel.setText(text);

		} else {
			final StringBuilder infoSb = new StringBuilder();
			final ArrayList<String> parentObjNames = new ArrayList<>();
			final AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
			final IBaseLabelProvider labelProvider = adminTree.getLabelProvider();
			if (labelProvider instanceof AdminTreeLabelProvider) {
				for (IAdminTreeChild parent = (IAdminTreeChild) object; parent != null; parent = parent.getParent()) {
					final String name = ((AdminTreeLabelProvider) labelProvider).getText(parent);
					if (!XMSystemUtil.isEmpty(name)) {
						parentObjNames.add(name);
					}
				}
			}
			final StringBuilder parentInfoSb = new StringBuilder();
			Collections.reverse(parentObjNames);
			for (String name : parentObjNames) {
				parentInfoSb.append(name).append(" > ");
			}
			if (parentInfoSb.length() > 2) {
				infoSb.append(parentInfoSb.substring(0, parentInfoSb.length() - 2)).append("\n\n\n");
			}
			final ArrayList<String> childObjNames = new ArrayList<>();
			final IContentProvider contentProvider = adminTree.getContentProvider();
			if (contentProvider instanceof AdminTreeContentProvider) {
				final Object[] children = ((AdminTreeContentProvider) contentProvider).getChildren(object);
				for (Object childObj : children) {
					if (labelProvider instanceof AdminTreeLabelProvider) {
						final String name = ((AdminTreeLabelProvider) labelProvider).getText(childObj);
						if (!XMSystemUtil.isEmpty(name)) {
							childObjNames.add(name);
						}
					}
				}
			}

			final StringBuilder childInfoSb = new StringBuilder();
			if (!childObjNames.isEmpty()) {
				childInfoSb.append(text).append(" ");
				for (String name : childObjNames) {
					childInfoSb.append(name).append(", ");
				}
			}

			if (childInfoSb.length() > 2) {
				infoSb.append(childInfoSb.substring(0, childInfoSb.length() - 2));
			}

			if (infoSb.length() > 0) {
				this.infoDisplayLabel.setData("org.eclipse.e4.ui.css.CssClassName", "InfoDefaultTextLabel");
				this.infoDisplayLabel.setText(infoSb.toString());
			}
		}
		this.infoDisplayLabel.requestLayout();
		this.infoDisplayLabel.update();
		this.infoDisplayLabel.redraw();
		this.infoDisplayLabel.getParent().requestLayout();
		this.infoDisplayLabel.getParent().update();
		this.infoDisplayLabel.getParent().redraw();
		this.requestLayout();
		this.update();
		this.redraw();
	}

	/**
	 * Register messages.
	 *
	 * @param registry
	 *            the registry
	 */
	public void registerMessages(final MessageRegistry registry) {
		registry.register((text) -> {
			if (infoDisplayLabel != null && !infoDisplayLabel.isDisposed()) {
				XMAdminUtil instance;
				if (!XMSystemUtil.isEmpty(text)) {
					final String[] split = text.split(":");
					if ((instance = XMAdminUtil.getInstance()) != null) {
						final AdminTreeviewer adminTree = instance.getAdminTree();
						final ITreeSelection structuredSelection = adminTree.getStructuredSelection();
						final Object firstElement = structuredSelection.getFirstElement();
						if (firstElement == null) {
							updateLabel(null, split[0]);
							return;
						}
						updateLabel(firstElement, split[1]);
						return;
					}
				}
			}
		}, (message) -> {
			return message.welcomePageMessage + ":" + message.infoDisplayMsg;
		});
	}
}
