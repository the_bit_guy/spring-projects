package com.magna.xmsystem.xmadmin.ui.parts.ldapconfig;

import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.ldapconfig.LdapConfiguration;

// TODO: Auto-generated Javadoc
/**
 * The Class LdapConfigCompositeUI.
 * 
 * @author archita.patel
 */
public class LdapConfigCompositeUI extends Composite {

	/** Logger instance. */
	private static final Logger LOGGER = LoggerFactory.getLogger(LdapConfigCompositeUI.class);

	/** The grp Ldap config. */
	protected Group grpLdapConfig;

	/** The lbl Ldap url. */
	protected Label lblLdapUrl;

	/** The lbl Ldap port. */
	protected Label lblLdapPort;

	/** The txt Ldap port. */
	protected Text txtLdapPort;

	/** The txt Ldap url. */
	protected Text txtLdapUrl;

	/** The btn save. */
	protected Button btnSave;

	/** The btn cancel. */
	protected Button btnCancel;

	/** The lbl ldap user name. */
	protected Label lblLdapUserName;

	/** The txt ldap user name. */
	protected Text txtLdapUserName;

	/** The lbl ldap password. */
	protected Label lblLdapPassword;

	/** The txt ldap password. */
	protected Text txtLdapPassword;

	/** The lbl ldap base. */
	protected Label lblLdapBase;

	/** The txt ldap base. */
	protected Text txtLdapBase;

	/** The lbl msg config. */
	protected Label lblMsgConfig;

	/**
	 * Instantiates a new ldap config composite UI.
	 *
	 * @param parent
	 *            the parent
	 * @param style
	 *            the style
	 */
	public LdapConfigCompositeUI(final Composite parent, final int style) {
		super(parent, style);
		this.initGUI();
	}

	/**
	 * Inits the GUI.
	 */
	private void initGUI() {

		try {
			GridLayoutFactory.fillDefaults().applyTo(this);
			this.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

			this.grpLdapConfig = new Group(this, SWT.NONE);
			GridLayoutFactory.fillDefaults().numColumns(1).applyTo(this.grpLdapConfig);
			GridDataFactory.fillDefaults().grab(true, true).span(SWT.FILL, SWT.FILL).applyTo(this.grpLdapConfig);

			final Composite widgetContainer = new Composite(this.grpLdapConfig, SWT.NONE);
			final GridLayout widgetContLayout = new GridLayout(2, false);
			widgetContLayout.horizontalSpacing = 10;
			widgetContainer.setLayout(widgetContLayout);
			widgetContainer.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
			
			this.lblLdapUserName = new Label(widgetContainer, SWT.NONE);
			this.txtLdapUserName = new Text(widgetContainer, SWT.BORDER);
			this.txtLdapUserName.setTextLimit(LdapConfiguration.NAME_LIMIT);
			GridDataFactory.fillDefaults().grab(true, false).span(1, 1).align(SWT.FILL, SWT.CENTER)
					.applyTo(this.txtLdapUserName);
			
			this.lblLdapPassword= new Label(widgetContainer, SWT.NONE);
			this.txtLdapPassword = new Text(widgetContainer, SWT.PASSWORD|SWT.BORDER);
			GridDataFactory.fillDefaults().grab(true, false).span(1, 1).align(SWT.FILL, SWT.CENTER)
					.applyTo(this.txtLdapPassword);

			this.lblLdapUrl = new Label(widgetContainer, SWT.NONE);
			this.txtLdapUrl = new Text(widgetContainer, SWT.BORDER);

			GridDataFactory.fillDefaults().grab(true, false).span(1, 1).align(SWT.FILL, SWT.CENTER)
					.applyTo(this.txtLdapUrl);

			this.lblLdapPort = new Label(widgetContainer, SWT.NONE);
			this.txtLdapPort = new Text(widgetContainer, SWT.BORDER);
			this.txtLdapPort.setTextLimit(LdapConfiguration.PORT_LIMIT);
			GridDataFactory.fillDefaults().grab(true, false).span(1, 1).align(SWT.FILL, SWT.CENTER)
					.applyTo(this.txtLdapPort);
			
			this.lblLdapBase = new Label(widgetContainer, SWT.NONE);
			this.txtLdapBase = new Text(widgetContainer, SWT.BORDER);
			GridDataFactory.fillDefaults().grab(true, false).span(1, 1).align(SWT.FILL, SWT.CENTER)
					.applyTo(this.txtLdapBase);
			
			this.lblMsgConfig = new Label(widgetContainer, SWT.NONE);
			GridDataFactory.fillDefaults().grab(true, false).span(2, 1).align(SWT.FILL, SWT.CENTER)
			.applyTo(this.lblMsgConfig);

			final Composite buttonBarComp = new Composite(widgetContainer, SWT.NONE);
			final GridLayout btnBarCompLayout = new GridLayout(2, true);
			btnBarCompLayout.marginRight = 0;
			btnBarCompLayout.marginLeft = 0;
			btnBarCompLayout.marginTop = 0;
			btnBarCompLayout.marginBottom = 0;
			btnBarCompLayout.marginWidth = 0;
			buttonBarComp.setLayout(btnBarCompLayout);
			buttonBarComp.setLayoutData(new GridData(SWT.FILL, SWT.DEFAULT, true, false, 3, 1));
			createButtonBar(buttonBarComp);
		} catch (Exception e) {
			LOGGER.error("Unable to create UI elements", e);
		}
	}

	/**
	 * Creates the button bar.
	 *
	 * @param buttonBarComp
	 *            the button bar comp
	 */
	private void createButtonBar(Composite buttonBarComp) {
		this.btnSave = new Button(buttonBarComp, SWT.NONE);
		this.btnSave.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, true, false));

		this.btnCancel = new Button(buttonBarComp, SWT.NONE);
		this.btnCancel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false));
	}
}
