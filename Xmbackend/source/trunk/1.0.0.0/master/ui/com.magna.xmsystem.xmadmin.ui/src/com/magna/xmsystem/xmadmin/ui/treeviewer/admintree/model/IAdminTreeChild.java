package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model;

/**
 * Marker interface for children of Admin Tree
 * 
 * @author shashwat.anand
 */
public interface IAdminTreeChild {

	/**
	 * Gets the parent.
	 *
	 * @return the parent
	 */
	IAdminTreeChild getParent();

	
	/**
	 * Sets the parent.
	 *
	 * @param parent the new parent
	 */
	void setParent(IAdminTreeChild parent);
}
