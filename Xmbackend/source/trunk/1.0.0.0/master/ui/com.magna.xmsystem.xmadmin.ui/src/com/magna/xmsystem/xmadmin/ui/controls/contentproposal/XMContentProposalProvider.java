package com.magna.xmsystem.xmadmin.ui.controls.contentproposal;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.jface.fieldassist.ContentProposal;
import org.eclipse.jface.fieldassist.IContentProposal;
import org.eclipse.jface.fieldassist.IContentProposalProvider;
import org.eclipse.jface.viewers.ITreeSelection;

import com.magna.xmsystem.dependencies.utils.XMSystemUtil;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.baseapplicationmodel.BaseApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.projectapplicationmodel.ProjectApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.userapplicationmodel.UserApplication;
import com.magna.xmsystem.xmadmin.util.CommonConstants;
import com.magna.xmsystem.xmadmin.util.XMAdminUtil;

/**
 * Class for XM content proposal provider.
 *
 * @author Chiranjeevi.Akula
 */
public class XMContentProposalProvider implements IContentProposalProvider {

	/**
	 * Member variable 'content proposals array' for {@link IContentProposal[]}.
	 */
	private IContentProposal contentProposalsArray[];

	/** Member variable 'filter proposals' for {@link Boolean}. */
	private boolean filterProposals;

	/** Member variable 'proposals map' for {@link Map<String,String>}. */
	private Map<String, String> proposalsMap;

	/**
	 * Constructor for XMContentProposalProvider Class.
	 *
	 * @param proposalsList
	 *            {@link List<IAdminTreeChild>}
	 * @param isPosition
	 *            {@link boolean}
	 */
	public XMContentProposalProvider(List<IAdminTreeChild> proposalsList, boolean isPosition) {
		this.filterProposals = false;
		this.proposalsMap = getProposalsMap(proposalsList, isPosition);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.fieldassist.IContentProposalProvider#getProposals(java.
	 * lang.String, int)
	 */
	@Override
	public IContentProposal[] getProposals(String contents, int position) {
		if (this.filterProposals) {
			List<IContentProposal> iContentProposalList = new ArrayList<IContentProposal>();
			Set<String> proposalValues = this.proposalsMap.keySet();
			for (String proposalName : proposalValues) {
				if (!XMSystemUtil.isEmpty(proposalName) && proposalName.length() >= contents.length()
						&& proposalName.substring(0, contents.length()).equalsIgnoreCase(contents)) {
					iContentProposalList.add(new ContentProposal(proposalName));
				}
			}
			return (IContentProposal[]) iContentProposalList.toArray(new IContentProposal[iContentProposalList.size()]);
		}
		if (this.contentProposalsArray == null) {
			List<IContentProposal> iContentProposalList = new ArrayList<IContentProposal>();
			Set<String> proposalValues = this.proposalsMap.keySet();
			for (String proposalName : proposalValues) {
				if (!XMSystemUtil.isEmpty(proposalName)) {
					iContentProposalList.add(new ContentProposal(proposalName));
				}
			}
			this.contentProposalsArray = (IContentProposal[]) iContentProposalList
					.toArray(new IContentProposal[iContentProposalList.size()]);
		}
		return this.contentProposalsArray;
	}

	/**
	 * Gets the proposals map.
	 *
	 * @param proposalsList
	 *            {@link List<IAdminTreeChild>}
	 * @param isPosition
	 *            {@link boolean}
	 * @return the proposals map
	 */
	private Map<String, String> getProposalsMap(List<IAdminTreeChild> proposalsList, boolean isPosition) {
		Map<String, String> proposalsIdNameMap = new HashMap<>();

		if (isPosition) {
			for (String positionType : CommonConstants.Application.POSITIONS) {
				proposalsIdNameMap.put(positionType, positionType);
			}
		}
		XMAdminUtil xmAdminUtilInstance = XMAdminUtil.getInstance();

		if (proposalsList != null && !proposalsList.isEmpty()) {
			for (IAdminTreeChild adminTreeChild : proposalsList) {
				String id = null;
				String name = null;
				if (adminTreeChild instanceof BaseApplication) {
					id = ((BaseApplication) adminTreeChild).getBaseApplicationId();
					name = ((BaseApplication) adminTreeChild).getName();
				} else if (adminTreeChild instanceof UserApplication) {
					if (((UserApplication) adminTreeChild).isParent()) {
						boolean isSelectedObj = false;
						ITreeSelection structuredSelection = xmAdminUtilInstance.getAdminTree()
								.getStructuredSelection();
						if (!structuredSelection.isEmpty()) {
							if (adminTreeChild.equals(structuredSelection.getFirstElement())) {
								isSelectedObj = true;
							}
						}
						if (!isSelectedObj) {
							id = ((UserApplication) adminTreeChild).getUserApplicationId();
							name = ((UserApplication) adminTreeChild).getName();
						}
					}
				} else if (adminTreeChild instanceof ProjectApplication) {
					if (((ProjectApplication) adminTreeChild).isParent()) {
						boolean isSelectedObj = false;
						ITreeSelection structuredSelection = xmAdminUtilInstance.getAdminTree()
								.getStructuredSelection();
						if (!structuredSelection.isEmpty()) {
							if (adminTreeChild.equals(structuredSelection.getFirstElement())) {
								isSelectedObj = true;
							}
						}
						if (!isSelectedObj) {
							id = ((ProjectApplication) adminTreeChild).getProjectApplicationId();
							name = ((ProjectApplication) adminTreeChild).getName();
						}
					}
				}
				if (!XMSystemUtil.isEmpty(id) && !XMSystemUtil.isEmpty(name)) {
					proposalsIdNameMap.put(name, id);
				}
			}
		}
		return proposalsIdNameMap;
	}

	/**
	 * Sets the filtering.
	 *
	 * @param filterProposals
	 *            the new filtering
	 */
	public void setFiltering(boolean filterProposals) {
		this.filterProposals = filterProposals;
		this.contentProposalsArray = null;
	}

	/**
	 * Gets the proposals map.
	 *
	 * @return the proposals map
	 */
	public Map<String, String> getProposalsMap() {
		return proposalsMap;
	}
}