package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.adminconf;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;

/**
 * The Class AdminUserApps.
 * 
 * @author shashwat.anand
 */
public class AdminUserApps implements IAdminTreeChild {
	/** The parent. */
	private IAdminTreeChild parent;
	
	/** The admin user app child. */
	final private Map<String, IAdminTreeChild> adminUserAppChild;

	/**
	 * Instantiates a new admin user apps.
	 *
	 * @param parent the parent
	 */
	public AdminUserApps(IAdminTreeChild parent) {
		this.parent = parent;
		this.adminUserAppChild = new LinkedHashMap<>();
	}
	
	/**
	 * Adds the.
	 *
	 * @param adminUserAppChildId the admin user app child id
	 * @param child the child
	 * @return the i admin tree child
	 */
	public IAdminTreeChild add(final String adminUserAppChildId, final IAdminTreeChild child) {
		child.setParent(this);
		return this.adminUserAppChild.put(adminUserAppChildId, child);
	}
	
	/**
	 * Removes the.
	 *
	 * @param adminUserAppChildId the admin user app child id
	 * @return the i admin tree child
	 */
	public IAdminTreeChild remove(final String adminUserAppChildId) {
		return this.adminUserAppChild.remove(adminUserAppChildId);
	}
	
	/**
	 * Gets the admin user app children.
	 *
	 * @return the admin user app children
	 */
	public Map<String, IAdminTreeChild> getAdminUserAppChildren() {
		return adminUserAppChild;
	}

	/**
	 * Gets the admin user app collection.
	 *
	 * @return the admin user app collection
	 */
	public Collection<IAdminTreeChild> getAdminUserAppCollection() {
		return this.adminUserAppChild.values();
	}

	/* (non-Javadoc)
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#getParent()
	 */
	@Override
	public IAdminTreeChild getParent() {
		return this.parent;
	}

	/* (non-Javadoc)
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#setParent(com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild)
	 */
	@Override
	public void setParent(IAdminTreeChild parent) {
		this.parent = parent;
	}
}
