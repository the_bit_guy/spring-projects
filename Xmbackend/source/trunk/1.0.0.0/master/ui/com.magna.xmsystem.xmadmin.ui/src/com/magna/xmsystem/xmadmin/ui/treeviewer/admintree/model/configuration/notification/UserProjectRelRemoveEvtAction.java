package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification;

import java.beans.PropertyChangeEvent;
import java.util.Set;
import java.util.TreeSet;

import com.magna.xmsystem.dependencies.utils.XMSystemUtil;
import com.magna.xmsystem.xmadmin.util.CommonConstants;

/**
 * The Class UserProjectRelRemoveEvtAction.
 */
public class UserProjectRelRemoveEvtAction extends NotificationEvtActions {

	/**
	 * Instantiates a new user project rel remove evt action.
	 *
	 * @param id the id
	 * @param name the name
	 * @param isActive the is active
	 * @param operationMode the operation mode
	 */
	public UserProjectRelRemoveEvtAction(final String id, final String name, final boolean isActive,
			final int operationMode) {
		super(id, name, isActive, operationMode);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.BeanModel#
	 * propertyChange(java.beans.PropertyChangeEvent)
	 */
	@Override
	public void propertyChange(PropertyChangeEvent event) {
		this.propertyChangeSupport.firePropertyChange(event.getPropertyName(), event.getOldValue(),
				event.getNewValue());
	}


	/**
	 * Deep copy pro rel remove evt action.
	 *
	 * @param update the update
	 * @param updateThisObject the update this object
	 * @return the user project rel remove evt action
	 */
	public UserProjectRelRemoveEvtAction deepCopyProRelRemoveEvtAction(boolean update,UserProjectRelRemoveEvtAction updateThisObject) {
		UserProjectRelRemoveEvtAction clonedUserProjectRelRemoveEvtAction = null;

		String currentId = XMSystemUtil.isEmpty(this.getId()) ? CommonConstants.EMPTY_STR : this.getId();
		String currentName = XMSystemUtil.isEmpty(this.getName()) ? CommonConstants.EMPTY_STR : this.getName();
		boolean currentIsActive = this.isActive();
		String currentDescription = XMSystemUtil.isEmpty(this.getDescription()) ? CommonConstants.EMPTY_STR
				: this.getDescription();
		Set<String> currentToUserList = this.getToUsers();
		Set<String> currentCCUserList = this.getCcUsers();

		NotificationTemplate currentTemplate = new NotificationTemplate();
		currentTemplate.setTemplateId(this.getTemplate().getTemplateId());
		currentTemplate.setName(this.getTemplate().getName());
		currentTemplate.setSubject(this.getTemplate().getSubject());
		currentTemplate.setMessage(this.getTemplate().getMessage());
		TreeSet<String> currentVariables;
		if (this.getTemplate().getVariables() != null) {
			currentVariables = new TreeSet<>(this.getTemplate().getVariables());
		} else {
			currentVariables = new TreeSet<>();
		}
		currentTemplate.setVariables(currentVariables);

		if (update) {
			clonedUserProjectRelRemoveEvtAction = updateThisObject;
		} else {
			clonedUserProjectRelRemoveEvtAction = new UserProjectRelRemoveEvtAction(currentId, currentName,
					currentIsActive, CommonConstants.OPERATIONMODE.VIEW);
		}
		clonedUserProjectRelRemoveEvtAction.setId(currentId);
		clonedUserProjectRelRemoveEvtAction.setName(currentName);
		clonedUserProjectRelRemoveEvtAction.setActive(currentIsActive);
		clonedUserProjectRelRemoveEvtAction.setDescription(currentDescription);
		clonedUserProjectRelRemoveEvtAction.setToUsers(currentToUserList);
		clonedUserProjectRelRemoveEvtAction.setCcUsers(currentCCUserList);
		clonedUserProjectRelRemoveEvtAction.setTemplate(currentTemplate);

		return clonedUserProjectRelRemoveEvtAction;
	}

}
