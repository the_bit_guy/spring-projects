package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users;

import java.beans.PropertyChangeEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.core.runtime.Assert;

import com.magna.xmsystem.dependencies.utils.LANG_ENUM;
import com.magna.xmsystem.dependencies.utils.XMSystemUtil;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.BeanModel;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.projectapplicationmodel.ProjectAppUsers;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.startapplicationmodel.StartAppUsers;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.userapplicationmodel.UserAppUsers;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.adminconf.AdminUsers;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.icon.Icon;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.RoleScopeObjectUsers;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.RoleUsers;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.directory.DirectoryUsers;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.usergroup.UserGroupUsers;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectUserChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.refs.RelationObj;
import com.magna.xmsystem.xmadmin.util.CommonConstants;
/**
 * Model class for User.
 *
 * @author archita.patel
 */
public class User extends BeanModel implements IAdminTreeChild {

	/** Constant variable for remark text limit. */
	public static final int REMARK_LIMIT = 1500;

	/** Constant variable for desc text limit. */
	public static final int DESC_LIMIT = 240;

	/** Constant variable for name text limit. */
	public static final int NAME_LIMIT = 30;

	/** PROPERTY_USERID constant. */
	public static final String PROPERTY_USERID = "userId"; //$NON-NLS-1$

	/** PROPERTY_USERNAME constant. */
	public static final String PROPERTY_USERNAME = "name"; //$NON-NLS-1$

	/** The Constant PROPERTY_FULLNAME. */
	public static final String PROPERTY_FULLNAME = "fullName"; //$NON-NLS-1$

	/** PROPERTY_ACTIVE constant. */
	public static final String PROPERTY_ACTIVE = "active"; //$NON-NLS-1$

	/** PROPERTY_DESC_MAP constant. */
	public static final String PROPERTY_DESC_MAP = "descriptionMap"; //$NON-NLS-1$

	/** PROPERTY_REMARKS_MAP constant. */
	public static final String PROPERTY_REMARKS_MAP = "remarksMap"; //$NON-NLS-1$

	/** PROPERTY_OPERATION_MODE constant. */
	public static final String PROPERTY_OPERATION_MODE = "operationMode"; //$NON-NLS-1$

	/** PROPERTY_ICON constant. */
	public static final String PROPERTY_ICON = "icon"; //$NON-NLS-1$

	/** The Constant PROPERTY_EMAIL. */
	public static final String PROPERTY_EMAIL = "email";

	/** The Constant PROPERTY_TELEPHONENUM. */
	public static final String PROPERTY_TELEPHONENUM = "telePhoneNum";

	/** The Constant PROPERTY_DEPARTMENT. */
	public static final String PROPERTY_DEPARTMENT = "department";

	/** The Constant PROPERTY_MANAGER. */
	public static final String PROPERTY_MANAGER = "manager";

	/** Member variable for user id. */
	private String userId;

	/** Member variable for user name. */
	private String name;

	/** The full name. */
	private String fullName;
	
	/** The manager. */
	private String manager;

	/** The email. */
	private String email;

	/** The tele phone num. */
	private String telePhoneNum;

	/** The department. */
	private String department;

	/** Member variable to store is user is active. */
	private boolean active;

	/** Member variable for user description. */
	private Map<LANG_ENUM, String> descriptionMap;

	/** Member variable for user remarksMap. */
	private Map<LANG_ENUM, String> remarksMap;

	/** Member variable for mode of operation. */
	private int operationMode;

	/** Member variable for icon. */
	private Icon icon;

	/** The user children. */
	private Map<String, IAdminTreeChild> userChildren;

	/** The rel ids. */
	private List<String> relIds;

	/**
	 * Instantiates a new user.
	 *
	 * @param userId the user id
	 * @param name the name
	 * @param fullName the full name
	 * @param manager the manager
	 * @param isActive the is active
	 * @param email the email
	 * @param telePhoneNum the tele phone num
	 * @param department the department
	 * @param icon the icon
	 * @param operationMode the operation mode
	 */
	public User(final String userId, final String name, final String fullName, final String manager, final boolean isActive,
			final String email, final String telePhoneNum, final String department, final Icon icon,
			final int operationMode) {
		this(userId, name, fullName, manager, isActive, email, telePhoneNum, department, new HashMap<>(), new HashMap<>(), icon,
				operationMode);
	}

	/**
	 * Instantiates a new user.
	 *
	 * @param userId the user id
	 * @param name the name
	 * @param fullName the full name
	 * @param manager the manager
	 * @param isActive the is active
	 * @param email the email
	 * @param telePhoneNum the tele phone num
	 * @param department the department
	 * @param descriptionMap the description map
	 * @param remarksMap the remarks map
	 * @param icon the icon
	 * @param operationMode the operation mode
	 */
	public User(final String userId, final String name, final String fullName, final String manager, final boolean isActive,
			final String email, final String telePhoneNum, final String department,
			final Map<LANG_ENUM, String> descriptionMap, final Map<LANG_ENUM, String> remarksMap, final Icon icon,
			final int operationMode) {
		super();
		this.userId = userId;
		this.name = name;
		this.fullName = fullName;
		this.manager = manager;
		this.active = isActive;
		this.email = email;
		this.telePhoneNum = telePhoneNum;
		this.department = department;
		this.descriptionMap = descriptionMap;
		this.remarksMap = remarksMap;
		this.icon = icon;
		this.operationMode = operationMode;
		this.userChildren = new LinkedHashMap<>();

		this.relIds = new ArrayList<>();
		addFixedChildren();
	}

	/**
	 * Method for Adds the fixed children.
	 */
	public void addFixedChildren() {
		this.userChildren.put(UserProjects.class.getName(), new UserProjects(this));
		this.userChildren.put(UserAdminAreas.class.getName(), new UserAdminAreas(this));
		// this.userChildren.put(UserUserApplications.class.getName(), new
		// UserUserApplications(this));
		this.userChildren.put(UserStartApplications.class.getName(), new UserStartApplications(this));
		// this.userChildren.put(UserGroups.class.getName(), new
		// UserGroups(this));
		// this.userChildren.put(UserInformations.class.getName(), new
		// UserInformations(this));
		// this.userChildren.put(UserGroupUsers.class.getName(), new
		// UserGroupUsers(this));
	}

	/**
	 * Gets the userId.
	 *
	 * @return the userId
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * Sets the userId.
	 *
	 * @param userID
	 *            the new user id
	 */
	public void setUserId(final String userID) {
		this.propertyChangeSupport.firePropertyChange(PROPERTY_USERID, this.userId, this.userId = userID);
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name
	 *            the name to set
	 */
	public void setName(final String name) {
		if (name == null) {
			throw new IllegalArgumentException();
		}
		this.propertyChangeSupport.firePropertyChange(PROPERTY_USERNAME, this.name, this.name = name.trim());
	}

	/**
	 * Gets the full name.
	 *
	 * @return the full name
	 */
	public String getFullName() {
		return fullName;
	}

	/**
	 * Sets the full name.
	 *
	 * @param fullName
	 *            the new full name
	 */
	public void setFullName(String fullName) {
		if (fullName == null) {
			throw new IllegalArgumentException();
		}
		this.propertyChangeSupport.firePropertyChange(PROPERTY_FULLNAME, this.fullName,
				this.fullName = fullName.trim());
	}
	
	/**
	 * Gets the manager.
	 * @return the manager
	 */
	public String getManager() {
		return manager;
	}

	/**
	 * Sets the manager.
	 * @param manager the new manager
	 */
	public void setManager(final String manager) {
		if (manager == null) {
			throw new IllegalArgumentException();
		}
		this.propertyChangeSupport.firePropertyChange(PROPERTY_MANAGER, this.manager,
				this.manager = manager.trim());
	}

	/**
	 * Checks if is active.
	 *
	 * @return the isActive
	 */
	public boolean isActive() {
		return active;
	}

	/**
	 * Sets the active.
	 *
	 * @param isActive
	 *            the isActive to set
	 */
	public void setActive(final boolean isActive) {
		this.propertyChangeSupport.firePropertyChange(PROPERTY_ACTIVE, this.active, this.active = isActive);
	}

	/**
	 * Gets the description map.
	 *
	 * @return the description
	 */

	public Map<LANG_ENUM, String> getDescriptionMap() {
		return descriptionMap;
	}

	/**
	 * Sets the description map.
	 *
	 * @param descriptionMap
	 *            the description map
	 */
	public void setDescriptionMap(final Map<LANG_ENUM, String> descriptionMap) {
		if (descriptionMap == null) {
			throw new IllegalArgumentException();
		}
		this.propertyChangeSupport.firePropertyChange(PROPERTY_DESC_MAP, this.descriptionMap,
				this.descriptionMap = descriptionMap);
	}

	/**
	 * Gets the description for given LANG_ENUM.
	 *
	 * @param lang
	 *            {@link LANG_ENUM}
	 * @return {@link String}
	 */
	public String getDescription(final LANG_ENUM lang) {
		return this.descriptionMap.get(lang);
	}

	/**
	 * Sets the description for given LANG_ENUM.
	 *
	 * @param lang
	 *            {@link LANG_ENUM}
	 * @param description
	 *            {@link String}
	 */
	public void setDescription(final LANG_ENUM lang, final String description) {
		if (lang == null || description == null) {
			throw new IllegalArgumentException();
		}
		this.propertyChangeSupport.firePropertyChange(PROPERTY_DESC_MAP, this.descriptionMap,
				this.descriptionMap.put(lang, description));
	}

	/**
	 * Gets the remarks map.
	 *
	 * @return the remarksMap
	 */
	public Map<LANG_ENUM, String> getRemarksMap() {
		return remarksMap;
	}

	/**
	 * Method for Sets the remarks map.
	 *
	 * @param remarksMap
	 *            the remarksMap to set
	 */
	public void setRemarksMap(final Map<LANG_ENUM, String> remarksMap) {
		if (remarksMap == null) {
			throw new IllegalArgumentException();
		}
		this.propertyChangeSupport.firePropertyChange(PROPERTY_REMARKS_MAP, this.remarksMap,
				this.remarksMap = remarksMap);
	}

	/**
	 * Gets the remarks for given LANG_ENUM.
	 *
	 * @param lang
	 *            {@link LANG_ENUM}
	 * @return {@link String}
	 */
	public String getRemarks(final LANG_ENUM lang) {
		return this.remarksMap.get(lang);
	}

	/**
	 * Sets the remarks for given LANG_ENUM.
	 *
	 * @param lang
	 *            {@link LANG_ENUM}
	 * @param remarks
	 *            {@link String}
	 */
	public void setRemarks(final LANG_ENUM lang, final String remarks) {
		if (lang == null || remarks == null) {
			throw new IllegalArgumentException();
		}
		this.propertyChangeSupport.firePropertyChange(PROPERTY_REMARKS_MAP, this.remarksMap,
				this.remarksMap.put(lang, remarks));
	}

	/**
	 * Gets the operation mode.
	 *
	 * @return the operation mode
	 */
	public int getOperationMode() {
		return operationMode;
	}

	/**
	 * Sets the operation mode.
	 *
	 * @param operationMode
	 *            the new operation mode
	 */
	public void setOperationMode(final int operationMode) {
		this.propertyChangeSupport.firePropertyChange(PROPERTY_OPERATION_MODE, this.operationMode,
				this.operationMode = operationMode);
	}

	/**
	 * Gets the user children.
	 *
	 * @return the user children
	 */
	public Map<String, IAdminTreeChild> getUserChildren() {
		return userChildren;
	}

	/**
	 * Sets the user children.
	 *
	 * @param userChildren
	 *            the user children
	 */
	public void setUserChildren(Map<String, IAdminTreeChild> userChildren) {
		this.userChildren = userChildren;
	}

	/**
	 * Gets the icon.
	 *
	 * @return the icon
	 */
	public Icon getIcon() {
		return icon;
	}

	/**
	 * Sets the icon.
	 *
	 * @param icon
	 *            the icon to set
	 */
	public void setIcon(final Icon icon) {
		if (icon == null) {
			throw new IllegalArgumentException();
		}
		this.propertyChangeSupport.firePropertyChange(PROPERTY_ICON, this.icon, this.icon = icon);
	}

	/**
	 * Gets the email.
	 *
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Sets the email.
	 *
	 * @param email
	 *            the new email
	 */
	public void setEmail(String email) {
		if (email == null) {
			throw new IllegalArgumentException();
		}
		this.propertyChangeSupport.firePropertyChange(PROPERTY_EMAIL, this.email, this.email = email);
		// this.email=email;
	}

	/**
	 * Gets the tele phone num.
	 *
	 * @return the tele phone num
	 */
	public String getTelePhoneNum() {
		return telePhoneNum;
	}

	/**
	 * Sets the tele phone num.
	 *
	 * @param telePhoneNum
	 *            the new tele phone num
	 */
	public void setTelePhoneNum(String telePhoneNum) {
		if (telePhoneNum == null) {
			throw new IllegalArgumentException();
		}
		this.propertyChangeSupport.firePropertyChange(PROPERTY_TELEPHONENUM, this.telePhoneNum,
				this.telePhoneNum = telePhoneNum);
		// this.telePhoneNum=telePhoneNum;
	}

	/**
	 * Gets the department.
	 *
	 * @return the department
	 */
	public String getDepartment() {
		return department;
	}

	/**
	 * Sets the department.
	 *
	 * @param department
	 *            the new department
	 */
	public void setDepartment(String department) {
		if (department == null) {
			throw new IllegalArgumentException();
		}
		this.propertyChangeSupport.firePropertyChange(PROPERTY_DEPARTMENT, this.department,
				this.department = department.trim());
		// this.department=department;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.BeanModel#
	 * propertyChange(java.beans.PropertyChangeEvent)
	 */
	@Override
	public void propertyChange(PropertyChangeEvent event) {
		this.propertyChangeSupport.firePropertyChange(event.getPropertyName(), event.getOldValue(),
				event.getNewValue());
	}

	/**
	 * Method for Deep copy user.
	 *
	 * @param update
	 *            {@link boolean}
	 * @param updateThisObject
	 *            the update this object
	 * @return the user {@link User}
	 */
	public User deepCopyUser(boolean update, User updateThisObject) {

		User clonedUser = null;
		try {
			String currentUserId = XMSystemUtil.isEmpty(this.getUserId()) ? CommonConstants.EMPTY_STR
					: this.getUserId();
			String currentName = XMSystemUtil.isEmpty(this.getName()) ? CommonConstants.EMPTY_STR : this.getName();
			String currentFullName = XMSystemUtil.isEmpty(this.getFullName()) ? CommonConstants.EMPTY_STR
					: this.getFullName();
			String currentManager = XMSystemUtil.isEmpty(this.getManager()) ? CommonConstants.EMPTY_STR
					: this.getManager();
			boolean currentIsActive = this.isActive();

			Map<LANG_ENUM, String> currentTranslationIdMap = new HashMap<>();
			Map<LANG_ENUM, String> currentDescriptionMap = new HashMap<>();
			Map<LANG_ENUM, String> currentRemarksMap = new HashMap<>();

			for (LANG_ENUM langEnum : LANG_ENUM.values()) {
				String translationId = this.getTranslationId(langEnum);
				currentTranslationIdMap.put(langEnum,
						XMSystemUtil.isEmpty(translationId) ? CommonConstants.EMPTY_STR : translationId);

				String description = this.getDescription(langEnum);
				currentDescriptionMap.put(langEnum,
						XMSystemUtil.isEmpty(description) ? CommonConstants.EMPTY_STR : description);

				String remarks = this.getRemarks(langEnum);
				currentRemarksMap.put(langEnum, XMSystemUtil.isEmpty(remarks) ? CommonConstants.EMPTY_STR : remarks);
			}

			//Icon currentIcon = this.getIcon() == null ? null : this.getIcon();
			Icon currentIcon = new Icon(this.getIcon().getIconId(), this.getIcon().getIconName(),
					this.getIcon().getIconPath(), this.getIcon().getIconType());
			String currentEmail = XMSystemUtil.isEmpty(this.getEmail()) ? CommonConstants.EMPTY_STR : this.getEmail();
			String currentPhoneNum = XMSystemUtil.isEmpty(this.getTelePhoneNum()) ? CommonConstants.EMPTY_STR
					: this.getTelePhoneNum();
			String currentdepartment = XMSystemUtil.isEmpty(this.getDepartment()) ? CommonConstants.EMPTY_STR
					: this.getDepartment();

			if (update) {
				clonedUser = updateThisObject;
			} else {
				clonedUser = new User(currentUserId, currentName, currentFullName, currentManager, currentIsActive, currentEmail,
						currentPhoneNum, currentdepartment, currentIcon, CommonConstants.OPERATIONMODE.VIEW);
			}
			clonedUser.setUserId(currentUserId);
			clonedUser.setName(currentName);
			clonedUser.setFullName(currentFullName);
			clonedUser.setManager(currentManager);
			clonedUser.setActive(currentIsActive);
			clonedUser.setEmail(currentEmail);
			clonedUser.setTelePhoneNum(currentPhoneNum);
			clonedUser.setDepartment(currentdepartment);
			clonedUser.setIcon(currentIcon);
			clonedUser.setTranslationIdMap(currentTranslationIdMap);
			clonedUser.setDescriptionMap(currentDescriptionMap);
			clonedUser.setRemarksMap(currentRemarksMap);
			return clonedUser;
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * Gets the rel ids.
	 *
	 * @return the rel ids
	 */
	public List<String> getRelIds() {
		return relIds;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.BeanModel#
	 * getAdapter(java.lang.Class,
	 * com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild)
	 */
	@Override
	public <T> RelationObj getAdapter(Class<T> adapterType, IAdminTreeChild parent, String relationId,
			boolean relationStatus) {
		Assert.isNotNull(adapterType);
		Assert.isNotNull(this);
		/* String relId = UUID.randomUUID().toString(); */
		this.relIds.add(relationId);
		if (adapterType == AdminUsers.class) {
			AdminUsers AdminUsers = new AdminUsers(parent);
			RelationObj relObj = new RelationObj(relationId, this, AdminUsers, relationStatus, null);
			return relObj;
		} else if (adapterType == ProjectUserChild.class) {
			ProjectUserChild projectUserChild = new ProjectUserChild(parent);
			RelationObj relObj = new RelationObj(relationId, this, projectUserChild, relationStatus, null);
			return relObj;
		} else if (adapterType == UserAppUsers.class) {
			UserAppUsers userAppUsers = new UserAppUsers(parent);
			RelationObj relObj = new RelationObj(relationId, this, userAppUsers, relationStatus, null);
			return relObj;
		} else if (adapterType == ProjectAppUsers.class) {
			ProjectAppUsers projectAppUsers = new ProjectAppUsers(parent);
			RelationObj relObj = new RelationObj(relationId, this, projectAppUsers, relationStatus, null);
			return relObj;
		} else if (adapterType == StartAppUsers.class) {
			StartAppUsers startAppUsers = new StartAppUsers(parent);
			RelationObj relObj = new RelationObj(relationId, this, startAppUsers, relationStatus, null);
			return relObj;
		} else if (adapterType == DirectoryUsers.class) {
			DirectoryUsers directoryUsers = new DirectoryUsers(parent);
			RelationObj relObj = new RelationObj(relationId, this, directoryUsers, relationStatus, null);
			return relObj;
		} else if (adapterType == RoleUsers.class) {
			RoleUsers roleUsers = new RoleUsers(parent);
			RelationObj relObj = new RelationObj(relationId, this, roleUsers, relationStatus, null);
			return relObj;
		} else if (adapterType == RoleScopeObjectUsers.class) {
			RoleScopeObjectUsers roleScopeObjectUsers = new RoleScopeObjectUsers(parent);
			RelationObj relObj = new RelationObj(relationId, this, roleScopeObjectUsers, relationStatus, null);
			return relObj;
		} else if (adapterType == UserGroupUsers.class) {
			UserGroupUsers userGroupUsers = new UserGroupUsers(parent);
			RelationObj relObj = new RelationObj(relationId, this, userGroupUsers, relationStatus, null);
			return relObj;
		}
		return super.getAdapter(adapterType, parent, relationId, relationStatus);

	}
}
