package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;

/**
 * Container model class for {@link AdministrartionAreas}
 * 
 * @author shashwat.anand
 *
 */
public class AdministrationAreas implements IAdminTreeChild {
	/** The parent. */
	private IAdminTreeChild parent;
	/**
	 * List for storing {@link AdministrartionArea}
	 */
	private Map<String, IAdminTreeChild> adminAreasChildren;

	/**
	 * Constructor
	 */
	public AdministrationAreas() {
		this.parent = null;
		this.adminAreasChildren = new LinkedHashMap<>();
	}

	/**
	 * Method to add child 
	 * @param child {@link IAdminTreeChild}
	 * @param adminAreaId {@link String}
	 * @return {@link IAdminTreeChild}
	 */
	public IAdminTreeChild add(final String adminAreaId, final IAdminTreeChild child) {
		child.setParent(this);
		IAdminTreeChild returnVal = this.adminAreasChildren.put(adminAreaId, child);
		sort();
		return returnVal;
	}

	/**
	 * Method to remove child
	 * @param adminAreaId {@link String}
	 * @return {@link IAdminTreeChild}
	 */
	public IAdminTreeChild remove(final String adminAreaId) {
		return this.adminAreasChildren.remove(adminAreaId);
	}

	/**
	 * Method for Removes the all.
	 */
	public void removeAll() {
		this.adminAreasChildren.clear();
	}
	
	/**
	 * @return {@link List} of {@link AdministrartionArea} 
	 */
	public Collection<IAdminTreeChild> getAdministrationAreasCollection() {
		return this.adminAreasChildren.values();
	}

	/**
	 * @return the adminstrationAreasChildren
	 */
	public Map<String, IAdminTreeChild> getAdminstrationAreasChildren() {
		return adminAreasChildren;
	}
	
	/**
	 * Returns null as its parent
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#getParent()
	 */
	@Override
	public IAdminTreeChild getParent() {
		return this.parent;
	}

	/**
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#setParent(com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild)
	 */
	@Override
	public void setParent(final IAdminTreeChild parent) {
		this.parent = parent;
	}
	
	/**
	 * Sort.
	 */
	public void sort() {
		final LinkedHashMap<String, IAdminTreeChild> collect = this.adminAreasChildren.entrySet().stream().sorted(
				(e1, e2) -> ((AdministrationArea) e1.getValue()).getName().compareTo(((AdministrationArea) e2.getValue()).getName()))
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
		this.adminAreasChildren = collect;
	}
}
