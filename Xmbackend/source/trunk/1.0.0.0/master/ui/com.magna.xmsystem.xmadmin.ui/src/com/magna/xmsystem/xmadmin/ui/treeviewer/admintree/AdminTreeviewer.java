package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.e4.core.contexts.ContextInjectionFactory;
import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.core.services.events.IEventBroker;
import org.eclipse.e4.core.services.nls.Translation;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.services.EMenuService;
import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.e4.ui.workbench.modeling.ESelectionService;
import org.eclipse.jface.util.LocalSelectionTransfer;
import org.eclipse.jface.viewers.IBaseLabelProvider;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITreeSelection;
import org.eclipse.jface.viewers.ITreeViewerListener;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TreeExpansionEvent;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StackLayout;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Item;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.swt.widgets.Widget;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magna.xmsystem.ui.controls.widgets.CustomMessageDialog;
import com.magna.xmsystem.xmadmin.message.Message;
import com.magna.xmsystem.xmadmin.message.MessageRegistry;
import com.magna.xmsystem.xmadmin.ui.parts.IEditablePart;
import com.magna.xmsystem.xmadmin.ui.parts.InformationPart;
import com.magna.xmsystem.xmadmin.ui.parts.adminarea.AdministrationAreaCompositeAction;
import com.magna.xmsystem.xmadmin.ui.parts.application.baseapplication.BaseAppCompositeAction;
import com.magna.xmsystem.xmadmin.ui.parts.application.projectapplication.ProjectApplicationCompositeAction;
import com.magna.xmsystem.xmadmin.ui.parts.application.startapplication.StartApplicationCompositeAction;
import com.magna.xmsystem.xmadmin.ui.parts.application.userapplication.UserApplicationCompositeAction;
import com.magna.xmsystem.xmadmin.ui.parts.directory.DirectoryCompositeAction;
import com.magna.xmsystem.xmadmin.ui.parts.dnd.NodeDragListener;
import com.magna.xmsystem.xmadmin.ui.parts.dnd.NodeDropListener;
import com.magna.xmsystem.xmadmin.ui.parts.groups.projectappgroup.ProjectAppGroupCompositeAction;
import com.magna.xmsystem.xmadmin.ui.parts.groups.projectgroup.ProjectGroupCompositeAction;
import com.magna.xmsystem.xmadmin.ui.parts.groups.userappgroup.UserAppGroupCompositeAction;
import com.magna.xmsystem.xmadmin.ui.parts.groups.usergroup.UserGroupCompositeAction;
import com.magna.xmsystem.xmadmin.ui.parts.icons.IconsCompositeAction;
import com.magna.xmsystem.xmadmin.ui.parts.ldapconfig.LdapConfigCompositeAction;
import com.magna.xmsystem.xmadmin.ui.parts.livemsgconfig.LiveMessageConfigAction;
import com.magna.xmsystem.xmadmin.ui.parts.notification.NotificationCompositeAction;
import com.magna.xmsystem.xmadmin.ui.parts.notificationtemplate.TemplateCompositeAction;
import com.magna.xmsystem.xmadmin.ui.parts.notifyaaprojectuser.NotifyAAProjectUserCompositeAction;
import com.magna.xmsystem.xmadmin.ui.parts.objexp.ObjectExpPage;
import com.magna.xmsystem.xmadmin.ui.parts.objexp.ObjectExplorer;
import com.magna.xmsystem.xmadmin.ui.parts.project.ProjectCompositeAction;
import com.magna.xmsystem.xmadmin.ui.parts.projectactivateevt.ProjectActivateEvtCompositeAction;
import com.magna.xmsystem.xmadmin.ui.parts.projectcreateevt.ProjectCreateEvtCompositeAction;
import com.magna.xmsystem.xmadmin.ui.parts.projectdeactivateevt.ProjectDeactivateEvtCompositeAction;
import com.magna.xmsystem.xmadmin.ui.parts.projectdeleteevt.ProjectDeleteEvtCompositeAction;
import com.magna.xmsystem.xmadmin.ui.parts.projectexpiryconfig.ProjectExpiryConfigCompositeAction;
import com.magna.xmsystem.xmadmin.ui.parts.role.RoleCompositeAction;
import com.magna.xmsystem.xmadmin.ui.parts.singletonapptimeconfig.SingletonAppTimeConfigComopsiteAction;
import com.magna.xmsystem.xmadmin.ui.parts.site.SiteCompositeAction;
import com.magna.xmsystem.xmadmin.ui.parts.smtpconfig.SmtpConfigCompositeAction;
import com.magna.xmsystem.xmadmin.ui.parts.unassignedusermessage.NoProjectPopmessageCompositeAction;
import com.magna.xmsystem.xmadmin.ui.parts.user.UserCompositeAction;
import com.magna.xmsystem.xmadmin.ui.parts.userproexpgrace.UserProjectExpGraceCompositeAction;
import com.magna.xmsystem.xmadmin.ui.parts.userprojectrelexpiry.UserProjectExpCompositeAction;
import com.magna.xmsystem.xmadmin.ui.parts.userprorelassignevt.UserProRelAssignEvtCompositeAction;
import com.magna.xmsystem.xmadmin.ui.parts.userprorelremoveevt.UserProRelRemoveEvtCompositeAction;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.AdminTreeFactory;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaInfo;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaInfoHistory;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaInfoStatus;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaProjectAppFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaProjectAppNotFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaProjectAppProtected;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaProjectApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaProjectStartApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaProjects;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaStartApp;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaStartApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaUserAppFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaUserAppNotFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaUserAppProtected;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaUserApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdministrationArea;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdministrationAreas;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.Applications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.baseapplicationmodel.BaseAppInformationHistory;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.baseapplicationmodel.BaseAppInformationModel;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.baseapplicationmodel.BaseAppInformationStatus;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.baseapplicationmodel.BaseAppProjectApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.baseapplicationmodel.BaseAppStartApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.baseapplicationmodel.BaseAppUserApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.baseapplicationmodel.BaseApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.baseapplicationmodel.BaseApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.projectapplicationmodel.ProjectAppAdminAreaProjects;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.projectapplicationmodel.ProjectAppAdminAreas;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.projectapplicationmodel.ProjectAppGroups;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.projectapplicationmodel.ProjectAppUsers;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.projectapplicationmodel.ProjectApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.projectapplicationmodel.ProjectApplicationChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.projectapplicationmodel.ProjectApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.startapplicationmodel.StartAppAdminAreas;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.startapplicationmodel.StartAppProjects;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.startapplicationmodel.StartAppUsers;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.startapplicationmodel.StartApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.startapplicationmodel.StartApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.userapplicationmodel.UserAppAdminAreas;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.userapplicationmodel.UserAppGroups;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.userapplicationmodel.UserAppUsers;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.userapplicationmodel.UserApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.userapplicationmodel.UserApplicationChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.userapplicationmodel.UserApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.Configurations;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.adminconf.AdminMenu;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.adminconf.AdminProjectApps;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.adminconf.AdminUserApps;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.adminconf.AdminUsers;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.icon.Icons;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.ldapconfig.LdapConfiguration;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.livemsgconfig.LiveMessage;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.livemsgconfig.LiveMessages;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.noProjectMessage.NoProjectPopMessage;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.Notification;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.NotificationTemplate;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.NotificationTemplates;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.Notifications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.NotifyAAProjectUserEvt;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.ProjectActivateEvt;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.ProjectActivateEvtAction;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.ProjectCreateEvt;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.ProjectCreateEvtAction;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.ProjectDeactivateEvt;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.ProjectDeactivateEvtAction;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.ProjectDeleteEvt;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.ProjectDeleteEvtAction;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.UserProExpEvt;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.UserProExpGraceEvt;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.UserProjectRelAssignEvt;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.UserProjectRelAssignEvtAction;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.UserProjectRelRemoveEvt;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.UserProjectRelRemoveEvtAction;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.projectexpiryconfig.ProjectExpiryConfig;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.Role;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.RoleScopeObjectUsers;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.RoleScopeObjects;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.RoleUsers;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.Roles;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.singletonapptimeconfig.SingletonAppTimeConfig;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.smtpconfig.SmtpConfiguration;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.directory.Directories;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.directory.Directory;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.directory.DirectoryApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.directory.DirectoryProjectApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.directory.DirectoryProjects;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.directory.DirectoryUserApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.directory.DirectoryUsers;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.Groups;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.projectappgroup.ProjectAppGroupProjectApps;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.projectappgroup.ProjectApplicationGroup;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.projectappgroup.ProjectApplicationGroups;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.projectgroup.ProjectGroupModel;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.projectgroup.ProjectGroupProjects;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.projectgroup.ProjectGroupsModel;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.userappgroup.UserAppGroupUserApps;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.userappgroup.UserApplicationGroup;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.userappgroup.UserApplicationGroups;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.usergroup.UserGroupModel;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.usergroup.UserGroupUsers;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.usergroup.UserGroupsModel;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProUserProjectApp;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.Project;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectAdminAreaProjectAppFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectAdminAreaProjectAppNotFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectAdminAreaProjectAppProtected;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectAdminAreaProjectApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectAdminAreaStartApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectAdminAreas;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectInfoHistory;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectInfoStatus;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectInformation;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectProjectApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectUserAAProjectAppAllowed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectUserAAProjectAppForbidden;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectUserAAProjectApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectUserAdminAreas;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectUsers;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.Projects;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.refs.RelationObj;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.Site;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaInformationHistory;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaInformationStatus;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaInformations;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaProjectApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaProjectChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaProjectStartApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaProjects;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaStartApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaUserAppFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaUserAppNotFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaUserAppProtected;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaUserApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminProjectAppFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminProjectAppNotFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminProjectAppProtected;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdministrationChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdministrations;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.Sites;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.User;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserAAUserAppAllowed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserAAUserAppForbidden;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserAAUserApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserAdminAreas;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserGroups;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserInformationHistory;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserInformationStatus;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserInformations;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserProjectAAProjectAppAllowed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserProjectAAProjectAppForbidden;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserProjectAAProjectApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserProjectAdminAreas;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserProjectChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserProjects;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserStartApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.Users;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UsersNameAlphabet;
import com.magna.xmsystem.xmadmin.ui.welcomepage.InfoDisplayPage;
import com.magna.xmsystem.xmadmin.util.CommonConstants;
import com.magna.xmsystem.xmadmin.util.NavigationStack;
import com.magna.xmsystem.xmadmin.util.XMAdminUtil;

/**
 * The Class AdminTreeviewer.
 * 
 * @author shashwat.anand
 * 
 */
public class AdminTreeviewer extends TreeViewer {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(AdminTreeviewer.class);

	/** The Constant KEY. */
	private static final String KEY = "IS_MESSAGE_REGISTERED";

	/** The event broker. */
	@Inject
	private IEventBroker eventBroker;

	/** The eclipse context. */
	@Inject
	private IEclipseContext eclipseContext;

	/** The registry. */
	@Inject
	private MessageRegistry registry;

	/** The selection service. */
	@Inject
	private ESelectionService selectionService;

	/** The model service. */
	@Inject
	private EModelService modelService;

	/** The application. */
	@Inject
	private MApplication application;

	/** The context menu service. */
	@Inject
	private EMenuService contextMenuService;

	/** The previous selection. */
	private IStructuredSelection previousSelection;

	/** The messages. */
	@Inject
	@Translation
	transient private Message messages;
	
	/** The backward stack. */
	private NavigationStack<Object> backwardStack;
	
	/** The forward stack. */
	private NavigationStack<Object> forwardStack;
	
	/** The selection type. */
	private int selectionType;
	
	/** The child size. */
	private int childObjCount;

	/**
	 * Instantiates a new admin treeviewer.
	 *
	 * @param parent
	 *            the parent
	 */
	@Inject
	public AdminTreeviewer(final Composite parent) {
		super(parent, SWT.FULL_SELECTION | SWT.V_SCROLL | SWT.H_SCROLL | SWT.BORDER);

	}

	/**
	 * Init method will be called after injection of members.
	 */
	@PostConstruct
	private void init() {
		try {
			this.backwardStack = new NavigationStack<>();
			this.forwardStack = new NavigationStack<>();
			this.getTree().setLayoutData(new GridData(GridData.FILL_BOTH));
			final AdminTreeContentProvider adminTreeContentProvider = ContextInjectionFactory
					.make(AdminTreeContentProvider.class, this.eclipseContext);
			this.eclipseContext.set(TreeViewer.class, this);
			final AdminTreeLabelProvider adminTreeLabelProvider = ContextInjectionFactory
					.make(AdminTreeLabelProvider.class, this.eclipseContext);
			setContentProvider(adminTreeContentProvider);
			setLabelProvider(adminTreeLabelProvider);
			setInput(AdminTreeFactory.getInstance().createInitialModel(false));
			setSelection();
			// Context Menu
			this.contextMenuService.registerContextMenu(this.getTree(),
					"com.magna.xmsystem.xmadmin.ui.popupmenu.admintree");
			addDndToAdminTree();
			expandAndCollapseTree();
		} catch (Exception e) {
			LOGGER.info("Inside init of Admin Tree Viewer ! " + e); //$NON-NLS-1$
		}
	}

	/**
	 * Adds the dnd to admin tree.
	 */
	private void addDndToAdminTree() {
		final LocalSelectionTransfer transfer = LocalSelectionTransfer.getTransfer();
		LocalSelectionTransfer[] transferTypes = new LocalSelectionTransfer[] { LocalSelectionTransfer.getTransfer() };
		eclipseContext.set(TreeViewer.class, this);
		this.addDragSupport(DND.DROP_MOVE | DND.DROP_COPY, transferTypes, new NodeDragListener(this, transfer));
		this.addDropSupport(DND.DROP_MOVE | DND.DROP_COPY, transferTypes,
				ContextInjectionFactory.make(NodeDropListener.class, eclipseContext));
	}

	/**
	 * Method for Expand and collapse tree.
	 */
	private void expandAndCollapseTree() {
		this.addTreeListener(new ITreeViewerListener() {

			@Override
			public void treeExpanded(TreeExpansionEvent event) {
				Object element = event.getElement();

				eclipseContext.set(Object.class, element);
				AdminTreeExpansion adminTreeExpansion = ContextInjectionFactory.make(AdminTreeExpansion.class,
						eclipseContext);
				Job job = new Job("Loading...") {

					@Override
					protected IStatus run(IProgressMonitor monitor) {
						//Tree tree = getTree();
						monitor.beginTask("Loading Tree Nodes..", 100);
						monitor.worked(30);
						/*Display.getDefault().syncExec(new Runnable() {
							@Override
							public void run() {
								tree.setRedraw(false);
							}
						});*/
						adminTreeExpansion.loadObjects();
						monitor.worked(70);
						/*// monitor.done();
						Display.getDefault().syncExec(new Runnable() {
							@Override
							public void run() {
								tree.setRedraw(true);
							}
						});*/
						return Status.OK_STATUS;
					}
				};
				job.setUser(true);
				job.schedule();
			}

			@Override
			public void treeCollapsed(TreeExpansionEvent arg0) {
				
			}
		});
	}

	/**
	 * Sets the selection.
	 */
	private void setSelection() {
		this.addSelectionChangedListener(new ISelectionChangedListener() {
			/**
			 * Selection changed method
			 */
			@Override
			public void selectionChanged(final SelectionChangedEvent event) {
				final IStructuredSelection selection = (IStructuredSelection) event.getSelection();
				Object lastElement = null;
				// set the selection to the service
				if (selection != null) {
					final Object firstElement = selection.getFirstElement();
					selectionType = 0;
					int size = backwardStack.size();
					if (size == 0) {
						backwardStack.push(firstElement);
					} else {
						int stackSize = backwardStack.size() - 1;
						lastElement = backwardStack.get(stackSize);

						if (firstElement != null && !firstElement.equals(lastElement)) {
							backwardStack.push(firstElement);
						}
					}
					if (firstElement == null) {
						return;
					}
					if (isPreviousSelectedObjModified()) {
						if (selection.equals(previousSelection)) {
							return;
						}
						boolean response = CustomMessageDialog.openSaveDiscardDialog(
								((AdminTreeviewer) event.getSource()).getTree().getShell(),
								messages.dirtyDialogTitle, messages.dirtyDialogMessage);
						XMAdminUtil instance = XMAdminUtil.getInstance();
						if (response) {
							// Save
							instance.savePreviousPart();
							if (isPreviousSelectedObjModified()) {
								((AdminTreeviewer) event.getSource()).setSelectionToWidget(previousSelection, true);
								return;
							}
						} else {
							// Discard
							instance.discardPreviousPart();
						}
						((AdminTreeviewer) event.getSource()).setSelectionToWidget(selection, true);
					}
					selectionService.setSelection(selection);
					previousSelection = selection;
					updateRightSidePart(firstElement);
					updateStatusBar();
				}
			}
		});
	}

	/**
	 * Checks if is previous selected obj modified.
	 *
	 * @return true, if is previous selected obj modified
	 */
	private boolean isPreviousSelectedObjModified() {
		MPart mPart = (MPart) this.modelService.find(CommonConstants.PART_ID.INFORMATION_PART_ID, application);
		InformationPart rightSidePart = (InformationPart) mPart.getObject();
		if (rightSidePart instanceof IEditablePart) {
			return ((IEditablePart) mPart.getObject()).isDirty();
		}
		return false;
	}
	
	/**
	 * @return the backwardStack
	 */
	public NavigationStack<Object> getBackwardStack() {
		return backwardStack;
	}

	/**
	 * @param backwardStack the backwardStack to set
	 */
	public void setBackwardStack(NavigationStack<Object> backwardStack) {
		this.backwardStack = backwardStack;
	}

	/**
	 * @return the forwardStack
	 */
	public NavigationStack<Object> getForwardStack() {
		return forwardStack;
	}

	/**
	 * @param forwardStack the forwardStack to set
	 */
	public void setForwardStack(NavigationStack<Object> forwardStack) {
		this.forwardStack = forwardStack;
	}

	/**
	 * Update right side part.
	 *
	 * @param firstElement
	 *            the first element
	 */
	private void updateRightSidePart(Object firstElement) {

		MPart mPart = (MPart) this.modelService.find(CommonConstants.PART_ID.INFORMATION_PART_ID, application);
		InformationPart rightSidePart = (InformationPart) mPart.getObject();
		Composite rightcomposite = rightSidePart.getComposite();
		StackLayout rightCompLayout = rightSidePart.getCompLayout();

		ObjectExpPage objectExpPage = rightSidePart.getObjectExpPartUI();
		ObjectExplorer objExpTableViewer = objectExpPage.getObjExpTableViewer();
		if (objectExpPage != null) {
			//displayAllObjectPage.setStructuredSelection(new HashMap<>());
			objExpTableViewer.setSelection(null);
		}
		if (firstElement instanceof Sites || firstElement instanceof AdministrationAreas
				|| firstElement instanceof Users || firstElement instanceof UsersNameAlphabet
				|| firstElement instanceof Projects || firstElement instanceof UserGroupUsers
				|| firstElement instanceof Directories 
				|| firstElement instanceof BaseApplications || firstElement instanceof UserApplications
				|| firstElement instanceof ProjectApplications || firstElement instanceof StartApplications
				|| firstElement instanceof Roles
				|| firstElement instanceof SiteAdministrations|| firstElement instanceof UserGroupsModel || firstElement instanceof ProjectGroupsModel ||  firstElement instanceof ProjectGroupProjects
				|| firstElement instanceof UserApplicationGroups || firstElement instanceof UserAppGroupUserApps|| firstElement instanceof ProjectApplicationGroups || firstElement instanceof ProjectAppGroupProjectApps|| firstElement instanceof SiteAdministrationChild
				|| firstElement instanceof SiteAdminAreaProjectApplications
				|| firstElement instanceof SiteAdminAreaProjectStartApplications
				|| firstElement instanceof SiteAdminAreaInformations || firstElement instanceof SiteAdminAreaProjects
				|| firstElement instanceof SiteAdminAreaStartApplications
				|| firstElement instanceof SiteAdminAreaUserApplications
				|| firstElement instanceof SiteAdminAreaProjectChild || firstElement instanceof SiteAdminProjectAppFixed
				|| firstElement instanceof SiteAdminProjectAppNotFixed
				|| firstElement instanceof SiteAdminProjectAppProtected || firstElement instanceof UserProjects || firstElement instanceof UserProjectAdminAreas
				|| firstElement instanceof UserProjectChild || firstElement instanceof UserProjectAAProjectApplications
				|| firstElement instanceof UserProjectAAProjectAppAllowed || firstElement instanceof UserProjectAAProjectAppForbidden
				|| firstElement instanceof UserAdminAreas || firstElement instanceof UserAAUserAppAllowed || firstElement instanceof UserAAUserAppForbidden
				|| firstElement instanceof UserAAUserApplications || firstElement instanceof UserStartApplications
				|| firstElement instanceof UserGroups || firstElement instanceof UserInformations
				|| firstElement instanceof UserInformationStatus || firstElement instanceof UserInformationHistory
				|| firstElement instanceof SiteAdminAreaUserAppNotFixed
				|| firstElement instanceof SiteAdminAreaUserAppFixed
				|| firstElement instanceof SiteAdminAreaUserAppProtected
				|| firstElement instanceof SiteAdminAreaInformationStatus
				|| firstElement instanceof SiteAdminAreaInformationHistory || firstElement instanceof ProjectAdminAreas
				|| firstElement instanceof ProjectAdminAreaProjectApplications
				|| firstElement instanceof ProjectAdminAreaProjectAppFixed
				|| firstElement instanceof ProjectAdminAreaProjectAppNotFixed
				|| firstElement instanceof ProjectAdminAreaProjectAppProtected
				|| firstElement instanceof ProjectAdminAreaStartApplications || firstElement instanceof ProjectUsers
				|| firstElement instanceof ProjectUserAdminAreas
				|| firstElement instanceof ProjectUserAAProjectApplications
				|| firstElement instanceof ProjectUserAAProjectAppAllowed
				|| firstElement instanceof ProjectUserAAProjectAppForbidden
				|| firstElement instanceof ProjectProjectApplications 
				|| firstElement instanceof ProjectInformation || firstElement instanceof ProjectInfoStatus
				|| firstElement instanceof ProjectInfoHistory || firstElement instanceof ProUserProjectApp
				|| firstElement instanceof BaseAppInformationHistory
				|| firstElement instanceof BaseAppInformationStatus
				|| firstElement instanceof BaseAppProjectApplications
				|| firstElement instanceof BaseAppStartApplications || firstElement instanceof ProjectAppGroups
				|| firstElement instanceof ProjectAppAdminAreas || firstElement instanceof ProjectAppAdminAreaProjects
				|| firstElement instanceof ProjectAppUsers || firstElement instanceof StartAppProjects
				|| firstElement instanceof BaseAppUserApplications || firstElement instanceof BaseAppInformationModel
				|| firstElement instanceof StartAppAdminAreas || firstElement instanceof StartAppUsers
				|| firstElement instanceof UserAppGroups || firstElement instanceof UserAppAdminAreas
				|| firstElement instanceof UserAppUsers || firstElement instanceof AdminAreaUserApplications
				|| firstElement instanceof AdminAreaUserAppFixed || firstElement instanceof AdminAreaUserAppNotFixed
				|| firstElement instanceof AdminAreaStartApp || firstElement instanceof AdminAreaUserAppProtected
				|| firstElement instanceof AdminAreaProjectApplications
				|| firstElement instanceof AdminAreaProjectAppFixed
				|| firstElement instanceof AdminAreaProjectAppNotFixed
				|| firstElement instanceof AdminAreaProjectAppProtected
				|| firstElement instanceof AdminAreaStartApplications
				|| firstElement instanceof AdminAreaProjectStartApplications
				|| firstElement instanceof AdminAreaProjects || firstElement instanceof AdminAreaInfo
				|| firstElement instanceof AdminAreaInfoHistory || firstElement instanceof AdminAreaInfoStatus
				|| firstElement instanceof RoleUsers || firstElement instanceof RoleScopeObjects
				|| firstElement instanceof RoleScopeObjectUsers || firstElement instanceof AdminUsers
				|| firstElement instanceof AdminUserApps || firstElement instanceof AdminProjectApps
				|| firstElement instanceof DirectoryUsers || firstElement instanceof DirectoryProjects
				|| firstElement instanceof DirectoryUserApplications
				|| firstElement instanceof DirectoryProjectApplications || firstElement instanceof LiveMessages
				|| firstElement instanceof ProjectCreateEvt || firstElement instanceof ProjectDeleteEvt
				|| firstElement instanceof NotificationTemplates || firstElement instanceof ProjectDeactivateEvt
				|| firstElement instanceof ProjectActivateEvt || firstElement instanceof UserProjectRelAssignEvt
				|| firstElement instanceof UserProjectRelRemoveEvt) {
			rightCompLayout.topControl = objectExpPage;
			objectExpPage.refreshExplorer();
			rightcomposite.layout();
			childObjCount = objectExpPage.getChildObjCount();
			getSelectionType(firstElement);
			return;
		} else if (firstElement instanceof Site) {
			SiteCompositeAction siteCompositeUI = rightSidePart.getSiteCompositeUI();
			rightCompLayout.topControl = siteCompositeUI;
			siteCompositeUI.setSite();
			rightcomposite.layout();
			selectionType = 1;
			return;
		} else if (firstElement instanceof AdministrationArea) {
			AdministrationAreaCompositeAction adminCompositeUI = rightSidePart.getAdminAreaCompisiteUI();
			rightCompLayout.topControl = adminCompositeUI;
			adminCompositeUI.setAdministrartionArea();
			rightcomposite.layout();
			selectionType = 1;
			return;
		} else if (firstElement instanceof User) {
			UserCompositeAction userCompositeUI = rightSidePart.getUserCompositeUI();
			rightCompLayout.topControl = userCompositeUI;
			userCompositeUI.setUser();
			rightcomposite.layout();
			selectionType = 1;
			return;
		} else if (firstElement instanceof Project) {
			ProjectCompositeAction projectCompositeUI = rightSidePart.getProjectCompsiteUI();
			rightCompLayout.topControl = projectCompositeUI;
			projectCompositeUI.setProject();
			rightcomposite.layout();
			selectionType = 1;
			return;
		} else if (firstElement instanceof UserGroupModel) {
			UserGroupCompositeAction groupCompositeUI = rightSidePart.getGroupCompositeUI();
			rightCompLayout.topControl = groupCompositeUI;
			groupCompositeUI.setGroup();
			rightcomposite.layout();
			selectionType = 1;
			return;
		}  else if (firstElement instanceof ProjectGroupModel) {
			ProjectGroupCompositeAction projectGroupCompositeUI = rightSidePart.getProjectGroupCompositeUI();
			rightCompLayout.topControl = projectGroupCompositeUI;
			projectGroupCompositeUI.setGroup();
			rightcomposite.layout();
			selectionType = 1;
			return;
		}   else if (firstElement instanceof UserApplicationGroup) {
			UserAppGroupCompositeAction userAppGroupCompositeUI = rightSidePart.getUserAppGroupCompositeUI();
			rightCompLayout.topControl = userAppGroupCompositeUI;
			userAppGroupCompositeUI.setUserAppGroup();
			rightcomposite.layout();
			selectionType = 1;
			return;
		}  else if (firstElement instanceof ProjectApplicationGroup) {
			ProjectAppGroupCompositeAction projectAppGroupCompositeUI = rightSidePart.getProjectAppGroupCompositeUI();
			rightCompLayout.topControl = projectAppGroupCompositeUI;
			projectAppGroupCompositeUI.setProjectAppGroup();
			rightcomposite.layout();
			selectionType = 1;
			return;
		}else if (firstElement instanceof Directory) {
			DirectoryCompositeAction directoryCompositeUI = rightSidePart.getDirectoryCompositeUI();
			rightCompLayout.topControl = directoryCompositeUI;
			directoryCompositeUI.setDirectory();
			rightcomposite.layout();
			selectionType = 1;
			return;
		} else if (firstElement instanceof Icons) {
			IconsCompositeAction iconCompositeUI = rightSidePart.getIconCompsiteUI();
			rightCompLayout.topControl = iconCompositeUI;
			iconCompositeUI.updateIconsInTree();
			iconCompositeUI.registerMessages(registry);
			rightcomposite.layout();
			return;
		} else if (firstElement instanceof Role) {
			RoleCompositeAction roleCompositeUI = rightSidePart.getRoleCompositeUI();
			rightCompLayout.topControl = roleCompositeUI;
			roleCompositeUI.setRole();
			rightcomposite.layout();
			selectionType = 1;
			return;
		} else if (firstElement instanceof SmtpConfiguration) {
			SmtpConfigCompositeAction smtpConfigCompositeUI = rightSidePart.getSmtpConfigCompositeUI();
			rightCompLayout.topControl = smtpConfigCompositeUI;
			smtpConfigCompositeUI.setSmtpConfiguration();
			rightcomposite.layout();
			return;
		} else if (firstElement instanceof LdapConfiguration) {
			LdapConfigCompositeAction ldapConfigCompositeUI = rightSidePart.getLdapConfigCompositeUI();
			rightCompLayout.topControl = ldapConfigCompositeUI;
			ldapConfigCompositeUI.setLdapConfiguration();
			rightcomposite.layout();
			return;
		} else if (firstElement instanceof SingletonAppTimeConfig) {
			SingletonAppTimeConfigComopsiteAction singletonAppTimeConfigCompositeUI = rightSidePart
					.getSingletonAppTimeConfigComopsiteUI();
			rightCompLayout.topControl = singletonAppTimeConfigCompositeUI;
			singletonAppTimeConfigCompositeUI.setSingletonAppTimeConfig();
			rightcomposite.layout();
			return;
		} else if (firstElement instanceof NoProjectPopMessage) {
			NoProjectPopmessageCompositeAction noProjectPopMessageCompositeUI = rightSidePart
					.getNoProjectPopMessageCompositeUI();
			rightCompLayout.topControl = noProjectPopMessageCompositeUI;
			noProjectPopMessageCompositeUI.setNoProjectPopMessage();
			rightcomposite.layout();
			return;
		} else if (firstElement instanceof ProjectExpiryConfig) {
			ProjectExpiryConfigCompositeAction projectExpiryConfigCompositeUI = rightSidePart
					.getProjectExpiryConfigCompositeUI();
			rightCompLayout.topControl = projectExpiryConfigCompositeUI;
			projectExpiryConfigCompositeUI.setProjectExpiryConfig();
			rightcomposite.layout();
			return;
		} else if (firstElement instanceof LiveMessage) {
			LiveMessageConfigAction liveMessagesConfigCompositeUI = rightSidePart.getLiveMessageConfigUI();
			rightCompLayout.topControl = liveMessagesConfigCompositeUI;
			liveMessagesConfigCompositeUI.setLiveMessage();
			rightcomposite.layout();
			selectionType = 1;
			return;
		} else if (firstElement instanceof Notification) {
			NotificationCompositeAction notificationCompUI = rightSidePart.getNotificationCompoUI();
			rightCompLayout.topControl = notificationCompUI;
			notificationCompUI.setNotification();
			rightcomposite.layout();
			return;
		} else if (firstElement instanceof BaseApplication) {
			BaseAppCompositeAction baseAppCompositeUI = rightSidePart.getBaseAppCompositeUI();
			rightCompLayout.topControl = baseAppCompositeUI;
			baseAppCompositeUI.setBaseApplication();
			rightcomposite.layout();
			selectionType = 1;
			return;

		} else if (firstElement instanceof UserApplication) {
			UserApplicationCompositeAction groupCompositeUI = rightSidePart.getUserAppCompositeUI();
			rightCompLayout.topControl = groupCompositeUI;
			groupCompositeUI.setUserApplication();
			rightcomposite.layout();
			selectionType = 1;
			return;
		}else if (firstElement instanceof UserApplicationChild) {
			UserApplicationCompositeAction groupCompositeUI = rightSidePart.getUserAppCompositeUI();
			rightCompLayout.topControl = groupCompositeUI;
			groupCompositeUI.setUserApplication();
			rightcomposite.layout();
			selectionType = 1;
			return;
		} else if (firstElement instanceof ProjectApplication) {
			ProjectApplicationCompositeAction projectAppCompositeUI = rightSidePart.getProjectAppCompositeUI();
			rightCompLayout.topControl = projectAppCompositeUI;
			projectAppCompositeUI.setProjectApplication();
			rightcomposite.layout();
			selectionType = 1;
			return;
		} else if (firstElement instanceof ProjectApplicationChild) {
			ProjectApplicationCompositeAction projectAppCompositeUI = rightSidePart.getProjectAppCompositeUI();
			rightCompLayout.topControl = projectAppCompositeUI;
			projectAppCompositeUI.setProjectApplication();
			rightcomposite.layout();
			selectionType = 1;
			return;
		}else if (firstElement instanceof StartApplication) {
			StartApplicationCompositeAction startAppCompositeUI = rightSidePart.getStartApplicationCompositeUI();
			rightCompLayout.topControl = startAppCompositeUI;
			startAppCompositeUI.setStartApplication();
			rightcomposite.layout();
			selectionType = 1;
			return;
		} else if (firstElement instanceof ProjectCreateEvtAction) {
			ProjectCreateEvtCompositeAction projCreateEvtActionCompositeUI = rightSidePart.getProjectCreateEvtCompositeUI();
			rightCompLayout.topControl = projCreateEvtActionCompositeUI;
			projCreateEvtActionCompositeUI.setProjectCreateEvt();
			rightcomposite.layout();
			selectionType = 1;
			return;
		} else if (firstElement instanceof ProjectDeleteEvtAction) {
			ProjectDeleteEvtCompositeAction projDeleteEvtActionCompositeUI = rightSidePart.getProjectDeleteCompositeUI();
			rightCompLayout.topControl = projDeleteEvtActionCompositeUI;
			projDeleteEvtActionCompositeUI.setProjectDeleteteEvt();
			rightcomposite.layout();
			selectionType = 1;
			return;
		} else if (firstElement instanceof ProjectDeactivateEvtAction) {
			ProjectDeactivateEvtCompositeAction projDeactivateEvtActionCompositeUI = rightSidePart.getProjectDeactivateCompositeUI();
			rightCompLayout.topControl = projDeactivateEvtActionCompositeUI;
			projDeactivateEvtActionCompositeUI.setProjectDeactivateEvt();
			rightcomposite.layout();
			selectionType = 1;
			return;
		} else if (firstElement instanceof ProjectActivateEvtAction) {
			ProjectActivateEvtCompositeAction projActivateEvtActionCompositeUI = rightSidePart.getProjectActivateCompositeUI();
			rightCompLayout.topControl = projActivateEvtActionCompositeUI;
			projActivateEvtActionCompositeUI.setProjectActivateEvt();
			rightcomposite.layout();
			selectionType = 1;
			return;
		} else if (firstElement instanceof UserProjectRelAssignEvtAction) {
			UserProRelAssignEvtCompositeAction userProRelAssignEvtActionCompositeUI = rightSidePart.getUserProRelAssignCompositeUI();
			rightCompLayout.topControl = userProRelAssignEvtActionCompositeUI;
			userProRelAssignEvtActionCompositeUI.setUserProRelAssignEvt();
			rightcomposite.layout();
			selectionType = 1;
			return;
		} else if (firstElement instanceof UserProjectRelRemoveEvtAction) {
			UserProRelRemoveEvtCompositeAction userProRelRemoveEvtActionCompositeUI = rightSidePart.getUserProRelRemoveCompositeUI();
			rightCompLayout.topControl = userProRelRemoveEvtActionCompositeUI;
			userProRelRemoveEvtActionCompositeUI.setUserProRelRemoveEvt();
			rightcomposite.layout();
			selectionType = 1;
			return;
		} else if (firstElement instanceof NotifyAAProjectUserEvt) {
			NotifyAAProjectUserCompositeAction notifyAAProjectUserCompositeAction = rightSidePart.getNotifyAAProjectUserCompositeUI();
			rightCompLayout.topControl = notifyAAProjectUserCompositeAction;
			notifyAAProjectUserCompositeAction.setNotifyAAProUserEvtAction();
			rightcomposite.layout();
			selectionType = 1;
			return;
		}  else if (firstElement instanceof UserProExpGraceEvt) {
			UserProjectExpGraceCompositeAction userProjectExpGraceCompositeAction = rightSidePart.getUserProjectExpGraceCompositeUI();
			rightCompLayout.topControl = userProjectExpGraceCompositeAction;
			userProjectExpGraceCompositeAction.setUserProExpGraceEvt();
			rightcomposite.layout();
			selectionType = 1;
			return;
		} else if (firstElement instanceof UserProExpEvt) {
			UserProjectExpCompositeAction userProjectRelExpCompositeAction = rightSidePart.getUserProjectRelExpCompositeUI();
			rightCompLayout.topControl = userProjectRelExpCompositeAction;
			userProjectRelExpCompositeAction.setUserProExpEvt();
			rightcomposite.layout();
			selectionType = 1;
			return;
		} else if (firstElement instanceof NotificationTemplate) {
			TemplateCompositeAction templateCompositeAction = rightSidePart.getTemplateCompositeUI();
			rightCompLayout.topControl = templateCompositeAction;
			templateCompositeAction.setTemplate();
			rightcomposite.layout();
			selectionType = 1;
			return;
		}else if (firstElement instanceof RelationObj || firstElement instanceof AdminMenu
				|| firstElement instanceof Applications || firstElement instanceof Groups
				|| firstElement instanceof DirectoryApplications || firstElement instanceof Configurations 
				|| firstElement instanceof Notifications) {
			InfoDisplayPage welcomePageUI = rightSidePart.getInfoDisplayPage();
			rightCompLayout.topControl = welcomePageUI;
			welcomePageUI.updateContent();
			rightcomposite.layout();
			if (firstElement instanceof RelationObj || firstElement instanceof DirectoryApplications) {
				selectionType = 2;
			}
		}
	}

	/**
	 * Gets the selection type.
	 *
	 * @param firstElement the first element
	 * @param objectExpPage the object exp page
	 * @return the selection type
	 */
	private void getSelectionType(Object firstElement) {
		if (firstElement instanceof SiteAdminAreaProjectChild || firstElement instanceof SiteAdminProjectAppFixed
				|| firstElement instanceof SiteAdminProjectAppNotFixed
				|| firstElement instanceof SiteAdminProjectAppProtected
				|| firstElement instanceof SiteAdminAreaUserAppNotFixed
				|| firstElement instanceof SiteAdminAreaUserAppFixed
				|| firstElement instanceof SiteAdminAreaUserAppProtected
				|| firstElement instanceof ProjectAdminAreaProjectAppFixed
				|| firstElement instanceof ProjectAdminAreaProjectAppNotFixed
				|| firstElement instanceof ProjectAdminAreaProjectAppProtected
				|| firstElement instanceof ProjectUserAAProjectAppAllowed
				|| firstElement instanceof ProjectUserAAProjectAppForbidden
				|| firstElement instanceof AdminAreaUserAppFixed || firstElement instanceof AdminAreaUserAppNotFixed
				|| firstElement instanceof AdminAreaUserAppProtected || firstElement instanceof AdminAreaProjectAppFixed
				|| firstElement instanceof AdminAreaProjectAppNotFixed
				|| firstElement instanceof AdminAreaProjectAppProtected || firstElement instanceof UserAAUserAppAllowed
				|| firstElement instanceof UserAAUserAppForbidden) {
			selectionType = 4;

		} else {
			selectionType = 3;
		}
	}

	/**
	 * Update status bar.
	 */
	protected void updateStatusBar() {
		//String selectionString = "";
		TreeItem[] treeSelection = this.getTree().getSelection();
		if (treeSelection != null && treeSelection.length > 0) {
			StringBuilder statusBarMessage = getStatusBarMessage();
			if(statusBarMessage.length() == 0){
				statusBarMessage.append(treeSelection[0].getText());
			}
			this.eventBroker.send(CommonConstants.EVENT_BROKER.STATUSBAR,
					statusBarMessage.toString());
		} else {
			this.eventBroker.send(CommonConstants.EVENT_BROKER.STATUSBAR, "caxStartAdmin");
		}
	}

	/**
	 * Gets the status bar message.
	 *
	 * @return the status bar message
	 */
	private StringBuilder getStatusBarMessage() {
		final StringBuilder message = new StringBuilder();
		ITreeSelection structuredSelection = this.getStructuredSelection();
		Object firstElement = structuredSelection.getFirstElement();
		TreeItem[] treeSelection = this.getTree().getSelection();
		final IBaseLabelProvider labelProvider = this.getLabelProvider();
		switch (selectionType) {
		case 0:
			message.append(treeSelection[0].getText());
			break;
		case 1:
			message.append(getClassName(firstElement)).append(" ");
			message.append(" ' ").append(treeSelection[0].getText()).append(" ' ").append(" ");
			message.append(messages.statusBarSelectedLbl);
			break;

		case 2:
			final ArrayList<String> parentObjNames = new ArrayList<>();
			if(firstElement instanceof DirectoryApplications){
				for (IAdminTreeChild parent = (IAdminTreeChild) firstElement; parent != null; parent = parent.getParent()) {
					final String name = ((AdminTreeLabelProvider) labelProvider).getText(parent);
					if (!messages.directoryNode.equalsIgnoreCase(name)){
					parentObjNames.add(name);
					}
				}
				Collections.reverse(parentObjNames);
				final StringBuilder parentInfoSb = new StringBuilder();
				for (String name : parentObjNames) {
					parentInfoSb.append(name).append(" / ");
				}
				if (parentInfoSb.length() > 2) {
					message.append(parentInfoSb.substring(0, parentInfoSb.length() - 2));
				}
				break;
			}
			for (IAdminTreeChild parent = (IAdminTreeChild) firstElement; parent != null; parent = parent.getParent()) {
				final String name = ((AdminTreeLabelProvider) labelProvider).getText(parent);
				if (!messages.usersNode.equalsIgnoreCase(name) && !messages.applicationsNode.equalsIgnoreCase(name)
						&& !messages.configurationNode.equalsIgnoreCase(name)) {
					parentObjNames.add(name);
				}
				parent = parent.getParent();
				if (parent == null) {
					break;
				}
			}
			Collections.reverse(parentObjNames);
			final StringBuilder parentInfoSb = new StringBuilder();
			for (String name : parentObjNames) {
				parentInfoSb.append(name).append(" / ");
			}
			if (parentInfoSb.length() > 2) {
				message.append(parentInfoSb.substring(0, parentInfoSb.length() - 2));
			}
			break;
		case 3:
			for (IAdminTreeChild parent = (IAdminTreeChild) firstElement; parent != null; parent = parent.getParent()) {

				if(parent instanceof UsersNameAlphabet){
					parent = parent.getParent();
				}
				final String name = ((AdminTreeLabelProvider) labelProvider).getText(parent);
				String refObjClassName;

				if (message.length() != 0) {
					if (parent instanceof RelationObj) {
						refObjClassName = getClassName(((RelationObj) parent).getRefObject());
					} else {
						refObjClassName = getClassName(parent);
					}
					
					parent = parent.getParent();
					if (parent == null) {
						break;
					}
					message.append(messages.statusBarInLbl).append(" ");
					message.append(refObjClassName).append(" ");
					message.append(" ' ").append(name).append(" ' ").append(" ");
				} else {
					message.append(" ' ").append(childObjCount).append(" ' ").append(" ");
					message.append(name).append(" ");
					if (firstElement instanceof DirectoryUserApplications
							|| firstElement instanceof DirectoryProjectApplications) {
						parent = parent.getParent();
					} else if (firstElement instanceof AdminUsers || firstElement instanceof AdminUserApps
							|| firstElement instanceof AdminProjectApps) {
						parent = parent.getParent();
						final String name2 = ((AdminTreeLabelProvider) labelProvider).getText(parent);
						message.append(messages.statusBarInLbl).append(" ").append(name2).append(" ");
					}
				}
			}
			message.append(messages.statusBarFoundLbl);
			break;

		case 4:
			for (IAdminTreeChild parent = (IAdminTreeChild) firstElement; parent != null; parent = parent.getParent()) {

				final String name = ((AdminTreeLabelProvider) labelProvider).getText(parent);
				String refObjClassName;
				if (message.length() != 0) {
					if (parent instanceof RelationObj) {
						refObjClassName = getClassName(((RelationObj) parent).getRefObject());/*((RelationObj) parent).getRefObject().getClass().getSimpleName()*/;
					} else {
						refObjClassName = getClassName(parent) /*parent.getClass().getSimpleName()*/;
					}
					parent = parent.getParent();
					if (parent == null) {
						break;
					}
					message.append(messages.statusBarInLbl).append(" ");
					message.append(refObjClassName).append(" ");
					message.append(" ' ").append(name).append(" ' ").append(" ");
				} else {
					message.append(" ' ").append(childObjCount).append(" ' ").append(" ");
					message.append(name).append(" ");
					parent = parent.getParent();
					String name2 = ((AdminTreeLabelProvider) labelProvider).getText(parent);
					message.append(name2).append(" ");
				}
			}
			message.append(messages.statusBarFoundLbl);
			break;
		default:
			message.append(treeSelection[0].getText());
			break;
		}
		return message;
	}

	/**
	 * Gets the class name.
	 *
	 * @param firstElement the first element
	 * @return the class name
	 */
	private String getClassName(Object firstElement) {
		final IBaseLabelProvider labelProvider = this.getLabelProvider();
		String className = firstElement.getClass().getSimpleName();

		IAdminTreeChild parent = ((IAdminTreeChild) firstElement).getParent();
		if (parent instanceof UsersNameAlphabet) {
			parent = parent.getParent();
		}
		if (parent != null) {
			className = ((AdminTreeLabelProvider) labelProvider).getText(parent);
		}
		return className;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.viewers.AbstractTreeViewer#doUpdateItem(org.eclipse.swt
	 * .widgets.Item, java.lang.Object)
	 */
	@Override
	protected void doUpdateItem(final Item item, final Object element) {
		Object data = null;
		if (item != null && !item.isDisposed()) {
			if (item.getData(KEY) == null) {
				item.setData(KEY, Boolean.FALSE);
				data = item.getData();
			}
			if (data instanceof Sites) {
				if (!(Boolean) (item.getData(KEY))) {
					registry.register((text) -> setTextToItem(item, text), (message) -> {
						return registerMessageToItem(item, message);
					});
				}
			} else if (data instanceof AdministrationAreas) {
				if (!(Boolean) (item.getData(KEY))) {
					registry.register((text) -> setTextToItem(item, text), (message) -> {
						return registerMessageToItem(item, message);
					});
				}
			} else if (data instanceof SiteAdministrations) {
				if (!(Boolean) (item.getData(KEY))) {
					registry.register((text) -> setTextToItem(item, text), (message) -> {
						return registerMessageToItem(item, message);
					});
				}
			} else if (data instanceof Users) {
				if (!(Boolean) (item.getData(KEY))) {
					registry.register((text) -> setTextToItem(item, text), (message) -> {
						return registerMessageToItem(item, message);
					});
				}
			} else if (data instanceof Projects) {
				if (!(Boolean) (item.getData(KEY))) {
					registry.register((text) -> setTextToItem(item, text), (message) -> {
						return registerMessageToItem(item, message);
					});
				}
			} else if (data instanceof Groups) {
				if (!(Boolean) (item.getData(KEY))) {
					registry.register((text) -> setTextToItem(item, text), (message) -> {
						return registerMessageToItem(item, message);
					});
				}
			} else if (data instanceof UserGroupsModel) {
				if (!(Boolean) (item.getData(KEY))) {
					registry.register((text) -> setTextToItem(item, text), (message) -> {
						return registerMessageToItem(item, message);
					});
				}
			} else if (data instanceof ProjectGroupsModel) {
				if (!(Boolean) (item.getData(KEY))) {
					registry.register((text) -> setTextToItem(item, text), (message) -> {
						return registerMessageToItem(item, message);
					});
				}
			} else if (data instanceof UserApplicationGroups) {
				if (!(Boolean) (item.getData(KEY))) {
					registry.register((text) -> setTextToItem(item, text), (message) -> {
						return registerMessageToItem(item, message);
					});
				}
			} else if (data instanceof ProjectApplicationGroups) {
				if (!(Boolean) (item.getData(KEY))) {
					registry.register((text) -> setTextToItem(item, text), (message) -> {
						return registerMessageToItem(item, message);
					});
				}
			} else if (data instanceof Directories) {
				if (!(Boolean) (item.getData(KEY))) {
					registry.register((text) -> setTextToItem(item, text), (message) -> {
						return registerMessageToItem(item, message);
					});
				}
			} else if (data instanceof Configurations) {
				if (!(Boolean) (item.getData(KEY))) {
					registry.register((text) -> setTextToItem(item, text), (message) -> {
						return registerMessageToItem(item, message);
					});
				}
			} else if (data instanceof Roles) {
				if (!(Boolean) (item.getData(KEY))) {
					registry.register((text) -> setTextToItem(item, text), (message) -> {
						return registerMessageToItem(item, message);
					});
				}
			} else if (data instanceof Icons) {
				if (!(Boolean) (item.getData(KEY))) {
					registry.register((text) -> setTextToItem(item, text), (message) -> {
						return registerMessageToItem(item, message);
					});
				}
			} else if (data instanceof SmtpConfiguration) {
				if (!(Boolean) (item.getData(KEY))) {
					registry.register((text) -> setTextToItem(item, text), (message) -> {
						return registerMessageToItem(item, message);
					});
				}
			} else if (data instanceof LdapConfiguration) {
				if (!(Boolean) (item.getData(KEY))) {
					registry.register((text) -> setTextToItem(item, text), (message) -> {
						return registerMessageToItem(item, message);
					});
				}
			} else if (data instanceof SingletonAppTimeConfig) {
				if (!(Boolean) (item.getData(KEY))) {
					registry.register((text) -> setTextToItem(item, text), (message) -> {
						return registerMessageToItem(item, message);
					});
				}
			} else if (data instanceof NoProjectPopMessage) {
				if (!(Boolean) (item.getData(KEY))) {
					registry.register((text) -> setTextToItem(item, text), (message) -> {
						return registerMessageToItem(item, message);
					});
				}
			} else if (data instanceof ProjectExpiryConfig) {
				if (!(Boolean) (item.getData(KEY))) {
					registry.register((text) -> setTextToItem(item, text), (message) -> {
						return registerMessageToItem(item, message);
					});
				}
			} else if (data instanceof LiveMessages) {
				if (!(Boolean) (item.getData(KEY))) {
					registry.register((text) -> setTextToItem(item, text), (message) -> {
						return registerMessageToItem(item, message);
					});
				}
			} else if (data instanceof Notifications) {
				if (!(Boolean) (item.getData(KEY))) {
					registry.register((text) -> setTextToItem(item, text), (message) -> {
						return registerMessageToItem(item, message);
					});
				}
			} else if (data instanceof Applications) {
				if (!(Boolean) (item.getData(KEY))) {
					registry.register((text) -> setTextToItem(item, text), (message) -> {
						return registerMessageToItem(item, message);
					});
				}
			} else if (data instanceof UserApplications) {
				if (!(Boolean) (item.getData(KEY))) {
					registry.register((text) -> setTextToItem(item, text), (message) -> {
						return registerMessageToItem(item, message);
					});
				}
			} else if (data instanceof UserApplication) {
				if (!(Boolean) (item.getData(KEY))) {
					registry.register((text) -> setTextToItem(item, text), (message) -> {
						return registerMessageToItem(item, message);
					});
				}
			} else if (data instanceof ProjectApplications) {
				if (!(Boolean) (item.getData(KEY))) {
					registry.register((text) -> setTextToItem(item, text), (message) -> {
						return registerMessageToItem(item, message);
					});
				}
			} else if (data instanceof ProjectApplication) {
				if (!(Boolean) (item.getData(KEY))) {
					registry.register((text) -> setTextToItem(item, text), (message) -> {
						return registerMessageToItem(item, message);
					});
				}
			} else if (element instanceof StartApplications) {
				if (!(Boolean) (item.getData(KEY))) {
					registry.register((text) -> setTextToItem(item, text), (message) -> {
						return registerMessageToItem(item, message);
					});
				}
			} else if (element instanceof StartApplication) {
				if (!(Boolean) (item.getData(KEY))) {
					registry.register((text) -> setTextToItem(item, text), (message) -> {
						return registerMessageToItem(item, message);
					});
				}
			} else if (data instanceof BaseApplications) {
				if (!(Boolean) (item.getData(KEY))) {
					registry.register((text) -> setTextToItem(item, text), (message) -> {
						return registerMessageToItem(item, message);
					});
				}
			} else if (data instanceof BaseApplication) {
				if (!(Boolean) (item.getData(KEY))) {
					registry.register((text) -> setTextToItem(item, text), (message) -> {
						return registerMessageToItem(item, message);
					});
				}
			} else if (data instanceof SiteAdminAreaProjects) {
				if (!(Boolean) (item.getData(KEY))) {
					registry.register((text) -> setTextToItem(item, text), (message) -> {
						return registerMessageToItem(item, message);
					});
				}
			} else if (data instanceof SiteAdminAreaUserApplications) {
				if (!(Boolean) (item.getData(KEY))) {
					registry.register((text) -> setTextToItem(item, text), (message) -> {
						return registerMessageToItem(item, message);
					});
				}
			} else if (data instanceof SiteAdminAreaUserAppFixed) {
				if (!(Boolean) (item.getData(KEY))) {
					registry.register((text) -> setTextToItem(item, text), (message) -> {
						return registerMessageToItem(item, message);
					});
				}
			} else if (data instanceof SiteAdminAreaUserAppNotFixed) {
				if (!(Boolean) (item.getData(KEY))) {
					registry.register((text) -> setTextToItem(item, text), (message) -> {
						return registerMessageToItem(item, message);
					});
				}
			} else if (data instanceof SiteAdminAreaUserAppProtected) {
				if (!(Boolean) (item.getData(KEY))) {
					registry.register((text) -> setTextToItem(item, text), (message) -> {
						return registerMessageToItem(item, message);
					});
				}
			} else if (data instanceof SiteAdminProjectAppFixed) {
				if (!(Boolean) (item.getData(KEY))) {
					registry.register((text) -> setTextToItem(item, text), (message) -> {
						return registerMessageToItem(item, message);
					});
				}
			} else if (data instanceof SiteAdminProjectAppNotFixed) {
				if (!(Boolean) (item.getData(KEY))) {
					registry.register((text) -> setTextToItem(item, text), (message) -> {
						return registerMessageToItem(item, message);
					});
				}
			} else if (data instanceof SiteAdminProjectAppProtected) {
				if (!(Boolean) (item.getData(KEY))) {
					registry.register((text) -> setTextToItem(item, text), (message) -> {
						return registerMessageToItem(item, message);
					});
				}
			} else if (data instanceof SiteAdminAreaProjectApplications) {
				if (!(Boolean) (item.getData(KEY))) {
					registry.register((text) -> setTextToItem(item, text), (message) -> {
						return registerMessageToItem(item, message);
					});
				}
			} else if (data instanceof SiteAdminAreaStartApplications) {
				if (!(Boolean) (item.getData(KEY))) {
					registry.register((text) -> setTextToItem(item, text), (message) -> {
						return registerMessageToItem(item, message);
					});
				}
			} else if (data instanceof SiteAdminAreaProjectStartApplications) {
				if (!(Boolean) (item.getData(KEY))) {
					registry.register((text) -> setTextToItem(item, text), (message) -> {
						return registerMessageToItem(item, message);
					});
				}
			} else if (data instanceof SiteAdminAreaInformations) {
				if (!(Boolean) (item.getData(KEY))) {
					registry.register((text) -> setTextToItem(item, text), (message) -> {
						return registerMessageToItem(item, message);
					});
				}
			} else if (data instanceof SiteAdminAreaInformationHistory) {
				if (!(Boolean) (item.getData(KEY))) {
					registry.register((text) -> setTextToItem(item, text), (message) -> {
						return registerMessageToItem(item, message);
					});
				}
			} else if (data instanceof SiteAdminAreaInformationStatus) {
				if (!(Boolean) (item.getData(KEY))) {
					registry.register((text) -> setTextToItem(item, text), (message) -> {
						return registerMessageToItem(item, message);
					});
				}
			} else if (data instanceof AdminAreaProjects) {
				if (!(Boolean) (item.getData(KEY))) {
					registry.register((text) -> setTextToItem(item, text), (message) -> {
						return registerMessageToItem(item, message);
					});
				}
			} else if (data instanceof AdminAreaProjectApplications) {
				if (!(Boolean) (item.getData(KEY))) {
					registry.register((text) -> setTextToItem(item, text), (message) -> {
						return registerMessageToItem(item, message);
					});
				}
			} else if (data instanceof AdminAreaProjectAppNotFixed) {
				if (!(Boolean) (item.getData(KEY))) {
					registry.register((text) -> setTextToItem(item, text), (message) -> {
						return registerMessageToItem(item, message);
					});
				}
			} else if (data instanceof AdminAreaProjectAppFixed) {
				if (!(Boolean) (item.getData(KEY))) {
					registry.register((text) -> setTextToItem(item, text), (message) -> {
						return registerMessageToItem(item, message);
					});
				}
			} else if (data instanceof AdminAreaProjectAppProtected) {
				if (!(Boolean) (item.getData(KEY))) {
					registry.register((text) -> setTextToItem(item, text), (message) -> {
						return registerMessageToItem(item, message);
					});
				}
			} else if (data instanceof AdminAreaStartApplications) {
				if (!(Boolean) (item.getData(KEY))) {
					registry.register((text) -> setTextToItem(item, text), (message) -> {
						return registerMessageToItem(item, message);
					});
				}
			} else if (data instanceof AdminAreaProjectStartApplications) {
				if (!(Boolean) (item.getData(KEY))) {
					registry.register((text) -> setTextToItem(item, text), (message) -> {
						return registerMessageToItem(item, message);
					});
				}
			} else if (data instanceof AdminAreaUserApplications) {
				if (!(Boolean) (item.getData(KEY))) {
					registry.register((text) -> setTextToItem(item, text), (message) -> {
						return registerMessageToItem(item, message);
					});
				}
			} else if (data instanceof AdminAreaUserAppFixed) {
				if (!(Boolean) (item.getData(KEY))) {
					registry.register((text) -> setTextToItem(item, text), (message) -> {
						return registerMessageToItem(item, message);
					});
				}
			} else if (data instanceof AdminAreaUserAppNotFixed) {
				if (!(Boolean) (item.getData(KEY))) {
					registry.register((text) -> setTextToItem(item, text), (message) -> {
						return registerMessageToItem(item, message);
					});
				}
			} else if (data instanceof AdminAreaUserAppProtected) {
				if (!(Boolean) (item.getData(KEY))) {
					registry.register((text) -> setTextToItem(item, text), (message) -> {
						return registerMessageToItem(item, message);
					});
				}
			} else if (data instanceof AdminAreaInfo) {
				if (!(Boolean) (item.getData(KEY))) {
					registry.register((text) -> setTextToItem(item, text), (message) -> {
						return registerMessageToItem(item, message);
					});
				}
			} else if (data instanceof AdminAreaInfoHistory) {
				if (!(Boolean) (item.getData(KEY))) {
					registry.register((text) -> setTextToItem(item, text), (message) -> {
						return registerMessageToItem(item, message);
					});
				}
			} else if (data instanceof AdminAreaInfoStatus) {
				if (!(Boolean) (item.getData(KEY))) {
					registry.register((text) -> setTextToItem(item, text), (message) -> {
						return registerMessageToItem(item, message);
					});
				}
			} else if (data instanceof AdminAreaStartApp) {
				if (!(Boolean) (item.getData(KEY))) {
					registry.register((text) -> setTextToItem(item, text), (message) -> {
						return registerMessageToItem(item, message);
					});
				}
			} else if (data instanceof ProjectAdminAreas) {
				if (!(Boolean) (item.getData(KEY))) {
					registry.register((text) -> setTextToItem(item, text), (message) -> {
						return registerMessageToItem(item, message);
					});
				}
			} else if (data instanceof ProjectAdminAreaProjectApplications) {
				if (!(Boolean) (item.getData(KEY))) {
					registry.register((text) -> setTextToItem(item, text), (message) -> {
						return registerMessageToItem(item, message);
					});
				}
			} else if (data instanceof ProjectAdminAreaProjectAppFixed) {
				if (!(Boolean) (item.getData(KEY))) {
					registry.register((text) -> setTextToItem(item, text), (message) -> {
						return registerMessageToItem(item, message);
					});
				}
			} else if (data instanceof ProjectAdminAreaProjectAppNotFixed) {
				if (!(Boolean) (item.getData(KEY))) {
					registry.register((text) -> setTextToItem(item, text), (message) -> {
						return registerMessageToItem(item, message);
					});
				}
			} else if (data instanceof ProjectAdminAreaProjectAppProtected) {
				if (!(Boolean) (item.getData(KEY))) {
					registry.register((text) -> setTextToItem(item, text), (message) -> {
						return registerMessageToItem(item, message);
					});
				}
			} else if (data instanceof ProjectAdminAreaStartApplications) {
				if (!(Boolean) (item.getData(KEY))) {
					registry.register((text) -> setTextToItem(item, text), (message) -> {
						return registerMessageToItem(item, message);
					});
				}
			} else if (data instanceof ProjectUsers) {
				if (!(Boolean) (item.getData(KEY))) {
					registry.register((text) -> setTextToItem(item, text), (message) -> {
						return registerMessageToItem(item, message);
					});
				}
			} else if (data instanceof ProjectProjectApplications) {
				if (!(Boolean) (item.getData(KEY))) {
					registry.register((text) -> setTextToItem(item, text), (message) -> {
						return registerMessageToItem(item, message);
					});
				}
			}  else if (data instanceof ProjectInformation) {
				if (!(Boolean) (item.getData(KEY))) {
					registry.register((text) -> setTextToItem(item, text), (message) -> {
						return registerMessageToItem(item, message);
					});
				}
			} else if (data instanceof ProjectInfoStatus) {
				if (!(Boolean) (item.getData(KEY))) {
					registry.register((text) -> setTextToItem(item, text), (message) -> {
						return registerMessageToItem(item, message);
					});
				}
			} else if (data instanceof ProjectInfoHistory) {
				if (!(Boolean) (item.getData(KEY))) {
					registry.register((text) -> setTextToItem(item, text), (message) -> {
						return registerMessageToItem(item, message);
					});
				}
			} else if (data instanceof UserProjects) {
				if (!(Boolean) (item.getData(KEY))) {
					registry.register((text) -> setTextToItem(item, text), (message) -> {
						return registerMessageToItem(item, message);
					});
				}
			} else if (data instanceof UserAAUserApplications) {
				if (!(Boolean) (item.getData(KEY))) {
					registry.register((text) -> setTextToItem(item, text), (message) -> {
						return registerMessageToItem(item, message);
					});
				}
			} else if (data instanceof UserStartApplications) {
				if (!(Boolean) (item.getData(KEY))) {
					registry.register((text) -> setTextToItem(item, text), (message) -> {
						return registerMessageToItem(item, message);
					});
				}
			} else if (data instanceof UserGroups) {
				if (!(Boolean) (item.getData(KEY))) {
					registry.register((text) -> setTextToItem(item, text), (message) -> {
						return registerMessageToItem(item, message);
					});
				}
			} else if (data instanceof UserInformations) {
				if (!(Boolean) (item.getData(KEY))) {
					registry.register((text) -> setTextToItem(item, text), (message) -> {
						return registerMessageToItem(item, message);
					});
				}
			} else if (data instanceof UserInformationStatus) {
				if (!(Boolean) (item.getData(KEY))) {
					registry.register((text) -> setTextToItem(item, text), (message) -> {
						return registerMessageToItem(item, message);
					});
				}
			} else if (data instanceof UserInformationHistory) {
				if (!(Boolean) (item.getData(KEY))) {
					registry.register((text) -> setTextToItem(item, text), (message) -> {
						return registerMessageToItem(item, message);
					});
				}
			} else if (data instanceof UserAAUserAppAllowed) {
				if (!(Boolean) (item.getData(KEY))) {
					registry.register((text) -> setTextToItem(item, text), (message) -> {
						return registerMessageToItem(item, message);
					});
				}
			} else if (data instanceof UserAAUserAppForbidden) {
				if (!(Boolean) (item.getData(KEY))) {
					registry.register((text) -> setTextToItem(item, text), (message) -> {
						return registerMessageToItem(item, message);
					});
				}
			} else if (data instanceof UserProjectAdminAreas) {
				if (!(Boolean) (item.getData(KEY))) {
					registry.register((text) -> setTextToItem(item, text), (message) -> {
						return registerMessageToItem(item, message);
					});
				}
			} else if (data instanceof UserProjectAAProjectApplications) {
				if (!(Boolean) (item.getData(KEY))) {
					registry.register((text) -> setTextToItem(item, text), (message) -> {
						return registerMessageToItem(item, message);
					});
				}
			} else if (data instanceof UserProjectAAProjectAppAllowed) {
				if (!(Boolean) (item.getData(KEY))) {
					registry.register((text) -> setTextToItem(item, text), (message) -> {
						return registerMessageToItem(item, message);
					});
				}
			} else if (data instanceof UserProjectAAProjectAppForbidden) {
				if (!(Boolean) (item.getData(KEY))) {
					registry.register((text) -> setTextToItem(item, text), (message) -> {
						return registerMessageToItem(item, message);
					});
				}
			} else if (data instanceof UserAdminAreas) {
				if (!(Boolean) (item.getData(KEY))) {
					registry.register((text) -> setTextToItem(item, text), (message) -> {
						return registerMessageToItem(item, message);
					});
				}
			} else if (data instanceof UserAppAdminAreas) {
				if (!(Boolean) (item.getData(KEY))) {
					registry.register((text) -> setTextToItem(item, text), (message) -> {
						return registerMessageToItem(item, message);
					});
				}
			} else if (data instanceof UserAppUsers) {
				if (!(Boolean) (item.getData(KEY))) {
					registry.register((text) -> setTextToItem(item, text), (message) -> {
						return registerMessageToItem(item, message);
					});
				}
			} else if (data instanceof ProjectAppAdminAreas) {
				if (!(Boolean) (item.getData(KEY))) {
					registry.register((text) -> setTextToItem(item, text), (message) -> {
						return registerMessageToItem(item, message);
					});
				}
			} else if (data instanceof ProjectAppAdminAreaProjects) {
				if (!(Boolean) (item.getData(KEY))) {
					registry.register((text) -> setTextToItem(item, text), (message) -> {
						return registerMessageToItem(item, message);
					});
				}
			} else if (data instanceof ProjectAppUsers) {
				if (!(Boolean) (item.getData(KEY))) {
					registry.register((text) -> setTextToItem(item, text), (message) -> {
						return registerMessageToItem(item, message);
					});
				}
			} else if (data instanceof StartAppAdminAreas) {
				if (!(Boolean) (item.getData(KEY))) {
					registry.register((text) -> setTextToItem(item, text), (message) -> {
						return registerMessageToItem(item, message);
					});
				}
			} else if (data instanceof StartAppProjects) {
				if (!(Boolean) (item.getData(KEY))) {
					registry.register((text) -> setTextToItem(item, text), (message) -> {
						return registerMessageToItem(item, message);
					});
				}
			} else if (data instanceof StartAppUsers) {
				if (!(Boolean) (item.getData(KEY))) {
					registry.register((text) -> setTextToItem(item, text), (message) -> {
						return registerMessageToItem(item, message);
					});
				}
			} else if (data instanceof BaseAppProjectApplications) {
				if (!(Boolean) (item.getData(KEY))) {
					registry.register((text) -> setTextToItem(item, text), (message) -> {
						return registerMessageToItem(item, message);
					});
				}
			} else if (data instanceof BaseAppUserApplications) {
				if (!(Boolean) (item.getData(KEY))) {
					registry.register((text) -> setTextToItem(item, text), (message) -> {
						return registerMessageToItem(item, message);
					});
				}
			} else if (data instanceof BaseAppStartApplications) {
				if (!(Boolean) (item.getData(KEY))) {
					registry.register((text) -> setTextToItem(item, text), (message) -> {
						return registerMessageToItem(item, message);
					});
				}
			} else if (data instanceof UserGroupUsers) {
				if (!(Boolean) (item.getData(KEY))) {
					registry.register((text) -> setTextToItem(item, text), (message) -> {
						return registerMessageToItem(item, message);
					});
				}
			} else if (data instanceof ProjectGroupProjects) {
				if (!(Boolean) (item.getData(KEY))) {
					registry.register((text) -> setTextToItem(item, text), (message) -> {
						return registerMessageToItem(item, message);
					});
				}
			} else if (data instanceof UserAppGroupUserApps) {
				if (!(Boolean) (item.getData(KEY))) {
					registry.register((text) -> setTextToItem(item, text), (message) -> {
						return registerMessageToItem(item, message);
					});
				}
			} else if (data instanceof ProjectAppGroupProjectApps) {
				if (!(Boolean) (item.getData(KEY))) {
					registry.register((text) -> setTextToItem(item, text), (message) -> {
						return registerMessageToItem(item, message);
					});
				}
			} else if (data instanceof DirectoryUsers) {
				if (!(Boolean) (item.getData(KEY))) {
					registry.register((text) -> setTextToItem(item, text), (message) -> {
						return registerMessageToItem(item, message);
					});
				}
			} else if (data instanceof DirectoryProjects) {
				if (!(Boolean) (item.getData(KEY))) {
					registry.register((text) -> setTextToItem(item, text), (message) -> {
						return registerMessageToItem(item, message);
					});
				}
			} else if (data instanceof DirectoryApplications) {
				if (!(Boolean) (item.getData(KEY))) {
					registry.register((text) -> setTextToItem(item, text), (message) -> {
						return registerMessageToItem(item, message);
					});
				}
			} else if (data instanceof DirectoryUserApplications) {
				if (!(Boolean) (item.getData(KEY))) {
					registry.register((text) -> setTextToItem(item, text), (message) -> {
						return registerMessageToItem(item, message);
					});
				}
			} else if (data instanceof DirectoryProjectApplications) {
				if (!(Boolean) (item.getData(KEY))) {
					registry.register((text) -> setTextToItem(item, text), (message) -> {
						return registerMessageToItem(item, message);
					});
				}
			} else if (data instanceof AdminMenu) {
				if (!(Boolean) (item.getData(KEY))) {
					registry.register((text) -> setTextToItem(item, text), (message) -> {
						return registerMessageToItem(item, message);
					});
				}
			} else if (data instanceof RoleScopeObjects) {
				if (!(Boolean) (item.getData(KEY))) {
					registry.register((text) -> setTextToItem(item, text), (message) -> {
						return registerMessageToItem(item, message);
					});
				}
			} else if (data instanceof RoleScopeObjectUsers) {
				if (!(Boolean) (item.getData(KEY))) {
					registry.register((text) -> setTextToItem(item, text), (message) -> {
						return registerMessageToItem(item, message);
					});
				}
			} else if (data instanceof RoleUsers) {
				if (!(Boolean) (item.getData(KEY))) {
					registry.register((text) -> setTextToItem(item, text), (message) -> {
						return registerMessageToItem(item, message);
					});
				}
			} else if (data instanceof AdminUsers) {
				if (!(Boolean) (item.getData(KEY))) {
					registry.register((text) -> setTextToItem(item, text), (message) -> {
						return registerMessageToItem(item, message);
					});
				}
			} else if (data instanceof AdminUserApps) {
				if (!(Boolean) (item.getData(KEY))) {
					registry.register((text) -> setTextToItem(item, text), (message) -> {
						return registerMessageToItem(item, message);
					});
				}
			} else if (data instanceof AdminProjectApps) {
				if (!(Boolean) (item.getData(KEY))) {
					registry.register((text) -> setTextToItem(item, text), (message) -> {
						return registerMessageToItem(item, message);
					});
				}
			}

		}
		super.doUpdateItem(item, element);
	}

	/**
	 * Sets the text to item.
	 *
	 * @param item
	 *            the item
	 * @param text
	 *            the text
	 */
	private void setTextToItem(final Item item, String text) {
		if (item != null && !item.isDisposed()) {
			item.setText(text);
		}
	}

	/**
	 * Register message to item.
	 *
	 * @param item
	 *            the item
	 * @param messages
	 *            the messages
	 * @return the string
	 */
	private String registerMessageToItem(final Item item, final Message messages) {
		if (item != null && !item.isDisposed()) {
			item.setData(KEY, true);
			Object element = item.getData();
			// FIXME 2017-10-11 SCP: Too many nested if-else-statements, extensively usage
			// of instanceof
			if (element instanceof Sites) {
				return messages.sitesNode;
			} else if (element instanceof AdministrationAreas) {
				return messages.administrationAreasNode;
			} else if (element instanceof Users) {
				return messages.usersNode;
			} else if (element instanceof Projects) {
				return messages.projectsNode;
			} else if (element instanceof Applications) {
				return messages.applicationsNode;
			} else if (element instanceof UserApplications) {
				return messages.userApplicationsNode;
			} else if (element instanceof UserApplication) {
				return ((UserApplication) element).getName();
			} else if (element instanceof ProjectApplications) {
				return messages.projectApplicationsNode;
			} else if (element instanceof ProjectApplication) {
				return ((ProjectApplication) element).getName();
			} else if (element instanceof StartApplications) {
				return messages.startApplicationsNode;
			} else if (element instanceof StartApplication) {
				return ((StartApplication) element).getName();
			} else if (element instanceof BaseApplications) {
				return messages.baseApplicationsNode;
			} else if (element instanceof BaseApplication) {
				return ((BaseApplication) element).getName();
			} else if (element instanceof Groups) {
				return messages.groupsNode;
			} else if (element instanceof UserGroupsModel) {
				return messages.userGroupsNode;
			} else if (element instanceof ProjectGroupsModel) {
				return messages.projectGroupsNode;
			} else if (element instanceof UserApplicationGroups) {
				return messages.userAppGroupsNode;
			} else if (element instanceof ProjectApplicationGroups) {
				return messages.projectAppGroupsNode;
			} else if (element instanceof Directories) {
				return messages.directoryNode;
			} else if (element instanceof Configurations) {
				return messages.configurationNode;
			} else if (element instanceof Icons) {
				return messages.iconsNode;
			} else if (element instanceof Roles) {
				return messages.rolesNode;
			} else if (element instanceof SmtpConfiguration) {
				return messages.smtpConfigNode;
			} else if (element instanceof LdapConfiguration) {
				return messages.ldapConfigNode;
			} else if (element instanceof SingletonAppTimeConfig) {
				return messages.singletonAppTimeNode;
			} else if (element instanceof NoProjectPopMessage) {
				return messages.noPopMessageNode;
			} else if (element instanceof LiveMessages) {
				return messages.liveMsgConfigNode;
			} else if (element instanceof Notifications) {
				return messages.notificationNode;
			} else if (element instanceof ProjectExpiryConfig) {
				return messages.projectExpiryNode;
			} else if (element instanceof SiteAdminAreaProjects) {
				return messages.projectsNode;
			} else if (element instanceof SiteAdministrations) {
				return messages.administrationAreasNode;
			} else if (element instanceof SiteAdminAreaUserApplications) {
				return messages.userApplicationsNode;
			} else if (element instanceof SiteAdminAreaUserAppFixed) {
				return messages.fixedNode;
			} else if (element instanceof SiteAdminAreaUserAppNotFixed) {
				return messages.notFixedNode;
			} else if (element instanceof SiteAdminAreaUserAppProtected) {
				return messages.protectedNode;
			} else if (element instanceof SiteAdminProjectAppProtected) {
				return messages.protectedNode;
			} else if (element instanceof SiteAdminProjectAppNotFixed) {
				return messages.notFixedNode;
			} else if (element instanceof SiteAdminProjectAppFixed) {
				return messages.fixedNode;
			} else if (element instanceof SiteAdminAreaProjectApplications) {
				return messages.projectApplicationsNode;
			} else if (element instanceof SiteAdminAreaStartApplications) {
				return messages.startApplicationsNode;
			} else if (element instanceof SiteAdminAreaProjectStartApplications) {
				return messages.startApplicationsNode;
			} else if (element instanceof SiteAdminAreaInformations) {
				return messages.informationNode;
			} else if (element instanceof SiteAdminAreaInformationHistory) {
				return messages.historyNode;
			} else if (element instanceof SiteAdminAreaInformationStatus) {
				return messages.statusNode;
			} else if (element instanceof AdminAreaProjects) {
				return messages.projectsNode;
			} else if (element instanceof AdminAreaProjectApplications) {
				return messages.projectApplicationsNode;
			} else if (element instanceof AdminAreaProjectAppNotFixed) {
				return messages.notFixedNode;
			} else if (element instanceof AdminAreaProjectAppFixed) {
				return messages.fixedNode;
			} else if (element instanceof AdminAreaProjectAppProtected) {
				return messages.protectedNode;
			} else if (element instanceof AdminAreaStartApplications) {
				return messages.startApplicationsNode;
			} else if (element instanceof AdminAreaProjectStartApplications) {
				return messages.startApplicationsNode;
			} else if (element instanceof AdminAreaUserApplications) {
				return messages.userApplicationsNode;
			} else if (element instanceof AdminAreaUserAppFixed) {
				return messages.fixedNode;
			} else if (element instanceof AdminAreaUserAppNotFixed) {
				return messages.notFixedNode;
			} else if (element instanceof AdminAreaUserAppProtected) {
				return messages.protectedNode;
			} else if (element instanceof AdminAreaInfo) {
				return messages.informationNode;
			} else if (element instanceof AdminAreaInfoHistory) {
				return messages.historyNode;
			} else if (element instanceof AdminAreaInfoStatus) {
				return messages.statusNode;
			} else if (element instanceof AdminAreaStartApp) {
				return messages.startApplicationsNode;
			} else if (element instanceof ProjectUsers) {
				return messages.usersNode;
			} else if (element instanceof ProjectProjectApplications) {
				return messages.projectApplicationsNode;
			} else if (element instanceof UserGroupUsers) {
				return messages.usersNode;
			} else if (element instanceof ProjectInformation) {
				return messages.informationNode;
			} else if (element instanceof ProjectInfoStatus) {
				return messages.statusNode;
			} else if (element instanceof ProjectInfoHistory) {
				return messages.historyNode;
			} else if (element instanceof UserProjects) {
				return messages.projectsNode;
			} else if (element instanceof UserAAUserApplications) {
				return messages.userApplicationsNode;
			} else if (element instanceof UserStartApplications) {
				return messages.startApplicationsNode;
			} else if (element instanceof UserGroups) {
				return messages.groupsNode;
			} else if (element instanceof UserInformations) {
				return messages.informationNode;
			} else if (element instanceof UserInformationStatus) {
				return messages.statusNode;
			} else if (element instanceof UserInformationHistory) {
				return messages.historyNode;
			} else if (element instanceof UserAAUserAppAllowed) {
				return messages.allowedNode;
			} else if (element instanceof UserAAUserAppForbidden) {
				return messages.forbiddenNode;
			} else if (element instanceof ProjectAdminAreas) {
				return messages.administrationAreasNode;
			} else if (element instanceof ProjectAdminAreaProjectApplications) {
				return messages.projectApplicationsNode;
			} else if (element instanceof ProjectAdminAreaProjectAppNotFixed) {
				return messages.notFixedNode;
			} else if (element instanceof ProjectAdminAreaProjectAppFixed) {
				return messages.fixedNode;
			} else if (element instanceof ProjectAdminAreaProjectAppProtected) {
				return messages.protectedNode;
			} else if (element instanceof ProjectAdminAreaStartApplications) {
				return messages.startApplicationsNode;
			} else if (element instanceof UserProjectAdminAreas) {
				return messages.administrationAreasNode;
			} else if (element instanceof UserProjectAAProjectApplications) {
				return messages.projectApplicationsNode;
			} else if (element instanceof UserProjectAAProjectAppAllowed) {
				return messages.allowedNode;
			} else if (element instanceof UserProjectAAProjectAppForbidden) {
				return messages.forbiddenNode;
			} else if (element instanceof UserAdminAreas) {
				return messages.administrationAreasNode;
			} else if (element instanceof UserAppAdminAreas) {
				return messages.administrationAreasNode;
			} else if (element instanceof UserAppUsers) {
				return messages.usersNode;
			} else if (element instanceof ProjectAppAdminAreas) {
				return messages.administrationAreasNode;
			} else if (element instanceof ProjectAppAdminAreaProjects) {
				return messages.projectsNode;
			} else if (element instanceof ProjectAppUsers) {
				return messages.usersNode;
			} else if (element instanceof StartAppAdminAreas) {
				return messages.administrationAreasNode;
			} else if (element instanceof StartAppProjects) {
				return messages.projectsNode;
			} else if (element instanceof StartAppUsers) {
				return messages.usersNode;
			} else if (element instanceof BaseAppProjectApplications) {
				return messages.projectApplicationsNode;
			} else if (element instanceof BaseAppUserApplications) {
				return messages.userApplicationsNode;
			} else if (element instanceof BaseAppStartApplications) {
				return messages.startApplicationsNode;
			} else if (element instanceof ProjectGroupProjects) {
				return messages.projectsNode;
			} else if (element instanceof UserAppGroupUserApps) {
				return messages.userApplicationsNode;
			} else if (element instanceof ProjectAppGroupProjectApps) {
				return messages.projectApplicationsNode;
			} else if (element instanceof DirectoryUsers) {
				return messages.usersNode;
			} else if (element instanceof DirectoryProjects) {
				return messages.projectsNode;
			} else if (element instanceof DirectoryApplications) {
				return messages.applicationsNode;
			} else if (element instanceof DirectoryUserApplications) {
				return messages.userApplicationsNode;
			} else if (element instanceof DirectoryProjectApplications) {
				return messages.projectApplicationsNode;
			} else if (element instanceof AdminMenu) {
				return messages.adminMenuNode;
			} else if (element instanceof RoleScopeObjects) {
				return messages.administrationAreasNode;
			} else if (element instanceof RoleScopeObjectUsers) {
				return messages.usersNode;
			} else if (element instanceof RoleUsers) {
				return messages.usersNode;
			} else if (element instanceof AdminUsers) {
				return messages.usersNode;
			} else if (element instanceof AdminUserApps) {
				return messages.userApplicationsNode;
			} else if (element instanceof AdminProjectApps) {
				return messages.projectApplicationsNode;
			}

		}
		return CommonConstants.EMPTY_STR;
	}

	/**
	 * Sets the previous selection.
	 *
	 * @param previousSelection
	 *            the new previous selection
	 */
	public void setPreviousSelection(IStructuredSelection previousSelection) {
		this.previousSelection = previousSelection;
	}
	
	/**
	 * Gets the pervious selection.
	 *
	 * @return the pervious selection
	 */
	public IStructuredSelection getPerviousSelection() {
		return this.previousSelection;
	}
	
	/**
	 * Refersh back reference.
	 *
	 * @param classType the class type
	 */
	public void refershBackReference(final Class<IAdminTreeChild>[] classType) {
		final Map<Class<IAdminTreeChild>, List<Widget>> backRefMap = findItemsByType(classType);
		final Set<Class<IAdminTreeChild>> keySet = backRefMap.keySet();
		for (Class<IAdminTreeChild> type : keySet) {
			List<Widget> widgetList = backRefMap.get(type);
			Object data;
			for (Widget widget : widgetList) {
				if ((data = widget.getData()) != null/* && this.getExpandedState(data)*/) {
					if (data instanceof ProjectAdminAreas) {
						new AdminTreeExpansion(((ProjectAdminAreas) data).getParent()).loadObjects();
					} else {
						new AdminTreeExpansion(data).loadObjects();
					}
				}
			}
		}
	}

	/**
	 * Find items by type.
	 *
	 * @param classType the class type
	 * @return the map
	 */
	private Map<Class<IAdminTreeChild>, List<Widget>> findItemsByType(Class<IAdminTreeChild>[] classType) {
		// compare with rootbackRefMap
		Object root = getRoot();
		if (root == null) {
			return null;
		}
		Map<Class<IAdminTreeChild>, List<Widget>> backRefMap = new HashMap<>();
		for (Class<IAdminTreeChild> type : classType) {
			Item[] items = getChildren(getControl());
			if (items != null) {
				for (int i = 0; i < items.length; i++) {
					Widget o = internalFindItemByType(items[i], type, backRefMap);
					if (o != null) {
						if (backRefMap.get(type) == null) {
							backRefMap.put(type, new ArrayList<>());
						}
						backRefMap.get(type).add(o);
					}
				}
			}
		}
		return backRefMap;
	}
	
	/**
	 * Internal find item by type.
	 *
	 * @param parent the parent
	 * @param type the type
	 * @return the widget
	 */
	private Widget internalFindItemByType(final Item parent, final Class<IAdminTreeChild> type, Map<Class<IAdminTreeChild>, List<Widget>> backRefMap) {
		// compare with node
		Object data = parent.getData();
		if (data != null) {
			if (data.getClass() == type) {
				return parent;
			}
			if (data instanceof RelationObj) {
				if (((RelationObj) data).getContainerObj().getClass() == type) {
					return parent;
				}
			}
		}
		// recurse over children
		Item[] items = getChildren(parent);
		for (int i = 0; i < items.length; i++) {
			final Item item = items[i];
			final Widget o = internalFindItemByType(item, type, backRefMap);
			if (o != null) {
				if (backRefMap.get(type) == null) {
					backRefMap.put(type, new ArrayList<>());
				}
				backRefMap.get(type).add(o);
				//return o;
			}
		}
		return null;
	}
}
