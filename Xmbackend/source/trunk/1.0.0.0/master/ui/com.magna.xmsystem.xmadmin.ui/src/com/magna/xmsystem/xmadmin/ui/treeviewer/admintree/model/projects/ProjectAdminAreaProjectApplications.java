package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.projectapplicationmodel.ProjectApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.refs.RelationObj;

/**
 * Class for Project admin area project applications.
 *
 * @author Chiranjeevi.Akula
 */
public class ProjectAdminAreaProjectApplications implements IAdminTreeChild {

	/** Member variable 'parent' for {@link IAdminTreeChild}. */
	private IAdminTreeChild parent;

	/** Member variable 'project admin area project app children' for {@link Map<String,IAdminTreeChild>}. */
	private Map<String, IAdminTreeChild> projectAdminAreaProjectAppChildren;

	/**
	 * Constructor for ProjectAdminAreaProjectApplications Class.
	 *
	 * @param parent {@link IAdminTreeChild}
	 */
	public ProjectAdminAreaProjectApplications(final IAdminTreeChild parent) {
		this.parent = parent;
		this.projectAdminAreaProjectAppChildren = new LinkedHashMap<>();
		this.addFixedChildren();
	}
	
	/**
	 * Method for Adds the fixed children.
	 */
	public void addFixedChildren() {
		projectAdminAreaProjectAppChildren.put(ProjectAdminAreaProjectAppNotFixed.class.getName(), new ProjectAdminAreaProjectAppNotFixed(
				this));
		projectAdminAreaProjectAppChildren.put(ProjectAdminAreaProjectAppFixed.class.getName(), new ProjectAdminAreaProjectAppFixed(
				this));
		projectAdminAreaProjectAppChildren.put(ProjectAdminAreaProjectAppProtected.class.getName(), new ProjectAdminAreaProjectAppProtected(
				this));
	}

	/**
	 * Method for Adds the.
	 *
	 * @param adminAreasChildProAppChildId {@link String}
	 * @param child {@link IAdminTreeChild}
	 * @return the i admin tree child {@link IAdminTreeChild}
	 */
	public IAdminTreeChild add(final String projectAdminAreasChildProAppChildId, final IAdminTreeChild child) {
		child.setParent(this);
		IAdminTreeChild returnVal = this.projectAdminAreaProjectAppChildren.put(projectAdminAreasChildProAppChildId, child);
		if (child instanceof RelationObj && ((RelationObj) child).getRefObject() instanceof ProjectApplication) {
			sort();
		}
		return returnVal;
	}

	/**
	 * Method for Removes the.
	 *
	 * @param adminAreasChildProAppChildId {@link String}
	 * @return the i admin tree child {@link IAdminTreeChild}
	 */
	public IAdminTreeChild remove(final String projectAdminAreasChildProAppChildId) {
		return this.projectAdminAreaProjectAppChildren.remove(projectAdminAreasChildProAppChildId);
	}

	/**
	 * Gets the project admin area project app collection.
	 *
	 * @return the project admin area project app collection
	 */
	public Collection<IAdminTreeChild> getProjectAdminAreaProjectAppCollection() {
		return this.projectAdminAreaProjectAppChildren.values();
	}

	/**
	 * Gets the project admin area project app child.
	 *
	 * @return the project admin area project app child
	 */
	public Map<String, IAdminTreeChild> getProjectAdminAreaProjectAppChild() {
		return projectAdminAreaProjectAppChildren;
	}
	
	/* (non-Javadoc)
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#getParent()
	 */
	@Override
	public IAdminTreeChild getParent() {
		return this.parent;
	}

	/* (non-Javadoc)
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#setParent(com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild)
	 */
	@Override
	public void setParent(final IAdminTreeChild parent) {
		this.parent = parent;
	}
	
	/**
	 * Sort.
	 */
	public void sort() {
		final LinkedHashMap<String, IAdminTreeChild> collect = this.projectAdminAreaProjectAppChildren.entrySet().stream().sorted(
				(e1, e2) -> ((ProjectApplication) (((RelationObj) e1.getValue()).getRefObject())).getName()
				.compareTo(((ProjectApplication) (((RelationObj) e2.getValue()).getRefObject())).getName()))
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
		this.projectAdminAreaProjectAppChildren = collect;
	}

}
