package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.projectapplicationmodel;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.refs.RelationObj;

/**
 * Class for Project app admin area child.
 *
 * @author Chiranjeevi.Akula
 */
public class ProjectAppAdminAreaChild implements IAdminTreeChild {

	/** Member variable 'parent' for {@link IAdminTreeChild}. */
	private IAdminTreeChild parent;

	/** Member variable 'project app admin children' for {@link Map<String,IAdminTreeChild>}. */
	final private Map<String, IAdminTreeChild> projectAppAdminAreaChildren;

	/**
	 * Constructor for ProjectAppAdminAreaChild Class.
	 *
	 * @param parent {@link IAdminTreeChild}
	 */
	public ProjectAppAdminAreaChild(final IAdminTreeChild parent) {
		this.parent = parent;
		this.projectAppAdminAreaChildren = new LinkedHashMap<>();
	}

	/**
	 * Method for Adds the fixed children.
	 *
	 * @param relationObj {@link RelationObj}
	 */
	public void addFixedChildren(RelationObj relationObj) {
		projectAppAdminAreaChildren.put(ProjectAppAdminAreaProjects.class.getName(), new ProjectAppAdminAreaProjects(relationObj));
	}

	/**
	 * Gets the project app admin children collection.
	 *
	 * @return the project app admin children collection
	 */
	public Collection<IAdminTreeChild> getProjectAppAdminAreaChildrenCollection() {
		return this.projectAppAdminAreaChildren.values();
	}

	/**
	 * Gets the project app admin children.
	 *
	 * @return the project app admin children
	 */
	public Map<String, IAdminTreeChild> getProjectAppAdminAreaChildren() {
		return projectAppAdminAreaChildren;
	}

	/* (non-Javadoc)
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#getParent()
	 */
	@Override
	public IAdminTreeChild getParent() {
		return this.parent;
	}

	/* (non-Javadoc)
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#setParent(com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild)
	 */
	@Override
	public void setParent(IAdminTreeChild parent) {
		this.parent = parent;
		
	}


}
