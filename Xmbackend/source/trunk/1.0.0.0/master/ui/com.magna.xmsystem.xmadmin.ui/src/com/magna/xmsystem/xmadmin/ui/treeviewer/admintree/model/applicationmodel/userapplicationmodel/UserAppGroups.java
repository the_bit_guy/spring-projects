package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.userapplicationmodel;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;

/**
 * Class for User app groups.
 *
 * @author Chiranjeevi.Akula
 */
public class UserAppGroups implements IAdminTreeChild {

	/** Member variable 'parent' for {@link IAdminTreeChild}. */
	private IAdminTreeChild parent;

	/** Member variable 'user app group children' for {@link Map<String,IAdminTreeChild>}. */
	final private Map<String, IAdminTreeChild> userAppGroupChildren;

	/**
	 * Constructor for UserAppGroups Class.
	 *
	 * @param parent {@link IAdminTreeChild}
	 */
	public UserAppGroups(final IAdminTreeChild parent) {
		this.parent = parent;
		this.userAppGroupChildren = new LinkedHashMap<>();
	}

	/**
	 * Method for Adds the.
	 *
	 * @param userAppGroupId {@link String}
	 * @param child {@link IAdminTreeChild}
	 * @return the i admin tree child {@link IAdminTreeChild}
	 */
	public IAdminTreeChild add(final String userAppGroupId, final IAdminTreeChild child) {
		child.setParent(this);
		return this.userAppGroupChildren.put(userAppGroupId, child);
	}

	/**
	 * Method for Removes the.
	 *
	 * @param userAppGroupId {@link String}
	 * @return the i admin tree child {@link IAdminTreeChild}
	 */
	public IAdminTreeChild remove(final String userAppGroupId) {
		return this.userAppGroupChildren.remove(userAppGroupId);
	}

	/**
	 * Gets the user app group collection.
	 *
	 * @return the user app group collection
	 */
	public Collection<IAdminTreeChild> getUserAppGroupCollection() {
		return this.userAppGroupChildren.values();
	}

	/**
	 * Gets the user app group.
	 *
	 * @return the user app group
	 */
	public Map<String, IAdminTreeChild> getUserAppGroup() {
		return userAppGroupChildren;
	}
	
	/* (non-Javadoc)
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#getParent()
	 */
	@Override
	public IAdminTreeChild getParent() {
		return this.parent;
	}

	/* (non-Javadoc)
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#setParent(com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild)
	 */
	@Override
	public void setParent(final IAdminTreeChild parent) {
		this.parent = parent;
	}

}
