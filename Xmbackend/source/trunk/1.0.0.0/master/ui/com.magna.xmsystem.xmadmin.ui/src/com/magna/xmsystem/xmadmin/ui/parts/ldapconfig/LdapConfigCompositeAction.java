package com.magna.xmsystem.xmadmin.ui.parts.ldapconfig;

import java.util.EnumMap;
import java.util.List;

import javax.inject.Inject;

import org.eclipse.core.databinding.Binding;
import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.beans.BeanProperties;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.core.databinding.observable.value.IValueChangeListener;
import org.eclipse.core.databinding.observable.value.ValueChangeEvent;
import org.eclipse.core.runtime.preferences.IEclipsePreferences;
import org.eclipse.e4.core.di.extensions.Preference;
import org.eclipse.e4.core.services.nls.Translation;
import org.eclipse.e4.ui.model.application.ui.MDirtyable;
import org.eclipse.jface.databinding.fieldassist.ControlDecorationSupport;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.ResourceAccessException;

import com.magna.xmbackend.entities.PropertyConfigTbl;
import com.magna.xmbackend.vo.enums.LdapKey;
import com.magna.xmsystem.dependencies.customExceptions.UnauthorizedAccessException;
import com.magna.xmsystem.dependencies.utils.XMSystemUtil;
import com.magna.xmsystem.ui.controls.widgets.CustomMessageDialog;
import com.magna.xmsystem.xmadmin.message.Message;
import com.magna.xmsystem.xmadmin.message.MessageRegistry;
import com.magna.xmsystem.xmadmin.restclient.propertyConfig.PropertyConfigController;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.AdminTreeviewer;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.ldapconfig.LdapConfiguration;
import com.magna.xmsystem.xmadmin.ui.validation.NameValidation;
import com.magna.xmsystem.xmadmin.ui.validation.StatusValidation;
import com.magna.xmsystem.xmadmin.util.CommonConstants;
import com.magna.xmsystem.xmadmin.util.MessageType;
import com.magna.xmsystem.xmadmin.util.XMAdminUtil;

// TODO: Auto-generated Javadoc
/**
 * The Class LdapConfigCompositeAction.
 * 
 * @author archita.patel
 */
@SuppressWarnings("restriction")
public class LdapConfigCompositeAction extends LdapConfigCompositeUI {

	/** Logger instance. */
	private static final Logger LOGGER = LoggerFactory.getLogger(LdapConfigCompositeAction.class);

	/** Member variable for widgetValue. */
	transient private IObservableValue<?> widgetValue;

	/** Member variable for modelValue. */
	transient private IObservableValue<?> modelValue;

	/** Member variable for binding. */
	transient private Binding bindValue;

	/**
	 * Member variable for data binding context the DataBindingContext object
	 * will manage the databindings.
	 */
	final transient DataBindingContext dataBindContext = new DataBindingContext();

	/** Member variable for {@link MessageRegistry}. */
	@Inject
	private MessageRegistry registry;

	/** Member variable for messages. */
	@Inject
	@Translation
	transient private Message messages;

	/** The ldap configuration model. */
	private LdapConfiguration ldapConfigModel;

	/** The old model. */
	private LdapConfiguration oldModel;

	/** Member variable for dirty. */
	transient private MDirtyable dirty;

	/** The ldap key value. */
	private EnumMap<LdapKey, String> ldapKeyValue;

	/**
	 * Instantiates a new ldap config composite action.
	 *
	 * @param parent
	 *            the parent
	 * @param preferences
	 *            the preferences
	 */
	@Inject
	public LdapConfigCompositeAction(final Composite parent, @Preference IEclipsePreferences preferences) {
		super(parent, SWT.NONE);
		initListener();
	}

	/**
	 * Inits the listener.
	 */
	private void initListener() {
		if (this.btnSave != null) {
			this.btnSave.addSelectionListener(new SelectionAdapter() {

				/**
				 * Save button handler
				 */
				@Override
				public void widgetSelected(final SelectionEvent event) {
					saveLdapConfiguration();
				}
			});
		}
		if (this.btnCancel != null) {
			this.btnCancel.addSelectionListener(new SelectionAdapter() {

				/**
				 * Save button handler
				 */
				@Override
				public void widgetSelected(final SelectionEvent event) {
					cancelLdapConfiguration();
				}
			});
		}
	}

	/**
	 * Cancel ldap configuration.
	 */
	public void cancelLdapConfiguration() {
		if (this.ldapConfigModel == null) {
			dirty.setDirty(false);
			return;
		}
		setLdapConfigModel(null);
		setOldModel(null);
		dirty.setDirty(false);
		final AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
		final IStructuredSelection selection = (IStructuredSelection) adminTree.getSelection();
		final Object firstElement = selection.getFirstElement();
		adminTree.setSelection(new StructuredSelection(firstElement), true);
	}

	/**
	 * Save ldap configuration.
	 */
	public void saveLdapConfiguration() {
		try {
			PropertyConfigController propertyConfigController = new PropertyConfigController();
			boolean isUpdate = propertyConfigController.updatePropertyValueLdap(mapVOObjectWithModel());
			if (isUpdate) {
				setOldModel(ldapConfigModel.deepCopyLdapConfigModel(true, getOldModel()));
				this.ldapConfigModel.setOperationMode(CommonConstants.OPERATIONMODE.VIEW);
				setOperationMode();
				this.dirty.setDirty(false);
				XMAdminUtil.getInstance().getAdminTree().refresh();
				XMAdminUtil.getInstance().getAdminTree().setSelection(new StructuredSelection(getOldModel()), true);
				XMAdminUtil.getInstance().updateLogFile(messages.ldapConfigNode + " " + messages.objectUpdate,
						MessageType.SUCCESS);
			}
		} catch (UnauthorizedAccessException e) {
			CustomMessageDialog.openError(this.getShell(), messages.objectPermissionDialogTitle,
					messages.objectPermissionDialogMsg);
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				CustomMessageDialog.openError(Display.getCurrent().getActiveShell(), messages.errorDialogTitile,
						messages.serverNotReachable);
			}
			LOGGER.error("Exception occured while saving ldap configuration", e);
		}
	}

	private com.magna.xmbackend.vo.propConfig.LdapConfigRequest mapVOObjectWithModel() {
		com.magna.xmbackend.vo.propConfig.LdapConfigRequest ldapConfigRequest = new com.magna.xmbackend.vo.propConfig.LdapConfigRequest();
		ldapKeyValue = new EnumMap<LdapKey, String>(LdapKey.class);
		LdapKey ldapUrl = LdapKey.LDAP_URL;
		LdapKey ldapPortNumber = LdapKey.LDAP_PORT_NUMBER;
		LdapKey ldapuserName = LdapKey.LDAP_USERNAME;
		LdapKey ldappassword = LdapKey.LDAP_PASSWORD;
		LdapKey ldapbase = LdapKey.LDAP_BASE;
		ldapKeyValue.put(ldapUrl, ldapConfigModel.getLdapUrl());
		ldapKeyValue.put(ldapPortNumber, ldapConfigModel.getLdapPort());
		ldapKeyValue.put(ldapuserName, ldapConfigModel.getLdapUserName());
		ldapKeyValue.put(ldappassword, ldapConfigModel.getLdapPassword());
		ldapKeyValue.put(ldapbase, ldapConfigModel.getLdapBase());
		ldapConfigRequest.setLdapKeyValue(ldapKeyValue);
		return ldapConfigRequest;

	}

	/**
	 * Register messages.
	 *
	 * @param registry
	 *            the registry
	 */
	public void registerMessages(final MessageRegistry registry) {
		registry.register((text) -> {
			if (grpLdapConfig != null && !grpLdapConfig.isDisposed()) {
				grpLdapConfig.setText(text);
			}
		}, (message) -> {
			return message.ldapGroupLabel;
		});

		registry.register((text) -> {
			if (lblLdapUrl != null && !lblLdapUrl.isDisposed()) {
				lblLdapUrl.setText(text);
			}
		}, (message) -> {
			if (lblLdapUrl != null && !lblLdapUrl.isDisposed()) {
				return getUpdatedWidgetText(message.ldapUrllabel, lblLdapUrl);
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (lblLdapPort != null && !lblLdapPort.isDisposed()) {
				lblLdapPort.setText(text);
			}
		}, (message) -> {
			if (lblLdapPort != null && !lblLdapPort.isDisposed()) {
				return getUpdatedWidgetText(message.ldapPortLabel, lblLdapPort);
			}
			return CommonConstants.EMPTY_STR;
		});
		
		registry.register((text) -> {
			if (lblLdapUserName!= null && !lblLdapUserName.isDisposed()) {
				lblLdapUserName.setText(text);
			}
		}, (message) -> {
			if (lblLdapUserName != null && !lblLdapUserName.isDisposed()) {
				return getUpdatedWidgetText(message.ldapUserNameLabel, lblLdapUserName);
			}
			return CommonConstants.EMPTY_STR;
		});
		
		registry.register((text) -> {
			if (lblLdapPassword != null && !lblLdapPassword.isDisposed()) {
				lblLdapPassword.setText(text);
			}
		}, (message) -> {
			if (lblLdapPassword != null && !lblLdapPassword.isDisposed()) {
				return getUpdatedWidgetText(message.ldapPasswordlabel, lblLdapPassword);
			}
			return CommonConstants.EMPTY_STR;
		});
		
		registry.register((text) -> {
			if (lblLdapBase != null && !lblLdapBase.isDisposed()) {
				lblLdapBase.setText(text);
			}
		}, (message) -> {
			if (lblLdapBase != null && !lblLdapBase.isDisposed()) {
				return getUpdatedWidgetText(message.ldapBaselabel, lblLdapBase);
			}
			return CommonConstants.EMPTY_STR;
		});
		
		registry.register((text) -> {
			if (lblMsgConfig != null && !lblMsgConfig.isDisposed()) {
				lblMsgConfig.setText(text);
			}
		}, (message) -> {
			if (lblMsgConfig != null && !lblMsgConfig.isDisposed()) {
				return getUpdatedWidgetText(message.msglabel, lblMsgConfig);
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (btnSave != null && !btnSave.isDisposed()) {
				btnSave.setText(text);
			}
		}, (message) -> {
			if (btnSave != null && !btnSave.isDisposed()) {
				return getUpdatedWidgetText(message.saveButtonText, btnSave);
			}
			return CommonConstants.EMPTY_STR;
		});
		registry.register((text) -> {
			if (btnCancel != null && !btnCancel.isDisposed()) {
				btnCancel.setText(text);
			}
		}, (message) -> {
			if (btnCancel != null && !btnCancel.isDisposed()) {
				return getUpdatedWidgetText(message.cancelButtonText, btnCancel);
			}
			return CommonConstants.EMPTY_STR;
		});
	}

	/**
	 * Bind values.
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void bindValues() {

		widgetValue = WidgetProperties.text(SWT.Modify).observe(this.txtLdapUrl);
		modelValue = BeanProperties.value(LdapConfiguration.class, LdapConfiguration.PROPERTY_LDAP_URL)
				.observe(this.ldapConfigModel);
		widgetValue.addValueChangeListener(new IValueChangeListener() {
			@Override
			public void handleValueChange(ValueChangeEvent event) {
				updateButtonStatus(event);
			}
		});
		UpdateValueStrategy update = new UpdateValueStrategy();
		update.setAfterGetValidator(new NameValidation(messages, StatusValidation.URL));
		bindValue = dataBindContext.bindValue(widgetValue, modelValue, update, null);
		ControlDecorationSupport.create(bindValue, SWT.TOP | SWT.LEFT);

		widgetValue = WidgetProperties.text(SWT.Modify).observe(this.txtLdapPort);
		modelValue = BeanProperties.value(LdapConfiguration.class, LdapConfiguration.PROPERTY_LDAP_PORT)
				.observe(this.ldapConfigModel);
		widgetValue.addValueChangeListener(new IValueChangeListener() {
			@Override
			public void handleValueChange(ValueChangeEvent event) {
				updateButtonStatus(event);
			}
		});
		update = new UpdateValueStrategy();
		update.setAfterGetValidator(new NameValidation(messages, StatusValidation.PORT_NUMBER));
		bindValue = dataBindContext.bindValue(widgetValue, modelValue, update, null);
		ControlDecorationSupport.create(bindValue, SWT.TOP | SWT.LEFT);
		//
		widgetValue = WidgetProperties.text(SWT.Modify).observe(this.txtLdapUserName);
		modelValue = BeanProperties.value(LdapConfiguration.class, LdapConfiguration.PROPERTY_LDAP_USERNAME)
				.observe(this.ldapConfigModel);
		widgetValue.addValueChangeListener(new IValueChangeListener() {
			@Override
			public void handleValueChange(ValueChangeEvent event) {
				updateButtonStatus(event);
			}
		});
		update = new UpdateValueStrategy();
		update.setAfterGetValidator(new NameValidation(messages, StatusValidation.LDAP_USERNAME));
		bindValue = dataBindContext.bindValue(widgetValue, modelValue, update, null);
		ControlDecorationSupport.create(bindValue, SWT.TOP | SWT.LEFT);
		//
		widgetValue = WidgetProperties.text(SWT.Modify).observe(this.txtLdapPassword);
		modelValue = BeanProperties.value(LdapConfiguration.class, LdapConfiguration.PROPERTY_LDAP_PASSWORD)
				.observe(this.ldapConfigModel);
		widgetValue.addValueChangeListener(new IValueChangeListener() {
			@Override
			public void handleValueChange(ValueChangeEvent event) {
				updateButtonStatus(event);
			}
		});
		update = new UpdateValueStrategy();
		update.setAfterGetValidator(new NameValidation(messages, StatusValidation.SMTP_PASSWORD));
		bindValue = dataBindContext.bindValue(widgetValue, modelValue, update, null);
		ControlDecorationSupport.create(bindValue, SWT.TOP | SWT.LEFT);
		//
		widgetValue = WidgetProperties.text(SWT.Modify).observe(this.txtLdapBase);
		modelValue = BeanProperties.value(LdapConfiguration.class, LdapConfiguration.PROPERTY_LDAP_BASE)
				.observe(this.ldapConfigModel);
		widgetValue.addValueChangeListener(new IValueChangeListener() {
			@Override
			public void handleValueChange(ValueChangeEvent event) {
				updateButtonStatus(event);
			}
		});
		update = new UpdateValueStrategy();
		update.setAfterGetValidator(new NameValidation(messages, StatusValidation.LDAP_BASE));
		bindValue = dataBindContext.bindValue(widgetValue, modelValue, update, null);
		ControlDecorationSupport.create(bindValue, SWT.TOP | SWT.LEFT);

	}

	/**
	 * Method to update the button status
	 * 
	 * @param event
	 */
	@SuppressWarnings("rawtypes")
	private void updateButtonStatus(ValueChangeEvent event) {
		final String urlLdapText = txtLdapUrl.getText().trim();
		final String portLdapText = txtLdapPort.getText().trim();
		final String userNameText = txtLdapUserName.getText().trim();
		final String ldapPassword = txtLdapPassword.getText().trim();
		final String ldapBase = txtLdapBase.getText().trim();
		if (this.btnSave != null) {
			if ((XMSystemUtil.isEmpty(urlLdapText) || urlLdapText.trim().length() == 0)
					|| (XMSystemUtil.isEmpty(portLdapText) || portLdapText.trim().length() == 0)
					|| (XMSystemUtil.isEmpty(userNameText) || userNameText.trim().length() == 0)
					|| (XMSystemUtil.isEmpty(ldapPassword) || ldapPassword.trim().length() == 0)
					|| (XMSystemUtil.isEmpty(ldapBase) || ldapBase.trim().length() == 0)
					|| (!portLdapText.matches(CommonConstants.RegularExpressions.PORT_NUMBER))
					|| (!urlLdapText.matches(CommonConstants.RegularExpressions.LDAP_URL))) {
				this.btnSave.setEnabled(false);
			} else {
				this.btnSave.setEnabled(true);
			}
		}

	}

	/**
	 * Sets the operation mode.
	 */
	public void setOperationMode() {
		if (this.ldapConfigModel != null) {
			if (ldapConfigModel.getOperationMode() == CommonConstants.OPERATIONMODE.VIEW) {
				this.txtLdapUrl.setEditable(false);
				this.txtLdapPort.setEditable(false);
				this.txtLdapUserName.setEditable(false);
				this.txtLdapPassword.setEditable(false);
				this.txtLdapBase.setEditable(false);
				setShowButtonBar(false);
			} else if (ldapConfigModel.getOperationMode() == CommonConstants.OPERATIONMODE.CHANGE) {
				this.txtLdapUrl.setEditable(true);
				this.txtLdapPort.setEditable(true);
				this.txtLdapUserName.setEditable(true);
				this.txtLdapPassword.setEditable(true);
				this.txtLdapBase.setEditable(true);
				setShowButtonBar(true);
				this.dirty.setDirty(true);
			} else {
				this.txtLdapUrl.setEditable(false);
				this.txtLdapPort.setEditable(false);
				this.txtLdapUserName.setEditable(false);
				this.txtLdapPassword.setEditable(false);
				this.txtLdapBase.setEditable(false);
				setShowButtonBar(false);
			}
		}
	}

	/**
	 * Sets the show button bar.
	 *
	 * @param showButtonBar
	 *            the new show button bar
	 */
	protected void setShowButtonBar(final boolean showButtonBar) {
		if (this.btnSave != null && !this.btnSave.isDisposed() && this.btnCancel != null
				&& !this.btnCancel.isDisposed()) {
			final GridData layoutData = (GridData) this.btnSave.getParent().getLayoutData();
			layoutData.exclude = !showButtonBar;
			this.btnSave.setVisible(showButtonBar);
			this.btnCancel.setVisible(showButtonBar);
			this.btnSave.getParent().requestLayout();
			this.btnSave.getParent().redraw();
			this.btnSave.getParent().getParent().update();
		}
	}

	/**
	 * Gets the updated widget text.
	 *
	 * @param message
	 *            the message
	 * @param control
	 *            the control
	 * @return the updated widget text
	 */
	private String getUpdatedWidgetText(final String message, final Control control) {
		control.requestLayout();
		control.getParent().redraw();
		control.getParent().getParent().update();
		control.getParent().getParent().getParent().update();
		return message;
	}

	/**
	 * Sets the ldap configuration.
	 */
	public void setLdapConfiguration() {
		final AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
		final Object selectionObj = adminTree.getSelection();
		if (selectionObj instanceof IStructuredSelection) {
			Object firstElement = ((IStructuredSelection) selectionObj).getFirstElement();
			if (firstElement instanceof LdapConfiguration) {
				ldapConfigModel = (LdapConfiguration) firstElement;
				PropertyConfigController propertyConfigController = new PropertyConfigController();
				List<PropertyConfigTbl> propVOList = propertyConfigController.getAllLDAPDetails(true);
				for (PropertyConfigTbl propertyTbl : propVOList) {
					String property = propertyTbl.getProperty();
					String propertyValue = propertyTbl.getValue();
					if (LdapKey.LDAP_URL.toString().equals(property)) {
						this.ldapConfigModel.setLdapUrl(propertyValue);

					} else if (LdapKey.LDAP_PORT_NUMBER.toString().equals(property)) {
						this.ldapConfigModel.setLdapPort(propertyValue);

					} else if(LdapKey.LDAP_USERNAME.toString().equals(property)){
						this.ldapConfigModel.setLdapUserName(propertyValue);
						
					} else if(LdapKey.LDAP_PASSWORD.toString().equals(property)){
						this.ldapConfigModel.setLdapPassword(propertyValue);
						
					} else if(LdapKey.LDAP_BASE.toString().equals(property)){
						this.ldapConfigModel.setLdapBase(propertyValue);
						
					}
				}
				setOldModel(ldapConfigModel);
				setLdapConfigModel(this.getOldModel().deepCopyLdapConfigModel(false, null));
				registerMessages(this.registry);
				bindValues();
				setOperationMode();
			}
		}
	}

	/**
	 * Sets the dirty object.
	 *
	 * @param dirty
	 *            the new dirty object
	 */
	public void setDirtyObject(final MDirtyable dirty) {
		this.dirty = dirty;
	}

	/**
	 * Gets the ldap config model.
	 *
	 * @return the ldap config model
	 */
	public LdapConfiguration getLdapConfigModel() {
		return ldapConfigModel;
	}

	/**
	 * Sets the ldap config model.
	 *
	 * @param ldapConfigModel
	 *            the new ldap config model
	 */
	public void setLdapConfigModel(LdapConfiguration ldapConfigModel) {
		this.ldapConfigModel = ldapConfigModel;
	}

	/**
	 * Gets the old model.
	 *
	 * @return the old model
	 */
	public LdapConfiguration getOldModel() {
		return oldModel;
	}

	/**
	 * Sets the old model.
	 *
	 * @param oldModel
	 *            the new old model
	 */
	public void setOldModel(final LdapConfiguration oldModel) {
		this.oldModel = oldModel;
	}
}
