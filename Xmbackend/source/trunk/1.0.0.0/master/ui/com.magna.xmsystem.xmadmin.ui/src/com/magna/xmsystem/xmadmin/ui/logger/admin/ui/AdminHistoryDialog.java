package com.magna.xmsystem.xmadmin.ui.logger.admin.ui;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;

import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.IJobManager;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.core.runtime.preferences.IEclipsePreferences;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.nebula.widgets.nattable.NatTable;
import org.eclipse.nebula.widgets.nattable.export.command.ExportCommand;
import org.eclipse.nebula.widgets.pgroup.PGroup;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.custom.StackLayout;
import org.eclipse.swt.events.ControlAdapter;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.events.ExpandEvent;
import org.eclipse.swt.events.ExpandListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.VerifyEvent;
import org.eclipse.swt.events.VerifyListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;
import org.osgi.service.prefs.BackingStoreException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.ResourceAccessException;

import com.magna.xmbackend.vo.adminHistory.AdminHistoryRequest;
import com.magna.xmsystem.dependencies.utils.LANG_ENUM;
import com.magna.xmsystem.dependencies.utils.XMSystemUtil;
import com.magna.xmsystem.ui.controls.widgets.CustomDateTime;
import com.magna.xmsystem.ui.controls.widgets.CustomMessageDialog;
import com.magna.xmsystem.xmadmin.message.Message;
import com.magna.xmsystem.xmadmin.message.MessageRegistry;
import com.magna.xmsystem.xmadmin.ui.logger.HistoryTableUtil;
import com.magna.xmsystem.xmadmin.util.CommonConstants;
import com.magna.xmsystem.xmadmin.util.XMAdminUtil;

/**
 * Class for Admin history dialog.
 *
 * @author subash.janarthanan
 * 
 */
public class AdminHistoryDialog extends Dialog {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(AdminHistoryDialog.class);

	/**
	 * Member variable 'relation objects history page' for {@link Composite}.
	 */
	private Composite relationObjectsHistoryPage;

	/** Member variable 'main data group' for {@link Group}. */
	private Group mainDataGroup;

	/** Member variable 'base objects history page' for {@link Composite}. */
	private Composite baseObjectsHistoryPage;

	/** Member variable 'txt admin namefiltertext' for {@link Text}. */
	private Text txtAdminNamefiltertext;

	/** Member variable 'txt object namefiltertext' for {@link Text}. */
	private Text txtObjectNamefiltertext;
	
	/** The txt result filter. */
	private Text txtResultFilter;

	/** Member variable 'txt limitfilter' for {@link Text}. */
	private Text txtLimitfilter;

	/** Member variable 'txt admim areafiltertext' for {@link Text}. */
	private Text txtAdmimAreafiltertext;

	/** Member variable 'txt projectfiltertext' for {@link Text}. */
	private Text txtProjectfiltertext;

	/** Member variable 'txt project applicationfiltertext' for {@link Text}. */
	private Text txtProjectApplicationfiltertext;

	/** Member variable 'txt user namefiltertext' for {@link Text}. */
	private Text txtUserNamefiltertext;

	/** Member variable 'txt start applicationfiltertext' for {@link Text}. */
	private Text txtStartApplicationfiltertext;
	
	/** The txt relation type filter. */
	private Text txtRelationTypeFilter;

	/** Member variable 'txt sitefiltertext' for {@link Text}. */
	private Text txtSitefiltertext;

	/** Member variable 'txt group namefiltertext' for {@link Text}. */
	private Text txtGroupNamefiltertext;

	/** Member variable 'txt user applicationfiltertext' for {@link Text}. */
	private Text txtUserApplicationfiltertext;

	/** Member variable 'txt directoryfiltertext' for {@link Text}. */
	private Text txtDirectoryfiltertext;

	/** Member variable 'txt rolefiltertext' for {@link Text}. */
	private Text txtRolefiltertext;

	/** Member variable 'prefs' for {@link IEclipsePreferences}. */
	private IEclipsePreferences prefs;

	/**
	 * Member variable 'admin history relations nat table' for {@link NatTable}.
	 */
	private NatTable adminHistoryRelationsNatTable;

	/**
	 * Member variable 'admin history base object nat table' for
	 * {@link NatTable}.
	 */
	private NatTable adminHistoryBaseObjectNatTable;

	/**
	 * Member variable 'txt admin name base objectfiltertext' for {@link Text}.
	 */
	private Text txtAdminNameBaseObjectfiltertext;

	/**
	 * Member variable 'admin history relationship nat table container' for
	 * {@link AdminHistoryRelationsNattable}.
	 */
	private AdminHistoryRelationsNattable adminHistoryRelationshipNatTableContainer;

	/**
	 * Member variable 'admin history base object container' for
	 * {@link AdminHistoryBaseObjectNattable}.
	 */
	private AdminHistoryBaseObjectNattable adminHistoryBaseObjectContainer;

	/** Member variable 'txt base object limitfilter' for {@link Text}. */
	private Text txtBaseObjectLimitfilter;

	/** Member variable 'messages' for {@link Message}. */
	private Message messages;
	
	/** The registry. */
	private MessageRegistry registry;

	/** Member variable 'refresh tool item' for {@link ToolItem}. */
	private ToolItem refreshToolItem;

	/** Member variable 'excel export tool item' for {@link ToolItem}. */
	private ToolItem excelExportToolItem;

	private CustomDateTime adminHistroyRelStartDate;

	private CustomDateTime adminHistroyRelEndDate;

	private CustomDateTime adminHistroyBaseObjStartDate;

	/**
	 * Member variable 'admin histroy base obj end date' for {@link CDateTime}.
	 */
	private CustomDateTime adminHistroyBaseObjEndDate;

	/** Member variable 'parent shell' for {@link Shell}. */
	private Shell parentShell;

	/** Member variable 'relation history apply button' for {@link Button}. */
	private Button relationHistoryApplyButton;

	/** Member variable 'base history apply button' for {@link Button}. */
	private Button baseHistoryApplyButton;

	/**
	 * Constructor for AdminHistoryDialog Class.
	 *
	 * @param parentShell
	 *            {@link Shell}
	 * @param prefs
	 *            {@link IEclipsePreferences}
	 * @param messages
	 *            {@link Message}
	 */
	public AdminHistoryDialog(Shell parentShell, IEclipsePreferences prefs, MessageRegistry registry,Message messages) {
		super(parentShell);
		this.parentShell = parentShell;
		this.prefs = prefs;
		this.registry = registry;
		this.messages = messages;
		setShellStyle(SWT.CLOSE | SWT.TITLE | SWT.MODELESS | SWT.SHELL_TRIM | SWT.RESIZE);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.window.Window#configureShell(org.eclipse.swt.widgets.
	 * Shell)
	 */
	@Override
	protected void configureShell(Shell newShell) {
		registry.register((text) -> {
			if (newShell != null && !newShell.isDisposed()) {
				newShell.setText(text);
			}
		}, (message) -> {
			if (newShell != null && !newShell.isDisposed()) {
				return message.adminHistoryDialogTitle;
			}
			return CommonConstants.EMPTY_STR;
		});
		Image image = parentShell.getImage();
		if (image != null) {
			newShell.setImage(image);
		}
		super.configureShell(newShell);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.dialogs.Dialog#createDialogArea(org.eclipse.swt.widgets
	 * .Composite)
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		Composite container = (Composite) super.createDialogArea(parent);
		container.setLayout(new GridLayout());

		final StackLayout stackLayout = new StackLayout();

		final Composite headerComposite = new Composite(container, SWT.NULL);
		final GridLayout headerGridLayout = new GridLayout(3, false);
		headerGridLayout.marginLeft = 10;
		headerGridLayout.marginRight = 10;
		headerGridLayout.marginTop = 0;
		headerGridLayout.marginBottom = 0;
		headerGridLayout.marginHeight = 0;
		headerGridLayout.marginWidth = 0;

		headerComposite.setLayout(headerGridLayout);
		headerComposite.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false));

		Button btnRelationHistory = new Button(headerComposite, SWT.RADIO);
		registry.register((text) -> {
			if (btnRelationHistory != null && !btnRelationHistory.isDisposed()) {
				btnRelationHistory.setText(text);
			}
		}, (message) -> {
			if (btnRelationHistory != null && !btnRelationHistory.isDisposed()) {
				return getUpdatedWidgetText(message.adminHistoryRelRadioBtn, btnRelationHistory);
			}
			return CommonConstants.EMPTY_STR;
		});

		btnRelationHistory.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				stackLayout.topControl = relationObjectsHistoryPage;
				mainDataGroup.layout();

			}
		});
		btnRelationHistory.setData("org.eclipse.e4.ui.css.id", "HistoryRadioButton");
		btnRelationHistory.setSelection(true);

		Button btnBaseDataHistory = new Button(headerComposite, SWT.RADIO);
		registry.register((text) -> {
			if (btnBaseDataHistory != null && !btnBaseDataHistory.isDisposed()) {
				btnBaseDataHistory.setText(text);
			}
		}, (message) -> {
			if (btnBaseDataHistory != null && !btnBaseDataHistory.isDisposed()) {
				return getUpdatedWidgetText(message.adminHistoryBaseObjBtn, btnBaseDataHistory);
			}
			return CommonConstants.EMPTY_STR;
		});

		btnBaseDataHistory.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				stackLayout.topControl = baseObjectsHistoryPage;
				mainDataGroup.layout();
			}
		});
		btnBaseDataHistory.setData("org.eclipse.e4.ui.css.id", "HistoryRadioButton");

		final ToolBar toolbar = new ToolBar(headerComposite, SWT.FLAT);
		toolbar.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, true, false, 1, 1));

		this.refreshToolItem = new ToolItem(toolbar, SWT.PUSH);
		this.refreshToolItem.setImage(XMSystemUtil.getInstance().getImage(this.getClass(), "icons/32x32/refresh_32.png"));
		//this.refreshToolItem.setToolTipText(messages.historyReload);
		registry.register((text) -> {
			if (refreshToolItem != null && !refreshToolItem.isDisposed()) {
				refreshToolItem.setToolTipText(text);
			}
		}, (message) -> {
			if (refreshToolItem != null && !refreshToolItem.isDisposed()) {
				return message.historyReload;
			}
			return CommonConstants.EMPTY_STR;
		});
		this.refreshToolItem.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {

				if (stackLayout.topControl == relationObjectsHistoryPage) {
					relationHistoryApplyButton.notifyListeners(SWT.Selection, new Event());
				} else {
					baseHistoryApplyButton.notifyListeners(SWT.Selection, new Event());
				}
			}
		});

		this.excelExportToolItem = new ToolItem(toolbar, SWT.PUSH);
		this.excelExportToolItem.setImage(XMSystemUtil.getInstance().getImage(this.getClass(), "icons/32x32/excel_icon.png"));
		toolbar.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, true, false, 1, 1));
		//this.excelExportToolItem.setToolTipText(messages.historyExportBtn);
		registry.register((text) -> {
			if (excelExportToolItem != null && !excelExportToolItem.isDisposed()) {
				excelExportToolItem.setToolTipText(text);
			}
		}, (message) -> {
			if (excelExportToolItem != null && !excelExportToolItem.isDisposed()) {
				return message.historyExportBtn;
			}
			return CommonConstants.EMPTY_STR;
		});
		excelExportToolItem.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if (stackLayout.topControl == relationObjectsHistoryPage) {
					adminHistoryRelationsNatTable
							.doCommand(new ExportCommand(adminHistoryRelationsNatTable.getConfigRegistry(),
									adminHistoryRelationsNatTable.getShell()));
				} else {
					adminHistoryBaseObjectNatTable
							.doCommand(new ExportCommand(adminHistoryBaseObjectNatTable.getConfigRegistry(),
									adminHistoryBaseObjectNatTable.getShell()));
				}
			}
		});

		mainDataGroup = new Group(container, SWT.NONE);
		mainDataGroup.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 3, 1));
		mainDataGroup.setLayout(stackLayout);

		try {
			generateRelationObjectsHistoryTable();
			generateBaseObjectsHistoryTable();
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				Display.getDefault().asyncExec(new Runnable() {
					@Override
					public void run() {
						CustomMessageDialog.openError(Display.getDefault().getActiveShell(), "Error",
								"Server not reachable");
						close();
					}
				});
			}
			LOGGER.error("Exception occured while loading adminHistory",e.getMessage());
		}

		stackLayout.topControl = relationObjectsHistoryPage;
		mainDataGroup.layout();
		
		
		registry.register((text) -> {
		}, (message) -> {
			updateNatTableColumnLabels();
			return CommonConstants.EMPTY_STR;
		});
		

		return container;
	}

	
	/**
	 * Update nat table column labels.
	 */
	private void updateNatTableColumnLabels() {
		if (adminHistoryRelationsNatTable != null && !adminHistoryRelationsNatTable.isDisposed() && adminHistoryBaseObjectNatTable != null && !adminHistoryBaseObjectNatTable.isDisposed()) {
			HistoryTableUtil.getInstance().getAdminHistoryBaseObjectPropToLabelMap();
			HistoryTableUtil.getInstance().getAdminHistoryRelationsPropToLabelMap();
			adminHistoryRelationsNatTable.refresh();
			adminHistoryBaseObjectNatTable.refresh();
		}
	}

	/**
	 * Method for Creates the relation obj history filter comp.
	 *
	 * @param parent
	 *            {@link Composite}
	 */
	private void createRelationObjHistoryFilterComp(final Composite parent) {
		final PGroup pGroup = new PGroup(parent, SWT.SMOOTH);
		pGroup.addExpandListener(new PGroupExpandListener());

		final GridLayout widgetContLayout = new GridLayout(6, false);
		widgetContLayout.horizontalSpacing = 10;
		pGroup.setLayout(widgetContLayout);
		pGroup.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 3, 1));
		registry.register((text) -> {
			if (pGroup != null && !pGroup.isDisposed()) {
				pGroup.setText(text);
			}
		}, (message) -> {
			if (pGroup != null && !pGroup.isDisposed()) {
				return getUpdatedWidgetText(message.adminHistoryRelFilterLabel, pGroup);
			}
			return CommonConstants.EMPTY_STR;
		});

		// LogTime
		Label lblLogTime = new Label(pGroup, SWT.NONE);
		registry.register((text) -> {
			if (lblLogTime != null && !lblLogTime.isDisposed()) {
				lblLogTime.setText(text);
			}
		}, (message) -> {
			if (lblLogTime != null && !lblLogTime.isDisposed()) {
				return getUpdatedWidgetText(message.historyLogTimeLabel, lblLogTime);
			}
			return CommonConstants.EMPTY_STR;
		});
		
		lblLogTime.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));

		new Label(pGroup, SWT.NONE);

		Label lblCdate = new Label(pGroup, SWT.NONE);
		lblCdate.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false));
		registry.register((text) -> {
			if (lblCdate != null && !lblCdate.isDisposed()) {
				lblCdate.setText(text);
			}
		}, (message) -> {
			if (lblCdate != null && !lblCdate.isDisposed()) {
				return getUpdatedWidgetText(message.historyStartDateLabel, lblCdate);
			}
			return CommonConstants.EMPTY_STR;
		});
		this.adminHistroyRelStartDate = new CustomDateTime(pGroup);
		this.adminHistroyRelStartDate.setLayoutData(new GridData(SWT.FILL, SWT.LEFT, true, false));
		String stDate = prefs.get("adminHistory.startDate", "");

		if (!stDate.isEmpty()) {
			try {
				this.adminHistroyRelStartDate.setDate(XMSystemUtil.getHistoryLogTimeDateFormat().parse(stDate));
			} catch (ParseException e1) {
				LOGGER.error("Exception while parsing the date!" + e1);
			}
		}
		
		registry.register((text) -> {
		}, (message) -> {
			if(adminHistroyRelStartDate != null && !adminHistroyRelStartDate.isDisposed()){
			LANG_ENUM currentLocaleEnum = XMAdminUtil.getInstance().getCurrentLocaleEnum();
			adminHistroyRelStartDate.updateTooltipText(currentLocaleEnum.toString());
			}
			return CommonConstants.EMPTY_STR;
		});


		Label lblEndDate = new Label(pGroup, SWT.NONE);
		lblEndDate.setLayoutData(new GridData(SWT.BEGINNING, SWT.CENTER, false, false));
		registry.register((text) -> {
			if (lblEndDate != null && !lblEndDate.isDisposed()) {
				lblEndDate.setText(text);
			}
		}, (message) -> {
			if (lblEndDate != null && !lblEndDate.isDisposed()) {
				return getUpdatedWidgetText(message.historyEndDateLabel, lblEndDate);
			}
			return CommonConstants.EMPTY_STR;
		});
		this.adminHistroyRelEndDate = new CustomDateTime(pGroup);
		this.adminHistroyRelEndDate.setLayoutData(new GridData(SWT.FILL, SWT.LEFT, true, false));
		String eDate = prefs.get("adminHistory.endDate", "");

		if (!eDate.isEmpty()) {
			try {
				this.adminHistroyRelEndDate.setDate(XMSystemUtil.getHistoryLogTimeDateFormat().parse(eDate));
			} catch (ParseException e1) {
				LOGGER.error("Exception while parsing the date!" + e1);
			}
		}

		registry.register((text) -> {
		}, (message) -> {
			if(adminHistroyRelEndDate != null && !adminHistroyRelEndDate.isDisposed()){
			LANG_ENUM currentLocaleEnum = XMAdminUtil.getInstance().getCurrentLocaleEnum();
			adminHistroyRelEndDate.updateTooltipText(currentLocaleEnum.toString());
			}
			return CommonConstants.EMPTY_STR;
		});
		
		Label lblAdminName = new Label(pGroup, SWT.NONE);
		lblAdminName.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		registry.register((text) -> {
			if (lblAdminName != null && !lblAdminName.isDisposed()) {
				lblAdminName.setText(text);
			}
		}, (message) -> {
			if (lblAdminName != null && !lblAdminName.isDisposed()) {
				return getUpdatedWidgetText(message.adminHistoryAdminName, lblAdminName);
			}
			return CommonConstants.EMPTY_STR;
		});
		Combo cmbAdminNameHistorytbl = new Combo(pGroup, SWT.NONE);
		cmbAdminNameHistorytbl.setItems(CommonConstants.History.QUERYTYPE);
		GridData gd_cmbUser = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gd_cmbUser.widthHint = 34;
		cmbAdminNameHistorytbl.setLayoutData(gd_cmbUser);

		txtAdminNamefiltertext = new Text(pGroup, SWT.BORDER);
		txtAdminNamefiltertext.setText("");
		txtAdminNamefiltertext.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 4, 1));

		String adminNameValue = prefs.get("adminHistory.adminName", "");
		if (!adminNameValue.isEmpty()) {
			String[] value = adminNameValue.split(",");
			cmbAdminNameHistorytbl.setText(value[0]);
			txtAdminNamefiltertext.setText(value[1]);
		}

		// operation
		Label lblOperation = new Label(pGroup, SWT.NONE);
		lblOperation.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		registry.register((text) -> {
			if (lblOperation != null && !lblOperation.isDisposed()) {
				lblOperation.setText(text);
			}
		}, (message) -> {
			if (lblOperation != null && !lblOperation.isDisposed()) {
				return getUpdatedWidgetText(message.adminHistoryOperation, lblOperation);
			}
			return CommonConstants.EMPTY_STR;
		});
		Combo cmbOperation = new Combo(pGroup, SWT.NONE);
		cmbOperation.setItems(CommonConstants.History.QUERYTYPE);
		cmbOperation.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));

		Text txtOperationFilter = new Text(pGroup, SWT.BORDER);
		txtOperationFilter.setText("");
		txtOperationFilter.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 4, 1));

		String operaionValue = prefs.get("adminHistory.operation", "");
		if (!operaionValue.isEmpty()) {
			String[] value = operaionValue.split(",");
			cmbOperation.setText(value[0]);
			txtOperationFilter.setText(value[1]);
		}

		// Adminarea
		Label lblAdinArea = new Label(pGroup, SWT.NONE);
		lblAdinArea.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		registry.register((text) -> {
			if (lblAdinArea != null && !lblAdinArea.isDisposed()) {
				lblAdinArea.setText(text);
			}
		}, (message) -> {
			if (lblAdinArea != null && !lblAdinArea.isDisposed()) {
				return getUpdatedWidgetText(message.adminHistoryAdminArea, lblAdinArea);
			}
			return CommonConstants.EMPTY_STR;
		});
		Combo cmbAdminArea = new Combo(pGroup, SWT.NONE);
		cmbAdminArea.setItems(CommonConstants.History.QUERYTYPE);
		cmbAdminArea.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));

		txtAdmimAreafiltertext = new Text(pGroup, SWT.BORDER);
		txtAdmimAreafiltertext.setText("");
		txtAdmimAreafiltertext.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 4, 1));
		String adminAreaValue = prefs.get("adminHistory.adminArea", "");
		if (!adminAreaValue.isEmpty()) {
			String[] value = adminAreaValue.split(",");
			cmbAdminArea.setText(value[0]);
			txtAdmimAreafiltertext.setText(value[1]);
		}

		// Project
		Label lblProject = new Label(pGroup, SWT.NONE);
		lblProject.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		registry.register((text) -> {
			if (lblProject != null && !lblProject.isDisposed()) {
				lblProject.setText(text);
			}
		}, (message) -> {
			if (lblProject != null && !lblProject.isDisposed()) {
				return getUpdatedWidgetText(message.adminHistoryProject, lblProject);
			}
			return CommonConstants.EMPTY_STR;
		});
		Combo cmbProject = new Combo(pGroup, SWT.NONE);
		cmbProject.setItems(CommonConstants.History.QUERYTYPE);
		cmbProject.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));

		txtProjectfiltertext = new Text(pGroup, SWT.BORDER);
		txtProjectfiltertext.setText("");
		txtProjectfiltertext.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 4, 1));

		String projectValue = prefs.get("adminHistory.project", "");
		if (!projectValue.isEmpty()) {
			String[] value = projectValue.split(",");
			cmbProject.setText(value[0]);
			txtProjectfiltertext.setText(value[1]);
		}

		// ProjectApplication
		Label lblProjectApplication = new Label(pGroup, SWT.NONE);
		lblProjectApplication.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		registry.register((text) -> {
			if (lblProjectApplication != null && !lblProjectApplication.isDisposed()) {
				lblProjectApplication.setText(text);
			}
		}, (message) -> {
			if (lblProjectApplication != null && !lblProjectApplication.isDisposed()) {
				return getUpdatedWidgetText(message.adminHistoryProjectApp, lblProjectApplication);
			}
			return CommonConstants.EMPTY_STR;
		});
		Combo cmbProjectApplication = new Combo(pGroup, SWT.NONE);
		cmbProjectApplication.setItems(CommonConstants.History.QUERYTYPE);
		cmbProjectApplication.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));

		txtProjectApplicationfiltertext = new Text(pGroup, SWT.BORDER);
		txtProjectApplicationfiltertext.setText("");
		txtProjectApplicationfiltertext.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 4, 1));

		String projectApplicationValue = prefs.get("adminHistory.projectApplication", "");
		if (!projectApplicationValue.isEmpty()) {
			String[] value = projectApplicationValue.split(",");
			cmbProjectApplication.setText(value[0]);
			txtProjectApplicationfiltertext.setText(value[1]);
		}

		// UserName
		Label lblUserName = new Label(pGroup, SWT.NONE);
		lblUserName.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		//lblUserName.setText(messages.adminHistoryUserName);
		registry.register((text) -> {
			if (lblUserName != null && !lblUserName.isDisposed()) {
				lblUserName.setText(text);
			}
		}, (message) -> {
			if (lblUserName != null && !lblUserName.isDisposed()) {
				return getUpdatedWidgetText(message.adminHistoryUserName, lblUserName);
			}
			return CommonConstants.EMPTY_STR;
		});
		Combo cmbUserName = new Combo(pGroup, SWT.NONE);
		cmbUserName.setItems(CommonConstants.History.QUERYTYPE);
		cmbUserName.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));

		txtUserNamefiltertext = new Text(pGroup, SWT.BORDER);
		txtUserNamefiltertext.setText("");
		txtUserNamefiltertext.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 4, 1));

		String userNameValue = prefs.get("adminHistory.userName", "");
		if (!userNameValue.isEmpty()) {
			String[] value = userNameValue.split(",");
			cmbUserName.setText(value[0]);
			txtUserNamefiltertext.setText(value[1]);
		}

		// StartApplication
		Label lblStartApplication = new Label(pGroup, SWT.NONE);
		lblStartApplication.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		registry.register((text) -> {
			if (lblStartApplication != null && !lblStartApplication.isDisposed()) {
				lblStartApplication.setText(text);
			}
		}, (message) -> {
			if (lblStartApplication != null && !lblStartApplication.isDisposed()) {
				return getUpdatedWidgetText(message.adminHistoryStartApplication, lblStartApplication);
			}
			return CommonConstants.EMPTY_STR;
		});
		Combo cmbStartApplication = new Combo(pGroup, SWT.NONE);
		cmbStartApplication.setItems(CommonConstants.History.QUERYTYPE);
		cmbStartApplication.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));

		txtStartApplicationfiltertext = new Text(pGroup, SWT.BORDER);
		txtStartApplicationfiltertext.setText("");
		txtStartApplicationfiltertext.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 4, 1));

		String startApplication = prefs.get("adminHistory.startApplication", "");
		if (!startApplication.isEmpty()) {
			String[] values = startApplication.split(",");
			cmbStartApplication.setText(values[0]);
			txtStartApplicationfiltertext.setText(values[1]);
		}

		// RelationType
		Label lblRelationType = new Label(pGroup, SWT.NONE);
		lblRelationType.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		registry.register((text) -> {
			if (lblRelationType != null && !lblRelationType.isDisposed()) {
				lblRelationType.setText(text);
			}
		}, (message) -> {
			if (lblRelationType != null && !lblRelationType.isDisposed()) {
				return getUpdatedWidgetText(message.adminHistoryRelationType, lblRelationType);
			}
			return CommonConstants.EMPTY_STR;
		});
		Combo cmbRelationType = new Combo(pGroup, SWT.NONE);
		cmbRelationType.setItems(CommonConstants.History.QUERYTYPE);
		cmbRelationType.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		
		txtRelationTypeFilter = new Text(pGroup, SWT.BORDER);
		txtRelationTypeFilter.setText("");
		txtRelationTypeFilter.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 4, 1));
		
		String relationType = prefs.get("adminHistory.relationType", "");
		if (!relationType.isEmpty()) {
			String[] values = relationType.split(",");
			cmbRelationType.setText(values[0]);
			txtRelationTypeFilter.setText(values[1]);
		}

		// Status
		Label lblStatus = new Label(pGroup, SWT.NONE);
		lblStatus.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		registry.register((text) -> {
			if (lblStatus != null && !lblStatus.isDisposed()) {
				lblStatus.setText(text);
			}
		}, (message) -> {
			if (lblStatus != null && !lblStatus.isDisposed()) {
				return getUpdatedWidgetText(message.adminHistoryStatus, lblStatus);
			}
			return CommonConstants.EMPTY_STR;
		});
		Combo cmbStatus = new Combo(pGroup, SWT.NONE);
		cmbStatus.setItems(CommonConstants.History.RESULT_QUERY_TYPE);
		cmbStatus.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));

		Combo statusFilter = new Combo(pGroup, SWT.NONE);
		statusFilter.setItems(CommonConstants.History.STATUS_TYPE);
		statusFilter.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 4, 1));
		String status = prefs.get("adminHistory.status", "");
		if (!status.isEmpty()) {
			String[] values = status.split(",");
			cmbStatus.setText(values[0]);
			statusFilter.setText(values[1]);
		}

		// Site
		Label lblSite = new Label(pGroup, SWT.NONE);
		lblSite.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		registry.register((text) -> {
			if (lblSite != null && !lblSite.isDisposed()) {
				lblSite.setText(text);
			}
		}, (message) -> {
			if (lblSite != null && !lblSite.isDisposed()) {
				return getUpdatedWidgetText(message.adminHistorySite, lblSite);
			}
			return CommonConstants.EMPTY_STR;
		});
		Combo cmbSite = new Combo(pGroup, SWT.NONE);
		cmbSite.setItems(CommonConstants.History.QUERYTYPE);
		cmbSite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));

		txtSitefiltertext = new Text(pGroup, SWT.BORDER);
		txtSitefiltertext.setText("");
		txtSitefiltertext.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 4, 1));

		String site = prefs.get("adminHistory.site", "");
		if (!site.isEmpty()) {
			String[] values = site.split(",");
			cmbSite.setText(values[0]);
			txtSitefiltertext.setText(values[1]);
		}

		// GroupName
		Label lblGroupName = new Label(pGroup, SWT.NONE);
		lblGroupName.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		registry.register((text) -> {
			if (lblGroupName != null && !lblGroupName.isDisposed()) {
				lblGroupName.setText(text);
			}
		}, (message) -> {
			if (lblGroupName != null && !lblGroupName.isDisposed()) {
				return getUpdatedWidgetText(message.adminHistoryGroupName, lblGroupName);
			}
			return CommonConstants.EMPTY_STR;
		});
		Combo cmbGroupName = new Combo(pGroup, SWT.NONE);
		cmbGroupName.setItems(CommonConstants.History.QUERYTYPE);
		cmbGroupName.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));

		txtGroupNamefiltertext = new Text(pGroup, SWT.BORDER);
		txtGroupNamefiltertext.setText("");
		txtGroupNamefiltertext.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 4, 1));

		String groupName = prefs.get("adminHistory.groupName", "");
		if (!groupName.isEmpty()) {
			String[] values = groupName.split(",");

			cmbGroupName.setText(values[0]);
			txtGroupNamefiltertext.setText(values[1]);
		}

		// UserApplication
		Label lblUserApplication = new Label(pGroup, SWT.NONE);
		lblUserApplication.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		registry.register((text) -> {
			if (lblUserApplication != null && !lblUserApplication.isDisposed()) {
				lblUserApplication.setText(text);
			}
		}, (message) -> {
			if (lblUserApplication != null && !lblUserApplication.isDisposed()) {
				return getUpdatedWidgetText(message.adminHistoryUserApplication, lblUserApplication);
			}
			return CommonConstants.EMPTY_STR;
		});
		Combo cmbUserApplication = new Combo(pGroup, SWT.NONE);
		cmbUserApplication.setItems(CommonConstants.History.QUERYTYPE);
		cmbUserApplication.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));

		txtUserApplicationfiltertext = new Text(pGroup, SWT.BORDER);
		txtUserApplicationfiltertext.setText("");
		txtUserApplicationfiltertext.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 4, 1));

		String userApplication = prefs.get("adminHistory.userApplication", "");
		if (!userApplication.isEmpty()) {
			String[] values = userApplication.split(",");
			cmbUserApplication.setText(values[0]);
			txtUserApplicationfiltertext.setText(values[1]);
		}

		// Directory
		Label lblDirectory = new Label(pGroup, SWT.NONE);
		lblDirectory.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		registry.register((text) -> {
			if (lblDirectory != null && !lblDirectory.isDisposed()) {
				lblDirectory.setText(text);
			}
		}, (message) -> {
			if (lblDirectory != null && !lblDirectory.isDisposed()) {
				return getUpdatedWidgetText(message.adminHistoryDirectory, lblUserApplication);
			}
			return CommonConstants.EMPTY_STR;
		});
		Combo cmbDirectory = new Combo(pGroup, SWT.NONE);
		cmbDirectory.setItems(CommonConstants.History.QUERYTYPE);
		cmbDirectory.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));

		txtDirectoryfiltertext = new Text(pGroup, SWT.BORDER);
		txtDirectoryfiltertext.setText("");
		txtDirectoryfiltertext.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 4, 1));
		String directory = prefs.get("adminHistory.directory", "");
		if (!directory.isEmpty()) {
			String[] values = directory.split(",");
			cmbDirectory.setText(values[0]);
			txtDirectoryfiltertext.setText(values[1]);
		}

		// Role

		Label lblRole = new Label(pGroup, SWT.NONE);
		lblRole.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		registry.register((text) -> {
			if (lblRole != null && !lblRole.isDisposed()) {
				lblRole.setText(text);
			}
		}, (message) -> {
			if (lblRole != null && !lblRole.isDisposed()) {
				return getUpdatedWidgetText(message.adminHistoryRole, lblRole);
			}
			return CommonConstants.EMPTY_STR;
		});
		Combo cmbRole = new Combo(pGroup, SWT.NONE);
		cmbRole.setItems(CommonConstants.History.QUERYTYPE);
		cmbRole.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));

		txtRolefiltertext = new Text(pGroup, SWT.BORDER);
		txtRolefiltertext.setText("");
		txtRolefiltertext.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 4, 1));

		String role = prefs.get("adminHistory.role", "");
		if (!role.isEmpty()) {
			String[] values = role.split(",");
			cmbRole.setText(values[0]);
			txtRolefiltertext.setText(values[1]);
		}
		
		// Result
		Label lblResult = new Label(pGroup, SWT.NONE);
		lblResult.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		registry.register((text) -> {
			if (lblResult != null && !lblResult.isDisposed()) {
				lblResult.setText(text);
			}
		}, (message) -> {
			if (lblResult != null && !lblResult.isDisposed()) {
				return getUpdatedWidgetText(message.adminHistoryResult, lblResult);
			}
			return CommonConstants.EMPTY_STR;
		}); 
		Combo cmbResult = new Combo(pGroup, SWT.NONE);
		cmbResult.setItems(CommonConstants.History.QUERYTYPE);
		cmbResult.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));

		Text txtResultFilter = new Text(pGroup, SWT.BORDER);
		txtResultFilter.setText("");
		txtResultFilter.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 4, 1));

		String resultValue = prefs.get("adminRelationHistory.result", "");
		if (!resultValue.isEmpty()) {
			String[] values = resultValue.split(",");
			cmbResult.setText(values[0]);
			txtResultFilter.setText(values[1]);
		}

		// Limit
		Label lblLimit = new Label(pGroup, SWT.NONE);
		lblLimit.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
	
		registry.register((text) -> {
			if (lblLimit != null && !lblLimit.isDisposed()) {
				lblLimit.setText(text);
			}
		}, (message) -> {
			if (lblLimit != null && !lblLimit.isDisposed()) {
				return getUpdatedWidgetText(message.adminHistoryLimit, lblLimit);
			}
			return CommonConstants.EMPTY_STR;
		});
		txtLimitfilter = new Text(pGroup, SWT.BORDER);
		txtLimitfilter.setText("");
		txtLimitfilter.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 5, 1));
		String limit = prefs.get("adminHistory.limit", CommonConstants.History.ADMIN_HISTORY_ROW_LIMIT);

		txtLimitfilter.setText(limit);
		txtLimitfilter.addVerifyListener(new VerifyListener() {

			@Override
			public void verifyText(VerifyEvent e) {
				String string = e.text;
				char[] chars = new char[string.length()];
				string.getChars(0, chars.length, chars, 0);
				for (int i = 0; i < chars.length; i++) {
					if (!('a' <= chars[i] && chars[i] <= 'z')) {
						e.doit = true;

					} else {
						e.doit = false;
					}
				}
			}
		});

		// ButtonComposite
		Composite buttonBarComposite = new Composite(pGroup, SWT.NONE);
		buttonBarComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 6, 1));
		buttonBarComposite.setLayout(new GridLayout(2, false));

		// DefaultButton
		Button defaultButton = new Button(buttonBarComposite, SWT.PUSH);
		defaultButton.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, true, false, 1, 1));
		registry.register((text) -> {
			if (defaultButton != null && !defaultButton.isDisposed()) {
				defaultButton.setText(text);
			}
		}, (message) -> {
			if (defaultButton != null && !defaultButton.isDisposed()) {
				return getUpdatedWidgetText(message.historyDefaultBtn, defaultButton);
			}
			return CommonConstants.EMPTY_STR;
		});
		defaultButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				prefs.put("adminHistory.startDate", "");
				adminHistroyRelStartDate.setDate(null);
				prefs.put("adminHistory.endDate", "");
				adminHistroyRelEndDate.setDate(null);
				prefs.put("adminHistory.adminName", "");
				cmbAdminNameHistorytbl.setText("");
				txtAdminNamefiltertext.setText("");
				prefs.put("adminHistory.operation", "");
				cmbOperation.setText("");
				txtOperationFilter.setText("");
				prefs.put("adminHistory.adminArea", "");
				cmbAdminArea.setText("");
				txtAdmimAreafiltertext.setText("");
				prefs.put("adminHistory.project", "");
				cmbProject.setText("");
				txtProjectfiltertext.setText("");
				prefs.put("adminHistory.projectApplication", "");
				cmbProjectApplication.setText("");
				txtProjectApplicationfiltertext.setText("");
				prefs.put("adminHistory.userName", "");
				cmbUserName.setText("");
				txtUserNamefiltertext.setText("");
				prefs.put("adminHistory.startApplication", "");
				cmbStartApplication.setText("");
				txtStartApplicationfiltertext.setText("");
				prefs.put("adminHistory.relationType", "");
				cmbRelationType.setText("");
				txtRelationTypeFilter.setText("");
				prefs.put("adminHistory.status", "");
				cmbStatus.setText("");
				statusFilter.setText("");
				prefs.put("adminHistory.site", "");
				cmbSite.setText("");
				txtSitefiltertext.setText("");
				prefs.put("adminHistory.groupName", "");
				cmbGroupName.setText("");
				txtGroupNamefiltertext.setText("");
				prefs.put("adminHistory.userApplication", "");
				cmbUserApplication.setText("");
				txtUserApplicationfiltertext.setText("");
				prefs.put("adminHistory.directory", "");
				cmbDirectory.setText("");
				txtDirectoryfiltertext.setText("");
				prefs.put("adminHistory.role", "");
				cmbRole.setText("");
				txtRolefiltertext.setText("");
				prefs.put("adminRelationHistory.result", "");
				cmbResult.setText("");
				txtResultFilter.setText("");
				
				txtLimitfilter.setText(CommonConstants.History.ADMIN_HISTORY_ROW_LIMIT);
				prefs.put("adminHistory.limit", CommonConstants.History.ADMIN_HISTORY_ROW_LIMIT);
			}
		});

		// ApplyButton
		relationHistoryApplyButton = new Button(buttonBarComposite, SWT.PUSH);
		relationHistoryApplyButton.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		registry.register((text) -> {
			if (relationHistoryApplyButton != null && !relationHistoryApplyButton.isDisposed()) {
				relationHistoryApplyButton.setText(text);
			}
		}, (message) -> {
			if (relationHistoryApplyButton != null && !relationHistoryApplyButton.isDisposed()) {
				return getUpdatedWidgetText(message.historyApplyBtn, relationHistoryApplyButton);
			}
			return CommonConstants.EMPTY_STR;
		});
		relationHistoryApplyButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent selectionEvent) {
				String stDateString = null;
				if (adminHistroyRelStartDate.getDate() != null) {
					Date stDate = adminHistroyRelStartDate.getDate();
					stDateString = XMSystemUtil.getHistoryLogTimeDateFormat().format(stDate);
					prefs.put("adminHistory.startDate", stDateString);
				} else {
					prefs.put("adminHistory.startDate", "");
				}

				String endDateString = null;
				if (adminHistroyRelEndDate.getDate() != null) {
					Date endDate = adminHistroyRelEndDate.getDate();
					endDateString = stDateString = XMSystemUtil.getHistoryLogTimeDateFormat().format(endDate);
					prefs.put("adminHistory.endDate", endDateString);
				} else {
					prefs.put("adminHistory.endDate", "");
				}

				String adminRule = cmbAdminNameHistorytbl.getText();
				String adminNameFltr = txtAdminNamefiltertext.getText();
				if (!adminNameFltr.isEmpty()) {
					prefs.put("adminHistory.adminName", adminRule + "," + adminNameFltr);
				} else {
					prefs.put("adminHistory.adminName", "");
				}

				String operationRule = cmbOperation.getText();
				String operationFltr = txtOperationFilter.getText().trim();
				if (!operationFltr.isEmpty()) {
					prefs.put("adminHistory.operation", operationRule + "," + operationFltr);
				} else {
					prefs.put("adminHistory.operation", "");
				}

				String adminAreaRule = cmbAdminArea.getText();
				String adminAreaFltr = txtAdmimAreafiltertext.getText();
				if (!adminAreaFltr.isEmpty()) {
					prefs.put("adminHistory.adminArea", adminAreaRule + "," + adminAreaFltr);
				} else {
					prefs.put("adminHistory.adminArea", "");
				}

				String projectRule = cmbProject.getText();
				String projectFilter = txtProjectfiltertext.getText();
				if (!projectFilter.isEmpty()) {
					prefs.put("adminHistory.project", projectRule + "," + projectFilter);
				} else {
					prefs.put("adminHistory.project", "");
				}

				String projectApplicationRule = cmbProjectApplication.getText();
				String projectApplicationFilter = txtProjectApplicationfiltertext.getText();
				if (!projectApplicationFilter.isEmpty()) {
					prefs.put("adminHistory.projectApplication",
							projectApplicationRule + "," + projectApplicationFilter);
				} else {
					prefs.put("adminHistory.projectApplication", "");
				}

				String userNameRule = cmbUserName.getText();
				String userNameFilter = txtUserNamefiltertext.getText();
				if (!userNameFilter.isEmpty()) {
					prefs.put("adminHistory.userName", userNameRule + "," + userNameFilter);
				} else {
					prefs.put("adminHistory.userName", "");
				}

				String startApplicationRule = cmbStartApplication.getText();
				String startApplicationFilter = txtStartApplicationfiltertext.getText();
				if (!startApplicationFilter.isEmpty()) {
					prefs.put("adminHistory.startApplication", startApplicationRule + "," + startApplicationFilter);
				} else {
					prefs.put("adminHistory.startApplication", "");
				}

				String relationTypeRule = cmbRelationType.getText();
				String relationTypeFltr = txtRelationTypeFilter.getText().trim();
				if (!relationTypeFltr.isEmpty()) {
					prefs.put("adminHistory.relationType", relationTypeRule + "," + relationTypeFltr);
				} else {
					prefs.put("adminHistory.relationType", "");
				}

				String statusRule = cmbStatus.getText();
				String statusFltr = statusFilter.getText();
				if (!statusFltr.isEmpty()) {
					prefs.put("adminHistory.status", statusRule + "," + statusFltr);
				} else {
					prefs.put("adminHistory.status", "");
				}

				String siteRule = cmbSite.getText();
				String siteFilter = txtSitefiltertext.getText();
				if (!siteFilter.isEmpty()) {
					prefs.put("adminHistory.site", siteRule + "," + siteFilter);
				} else {
					prefs.put("adminHistory.site", "");
				}

				String groupNameRule = cmbGroupName.getText();
				String groupNameFilter = txtGroupNamefiltertext.getText();
				if (!groupNameFilter.isEmpty()) {
					prefs.put("adminHistory.groupName", groupNameRule + "," + groupNameFilter);
				} else {
					prefs.put("adminHistory.groupName", "");
				}

				String userApplicationRule = cmbUserApplication.getText();
				String userApplicationFilter = txtUserApplicationfiltertext.getText();
				if (!userApplicationFilter.isEmpty()) {
					prefs.put("adminHistory.userApplication", userApplicationRule + "," + userApplicationFilter);
				} else {
					prefs.put("adminHistory.userApplication", "");
				}

				String directoryRule = cmbDirectory.getText();
				String directoryFilter = txtDirectoryfiltertext.getText();
				if (!directoryFilter.isEmpty()) {
					prefs.put("adminHistory.directory", directoryRule + "," + directoryFilter);
				} else {
					prefs.put("adminHistory.directory", "");
				}

				String roleRule = cmbRole.getText();
				String roleFilter = txtRolefiltertext.getText();
				if (!roleFilter.isEmpty()) {
					prefs.put("adminHistory.role", roleRule + "," + roleFilter);
				} else {
					prefs.put("adminHistory.role", "");
				}
				
				String resultRule = cmbResult.getText();
				String resultFilter = txtResultFilter.getText();
				if (!resultFilter.isEmpty()) {
					prefs.put("adminRelationHistory.result", resultRule + "," + resultFilter);
				} else {
					prefs.put("adminRelationHistory.result", "");
				}

				String limitString = txtLimitfilter.getText();
				if (!limitString.isEmpty()) {
					try {
						Integer queryLimit = Integer.parseInt(limitString);
						prefs.put("adminHistory.limit", String.valueOf(queryLimit));
					} catch (NumberFormatException e) {
						CustomMessageDialog.openError(Display.getCurrent().getActiveShell(), messages.errorDialogTitile,
								messages.historyLimitErrorMsg);
					}
				} else {
					/*
					 * CustomMessageDialog.openError(Display.getCurrent().
					 * getActiveShell(), messages.errorDialogTitile,
					 * messages.historyEmptyLimitMsg); return;
					 */
					prefs.put("adminHistory.limit", CommonConstants.History.HISTORY_HIDDEN_ROW_LIMIT);
				}

				AdminHistoryRequest adminHistoryRequest = new AdminHistoryRequest();
				adminHistoryRequest.setQueryCondition(getQueryConditionForRelationHistory());
				adminHistoryRequest.setQueryLimit(Integer
						.parseInt(prefs.get("adminHistory.limit", CommonConstants.History.ADMIN_HISTORY_ROW_LIMIT)));
				adminHistoryRelationshipNatTableContainer.updateNatTable(adminHistoryRequest);
				if (prefs.get("adminHistory.limit", CommonConstants.History.ADMIN_HISTORY_ROW_LIMIT)
						.equals(CommonConstants.History.HISTORY_HIDDEN_ROW_LIMIT)) {
					prefs.put("adminHistory.limit", CommonConstants.History.ADMIN_HISTORY_ROW_LIMIT);
				}

				try {
					prefs.flush();
				} catch (BackingStoreException e1) {
					LOGGER.warn("Cannot store login preferences!", e1);
				}
			}
		});
		pGroup.setExpanded(false);
	}

	/**
	 * Method for Creates the base obj history filter comp.
	 *
	 * @param parent
	 *            {@link Composite}
	 */
	private void createBaseObjHistoryFilterComp(final Composite parent) {
		final PGroup pGroup = new PGroup(parent, SWT.SMOOTH);
		pGroup.addExpandListener(new PGroupExpandListener());

		final GridLayout widgetContLayout = new GridLayout(6, false);
		widgetContLayout.horizontalSpacing = 10;
		pGroup.setLayout(widgetContLayout);
		pGroup.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 3, 1));
		registry.register((text) -> {
			if (pGroup != null && !pGroup.isDisposed()) {
				pGroup.setText(text);
			}
		}, (message) -> {
			if (pGroup != null && !pGroup.isDisposed()) {
				return getUpdatedWidgetText(message.adminHistoryBaseFilterLabel, pGroup);
			}
			return CommonConstants.EMPTY_STR;
		});
		Label lblLogTime = new Label(pGroup, SWT.NONE);
		registry.register((text) -> {
			if (lblLogTime != null && !lblLogTime.isDisposed()) {
				lblLogTime.setText(text);
			}
		}, (message) -> {
			if (lblLogTime != null && !lblLogTime.isDisposed()) {
				return getUpdatedWidgetText(message.historyLogTimeLabel, lblLogTime);
			}
			return CommonConstants.EMPTY_STR;
		});
		lblLogTime.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));

		new Label(pGroup, SWT.NONE);

		Label lblCdate = new Label(pGroup, SWT.NONE);
		lblCdate.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false));
		
		registry.register((text) -> {
			if (lblCdate != null && !lblCdate.isDisposed()) {
				lblCdate.setText(text);
			}
		}, (message) -> {
			if (lblCdate != null && !lblCdate.isDisposed()) {
				return getUpdatedWidgetText(message.historyStartDateLabel, lblCdate);
			}
			return CommonConstants.EMPTY_STR;
		});
		this.adminHistroyBaseObjStartDate = new CustomDateTime(pGroup); 
		this.adminHistroyBaseObjStartDate.setLayoutData(new GridData(SWT.FILL, SWT.LEFT, true, false));
		String stDate = prefs.get("adminBaseObject.stDate", "");

		if (!stDate.isEmpty()) {
			try {
				this.adminHistroyBaseObjStartDate
						.setDate(XMSystemUtil.getHistoryLogTimeDateFormat().parse(stDate));
			} catch (ParseException e1) {
				LOGGER.error("Exception while parsing date!", e1);
			}
		}

		registry.register((text) -> {
		}, (message) -> {
			if(adminHistroyBaseObjStartDate != null && !adminHistroyBaseObjStartDate.isDisposed()){
			LANG_ENUM currentLocaleEnum = XMAdminUtil.getInstance().getCurrentLocaleEnum();
			adminHistroyBaseObjStartDate.updateTooltipText(currentLocaleEnum.toString());
			}
			return CommonConstants.EMPTY_STR;
		});
		
		
		Label lblCdate1 = new Label(pGroup, SWT.NONE);
		lblCdate1.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false));
		registry.register((text) -> {
			if (lblCdate1 != null && !lblCdate1.isDisposed()) {
				lblCdate1.setText(text);
			}
		}, (message) -> {
			if (lblCdate1 != null && !lblCdate1.isDisposed()) {
				return getUpdatedWidgetText(message.historyEndDateLabel, lblCdate1);
			}
			return CommonConstants.EMPTY_STR;
		});
		this.adminHistroyBaseObjEndDate = new CustomDateTime(pGroup);
		this.adminHistroyBaseObjEndDate.setLayoutData(new GridData(SWT.FILL, SWT.LEFT, true, false));
		String eDate = prefs.get("adminBaseObject.eDate", "");
		if (!eDate.isEmpty()) {
			try {
				this.adminHistroyBaseObjEndDate.setDate(XMSystemUtil.getHistoryLogTimeDateFormat().parse(eDate));
			} catch (ParseException e1) {
				LOGGER.error("Exception while parsing date!", e1);
			}
		}

		registry.register((text) -> {
		}, (message) -> {
			if(adminHistroyBaseObjEndDate != null && !adminHistroyBaseObjEndDate.isDisposed()){
			LANG_ENUM currentLocaleEnum = XMAdminUtil.getInstance().getCurrentLocaleEnum();
			adminHistroyBaseObjEndDate.updateTooltipText(currentLocaleEnum.toString());
			}
			return CommonConstants.EMPTY_STR;
		});
		
		// AdminName
		Label lblAdminName = new Label(pGroup, SWT.NONE);
		lblAdminName.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		registry.register((text) -> {
			if (lblAdminName != null && !lblAdminName.isDisposed()) {
				lblAdminName.setText(text);
			}
		}, (message) -> {
			if (lblAdminName != null && !lblAdminName.isDisposed()) {
				return getUpdatedWidgetText(message.adminHistoryAdminName, lblAdminName);
			}
			return CommonConstants.EMPTY_STR;
		});
		Combo cmbAdminName = new Combo(pGroup, SWT.NONE);
		cmbAdminName.setItems(CommonConstants.History.QUERYTYPE);
		GridData gd_cmbUser = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gd_cmbUser.widthHint = 34;
		cmbAdminName.setLayoutData(gd_cmbUser);

		txtAdminNameBaseObjectfiltertext = new Text(pGroup, SWT.BORDER);
		txtAdminNameBaseObjectfiltertext.setText("");
		txtAdminNameBaseObjectfiltertext.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 4, 1));

		String adminNameBaseObjectValue = prefs.get("adminBaseObject.adminName", "");
		if (!adminNameBaseObjectValue.isEmpty()) {
			String[] values = adminNameBaseObjectValue.split(",");
			cmbAdminName.setText(values[0]);
			txtAdminNameBaseObjectfiltertext.setText(values[1]);
		}

		// operation
		Label lblOperation = new Label(pGroup, SWT.NONE);
		lblOperation.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		registry.register((text) -> {
			if (lblOperation != null && !lblOperation.isDisposed()) {
				lblOperation.setText(text);
			}
		}, (message) -> {
			if (lblOperation != null && !lblOperation.isDisposed()) {
				return getUpdatedWidgetText(message.adminHistoryOperation, lblOperation);
			}
			return CommonConstants.EMPTY_STR;
		});
		Combo cmbOperation = new Combo(pGroup, SWT.NONE);
		cmbOperation.setItems(CommonConstants.History.QUERYTYPE);
		cmbOperation.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));

		Text txtOperationFilter = new Text(pGroup, SWT.BORDER);
		txtOperationFilter.setText("");
		txtOperationFilter.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 4, 1));
		
		String operationValue = prefs.get("adminBaseObject.operation", "");
		if (!operationValue.isEmpty()) {
			String[] values = operationValue.split(",");
			cmbOperation.setText(values[0]);
			txtOperationFilter.setText(values[1]);
		}

		// ObjectName
		Label lblObjectName = new Label(pGroup, SWT.NONE);
		lblObjectName.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		registry.register((text) -> {
			if (lblObjectName != null && !lblObjectName.isDisposed()) {
				lblObjectName.setText(text);
			}
		}, (message) -> {
			if (lblObjectName != null && !lblObjectName.isDisposed()) {
				return getUpdatedWidgetText(message.adminHistoryObjectName, lblObjectName);
			}
			return CommonConstants.EMPTY_STR;
		});
		Combo cmbObjectName = new Combo(pGroup, SWT.NONE);
		cmbObjectName.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		cmbObjectName.setItems(CommonConstants.History.QUERYTYPE);

		txtObjectNamefiltertext = new Text(pGroup, SWT.BORDER);
		txtObjectNamefiltertext.setText("");
		txtObjectNamefiltertext.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 4, 1));
		String objectNameValue = prefs.get("adminBaseObject.objectName", "");
		if (!objectNameValue.isEmpty()) {
			String[] values = objectNameValue.split(",");
			cmbObjectName.setText(values[0]);
			txtObjectNamefiltertext.setText(values[1]);
		}

		// Result
		Label lblResult = new Label(pGroup, SWT.NONE);
		lblResult.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		registry.register((text) -> {
			if (lblResult != null && !lblResult.isDisposed()) {
				lblResult.setText(text);
			}
		}, (message) -> {
			if (lblResult != null && !lblResult.isDisposed()) {
				return getUpdatedWidgetText(message.adminHistoryResult, lblResult);
			}
			return CommonConstants.EMPTY_STR;
		});
		Combo cmbResult = new Combo(pGroup, SWT.NONE);
		cmbResult.setItems(CommonConstants.History.QUERYTYPE);
		cmbResult.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		
		txtResultFilter = new Text(pGroup, SWT.BORDER);
		txtResultFilter.setText("");
		txtResultFilter.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 4, 1));
		
		String resultValue = prefs.get("adminBaseObject.result", "");
		if (!resultValue.isEmpty()) {
			String[] values = resultValue.split(",");
			cmbResult.setText(values[0]);
			txtResultFilter.setText(values[1]);
		}

		// Limit
		Label lblLimit = new Label(pGroup, SWT.NONE);
		lblLimit.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		registry.register((text) -> {
			if (lblLimit != null && !lblLimit.isDisposed()) {
				lblLimit.setText(text);
			}
		}, (message) -> {
			if (lblLimit != null && !lblLimit.isDisposed()) {
				return getUpdatedWidgetText(message.adminHistoryLimit, lblLimit);
			}
			return CommonConstants.EMPTY_STR;
		});
		txtBaseObjectLimitfilter = new Text(pGroup, SWT.BORDER);
		txtBaseObjectLimitfilter.setText("");
		txtBaseObjectLimitfilter.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 5, 1));

		String limit = prefs.get("adminBaseObject.limit", CommonConstants.History.ADMIN_HISTORY_ROW_LIMIT);

		txtBaseObjectLimitfilter.setText(limit);

		txtBaseObjectLimitfilter.addVerifyListener(new VerifyListener() {

			@Override
			public void verifyText(VerifyEvent e) {
				String string = e.text;
				char[] chars = new char[string.length()];
				string.getChars(0, chars.length, chars, 0);
				for (int i = 0; i < chars.length; i++) {
					if (!('a' <= chars[i] && chars[i] <= 'z')) {
						e.doit = true;

					} else {
						e.doit = false;
					}
				}
			}
		});

		// ButtonComposite
		Composite buttonBarComposite = new Composite(pGroup, SWT.NONE);
		buttonBarComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 6, 1));
		buttonBarComposite.setLayout(new GridLayout(2, false));

		// DefaultButton
		Button defaultButton = new Button(buttonBarComposite, SWT.PUSH);
		defaultButton.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, true, false, 1, 1));
		registry.register((text) -> {
			if (defaultButton != null && !defaultButton.isDisposed()) {
				defaultButton.setText(text);
			}
		}, (message) -> {
			if (defaultButton != null && !defaultButton.isDisposed()) {
				return getUpdatedWidgetText(message.historyDefaultBtn, defaultButton);
			}
			return CommonConstants.EMPTY_STR;
		});
		defaultButton.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				prefs.put("adminBaseObject.stDate", "");
				adminHistroyBaseObjStartDate.setDate(null);
				prefs.put("adminBaseObject.eDate", "");
				adminHistroyBaseObjEndDate.setDate(null);
				prefs.put("adminBaseObject.adminName", "");
				cmbAdminName.setText("");
				txtAdminNameBaseObjectfiltertext.setText("");
				prefs.put("adminBaseObject.operation", "");
				cmbOperation.setText("");
				txtOperationFilter.setText("");
				prefs.put("adminBaseObject.objectName", "");
				cmbObjectName.setText("");
				txtObjectNamefiltertext.setText("");
				prefs.put("adminBaseObject.result", "");
				cmbResult.setText("");
				txtResultFilter.setText("");
				txtBaseObjectLimitfilter.setText("");
				prefs.get("adminBaseObject.limit", "");
				prefs.get("adminBaseObject.limit", CommonConstants.History.ADMIN_HISTORY_ROW_LIMIT);
				txtBaseObjectLimitfilter.setText(CommonConstants.History.ADMIN_HISTORY_ROW_LIMIT);
			}
		});

		// ApplyButton
		baseHistoryApplyButton = new Button(buttonBarComposite, SWT.PUSH);
		baseHistoryApplyButton.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		registry.register((text) -> {
			if (baseHistoryApplyButton != null && !baseHistoryApplyButton.isDisposed()) {
				baseHistoryApplyButton.setText(text);
			}
		}, (message) -> {
			if (baseHistoryApplyButton != null && !baseHistoryApplyButton.isDisposed()) {
				return getUpdatedWidgetText(message.historyApplyBtn, baseHistoryApplyButton);
			}
			return CommonConstants.EMPTY_STR;
		});
		baseHistoryApplyButton.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				String stDateString = null;
				if (adminHistroyBaseObjStartDate.getDate() != null) {
					Date stDate = adminHistroyBaseObjStartDate.getDate();
					stDateString = XMSystemUtil.getHistoryLogTimeDateFormat().format(stDate);
					prefs.put("adminBaseObject.stDate", stDateString);
				} else {
					prefs.put("adminBaseObject.stDate", "");
				}

				String endDateString = null;
				if (adminHistroyBaseObjEndDate.getDate() != null) {
					Date eDate = adminHistroyBaseObjEndDate.getDate();
					endDateString = XMSystemUtil.getHistoryLogTimeDateFormat().format(eDate);
					prefs.put("adminBaseObject.eDate", endDateString);
				} else {
					prefs.put("adminBaseObject.eDate", "");
				}

				String adminNameRule = cmbAdminName.getText();
				String adminNameFilterText = txtAdminNameBaseObjectfiltertext.getText();
				if (!adminNameFilterText.isEmpty()) {
					prefs.put("adminBaseObject.adminName", adminNameRule + "," + adminNameFilterText);
				} else {
					prefs.put("adminBaseObject.adminName", "");
				}

				String operationRule = cmbOperation.getText();
				String operationFltr = txtOperationFilter.getText().trim();
				if (!operationFltr.isEmpty()) {
					prefs.put("adminBaseObject.operation", operationRule + "," + operationFltr);
				} else {
					prefs.put("adminBaseObject.operation", "");
				}

				String objectNameRule = cmbObjectName.getText();
				String objectFilter = txtObjectNamefiltertext.getText();
				if (!objectFilter.isEmpty()) {
					prefs.put("adminBaseObject.objectName", objectNameRule + "," + objectFilter);
				} else {
					prefs.put("adminBaseObject.objectName", "");
				}

				String resultRule = cmbResult.getText();
				String resultFltr = txtResultFilter.getText().trim();
				if (!resultFltr.isEmpty()) {
					prefs.put("adminBaseObject.result", resultRule + "," + resultFltr);
				} else {
					prefs.put("adminBaseObject.result", "");
				}

				String limitString = txtBaseObjectLimitfilter.getText();
				if (!limitString.isEmpty()) {
					try {
						Integer queryLimit = Integer.parseInt(limitString);
						prefs.put("adminBaseObject.limit", String.valueOf(queryLimit));
					} catch (NumberFormatException e1) {
						CustomMessageDialog.openError(Display.getCurrent().getActiveShell(), messages.errorDialogTitile,
								messages.historyLimitErrorMsg);
					}
				} else {
					/*
					 * CustomMessageDialog.openError(Display.getCurrent().
					 * getActiveShell(), messages.errorDialogTitile,
					 * messages.historyEmptyLimitMsg); return;
					 */
					prefs.put("adminBaseObject.limit", CommonConstants.History.HISTORY_HIDDEN_ROW_LIMIT);
				}

				AdminHistoryRequest adminHistoryRequest = new AdminHistoryRequest();
				adminHistoryRequest.setQueryCondition(getQueryConditionForBaseObjectHistory());
				adminHistoryRequest.setQueryLimit(Integer
						.parseInt(prefs.get("adminBaseObject.limit", CommonConstants.History.ADMIN_HISTORY_ROW_LIMIT)));
				adminHistoryBaseObjectContainer.updateNatTable(adminHistoryRequest);
				if (prefs.get("adminBaseObject.limit", CommonConstants.History.ADMIN_HISTORY_ROW_LIMIT)
						.equals(CommonConstants.History.HISTORY_HIDDEN_ROW_LIMIT)) {
					prefs.put("adminBaseObject.limit", CommonConstants.History.ADMIN_HISTORY_ROW_LIMIT);
				}
				try {
					prefs.flush();
				} catch (BackingStoreException e1) {
					LOGGER.warn("Cannot store login preferences!", e1);
				}
			}
		});
		pGroup.setExpanded(false);
	}

	/**
	 * Method for Generate base objects history table.
	 */
	private void generateBaseObjectsHistoryTable() {
		GridLayout gridLayout = new GridLayout();
		gridLayout.marginWidth = 0;
		gridLayout.marginHeight = 0;

		baseObjectsHistoryPage = new Composite(mainDataGroup, SWT.NONE);
		baseObjectsHistoryPage.setLayout(gridLayout);
		baseObjectsHistoryPage.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

		final ScrolledComposite scrolledComposite = new ScrolledComposite(baseObjectsHistoryPage, SWT.V_SCROLL);
		scrolledComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		scrolledComposite.setExpandHorizontal(true);
		scrolledComposite.setExpandVertical(true);

		final Composite pageScrolledComposite = new Composite(scrolledComposite, SWT.NONE);
		pageScrolledComposite.setLayout(gridLayout);
		pageScrolledComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

		createBaseObjHistoryFilterComp(pageScrolledComposite);

		AdminHistoryRequest adminHistoryRequest = new AdminHistoryRequest();
		adminHistoryRequest.setQueryCondition(getQueryConditionForBaseObjectHistory());
		adminHistoryRequest.setQueryLimit(
				Integer.parseInt(prefs.get("adminBaseObject.limit", CommonConstants.History.ADMIN_HISTORY_ROW_LIMIT)));

		Composite natTableComp = new Composite(pageScrolledComposite, SWT.BORDER);
		GridLayout natTableCompGL = new GridLayout(1, false);
		natTableCompGL.marginWidth = 0;
		natTableCompGL.marginHeight = 0;

		natTableComp.setLayout(natTableCompGL);
		GridData gridData = new GridData(GridData.FILL, SWT.FILL, true, true);
		gridData.widthHint = 600;
		gridData.minimumHeight = 300;
		gridData.heightHint = 300;
		natTableComp.setLayoutData(gridData);

		adminHistoryBaseObjectContainer = new AdminHistoryBaseObjectNattable();
		adminHistoryBaseObjectNatTable = adminHistoryBaseObjectContainer
				.createAdminHistoryBaseObjectNatTable(natTableComp, adminHistoryRequest);

		scrolledComposite.setContent(pageScrolledComposite);
		scrolledComposite.update();

		scrolledComposite.addControlListener(new ControlAdapter() {
			public void controlResized(final ControlEvent e) {
				Rectangle rectangle = scrolledComposite.getClientArea();
				scrolledComposite.setMinSize(pageScrolledComposite.computeSize(rectangle.width, SWT.DEFAULT));
			}
		});

		scrolledComposite.addListener(SWT.Activate, new Listener() {
			public void handleEvent(Event e) {
				scrolledComposite.setFocus();
			}
		});
	}

	/**
	 * Method for Generate relation objects history table.
	 */
	private void generateRelationObjectsHistoryTable() {
		GridLayout gridLayout = new GridLayout();
		gridLayout.marginWidth = 0;
		gridLayout.marginHeight = 0;

		relationObjectsHistoryPage = new Composite(mainDataGroup, SWT.NONE);
		relationObjectsHistoryPage.setLayout(gridLayout);
		relationObjectsHistoryPage.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

		final ScrolledComposite scrolledComposite = new ScrolledComposite(relationObjectsHistoryPage, SWT.V_SCROLL);
		scrolledComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		scrolledComposite.setExpandHorizontal(true);
		scrolledComposite.setExpandVertical(true);

		final Composite pageScrolledComposite = new Composite(scrolledComposite, SWT.NONE);
		pageScrolledComposite.setLayout(gridLayout);
		pageScrolledComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

		createRelationObjHistoryFilterComp(pageScrolledComposite);

		AdminHistoryRequest adminHistoryRequest = new AdminHistoryRequest();
		adminHistoryRequest.setQueryCondition(getQueryConditionForRelationHistory());
		adminHistoryRequest.setQueryLimit(
				Integer.parseInt(prefs.get("adminHistory.limit", CommonConstants.History.ADMIN_HISTORY_ROW_LIMIT)));

		Composite natTableComp = new Composite(pageScrolledComposite, SWT.BORDER);
		GridLayout natTableCompGL = new GridLayout(1, false);
		natTableCompGL.marginWidth = 0;
		natTableCompGL.marginHeight = 0;

		natTableComp.setLayout(natTableCompGL);
		GridData gridData = new GridData(GridData.FILL, SWT.FILL, true, true);
		gridData.widthHint = 600;
		gridData.minimumHeight = 300;
		gridData.heightHint = 300;
		natTableComp.setLayoutData(gridData);

		adminHistoryRelationshipNatTableContainer = new AdminHistoryRelationsNattable();
		adminHistoryRelationsNatTable = adminHistoryRelationshipNatTableContainer
				.createAdminHistoryRelationsNatTable(natTableComp, adminHistoryRequest);

		scrolledComposite.setContent(pageScrolledComposite);
		scrolledComposite.update();

		scrolledComposite.addControlListener(new ControlAdapter() {
			public void controlResized(final ControlEvent e) {
				Rectangle rectangle = scrolledComposite.getClientArea();
				scrolledComposite.setMinSize(pageScrolledComposite.computeSize(rectangle.width, SWT.DEFAULT));
			}
		});

		scrolledComposite.addListener(SWT.Activate, new Listener() {
			public void handleEvent(Event e) {
				scrolledComposite.setFocus();
			}
		});
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.dialogs.Dialog#createButtonsForButtonBar(org.eclipse.
	 * swt.widgets.Composite)
	 */
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		// createButton(parent, IDialogConstants.OK_ID,
		// IDialogConstants.OK_LABEL, true);
		// createButton(parent, IDialogConstants.CANCEL_ID,
		// IDialogConstants.CANCEL_LABEL, false);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.dialogs.Dialog#isResizable()
	 */
	@Override
	protected boolean isResizable() {
		return true;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.dialogs.Dialog#getInitialSize()
	 */
	@Override
	protected Point getInitialSize() {
		return new Point(800, 500);
	}

	/**
	 * Gets the query condition for relation history.
	 *
	 * @return the query condition for relation history
	 */
	private String getQueryConditionForRelationHistory() {
		ArrayList<String> conditions = new ArrayList<String>();
		String stDate = prefs.get("adminHistory.startDate", "");
		String eDate = prefs.get("adminHistory.endDate", "");
		if (!stDate.isEmpty() && !eDate.isEmpty()) {
			String condition = "LOG_TIME > " + "'" + stDate + "'" + " AND LOG_TIME < " + "'" + eDate + "'";
			conditions.add(condition);
		} else if (!stDate.isEmpty()) {
			String condition = "LOG_TIME > " + "'" + stDate + "'";
			conditions.add(condition);
		} else if (!eDate.isEmpty()) {
			String condition = "LOG_TIME < " + "'" + eDate + "'";
			conditions.add(condition);
		}
		String adminName;
		adminName = prefs.get("adminHistory.adminName", "");
		if (!adminName.isEmpty()) {
			String[] values = adminName.split(",");
			String adminRule = values[0];
			String adminNameFltr = values[1];
			String condition = "ADMIN_NAME " + adminRule + " '" + adminNameFltr + "'";
			conditions.add(condition);
		}

		String operation;
		operation = prefs.get("adminHistory.operation", "");
		if (!operation.isEmpty()) {
			String[] values = operation.split(",");
			String operationRule = values[0];
			String operationFltr = values[1];
			String condition = "OPERATION " + operationRule + " '" + operationFltr + "'";
			conditions.add(condition);
		}

		String adminArea;
		adminArea = prefs.get("adminHistory.adminArea", "");
		if (!adminArea.isEmpty()) {
			String[] values = adminArea.split(",");
			String adminAreaRule = values[0];
			String adminAreaFltr = values[1];
			String condition = "ADMIN_AREA " + adminAreaRule + " '" + adminAreaFltr + "'";
			conditions.add(condition);
		}
		
		String project;
		project = prefs.get("adminHistory.project", "");
		if (!project.isEmpty()) {
			String[] values = project.split(",");
			String projectRule = values[0];
			String projectFltr = values[1];
			String condition = "PROJECT " + projectRule + " '" + projectFltr + "'";
			conditions.add(condition);
		}

		String projectApplication;
		projectApplication = prefs.get("adminHistory.projectApplication", "");
		if (!projectApplication.isEmpty()) {
			String[] values = projectApplication.split(",");
			String projectApplicationRule = values[0];
			String projectApplicationFilter = values[1];
			String condition = "PROJECT_APPLICATION " + projectApplicationRule + " '" + projectApplicationFilter + "'";
			conditions.add(condition);
		}

		String userName;
		userName = prefs.get("adminHistory.userName", "");
		if (!userName.isEmpty()) {
			String[] values = userName.split(",");
			String userNameRule = values[0];
			String userNameFilter = values[1];
			String condition = "USER_NAME " + userNameRule + " '" + userNameFilter + "'";
			conditions.add(condition);
		}

		String startApplication;
		startApplication = prefs.get("adminHistory.startApplication", "");
		if (!startApplication.isEmpty()) {
			String[] values = startApplication.split(",");
			String startApplicationRule = values[0];
			String startApplicationFilter = values[1];
			String condition = "START_APPLICATION " + startApplicationRule + " '" + startApplicationFilter + "'";
			conditions.add(condition);
		}

		String relationType;
		relationType = prefs.get("adminHistory.relationType", "");
		if (!relationType.isEmpty()) {
			String[] values = relationType.split(",");
			String relationTypeRule = values[0];
			String relationTypeFltr = values[1];
			String condition = "RELATION_TYPE " + relationTypeRule + " '" + relationTypeFltr + "'";
			conditions.add(condition);
		}

		String status;
		status = prefs.get("adminHistory.status", "");
		if (!status.isEmpty()) {
			String[] values = status.split(",");
			String statusRule = values[0];
			String statusFltr = values[1];
			String condition = "STATUS " + statusRule + " '" + statusFltr + "'";
			conditions.add(condition);
		}

		String site;
		site = prefs.get("adminHistory.site", "");
		if (!site.isEmpty()) {
			String[] values = site.split(",");
			String siteRule = values[0];
			String siteFilter = values[1];
			String condition = " SITE " + siteRule + " '" + siteFilter + "'";
			conditions.add(condition);
		}

		String groupName;
		groupName = prefs.get("adminHistory.groupName", "");
		if (!groupName.isEmpty()) {
			String[] values = groupName.split(",");
			String groupNameRule = values[0];
			String groupNameFilter = values[1];
			String condition = "GROUP_NAME " + groupNameRule + " '" + groupNameFilter + "'";
			conditions.add(condition);
		}

		String userApplication;
		userApplication = prefs.get("adminHistory.userApplication", "");
		if (!userApplication.isEmpty()) {
			String[] values = userApplication.split(",");
			String userApplicationRule = values[0];
			String userApplicationFilter = values[1];
			String condition = "USER_APPLICATION " + userApplicationRule + " '" + userApplicationFilter + "'";
			conditions.add(condition);
		}

		String directory;
		directory = prefs.get("adminHistory.directory", "");
		if (!directory.isEmpty()) {
			String[] values = directory.split(",");
			String directoryRule = values[0];
			String directoryFilter = values[1];
			String condition = "DIRECTORY " + directoryRule + " '" + directoryFilter + "'";
			conditions.add(condition);
		}

		String role;
		role = prefs.get("adminHistory.role", "");
		if (!role.isEmpty()) {
			String[] values = role.split(",");
			String roleRule = values[0];
			String roleFilter = values[1];
			String condition = "ROLE " + roleRule + " '" + roleFilter + "'";
			conditions.add(condition);
		}
		
		String result;
		result = prefs.get("adminRelationHistory.result", "");
		if (!result.isEmpty()) {
			String[] values = result.split(",");
			String resultRule = values[0];
			String resultFilter = values[1];
			String condition = "RESULT " + resultRule + " '" + resultFilter + "'";
			conditions.add(condition);
		}

		StringBuilder query = new StringBuilder();
		for (String condition : conditions) {
			if (conditions.indexOf(condition) == 0) {
				query.append(condition).toString();
			} else {
				query.append(" AND " + condition).toString();
			}
		}
		if (!query.toString().trim().isEmpty()) {
			return query.toString();
		} else {
			return null;
		}
	}

	/**
	 * Gets the query condition for base object history.
	 *
	 * @return the query condition for base object history
	 */
	private String getQueryConditionForBaseObjectHistory() {
		ArrayList<String> conditions = new ArrayList<String>();
		String stDate = prefs.get("adminBaseObject.stDate", "");
		String eDate = prefs.get("adminBaseObject.eDate", "");
		if (!stDate.isEmpty() && !eDate.isEmpty()) {
			String condition = "LOG_TIME > " + "'" + stDate + "'" + " AND LOG_TIME < " + "'" + eDate + "'";
			conditions.add(condition);
		} else if (!stDate.isEmpty()) {
			String condition = "LOG_TIME > " + "'" + stDate + "'";
			conditions.add(condition);
		} else if (!eDate.isEmpty()) {
			String condition = "LOG_TIME < " + "'" + eDate + "'";
			conditions.add(condition);
		}
		String adminName;
		adminName = prefs.get("adminBaseObject.adminName", "");
		if (!adminName.isEmpty()) {
			String[] values = adminName.split(",");
			String adminNameRule = values[0];
			String adminNameFilterText = values[1];
			String condition = "ADMIN_NAME " + adminNameRule + " '" + adminNameFilterText + "'";
			conditions.add(condition);
		}

		String operation;
		operation = prefs.get("adminBaseObject.operation", "");
		if (!operation.isEmpty()) {
			String[] values = operation.split(",");
			String operationRule = values[0];
			String operationFltr = values[1];
			String condition = "OPERATION " + operationRule + " '" + operationFltr + "'";
			conditions.add(condition);
		}

		String objectName;
		objectName = prefs.get("adminBaseObject.objectName", "");
		if (!objectName.isEmpty()) {
			String[] values = objectName.split(",");
			String objectNameRule = values[0];
			String objectFilter = values[1];
			String condition = "OBJECT_NAME " + objectNameRule + " '" + objectFilter + "'";
			conditions.add(condition);
		}

		String result;
		result = prefs.get("adminBaseObject.result", "");
		if (!result.isEmpty()) {
			String[] values = result.split(",");
			String resultRule = values[0];
			String resultFltr = values[1];
			String condition = "RESULT " + resultRule + " '" + resultFltr + "'";
			conditions.add(condition);
		}

		StringBuilder query = new StringBuilder();
		for (String condition : conditions) {
			if (conditions.indexOf(condition) == 0) {
				query.append(condition).toString();
			} else {
				query.append(" AND " + condition).toString();
			}
		}
		if (!query.toString().trim().isEmpty()) {
			return query.toString();
		} else {
			return null;
		}
	}

	/**
	 * The listener interface for receiving PGroupExpand events. The class that
	 * is interested in processing a PGroupExpand event implements this
	 * interface, and the object created with that class is registered with a
	 * component using the component's <code>addPGroupExpandListener<code>
	 * method. When the PGroupExpand event occurs, that object's appropriate
	 * method is invoked.
	 *
	 * @see PGroupExpandEvent
	 */
	private class PGroupExpandListener implements ExpandListener {

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * org.eclipse.swt.events.ExpandListener#itemExpanded(org.eclipse.swt.
		 * events.ExpandEvent)
		 */
		@Override
		public void itemExpanded(final ExpandEvent event) {
			pGroupRepaint((PGroup) event.widget);
			((PGroup) event.widget).forceFocus();
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * org.eclipse.swt.events.ExpandListener#itemCollapsed(org.eclipse.swt.
		 * events.ExpandEvent)
		 */
		@Override
		public void itemCollapsed(final ExpandEvent event) {
			pGroupRepaint((PGroup) event.widget);
		}

		/**
		 * Method for P group repaint.
		 *
		 * @param pGroup
		 *            the group
		 */
		private void pGroupRepaint(final PGroup pGroup) {
			pGroup.requestLayout();
			pGroup.redraw();
			pGroup.getParent().update();
			pGroup.getParent().getParent().notifyListeners(SWT.Resize, new Event());
			// groupScrolledComposite.notifyListeners(SWT.Resize, new Event());
		}
	}
	
	@Override
	public boolean close() {
		 final IJobManager manager = Job.getJobManager();
		  final Job[] jobs = manager.find(null); //return all jobs
		  for (Job job: jobs) {
			if(CommonConstants.History.ADMIN_HISTORY_JOB_NAME.equals(job.getName())) {
				if (job.getState() == Job.RUNNING) {
					job.done(Status.OK_STATUS);
				}
			}
		}
		return super.close();
	}

	/**
	 * Gets the new text based on new locale.
	 *
	 * @param message
	 *            {@link String}
	 * @param control
	 *            {@link Control}
	 * @return {@link String} new text
	 */
	private String getUpdatedWidgetText(final String message, final Control control) {
		control.requestLayout();
		control.getParent().redraw();
		control.getParent().getParent().update();
		control.getParent().getParent().getParent().update();
		return message;
	}
}
