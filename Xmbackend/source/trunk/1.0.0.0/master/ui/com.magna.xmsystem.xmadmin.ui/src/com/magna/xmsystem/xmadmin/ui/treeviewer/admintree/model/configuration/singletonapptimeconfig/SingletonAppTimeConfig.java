package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.singletonapptimeconfig;

import java.beans.PropertyChangeEvent;

import com.magna.xmsystem.dependencies.utils.XMSystemUtil;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.BeanModel;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.util.CommonConstants;

// TODO: Auto-generated Javadoc
/**
 * The Class SingletonAppTimeConfig.
 * 
 * @author archita.patel
 */
public class SingletonAppTimeConfig extends BeanModel implements IAdminTreeChild {

	/** The Constant MAX_TIME. */
	public static final int MAX_TIME = 7200;

	/** The Constant PROPERTY_SINGLETONAPP_TIME_ID. */
	public static final String PROPERTY_SINGLETONAPP_TIME_ID = "singletonAppTimeId"; //$NON-NLS-1$

	/** The Constant PROPERTY_SINGLETONAPP_TIME. */
	public static final String PROPERTY_SINGLETONAPP_TIME = "singletonAppTime"; //$NON-NLS-1$

	/** The Constant PROPERTY_OPERATIONMODE. */
	public static final String PROPERTY_OPERATIONMODE = "operationMode"; //$NON-NLS-1$

	/** The singleton app time config id. */
	private String singletonAppTimeConfigId;

	/** The singleton app time. */
	private String singletonAppTime;

	/** The operation mode. */
	private int operationMode;

	/** The parent. */
	private IAdminTreeChild parent;

	/**
	 * Instantiates a new singleton app time config.
	 *
	 * @param SingletonAppTimeId
	 *            the singleton app time id
	 * @param operationMode
	 *            the operation mode
	 */
	public SingletonAppTimeConfig(final String SingletonAppTimeId, final int operationMode) {
		this(SingletonAppTimeId, null, operationMode);
	}

	/**
	 * Instantiates a new singleton app time config.
	 *
	 * @param SingletonAppTimeId
	 *            the singleton app time id
	 * @param SingletonAppTime
	 *            the singleton app time
	 * @param operationMode
	 *            the operation mode
	 */
	public SingletonAppTimeConfig(final String SingletonAppTimeId, final String SingletonAppTime,
			final int operationMode) {
		super();
		this.singletonAppTimeConfigId = SingletonAppTimeId;
		this.singletonAppTime = SingletonAppTime;
		this.operationMode = operationMode;
	}

	/**
	 * Gets the singleton app time config id.
	 *
	 * @return the singleton app time config id
	 */
	public String getsingletonAppTimeConfigId() {
		return singletonAppTimeConfigId;
	}

	/**
	 * Sets the singleton app time config id.
	 *
	 * @param singletonAppTimeConfigId
	 *            the new singleton app time config id
	 */
	public void setsingletonAppTimeConfigId(String singletonAppTimeConfigId) {
		if (singletonAppTimeConfigId == null)
			throw new IllegalArgumentException();
		this.propertyChangeSupport.firePropertyChange(PROPERTY_SINGLETONAPP_TIME_ID, this.singletonAppTimeConfigId,
				this.singletonAppTimeConfigId = singletonAppTimeConfigId);
		// SingletonAppTimeId = singletonAppTimeId;
	}

	/**
	 * Gets the singleton app time.
	 *
	 * @return the singleton app time
	 */
	public String getSingletonAppTime() {
		return singletonAppTime;
	}

	/**
	 * Sets the singleton app time.
	 *
	 * @param singletonAppTime
	 *            the new singleton app time
	 */
	public void setSingletonAppTime(String singletonAppTime) {
		if (singletonAppTime == null)
			throw new IllegalArgumentException();
		this.propertyChangeSupport.firePropertyChange(PROPERTY_SINGLETONAPP_TIME, this.singletonAppTime,
				this.singletonAppTime = singletonAppTime.replace(",", ""));
		// this.singletonAppTime = singletonAppTime;
	}

	/**
	 * Gets the operation mode.
	 *
	 * @return the operation mode
	 */
	public int getOperationMode() {
		return operationMode;
	}

	/**
	 * Sets the operation mode.
	 *
	 * @param operationMode
	 *            the new operation mode
	 */
	public void setOperationMode(int operationMode) {
		this.propertyChangeSupport.firePropertyChange(PROPERTY_OPERATIONMODE, this.operationMode,
				this.operationMode = operationMode);
		// this.operationMode = operationMode;
	}

	/**
	 * Deep copy singleton app time config model.
	 *
	 * @param update
	 *            the update
	 * @param updateThisObject
	 *            the update this object
	 * @return the singleton app time config
	 */
	public SingletonAppTimeConfig deepCopySingletonAppTimeConfigModel(boolean update,
			SingletonAppTimeConfig updateThisObject) {

		SingletonAppTimeConfig clonedSingletonAppTimeConfig = null;
		try {
			String currentSingletonAppTimeId = XMSystemUtil.isEmpty(this.getsingletonAppTimeConfigId())
					? CommonConstants.EMPTY_STR : this.getsingletonAppTimeConfigId();

			String currentsingletonAppTime = XMSystemUtil.isEmpty(this.getSingletonAppTime())
					? CommonConstants.EMPTY_STR : this.getSingletonAppTime();

			if (update) {
				clonedSingletonAppTimeConfig = updateThisObject;
			} else {
				clonedSingletonAppTimeConfig = new SingletonAppTimeConfig(currentSingletonAppTimeId,
						currentsingletonAppTime, CommonConstants.OPERATIONMODE.VIEW);
			}
			clonedSingletonAppTimeConfig.setsingletonAppTimeConfigId(currentSingletonAppTimeId);
			clonedSingletonAppTimeConfig.setSingletonAppTime(currentsingletonAppTime);

			return clonedSingletonAppTimeConfig;
		} catch (Exception e) {
			return null;
		}

	}

	
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.BeanModel#
	 * propertyChange(java.beans.PropertyChangeEvent)
	 */
	@Override
	public void propertyChange(PropertyChangeEvent event) {
		this.propertyChangeSupport.firePropertyChange(event.getPropertyName(), event.getOldValue(), event.getNewValue());
	}

}
