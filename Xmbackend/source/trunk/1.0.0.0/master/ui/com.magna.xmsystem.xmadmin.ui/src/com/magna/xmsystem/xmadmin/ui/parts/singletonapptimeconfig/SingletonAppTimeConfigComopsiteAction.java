package com.magna.xmsystem.xmadmin.ui.parts.singletonapptimeconfig;

import java.util.EnumMap;
import java.util.List;

import javax.inject.Inject;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.beans.BeanProperties;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.core.databinding.observable.value.IValueChangeListener;
import org.eclipse.core.databinding.observable.value.ValueChangeEvent;
import org.eclipse.core.runtime.preferences.IEclipsePreferences;
import org.eclipse.e4.core.di.extensions.Preference;
import org.eclipse.e4.core.services.nls.Translation;
import org.eclipse.e4.ui.model.application.ui.MDirtyable;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.jface.fieldassist.ControlDecoration;
import org.eclipse.jface.fieldassist.FieldDecorationRegistry;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Spinner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.ResourceAccessException;

import com.magna.xmbackend.entities.PropertyConfigTbl;
import com.magna.xmbackend.vo.enums.Singleton;
import com.magna.xmsystem.dependencies.customExceptions.UnauthorizedAccessException;
import com.magna.xmsystem.dependencies.utils.XMSystemUtil;
import com.magna.xmsystem.ui.controls.widgets.CustomMessageDialog;
import com.magna.xmsystem.xmadmin.message.Message;
import com.magna.xmsystem.xmadmin.message.MessageRegistry;
import com.magna.xmsystem.xmadmin.restclient.propertyConfig.PropertyConfigController;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.AdminTreeviewer;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.singletonapptimeconfig.SingletonAppTimeConfig;
import com.magna.xmsystem.xmadmin.util.CommonConstants;
import com.magna.xmsystem.xmadmin.util.MessageType;
import com.magna.xmsystem.xmadmin.util.XMAdminUtil;

/**
 * The Class SingletonAppTimeConfigComopsiteAction.
 * 
 * @author archita.patel
 */
@SuppressWarnings("restriction")
public class SingletonAppTimeConfigComopsiteAction extends SingletonAppTimeConfigComopsiteUI {

	/** Logger instance. */
	private static final Logger LOGGER = LoggerFactory.getLogger(SingletonAppTimeConfigComopsiteAction.class);

	/** Member variable for widgetValue. */
	transient private IObservableValue<?> widgetValue;

	/** Member variable for modelValue. */
	transient private IObservableValue<?> modelValue;
	/**
	 * Member variable for data binding context the DataBindingContext object
	 * will manage the databindings.
	 */
	final DataBindingContext dataBindContext = new DataBindingContext();

	/** Member variable for {@link MessageRegistry}. */
	@Inject
	private MessageRegistry registry;

	/** Member variable for messages. */
	@Inject
	@Translation
	transient private Message messages;

	/** The smtp configuration model. */
	private SingletonAppTimeConfig singletonAppTimeConfigModel;

	/** The old model. */
	private SingletonAppTimeConfig oldModel;

	/** Member variable for dirty. */
	transient private MDirtyable dirty;

	/** The singleton key value. */
	private EnumMap<Singleton, String> singletonKeyValue;

	/**
	 * Instantiates a new singleton app time config comopsite action.
	 *
	 * @param parent
	 *            the parent
	 * @param preferences
	 *            the preferences
	 */
	@Inject
	public SingletonAppTimeConfigComopsiteAction(final Composite parent, @Preference IEclipsePreferences preferences) {
		super(parent, SWT.NONE);
		initListener();

	}

	/**
	 * Inits the listener.
	 */
	private void initListener() {
		if (this.btnSave != null) {
			this.btnSave.addSelectionListener(new SelectionAdapter() {

				/**
				 * Save button handler
				 */
				@Override
				public void widgetSelected(final SelectionEvent event) {
					saveSingletonAppTime();
				}
			});
		}
		if (this.btnCancel != null) {
			this.btnCancel.addSelectionListener(new SelectionAdapter() {

				/**
				 * Save button handler
				 */
				@Override
				public void widgetSelected(final SelectionEvent event) {
					cancelSingletonAppTimeConfig();
				}
			});
		}

		this.timeSpinner.addModifyListener(new ModifyListener() {
			ControlDecoration txtNameDecroator = new ControlDecoration(timeSpinner, SWT.TOP);
			
			ControlDecoration txtInfoDecroator = new ControlDecoration(timeSpinner, SWT.TOP);
			
			@Override
			public void modifyText(ModifyEvent event) {
				Spinner timeSpinner = (Spinner) event.widget;
				String updatedText = timeSpinner.getText();
				final Image nameDecoratorImage = FieldDecorationRegistry.getDefault()
						.getFieldDecoration(FieldDecorationRegistry.DEC_ERROR).getImage();
				final Image nameInfoDecoratorImage = FieldDecorationRegistry.getDefault()
						.getFieldDecoration(FieldDecorationRegistry.DEC_INFORMATION).getImage();
				txtNameDecroator.setImage(nameDecoratorImage);
				txtInfoDecroator.setImage(nameInfoDecoratorImage);
				txtInfoDecroator.setShowOnlyOnFocus(true);
				txtInfoDecroator.setDescriptionText(messages.infoTimeSpinnerValidate);
				if (updatedText.trim().length() <= 0) {
					txtNameDecroator.setDescriptionText(messages.emptyTimeSpinnerValidateErr);
					txtNameDecroator.show();
					txtInfoDecroator.hide();
					updateButtonStatus();
				} else {
					txtNameDecroator.hide();
					txtInfoDecroator.show();
					/*txtInfoDecroator.setDescriptionText(messages.infoTimeSpinnerValidate);
					txtInfoDecroator.show();*/
				}

			}
		});

	}

	/**
	 * Cancel singleton app time config.
	 */
	public void cancelSingletonAppTimeConfig() {
		if (this.singletonAppTimeConfigModel == null) {
			dirty.setDirty(false);
			return;
		}
		setSingletonAppTimeConfigModel(null);
		setOldModel(null);
		dirty.setDirty(false);
		final AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
		final IStructuredSelection selection = (IStructuredSelection) adminTree.getSelection();
		final Object firstElement = selection.getFirstElement();
		adminTree.setSelection(new StructuredSelection(firstElement), true);
	}

	/**
	 * Save singleton app time.
	 */
	public void saveSingletonAppTime() {
		try {
			PropertyConfigController propertyConfigController = new PropertyConfigController();
			boolean isUpdate = propertyConfigController.updateSingleAppTime(mapVOObjectWithModel());
			if (isUpdate) {
				setOldModel(singletonAppTimeConfigModel.deepCopySingletonAppTimeConfigModel(true, getOldModel()));
				singletonAppTimeConfigModel.setOperationMode(CommonConstants.OPERATIONMODE.VIEW);
				setOperationMode();
				this.dirty.setDirty(false);
				XMAdminUtil.getInstance().getAdminTree().refresh();
				XMAdminUtil.getInstance().getAdminTree().setSelection(new StructuredSelection(getOldModel()), true);
				XMAdminUtil.getInstance().updateLogFile(messages.singletonAppTimeNode + " " + messages.objectUpdate,
						MessageType.SUCCESS);
			}
		} catch (UnauthorizedAccessException e) {
			CustomMessageDialog.openError(this.getShell(), messages.objectPermissionDialogTitle,
					messages.objectPermissionDialogMsg);
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				CustomMessageDialog.openError(Display.getCurrent().getActiveShell(), messages.errorDialogTitile,
						messages.serverNotReachable);
			}
			LOGGER.error("Exception occured while saving singletonApp Time  configuration", e);
			e.printStackTrace();
		}

	}

	/**
	 * Map VO object with model.
	 *
	 * @return the com.magna.xmbackend.vo.prop config. singleton config request
	 */
	private com.magna.xmbackend.vo.propConfig.SingletonConfigRequest mapVOObjectWithModel() {
		com.magna.xmbackend.vo.propConfig.SingletonConfigRequest singletonRequest = new com.magna.xmbackend.vo.propConfig.SingletonConfigRequest();
		singletonKeyValue = new EnumMap<Singleton, String>(Singleton.class);
		Singleton singleAppTime = Singleton.SINGLETON_APP_TIME;
		singletonKeyValue.put(singleAppTime, singletonAppTimeConfigModel.getSingletonAppTime());
		singletonRequest.setSingletonKeyValue(singletonKeyValue);
		return singletonRequest;
	}

	/**
	 * Register messages.
	 *
	 * @param registry
	 *            the registry
	 */
	public void registerMessages(final MessageRegistry registry) {
		registry.register((text) -> {
			if (grpSingletonAppTimeConfig != null && !grpSingletonAppTimeConfig.isDisposed()) {
				grpSingletonAppTimeConfig.setText(text);
			}
		}, (message) -> {
			return message.singletonAppTimeGroupLabel;
		});

		registry.register((text) -> {
			if (lblSingletonAppTime != null && !lblSingletonAppTime.isDisposed()) {
				lblSingletonAppTime.setText(text);
			}
		}, (message) -> {
			if (lblSingletonAppTime != null && !lblSingletonAppTime.isDisposed()) {
				return getUpdatedWidgetText(message.singletonAppTimelabel, lblSingletonAppTime);
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (lblsecTime != null && !lblsecTime.isDisposed()) {
				lblsecTime.setText(text);
			}
		}, (message) -> {
			if (lblsecTime != null && !lblsecTime.isDisposed()) {
				return getUpdatedWidgetText(message.singletonAppTimeSecondsLabel, lblsecTime);
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (btnSave != null && !btnSave.isDisposed()) {
				btnSave.setText(text);
			}
		}, (message) -> {
			if (btnSave != null && !btnSave.isDisposed()) {
				return getUpdatedWidgetText(message.saveButtonText, btnSave);
			}
			return CommonConstants.EMPTY_STR;
		});
		registry.register((text) -> {
			if (btnCancel != null && !btnCancel.isDisposed()) {
				btnCancel.setText(text);
			}
		}, (message) -> {
			if (btnCancel != null && !btnCancel.isDisposed()) {
				return getUpdatedWidgetText(message.cancelButtonText, btnCancel);
			}
			return CommonConstants.EMPTY_STR;
		});
	}

	/**
	 * Bind values.
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void bindValues() {
		widgetValue = WidgetProperties.selection().observe(this.timeSpinner);
		modelValue = BeanProperties
				.value(SingletonAppTimeConfig.class, SingletonAppTimeConfig.PROPERTY_SINGLETONAPP_TIME)
				.observe(this.singletonAppTimeConfigModel);
		dataBindContext.bindValue(widgetValue, modelValue);
		widgetValue.addValueChangeListener(new IValueChangeListener() {
			@Override
			public void handleValueChange(ValueChangeEvent event) {
				updateButtonStatus();
			}
		});
	}

	/**
	 * Method to update the button status
	 * 
	 * @param event
	 */
	private void updateButtonStatus() {
		final String nameText = timeSpinner.getText().trim();
		if (this.btnSave != null) {
			if ((XMSystemUtil.isEmpty(nameText) || nameText.trim().length() == 0)) {
				this.btnSave.setEnabled(false);
			} else {
				this.btnSave.setEnabled(true);
			}
		}
	}

	/**
	 * Sets the operation mode.
	 */
	public void setOperationMode() {
		if (this.singletonAppTimeConfigModel != null) {
			if (singletonAppTimeConfigModel.getOperationMode() == CommonConstants.OPERATIONMODE.VIEW) {
				this.timeSpinner.setEnabled(false);
				setShowButtonBar(false);
			} else if (singletonAppTimeConfigModel.getOperationMode() == CommonConstants.OPERATIONMODE.CHANGE) {
				this.timeSpinner.setEnabled(true);
				setShowButtonBar(true);
				this.dirty.setDirty(true);
			} else {
				this.timeSpinner.setEnabled(false);
				setShowButtonBar(false);
			}
		}
	}

	/**
	 * Sets the show button bar.
	 *
	 * @param showButtonBar
	 *            the new show button bar
	 */
	protected void setShowButtonBar(final boolean showButtonBar) {
		if (this.btnSave != null && !this.btnSave.isDisposed() && this.btnCancel != null
				&& !this.btnCancel.isDisposed()) {
			final GridData layoutData = (GridData) this.btnSave.getParent().getLayoutData();
			layoutData.exclude = !showButtonBar;
			this.btnSave.setVisible(showButtonBar);
			this.btnCancel.setVisible(showButtonBar);
			this.btnSave.getParent().requestLayout();
			this.btnSave.getParent().redraw();
			this.btnSave.getParent().getParent().update();
		}
	}

	/**
	 * Gets the updated widget text.
	 *
	 * @param message
	 *            the message
	 * @param control
	 *            the control
	 * @return the updated widget text
	 */
	private String getUpdatedWidgetText(final String message, final Control control) {
		control.requestLayout();
		control.getParent().redraw();
		control.getParent().getParent().update();
		control.getParent().getParent().getParent().update();
		return message;
	}

	/**
	 * Sets the singleton app time config.
	 */
	public void setSingletonAppTimeConfig() {
		final AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
		final Object selectionObj = adminTree.getSelection();
		if (selectionObj instanceof IStructuredSelection) {
			Object firstElement = ((IStructuredSelection) selectionObj).getFirstElement();
			if (firstElement instanceof SingletonAppTimeConfig) {
				singletonAppTimeConfigModel = (SingletonAppTimeConfig) firstElement;
				PropertyConfigController propertyConfigController = new PropertyConfigController();
				List<PropertyConfigTbl> propVOList = propertyConfigController.getAllSingletonTime(true);
				for (PropertyConfigTbl propertyTbl : propVOList) {
					String property = propertyTbl.getProperty();
					String propertyValue = propertyTbl.getValue();
					if (Singleton.SINGLETON_APP_TIME.toString().equals(property)) {
						this.singletonAppTimeConfigModel.setSingletonAppTime(propertyValue);
					}
				}
				// singletonAppTimeConfigModel.setSingletonAppTime(preferences.get("singletonAppTime",
				// "Time"));

				setOldModel(singletonAppTimeConfigModel);
				setSingletonAppTimeConfigModel(this.getOldModel().deepCopySingletonAppTimeConfigModel(false, null));
				registerMessages(this.registry);
				bindValues();
				setOperationMode();
			}
		}
	}

	/**
	 * Sets the dirty object.
	 *
	 * @param dirty
	 *            the new dirty object
	 */
	public void setDirtyObject(final MDirtyable dirty) {
		this.dirty = dirty;
	}

	/**
	 * Gets the singleton app time config model.
	 *
	 * @return the singleton app time config model
	 */
	public SingletonAppTimeConfig getSingletonAppTimeConfigModel() {
		return singletonAppTimeConfigModel;
	}

	/**
	 * Sets the singleton app time config model.
	 *
	 * @param singletonAppTimeConfigModel
	 *            the new singleton app time config model
	 */
	public void setSingletonAppTimeConfigModel(final SingletonAppTimeConfig singletonAppTimeConfigModel) {
		this.singletonAppTimeConfigModel = singletonAppTimeConfigModel;
	}

	/**
	 * Gets the old model.
	 *
	 * @return the old model
	 */
	public SingletonAppTimeConfig getOldModel() {
		return oldModel;
	}

	/**
	 * Sets the old model.
	 *
	 * @param oldModel
	 *            the new old model
	 */
	public void setOldModel(SingletonAppTimeConfig oldModel) {
		this.oldModel = oldModel;
	}

}
