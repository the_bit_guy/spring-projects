package com.magna.xmsystem.xmadmin.ui.parts.dndperspective;

import javax.inject.Inject;

import org.eclipse.e4.core.services.nls.Translation;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.graphics.Image;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magna.xmsystem.dependencies.utils.XMSystemUtil;
import com.magna.xmsystem.xmadmin.message.Message;
import com.magna.xmsystem.xmadmin.ui.parts.dndperspective.model.AAWarpperObj;
import com.magna.xmsystem.xmadmin.ui.parts.dndperspective.model.ProjectWrapperObj;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.TreeLoading;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaInfo;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaInfoHistory;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaInfoStatus;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaProjectAppFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaProjectAppNotFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaProjectAppProtected;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaProjectApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaProjectStartApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaProjects;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaStartApp;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaStartApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaUserAppFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaUserAppNotFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaUserAppProtected;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaUserApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdministrationArea;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdministrationAreas;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.Applications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.baseapplicationmodel.BaseAppInformationHistory;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.baseapplicationmodel.BaseAppInformationModel;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.baseapplicationmodel.BaseAppInformationStatus;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.baseapplicationmodel.BaseAppProjectApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.baseapplicationmodel.BaseAppStartApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.baseapplicationmodel.BaseAppUserApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.baseapplicationmodel.BaseApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.baseapplicationmodel.BaseApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.projectapplicationmodel.ProjectAppAdminAreaProjects;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.projectapplicationmodel.ProjectAppAdminAreas;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.projectapplicationmodel.ProjectAppGroups;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.projectapplicationmodel.ProjectAppUsers;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.projectapplicationmodel.ProjectApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.projectapplicationmodel.ProjectApplicationChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.projectapplicationmodel.ProjectApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.startapplicationmodel.StartAppAdminAreas;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.startapplicationmodel.StartAppProjects;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.startapplicationmodel.StartAppUsers;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.startapplicationmodel.StartApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.startapplicationmodel.StartApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.userapplicationmodel.UserAppAdminAreas;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.userapplicationmodel.UserAppGroups;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.userapplicationmodel.UserAppUsers;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.userapplicationmodel.UserApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.userapplicationmodel.UserApplicationChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.userapplicationmodel.UserApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.Configurations;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.adminconf.AdminMenu;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.adminconf.AdminProjectApps;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.adminconf.AdminUserApps;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.adminconf.AdminUsers;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.icon.Icon;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.icon.Icons;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.ldapconfig.LdapConfiguration;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.livemsgconfig.LiveMessage;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.livemsgconfig.LiveMessages;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.noProjectMessage.NoProjectPopMessage;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.Notification;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.projectexpiryconfig.ProjectExpiryConfig;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.Role;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.RoleScopeObjectUsers;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.RoleScopeObjects;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.RoleUsers;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.Roles;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.singletonapptimeconfig.SingletonAppTimeConfig;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.smtpconfig.SmtpConfiguration;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.directory.Directories;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.directory.Directory;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.directory.DirectoryApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.directory.DirectoryProjectApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.directory.DirectoryProjects;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.directory.DirectoryUserApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.directory.DirectoryUsers;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.Groups;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProUserProjectApp;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.Project;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectAdminAreaProjectAppFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectAdminAreaProjectAppNotFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectAdminAreaProjectAppProtected;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectAdminAreaProjectApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectAdminAreaStartApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectAdminAreas;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectInfoHistory;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectInfoStatus;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectInformation;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectProjectApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectUserAAProjectAppAllowed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectUserAAProjectAppForbidden;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectUserAAProjectApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectUserAdminAreas;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectUsers;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.Projects;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.refs.RelationObj;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.Site;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaInformationHistory;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaInformationStatus;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaInformations;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaProjectApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaProjectChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaProjectStartApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaProjects;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaStartApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaUserAppFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaUserAppNotFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaUserAppProtected;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaUserApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminProjectAppFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminProjectAppNotFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminProjectAppProtected;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdministrationChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdministrations;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.Sites;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.User;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserAAUserAppAllowed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserAAUserAppForbidden;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserAAUserApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserAdminAreas;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserGroups;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserInformationHistory;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserInformationStatus;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserInformations;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserProjectAAProjectAppAllowed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserProjectAAProjectAppForbidden;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserProjectAAProjectApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserProjectAdminAreas;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserProjects;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserStartApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.Users;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UsersNameAlphabet;
import com.magna.xmsystem.xmadmin.util.CommonConstants;

/**
 * The Class ParkingLotObjectLabelProvider.
 */
public class ParkingLotObjectLabelProvider extends LabelProvider {
	/**
	 * Logger instance
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(ParkingLotObjectLabelProvider.class);

	/**
	 * Member variable for {@link Message}
	 */
	@Inject
	@Translation
	private Message messages;

	/**
	 * Constructor
	 */
	@Inject
	public ParkingLotObjectLabelProvider() {
		super();
	}

	/**
	 * Get the text for given object
	 * 
	 * @param element
	 *            {@link Object}
	 */
	@Override
	public String getText(final Object element) {
		if (element instanceof TreeLoading) {
			return messages.treeLoading;
		} else if (element instanceof Sites) {
			return messages.sitesNode;
		} else if (element instanceof Site) {
			return ((Site) element).getName();
		} else if (element instanceof SiteAdministrations || element instanceof ProjectAdminAreas) {
			return messages.administrationAreasNode;
		} else if (element instanceof SiteAdminAreaProjects) {
			return messages.projectsNode;
		} else if (element instanceof SiteAdminAreaUserApplications) {
			return messages.userApplicationsNode;
		} else if (element instanceof SiteAdminAreaStartApplications) {
			return messages.startApplicationsNode;
		} else if (element instanceof SiteAdminAreaInformations) {
			return messages.informationNode;
		} else if (element instanceof UserInformations) {
			return messages.informationNode;
		} else if (element instanceof UserInformationStatus) {
			return messages.statusNode;
		} else if (element instanceof UserInformationHistory) {
			return messages.historyNode;
		} else if (element instanceof SiteAdminAreaProjectApplications) {
			return messages.projectApplicationsNode;
		} else if (element instanceof SiteAdminAreaProjectStartApplications) {
			return messages.startApplicationsNode;
		} else if (element instanceof SiteAdminProjectAppNotFixed) {
			return messages.notFixedNode;
		} else if (element instanceof SiteAdminProjectAppFixed) {
			return messages.fixedNode;
		} else if (element instanceof SiteAdminProjectAppProtected) {
			return messages.protectedNode;
		} else if (element instanceof SiteAdminAreaUserAppNotFixed) {
			return messages.notFixedNode;
		} else if (element instanceof SiteAdminAreaUserAppFixed) {
			return messages.fixedNode;
		} else if (element instanceof SiteAdminAreaUserAppProtected) {
			return messages.protectedNode;
		} else if (element instanceof SiteAdminAreaInformationStatus) {
			return messages.statusNode;
		} else if (element instanceof SiteAdminAreaInformationHistory) {
			return messages.historyNode;
		} else if (element instanceof AdministrationAreas) {
			return messages.administrationAreasNode;
		} else if (element instanceof AAWarpperObj) {
			AAWarpperObj aaWrapperObj = (AAWarpperObj) element;
			String returnVal = aaWrapperObj.getName(); 
			IAdminTreeChild relatedObj;
			if ((relatedObj = aaWrapperObj.getRelatedObj()) != null) {
				if (relatedObj instanceof SiteAdministrationChild) {
					Site site = (Site) relatedObj.getParent().getParent();
					return site.getName() + " / " + returnVal;
				}
			}
			return returnVal;
		} else if (element instanceof ProjectWrapperObj) {
			ProjectWrapperObj projectWrapperObj = (ProjectWrapperObj) element;
			String returnVal = projectWrapperObj.getName(); 
			IAdminTreeChild relatedObj;
			if ((relatedObj = projectWrapperObj.getRelatedObj()) != null) {
				if (relatedObj instanceof SiteAdminAreaProjectChild) {
					RelationObj relationObj = (RelationObj) relatedObj.getParent().getParent();
					AdministrationArea administrationArea = (AdministrationArea) relationObj.getRefObject();
					Site site = (Site) relationObj.getParent().getParent();
					return site.getName() + " / " + administrationArea.getName() + " / " + returnVal;
				}
			}
			return returnVal;
		} else if (element instanceof AdminAreaUserApplications) {
			return messages.userApplicationsNode;
		} else if (element instanceof AdminAreaUserAppFixed) {
			return messages.fixedNode;
		} else if (element instanceof AdminAreaUserAppNotFixed) {
			return messages.notFixedNode;
		} else if (element instanceof AdminAreaUserAppProtected) {
			return messages.protectedNode;
		} else if (element instanceof AdminAreaProjectApplications) {
			return messages.projectApplicationsNode;
		} else if (element instanceof AdminAreaProjectAppFixed) {
			return messages.fixedNode;
		} else if (element instanceof AdminAreaProjectAppNotFixed) {
			return messages.notFixedNode;
		} else if (element instanceof AdminAreaProjectAppProtected) {
			return messages.protectedNode;
		} else if (element instanceof AdminAreaStartApplications) {
			return messages.startApplicationsNode;
		} else if (element instanceof AdminAreaProjectStartApplications) {
			return messages.startApplicationsNode;
		} else if (element instanceof AdminAreaProjects) {
			return messages.projectsNode;
		} else if (element instanceof AdminAreaInfo) {
			return messages.informationNode;
		} else if (element instanceof AdminAreaInfoHistory) {
			return messages.historyNode;
		} else if (element instanceof AdminAreaInfoStatus) {
			return messages.statusNode;
		} else if (element instanceof AdminAreaStartApp) {
			return messages.startApplicationsNode;
		} else if (element instanceof Users) {
			return messages.usersNode;
		} else if (element instanceof UsersNameAlphabet) {
			return ((UsersNameAlphabet) element).getFirstChar();
		} else if (element instanceof User) {
			return ((User) element).getName();
		} else if (element instanceof UserProjects) {
			return messages.projectsNode;
		} else if (element instanceof UserProjectAdminAreas) {
			return messages.administrationAreasNode;
		} else if (element instanceof UserProjectAAProjectApplications) {
			return messages.projectApplicationsNode;
		} else if (element instanceof UserProjectAAProjectAppAllowed) {
			return messages.allowedNode;
		} else if (element instanceof UserProjectAAProjectAppForbidden) {
			return messages.forbiddenNode;
		} else if (element instanceof UserAAUserAppAllowed) {
			return messages.allowedNode;
		} else if (element instanceof UserAAUserAppForbidden) {
			return messages.forbiddenNode;
		} else if (element instanceof UserAdminAreas) {
			return messages.administrationAreasNode;
		} else if (element instanceof UserAAUserApplications) {
			return messages.userApplicationsNode;
		} else if (element instanceof UserStartApplications) {
			return messages.startApplicationsNode;
		} else if (element instanceof UserGroups) {
			return messages.groupsNode;
		} else if (element instanceof Projects) {
			return messages.projectsNode;
		} else if (element instanceof Project) {
			return ((Project) element).getName();
		} else if (element instanceof ProjectUsers) {
			return messages.usersNode;
		} else if (element instanceof ProjectUserAdminAreas) {
			return messages.administrationAreasNode;
		} else if (element instanceof ProjectUserAAProjectApplications) {
			return messages.projectApplicationsNode;
		} else if (element instanceof ProjectUserAAProjectAppAllowed) {
			return messages.allowedNode;
		} else if (element instanceof ProjectUserAAProjectAppForbidden) {
			return messages.forbiddenNode;
		}else if (element instanceof ProjectProjectApplications) {
			return messages.projectApplicationsNode;
		} else if (element instanceof ProUserProjectApp || element instanceof ProjectAdminAreaProjectApplications) {
			return messages.projectApplicationsNode;
		} else if (element instanceof ProjectAdminAreaProjectAppNotFixed) {
			return messages.notFixedNode;
		} else if (element instanceof ProjectAdminAreaProjectAppFixed) {
			return messages.fixedNode;
		} else if (element instanceof ProjectAdminAreaProjectAppProtected) {
			return messages.protectedNode;
		} else if (element instanceof ProjectAdminAreaStartApplications) {
			return messages.startApplicationsNode;
		} else if (element instanceof ProjectInformation) {
			return messages.informationNode;
		} else if (element instanceof ProjectInfoStatus) {
			return messages.statusNode;
		} else if (element instanceof ProjectInfoHistory) {
			return messages.historyNode;
		} else if (element instanceof ProjectInfoHistory) {
			return messages.historyNode;
		} else if (element instanceof Applications) {
			return messages.applicationsNode;
		} else if (element instanceof UserApplications) {
			return messages.userApplicationsNode;
		} else if (element instanceof UserAppAdminAreas) {
			return messages.administrationAreasNode;
		} else if (element instanceof UserAppUsers) {
			return messages.usersNode;
		} else if (element instanceof UserAppGroups) {
			return messages.groupsNode;
		} else if (element instanceof ProjectAppAdminAreas) {
			return messages.administrationAreasNode;
		} else if (element instanceof ProjectAppAdminAreaProjects) {
			return messages.projectsNode;
		} else if (element instanceof ProjectAppUsers) {
			return messages.usersNode;
		} else if (element instanceof ProjectAppGroups) {
			return messages.groupsNode;
		} else if (element instanceof StartAppUsers) {
			return messages.usersNode;
		} else if (element instanceof StartAppProjects) {
			return messages.projectsNode;
		} else if (element instanceof StartAppAdminAreas) {
			return messages.administrationAreasNode;
		} else if (element instanceof BaseAppProjectApplications) {
			return messages.projectApplicationsNode;
		} else if (element instanceof BaseAppUserApplications) {
			return messages.userApplicationsNode;
		} else if (element instanceof BaseAppStartApplications) {
			return messages.startApplicationsNode;
		} else if (element instanceof BaseAppInformationModel) {
			return messages.informationNode;
		} else if (element instanceof BaseAppInformationStatus) {
			return messages.statusNode;
		} else if (element instanceof BaseAppInformationHistory) {
			return messages.historyNode;
		} else if (element instanceof UserApplication) {
			return ((UserApplication) element).getName();
		} else if (element instanceof UserApplicationChild) {
			return ((UserApplicationChild) element).getName();
		} else if (element instanceof ProjectApplications) {
			return messages.projectApplicationsNode;
		} else if (element instanceof ProjectApplication) {
			return ((ProjectApplication) element).getName();
		} else if (element instanceof ProjectApplicationChild) {
			return ((ProjectApplicationChild) element).getName();
		} else if (element instanceof StartApplications) {
			return messages.startApplicationsNode;
		} else if (element instanceof StartApplication) {
			return ((StartApplication) element).getName();
		} else if (element instanceof BaseApplications) {
			return messages.baseApplicationsNode;
		} else if (element instanceof BaseApplication) {
			return ((BaseApplication) element).getName();
		} else if (element instanceof Groups) {
			return messages.groupsNode;
		} else if (element instanceof Directories) {
			return messages.directoryNode;
		} else if (element instanceof Directory) {
			return ((Directory) element).getName();
		} else if (element instanceof DirectoryUsers) {
			return messages.usersNode;
		} else if (element instanceof DirectoryApplications) {
			return messages.applicationsNode;
		} else if (element instanceof DirectoryProjects) {
			return messages.projectsNode;
		} else if (element instanceof DirectoryUserApplications) {
			return messages.userApplicationsNode;
		} else if (element instanceof DirectoryProjectApplications) {
			return messages.projectApplicationsNode;
		} else if (element instanceof Configurations) {
			return messages.configurationNode;
		} else if (element instanceof AdminMenu) {
			return messages.adminMenuNode;
		} else if (element instanceof AdminUsers) {
			return messages.usersNode;
		} else if (element instanceof AdminUserApps) {
			return messages.userApplicationsNode;
		} else if (element instanceof AdminProjectApps) {
			return messages.projectApplicationsNode;
		} else if (element instanceof RelationObj) {
			IAdminTreeChild refObject = ((RelationObj) element).getRefObject();
			if (refObject instanceof Site) {
				return ((Site) refObject).getName();
			} else if (refObject instanceof AdministrationArea) {
				return ((AdministrationArea) refObject).getName();
			} else if (refObject instanceof Project) {
				return ((Project) refObject).getName();
			} else if (refObject instanceof User) {
				return ((User) refObject).getName();
			} else if (refObject instanceof Directory) {
				return ((Directory) refObject).getName();
			} else if (element instanceof UserApplication) {
				return ((UserApplication) element).getName();
			} else if (element instanceof ProjectApplication) {
				return ((ProjectApplication) element).getName();
			} else if (element instanceof StartApplication) {
				return ((StartApplication) element).getName();
			} else if (element instanceof BaseApplication) {
				return ((BaseApplication) element).getName();
			}
		}

		return super.getText(element.getClass() + " " + element.hashCode());
	}

	/**
	 * Get the image for object
	 * 
	 * @param element
	 *            {@link Object}
	 */
	@Override
	public Image getImage(final Object element) {
		Icon icon;
		try {
			XMSystemUtil instance = XMSystemUtil.getInstance();
			if (element instanceof TreeLoading) {
				return instance.getImage(this.getClass(), "icons/16x16/loading.png");
			} else if (element instanceof Sites) {
				return instance.getImage(this.getClass(), "icons/16x16/sites.png");
			} else if (element instanceof SiteAdministrations || element instanceof ProjectAdminAreas) {
				return instance.getImage(this.getClass(), "icons/16x16/administrationarea.png");
			} else if (element instanceof SiteAdminAreaProjects) {
				return instance.getImage(this.getClass(), "icons/16x16/projects.png");
			} else if (element instanceof SiteAdminAreaUserApplications) {
				return instance.getImage(this.getClass(), "icons/16x16/ausertasks.png");
			} else if (element instanceof UserProjects) {
				return instance.getImage(this.getClass(), "icons/16x16/projects.png");
			} else if (element instanceof UserProjectAdminAreas) {
				return instance.getImage(this.getClass(), "icons/16x16/administrationarea.png");
			} else if (element instanceof UserProjectAAProjectApplications) {
				return instance.getImage(this.getClass(), "icons/16x16/projecttasks.gif");
			} else if (element instanceof UserProjectAAProjectAppAllowed) {
				return instance.getImage(this.getClass(), "icons/16x16/allowed.gif");
			} else if (element instanceof UserProjectAAProjectAppForbidden) {
				return instance.getImage(this.getClass(), "icons/16x16/dprojecttasks.gif");
			} else if (element instanceof UserAdminAreas) {
				return instance.getImage(this.getClass(), "icons/16x16/administrationarea.png");
			} else if (element instanceof UserAAUserApplications) {
				return instance.getImage(this.getClass(), "icons/16x16/ausertasks.png");
			} else if (element instanceof UserAAUserAppAllowed) {
				return instance.getImage(this.getClass(), "icons/16x16/ausertasks.png");
			} else if (element instanceof UserAAUserAppForbidden) {
				return instance.getImage(this.getClass(), "icons/16x16/dusertasks.gif");
			} else if (element instanceof UserStartApplications) {
				return instance.getImage(this.getClass(), "icons/16x16/startapplications.png");
			} else if (element instanceof UserGroups) {
				return instance.getImage(this.getClass(), "icons/16x16/groups.png");
			} else if (element instanceof UserInformations) {
				return instance.getImage(this.getClass(), "icons/16x16/info.gif");
			} else if (element instanceof UserInformationStatus) {
				return instance.getImage(this.getClass(), "icons/16x16/status.gif");
			} else if (element instanceof UserInformationHistory) {
				return instance.getImage(this.getClass(), "icons/16x16/history.gif");
			} else if (element instanceof SiteAdminAreaStartApplications) {
				return instance.getImage(this.getClass(), "icons/16x16/startapplications.png");
			} else if (element instanceof SiteAdminAreaInformations) {
				return instance.getImage(this.getClass(), "icons/16x16/info.gif");
			} else if (element instanceof SiteAdminAreaProjectApplications) {
				return instance.getImage(this.getClass(), "icons/16x16/projecttasks.gif");
			} else if (element instanceof SiteAdminAreaProjectStartApplications) {
				return instance.getImage(this.getClass(), "icons/16x16/startapplications.png");
			} else if (element instanceof SiteAdminProjectAppNotFixed) {
				return instance.getImage(this.getClass(), "icons/16x16/notfixed.gif");
			} else if (element instanceof SiteAdminProjectAppFixed) {
				return instance.getImage(this.getClass(), "icons/16x16/fixed.gif");
			} else if (element instanceof SiteAdminProjectAppProtected) {
				return instance.getImage(this.getClass(), "icons/16x16/protected.gif");
			} else if (element instanceof SiteAdminAreaUserAppNotFixed) {
				return instance.getImage(this.getClass(), "icons/16x16/notfixed.gif");
			} else if (element instanceof SiteAdminAreaUserAppFixed) {
				return instance.getImage(this.getClass(), "icons/16x16/fixed.gif");
			} else if (element instanceof SiteAdminAreaInformationStatus) {
				return instance.getImage(this.getClass(), "icons/16x16/status.gif");
			} else if (element instanceof SiteAdminAreaInformationHistory) {
				return instance.getImage(this.getClass(), "icons/16x16/history.gif");
			} else if (element instanceof SiteAdminAreaUserAppProtected) {
				return instance.getImage(this.getClass(), "icons/16x16/protected.gif");
			} else if (element instanceof AdministrationAreas) {
				return instance.getImage(this.getClass(), "icons/16x16/administrationarea.png");
			} else if (element instanceof AdminAreaUserApplications) {
				return instance.getImage(this.getClass(), "icons/16x16/ausertasks.png");
			} else if (element instanceof AdminAreaUserAppFixed) {
				return instance.getImage(this.getClass(), "icons/16x16/fixed.gif");
			} else if (element instanceof AdminAreaUserAppNotFixed) {
				return instance.getImage(this.getClass(), "icons/16x16/notfixed.gif");
			} else if (element instanceof AdminAreaUserAppProtected) {
				return instance.getImage(this.getClass(), "icons/16x16/protected.gif");
			} else if (element instanceof AdminAreaProjectApplications) {
				return instance.getImage(this.getClass(), "icons/16x16/projecttasks.gif");
			} else if (element instanceof AdminAreaProjectAppFixed) {
				return instance.getImage(this.getClass(), "icons/16x16/fixed.gif");
			} else if (element instanceof AdminAreaProjectAppNotFixed) {
				return instance.getImage(this.getClass(), "icons/16x16/notfixed.gif");
			} else if (element instanceof AdminAreaProjectAppProtected) {
				return instance.getImage(this.getClass(), "icons/16x16/protected.gif");
			} else if (element instanceof AdminAreaStartApplications) {
				return instance.getImage(this.getClass(), "icons/16x16/startapplications.png");
			} else if (element instanceof AdminAreaProjectStartApplications) {
				return instance.getImage(this.getClass(), "icons/16x16/startapplications.png");
			} else if (element instanceof AdminAreaProjects) {
				return instance.getImage(this.getClass(), "icons/16x16/projects.png");
			} else if (element instanceof AdminAreaInfo) {
				return instance.getImage(this.getClass(), "icons/16x16/info.gif");
			} else if (element instanceof AdminAreaInfoHistory) {
				return instance.getImage(this.getClass(), "icons/16x16/history.gif");
			} else if (element instanceof AdminAreaInfoStatus) {
				return instance.getImage(this.getClass(), "icons/16x16/status.gif");
			} else if (element instanceof AdminAreaStartApp) {
				return instance.getImage(this.getClass(), "icons/16x16/startapplications.png");
			} else if (element instanceof Users) {
				return instance.getImage(this.getClass(), "icons/16x16/users.png");
			} else if (element instanceof UsersNameAlphabet) {
				return instance.getImage(this.getClass(), "icons/16x16/users.png");
			} else if (element instanceof Projects) {
				return instance.getImage(this.getClass(), "icons/16x16/projects.png");
			} else if (element instanceof ProjectUsers) {
				return instance.getImage(this.getClass(), "icons/16x16/users.png");
			} else if (element instanceof ProjectUserAdminAreas) {
				return instance.getImage(this.getClass(), "icons/16x16/administrationarea.png");
			} else if (element instanceof ProjectUserAAProjectApplications) {
				return instance.getImage(this.getClass(), "icons/16x16/projecttasks.gif");
			} else if (element instanceof ProjectUserAAProjectAppAllowed) {
				return instance.getImage(this.getClass(), "icons/16x16/allowed.gif");
			} else if (element instanceof ProjectUserAAProjectAppForbidden) {
				return instance.getImage(this.getClass(), "icons/16x16/dprojecttasks.gif");
			} else if (element instanceof ProUserProjectApp || element instanceof ProjectAdminAreaProjectApplications) {
				return instance.getImage(this.getClass(), "icons/16x16/projecttasks.gif");
			} else if (element instanceof ProjectAdminAreaProjectAppNotFixed) {
				return instance.getImage(this.getClass(), "icons/16x16/notfixed.gif");
			} else if (element instanceof ProjectAdminAreaProjectAppFixed) {
				return instance.getImage(this.getClass(), "icons/16x16/fixed.gif");
			} else if (element instanceof ProjectAdminAreaProjectAppProtected) {
				return instance.getImage(this.getClass(), "icons/16x16/protected.gif");
			} else if (element instanceof ProjectAdminAreaStartApplications) {
				return instance.getImage(this.getClass(), "icons/16x16/startapplications.png");
			} else if (element instanceof ProjectProjectApplications) {
				return instance.getImage(this.getClass(), "icons/16x16/projecttasks.gif");
			} else if (element instanceof ProjectInformation) {
				return instance.getImage(this.getClass(), "icons/16x16/info.gif");
			} else if (element instanceof ProjectInfoStatus) {
				return instance.getImage(this.getClass(), "icons/16x16/status.gif");
			} else if (element instanceof ProjectInfoHistory) {
				return instance.getImage(this.getClass(), "icons/16x16/history.gif");
			} else if (element instanceof Applications) {
				return instance.getImage(this.getClass(), "icons/16x16/applications.png");
			} else if (element instanceof UserApplications) {
				return instance.getImage(this.getClass(), "icons/16x16/ausertasks.png");
			} else if (element instanceof UserAppAdminAreas) {
				return instance.getImage(this.getClass(), "icons/16x16/administrationarea.png");
			} else if (element instanceof UserAppUsers) {
				return instance.getImage(this.getClass(), "icons/16x16/users.png");
			} else if (element instanceof UserAppGroups) {
				return instance.getImage(this.getClass(), "icons/16x16/groups.png");
			} else if (element instanceof ProjectApplications) {
				return instance.getImage(this.getClass(), "icons/16x16/projecttasks.gif");
			} else if (element instanceof ProjectAppAdminAreas) {
				return instance.getImage(this.getClass(), "icons/16x16/administrationarea.png");
			} else if (element instanceof ProjectAppAdminAreaProjects) {
				return instance.getImage(this.getClass(), "icons/16x16/projects.png");
			} else if (element instanceof ProjectAppUsers) {
				return instance.getImage(this.getClass(), "icons/16x16/users.png");
			} else if (element instanceof ProjectAppGroups) {
				return instance.getImage(this.getClass(), "icons/16x16/groups.png");
			} else if (element instanceof StartApplications) {
				return instance.getImage(this.getClass(), "icons/16x16/startapplications.png");
			} else if (element instanceof StartAppAdminAreas) {
				return instance.getImage(this.getClass(), "icons/16x16/administrationarea.png");
			} else if (element instanceof StartAppProjects) {
				return instance.getImage(this.getClass(), "icons/16x16/projects.png");
			} else if (element instanceof StartAppUsers) {
				return instance.getImage(this.getClass(), "icons/16x16/users.png");
			} else if (element instanceof BaseApplications) {
				return instance.getImage(this.getClass(), "icons/16x16/tasks.png");
			} else if (element instanceof BaseAppProjectApplications) {
				return instance.getImage(this.getClass(), "icons/16x16/projecttasks.gif");
			} else if (element instanceof BaseAppUserApplications) {
				return instance.getImage(this.getClass(), "icons/16x16/ausertasks.png");
			} else if (element instanceof BaseAppStartApplications) {
				return instance.getImage(this.getClass(), "icons/16x16/startapplications.png");
			} else if (element instanceof BaseAppInformationModel) {
				return instance.getImage(this.getClass(), "icons/16x16/info.gif");
			} else if (element instanceof BaseAppInformationStatus) {
				return instance.getImage(this.getClass(), "icons/16x16/status.gif");
			} else if (element instanceof BaseAppInformationHistory) {
				return instance.getImage(this.getClass(), "icons/16x16/history.gif");
			} else if (element instanceof Groups) {
				return instance.getImage(this.getClass(), "icons/16x16/groups.png");
			} else if (element instanceof Directories) {
				return instance.getImage(this.getClass(), "icons/16x16/directory.png");
			} else if (element instanceof DirectoryUsers) {
				return instance.getImage(this.getClass(), "icons/16x16/users.png");
			} else if (element instanceof DirectoryProjects) {
				return instance.getImage(this.getClass(), "icons/16x16/projects.png");
			} else if (element instanceof DirectoryApplications) {
				return instance.getImage(this.getClass(), "icons/16x16/tasks.png");
			} else if (element instanceof DirectoryUserApplications) {
				return instance.getImage(this.getClass(), "icons/16x16/ausertasks.png");
			} else if (element instanceof DirectoryProjectApplications) {
				return instance.getImage(this.getClass(), "icons/16x16/projecttasks.gif");
			} else if (element instanceof Configurations) {
				return instance.getImage(this.getClass(), "icons/16x16/configurations.png");
			} else if (element instanceof Icons) {
				return instance.getImage(this.getClass(), "icons/16x16/icons.png");
			} else if (element instanceof AdminMenu) {
				return instance.getImage(this.getClass(), "icons/16x16/superadmin.png");
			} else if (element instanceof AdminUsers) {
				return instance.getImage(this.getClass(), "icons/16x16/users.png");
			} else if (element instanceof AdminUserApps) {
				return instance.getImage(this.getClass(), "icons/16x16/ausertasks.png");
			} else if (element instanceof AdminProjectApps) {
				return instance.getImage(this.getClass(), "icons/16x16/projecttasks.gif");
			} else if (element instanceof Roles) {
				return instance.getImage(this.getClass(), "icons/16x16/roles.png");
			} else if (element instanceof Role) {
				String roleName = ((Role) element).getRoleName();
				if (CommonConstants.SuperAdminRole.NAME.equalsIgnoreCase(roleName)) {
					return instance.getImage(this.getClass(), "icons/16x16/superadmin.png");
				}
				return instance.getImage(this.getClass(), "icons/16x16/roles.png");
			} else if (element instanceof RoleUsers) {
				return instance.getImage(this.getClass(), "icons/16x16/users.png");
			} else if (element instanceof RoleScopeObjectUsers) {
				return instance.getImage(this.getClass(), "icons/16x16/users.png");
			} else if (element instanceof RoleScopeObjects) {
				String name = ((RoleScopeObjects) element).getName();
				if (name.equals(AdministrationAreas.class.getName())) {
					return instance.getImage(this.getClass(), "icons/16x16/administrationarea.png");
				}
			} else if (element instanceof SmtpConfiguration) {
				return instance.getImage(this.getClass(), "icons/16x16/smtpconfig.png");
			} else if (element instanceof LdapConfiguration) {
				return instance.getImage(this.getClass(), "icons/16x16/ldapconfig.png");
			} else if (element instanceof SingletonAppTimeConfig) {
				return instance.getImage(this.getClass(), "icons/16x16/singletonapptimeconfig.png");
			} else if (element instanceof NoProjectPopMessage) {
				return instance.getImage(this.getClass(), "icons/16x16/noprojectpopupmsg.png");
			} else if (element instanceof ProjectExpiryConfig) {
				return instance.getImage(this.getClass(), "icons/16x16/projectexpirydaysconfig.png");
			} else if (element instanceof LiveMessages) {
				return instance.getImage(this.getClass(), "icons/16x16/livemessagesconfig.png");
			} else if (element instanceof LiveMessage) {
				return instance.getImage(this.getClass(), "icons/16x16/livemessagesconfig.png");
			} else if (element instanceof Notification) {
				return instance.getImage(this.getClass(), "icons/16x16/notification.png");
			} else if (element instanceof Site && (icon = ((Site) element).getIcon()) != null) {
				return getIconObject(icon, ((Site) element).isActive());
			} else if (element instanceof AAWarpperObj) {
				final AdministrationArea adminArea = ((AAWarpperObj) element).getAdminArea();
				if ((icon = adminArea.getIcon()) != null) {
					return getIconObject(icon, adminArea.isActive());
				} else {
					return instance.getImage(this.getClass(), "icons/16x16/administrationarea.png");
				}
			} else if (element instanceof ProjectWrapperObj) {
				final Project project = ((ProjectWrapperObj) element).getProject();
				if ((icon = project.getIcon()) != null) {
					return getIconObject(icon, project.isActive());
				} else {
					return instance.getImage(this.getClass(), "icons/16x16/projects.png");
				}
			}else if (element instanceof User && (icon = ((User) element).getIcon()) != null) {
				return getIconObject(icon, ((User) element).isActive());
			} else if (element instanceof Project && (icon = ((Project) element).getIcon()) != null) {
				return getIconObject(icon, ((Project) element).isActive());
			} else if (element instanceof Directory && (icon = ((Directory) element).getIcon()) != null) {
				return getIconObject(icon, true);
			} else if (element instanceof UserApplication && (icon = ((UserApplication) element).getIcon()) != null) {
				return getIconObject(icon, ((UserApplication) element).isActive());
			} else if (element instanceof UserApplicationChild
					&& (icon = ((UserApplicationChild) element).getIcon()) != null) {
				return getIconObject(icon, ((UserApplicationChild) element).isActive());
			} else if (element instanceof ProjectApplication
					&& (icon = ((ProjectApplication) element).getIcon()) != null) {
				return getIconObject(icon, ((ProjectApplication) element).isActive());
			} else if (element instanceof ProjectApplicationChild
					&& (icon = ((ProjectApplicationChild) element).getIcon()) != null) {
				return getIconObject(icon, ((ProjectApplicationChild) element).isActive());
			} else if (element instanceof StartApplication && (icon = ((StartApplication) element).getIcon()) != null) {
				return getIconObject(icon, ((StartApplication) element).isActive());
			} else if (element instanceof BaseApplication && (icon = ((BaseApplication) element).getIcon()) != null) {
				return getIconObject(icon, ((BaseApplication) element).isActive());
			} else if (element instanceof RelationObj) {
				IAdminTreeChild refObject = ((RelationObj) element).getRefObject();
				boolean relationStatus = ((RelationObj) element).isActive();
				if (refObject instanceof Site) {
					if ((icon = ((Site) refObject).getIcon()) != null) {
						return getIconObject(icon, relationStatus);
					}
				} else if (refObject instanceof AdministrationArea) {
					if ((icon = ((AdministrationArea) refObject).getIcon()) != null) {
						return getIconObject(icon, relationStatus);
					}
				} else if (refObject instanceof Project) {
					if ((icon = ((Project) refObject).getIcon()) != null) {
						return getIconObject(icon, relationStatus);
					}
				} else if (refObject instanceof User) {
					if ((icon = ((User) refObject).getIcon()) != null) {
						return getIconObject(icon, relationStatus);
					}
				} else if (refObject instanceof Directory) {
					if ((icon = ((Directory) refObject).getIcon()) != null) {
						return getIconObject(icon, relationStatus);
					}
				} else if (element instanceof UserApplication) {
					if ((icon = ((UserApplication) refObject).getIcon()) != null) {
						return getIconObject(icon, relationStatus);
					}
				} else if (element instanceof ProjectApplication) {
					if ((icon = ((ProjectApplication) refObject).getIcon()) != null) {
						return getIconObject(icon, relationStatus);
					}
				} else if (element instanceof StartApplication) {
					if ((icon = ((StartApplication) refObject).getIcon()) != null) {
						return getIconObject(icon, relationStatus);
					}
				} else if (element instanceof BaseApplication) {
					if ((icon = ((BaseApplication) refObject).getIcon()) != null) {
						return getIconObject(icon, relationStatus);
					}
				}
			}
		} catch (Exception e) {
			LOGGER.error("Exception occured in getImage method ! " + e);
		}
		return super.getImage(element);
	}

	/**
	 * Gets the icon object.
	 *
	 * @param icon
	 *            {@link Icon}
	 * @param active
	 *            {@link boolean}
	 * @return the icon object
	 */
	private Image getIconObject(Icon icon, boolean active) {
		Image image = null;
		if (icon != null) {
			String iconPath = icon.getIconPath();
			if (!XMSystemUtil.isEmpty(iconPath)) {
				image = XMSystemUtil.getInstance().getImage(this.getClass(), iconPath, active, true);
			}
		}
		return image;
	}

	/**
	 * Method called before dispose
	 */
	@Override
	public void dispose() {
		super.dispose();
	}
}
