package com.magna.xmsystem.xmadmin.ui.handlers.toolbar;

import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.jface.viewers.StructuredSelection;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.AdminTreeviewer;
import com.magna.xmsystem.xmadmin.util.NavigationStack;
import com.magna.xmsystem.xmadmin.util.XMAdminUtil;

/**
 * The Class PreviousSelectionHandler.
 * 
 * @author subash.janarthanan
 * 
 */
public class PreviousSelectionHandler {
	
	/**
	 * Execute.
	 */
	@Execute
	public void execute(){
		XMAdminUtil instance = XMAdminUtil.getInstance();
		AdminTreeviewer adminTree = instance.getAdminTree();
		NavigationStack<Object> backwardStack = adminTree.getBackwardStack();
		NavigationStack<Object> forwardStack = adminTree.getForwardStack();	
		forwardStack.push(backwardStack.lastElement());
		backwardStack.remove(backwardStack.lastElement());
		adminTree.setSelection(new StructuredSelection(backwardStack.lastElement()), true);	
		adminTree.setForwardStack(forwardStack);
	}
	
	/**
	 * Can execute.
	 *
	 * @return true, if successful
	 */
	@CanExecute
	public boolean canExecute(){
		XMAdminUtil instance = XMAdminUtil.getInstance();
		AdminTreeviewer adminTree = instance.getAdminTree();
		NavigationStack<Object> backwardStack = adminTree.getBackwardStack();
		int stackSize = backwardStack.size();	
		if(stackSize > 1){
			return true;
		}
		return false;	
	}
}
