package com.magna.xmsystem.xmadmin.ui.importuser.wizard.model;

// TODO: Auto-generated Javadoc
/**
 * The Class LdapUserProperty.
 * 
 * @author archita.patel
 */
public class LdapUserProperty {

	/** The Propert name. */
	private String PropertName;

	/** The value. */
	private String value;

	/**
	 * Gets the propert name.
	 *
	 * @return the propert name
	 */
	public String getPropertName() {
		return PropertName;
	}

	/**
	 * Sets the propert name.
	 *
	 * @param propertName
	 *            the new propert name
	 */
	public void setPropertName(final String propertName) {
		PropertName = propertName;
	}

	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * Sets the value.
	 *
	 * @param value
	 *            the new value
	 */
	public void setValue(final String value) {
		this.value = value;
	}
}
