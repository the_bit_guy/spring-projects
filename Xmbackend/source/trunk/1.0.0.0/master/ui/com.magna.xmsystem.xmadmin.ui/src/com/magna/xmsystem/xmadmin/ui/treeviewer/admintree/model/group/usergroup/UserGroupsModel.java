package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.usergroup;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;

public class UserGroupsModel implements IAdminTreeChild {
	/** The parent. */
	private IAdminTreeChild parent;
	
	/**
	 * List for storing {@link GroupModel}
	 */
	private Map<String, IAdminTreeChild> userGroupsChildren;

	/**
	 * Constructor
	 */
	public UserGroupsModel() {
		this.parent = null;
		this.userGroupsChildren = new LinkedHashMap<>();
	}

	/**
	 * Adds the child
	 * @param child {@link IAdminTreeChild}
	 * @param groupId {@link String}
	 * @return {@link IAdminTreeChild}
	 */
	public IAdminTreeChild add(final String groupId, final IAdminTreeChild child) {
		child.setParent(this);
		final IAdminTreeChild returnVal = this.userGroupsChildren.put(groupId, child);
		sort();
		return returnVal;
	}

	/**
	 * Removes the child
	 * @param groupId {@link String}
	 * @return {@link IAdminTreeChild}
	 */
	public IAdminTreeChild remove(final String groupId) {
		return this.userGroupsChildren.remove(groupId);
	}

	/**
	 * @return {@link List} of {@link GroupModel} 
	 */
	public Collection<IAdminTreeChild> getUserGroupsCollection() {
		return this.userGroupsChildren.values();
	}

	/**
	 * @return the groupsChildren
	 */
	public Map<String, IAdminTreeChild> getUserGroupsChildren() {
		return userGroupsChildren;
	}
	/**
	 * Returns null as its parent
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#getParent()
	 */
	@Override
	public IAdminTreeChild getParent() {
		return this.parent;
	}

	/**
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#setParent(com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild)
	 */
	@Override
	public void setParent(final IAdminTreeChild parent) {
		this.parent = parent;
	}
	/**
	 * Sort.
	 */
	public void sort() {
		final LinkedHashMap<String, IAdminTreeChild> collect = this.userGroupsChildren.entrySet().stream().sorted(
				(e1, e2) -> ((UserGroupModel) e1.getValue()).getName().compareTo(((UserGroupModel) e2.getValue()).getName()))
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
		this.userGroupsChildren = collect;
	}
	
}

