package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;

/**
 * Container class for Users
 * 
 * @author archita.patel
 *
 */
public class Users implements IAdminTreeChild {
	/** The parent. */
	private IAdminTreeChild parent;
	/**
	 * Member variable for storing user children
	 */
	final private Map<String, IAdminTreeChild> usersChildren;

	/**
	 * Constructor
	 */
	public Users() {
		this.parent = null;
		this.usersChildren = new LinkedHashMap<>();
	}

	/**
	 * Method to add child
	 * 
	 * @param child
	 *            {@link IAdminTreeChild}
	 * @param userName
	 *            String
	 * @return {@link IAdminTreeChild}
	 */
	public IAdminTreeChild add(final String userId, final IAdminTreeChild child) {
		child.setParent(this);
		return this.usersChildren.put(userId, child);
	}

	/**
	 * Method to remove child
	 * 
	 * @param userName
	 *            {@link String}
	 * @return {@link IAdminTreeChild}
	 */
	public IAdminTreeChild remove(final String userId) {
		return this.usersChildren.remove(userId);
	}

	/**
	 * Get all user children
	 * 
	 * @return {@link Collection} of {@link IAdminTreeChild}
	 */
	public Collection<IAdminTreeChild> getUsersCollection() {
		return this.usersChildren.values();
	}

	/**
	 * @return the usersChildren
	 */
	public Map<String, IAdminTreeChild> getUsersChildren() {
		return usersChildren;
	}
	
	/**
	 * Returns null as its parent
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#getParent()
	 */
	@Override
	public IAdminTreeChild getParent() {
		return this.parent;
	}

	/**
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#setParent(com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild)
	 */
	@Override
	public void setParent(final IAdminTreeChild parent) {
		this.parent = parent;
	}
}
