package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;

/**
 * The Class ProjectInformation.
 */
public class ProjectInformation implements IAdminTreeChild {

	/** The parent. */
	private IAdminTreeChild parent;

	/** The project information child. */
	final private Map<String, IAdminTreeChild> projectInformationChild;

	/**
	 * Instantiates a new project information.
	 *
	 * @param parent
	 *            the parent
	 */
	public ProjectInformation(final IAdminTreeChild parent) {
		this.parent = parent;
		this.projectInformationChild = new LinkedHashMap<>();
		this.add(UUID.randomUUID().toString(), new ProjectInfoStatus(this));
		this.add(UUID.randomUUID().toString(), new ProjectInfoHistory(this));
	}

	/**
	 * Add.
	 *
	 * @param projectInfoChildId
	 *            the project info child id
	 * @param child
	 *            the child
	 * @return the i admin tree child
	 */
	public IAdminTreeChild add(final String projectInfoChildId, final IAdminTreeChild child) {
		return this.projectInformationChild.put(projectInfoChildId, child);
	}

	/**
	 * Remove.
	 *
	 * @param projectInfoChildId
	 *            the project info child id
	 * @return the i admin tree child
	 */
	public IAdminTreeChild remove(final String projectInfoChildId) {
		return this.projectInformationChild.remove(projectInfoChildId);
	}

	/**
	 * Gets the project info child collection.
	 *
	 * @return the project info child collection
	 */
	public Collection<IAdminTreeChild> getProjectInfoChildCollection() {
		return this.projectInformationChild.values();
	}

	/**
	 * Gets the project information child.
	 *
	 * @return the project information child
	 */
	public Map<String, IAdminTreeChild> getProjectInformationChild() {
		return projectInformationChild;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#
	 * getParent()
	 */
	@Override
	public IAdminTreeChild getParent() {
		// TODO Auto-generated method stub
		return this.parent;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#
	 * setParent(com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.
	 * IAdminTreeChild)
	 */
	@Override
	public void setParent(IAdminTreeChild parent) {
		this.parent = parent;

	}
}
