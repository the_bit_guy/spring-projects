package com.magna.xmsystem.xmadmin.ui.handlers.toolbar;

import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.eclipse.core.commands.ParameterizedCommand;
import org.eclipse.e4.core.commands.ECommandService;
import org.eclipse.e4.core.commands.EHandlerService;
import org.eclipse.e4.core.contexts.Active;
import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.model.application.ui.menu.MToolItem;
import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITreeSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreePath;

import com.magna.xmsystem.xmadmin.ui.parts.IEditablePart;
import com.magna.xmsystem.xmadmin.ui.parts.InformationPart;
import com.magna.xmsystem.xmadmin.ui.parts.objexp.ObjectExpPage;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.AdminTreeviewer;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.AdminTreeFactory;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdministrationArea;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.baseapplicationmodel.BaseApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.projectapplicationmodel.ProjectApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.startapplicationmodel.StartApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.userapplicationmodel.UserApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.ldapconfig.LdapConfiguration;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.livemsgconfig.LiveMessage;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.noProjectMessage.NoProjectPopMessage;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.projectexpiryconfig.ProjectExpiryConfig;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.Role;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.singletonapptimeconfig.SingletonAppTimeConfig;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.smtpconfig.SmtpConfiguration;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.directory.Directory;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.projectappgroup.ProjectApplicationGroup;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.projectgroup.ProjectGroupModel;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.userappgroup.UserApplicationGroup;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.usergroup.UserGroupModel;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.Project;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.Site;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.User;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.Users;
import com.magna.xmsystem.xmadmin.util.CommonConstants;
import com.magna.xmsystem.xmadmin.util.XMAdminUtil;

/**
 * The Class EditToolBarHandler.
 * 
 * @author subash.janarthanan
 * 
 */
@SuppressWarnings("restriction")
public class EditToolBarHandler {

	/** The handler service. */
	@Inject
	private EHandlerService handlerService;

	/** The command service. */
	@Inject
	private ECommandService commandService;

	/** Inject of {@link EModelService}. */
	@Inject
	private EModelService modelService;

	/** Inject of {@link MApplication}. */
	@Inject
	private MApplication application;

	/**
	 * Execute.
	 */
	@Execute
	public void execute() {
		final AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
		IStructuredSelection selection;
		Object selectionObj = null;

		MPart mPart = (MPart) this.modelService.find(CommonConstants.PART_ID.INFORMATION_PART_ID, application);
		InformationPart rightSidePart2 = (InformationPart) mPart.getObject();
		ObjectExpPage objectExpPage = rightSidePart2.getObjectExpPartUI();
		if (objectExpPage != null && (selection = objectExpPage.getObjExpTableViewer().getStructuredSelection()) != null
				&& (selectionObj = selection.getFirstElement()) != null) {
			TreePath[] selectionPaths = adminTree.getStructuredSelection().getPaths();
			if (selectionPaths != null && selectionPaths.length > 0) {
				adminTree.setExpandedState(selectionPaths[0], true);
			}
			if (selectionObj instanceof User) {
				Users users = AdminTreeFactory.getInstance().getUsers();
				Map<String, IAdminTreeChild> userChildren = users.getUsersChildren();
				char startChar = (((User) selectionObj).getName().toString().toLowerCase().toCharArray()[0]);
				IAdminTreeChild iAdminTreeChild = userChildren.get(String.valueOf(startChar));
				adminTree.setExpandedState(iAdminTreeChild, true);
			}
			adminTree.setSelection(new StructuredSelection(selectionObj));
		} else if (adminTree != null && (selection = adminTree.getStructuredSelection()) != null) {
			selectionObj = selection.getFirstElement();
		}

		if (selectionObj instanceof Site) {
			ParameterizedCommand cmd = commandService.createCommand(CommonConstants.COMMAND_ID.CHANGE_SITE, null);
			if (handlerService.canExecute(cmd)) {
				handlerService.executeHandler(cmd);
			}
		} else if (selectionObj instanceof Project) {
			ParameterizedCommand cmd = commandService.createCommand(CommonConstants.COMMAND_ID.CHANGE_PROJECT, null);
			if (handlerService.canExecute(cmd)) {
				handlerService.executeHandler(cmd);
			}
		} else if (selectionObj instanceof AdministrationArea) {
			ParameterizedCommand cmd = commandService.createCommand(CommonConstants.COMMAND_ID.CHANGE_ADMINAREA, null);
			if (handlerService.canExecute(cmd)) {
				handlerService.executeHandler(cmd);
			}
		} else if (selectionObj instanceof UserApplication) {
			ParameterizedCommand cmd = commandService.createCommand(CommonConstants.COMMAND_ID.CHANGE_USERAPP, null);
			if (handlerService.canExecute(cmd)) {
				handlerService.executeHandler(cmd);
			}
		} else if (selectionObj instanceof ProjectApplication) {
			ParameterizedCommand cmd = commandService.createCommand(CommonConstants.COMMAND_ID.CHANGE_PROJECTAPP, null);
			if (handlerService.canExecute(cmd)) {
				handlerService.executeHandler(cmd);
			}
		} else if (selectionObj instanceof StartApplication) {
			ParameterizedCommand cmd = commandService.createCommand(CommonConstants.COMMAND_ID.CHANGE_STARTAPP, null);
			if (handlerService.canExecute(cmd)) {
				handlerService.executeHandler(cmd);
			}
		} else if (selectionObj instanceof BaseApplication) {
			ParameterizedCommand cmd = commandService.createCommand(CommonConstants.COMMAND_ID.CHANGE_BASEAPP, null);
			if (handlerService.canExecute(cmd)) {
				handlerService.executeHandler(cmd);
			}
		} else if (selectionObj instanceof User) {
			ParameterizedCommand cmd = commandService.createCommand(CommonConstants.COMMAND_ID.CHANGE_USER, null);
			if (handlerService.canExecute(cmd)) {
				handlerService.executeHandler(cmd);
			}
		} else if (selectionObj instanceof UserGroupModel) {
			ParameterizedCommand cmd = commandService.createCommand(CommonConstants.COMMAND_ID.CHANGE_USER_GROUP, null);
			if (handlerService.canExecute(cmd)) {
				handlerService.executeHandler(cmd);
			}
		} else if (selectionObj instanceof ProjectGroupModel) {
			ParameterizedCommand cmd = commandService.createCommand(CommonConstants.COMMAND_ID.CHANGE_PROJECT_GROUP,
					null);
			if (handlerService.canExecute(cmd)) {
				handlerService.executeHandler(cmd);
			}
		} else if (selectionObj instanceof UserApplicationGroup) {
			ParameterizedCommand cmd = commandService.createCommand(CommonConstants.COMMAND_ID.CHANGE_USERAPP_GROUP,
					null);
			if (handlerService.canExecute(cmd)) {
				handlerService.executeHandler(cmd);
			}
		} else if (selectionObj instanceof ProjectApplicationGroup) {
			ParameterizedCommand cmd = commandService.createCommand(CommonConstants.COMMAND_ID.CHANGE_PROJECTAPP_GROUP,
					null);
			if (handlerService.canExecute(cmd)) {
				handlerService.executeHandler(cmd);
			}
		} else if (selectionObj instanceof Directory) {
			ParameterizedCommand cmd = commandService.createCommand(CommonConstants.COMMAND_ID.CHANGE_DIRECTORY, null);
			if (handlerService.canExecute(cmd)) {
				handlerService.executeHandler(cmd);
			}
		} else if (selectionObj instanceof SmtpConfiguration) {
			ParameterizedCommand cmd = commandService.createCommand(CommonConstants.COMMAND_ID.CHANGE_SMTP_CONFIG,
					null);
			if (handlerService.canExecute(cmd)) {
				handlerService.executeHandler(cmd);
			}
		} else if (selectionObj instanceof LdapConfiguration) {
			ParameterizedCommand cmd = commandService.createCommand(CommonConstants.COMMAND_ID.CHANGE_LDAP_CONFIG,
					null);
			if (handlerService.canExecute(cmd)) {
				handlerService.executeHandler(cmd);
			}
		} else if (selectionObj instanceof SingletonAppTimeConfig) {
			ParameterizedCommand cmd = commandService.createCommand(CommonConstants.COMMAND_ID.CHANGE_SINGLETON_CONFIG,
					null);
			if (handlerService.canExecute(cmd)) {
				handlerService.executeHandler(cmd);
			}
		} else if (selectionObj instanceof NoProjectPopMessage) {
			ParameterizedCommand cmd = commandService.createCommand(CommonConstants.COMMAND_ID.CHANGE_NOPROJECT_POPUP,
					null);
			if (handlerService.canExecute(cmd)) {
				handlerService.executeHandler(cmd);
			}
		} else if (selectionObj instanceof ProjectExpiryConfig) {
			ParameterizedCommand cmd = commandService.createCommand(CommonConstants.COMMAND_ID.CHANGE_PROJECT_EXPIRY,
					null);
			if (handlerService.canExecute(cmd)) {
				handlerService.executeHandler(cmd);
			}
		} else if (selectionObj instanceof Role) {
			ParameterizedCommand cmd = commandService.createCommand(CommonConstants.COMMAND_ID.CHANGE_ROLE, null);
			if (handlerService.canExecute(cmd)) {
				handlerService.executeHandler(cmd);
			}
		} else if (selectionObj instanceof LiveMessage) {
			ParameterizedCommand cmd = commandService.createCommand(CommonConstants.COMMAND_ID.CHANGE_LIVEMESSAGE,
					null);
			if (handlerService.canExecute(cmd)) {
				handlerService.executeHandler(cmd);
			}
		}
	}

	/**
	 * Can execute.
	 *
	 * @return true, if successful
	 */
	@CanExecute
	public boolean canExecute(@Active MPart part, MApplication application, EModelService service,
			EModelService eModelService) {
		Object selectionObj = null;
		MPart rightHandMPartOfOldPerspective = (MPart) eModelService.find(CommonConstants.PART_ID.INFORMATION_PART_ID,
				application);
		boolean returnValue = true;
		boolean checkValue = false;
		XMAdminUtil instance = XMAdminUtil.getInstance();
		if (rightHandMPartOfOldPerspective.getObject() instanceof IEditablePart
				&& rightHandMPartOfOldPerspective.isDirty()) {
			return false;
		} else if (part.getElementId().equals(CommonConstants.PART_ID.INFORMATION_PART_ID)) {
			MPart mPart = instance.getInformationPart();
			InformationPart rightSidePart = (InformationPart) mPart.getObject();
			ObjectExpPage objectExpPage = rightSidePart.getObjectExpPartUI();
			ITreeSelection selection;
			if (objectExpPage != null
					&& (selection = objectExpPage.getObjExpTableViewer().getStructuredSelection()) != null
					&& (selectionObj = selection.getFirstElement()) != null && selection.size() == 1) {
				selectionObj = selection.getFirstElement();
				if (selectionObj instanceof Site || selectionObj instanceof Project
						|| selectionObj instanceof AdministrationArea || selectionObj instanceof User
						|| selectionObj instanceof UserApplication || selectionObj instanceof ProjectApplication
						|| selectionObj instanceof StartApplication || selectionObj instanceof BaseApplication
						|| selectionObj instanceof UserGroupModel || selectionObj instanceof ProjectGroupModel
						|| selectionObj instanceof UserApplicationGroup
						|| selectionObj instanceof ProjectApplicationGroup || selectionObj instanceof Directory
						|| selectionObj instanceof LiveMessage) {
					checkValue = true;
				} else if (selectionObj instanceof Role) {
					final Role role = (Role) selectionObj;
					if (!(CommonConstants.SuperAdminRole.NAME.equals(role.getRoleName()))) {
						checkValue = true;
					}
				} else {
					checkValue = false;
				}
			}
			if (returnValue) {
				List<MToolItem> mToolItems = service.findElements(application,
						"com.magna.xmsystem.xmadmin.ui.handledtoolitem.edit", MToolItem.class, null); //$NON-NLS-1$
				if (mToolItems.size() > 0) {
					mToolItems.get(0).setEnabled(checkValue);
				}
				return checkValue;
			}
		} else {
			if (instance != null) {
				final AdminTreeviewer adminTree = instance.getAdminTree();
				IStructuredSelection selection = adminTree.getStructuredSelection();
				Object treeSelectionObj = selection.getFirstElement();
				if (treeSelectionObj instanceof Site || treeSelectionObj instanceof Project
						|| treeSelectionObj instanceof AdministrationArea || treeSelectionObj instanceof User
						|| treeSelectionObj instanceof UserApplication || treeSelectionObj instanceof ProjectApplication
						|| treeSelectionObj instanceof StartApplication || treeSelectionObj instanceof BaseApplication
						|| treeSelectionObj instanceof UserGroupModel || treeSelectionObj instanceof ProjectGroupModel
						|| treeSelectionObj instanceof UserApplicationGroup
						|| treeSelectionObj instanceof ProjectApplicationGroup || treeSelectionObj instanceof Directory
						|| treeSelectionObj instanceof SmtpConfiguration
						|| treeSelectionObj instanceof LdapConfiguration
						|| treeSelectionObj instanceof SingletonAppTimeConfig
						|| treeSelectionObj instanceof NoProjectPopMessage
						|| treeSelectionObj instanceof ProjectExpiryConfig || treeSelectionObj instanceof LiveMessage) {
					return true;
				}
				if (treeSelectionObj instanceof Role) {
					final Role role = (Role) treeSelectionObj;
					if (!(CommonConstants.SuperAdminRole.NAME.equals(role.getRoleName()))) {
						return true;
					}
				}
			}
		}
		return false;
	}
}
