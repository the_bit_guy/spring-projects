package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;

/**
 * Model class for Groups
 * 
 * @author subash.janarthanan
 *
 */
public class Groups implements IAdminTreeChild {
	/** The parent. */
	private IAdminTreeChild parent;
	
	/**
	 * List for storing {@link GroupModel}
	 */
	private transient final Map<String, IAdminTreeChild> groupsChildren;

	/**
	 * Constructor
	 */
	public Groups() {
		this.parent = null;
		this.groupsChildren = new LinkedHashMap<>();
	}

	/**
	 * Adds the child
	 * @param child {@link IAdminTreeChild}
	 * @param groupName {@link String}
	 * @return {@link IAdminTreeChild}
	 */
	public IAdminTreeChild add(final String groupName, final IAdminTreeChild child) {
		child.setParent(this);
		return this.groupsChildren.put(groupName, child);
	}

	/**
	 * Removes the child
	 * @param objectId {@link String}
	 * @return {@link IAdminTreeChild}
	 */
	public IAdminTreeChild remove(final String groupName) {
		return this.groupsChildren.remove(groupName);
	}

	/**
	 * @return {@link List} of {@link GroupModel} 
	 */
	public Collection<IAdminTreeChild> getGroupsCollection() {
		return this.groupsChildren.values();
	}

	/**
	 * @return the groupsChildren
	 */
	public Map<String, IAdminTreeChild> getGroupsChildren() {
		return groupsChildren;
	}
	/**
	 * Returns null as its parent
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#getParent()
	 */
	@Override
	public IAdminTreeChild getParent() {
		return this.parent;
	}

	/**
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#setParent(com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild)
	 */
	@Override
	public void setParent(final IAdminTreeChild parent) {
		this.parent = parent;
	}
}

