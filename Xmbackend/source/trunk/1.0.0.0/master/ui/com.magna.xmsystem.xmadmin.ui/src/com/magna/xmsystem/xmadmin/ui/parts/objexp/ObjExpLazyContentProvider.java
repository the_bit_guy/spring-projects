package com.magna.xmsystem.xmadmin.ui.parts.objexp;

import java.util.List;

import org.eclipse.jface.viewers.ILazyTreeContentProvider;
import org.eclipse.jface.viewers.Viewer;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;

/**
 * The Class ObjExpLazyContentProvider.
 * 
 * @author shashwat.anand
 */
public class ObjExpLazyContentProvider implements ILazyTreeContentProvider {

	/** The object explorer. */
	private ObjectExplorer objectExplorer;

	/**
	 * Instantiates a new obj exp lazy content provider.
	 *
	 * @param objectExplorer the object explorer
	 */
	public ObjExpLazyContentProvider(final ObjectExplorer objectExplorer) {
		this.objectExplorer = objectExplorer;
	}
	
	/* (non-Javadoc)
	 * @see org.eclipse.jface.viewers.IContentProvider#inputChanged(org.eclipse.jface.viewers.Viewer, java.lang.Object, java.lang.Object)
	 */
	@Override
	public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		 if (newInput instanceof List<?>) {
			 ILazyTreeContentProvider.super.inputChanged(viewer, oldInput, ((List<?>) newInput).toArray());
		 }
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.viewers.ILazyTreeContentProvider#updateElement(java.lang.Object, int)
	 */
	@Override
	public void updateElement(Object parent, int index) {
		
		Object element;
		if (parent instanceof List<?>) {
			int size = ((List<?>) parent).size();
			if (size > index) {
				element = ((List<?>) parent).get(index);
				this.objectExplorer.replace(parent, index, element);
				updateChildCount(element, -1);
			}
		}
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.viewers.ILazyTreeContentProvider#updateChildCount(java.lang.Object, int)
	 */
	@Override
	public void updateChildCount(Object element, int currentChildCount) {
		if (element instanceof List<?>) {
			this.objectExplorer.setChildCount(element, ((List<?>) element).size());
		}
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.viewers.ILazyTreeContentProvider#getParent(java.lang.Object)
	 */
	@Override
	public Object getParent(Object element) {
		if (element instanceof IAdminTreeChild) {
			return ((IAdminTreeChild) element).getParent();
		}
		return null;
	}

}
