package com.magna.xmsystem.xmadmin.ui.dialogs.lang;

import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

/**
 * The listener interface for receiving textAreaModify events. The class that is
 * interested in processing a textAreaModify event implements this interface,
 * and the object created with that class is registered with a component using
 * the component's <code>addTextAreaModifyListener<code> method. When the
 * textAreaModify event occurs, that object's appropriate method is invoked.
 *
 * @author shashwat.anand
 */
public class TextAreaModifyListener implements ModifyListener {

	/** The count label. */
	private Label countLabel;

	/** The max limit. */
	private int maxLimit;

	/**
	 * Instantiates a new text area modify listener.
	 *
	 * @param lblCount
	 *            the lbl count
	 * @param maxLimit
	 *            the max limit
	 */
	public TextAreaModifyListener(final Label lblCount, final int maxLimit) {
		this.countLabel = lblCount;
		this.maxLimit = maxLimit;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.swt.events.ModifyListener#modifyText(org.eclipse.swt.events.
	 * ModifyEvent)
	 */
	@Override
	public void modifyText(ModifyEvent event) {
		final Text text = (Text) event.widget;
		countCharactor(text, countLabel);
	}

	/**
	 * method for countCharactor.
	 *
	 * @param text
	 *            the text
	 * @param lblCount
	 *            the lbl count
	 */
	public void countCharactor(Text text, Label lblCount) {
		lblCount.setText(text.getText().length() + XMAdminLangTextAreaDialog.SLASH + this.maxLimit);
	}
}
