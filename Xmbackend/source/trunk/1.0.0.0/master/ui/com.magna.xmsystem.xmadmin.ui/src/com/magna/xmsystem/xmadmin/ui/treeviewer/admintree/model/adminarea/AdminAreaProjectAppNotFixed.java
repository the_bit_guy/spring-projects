package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;

/**
 * The Class AdminProAppChildNotFixed.
 */
public class AdminAreaProjectAppNotFixed implements IAdminTreeChild {

	/** The parent. */
	private IAdminTreeChild parent;

	/**
	 * Instantiates a new admin pro app child not fixed.
	 *
	 * @param parent the parent
	 */
	public AdminAreaProjectAppNotFixed(final IAdminTreeChild parent) {
		this.parent = parent;
	}
	
	/* (non-Javadoc)
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#getParent()
	 */
	@Override
	public IAdminTreeChild getParent() {
		return this.parent;
	}

	/* (non-Javadoc)
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#setParent(com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild)
	 */
	@Override
	public void setParent(final IAdminTreeChild parent) {
		this.parent = parent;
	}

}
