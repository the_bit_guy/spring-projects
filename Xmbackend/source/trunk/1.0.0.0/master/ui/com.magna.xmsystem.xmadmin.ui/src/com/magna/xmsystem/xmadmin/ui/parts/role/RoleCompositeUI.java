package com.magna.xmsystem.xmadmin.ui.parts.role;

import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.nebula.widgets.pgroup.PGroup;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.events.ControlAdapter;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.events.ExpandEvent;
import org.eclipse.swt.events.ExpandListener;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.Role;
import com.magna.xmsystem.xmadmin.util.XMAdminUtil;

/**
 * The Class RoleCompositeUI.
 * 
 * @author archita.patel
 */
public class RoleCompositeUI extends Composite {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(RoleCompositeUI.class);

	/** The grp roles. */
	transient protected Group grpRoles;

	/** The txt name. */
	transient protected Text txtName;

	/** The lbl name. */
	transient protected Label lblName;

	/** The txt desc. */
	transient protected Text txtDesc;

	/** The save btn. */
	transient protected Button saveBtn;

	/** The cancel btn. */
	transient protected Button cancelBtn;
	
	/**Member variable for view inactive Button*/
	protected Button viewInactiveBtn;

	/** Member variable for xm admin Button*/
	protected Button xmAdminBtn;

	/**Member variable for xm hotline Button*/
	protected Button xmHotlineBtn;
	
	/** The lbl description. */
	transient protected Label lblDescription;
	
	/** The lbl view inactive. */
	protected Label lblViewInactive;
	
	/** The lbl xm admin. */
	protected Label lblXmAdmin;
	
	/** The lbl xm hotline. */
	protected Label lblXmHotline;

	/** The button bar comp. */
	protected Composite buttonBarComp;

	/** Member variable 'permission checkbox edit msg lbl' for {@link Label}. */
	protected Label permissionCheckboxEditMsgLbl;
	
	/** The p group object permission. */
	protected PGroup pGroupObjectPermission;

	/** The p group obj relation permission. */
	protected PGroup pGroupObjRelationPermission;

	/** The object permission viewer. */
	protected ObjectPermissionsTable objectPermissionViewer;

	/** The obj relation permission viewer. */
	protected ObjRelationPermissionsTable objRelationPermissionViewer;

	/** The scrolled composite. */
	protected ScrolledComposite scrolledComposite;

	/**
	 * Instantiates a new role composite UI.
	 *
	 * @param parent
	 *            the parent
	 * @param style
	 *            the style
	 * @param messages2 
	 */
	public RoleCompositeUI(final Composite parent, final int style) {
		super(parent, style);
		this.initGUI();
	}

	/**
	 * Init GUI.
	 */
	private void initGUI() {
		try {
			GridLayoutFactory.fillDefaults().applyTo(this);
			this.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

			this.grpRoles = new Group(this, SWT.NONE);
			this.grpRoles.setBackgroundMode(SWT.INHERIT_FORCE);
			GridLayoutFactory.fillDefaults().numColumns(1).applyTo(this.grpRoles);
			GridDataFactory.fillDefaults().grab(true, true).align(SWT.FILL, SWT.FILL).applyTo(this.grpRoles);
			
			this.scrolledComposite = XMAdminUtil.getInstance().createScrolledComposite(this.grpRoles);
			this.scrolledComposite.setLayout(new GridLayout(1, false));
			GridDataFactory.fillDefaults().grab(true, true).span(2, 1).applyTo(this.scrolledComposite);

			final Composite widgetContainer = new Composite(this.scrolledComposite, SWT.NONE);
			final GridLayout widgetContLayout = new GridLayout(2, false);

			widgetContainer.setLayout(widgetContLayout);
			widgetContainer.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

			this.lblName = new Label(widgetContainer, SWT.NONE);
			this.txtName = new Text(widgetContainer, SWT.BORDER);
			this.txtName.setTextLimit(Role.NAME_LIMIT);
			GridDataFactory.fillDefaults().grab(true, false).span(1, 1).align(SWT.FILL, SWT.CENTER).applyTo(this.txtName);

			this.lblDescription = new Label(widgetContainer, SWT.NONE);
			this.txtDesc = new Text(widgetContainer, SWT.BORDER);
			this.txtDesc.setTextLimit(Role.DESC_LIMIT);
			GridDataFactory.fillDefaults().grab(true, false).span(1, 1).align(SWT.FILL, SWT.CENTER).applyTo(this.txtDesc);
			
			this.lblViewInactive = new Label(widgetContainer, SWT.NONE);
			this.viewInactiveBtn = new Button(widgetContainer, SWT.CHECK);
			GridDataFactory.fillDefaults().grab(false, false).align(SWT.LEFT, SWT.CENTER).applyTo(this.lblViewInactive);
			/*GridDataFactory.fillDefaults().grab(false, false).align(SWT.LEFT, SWT.CENTER ).span(2, 1)
					.applyTo(this.viewInactiveBtn);*/
			
			this.lblXmAdmin = new Label(widgetContainer, SWT.NONE);
			this.xmAdminBtn = new Button(widgetContainer, SWT.CHECK);
			GridDataFactory.fillDefaults().grab(false, false).align(SWT.LEFT, SWT.CENTER).applyTo(this.lblViewInactive);
			
			this.lblXmHotline = new Label(widgetContainer, SWT.NONE);
			this.xmHotlineBtn = new Button(widgetContainer, SWT.CHECK);
			GridDataFactory.fillDefaults().grab(false, false).align(SWT.LEFT, SWT.CENTER).applyTo(this.lblViewInactive);
			
			this.permissionCheckboxEditMsgLbl = new Label(widgetContainer, SWT.NONE);
			this.permissionCheckboxEditMsgLbl.setData("org.eclipse.e4.ui.css.id", "MessageLabel");
			this.permissionCheckboxEditMsgLbl.setVisible(false);
			GridDataFactory.fillDefaults().grab(false, false).align(SWT.LEFT, SWT.CENTER ).span(2, 1).applyTo(this.permissionCheckboxEditMsgLbl);		
			
			final Composite pGroupContainer = new Composite(widgetContainer, SWT.NONE);
			pGroupContainer.setBackgroundMode(SWT.INHERIT_DEFAULT);
			final GridLayout pGroupCompositeLayout = new GridLayout(1, false);
			pGroupCompositeLayout.marginRight = 0;
			pGroupCompositeLayout.marginLeft = 0;
			pGroupCompositeLayout.marginTop = 0;
			pGroupCompositeLayout.marginBottom = 0;
			pGroupCompositeLayout.marginWidth = 0;
			pGroupCompositeLayout.marginHeight = 0;
			pGroupContainer.setLayout(pGroupCompositeLayout);
			GridDataFactory.fillDefaults().grab(true, false).span(2, 1).align(SWT.FILL, SWT.CENTER)
					.applyTo(pGroupContainer);

			pGroupObjectPermission = new PGroup(pGroupContainer, SWT.SMOOTH);
			pGroupObjectPermission.setLayout(new GridLayout());
			GridDataFactory.fillDefaults().grab(true, false).span(1, 1).align(SWT.FILL, SWT.CENTER)
					.applyTo(this.pGroupObjectPermission);

			pGroupObjectPermission.addExpandListener(new ExpandListener() {

				@Override
				public void itemExpanded(final ExpandEvent event) {
					pGroupRepaint((PGroup) event.widget);
				}

				@Override
				public void itemCollapsed(final ExpandEvent event) {
					pGroupRepaint((PGroup) event.widget);
				}
			});

			pGroupObjRelationPermission = new PGroup(pGroupContainer, SWT.SMOOTH);
			pGroupObjRelationPermission.setLayout(new GridLayout());
			GridDataFactory.fillDefaults().grab(true, false).span(1, 1).align(SWT.FILL, SWT.FILL)
					.applyTo(this.pGroupObjRelationPermission);
			pGroupObjRelationPermission.addExpandListener(new ExpandListener() {

				@Override
				public void itemExpanded(final ExpandEvent event) {
					pGroupRepaint((PGroup) event.widget);
				}

				@Override
				public void itemCollapsed(final ExpandEvent event) {
					pGroupRepaint((PGroup) event.widget);
				}
			});

			final Composite objRelPermissionCompo = new Composite(pGroupObjRelationPermission, SWT.NONE);
			objRelPermissionCompo.setLayout(new FillLayout());
			objRelPermissionCompo.setLayoutData(new GridData(SWT.FILL, SWT.DEFAULT, true, true));

			objRelationPermissionViewer = new ObjRelationPermissionsTable(objRelPermissionCompo);
			pGroupObjRelationPermission.setData(objRelationPermissionViewer);
			
			final Composite objPermissionCompo = new Composite(pGroupObjectPermission, SWT.NONE);
			objPermissionCompo.setLayout(new FillLayout());
			objPermissionCompo.setLayoutData(new GridData(SWT.FILL, SWT.DEFAULT, true, true));

			objectPermissionViewer = new ObjectPermissionsTable(objPermissionCompo);			
			pGroupObjectPermission.setData(objectPermissionViewer);

			final Composite buttonBarComp = new Composite(this.grpRoles, SWT.NONE);
			final GridLayout btnBarCompLayout = new GridLayout(2, true);
			btnBarCompLayout.marginRight = 0;
			btnBarCompLayout.marginLeft = 0;
			btnBarCompLayout.marginTop = 0;
			btnBarCompLayout.marginBottom = 0;
			btnBarCompLayout.marginWidth = 0;
			buttonBarComp.setLayout(btnBarCompLayout);
			buttonBarComp.setLayoutData(new GridData(SWT.FILL, SWT.DEFAULT, true, false, 3, 1));
			createButtonBar(buttonBarComp);
			

			this.scrolledComposite.setContent(widgetContainer);
			this.scrolledComposite.setSize(widgetContainer.getSize());
			this.scrolledComposite.setExpandVertical(true);
			this.scrolledComposite.setExpandHorizontal(true);
			this.scrolledComposite.update();

			this.scrolledComposite.addControlListener(new ControlAdapter() {
				public void controlResized(final ControlEvent e) {
					Rectangle rectangle = scrolledComposite.getClientArea();
					scrolledComposite.setMinSize(widgetContainer.computeSize(rectangle.width, SWT.DEFAULT));
				}
			});
		} catch (Exception e) {
			LOGGER.error("Unable to crete UI elements", e); //$NON-NLS-1$
		}
	}

	/**
	 * P group repaint.
	 *
	 * @param pGroup
	 *            the group
	 */
	private void pGroupRepaint(final PGroup pGroup) {
		pGroup.requestLayout();
		pGroup.redraw();
		pGroup.getParent().update();
		this.scrolledComposite.notifyListeners(SWT.Resize, new Event());
	}

	/**
	 * Create button bar.
	 *
	 * @param buttonBarComp
	 *            the button bar comp
	 */
	private void createButtonBar(Composite buttonBarComp) {
		this.saveBtn = new Button(buttonBarComp, SWT.PUSH);
		this.saveBtn.setLayoutData(new GridData(SWT.RIGHT, SWT.BOTTOM, true, true, 1, 1));

		this.cancelBtn = new Button(buttonBarComp, SWT.PUSH);
		this.cancelBtn.setLayoutData(new GridData(SWT.LEFT, SWT.BOTTOM, true, true, 1, 1));

	}

	/**
	 * Sets the show button bar.
	 *
	 * @param showButtonBar
	 *            the new show button bar
	 */
	protected void setShowButtonBar(final boolean showButtonBar) {
		if (this.saveBtn != null && !this.saveBtn.isDisposed() && this.cancelBtn != null
				&& !this.cancelBtn.isDisposed()) {
			final GridData layoutData = (GridData) this.saveBtn.getParent().getLayoutData();
			layoutData.exclude = !showButtonBar;
			this.saveBtn.setVisible(showButtonBar);
			this.cancelBtn.setVisible(showButtonBar);
			this.saveBtn.getParent().requestLayout();
			this.saveBtn.getParent().redraw();
			this.saveBtn.getParent().getParent().update();
			this.scrolledComposite.notifyListeners(SWT.Resize, new Event());
		}
	}
}
