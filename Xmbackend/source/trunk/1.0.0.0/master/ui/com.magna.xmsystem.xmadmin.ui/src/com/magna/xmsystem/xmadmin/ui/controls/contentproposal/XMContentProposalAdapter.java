package com.magna.xmsystem.xmadmin.ui.controls.contentproposal;

import org.eclipse.jface.bindings.keys.KeyStroke;
import org.eclipse.jface.bindings.keys.ParseException;
import org.eclipse.jface.fieldassist.ContentProposalAdapter;
import org.eclipse.jface.fieldassist.IContentProposalProvider;
import org.eclipse.jface.fieldassist.IControlContentAdapter;
import org.eclipse.swt.widgets.Control;

/**
 * Class for XM content proposal adapter.
 *
 * @author Chiranjeevi.Akula
 */
public class XMContentProposalAdapter extends ContentProposalAdapter {

	/**
	 * Constructor for XMContentProposalAdapter Class.
	 *
	 * @param control
	 *            {@link Control}
	 * @throws ParseException
	 *             the parse exception
	 */
	public XMContentProposalAdapter(Control control) throws ParseException {
		this(control, new XMTextContentAdapter(), null, KeyStroke.getInstance("Ctrl+Space"), getActivationCharacters());
	}

	/**
	 * Constructor for XMContentProposalAdapter Class.
	 *
	 * @param control
	 *            {@link Control}
	 * @param controlContentAdapter
	 *            {@link IControlContentAdapter}
	 * @param proposalProvider
	 *            {@link IContentProposalProvider}
	 * @param keyStroke
	 *            {@link KeyStroke}
	 * @param autoActivationCharacters
	 *            {@link char[]}
	 */
	public XMContentProposalAdapter(Control control, IControlContentAdapter controlContentAdapter,
			IContentProposalProvider proposalProvider, KeyStroke keyStroke, char[] autoActivationCharacters) {
		super(control, controlContentAdapter, proposalProvider, keyStroke, autoActivationCharacters);
	}

	/**
	 * Gets the activation characters.
	 *
	 * @return the activation characters
	 */
	private static char[] getActivationCharacters() {
		final StringBuilder stringBuffer = new StringBuilder();
		// ASCII A-Z
		for (int i = 0; i < 26; i++) {
			stringBuffer.append((char) (65 + i));
		}

		// ASCII a-z
		for (int i = 0; i < 26; i++) {
			stringBuffer.append((char) (97 + i));
		}
		// ASCII 0-9
		for (int i = 0; i < 10; i++) {
			stringBuffer.append((char) (48 + i));
		}

		return stringBuffer.toString().toCharArray();
	}

}
