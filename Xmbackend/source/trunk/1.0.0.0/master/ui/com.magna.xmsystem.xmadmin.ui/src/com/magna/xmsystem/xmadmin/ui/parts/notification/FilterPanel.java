package com.magna.xmsystem.xmadmin.ui.parts.notification;

import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.events.ControlAdapter;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import com.magna.xmsystem.ui.controls.widgets.MagnaCustomCombo;
import com.magna.xmsystem.xmadmin.message.MessageRegistry;
import com.magna.xmsystem.xmadmin.util.XMAdminUtil;

/**
 * The Class FilterPanel.
 * 
 * @author archita.patel
 */
public class FilterPanel extends Composite {

	/** The admin area filter text. */
	private Text adminAreaFilterText;

	/** The admin area filter button. */
	private Button adminAreaFilterButton;

	/** The admin area filter combo. */
	private MagnaCustomCombo adminAreaFilterCombo;

	/** The user filter text. */
	private Text userFilterText;

	/** The user filter button. */
	private Button userFilterButton;

	/** The user filter combo. */
	private MagnaCustomCombo userFilterCombo;

	/** The project filter text. */
	private Text projectFilterText;

	/** The project filter button. */
	private Button projectFilterButton;

	/** The project filter combo. */
	private MagnaCustomCombo projectFilterCombo;

	/** The admin area filter group. */
	//private Group adminAreaFilterGroup;

	/** The user filter group. */
	//private Group userFilterGroup;

	/** The project filter group. */
	//private Group projectFilterGroup;

	/** The site filter group. */
	//private Group siteFilterGroup;

	/** The site filter text. */
	private Text siteFilterText;

	/** The site filter combo. */
	private MagnaCustomCombo siteFilterCombo;

	/** The filter label site. */
	private Label filterLabelSite;

	/** The filter label admin area. */
	private Label filterLabelAdminArea;

	/** The filter label project. */
	private Label filterLabelProject;

	/** The filter label user. */
	private Label filterLabelUser;

	/** Member variable 'site filter button' for {@link Button}. */
	private Button siteFilterButton;

	/**
	 * Instantiates a new filter panel.
	 *
	 * @param parent
	 *            the parent
	 */
	public FilterPanel(Composite parent) {
		super(parent, SWT.NONE);
		createFilterPanel();
	}

	/**
	 * Creates the filter panel.
	 */
	private void createFilterPanel() {
		ScrolledComposite scrolledComposite = XMAdminUtil.getInstance().createScrolledComposite(this);
		final Composite filterPanelContainer = new Composite(scrolledComposite, SWT.NONE);
		filterPanelContainer.setLayout(new GridLayout(10, false));
		filterPanelContainer.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		
		//final GridLayout filterGridLayout = new GridLayout(5, false);
		//final GridData filterGridData = new GridData(GridData.FILL_HORIZONTAL);

		final GridData filterButtonsGridData = new GridData();
		filterButtonsGridData.widthHint = 30;
		filterButtonsGridData.heightHint = 30;
		
		/*final Composite siteLblCont = new Composite(filterPanelContainer, SWT.NONE);
		final GridLayout lblContLayout = new GridLayout(1, false);
		lblContLayout.marginRight = 0;
		lblContLayout.marginLeft = 0;
		lblContLayout.marginTop = 0;
		lblContLayout.marginBottom = 0;
		lblContLayout.marginWidth = 0;
		lblContLayout.marginHeight = 0;
		siteLblCont.setLayout(lblContLayout);
		GridDataFactory.fillDefaults().grab(false, false).span(1, 1).align(SWT.FILL, SWT.CENTER)
				.applyTo(siteLblCont);*/
		

		this.filterLabelSite = new Label(filterPanelContainer, SWT.NONE);
		GridDataFactory.fillDefaults().grab(true, false).span(1, 1).align(SWT.FILL, SWT.CENTER).minSize(120, SWT.DEFAULT).applyTo(this.filterLabelSite);
		this.siteFilterText = new Text(filterPanelContainer, SWT.BORDER);
		GridDataFactory.fillDefaults().grab(true, false).span(1, 1).minSize(100, SWT.DEFAULT).align(SWT.FILL, SWT.CENTER)
				.applyTo(this.siteFilterText);
		this.siteFilterButton = new Button(filterPanelContainer, SWT.PUSH);
		this.siteFilterButton.setLayoutData(filterButtonsGridData);

		this.siteFilterCombo = new MagnaCustomCombo(filterPanelContainer, SWT.BORDER | SWT.READ_ONLY);
		GridDataFactory.fillDefaults().grab(true, false).span(2, 1).minSize(120, SWT.DEFAULT).align(SWT.FILL, SWT.CENTER)
				.applyTo(this.siteFilterCombo);

		final Composite adminAreaLblCont = new Composite(filterPanelContainer, SWT.NONE);
		final GridLayout lblContLayout1 = new GridLayout(1, false);
		lblContLayout1.marginRight = 0;
		lblContLayout1.marginLeft = 20;
		lblContLayout1.marginTop = 0;
		lblContLayout1.marginBottom = 0;
		lblContLayout1.marginWidth = 0;
		lblContLayout1.marginHeight = 0;
		adminAreaLblCont.setLayout(lblContLayout1);
		GridDataFactory.fillDefaults().grab(false, false).span(1, 1).align(SWT.FILL, SWT.CENTER)
				.applyTo(adminAreaLblCont);
		
		this.filterLabelAdminArea = new Label(adminAreaLblCont, SWT.NONE);
		GridDataFactory.fillDefaults().grab(true, false).span(1, 1).align(SWT.RIGHT, SWT.CENTER).minSize(200, SWT.DEFAULT).applyTo(this.filterLabelAdminArea);
		this.adminAreaFilterText = new Text(filterPanelContainer, SWT.BORDER);
		GridDataFactory.fillDefaults().grab(true, false).span(1, 1).minSize(100, SWT.DEFAULT).align(SWT.FILL, SWT.CENTER)
				.applyTo(this.adminAreaFilterText);
		this.adminAreaFilterButton = new Button(filterPanelContainer, SWT.PUSH);
		this.adminAreaFilterButton.setLayoutData(filterButtonsGridData);
		this.adminAreaFilterCombo = new MagnaCustomCombo(filterPanelContainer, SWT.BORDER | SWT.READ_ONLY);
		GridDataFactory.fillDefaults().grab(true, false).span(2, 1).align(SWT.FILL, SWT.CENTER).minSize(120, SWT.DEFAULT)
				.applyTo(this.adminAreaFilterCombo);

		/*final Composite projectblCont = new Composite(filterPanelContainer, SWT.NONE);
		final GridLayout lblContLayout2 = new GridLayout(1, false);
		lblContLayout2.marginRight = 0;
		lblContLayout2.marginLeft = 0;
		lblContLayout2.marginTop = 0;
		lblContLayout2.marginBottom = 0;
		lblContLayout2.marginWidth = 0;
		lblContLayout2.marginHeight = 0;
		projectblCont.setLayout(lblContLayout2);
		GridDataFactory.fillDefaults().grab(false, false).span(1, 1).align(SWT.FILL, SWT.CENTER)
				.applyTo(projectblCont);*/
		
		this.filterLabelProject = new Label(filterPanelContainer, SWT.NONE);
		GridDataFactory.fillDefaults().grab(true, false).span(1, 1).align(SWT.FILL, SWT.CENTER).minSize(120, SWT.DEFAULT).applyTo(this.filterLabelProject);
		this.projectFilterText = new Text(filterPanelContainer, SWT.BORDER);
		GridDataFactory.fillDefaults().grab(true, false).span(1, 1).align(SWT.FILL, SWT.CENTER).minSize(100, SWT.DEFAULT)
				.applyTo(this.projectFilterText);
		this.projectFilterButton = new Button(filterPanelContainer, SWT.PUSH);
		this.projectFilterButton.setLayoutData(filterButtonsGridData);
		this.projectFilterCombo = new MagnaCustomCombo(filterPanelContainer, SWT.BORDER | SWT.READ_ONLY);
		GridDataFactory.fillDefaults().grab(true, false).span(2, 1).minSize(120, SWT.DEFAULT).align(SWT.FILL, SWT.CENTER)
				.applyTo(this.projectFilterCombo);

		final Composite userLblCont = new Composite(filterPanelContainer, SWT.NONE);
		final GridLayout lblContLayout3 = new GridLayout(1, false);
		lblContLayout3.marginRight = 0;
		lblContLayout3.marginLeft = 20;
		lblContLayout3.marginTop = 0;
		lblContLayout3.marginBottom = 0;
		lblContLayout3.marginWidth = 0;
		lblContLayout3.marginHeight = 0;
		userLblCont.setLayout(lblContLayout3);
		GridDataFactory.fillDefaults().grab(false, false).span(1, 1).align(SWT.FILL, SWT.CENTER)
				.applyTo(userLblCont);
		
		this.filterLabelUser = new Label(userLblCont, SWT.NONE);
		GridDataFactory.fillDefaults().grab(true, false).span(1, 1).align(SWT.RIGHT, SWT.CENTER).minSize(200, SWT.DEFAULT).applyTo(this.filterLabelUser);
		this.userFilterText = new Text(filterPanelContainer, SWT.BORDER);
		GridDataFactory.fillDefaults().grab(true, false).span(1, 1).align(SWT.FILL, SWT.CENTER).minSize(100, SWT.DEFAULT)
				.applyTo(this.userFilterText);
		this.userFilterButton = new Button(filterPanelContainer, SWT.PUSH);
		this.userFilterButton.setLayoutData(filterButtonsGridData);
		this.userFilterCombo = new MagnaCustomCombo(filterPanelContainer, SWT.BORDER | SWT.READ_ONLY);
		GridDataFactory.fillDefaults().grab(true, false).span(2, 1).minSize(120, SWT.DEFAULT).align(SWT.FILL, SWT.CENTER)
				.applyTo(this.userFilterCombo);
		
		scrolledComposite.setContent(filterPanelContainer);
		scrolledComposite.setSize(filterPanelContainer.getSize());
		scrolledComposite.setExpandVertical(true);
		scrolledComposite.setExpandHorizontal(true);
		scrolledComposite.update();

		scrolledComposite.addControlListener(new ControlAdapter() {
			public void controlResized(final ControlEvent e) {
				//Rectangle rectangle = filterPanelContainer.getClientArea();
				scrolledComposite.setMinSize(filterPanelContainer.computeSize(SWT.DEFAULT, SWT.DEFAULT));
			}
		});
	}

	/**
	 * Register messages.
	 *
	 * @param registry
	 *            the registry
	 */
	public void registerMessages(final MessageRegistry registry) {
		//registry.register(this.siteFilterGroup::setText, (message) -> message.siteFilterGroup);
		//registry.register(this.adminAreaFilterGroup::setText, (message) -> message.adminAreaFilterGroup);
		//registry.register(this.projectFilterGroup::setText, (message) -> message.projectFilterGroup);
		//registry.register(this.userFilterGroup::setText, (message) -> message.userFilterGroup);
		registry.register(this.filterLabelSite::setText, (message) -> message.siteFilterLabel/*message.filterLabel*/);
		registry.register(this.filterLabelAdminArea::setText, (message) -> message.adminAreaFilterLabel/*message.filterLabel*/);
		registry.register(this.filterLabelProject::setText, (message) -> message.projectFilterLabel/*message.filterLabel*/);
		registry.register(this.filterLabelUser::setText, (message) -> message.userFilterLabel/*message.filterLabel*/);
		registry.register(this.siteFilterButton::setText, (message) -> message.allBtnLabel);
		registry.register(this.adminAreaFilterButton::setText, (message) -> message.allBtnLabel);
		registry.register(this.projectFilterButton::setText, (message) -> message.allBtnLabel);
		registry.register(this.userFilterButton::setText, (message) -> message.allBtnLabel);

	}

	
	/**
	 * Enable site filter.
	 *
	 * @param flag
	 *            the flag
	 */
	public void enableSiteFilter(boolean flag) {
		this.siteFilterCombo.setEnabled(flag);
		this.siteFilterText.setEnabled(flag);
		this.siteFilterButton.setEnabled(flag);
	}

	/**
	 * Enable admin area filter.
	 *
	 * @param flag
	 *            the flag
	 */
	public void enableAdminAreaFilter(boolean flag) {
		this.adminAreaFilterCombo.setEnabled(flag);
		this.adminAreaFilterText.setEnabled(flag);
		this.adminAreaFilterButton.setEnabled(flag);
	}

	/**
	 * Enable project filter.
	 *
	 * @param flag
	 *            the flag
	 */
	public void enableProjectFilter(boolean flag) {
		this.projectFilterCombo.setEnabled(flag);
		this.projectFilterText.setEnabled(flag);
		this.projectFilterButton.setEnabled(flag);
	}

	/**
	 * Enable user filter.
	 *
	 * @param flag
	 *            the flag
	 */
	public void enableUserFilter(boolean flag) {
		this.userFilterCombo.setEnabled(flag);
		this.userFilterText.setEnabled(flag);
		this.userFilterButton.setEnabled(flag);
	}

	
	/**
	 * Gets the admin area filter text.
	 *
	 * @return the admin area filter text
	 */
	public Text getAdminAreaFilterText() {
		return adminAreaFilterText;
	}

	/**
	 * Gets the admin area filter combo.
	 *
	 * @return the admin area filter combo
	 */
	public MagnaCustomCombo getAdminAreaFilterCombo() {
		return adminAreaFilterCombo;
	}

	/**
	 * Gets the user filter text.
	 *
	 * @return the user filter text
	 */
	public Text getUserFilterText() {
		return userFilterText;
	}

	/**
	 * Gets the user filter combo.
	 *
	 * @return the user filter combo
	 */
	public MagnaCustomCombo getUserFilterCombo() {
		return userFilterCombo;
	}

	/**
	 * Gets the project filter text.
	 *
	 * @return the project filter text
	 */
	public Text getProjectFilterText() {
		return projectFilterText;
	}

	/**
	 * Gets the project filter combo.
	 *
	 * @return the project filter combo
	 */
	public MagnaCustomCombo getProjectFilterCombo() {
		return projectFilterCombo;
	}

	/**
	 * Gets the site filter text.
	 *
	 * @return the site filter text
	 */
	public Text getSiteFilterText() {
		return siteFilterText;
	}

	/**
	 * Gets the site filter combo.
	 *
	 * @return the site filter combo
	 */
	public MagnaCustomCombo getSiteFilterCombo() {
		return siteFilterCombo;
	}

	/**
	 * Gets the admin area filter button.
	 *
	 * @return the admin area filter button
	 */
	public Button getAdminAreaFilterButton() {
		return adminAreaFilterButton;
	}

	/**
	 * Sets the admin area filter button.
	 *
	 * @param adminAreaFilterButton the new admin area filter button
	 */
	public void setAdminAreaFilterButton(Button adminAreaFilterButton) {
		this.adminAreaFilterButton = adminAreaFilterButton;
	}

	/**
	 * Gets the user filter button.
	 *
	 * @return the user filter button
	 */
	public Button getUserFilterButton() {
		return userFilterButton;
	}

	/**
	 * Gets the project filter button.
	 *
	 * @return the project filter button
	 */
	public Button getProjectFilterButton() {
		return projectFilterButton;
	}

	/**
	 * Gets the site filter button.
	 *
	 * @return the site filter button
	 */
	public Button getSiteFilterButton() {
		return siteFilterButton;
	}
}
