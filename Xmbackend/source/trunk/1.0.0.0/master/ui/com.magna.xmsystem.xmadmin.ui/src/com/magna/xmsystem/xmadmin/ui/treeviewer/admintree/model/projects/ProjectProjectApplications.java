package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.projectapplicationmodel.ProjectApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.refs.RelationObj;

/**
 * The Class ProjectApplicationsModel.
 */
public class ProjectProjectApplications implements IAdminTreeChild {
	
	/** The parent. */
	private IAdminTreeChild parent;
	
	private Map<String, IAdminTreeChild> projectProjectAppChild;
	
	/**
	 * Instantiates a new project applications model.
	 *
	 * @param parent the parent
	 */
	public ProjectProjectApplications(final IAdminTreeChild parent) {
		this.parent = parent;
		this.projectProjectAppChild = new LinkedHashMap<>();
	}
	
	/**
	 * Adds the.
	 *
	 * @param projectProjectAppChildId the project project app child id
	 * @param child the child
	 * @return the i admin tree child
	 */
	public IAdminTreeChild add(final String projectProjectAppChildId, final IAdminTreeChild child) {
		child.setParent(this);
		IAdminTreeChild returnVal = this.projectProjectAppChild.put(projectProjectAppChildId, child);
		if (child instanceof RelationObj && ((RelationObj) child).getRefObject() instanceof ProjectApplication) {
			sort();
		}
		return returnVal;
	}
	
	/**
	 * Removes the.
	 *
	 * @param projectProjectAppChildId the project project app child id
	 * @return the i admin tree child
	 */
	public IAdminTreeChild remove(final String projectProjectAppChildId) {
		return this.projectProjectAppChild.remove(projectProjectAppChildId);
	}

	/**
	 * Method for Removes the all.
	 */
	public void removeAll() {
		this.projectProjectAppChild.clear();
	}
	
	/**
	 * Gets the project project app child.
	 *
	 * @return the project project app child
	 */
	public Map<String, IAdminTreeChild> getProjectProjectAppChild() {
		return projectProjectAppChild;
	}
	
	/**
	 * Gets the project project app collection.
	 *
	 * @return the project project app collection
	 */
	public Collection<IAdminTreeChild> getProjectProjectAppCollection() {
		return this.projectProjectAppChild.values();
	}

	/* (non-Javadoc)
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#getParent()
	 */
	@Override
	public IAdminTreeChild getParent() {
		return this.parent;
	}

	/* (non-Javadoc)
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#setParent(com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild)
	 */
	@Override
	public void setParent(IAdminTreeChild parent) {
		this.parent = parent;
		
	}
	/**
	 * Sort.
	 */
	public void sort() {
		final LinkedHashMap<String, IAdminTreeChild> collect = this.projectProjectAppChild.entrySet().stream().sorted(
				(e1, e2) -> ((ProjectApplication) (((RelationObj) e1.getValue()).getRefObject())).getName()
				.compareTo(((ProjectApplication) (((RelationObj) e2.getValue()).getRefObject())).getName()))
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
		this.projectProjectAppChild = collect;
	}
}
