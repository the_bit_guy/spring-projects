package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.userapplicationmodel.UserApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.refs.RelationObj;

/**
 * Class for User AA user applications.
 *
 * @author Chiranjeevi.Akula
 */
public class UserAAUserApplications implements IAdminTreeChild {

	/** Member variable 'parent' for {@link IAdminTreeChild}. */
	private IAdminTreeChild parent;

	/**
	 * Member variable 'user AA user app children' for
	 * {@link Map<String,IAdminTreeChild>}.
	 */
	private Map<String, IAdminTreeChild> userAAUserAppChildren;

	/**
	 * Constructor for UserAAUserApplications Class.
	 *
	 * @param parent
	 *            {@link IAdminTreeChild}
	 */
	public UserAAUserApplications(final IAdminTreeChild parent) {
		this.parent = parent;
		this.userAAUserAppChildren = new LinkedHashMap<>();

		addFixedChildren();
	}

	/**
	 * Method for Adds the fixed children.
	 */
	public void addFixedChildren() {
		this.userAAUserAppChildren.put(UserAAUserAppAllowed.class.getName(), new UserAAUserAppAllowed(this));
		this.userAAUserAppChildren.put(UserAAUserAppForbidden.class.getName(), new UserAAUserAppForbidden(this));
	}

	/**
	 * Method for Adds the.
	 *
	 * @param userAAUserAppChildId
	 *            {@link String}
	 * @param child
	 *            {@link IAdminTreeChild}
	 * @return the i admin tree child {@link IAdminTreeChild}
	 */
	public IAdminTreeChild add(final String userAAUserAppChildId, final IAdminTreeChild child) {
		child.setParent(this);
		IAdminTreeChild returnVal = this.userAAUserAppChildren.put(userAAUserAppChildId, child);
		if (child instanceof RelationObj && ((RelationObj) child).getRefObject() instanceof UserApplication) {
			sort();
		}
		return returnVal;
	}

	/**
	 * Method for Removes the.
	 *
	 * @param userChildUserAppChildId
	 *            {@link String}
	 * @return the i admin tree child {@link IAdminTreeChild}
	 */
	public IAdminTreeChild remove(final String userChildUserAppChildId) {
		return this.userAAUserAppChildren.remove(userChildUserAppChildId);
	}

	/**
	 * Gets the user AA user app children collection.
	 *
	 * @return the user AA user app children collection
	 */
	public Collection<IAdminTreeChild> getUserAAUserAppChildrenCollection() {
		return this.userAAUserAppChildren.values();
	}

	/**
	 * Gets the user AA user app children.
	 *
	 * @return the user AA user app children
	 */
	public Map<String, IAdminTreeChild> getUserAAUserAppChildren() {
		return userAAUserAppChildren;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#
	 * getParent()
	 */
	@Override
	public IAdminTreeChild getParent() {
		return this.parent;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#
	 * setParent(com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.
	 * IAdminTreeChild)
	 */
	@Override
	public void setParent(final IAdminTreeChild parent) {
		this.parent = parent;
	}
	
	/**
	 * Sort.
	 */
	public void sort() {
		final LinkedHashMap<String, IAdminTreeChild> collect = this.userAAUserAppChildren.entrySet().stream().sorted(
				(e1, e2) -> ((UserApplication) (((RelationObj) e1.getValue()).getRefObject())).getName()
				.compareTo(((UserApplication) (((RelationObj) e2.getValue()).getRefObject())).getName()))
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
		this.userAAUserAppChildren = collect;
	}

}
