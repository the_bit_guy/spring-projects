package com.magna.xmsystem.xmadmin.ui.handlers.toolbar;

import java.util.List;

import javax.inject.Inject;

import org.eclipse.core.commands.ParameterizedCommand;
import org.eclipse.e4.core.commands.ECommandService;
import org.eclipse.e4.core.commands.EHandlerService;
import org.eclipse.e4.core.contexts.Active;
import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.model.application.ui.menu.MToolItem;
import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.jface.viewers.IStructuredSelection;

import com.magna.xmsystem.xmadmin.ui.parts.IEditablePart;
import com.magna.xmsystem.xmadmin.ui.parts.InformationPart;
import com.magna.xmsystem.xmadmin.ui.parts.objexp.ObjectExpPage;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.AdminTreeviewer;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaProjectApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaUserApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdministrationArea;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.baseapplicationmodel.BaseAppProjectApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.baseapplicationmodel.BaseAppStartApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.baseapplicationmodel.BaseAppUserApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.baseapplicationmodel.BaseApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.projectapplicationmodel.ProjectAppAdminAreaChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.projectapplicationmodel.ProjectAppUsers;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.projectapplicationmodel.ProjectApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.projectapplicationmodel.ProjectApplicationChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.startapplicationmodel.StartAppAdminAreas;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.startapplicationmodel.StartAppProjects;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.startapplicationmodel.StartAppUsers;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.startapplicationmodel.StartApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.userapplicationmodel.UserAppAdminAreas;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.userapplicationmodel.UserAppUsers;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.userapplicationmodel.UserApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.userapplicationmodel.UserApplicationChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.livemsgconfig.LiveMessage;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.Role;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.directory.Directory;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.projectappgroup.ProjectApplicationGroup;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.projectgroup.ProjectGroupModel;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.userappgroup.UserApplicationGroup;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.usergroup.UserGroupModel;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.Project;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectAdminAreaProjectApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectProjectApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectUserAdminAreaChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.refs.RelationObj;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.Site;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaProjectApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaUserApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.User;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserAAUserApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserAdminAreaChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserProjectAAProjectApplications;
import com.magna.xmsystem.xmadmin.util.CommonConstants;
import com.magna.xmsystem.xmadmin.util.XMAdminUtil;

/**
 * The Class DeleteToolBarHandler.
 * 
 * @author subash.janarthanan
 * 
 */
@SuppressWarnings("restriction")
public class DeleteToolBarHandler {

	/** The handler service. */
	@Inject
	private EHandlerService handlerService;

	/** The command service. */
	@Inject
	private ECommandService commandService;

	/** Inject of {@link EModelService}. */
	@Inject
	private EModelService modelService;

	/** Inject of {@link MApplication}. */
	@Inject
	private MApplication application;

	/**
	 * Execute.
	 */
	@Execute
	public void execute() {
		final AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
		IStructuredSelection selection;
		Object selectionObj = null;

		MPart mPart = (MPart) this.modelService.find(CommonConstants.PART_ID.INFORMATION_PART_ID, application);
		InformationPart rightSidePart2 = (InformationPart) mPart.getObject();
		ObjectExpPage objectExpPage = rightSidePart2.getObjectExpPartUI();
		if (objectExpPage != null && (selection = objectExpPage.getObjExpTableViewer().getStructuredSelection()) != null
				&& (selectionObj = selection.getFirstElement()) != null) {
		} else if (adminTree != null && (selection = adminTree.getStructuredSelection()) != null) {
			selectionObj = selection.getFirstElement();
		}

		if (selectionObj instanceof Site) {
			ParameterizedCommand cmd = commandService.createCommand(CommonConstants.COMMAND_ID.DELETE_SITE, null);
			if (handlerService.canExecute(cmd)) {
				handlerService.executeHandler(cmd);
			}
		} else if (selectionObj instanceof Project) {
			ParameterizedCommand cmd = commandService.createCommand(CommonConstants.COMMAND_ID.DELETE_PROJECT, null);
			if (handlerService.canExecute(cmd)) {
				handlerService.executeHandler(cmd);
			}
		} else if (selectionObj instanceof AdministrationArea) {
			ParameterizedCommand cmd = commandService.createCommand(CommonConstants.COMMAND_ID.DELETE_ADMINAREA, null);
			if (handlerService.canExecute(cmd)) {
				handlerService.executeHandler(cmd);
			}
		} else if (UserApplication.class.getSimpleName().equals(selectionObj.getClass().getSimpleName())) {
			ParameterizedCommand cmd = commandService.createCommand(CommonConstants.COMMAND_ID.DELETE_USERAPP, null);
			if (handlerService.canExecute(cmd)) {
				handlerService.executeHandler(cmd);
			}
		} else if (selectionObj instanceof UserApplicationChild) {
			ParameterizedCommand cmd = commandService.createCommand(CommonConstants.COMMAND_ID.DELETE_USERAPP_CHILD, null);
			if (handlerService.canExecute(cmd)) {
				handlerService.executeHandler(cmd);
			}
		} else if (ProjectApplication.class.getSimpleName().equals(selectionObj.getClass().getSimpleName())) {
			ParameterizedCommand cmd = commandService.createCommand(CommonConstants.COMMAND_ID.DELETE_PROJECTAPP, null);
			if (handlerService.canExecute(cmd)) {
				handlerService.executeHandler(cmd);
			}
		} else if (selectionObj instanceof ProjectApplicationChild) {
			ParameterizedCommand cmd = commandService.createCommand(CommonConstants.COMMAND_ID.DELETE_PROJECTAPP_CHILD, null);
			if (handlerService.canExecute(cmd)) {
				handlerService.executeHandler(cmd);
			}
		}else if (selectionObj instanceof StartApplication) {
			ParameterizedCommand cmd = commandService.createCommand(CommonConstants.COMMAND_ID.DELETE_STARTAPP, null);
			if (handlerService.canExecute(cmd)) {
				handlerService.executeHandler(cmd);
			}
		} else if (selectionObj instanceof BaseApplication) {
			ParameterizedCommand cmd = commandService.createCommand(CommonConstants.COMMAND_ID.DELETE_BASEAPP, null);
			if (handlerService.canExecute(cmd)) {
				handlerService.executeHandler(cmd);
			}
		} else if (selectionObj instanceof User) {
			ParameterizedCommand cmd = commandService.createCommand(CommonConstants.COMMAND_ID.DELETE_USER, null);
			if (handlerService.canExecute(cmd)) {
				handlerService.executeHandler(cmd);
			}
		} else if (selectionObj instanceof Role) {
			ParameterizedCommand cmd = commandService.createCommand(CommonConstants.COMMAND_ID.DELETE_ROLE, null);
			if (handlerService.canExecute(cmd)) {
				handlerService.executeHandler(cmd);
			}
		} else if (selectionObj instanceof UserGroupModel) {
			ParameterizedCommand cmd = commandService.createCommand(CommonConstants.COMMAND_ID.DELETE_USER_GROUP, null);
			if (handlerService.canExecute(cmd)) {
				handlerService.executeHandler(cmd);
			}
		} else if (selectionObj instanceof ProjectGroupModel) {
			ParameterizedCommand cmd = commandService.createCommand(CommonConstants.COMMAND_ID.DELETE_PROJECT_GROUP,
					null);
			if (handlerService.canExecute(cmd)) {
				handlerService.executeHandler(cmd);
			}
		} else if (selectionObj instanceof UserApplicationGroup) {
			ParameterizedCommand cmd = commandService.createCommand(CommonConstants.COMMAND_ID.DELETE_USERAPP_GROUP,
					null);
			if (handlerService.canExecute(cmd)) {
				handlerService.executeHandler(cmd);
			}
		} else if (selectionObj instanceof ProjectApplicationGroup) {
			ParameterizedCommand cmd = commandService.createCommand(CommonConstants.COMMAND_ID.DELETE_PROJECTAPP_GROUP,
					null);
			if (handlerService.canExecute(cmd)) {
				handlerService.executeHandler(cmd);
			}
		} else if (selectionObj instanceof Directory) {
			ParameterizedCommand cmd = commandService.createCommand(CommonConstants.COMMAND_ID.DELETE_DIRECTORY, null);
			if (handlerService.canExecute(cmd)) {
				handlerService.executeHandler(cmd);
			}
		} else if (selectionObj instanceof RelationObj) {
			ParameterizedCommand cmd = commandService.createCommand(CommonConstants.COMMAND_ID.REMOVE_RELATION, null);
			if (handlerService.canExecute(cmd)) {
				handlerService.executeHandler(cmd);
			}
		} else if (selectionObj instanceof LiveMessage) {
			ParameterizedCommand cmd = commandService.createCommand(CommonConstants.COMMAND_ID.DELETE_LIVEMESSAGE,
					null);
			if (handlerService.canExecute(cmd)) {
				handlerService.executeHandler(cmd);
			}
		}
	}

	/**
	 * Can execute.
	 *
	 * @return true, if successful
	 */
	@CanExecute
	public boolean canExecute(@Active MPart part, MApplication application, EModelService service,
			EModelService eModelService) {
		Object selectionObj = null;
		MPart rightHandMPartOfOldPerspective = (MPart) eModelService.find(CommonConstants.PART_ID.INFORMATION_PART_ID,
				application);
		boolean returnValue = true;
		boolean checkValue = false;
		XMAdminUtil instance = XMAdminUtil.getInstance();
		if (rightHandMPartOfOldPerspective.getObject() instanceof IEditablePart
				&& rightHandMPartOfOldPerspective.isDirty()) {
			return false;
		} else if (part.getElementId().equals(CommonConstants.PART_ID.INFORMATION_PART_ID)) {
			MPart mPart = instance.getInformationPart();
			InformationPart rightSidePart = (InformationPart) mPart.getObject();
			ObjectExpPage objectExpPage = rightSidePart.getObjectExpPartUI();
			IStructuredSelection selection = null;
			selection = objectExpPage.getObjExpTableViewer().getStructuredSelection();
			selectionObj = selection.getFirstElement();
			if(selectionObj == null){
				selection = (IStructuredSelection) XMAdminUtil.getInstance().getAdminTree().getSelection();
				selectionObj = selection.getFirstElement();
			}
			if (objectExpPage != null && selection != null && selectionObj != null) {
				if (selectionObj instanceof Site || selectionObj instanceof Project
						|| selectionObj instanceof AdministrationArea || selectionObj instanceof User
						|| selectionObj instanceof UserApplication || selectionObj instanceof ProjectApplication
						|| selectionObj instanceof StartApplication || selectionObj instanceof BaseApplication
						|| selectionObj instanceof UserGroupModel || selectionObj instanceof ProjectGroupModel
						|| selectionObj instanceof UserApplicationGroup
						|| selectionObj instanceof ProjectApplicationGroup || selectionObj instanceof Directory
						|| selectionObj instanceof LiveMessage) {
					checkValue = true;
				} else if (selectionObj instanceof RelationObj) {
					if ((((RelationObj) selectionObj).getContainerObj() instanceof UserAdminAreaChild)
							|| (((RelationObj) selectionObj).getContainerObj() instanceof SiteAdminAreaProjectApplications)
							|| (((RelationObj) selectionObj).getContainerObj() instanceof SiteAdminAreaUserApplications)
							|| (((RelationObj) selectionObj).getContainerObj() instanceof AdminAreaProjectApplications)
							|| (((RelationObj) selectionObj).getContainerObj() instanceof AdminAreaUserApplications)
							|| (((RelationObj) selectionObj).getContainerObj() instanceof ProjectProjectApplications)
							|| (((RelationObj) selectionObj).getContainerObj() instanceof ProjectUserAdminAreaChild)
							|| (((RelationObj) selectionObj).getContainerObj() instanceof UserAppAdminAreas)
							|| (((RelationObj) selectionObj).getContainerObj() instanceof UserAppUsers)
							|| (((RelationObj) selectionObj).getContainerObj() instanceof ProjectAppAdminAreaChild)
							|| (((RelationObj) selectionObj).getContainerObj() instanceof StartAppAdminAreas)
							|| (((RelationObj) selectionObj).getContainerObj() instanceof StartAppProjects)
							|| (((RelationObj) selectionObj).getContainerObj() instanceof UserAAUserApplications)
							|| (((RelationObj) selectionObj)
									.getContainerObj() instanceof UserProjectAAProjectApplications)
							|| (((RelationObj) selectionObj).getContainerObj() instanceof StartAppUsers)
							|| (((RelationObj) selectionObj).getContainerObj() instanceof BaseAppProjectApplications)
							|| (((RelationObj) selectionObj).getContainerObj() instanceof BaseAppUserApplications)
							|| (((RelationObj) selectionObj).getContainerObj() instanceof BaseAppStartApplications)
							|| (((RelationObj) selectionObj)
									.getContainerObj() instanceof ProjectAdminAreaProjectApplications)
							|| (((RelationObj) selectionObj).getContainerObj() instanceof ProjectAppUsers)) {
						checkValue = false;
					} else {
						checkValue = true;
					}
				} else if (selectionObj instanceof Role) {
					final Role role = (Role) selectionObj;
					if (!(CommonConstants.SuperAdminRole.NAME.equals(role.getRoleName()))) {
						checkValue = true;
					}
				} else {
					checkValue = false;
				}
			}
			if (returnValue) {
				List<MToolItem> mToolItems = service.findElements(application,
						"com.magna.xmsystem.xmadmin.ui.handledtoolitem.delete", MToolItem.class, null); //$NON-NLS-1$
				if (mToolItems.size() > 0) {
					mToolItems.get(0).setEnabled(checkValue);
				}
				return checkValue;
			}
		} else {
			if (instance != null) {
				final AdminTreeviewer adminTree = instance.getAdminTree();
				IStructuredSelection selection = adminTree.getStructuredSelection();
				Object treeSelectionObj = selection.getFirstElement();
				if (treeSelectionObj instanceof Site || treeSelectionObj instanceof Project
						|| treeSelectionObj instanceof AdministrationArea || treeSelectionObj instanceof User
						|| treeSelectionObj instanceof UserApplication || treeSelectionObj instanceof ProjectApplication
						|| treeSelectionObj instanceof StartApplication || treeSelectionObj instanceof BaseApplication
						|| treeSelectionObj instanceof UserGroupModel || treeSelectionObj instanceof ProjectGroupModel
						|| treeSelectionObj instanceof UserApplicationGroup
						|| treeSelectionObj instanceof ProjectApplicationGroup || treeSelectionObj instanceof Directory
						|| treeSelectionObj instanceof LiveMessage) {
					return true;
				}
				else if (treeSelectionObj instanceof RelationObj) {
					if ((((RelationObj) treeSelectionObj).getContainerObj() instanceof UserAdminAreaChild)
							|| (((RelationObj) treeSelectionObj).getContainerObj() instanceof SiteAdminAreaProjectApplications)
							|| (((RelationObj) treeSelectionObj).getContainerObj() instanceof SiteAdminAreaUserApplications)
							|| (((RelationObj) treeSelectionObj).getContainerObj() instanceof AdminAreaProjectApplications)
							|| (((RelationObj) treeSelectionObj).getContainerObj() instanceof AdminAreaUserApplications)
							|| (((RelationObj) treeSelectionObj).getContainerObj() instanceof ProjectProjectApplications)
							|| (((RelationObj) treeSelectionObj).getContainerObj() instanceof ProjectUserAdminAreaChild)
							|| (((RelationObj) treeSelectionObj).getContainerObj() instanceof UserAppAdminAreas)
							|| (((RelationObj) treeSelectionObj).getContainerObj() instanceof UserAppUsers)
							|| (((RelationObj) treeSelectionObj).getContainerObj() instanceof ProjectAppAdminAreaChild)
							|| (((RelationObj) treeSelectionObj).getContainerObj() instanceof StartAppAdminAreas)
							|| (((RelationObj) treeSelectionObj).getContainerObj() instanceof StartAppProjects)
							|| (((RelationObj) treeSelectionObj).getContainerObj() instanceof UserAAUserApplications)
							|| (((RelationObj) treeSelectionObj)
									.getContainerObj() instanceof UserProjectAAProjectApplications)
							|| (((RelationObj) treeSelectionObj).getContainerObj() instanceof StartAppUsers)
							|| (((RelationObj) treeSelectionObj).getContainerObj() instanceof BaseAppProjectApplications)
							|| (((RelationObj) treeSelectionObj).getContainerObj() instanceof BaseAppUserApplications)
							|| (((RelationObj) treeSelectionObj).getContainerObj() instanceof BaseAppStartApplications)
							|| (((RelationObj) treeSelectionObj)
									.getContainerObj() instanceof ProjectAdminAreaProjectApplications)
							|| (((RelationObj) treeSelectionObj).getContainerObj() instanceof ProjectAppUsers)) {
						return  false;
					} else {
						return  true;
					}
				}
				if (treeSelectionObj instanceof Role) {
					final Role role = (Role) treeSelectionObj;
					if (!(CommonConstants.SuperAdminRole.NAME.equals(role.getRoleName()))) {
						return true;
					}
				}
			}
		}
		return false;
	}
}
