package com.magna.xmsystem.xmadmin.ui.parts.role.editingsupport;

import com.magna.xmbackend.vo.enums.CreationType;
import com.magna.xmbackend.vo.roles.ObjectPermission;
import com.magna.xmsystem.xmadmin.ui.parts.role.ObjRelationPermissionsTable;
import com.magna.xmsystem.xmadmin.ui.parts.role.ObjectPermissionsTable;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.ObjectRelationType;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.VoPermContainer;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.objRelationPermissions.ObjectRelationPermissions;
import com.magna.xmsystem.xmadmin.util.XMAdminUtil;

/**
 * The Class RemoveEditingSupport.
 * 
 * @author shashwat.anand
 */
public class RemoveEditingSupport extends CheckBoxEditingSupport {
	/** Member variable 'instance' for {@link XMAdminUtil}. */
	private XMAdminUtil instance;
	
	/** Member variable 'viewer' for {@link ObjectPermissionsTable}. */
	private ObjRelationPermissionsTable viewer;
	
	/**
	 * Instantiates a new removes the editing support.
	 *
	 * @param viewer the viewer
	 */
	public RemoveEditingSupport(final ObjRelationPermissionsTable viewer) {
		super(viewer);
		this.viewer = viewer;
		this.instance = XMAdminUtil.getInstance();
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.viewers.EditingSupport#getValue(java.lang.Object)
	 */
	@Override
	protected Object getValue(Object element) {
		if (element instanceof ObjectRelationPermissions) {
			return ((ObjectRelationPermissions) element).isRemove();
		}
		return "N";
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.viewers.EditingSupport#setValue(java.lang.Object, java.lang.Object)
	 */
	@Override
	protected void setValue(Object element, Object value) {
		if (element instanceof ObjectRelationPermissions) {
			ObjectRelationPermissions elem = (ObjectRelationPermissions) element;
			ObjectRelationType objectRelationType = ObjectRelationType.getObjectType(elem.getPermissionName());
			String permissionId = this.instance.getObjectRelPermissionMap().get(objectRelationType).getRemovePermission().getPermissionId();
			VoPermContainer voPermContainer = new VoPermContainer();
			ObjectPermission objectPermission = new ObjectPermission();
			objectPermission.setPermissionId(permissionId);
			voPermContainer.setObjectPermission(objectPermission);
			if (elem.isRemove() && ! (boolean) value) {
				objectPermission.setCreationType(CreationType.DELETE);
				elem.setRemovePermission(voPermContainer);
			} else if (!elem.isRemove() && (boolean) value) {
				objectPermission.setCreationType(CreationType.ADD);
				elem.setRemovePermission(voPermContainer);
			} else {
				return;
			}
			elem.setRemove((boolean) value);
			this.viewer.getCachedObjPermissionMap().put(permissionId, voPermContainer);
			getViewer().refresh(element);
		}
	}

}
