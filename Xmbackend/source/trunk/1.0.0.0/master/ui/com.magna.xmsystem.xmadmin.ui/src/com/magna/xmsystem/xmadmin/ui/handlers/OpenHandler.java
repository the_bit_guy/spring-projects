package com.magna.xmsystem.xmadmin.ui.handlers;

import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Shell;
public class OpenHandler {

	@Execute
	public void execute(Shell shell){
		final FileDialog dialog = new FileDialog(shell);
		dialog.open();
	}
}
