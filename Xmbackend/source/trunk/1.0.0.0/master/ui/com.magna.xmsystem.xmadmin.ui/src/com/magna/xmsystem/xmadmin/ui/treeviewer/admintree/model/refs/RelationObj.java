package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.refs;

import java.beans.PropertyChangeEvent;
import java.util.Date;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.BeanModel;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;

public class RelationObj extends BeanModel implements IAdminTreeChild {

	/** The rel id. */
	private String relId;
	
	/** The ref object. */
	private IAdminTreeChild refObject;
	
	/** The container obj. */
	private IAdminTreeChild containerObj;
	
	/** The is active. */
	private boolean isActive;
	
	/** The expiry date. */
	private Date expiryDate;
   
	/** The expiry days. */
	private String expiryDays;

	/**
	 * @param relId
	 * @param refObject
	 * @param type
	 * @param isActive
	 * @param expiryDate can be null
	 */
	public RelationObj(String relId, IAdminTreeChild refObject, IAdminTreeChild containerObj, boolean isActive,
			Date expiryDate) {
		super();
		this.relId = relId;
		this.refObject = refObject;
		this.containerObj = containerObj;
		this.isActive = isActive;
		this.expiryDate = expiryDate;
	}

	public RelationObj(String relId, IAdminTreeChild refObject, IAdminTreeChild containerObj, boolean isActive,
			Date expiryDate,String expiryDays) {
		super();
		this.relId = relId;
		this.refObject = refObject;
		this.containerObj = containerObj;
		this.isActive = isActive;
		this.expiryDate = expiryDate;
		this.expiryDays = expiryDays;
	}


	/**
	 * @return the relId
	 */
	public String getRelId() {
		return relId;
	}


	/**
	 * @param relId the relId to set
	 */
	public void setRelId(String relId) {
		this.relId = relId;
	}


	/**
	 * @return the refObject
	 */
	public IAdminTreeChild getRefObject() {
		return refObject;
	}


	/**
	 * @param refObject the refObject to set
	 */
	public void setRefObject(IAdminTreeChild refObject) {
		this.refObject = refObject;
	}


	/**
	 * @return the containerObj
	 */
	public IAdminTreeChild getContainerObj() {
		return containerObj;
	}


	/**
	 * @param containerObj the containerObj to set
	 */
	public void setContainerObj(IAdminTreeChild containerObj) {
		this.containerObj = containerObj;
	}


	/**
	 * @return the isActive
	 */
	public boolean isActive() {
		return isActive;
	}


	/**
	 * @param isActive the isActive to set
	 */
	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}


	/**
	 * @return the expiryDate
	 */
	public Date getExpiryDate() {
		return expiryDate;
	}


	/**
	 * @param expiryDate the expiryDate to set
	 */
	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}


	public String getExpiryDays() {
		return expiryDays;
	}


	public void setExpiryDays(String expiryDays) {
		this.expiryDays = expiryDays;
	}


	@Override
	public void propertyChange(PropertyChangeEvent event) {
		// TODO Auto-generated method stub
	}

}
