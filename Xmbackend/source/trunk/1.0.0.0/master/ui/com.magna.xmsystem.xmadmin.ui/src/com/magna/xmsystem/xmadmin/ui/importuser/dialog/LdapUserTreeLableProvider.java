package com.magna.xmsystem.xmadmin.ui.importuser.dialog;

import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.swt.graphics.Image;

import com.magna.xmbackend.vo.ldap.LdapUser;
import com.magna.xmsystem.xmadmin.ui.importuser.wizard.model.LdapUserProperty;

// TODO: Auto-generated Javadoc
/**
 * The Class UserInfoViewerLableProvider.
 * 
 * @author archita.patel
 */
public class LdapUserTreeLableProvider implements ITableLabelProvider,ILabelProvider {

	
	@Override
	public Image getColumnImage(final Object element, final int index) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getColumnText(final Object element,final int columnIndex) {
		
		if (element instanceof LdapUserProperty) {
			switch (columnIndex) {
			case 0:
				return ((LdapUserProperty) element).getPropertName();
			case 1:
				return ((LdapUserProperty) element).getValue();

			}
		}
		return null;
	}
	@Override
	public String getText(final Object element) {
		if (element instanceof LdapUser) {
			return ((LdapUser) element).getsAMAccountName();
		}
		return null;
	}

	@Override
	public void addListener(final ILabelProviderListener arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean isLabelProperty(final Object arg0,final String arg1) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void removeListener(final ILabelProviderListener arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Image getImage(final Object arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	
	
}
