package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.refs.RelationObj;

/**
 * Class for Project user child.
 *
 * @author Chiranjeevi.Akula
 */
public class ProjectUserChild implements IAdminTreeChild {

	/** Member variable 'parent' for {@link IAdminTreeChild}. */
	private IAdminTreeChild parent;

	/**
	 * Member variable 'project user children' for
	 * {@link Map<String,IAdminTreeChild>}.
	 */
	private Map<String, IAdminTreeChild> projectUserChildren;

	/**
	 * Constructor for ProjectUserChild Class.
	 *
	 * @param parent
	 *            {@link IAdminTreeChild}
	 */
	public ProjectUserChild(final IAdminTreeChild parent) {
		this.parent = parent;
		this.projectUserChildren = new LinkedHashMap<>();
	}

	/**
	 * Method for Adds the fixed children.
	 *
	 * @param relationObj
	 *            {@link RelationObj}
	 */
	public void addFixedChildren(RelationObj relationObj) {
		this.projectUserChildren.put(ProjectUserAdminAreas.class.getName(), new ProjectUserAdminAreas(relationObj));
	}

	/**
	 * Gets the project user children collection.
	 *
	 * @return the project user children collection
	 */
	public Collection<IAdminTreeChild> getProjectUserChildrenCollection() {
		return this.projectUserChildren.values();
	}

	/**
	 * Gets the project user children children.
	 *
	 * @return the project user children children
	 */
	public Map<String, IAdminTreeChild> getProjectUserChildren() {
		return projectUserChildren;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#
	 * getParent()
	 */
	@Override
	public IAdminTreeChild getParent() {
		return this.parent;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#
	 * setParent(com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.
	 * IAdminTreeChild)
	 */
	@Override
	public void setParent(final IAdminTreeChild parent) {
		this.parent = parent;
	}

}
