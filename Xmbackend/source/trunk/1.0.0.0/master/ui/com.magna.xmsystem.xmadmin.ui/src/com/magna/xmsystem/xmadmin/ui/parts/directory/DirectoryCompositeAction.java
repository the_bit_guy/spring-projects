package com.magna.xmsystem.xmadmin.ui.parts.directory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.inject.Inject;

import org.eclipse.core.databinding.Binding;
import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.beans.BeanProperties;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.core.databinding.observable.value.IValueChangeListener;
import org.eclipse.core.databinding.observable.value.ValueChangeEvent;
import org.eclipse.e4.core.services.nls.Translation;
import org.eclipse.e4.ui.model.application.ui.MDirtyable;
import org.eclipse.jface.databinding.fieldassist.ControlDecorationSupport;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreePath;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.VerifyEvent;
import org.eclipse.swt.events.VerifyListener;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Link;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.ToolItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.ResourceAccessException;

import com.magna.xmbackend.entities.DirectoryTbl;
import com.magna.xmbackend.entities.DirectoryTranslationTbl;
import com.magna.xmbackend.entities.LanguagesTbl;
import com.magna.xmsystem.dependencies.utils.LANG_ENUM;
import com.magna.xmsystem.dependencies.utils.XMSystemUtil;
import com.magna.xmsystem.ui.controls.widgets.CustomMessageDialog;
import com.magna.xmsystem.xmadmin.message.Message;
import com.magna.xmsystem.xmadmin.message.MessageRegistry;
import com.magna.xmsystem.xmadmin.restclient.directory.DirectoryController;
import com.magna.xmsystem.xmadmin.ui.dialogs.lang.ControlModel;
import com.magna.xmsystem.xmadmin.ui.dialogs.lang.TextAreaModifyListener;
import com.magna.xmsystem.xmadmin.ui.dialogs.lang.XMAdminLangTextAreaDialog;
import com.magna.xmsystem.xmadmin.ui.dialogs.lang.XMAdminLangTextDialog;
import com.magna.xmsystem.xmadmin.ui.parts.icons.IconDialog;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.AdminTreeviewer;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.AdminTreeFactory;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.icon.Icon;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.directory.Directories;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.directory.Directory;
import com.magna.xmsystem.xmadmin.ui.validation.NameValidation;
import com.magna.xmsystem.xmadmin.ui.validation.StatusValidation;
import com.magna.xmsystem.xmadmin.ui.validation.SymbolValidation;
import com.magna.xmsystem.xmadmin.util.CommonConstants;
import com.magna.xmsystem.xmadmin.util.MessageType;
import com.magna.xmsystem.xmadmin.util.XMAdminUtil;

/**
 * The Class DirectoryCompositeAction.
 * 
 * @author Archita.patel
 */
public class DirectoryCompositeAction extends DirectoryCompositeUI {
	/** Logger instance. */
	private static final Logger LOGGER = LoggerFactory.getLogger(DirectoryCompositeAction.class);

	/** Member variable for message. */
	@Inject
	@Translation
	private Message messages;

	/** Member variable for {@link MessageRegistry}. */
	@Inject
	private MessageRegistry registry;

	/** MDirtyable flag. */
	private MDirtyable dirty;

	/** Member variable to store old model. */
	private Directory oldModel;

	/** Member variable for directory model. */
	private Directory directoryModel;

	/** Member variable for widgetValue. */
	private IObservableValue<?> widgetValue;

	/** Member variable for modelValue. */
	private IObservableValue<?> modelValue;

	/**
	 * Member variable for data binding context the DataBindingContext object
	 * will manage the databindings.
	 */
	final DataBindingContext dataBindContext = new DataBindingContext();

	/**
	 * Constructor.
	 *
	 * @param parent
	 *            {@link Composite}
	 */
	@Inject
	public DirectoryCompositeAction(final Composite parent) {
		super(parent, SWT.NONE);
		initListeners();
	}

	/**
	 * Add the listener to widgets.
	 */
	private void initListeners() {
		this.descLink.addSelectionListener(new SelectionAdapter() {
			/**
			 * description link handler
			 */
			@Override
			public void widgetSelected(final SelectionEvent event) {
				final Link linkWidget = (Link) event.widget;
				openDescDialog(linkWidget.getShell());
			}
		});

		// Event handling when users click on remarks lang links.
		this.remarksTranslate.addSelectionListener(new SelectionAdapter() {
			/**
			 * remarks text link handler
			 */
			@Override
			public void widgetSelected(final SelectionEvent event) {
				final Link linkWidget = (Link) event.widget;
				openRemarkDialog(linkWidget.getShell());
			}
		});

		if (this.saveBtn != null) {
			this.saveBtn.addSelectionListener(new SelectionAdapter() {

				/**
				 * Save button handler
				 */
				@Override
				public void widgetSelected(final SelectionEvent event) {
					saveDirectoryHandler();
				}
			});
		}

		if (this.cancelBtn != null) {
			this.cancelBtn.addSelectionListener(new SelectionAdapter() {

				/**
				 * Cancel button handler
				 */
				@Override
				public void widgetSelected(final SelectionEvent event) {
					cancelDirectoryHandler();
				}
			});
		}

		this.toolItem.addSelectionListener(new SelectionAdapter() {

			/**
			 * Symbol button handler
			 */
			@Override
			public void widgetSelected(final SelectionEvent event) {
				if ((boolean) toolItem.getData("editable")) {
					final ToolItem widget = (ToolItem) event.widget;
					final IconDialog dialog = new IconDialog(widget.getParent().getShell(),
							messages.browseIconDialogTitle, messages.icontableviewerSecondColumnLabel);

					final int returnVal = dialog.open();
					if (IDialogConstants.OK_ID == returnVal) {
						final Icon checkedIcon = dialog.getCheckedIcon();
						toolItem.setImage(XMSystemUtil.getInstance().getImage(this.getClass(), checkedIcon.getIconPath(), true, true));
						directoryModel.setIcon(checkedIcon);
					}
				}
			}
		});

		this.txtRemarks.addModifyListener(new TextAreaModifyListener(this.lblremarksCount, Directory.REMARK_LIMIT));
		
		this.txtRemarks.addVerifyListener(new VerifyListener() {

			@Override
			public void verifyText(VerifyEvent event) {
				String source = ((Text) event.widget).getText();
				final String remarkText = source.substring(0, event.start) + event.text + source.substring(event.end);
				int length = remarkText.length();
				if (length > Directory.REMARK_LIMIT) {
					event.doit = false;

				}
			}
		});
	}

	/**
	 * Cancel directory handler.
	 */
	public void cancelDirectoryHandler() {
		if (directoryModel == null) {
			dirty.setDirty(false);
			return;
		}
		String siteId = CommonConstants.EMPTY_STR;
		final int operationMode = this.directoryModel.getOperationMode();
		final Directory oldModel = getOldModel();
		if (oldModel != null) {
			siteId = oldModel.getDirectoryId();
		}
		setDirectoryModel(null);
		setOldModel(null);
		this.saveBtn.setEnabled(true);
		dirty.setDirty(false);
		final AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
		final Directories directories = AdminTreeFactory.getInstance().getDirectories();
		if (operationMode == CommonConstants.OPERATIONMODE.CHANGE) {
			final IStructuredSelection selection = (IStructuredSelection) adminTree.getSelection();
			final Object firstElement = selection.getFirstElement();
			if (firstElement != null && firstElement.equals(directories.getDirectoriesChildren().get(siteId))) {
				adminTree.setSelection(new StructuredSelection(directories.getDirectoriesChildren().get(siteId)), true);
			}
		} else {
			adminTree.setSelection(new StructuredSelection(directories), true);
		}
	}

	/**
	 * Save directory handler.
	 */
	public void saveDirectoryHandler() {
		// validate the model
		saveDescAndRemarks();
		if (validate()) {

			if (!directoryModel.getName().isEmpty() && directoryModel.getIcon().getIconId() != null) {
				if (directoryModel.getOperationMode() == CommonConstants.OPERATIONMODE.CREATE) {
					createDirectoryOperation();
				} else if (directoryModel.getOperationMode() == CommonConstants.OPERATIONMODE.CHANGE) {
					changeDirectoryOperation();
				}
			}
		}
	}

	/**
	 * Validate.
	 *
	 * @return true, if successful
	 */
	private boolean validate() {
		final String directoryName = this.directoryModel.getName();
		Icon icon;
		if ((XMSystemUtil.isEmpty(directoryName) && (icon = this.directoryModel.getIcon()) != null
				&& XMSystemUtil.isEmpty(icon.getIconName()))) {
			CustomMessageDialog.openError(this.getShell(), messages.nameSymbolErrorTitle, messages.nameSymbolError);
			return false;
		}
		if (XMSystemUtil.isEmpty(directoryName)) {
			CustomMessageDialog.openError(this.getShell(), messages.nameErrorTitle, messages.nameError);
			return false;
		}
		if ((icon = this.directoryModel.getIcon()) != null && XMSystemUtil.isEmpty(icon.getIconName())) {
			CustomMessageDialog.openError(this.getShell(), messages.symbolErrorTitle, messages.symbolError);
			return false;
		}
		final Directories directories = AdminTreeFactory.getInstance().getDirectories();
		final Collection<IAdminTreeChild> directoriesCollection = directories.getDirectoriesCollection();
		if (directoryModel.getOperationMode() == CommonConstants.OPERATIONMODE.CHANGE) {
			if (!directoryName.equalsIgnoreCase(this.oldModel.getName())) {
				final Map<String, Long> result = directoriesCollection.parallelStream().collect(
						Collectors.groupingBy(directory -> ((Directory) directory).getName().toUpperCase(), Collectors.counting()));
				if (result.containsKey(directoryName.toUpperCase())) {
					CustomMessageDialog.openError(this.getShell(), messages.existingDirectoryNameTitle,
							messages.existingDirectoryNameError);
					return false;
				}
			}
		} else if (this.directoryModel.getOperationMode() == CommonConstants.OPERATIONMODE.CREATE) {
			for (final IAdminTreeChild directory : directoriesCollection) {
				if (directoryName.equalsIgnoreCase(((Directory) directory).getName())) {
					CustomMessageDialog.openError(this.getShell(), messages.existingDirectoryNameTitle,
							messages.existingDirectoryNameError);
					return false;
				}
			}
		}

		return true;
	}

	/**
	 * Change directory operation.
	 */
	private void changeDirectoryOperation() {
		try {
			DirectoryController directoryController = new DirectoryController();
			boolean isUpdated = directoryController.updateDirectory(mapVOObjectWithModel());
			if (isUpdated) {
				setOldModel(directoryModel.deepCopyDirectory(true, getOldModel()));
				this.directoryModel.setOperationMode(CommonConstants.OPERATIONMODE.VIEW);
				setOperationMode();
				this.dirty.setDirty(false);
				final Directories directories = AdminTreeFactory.getInstance().getDirectories();
				directories.sort();
				XMAdminUtil.getInstance().getAdminTree().refresh(true);
				XMAdminUtil.getInstance().getAdminTree().setSelection(new StructuredSelection(getOldModel()), true);
				XMAdminUtil.getInstance().updateLogFile(messages.directoryObject + " " + "'"
						+ this.directoryModel.getName() + "'" + " " + messages.objectUpdate,
						MessageType.SUCCESS);
			}
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				CustomMessageDialog.openError(Display.getCurrent().getActiveShell(), messages.errorDialogTitile,
						messages.serverNotReachable);
			}
			LOGGER.warn("Unable to Update Directory data ! " + e);
		}

	}

	/**
	 * Creates the directory operation.
	 */
	private void createDirectoryOperation() {

		try {
			DirectoryController directoryController = new DirectoryController();
			DirectoryTbl directoryVo = directoryController.createDirectory(mapVOObjectWithModel());
			String directoryId = directoryVo.getDirectoryId();
			if (!XMSystemUtil.isEmpty(directoryId)) {
				this.directoryModel.setDirectoryId(directoryId);
				Collection<DirectoryTranslationTbl> directoryTranslationTblCollection = directoryVo
						.getDirectoryTranslationTblCollection();
				for (DirectoryTranslationTbl directoryTranslationTbl : directoryTranslationTblCollection) {
					String directoryTranslationId = directoryTranslationTbl.getDirectoryTranslationId();
					LanguagesTbl languageCode = directoryTranslationTbl.getLanguageCode();
					LANG_ENUM langEnum = LANG_ENUM.getLangEnum(languageCode.getLanguageCode());
					this.directoryModel.setTranslationId(langEnum, directoryTranslationId);
				}

				final AdminTreeFactory instance = AdminTreeFactory.getInstance();
				// Attach model to old model
				if (oldModel == null) { // Attach this to tree
					setOldModel(directoryModel.deepCopyDirectory(false, null));
					instance.getDirectories().add(directoryId, getOldModel());
				}
				this.dirty.setDirty(false);
				final AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
				adminTree.refresh(true);

				adminTree.setSelection(new StructuredSelection(instance.getDirectories()), true);
				final TreePath[] selectionPaths = adminTree.getStructuredSelection().getPaths();
				if (selectionPaths != null && selectionPaths.length > 0) {
					adminTree.setExpandedState(selectionPaths[0], true);
				}
				adminTree.setSelection(new StructuredSelection(getOldModel()), true);
				XMAdminUtil.getInstance().updateLogFile(messages.directoryObject + " " + "'"
						+ this.directoryModel.getName() + "'" + " " + messages.objectCreate,
						MessageType.SUCCESS);
			}
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				CustomMessageDialog.openError(Display.getCurrent().getActiveShell(), messages.errorDialogTitile,
						messages.serverNotReachable);
			}
			LOGGER.warn("Unable to Save data ! " + e);
		}

	}

	private com.magna.xmbackend.vo.directory.DirectoryRequest mapVOObjectWithModel() {
		com.magna.xmbackend.vo.directory.DirectoryRequest directoryRequest = new com.magna.xmbackend.vo.directory.DirectoryRequest();
		directoryRequest.setId(this.directoryModel.getDirectoryId());
		directoryRequest.setName(this.directoryModel.getName());
		directoryRequest.setIconId(this.directoryModel.getIcon().getIconId());
		List<com.magna.xmbackend.vo.directory.DirectoryTransalation> directoryTranslationList = new ArrayList<>();
		LANG_ENUM[] lang_values = LANG_ENUM.values();
		for (int index = 0; index < lang_values.length; index++) {
			com.magna.xmbackend.vo.directory.DirectoryTransalation directorytranslation = new com.magna.xmbackend.vo.directory.DirectoryTransalation();
			directorytranslation.setLanguageCode(lang_values[index].getLangCode());
			directorytranslation.setRemarks(this.directoryModel.getRemarks(lang_values[index]));
			directorytranslation.setDescription(this.directoryModel.getDescription(lang_values[index]));
			if (this.directoryModel.getOperationMode() == CommonConstants.OPERATIONMODE.CHANGE) {
				String translationId = directoryModel.getTranslationId(lang_values[index]);
				directorytranslation.setId(XMSystemUtil.isEmpty(translationId) ? CommonConstants.EMPTY_STR : translationId);
			}
			directoryTranslationList.add(directorytranslation);
		}

		directoryRequest.setDirectoryTranslations(directoryTranslationList);

		return directoryRequest;

	}

	/**
	 * Save desc and remarks.
	 */
	private void saveDescAndRemarks() {
		final LANG_ENUM currentLocaleEnum = XMAdminUtil.getInstance().getCurrentLocaleEnum();
		final String desc = txtDesc.getText();
		directoryModel.setDescription(currentLocaleEnum, desc);

		final String remarks = txtRemarks.getText();
		directoryModel.setRemarks(currentLocaleEnum, remarks);
	}

	/**
	 * Register messages.
	 *
	 * @param registry
	 *            the registry
	 */
	public void registerMessages(final MessageRegistry registry) {
		registry.register((text) -> {
			if (grpDirectory != null && !grpDirectory.isDisposed()) {
				grpDirectory.setText(text);
			}
		}, (message) -> {
			if (directoryModel != null) {
				if (directoryModel.getOperationMode() == CommonConstants.OPERATIONMODE.VIEW) {
					return message.objectGroupDisaplyLabel + " \'" + this.directoryModel.getName() + "\'";
				} else if (directoryModel.getOperationMode() == CommonConstants.OPERATIONMODE.CHANGE) {
					return message.objectGroupChangeLabel + " \'" + this.directoryModel.getName() + "\'";
				} else if (directoryModel.getOperationMode() == CommonConstants.OPERATIONMODE.CREATE) {
					return message.directoryGroupCreateLabel;
				}
			}
			return CommonConstants.EMPTY_STR;
		});
		registry.register((text) -> {
			if (lblName != null && !lblName.isDisposed()) {
				lblName.setText(text);
			}
		}, (message) -> {
			if (lblName != null && !lblName.isDisposed()) {
				return getUpdatedWidgetText(message.objectNameLabel, lblName);
			}
			return CommonConstants.EMPTY_STR;
		});
		registry.register((text) -> {
			if (lblDescrition != null && !lblDescrition.isDisposed()) {
				lblDescrition.setText(text);
			}
		}, (message) -> {
			if (lblDescrition != null && !lblDescrition.isDisposed()) {
				return getUpdatedWidgetText(message.objectDescriptionLabel, lblDescrition);
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (lblSymbol != null && !lblSymbol.isDisposed()) {
				lblSymbol.setText(text);
			}
		}, (message) -> {
			if (lblSymbol != null && !lblSymbol.isDisposed()) {
				return getUpdatedWidgetText(message.objectSymbolLabel, lblSymbol);
			}
			return CommonConstants.EMPTY_STR;
		});
		if (saveBtn != null) {
			registry.register((text) -> {
				if (saveBtn != null && !saveBtn.isDisposed()) {
					saveBtn.setText(text);
				}
			}, (message) -> {
				if (saveBtn != null && !saveBtn.isDisposed()) {
					return getUpdatedWidgetText(message.saveButtonText, saveBtn);
				}
				return CommonConstants.EMPTY_STR;
			});
		}
		if (cancelBtn != null) {
			registry.register((text) -> {
				if (cancelBtn != null && !cancelBtn.isDisposed()) {
					cancelBtn.setText(text);
				}
			}, (message) -> {
				if (cancelBtn != null && !cancelBtn.isDisposed()) {
					return getUpdatedWidgetText(message.cancelButtonText, cancelBtn);
				}
				return CommonConstants.EMPTY_STR;
			});
		}
		registry.register((text) -> {
			if (remarksTranslate != null && !remarksTranslate.isDisposed()) {
				remarksTranslate.setText(text);
			}
		}, (message) -> {
			if (remarksTranslate != null && !remarksTranslate.isDisposed()) {
				return getUpdatedWidgetText("<a>" + message.objectTranslationLinkText + "</a>", remarksTranslate);
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (remarksLabel != null && !remarksLabel.isDisposed()) {
				remarksLabel.setText(text);
			}
		}, (message) -> {
			if (remarksLabel != null && !remarksLabel.isDisposed()) {
				return getUpdatedWidgetText(message.objectRemarkLabel, remarksLabel);
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (descLink != null && !descLink.isDisposed()) {
				descLink.setText(text);
			}
		}, (message) -> {
			if (descLink != null && !descLink.isDisposed()) {
				return getUpdatedWidgetText("<a>" + message.objectTranslationLinkText + "</a>", descLink);
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (txtDesc != null && !txtDesc.isDisposed()) {
				txtDesc.setText(text);
				updateDescWidget();
			}
		}, (message) -> {
			if (directoryModel != null && txtDesc != null && !txtDesc.isDisposed()) {
				final LANG_ENUM langEnum = XMAdminUtil.getInstance().getCurrentLocaleEnum();
				return this.directoryModel.getDescription(langEnum) == null ? CommonConstants.EMPTY_STR
						: this.directoryModel.getDescription(langEnum);
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (txtRemarks != null && !txtRemarks.isDisposed()) {
				txtRemarks.setText(text);
				updateRemarkWidget();
			}
		}, (message) -> {
			if (directoryModel != null && txtRemarks != null && !txtRemarks.isDisposed()) {
				final LANG_ENUM langEnum = XMAdminUtil.getInstance().getCurrentLocaleEnum();
				return this.directoryModel.getRemarks(langEnum) == null ? CommonConstants.EMPTY_STR
						: this.directoryModel.getRemarks(langEnum);
			}
			return CommonConstants.EMPTY_STR;
		});
	}

	/**
	 * Gets the updated widget text.
	 *
	 * @param message
	 *            the message
	 * @param control
	 *            the control
	 * @return the updated widget text
	 */
	private String getUpdatedWidgetText(final String message, final Control control) {
		control.requestLayout();
		control.getParent().redraw();
		control.getParent().getParent().update();
		control.getParent().getParent().getParent().update();
		return message;
	}

	/**
	 * Open desc dialog.
	 *
	 * @param shell
	 *            the shell
	 */
	private void openDescDialog(final Shell shell) {
		if (directoryModel == null) {
			return;
		}
		if (directoryModel.getOperationMode() != CommonConstants.OPERATIONMODE.VIEW) {
			final String text = txtDesc.getText();
			final LANG_ENUM currentLocaleEnum = XMAdminUtil.getInstance().getCurrentLocaleEnum();
			directoryModel.setDescription(currentLocaleEnum, text);
		}
		final Map<LANG_ENUM, String> obModelMap = new HashMap<>();
		obModelMap.put(LANG_ENUM.ENGLISH, this.directoryModel.getDescription(LANG_ENUM.ENGLISH));
		obModelMap.put(LANG_ENUM.GERMAN, this.directoryModel.getDescription(LANG_ENUM.GERMAN));
		final boolean isEditable = txtDesc.getEditable();
		final ControlModel controlModel = new ControlModel(this.messages.objectDescriptionLabel, obModelMap,
				Directory.DESC_LIMIT, false, isEditable);
		controlModel.initDefaultLabels(this.messages);
		final XMAdminLangTextDialog dialogArea = new XMAdminLangTextDialog(shell, controlModel);
		final int retVal = dialogArea.open();
		if (retVal == IDialogConstants.OK_ID) {
			final Map<LANG_ENUM, String> descriptionMap = this.directoryModel.getDescriptionMap();
			descriptionMap.put(LANG_ENUM.ENGLISH, controlModel.getObjectModel(LANG_ENUM.ENGLISH));
			descriptionMap.put(LANG_ENUM.GERMAN, controlModel.getObjectModel(LANG_ENUM.GERMAN));
			updateDescWidget();
		}
	}

	/**
	 * Open remark dialog.
	 *
	 * @param shell
	 *            the shell
	 */
	private void openRemarkDialog(final Shell shell) {
		if (directoryModel == null) {
			return;
		}
		if (directoryModel.getOperationMode() != CommonConstants.OPERATIONMODE.VIEW) {
			final String text = txtRemarks.getText();
			final LANG_ENUM currentLocaleEnum = XMAdminUtil.getInstance().getCurrentLocaleEnum();
			directoryModel.setRemarks(currentLocaleEnum, text);
		}
		final Map<LANG_ENUM, String> obModelMap = new HashMap<>();
		obModelMap.put(LANG_ENUM.ENGLISH, this.directoryModel.getRemarks(LANG_ENUM.ENGLISH));
		obModelMap.put(LANG_ENUM.GERMAN, this.directoryModel.getRemarks(LANG_ENUM.GERMAN));
		final boolean isEditable = txtRemarks.getEditable();
		final ControlModel controlModel = new ControlModel(this.messages.objectRemarkLabel, obModelMap,
				Directory.REMARK_LIMIT, false, isEditable);
		controlModel.initDefaultLabels(this.messages);
		final XMAdminLangTextAreaDialog dialogArea = new XMAdminLangTextAreaDialog(shell, controlModel);
		final int retVal = dialogArea.open();
		if (retVal == IDialogConstants.OK_ID) {
			final Map<LANG_ENUM, String> remarksMap = this.directoryModel.getRemarksMap();
			remarksMap.put(LANG_ENUM.ENGLISH, controlModel.getObjectModel(LANG_ENUM.ENGLISH));
			remarksMap.put(LANG_ENUM.GERMAN, controlModel.getObjectModel(LANG_ENUM.GERMAN));
			updateRemarkWidget();
		}
	}

	/**
	 * Update remark widget.
	 */
	public void updateRemarkWidget() {
		if (this.directoryModel == null) {
			return;
		}
		final int operationMode = this.directoryModel.getOperationMode();
		final LANG_ENUM currentLocaleEnum = XMAdminUtil.getInstance().getCurrentLocaleEnum();
		final String remarkForCurLocale = this.directoryModel.getRemarks(currentLocaleEnum) == null
				? CommonConstants.EMPTY_STR : this.directoryModel.getRemarks(currentLocaleEnum);
		if (operationMode != CommonConstants.OPERATIONMODE.VIEW) {
			this.txtRemarks.setText(remarkForCurLocale);
			return;
		}
		if (operationMode == CommonConstants.OPERATIONMODE.VIEW) {
			if (!XMSystemUtil.isEmpty(remarkForCurLocale)) {
				this.txtRemarks.setText(remarkForCurLocale);
				return;
			}
			final String remarkEN = this.directoryModel.getRemarks(LANG_ENUM.ENGLISH);
			final String remarkDE = this.directoryModel.getRemarks(LANG_ENUM.GERMAN);
			if (!XMSystemUtil.isEmpty(remarkEN)) {
				this.txtRemarks.setText(remarkEN);
				return;
			}

			if (!XMSystemUtil.isEmpty(remarkDE)) {
				this.txtRemarks.setText(remarkDE);
				return;
			}
		}
	}

	/**
	 * Inits the widgets.
	 */
	public void updateDescWidget() {
		if (this.directoryModel == null) {
			return;
		}
		final int operationMode = this.directoryModel.getOperationMode();
		final LANG_ENUM currentLocaleEnum = XMAdminUtil.getInstance().getCurrentLocaleEnum();
		final String descForCurLocale = this.directoryModel.getDescription(currentLocaleEnum) == null
				? CommonConstants.EMPTY_STR : this.directoryModel.getDescription(currentLocaleEnum);
		if (operationMode != CommonConstants.OPERATIONMODE.VIEW) {
			this.txtDesc.setText(descForCurLocale);
			return;
		}
		if (operationMode == CommonConstants.OPERATIONMODE.VIEW) {
			if (!XMSystemUtil.isEmpty(descForCurLocale)) {
				this.txtDesc.setText(descForCurLocale);
				return;
			}
			final String descriptionEN = this.directoryModel.getDescription(LANG_ENUM.ENGLISH);
			final String descriptionDE = this.directoryModel.getDescription(LANG_ENUM.GERMAN);
			if (!XMSystemUtil.isEmpty(descriptionEN)) {
				this.txtDesc.setText(descriptionEN);
				return;
			}

			if (!XMSystemUtil.isEmpty(descriptionDE)) {
				this.txtDesc.setText(descriptionDE);
				return;
			}
		}
	}

	/**
	 * Method for Binding model to widget.
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void bindValues() {
		try {
			// Name field binding
			widgetValue = WidgetProperties.text(SWT.Modify).observe(this.txtName);
			modelValue = BeanProperties.value(Directory.class, Directory.PROPERTY_DIRNAME).observe(this.directoryModel);
			widgetValue.addValueChangeListener(new IValueChangeListener() {

				@Override
				public void handleValueChange(final ValueChangeEvent event) {
					updateButtonStatus(event);
				}
			});

			// define the UpdateValueStrategy
			final UpdateValueStrategy update = new UpdateValueStrategy();
			update.setAfterGetValidator(new NameValidation(messages, StatusValidation.DIRECTORY));
			Binding bindValue = dataBindContext.bindValue(widgetValue, modelValue, update, null);
			ControlDecorationSupport.create(bindValue, SWT.TOP | SWT.LEFT);

			// Symbol toolItem setup
			Icon icon;
			String iconPath;
			if ((icon = this.directoryModel.getIcon()) != null
					&& !XMSystemUtil.isEmpty(iconPath = icon.getIconPath())) {
				if (iconPath.contains("null")) { //$NON-NLS-1$
					toolItem.setImage(XMSystemUtil.getInstance().getImage(this.getClass(), "icons/16x16/browse.png"));//$NON-NLS-1$ 
				} else {
					toolItem.setImage(XMSystemUtil.getInstance().getImage(this.getClass(), iconPath, true, true));
					// Symbol toolItem binding
					widgetValue = WidgetProperties.tooltipText().observe(this.toolItem);
					modelValue = BeanProperties
							.value(Directory.class, Directory.PROPERTY_ICON + "." + Icon.PROPERTY_ICONNAME, Icon.class)
							.observe(this.directoryModel);
					bindValue = dataBindContext.bindValue(widgetValue, modelValue);

					// Symbol Field binding
					widgetValue = WidgetProperties.text(SWT.Modify).observe(this.txtSymbol);
					modelValue = BeanProperties
							.value(Directory.class, Directory.PROPERTY_ICON + "." + Icon.PROPERTY_ICONNAME, Icon.class)
							.observe(this.directoryModel);
					final UpdateValueStrategy symbolUpdate = new UpdateValueStrategy();
					symbolUpdate.setAfterGetValidator(new SymbolValidation());
					bindValue = dataBindContext.bindValue(widgetValue, modelValue, symbolUpdate, null);
					ControlDecorationSupport.create(bindValue, SWT.TOP | SWT.LEFT);
				}
			}
		} catch (Exception e) {
			LOGGER.error("Exception occured while databinding", e);
		}
	}

	/**
	 * Sets the operation mode.
	 */
	public void setOperationMode() {
		if (this.directoryModel != null) {
			if (directoryModel.getOperationMode() == CommonConstants.OPERATIONMODE.VIEW) {
				this.txtName.setEditable(false);
				this.txtDesc.setEditable(false);
				this.txtRemarks.setEditable(false);
				this.toolItem.setData("editable", false);
				setShowButtonBar(false);
			} else if (directoryModel.getOperationMode() == CommonConstants.OPERATIONMODE.CREATE) {
				this.txtName.setEditable(true);
				this.saveBtn.setEnabled(false);
				this.txtDesc.setEditable(true);
				this.txtRemarks.setEditable(true);
				this.toolItem.setData("editable", true);
				setShowButtonBar(true);
				this.dirty.setDirty(true);
			} else if (directoryModel.getOperationMode() == CommonConstants.OPERATIONMODE.CHANGE) {
				this.txtName.setEditable(true);
				this.toolItem.setData("editable", true);
				this.txtDesc.setEditable(true);
				this.txtRemarks.setEditable(true);
				setShowButtonBar(true);
				this.dirty.setDirty(true);
			} else {
				this.txtName.setEditable(false);
				this.txtDesc.setEditable(false);
				this.txtRemarks.setEditable(false);
				this.toolItem.setData("editable", false);
				setShowButtonBar(false);
			}
		}
	}

	/**
	 * Method to update the button status.
	 *
	 * @param event
	 *            the event
	 */
	@SuppressWarnings("rawtypes")
	private void updateButtonStatus(final ValueChangeEvent event) {
		final String name = (String) event.getObservableValue().getValue();
		if (this.saveBtn != null) {
			if (XMSystemUtil.isEmpty(name) || name.trim().length() == 0
					|| (!name.matches(CommonConstants.RegularExpressions.ALLOWED_APP_NAME_REGEX))) {
				this.saveBtn.setEnabled(false);
			} else {
				this.saveBtn.setEnabled(true);
			}
		}
	}

	/**
	 * Sets the dirty object.
	 *
	 * @param dirty
	 *            the new dirty object
	 */
	public void setDirtyObject(final MDirtyable dirty) {
		this.dirty = dirty;
	}

	/**
	 * Gets the old model.
	 *
	 * @return the oldModel
	 */
	public Directory getOldModel() {
		return oldModel;
	}

	/**
	 * Sets the old model.
	 *
	 * @param oldModel
	 *            the oldModel to set
	 */
	public void setOldModel(final Directory oldModel) {
		this.oldModel = oldModel;
	}

	/**
	 * Gets the directory model.
	 *
	 * @return the directory
	 */
	public Directory getDirectoryModel() {
		return directoryModel;
	}

	/**
	 * Sets the directory model.
	 *
	 * @param directory
	 *            the directory to set
	 */
	public void setDirectoryModel(final Directory directory) {
		this.directoryModel = directory;
	}

	/**
	 * Sets the directory.
	 */
	public void setDirectory() {
		try {
			final AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
			final Object selectionObj = adminTree.getSelection();
			if (selectionObj instanceof IStructuredSelection) {
				final Object firstElement = ((IStructuredSelection) selectionObj).getFirstElement();
				if (firstElement instanceof Directory) {
					this.setOldModel((Directory) firstElement);
					final Directory rightHandObject = this.getOldModel().deepCopyDirectory(false, null);
					this.setDirectoryModel(rightHandObject);
					this.registerMessages(this.registry);
					this.bindValues();
					this.setOperationMode();
				}
			}
		} catch (Exception e) {
			LOGGER.warn("Unable to set directory model selection ! " + e);

		}

	}

	/**
	 * Sets the model.
	 *
	 * @param group
	 *            the new model
	 */
	public void setModel(final Directory group) {
		try {
			setOldModel(null);
			setDirectoryModel(group);
			registerMessages(this.registry);
			bindValues();
			setOperationMode();
		} catch (Exception e) {
			LOGGER.warn("Unable to set directory model ! " + e);
		}
	}

}
