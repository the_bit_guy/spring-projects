
package com.magna.xmsystem.xmadmin.ui.handlers.projectapplicationchild;

import javax.inject.Inject;

import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.core.services.nls.Translation;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.widgets.Display;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.ResourceAccessException;

import com.magna.xmsystem.dependencies.customExceptions.UnauthorizedAccessException;
import com.magna.xmsystem.ui.controls.widgets.CustomMessageDialog;
import com.magna.xmsystem.xmadmin.message.Message;
import com.magna.xmsystem.xmadmin.restclient.application.ProjectAppController;
import com.magna.xmsystem.xmadmin.ui.parts.IEditablePart;
import com.magna.xmsystem.xmadmin.ui.parts.InformationPart;
import com.magna.xmsystem.xmadmin.ui.parts.objexp.ObjectExpPage;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.AdminTreeviewer;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.projectapplicationmodel.ProjectApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.projectapplicationmodel.ProjectApplicationChild;
import com.magna.xmsystem.xmadmin.util.CommonConstants;
import com.magna.xmsystem.xmadmin.util.MessageType;
import com.magna.xmsystem.xmadmin.util.XMAdminUtil;

// TODO: Auto-generated Javadoc
/**
 * The Class DeleteProjectAppChild.
 * 
 * @author archita.patel
 */
public class DeleteProjectAppChild {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(DeleteProjectAppChild.class);

	/** Member variable 'messages' for {@link Message}. */
	@Inject
	@Translation
	private Message messages;

	/** Member variable 'application' for {@link MApplication}. */
	@Inject
	private MApplication application;

	/** Member variable 'model service' for {@link EModelService}. */
	@Inject
	private EModelService modelService;

	/**
	 * Method for Execute.
	 */
	@Execute
	public void execute() {
		try {
			if (XMAdminUtil.getInstance().isAcessAllowed("PROJECT_APPLICATION-DELETE")) {
				final AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
				IStructuredSelection selection;
				Object selectionObj;

				MPart mPart = (MPart) this.modelService.find(CommonConstants.PART_ID.INFORMATION_PART_ID, application);
				InformationPart rightSidePart2 = (InformationPart) mPart.getObject();
				ObjectExpPage objectExpPage = rightSidePart2.getObjectExpPartUI();
				if (objectExpPage != null
						&& (selection = objectExpPage.getObjExpTableViewer().getStructuredSelection()) != null
						&& (selectionObj = selection.getFirstElement()) != null
						&& selectionObj instanceof ProjectApplicationChild) {
					deleteProjectApplication(selectionObj);
					objectExpPage.refreshExplorer();
				} else if (adminTree != null && (selection = adminTree.getStructuredSelection()) != null
						&& (selectionObj = selection.getFirstElement()) != null
						&& selectionObj instanceof ProjectApplicationChild) {
					deleteProjectApplication(selectionObj);
				}
				XMAdminUtil.getInstance().clearClipBoardContents();
			} else {
				CustomMessageDialog.openError(Display.getCurrent().getActiveShell(), messages.objectPermissionDialogTitle,
						messages.objectPermissionDialogMsg);
			}
		} catch (ClassCastException e) {
			LOGGER.error("Exception occured while deleting projectApplication " + e);
		} catch (UnauthorizedAccessException e) {
			LOGGER.error("Current user is Unauthorized " + e);
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				CustomMessageDialog.openError(Display.getCurrent().getActiveShell(), messages.errorDialogTitile,
						messages.serverNotReachable);
			}
			LOGGER.error("Exception occured in calling access allowed API " + e);
		}
	}

	/**
	 * Checks if is acess allowed.
	 *
	 * @return true, if is acess allowed
	 */
	/*public boolean isAcessAllowed() {
		boolean returnVal = true;
		try {
			ValidationController controller = new ValidationController();
			ValidationRequest request = new ValidationRequest();
			request.setUserName(RestClientUtil.getInstance().getUserName());
			request.setPermissionType(PermissionType.OBJECT_PERMISSION.name());
			request.setPermissionName("PROJECT_APPLICATION-DELETE"); //$NON-NLS-1$
			returnVal = controller.getAccessAllowed(request);
		} catch (UnauthorizedAccessException e) {
			LOGGER.error("Current user is Unauthorized " + e);
			returnVal = false;
		} catch (Exception e) {
			LOGGER.error("Exception occured in calling access allowed API " + e);
			returnVal = false;
		}
		return returnVal;
	}*/

	/**
	 * Delete project application.
	 *
	 * @param selectionObj
	 *            the selection obj
	 */
	private void deleteProjectApplication(Object selectionObj) {
		String projectAppName = "";
		boolean isDeleted = false;
		final AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
		projectAppName = ((ProjectApplicationChild) selectionObj).getName();

		String confirmDialogTitle = messages.deleteConfirmDialogTitle;
		String confirmDialogMsg = messages.deleteProjectAppConfirmDialogMsgPart1 + " \'" + projectAppName + "\' "
				+ messages.deleteProjectAppConfirmDialogMsgPart2 + " ?";
		boolean isConfirmed = CustomMessageDialog.openConfirm(adminTree.getControl().getShell(), confirmDialogTitle,
				confirmDialogMsg);
		if (isConfirmed) {
			ProjectApplicationChild projectApplicationChild = (ProjectApplicationChild) selectionObj;
			IAdminTreeChild projectAppParent = projectApplicationChild.getParent();
			final String projectApplicationId = projectApplicationChild.getProjectApplicationId();
			ProjectAppController projectAppController = new ProjectAppController();

			if (selectionObj instanceof ProjectApplicationChild) {
				isDeleted = projectAppController.deleteProjectApplication(projectApplicationId);
				if (isDeleted) {
					((ProjectApplication) projectAppParent).remove(projectApplicationId);
					adminTree.setSelection(new StructuredSelection(projectAppParent), true);
					adminTree.refresh(projectAppParent);
					XMAdminUtil.getInstance().updateLogFile(messages.projectApplicationObject + " "
							+ "'" + projectAppName + "'" + " " + messages.objectDelete,MessageType.SUCCESS);
				}
			}
		}
	}

	/**
	 * Method for Can execute.
	 *
	 * @return true, if successful
	 */
	@CanExecute
	public boolean canExecute() {
		// XMAdminUtil instance = XMAdminUtil.getInstance();
		MPart rightHandMPartOfOldPerspective = (MPart) modelService.find(CommonConstants.PART_ID.INFORMATION_PART_ID,
				application);
		if (rightHandMPartOfOldPerspective.getObject() instanceof IEditablePart
				&& rightHandMPartOfOldPerspective.isDirty()) {
			return false;
		}
		return true;
	}

}