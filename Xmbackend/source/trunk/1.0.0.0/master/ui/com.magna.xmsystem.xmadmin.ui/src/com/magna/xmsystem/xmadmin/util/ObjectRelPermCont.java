package com.magna.xmsystem.xmadmin.util;

import com.magna.xmbackend.entities.PermissionTbl;

public class ObjectRelPermCont {
	private PermissionTbl assignPermission;
	private PermissionTbl removePermission;
	private PermissionTbl activatePermission;
	private PermissionTbl inactiveAssignmentPermission;
	/**
	 * @return the assignPermission
	 */
	public PermissionTbl getAssignPermission() {
		return assignPermission;
	}
	/**
	 * @param assignPermission the assignPermission to set
	 */
	public void setAssignPermission(PermissionTbl assignPermission) {
		this.assignPermission = assignPermission;
	}
	/**
	 * @return the removePermission
	 */
	public PermissionTbl getRemovePermission() {
		return removePermission;
	}
	/**
	 * @param removePermission the removePermission to set
	 */
	public void setRemovePermission(PermissionTbl removePermission) {
		this.removePermission = removePermission;
	}
	/**
	 * @return the activatePermission
	 */
	public PermissionTbl getActivatePermission() {
		return activatePermission;
	}
	/**
	 * @param activatePermission the activatePermission to set
	 */
	public void setActivatePermission(PermissionTbl activatePermission) {
		this.activatePermission = activatePermission;
	}
	/**
	 * @return the inactiveAssignmentPermission
	 */
	public PermissionTbl getInactiveAssignmentPermission() {
		return inactiveAssignmentPermission;
	}
	/**
	 * @param inactiveAssignmentPermission the inactiveAssignmentPermission to set
	 */
	public void setInactiveAssignmentPermission(PermissionTbl inactiveAssignmentPermission) {
		this.inactiveAssignmentPermission = inactiveAssignmentPermission;
	}
}
