package com.magna.xmsystem.xmadmin.ui.parts.unassignedusermessage;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.inject.Inject;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.e4.core.services.nls.Translation;
import org.eclipse.e4.ui.model.application.ui.MDirtyable;
import org.eclipse.jface.fieldassist.ControlDecoration;
import org.eclipse.jface.fieldassist.FieldDecorationRegistry;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyleRange;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.FocusListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.VerifyEvent;
import org.eclipse.swt.events.VerifyListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.ResourceAccessException;

import com.magna.xmbackend.entities.PropertyConfigTbl;
import com.magna.xmbackend.vo.enums.ProjectPopupKey;
import com.magna.xmsystem.ui.controls.widgets.CustomMessageDialog;
import com.magna.xmsystem.xmadmin.message.Message;
import com.magna.xmsystem.xmadmin.message.MessageRegistry;
import com.magna.xmsystem.xmadmin.restclient.propertyConfig.PropertyConfigController;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.AdminTreeviewer;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.noProjectMessage.NoProjectPopMessage;
import com.magna.xmsystem.xmadmin.util.CommonConstants;
import com.magna.xmsystem.xmadmin.util.MessageType;
import com.magna.xmsystem.xmadmin.util.XMAdminUtil;

/**
 * The Class NoProjectPopmessageCompositeAction.
 */
public class NoProjectPopmessageCompositeAction extends NoProjectPopMessageCompositeUI {

	/** Logger instance. */
	private static final Logger LOGGER = LoggerFactory.getLogger(NoProjectPopmessageCompositeAction.class);

	/**
	 * Member variable for data binding context the DataBindingContext object
	 * will manage the databindings.
	 */
	final transient DataBindingContext dataBindContext = new DataBindingContext();

	/** Member variable for {@link MessageRegistry}. */
	@Inject
	private MessageRegistry registry;

	/** Member variable for messages. */
	@Inject
	@Translation
	transient private Message messages;

	/** The Project pop message model. */
	private NoProjectPopMessage NoProjectPopMessageModel;

	/** The old model. */
	private NoProjectPopMessage oldModel;

	/** Member variable for dirty. */
	private MDirtyable dirty;

	/** The singleton key value. */
	private EnumMap<ProjectPopupKey, String> NoProjectPOPKeyValue;

	/**
	 * Instantiates a new no project pop message composite action.
	 *
	 * @param parent
	 *            the parent
	 */
	@Inject
	public NoProjectPopmessageCompositeAction(final Composite parent) {
		super(parent, SWT.NONE);
		initListener();
	}

	/**
	 * Inits the listener.
	 */
	private void initListener() {
		if (this.btnSave != null) {
			this.btnSave.addSelectionListener(new SelectionAdapter() {

				/**
				 * Save button handler
				 */
				@Override
				public void widgetSelected(final SelectionEvent event) {
					saveNoprojectPopMessage();
				}
			});
		}
		if (this.btnCancel != null) {
			this.btnCancel.addSelectionListener(new SelectionAdapter() {

				/**
				 * Save button handler
				 */
				@Override
				public void widgetSelected(final SelectionEvent event) {
					cancelNoProjectPopmessage();
				}
			});
		}

		this.popMsgStyledTxt.addFocusListener(new FocusListener() {

			@Override
			public void focusLost(FocusEvent paramFocusEvent) {
				final List<String> list = new ArrayList<String>();
				final Pattern pattern = Pattern.compile(CommonConstants.RegularExpressions.URL, Pattern.MULTILINE);
				final String strValue = popMsgStyledTxt.getText();
				final Matcher matcher = pattern.matcher(strValue);
				while (matcher.find()) {
					list.add(matcher.group());
				}
				final List<StyleRange> styleRangeList = new ArrayList<>();
				final List<Integer> rangeList = new ArrayList<>();
				int fromIndex = 0;
				for (String link : list) {
					final int indexOf = strValue.indexOf(link, fromIndex);
					rangeList.add(indexOf);
					final int length = indexOf + link.length();
					fromIndex = length;
					rangeList.add(length);

					final StyleRange style = new StyleRange();
					style.underline = true;
					style.underlineStyle = SWT.UNDERLINE_LINK;
					style.start = indexOf;
					style.length = link.length();
					styleRangeList.add(style);
				}
				popMsgStyledTxt.setStyleRanges(styleRangeList.toArray(new StyleRange[styleRangeList.size()]));
			}

			@Override
			public void focusGained(FocusEvent paramFocusEvent) {
			}
		});

		this.popMsgStyledTxt.addVerifyListener(new VerifyListener() {
			ControlDecoration txtNameDecroator = new ControlDecoration(popMsgStyledTxt, SWT.TOP);

			@Override
			public void verifyText(VerifyEvent event) {
				final StyledText source = (StyledText) event.getSource();
				final String existingText = source.getText();
				final String updatedText = existingText.substring(0, event.start) + event.text
						+ existingText.substring(event.end);
				final Image nameDecoratorImage = FieldDecorationRegistry.getDefault()
						.getFieldDecoration(FieldDecorationRegistry.DEC_ERROR).getImage();
				txtNameDecroator.setImage(nameDecoratorImage);
				if (updatedText.trim().length() <= 0) {
					txtNameDecroator.setDescriptionText(messages.emptyNoProjectMessageValidateErr);
					txtNameDecroator.show();
					btnSave.setEnabled(false);
				} else {
					btnSave.setEnabled(true);
					txtNameDecroator.hide();
				}
			}

		});

	}

	/**
	 * Cancel no project popmessage.
	 */
	public void cancelNoProjectPopmessage() {
		if (this.NoProjectPopMessageModel == null) {
			dirty.setDirty(false);
			return;
		}
		setNoProjectPopMessageModel(null);
		setOldModel(null);
		dirty.setDirty(false);
		final AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
		final IStructuredSelection selection = (IStructuredSelection) adminTree.getSelection();
		final Object firstElement = selection.getFirstElement();
		adminTree.setSelection(new StructuredSelection(firstElement), true);
	}

	/**
	 * Save noproject pop message.
	 */
	public void saveNoprojectPopMessage() {
		try {
			this.NoProjectPopMessageModel.setStyledtext(this.popMsgStyledTxt.getText());
			PropertyConfigController propertyConfigController = new PropertyConfigController();
			boolean isUpdate = propertyConfigController.UpdateNoProjectPopMessage(mapVOObjectWithModel());
			if (isUpdate) {
				setOldModel(NoProjectPopMessageModel.deepCopyNoProjectPopMessageConfigModel(true, getOldModel()));
				NoProjectPopMessageModel.setOperationMode(CommonConstants.OPERATIONMODE.VIEW);
				setOperationMode();
				this.dirty.setDirty(false);
				XMAdminUtil.getInstance().getAdminTree().refresh();
				XMAdminUtil.getInstance().getAdminTree().setSelection(new StructuredSelection(getOldModel()), true);
				XMAdminUtil.getInstance().updateLogFile(messages.noPopMessageNode + " " + messages.objectUpdate,
						MessageType.SUCCESS);
			}
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				CustomMessageDialog.openError(Display.getCurrent().getActiveShell(), messages.errorDialogTitile,
						messages.serverNotReachable);
			}
			LOGGER.error("Exception occured while saving Noproject PopMessage Time  configuration", e);
			e.printStackTrace();
		}

	}

	/**
	 * Map VO object with model.
	 *
	 * @return the com.magna.xmbackend.vo.prop config. project popup config
	 *         request
	 */
	private com.magna.xmbackend.vo.propConfig.ProjectPopupConfigRequest mapVOObjectWithModel() {
		com.magna.xmbackend.vo.propConfig.ProjectPopupConfigRequest projectPopConfigurRequest = new com.magna.xmbackend.vo.propConfig.ProjectPopupConfigRequest();
		NoProjectPOPKeyValue = new EnumMap<ProjectPopupKey, String>(ProjectPopupKey.class);
		ProjectPopupKey NoProjectpopKey = ProjectPopupKey.PROJECT_URL;
		NoProjectPOPKeyValue.put(NoProjectpopKey, NoProjectPopMessageModel.getStyledtext());
		projectPopConfigurRequest.setProjectPopupKeyValue(NoProjectPOPKeyValue);
		return projectPopConfigurRequest;

	}

	/**
	 * Sets the no project pop message.
	 */
	public void setNoProjectPopMessage() {
		final AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
		final Object selectionObj = adminTree.getSelection();
		if (selectionObj instanceof IStructuredSelection) {
			Object firstElement = ((IStructuredSelection) selectionObj).getFirstElement();
			if (firstElement instanceof NoProjectPopMessage) {
				NoProjectPopMessageModel = (NoProjectPopMessage) firstElement;
				PropertyConfigController propertyConfigController = new PropertyConfigController();
				List<PropertyConfigTbl> propVOList = propertyConfigController.getNoProjectPopMessage();
				for (PropertyConfigTbl propertyTbl : propVOList) {
					String property = propertyTbl.getProperty();
					String propertyValue = propertyTbl.getValue();
					if (ProjectPopupKey.PROJECT_URL.toString().equals(property)) {
						this.NoProjectPopMessageModel.setStyledtext(propertyValue);
					}
				}
				setOldModel(NoProjectPopMessageModel);
				setNoProjectPopMessageModel(this.getOldModel().deepCopyNoProjectPopMessageConfigModel(false, null));
				this.popMsgStyledTxt.setText(this.NoProjectPopMessageModel.getStyledtext());

				final List<String> list = new ArrayList<String>();
				final Pattern pattern = Pattern.compile(CommonConstants.RegularExpressions.URL);
				final String strValue = popMsgStyledTxt.getText();
				final Matcher matcher = pattern.matcher(strValue);
				while (matcher.find()) {
					list.add(matcher.group());
				}

				final List<StyleRange> styleRangeList = new ArrayList<>();
				final List<Integer> rangeList = new ArrayList<>();
				int fromIndex = 0;
				for (String link : list) {
					final int indexOf = strValue.indexOf(link, fromIndex);
					rangeList.add(indexOf);
					final int length = indexOf + link.length();
					fromIndex = length;
					rangeList.add(length);

					final StyleRange style = new StyleRange();
					style.underline = true;
					style.underlineStyle = SWT.UNDERLINE_LINK;
					style.start = indexOf;
					style.length = link.length();
					styleRangeList.add(style);
				}
				popMsgStyledTxt.setStyleRanges(styleRangeList.toArray(new StyleRange[styleRangeList.size()]));
				registerMessages(this.registry);
				setOperationMode();
			}
		}
	}

	/**
	 * Register messages.
	 *
	 * @param registry
	 *            the registry
	 */
	public void registerMessages(final MessageRegistry registry) {

		registry.register((text) -> {
			if (grpPopMsgConfig != null && !grpPopMsgConfig.isDisposed()) {
				grpPopMsgConfig.setText(text);
			}
		}, (message) -> {
			return message.noProjectPopupGroupLabel;
		});
		registry.register((text) -> {
			if (lblPopMsg != null && !lblPopMsg.isDisposed()) {
				lblPopMsg.setText(text);
			}
		}, (message) -> {
			if (lblPopMsg != null && !lblPopMsg.isDisposed()) {
				return getUpdatedWidgetText(message.noProjectPopMsglabel, lblPopMsg);
			}
			return CommonConstants.EMPTY_STR;
		});
		registry.register((text) -> {
			if (btnSave != null && !btnSave.isDisposed()) {
				btnSave.setText(text);
			}
		}, (message) -> {
			if (btnSave != null && !btnSave.isDisposed()) {
				return getUpdatedWidgetText(message.saveButtonText, btnSave);
			}
			return CommonConstants.EMPTY_STR;
		});
		registry.register((text) -> {
			if (btnCancel != null && !btnCancel.isDisposed()) {
				btnCancel.setText(text);
			}
		}, (message) -> {
			if (btnCancel != null && !btnCancel.isDisposed()) {
				return getUpdatedWidgetText(message.cancelButtonText, btnCancel);
			}
			return CommonConstants.EMPTY_STR;
		});
	}

	/**
	 * Sets the operation mode.
	 */
	public void setOperationMode() {
		if (this.NoProjectPopMessageModel != null) {
			if (NoProjectPopMessageModel.getOperationMode() == CommonConstants.OPERATIONMODE.VIEW) {
				this.popMsgStyledTxt.setEditable(false);
				setShowButtonBar(false);
			} else if (NoProjectPopMessageModel.getOperationMode() == CommonConstants.OPERATIONMODE.CHANGE) {
				this.popMsgStyledTxt.setEditable(true);
				setShowButtonBar(true);
				this.dirty.setDirty(true);
			} else {
				this.popMsgStyledTxt.setEditable(false);
				setShowButtonBar(false);
			}
		}
	}

	/**
	 * Sets the show button bar.
	 *
	 * @param showButtonBar
	 *            the new show button bar
	 */
	protected void setShowButtonBar(final boolean showButtonBar) {
		if (this.btnSave != null && !this.btnSave.isDisposed() && this.btnCancel != null
				&& !this.btnCancel.isDisposed()) {
			final GridData layoutData = (GridData) this.btnSave.getParent().getLayoutData();
			layoutData.exclude = !showButtonBar;
			this.btnSave.setVisible(showButtonBar);
			this.btnCancel.setVisible(showButtonBar);
			this.btnSave.getParent().requestLayout();
			this.btnSave.getParent().redraw();
			this.btnSave.getParent().getParent().update();
		}
	}

	/**
	 * Gets the updated widget text.
	 *
	 * @param message
	 *            the message
	 * @param control
	 *            the control
	 * @return the updated widget text
	 */
	private String getUpdatedWidgetText(final String message, final Control control) {
		control.requestLayout();
		control.getParent().redraw();
		control.getParent().getParent().update();
		control.getParent().getParent().getParent().update();
		return message;
	}

	/**
	 * Sets the dirty.
	 *
	 * @param dirty
	 *            the new dirty
	 */
	public void setDirty(MDirtyable dirty) {
		this.dirty = dirty;
	}

	/**
	 * Gets the no project pop message model.
	 *
	 * @return the no project pop message model
	 */
	public NoProjectPopMessage getNoProjectPopMessageModel() {
		return NoProjectPopMessageModel;
	}

	/**
	 * Sets the no project pop message model.
	 *
	 * @param noProjectPopMessageModel
	 *            the new no project pop message model
	 */
	public void setNoProjectPopMessageModel(NoProjectPopMessage noProjectPopMessageModel) {
		NoProjectPopMessageModel = noProjectPopMessageModel;
	}

	/**
	 * Gets the old model.
	 *
	 * @return the old model
	 */
	public NoProjectPopMessage getOldModel() {
		return oldModel;
	}

	/**
	 * Sets the old model.
	 *
	 * @param oldModel
	 *            the new old model
	 */
	public void setOldModel(NoProjectPopMessage oldModel) {
		this.oldModel = oldModel;
	}

}
