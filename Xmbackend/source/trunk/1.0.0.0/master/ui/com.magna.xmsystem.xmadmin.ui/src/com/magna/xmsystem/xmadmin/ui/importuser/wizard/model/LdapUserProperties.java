package com.magna.xmsystem.xmadmin.ui.importuser.wizard.model;

import java.util.ArrayList;
import java.util.List;

// TODO: Auto-generated Javadoc
/**
 * The Class LdapUserProperties.
 * 
 * @author archita.patel
 */
public class LdapUserProperties {

	/** The ldap user properties. */
	private List<LdapUserProperty> ldapUserProperties;

	public LdapUserProperties() {
		ldapUserProperties = new ArrayList<>();
	}

	/**
	 * Gets the ldap user properties.
	 *
	 * @return the ldap user properties
	 */
	public List<LdapUserProperty> getLdapUserProperties() {
		return ldapUserProperties;
	}

	/**
	 * Sets the ldap user properties.
	 *
	 * @param ldapUserProperties
	 *            the new ldap user properties
	 */
	public void setLdapUserProperties(final List<LdapUserProperty> ldapUserProperties) {
		this.ldapUserProperties = ldapUserProperties;
	}

}
