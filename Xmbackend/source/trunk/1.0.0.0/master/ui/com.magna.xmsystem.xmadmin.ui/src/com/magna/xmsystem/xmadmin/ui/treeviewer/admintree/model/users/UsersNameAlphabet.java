package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;

/**
 * Class for Users name alphabet.
 *
 * @author Chiranjeevi.Akula
 */
public class UsersNameAlphabet implements IAdminTreeChild {
	
	/** Member variable 'parent' for {@link IAdminTreeChild}. */
	private IAdminTreeChild parent;
	
	/** Member variable 'users children' for {@link Map<String,IAdminTreeChild>}. */
	private Map<String, IAdminTreeChild> usersChildren;

	/** Member variable 'first char' for {@link String}. */
	private String firstChar;

	/**
	 * Constructor for UsersNameAlphabet Class.
	 *
	 * @param firstChar {@link String}
	 */
	public UsersNameAlphabet(String firstChar) {
		this.firstChar = firstChar;
		this.usersChildren = new LinkedHashMap<>();
	}

	/**
	 * Method for Adds the.
	 *
	 * @param userId {@link String}
	 * @param child {@link IAdminTreeChild}
	 * @return the i admin tree child {@link IAdminTreeChild}
	 */
	public IAdminTreeChild add(final String userId, final IAdminTreeChild child) {
		child.setParent(this);
		IAdminTreeChild returnVal = this.usersChildren.put(userId, child);
		sort();
		return returnVal;
	}

	/**
	 * Method for Removes the.
	 *
	 * @param userId {@link String}
	 * @return the i admin tree child {@link IAdminTreeChild}
	 */
	public IAdminTreeChild remove(final String userId) {
		return this.usersChildren.remove(userId);
	}

	/**
	 * Gets the users collection.
	 *
	 * @return the users collection
	 */
	public Collection<IAdminTreeChild> getUsersCollection() {
		return this.usersChildren.values();
	}

	/**
	 * Gets the users children.
	 *
	 * @return the users children
	 */
	public Map<String, IAdminTreeChild> getUsersChildren() {
		return usersChildren;
	}
	
	/* (non-Javadoc)
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#getParent()
	 */
	@Override
	public IAdminTreeChild getParent() {
		return this.parent;
	}

	/* (non-Javadoc)
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#setParent(com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild)
	 */
	@Override
	public void setParent(final IAdminTreeChild parent) {
		this.parent = parent;
	}

	/**
	 * Gets the first char.
	 *
	 * @return the first char
	 */
	public String getFirstChar() {
		return firstChar;
	}

	/**
	 * Sets the first char.
	 *
	 * @param firstChar the new first char
	 */
	public void setFirstChar(String firstChar) {
		this.firstChar = firstChar;
	}
	
	/**
	 * Sort.
	 */
	public void sort() {
		final LinkedHashMap<String, IAdminTreeChild> collect = this.usersChildren.entrySet().stream()
				.sorted((e1, e2) -> ((User) e1.getValue()).getName().compareTo(((User) e2.getValue()).getName()))
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
		this.usersChildren = collect;
	}
}
