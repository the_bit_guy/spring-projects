package com.magna.xmsystem.xmadmin.ui.parts.application.baseapplication;

import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.events.ControlAdapter;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Link;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magna.xmsystem.dependencies.utils.XMSystemUtil;
import com.magna.xmsystem.xmadmin.ui.dialogs.lang.XMAdminLangTextAreaDialog;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.baseapplicationmodel.BaseApplication;
import com.magna.xmsystem.xmadmin.util.XMAdminUtil;

/**
 * Class for Base app composite UI.
 *
 * @author Chiranjeevi.Akula
 */
public abstract class BaseAppCompositeUI extends Composite {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(BaseAppCompositeUI.class);

	/** Member variable 'cancel btn' for {@link Button}. */
	protected Button saveBtn, cancelBtn;

	/** Member variable 'lbl active' for {@link Label}. */
	protected Label lblActive;

	/** Member variable 'lbl symbol' for {@link Label}. */
	protected Label lblSymbol;

	/** Member variable 'lbl platforms' for {@link Label}. */
	protected Label lblPlatforms;

	/** Member variable 'lbl program' for {@link Label}. */
	protected Label lblProgram;

	/** Member variable 'active btn' for {@link Button}. */
	protected Button activeBtn;
	
	/** Member variable 'txt symbol' for {@link Text}. */
	protected Text txtSymbol;

	/** Member variable 'grp base app' for {@link Group}. */
	transient protected Group grpBaseApp;

	/** Member variable 'lbl name' for {@link Label}. */
	transient protected Label lblName;

	/** Member variable 'lbl descrition' for {@link Label}. */
	transient protected Label lblDescrition;

	/** Member variable 'txt program' for {@link Text}. */
	transient protected Text txtProgram;

	/** Member variable 'program tool item' for {@link ToolItem}. */
	transient protected ToolItem programToolItem;

	/** Member variable 'parent shell' for {@link Shell}. */
	protected Shell parentShell;

	/** Member variable 'icon tool item' for {@link ToolItem}. */
	protected ToolItem iconToolItem;

	/** Member variable 'btn platforms' for {@link Button[]}. */
	protected Button[] btnPlatforms;

	/** Member variable 'txt desc' for {@link Text}. */
	protected Text txtDesc;

	/** Member variable 'txt name' for {@link Text}. */
	protected Text txtName;

	/** Member variable 'desc link' for {@link Link}. */
	protected Link descLink;

	/** Member variable 'remarks label' for {@link Label}. */
	protected Label remarksLabel;

	/** Member variable 'txt remarks' for {@link Text}. */
	protected Text txtRemarks;

	/** Member variable 'remarks link' for {@link Link}. */
	protected Link remarksLink;

	/** Member variable 'lbl remarks count' for {@link Label}. */
	protected Label lblRemarksCount;

	/**
	 * Constructor for BaseAppCompositeUI Class.
	 *
	 * @param parent {@link Composite}
	 * @param style {@link int}
	 */
	public BaseAppCompositeUI(final Composite parent, final int style) {
		super(parent, style);
		this.initGUI();
		setShowButtonBar(false);
	}

	/**
	 * Method for Inits the GUI.
	 */
	private void initGUI() {
		try {
			GridLayoutFactory.fillDefaults().applyTo(this);
			this.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

			this.grpBaseApp = new Group(this, SWT.NONE);
			this.grpBaseApp.setBackgroundMode(SWT.INHERIT_FORCE);
			GridLayoutFactory.fillDefaults().numColumns(1).applyTo(this.grpBaseApp);
			GridDataFactory.fillDefaults().grab(true, true).align(SWT.FILL, SWT.FILL).applyTo(this.grpBaseApp);
			final ScrolledComposite scrolledComposite = XMAdminUtil.getInstance().createScrolledComposite(this.grpBaseApp);
			scrolledComposite.setBackgroundMode(SWT.INHERIT_FORCE);

			final Composite widgetContainer = new Composite(scrolledComposite, SWT.NONE);
			final GridLayout widgetContLayout = new GridLayout(3, false);

			widgetContainer.setLayout(widgetContLayout);
			widgetContainer.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

			this.lblName = new Label(widgetContainer, SWT.NONE);
			this.txtName = new Text(widgetContainer, SWT.BORDER);
			this.txtName.setTextLimit(BaseApplication.NAME_LIMIT);
			GridDataFactory.fillDefaults().grab(true, false).align(SWT.FILL, SWT.CENTER).span(2, 1).applyTo(this.txtName);

			this.lblActive = new Label(widgetContainer, SWT.NONE);
			this.activeBtn = new Button(widgetContainer, SWT.CHECK);

			GridDataFactory.fillDefaults().grab(false, false).align(SWT.LEFT, SWT.CENTER).applyTo(this.lblActive);
			GridDataFactory.fillDefaults().grab(false, false).align(SWT.LEFT, SWT.CENTER).span(2, 1).applyTo(this.activeBtn);

			this.lblDescrition = new Label(widgetContainer, SWT.NONE);
			this.txtDesc = new Text(widgetContainer, SWT.BORDER);
			this.txtDesc.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
			this.txtDesc.setTextLimit(BaseApplication.DESCRIPTION_LIMIT);
			this.descLink = new Link(widgetContainer, SWT.NONE);

			this.lblSymbol = new Label(widgetContainer, SWT.NONE);
			this.txtSymbol = new Text(widgetContainer, SWT.BORDER);
			this.txtSymbol.setEditable(false);
			GridDataFactory.fillDefaults().grab(true, false).span(1, 1).align(SWT.FILL, SWT.CENTER).applyTo(this.txtSymbol);
			final ToolBar toolbar = new ToolBar(widgetContainer, SWT.NONE);
			this.iconToolItem = new ToolItem(toolbar, SWT.FLAT);
			this.iconToolItem
					.setImage(XMSystemUtil.getInstance().getImage(this.getClass(), "icons/16x16/browse.png"));
			toolbar.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false));

			this.remarksLabel = new Label(widgetContainer, SWT.NONE);
			this.remarksLink = new Link(widgetContainer, SWT.NONE);
			this.remarksLink.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, true, false, 1, 1));

			this.lblRemarksCount = new Label(widgetContainer, SWT.BORDER);
			final GridData gridDataRemarkCount = new GridData(SWT.LEFT, SWT.CENTER, false, false);
			gridDataRemarkCount.widthHint = 70;
			this.lblRemarksCount.setLayoutData(gridDataRemarkCount);

			this.txtRemarks = new Text(widgetContainer, SWT.BORDER | SWT.MULTI | SWT.WRAP | SWT.H_SCROLL |SWT.V_SCROLL);
			final GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true);
			gridData.minimumHeight = 100;
			gridData.horizontalSpan = 3;
			this.txtRemarks.setLayoutData(gridData);
			this.txtRemarks.setTextLimit(BaseApplication.REMARK_LIMIT);
			this.lblRemarksCount.setText(XMAdminLangTextAreaDialog.LIMIT_COUNT_PADDING
					+ this.txtRemarks.getText().length() + XMAdminLangTextAreaDialog.SLASH
					+ BaseApplication.REMARK_LIMIT + XMAdminLangTextAreaDialog.LIMIT_COUNT_PADDING);
			
			Composite platformsComposite = new Composite(widgetContainer, SWT.NONE);
			final GridLayout platformsCompositeLayout = new GridLayout(2, false);
			platformsCompositeLayout.marginRight = 0;
			platformsCompositeLayout.marginLeft = 0;
			platformsCompositeLayout.marginTop = 0;
			platformsCompositeLayout.marginBottom = 0;
			platformsCompositeLayout.marginWidth = 0;
			platformsCompositeLayout.marginHeight = 0;
			platformsComposite.setLayout(platformsCompositeLayout);
			GridDataFactory.fillDefaults().grab(true, false).span(3, 1).align(SWT.FILL, SWT.CENTER)
			.applyTo(platformsComposite);
			
			this.lblPlatforms = new Label(platformsComposite, SWT.NONE);
			GridDataFactory.fillDefaults().grab(false, false).align(SWT.LEFT, SWT.CENTER).span(1, 1)
					.applyTo(this.lblPlatforms);

			Composite checkBoxComposite = new Composite(platformsComposite, SWT.NONE);
			final GridLayout widgetContainerLayout = new GridLayout(5, false);
			widgetContainerLayout.marginRight = 0;
			widgetContainerLayout.marginLeft = 0;
			widgetContainerLayout.marginTop = 0;
			widgetContainerLayout.marginBottom = 0;
			widgetContainerLayout.marginWidth = 0;
			widgetContainerLayout.marginHeight = 2;
			checkBoxComposite.setLayout(widgetContainerLayout);
			GridDataFactory.fillDefaults().grab(true, false).span(1, 1).indent(2, 0).align(SWT.FILL, SWT.CENTER)
					.applyTo(checkBoxComposite);

			
			this.btnPlatforms = new Button[BaseApplication.SUPPORTED_PLATFORMS.size()];
			for (int index = 0; index < this.btnPlatforms.length; index++) {
				this.btnPlatforms[index] = new Button(checkBoxComposite, SWT.CHECK);
				this.btnPlatforms[index].setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
			}

			this.lblProgram = new Label(widgetContainer, SWT.NONE);
			this.txtProgram = new Text(widgetContainer, SWT.BORDER);
			GridDataFactory.fillDefaults().grab(true, false).span(1, 1).indent(2, 0).align(SWT.FILL, SWT.CENTER)
					.applyTo(this.txtProgram);
			this.programToolItem = new ToolItem(new ToolBar(widgetContainer, SWT.NONE), SWT.FLAT);
			this.programToolItem.setText(" ... ");
			toolbar.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false));

			final Composite buttonBarComp = new Composite(this.grpBaseApp, SWT.NONE);
			final GridLayout btnBarCompLayout = new GridLayout(2, true);
			btnBarCompLayout.marginRight = 0;
			btnBarCompLayout.marginLeft = 0;
			btnBarCompLayout.marginTop = 0;
			btnBarCompLayout.marginBottom = 0;
			btnBarCompLayout.marginWidth = 0;
			buttonBarComp.setLayout(btnBarCompLayout);
			buttonBarComp.setLayoutData(new GridData(SWT.FILL, SWT.DEFAULT, true, false, 3, 1));
			createButtonBar(buttonBarComp);
			
			scrolledComposite.setContent(widgetContainer);
			scrolledComposite.setSize(widgetContainer.getSize());
			scrolledComposite.setExpandVertical(true);
			scrolledComposite.setExpandHorizontal(true);
			scrolledComposite.update();

			scrolledComposite.addControlListener(new ControlAdapter() {
				public void controlResized(final ControlEvent e) {
					Rectangle rectangle = scrolledComposite.getClientArea();
					scrolledComposite.setMinSize(widgetContainer.computeSize(rectangle.width, SWT.DEFAULT));
				}
			});
		} catch (Exception ex) {
			LOGGER.error("Unable to crete UI elements", ex); //$NON-NLS-1$
		}
	}

	/**
	 * Method for Creates the button bar.
	 *
	 * @param buttonBarComp {@link Composite}
	 */
	private void createButtonBar(final Composite buttonBarComp) {
		this.saveBtn = new Button(buttonBarComp, SWT.NONE);
		this.saveBtn.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, true, false));

		this.cancelBtn = new Button(buttonBarComp, SWT.NONE);
		this.cancelBtn.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false));
	}

	/**
	 * Gets the grp site.
	 *
	 * @return the grp site
	 */
	public Group getGrpSite() {
		return grpBaseApp;
	}

	/**
	 * Gets the lbl name.
	 *
	 * @return the lbl name
	 */
	public Label getLblName() {
		return lblName;
	}

	/**
	 * Gets the parent shell.
	 *
	 * @return the parent shell
	 */
	public Shell getParentShell() {
		return parentShell;
	}

	/**
	 * Gets the lbl descrition.
	 *
	 * @return the lbl descrition
	 */
	public Label getLblDescrition() {
		return lblDescrition;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.swt.widgets.Widget#dispose()
	 */
	@Override
	public void dispose() {
		super.dispose();
	}

	/**
	 * Sets the show button bar.
	 *
	 * @param showButtonBar the new show button bar
	 */
	public void setShowButtonBar(final boolean showButtonBar) {
		if (this.saveBtn != null && !this.saveBtn.isDisposed() && this.cancelBtn != null
				&& !this.cancelBtn.isDisposed()) {
			GridData layoutData = (GridData) this.saveBtn.getParent().getLayoutData();
			layoutData.exclude = !showButtonBar;
			this.saveBtn.setVisible(showButtonBar);
			this.cancelBtn.setVisible(showButtonBar);
			this.saveBtn.getParent().setVisible(showButtonBar);
			this.saveBtn.getParent().requestLayout();
			this.saveBtn.getParent().redraw();
			this.saveBtn.getParent().getParent().update();
		}
	}
}
