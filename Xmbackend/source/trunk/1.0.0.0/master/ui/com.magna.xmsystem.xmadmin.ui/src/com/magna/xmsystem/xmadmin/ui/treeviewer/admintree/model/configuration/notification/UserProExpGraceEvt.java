/**
 * 
 */
package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification;

import java.beans.PropertyChangeEvent;

import com.magna.xmbackend.vo.enums.NotificationEventType;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.BeanModel;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;

// TODO: Auto-generated Javadoc
/**
 * The Class UserProExpGraceEvt.
 *
 * @author shashwat.anand
 */
public class UserProExpGraceEvt extends BeanModel implements IAdminTreeChild {
	/** The event type. */
	protected NotificationEventType eventType;

	/** Constant variable for desc text limit. */
	public static final int DESC_LIMIT = 240;

	/** The Constant SUB_LIMIT. */
	public static final int SUB_LIMIT = 100;
	
	/** The Constant MESSAGE_LIMIT. */
	public static final int MESSAGE_LIMIT = 1500;
	
	/** PROPERTY_SITEID constant. */
	public static final String PROPERTY_ID = "Id"; //$NON-NLS-1$

	/** The Constant PROPERTY_DESCRIPTION. */
	public static final String PROPERTY_DESCRIPTION = "description"; //$NON-NLS-1$

	/** The Constant PROPERTY_SUBJECT. */
	public static final String PROPERTY_SUBJECT = "subject"; //$NON-NLS-1$

	/** The Constant PROPERTY_MESSAGE. */
	public static final String PROPERTY_MESSAGE = "message"; //$NON-NLS-1$

	/** The Constant PROPERTY_OPERATION_MODE. */
	public static final String PROPERTY_OPERATION_MODE = "operationMode"; //$NON-NLS-1$

	/** The id. */
	private String id;

	/** The description. */
	private String description;

	/** The subject. */
	private String subject;

	/** The message. */
	private String message;

	/** The operation mode. */
	private int operationMode;
	
	/** The active. */
	private boolean active;

	/**
	 * Instantiates a new user pro exp grace evt.
	 *
	 * @param id
	 *            the id
	 * @param operationMode
	 *            the operation mode
	 */
	public UserProExpGraceEvt(final String id, final int operationMode) {
		this(id, null, null, null, operationMode);
	}

	/**
	 * Instantiates a new user pro exp grace evt.
	 *
	 * @param id
	 *            the id
	 * @param description
	 *            the description
	 * @param subject
	 *            the subject
	 * @param message
	 *            the message
	 * @param operationMode
	 *            the operation mode
	 */
	public UserProExpGraceEvt(final String id, final String description, final String subject, final String message,
			final int operationMode) {
		super();
		this.id = id;
		this.description = description;
		this.subject = subject;
		this.message = message;
		this.operationMode = operationMode;
	}

	/**
	 * Instantiates a new user pro exp grace evt.
	 */
	public UserProExpGraceEvt() {
		this.eventType = NotificationEventType.USER_PROJECT_RELATION_GRACE_EXPIRY;
	}

	/**
	 * Gets the event type.
	 *
	 * @return the eventType
	 */
	public NotificationEventType getEventType() {
		return eventType;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id
	 *            the new id
	 */
	public void setId(String id) {
		if (id == null)
			throw new IllegalArgumentException();
		this.propertyChangeSupport.firePropertyChange(PROPERTY_ID, this.id, this.id = id);
		// this.id = id;
	}

	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description.
	 *
	 * @param description
	 *            the new description
	 */
	public void setDescription(String description) {
		if (description == null)
			throw new IllegalArgumentException();
		this.propertyChangeSupport.firePropertyChange(PROPERTY_DESCRIPTION, this.description,
				this.description = description.trim());
		// this.description = description;
	}

	/**
	 * Gets the subject.
	 *
	 * @return the subject
	 */
	public String getSubject() {
		return subject;
	}

	/**
	 * Sets the subject.
	 *
	 * @param subject
	 *            the new subject
	 */
	public void setSubject(String subject) {
		if (subject == null)
			throw new IllegalArgumentException();
		this.propertyChangeSupport.firePropertyChange(PROPERTY_SUBJECT, this.subject, this.subject = subject.trim());
		// this.subject = subject;
	}

	/**
	 * Gets the message.
	 *
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * Sets the message.
	 *
	 * @param message
	 *            the new message
	 */
	public void setMessage(String message) {
		if (message == null)
			throw new IllegalArgumentException();
		this.propertyChangeSupport.firePropertyChange(PROPERTY_MESSAGE, this.message, this.message = message.trim());
		// this.message = message;
	}

	/**
	 * Checks if is active.
	 *
	 * @return true, if is active
	 */
	public boolean isActive() {
		return active;
	}

	/**
	 * Sets the active.
	 *
	 * @param active the new active
	 */
	public void setActive(boolean active) {
		this.active = active;
	}

	/**
	 * Gets the operation mode.
	 *
	 * @return the operation mode
	 */
	public int getOperationMode() {
		return operationMode;
	}

	/**
	 * Sets the operation mode.
	 *
	 * @param operationMode
	 *            the new operation mode
	 */
	public void setOperationMode(int operationMode) {
		this.propertyChangeSupport.firePropertyChange(PROPERTY_OPERATION_MODE, this.operationMode,
				this.operationMode = operationMode);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.BeanModel#
	 * propertyChange(java.beans.PropertyChangeEvent)
	 */
	@Override
	public void propertyChange(PropertyChangeEvent event) {
		this.propertyChangeSupport.firePropertyChange(event.getPropertyName(), event.getOldValue(),
				event.getNewValue());
	}

	/**
	 * Deep copy user pro exp grace evt action.
	 *
	 * @param b
	 *            the b
	 * @param object
	 *            the object
	 * @return the user pro exp grace evt
	 */
	public UserProExpGraceEvt deepCopyUserProExpGraceEvtAction(boolean b, Object object) {
		// TODO Auto-generated method stub
		return null;
	}

}
