package com.magna.xmsystem.xmadmin.ui.parts.role.editingsupport;

import com.magna.xmbackend.vo.enums.CreationType;
import com.magna.xmbackend.vo.roles.ObjectPermission;
import com.magna.xmsystem.xmadmin.ui.parts.role.ObjectPermissionsTable;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.ObjectType;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.VoPermContainer;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.objectpermissions.ObjectPermissions;
import com.magna.xmsystem.xmadmin.util.XMAdminUtil;

/**
 * The Class ChangeEditingSupport.
 * 
 * @author shashwat.anand
 */
public class ChangeEditingSupport extends CheckBoxEditingSupport {
	/** Member variable 'instance' for {@link XMAdminUtil}. */
	private XMAdminUtil instance;
	
	/** Member variable 'viewer' for {@link ObjectPermissionsTable}. */
	private ObjectPermissionsTable viewer;

	/**
	 * Instantiates a new change editing support.
	 *
	 * @param viewer the viewer
	 */
	public ChangeEditingSupport(final ObjectPermissionsTable viewer) {
		super(viewer);
		this.viewer = viewer;
		this.instance = XMAdminUtil.getInstance();
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.viewers.EditingSupport#getValue(java.lang.Object)
	 */
	@Override
	protected Object getValue(Object element) {
		if (element instanceof ObjectPermissions) {
			return ((ObjectPermissions) element).isChange();
		}
		return "N";
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.viewers.EditingSupport#setValue(java.lang.Object, java.lang.Object)
	 */
	@Override
	protected void setValue(Object element, Object value) {
		if (element instanceof ObjectPermissions) {
			ObjectPermissions elem = (ObjectPermissions) element;
			ObjectType objectType = ObjectType.getObjectType(elem.getPermissionName());
			String permissionId = this.instance.getObjectPermissionMap().get(objectType).getChangePermission().getPermissionId();
			VoPermContainer voObjectPermContainer = new VoPermContainer();
			ObjectPermission objectPermission = new ObjectPermission();
			objectPermission.setPermissionId(permissionId);
			voObjectPermContainer.setObjectPermission(objectPermission);
			if (elem.isChange() && ! (boolean) value) {
				objectPermission.setCreationType(CreationType.DELETE);
				elem.setChangePermission(voObjectPermContainer);
			} else if (!elem.isChange() && (boolean) value) {
				objectPermission.setCreationType(CreationType.ADD);
				elem.setChangePermission(voObjectPermContainer);
				checkAndUpdateForActivatePermission(elem, objectType, CreationType.ADD);
			} else {
				return;
			}
			elem.setChange((boolean) value);
			this.viewer.getCachedObjPermissionMap().put(permissionId, voObjectPermContainer);
			getViewer().refresh(element);
		}
	}
	
	/**
	 * Check and update for activate permission.
	 *
	 * @param element the element
	 * @param objectType the object type
	 * @param creationType the creation type
	 */
	private void checkAndUpdateForActivatePermission(ObjectPermissions element, ObjectType objectType, CreationType creationType) {
		if (objectType == ObjectType.DIRECTORY || objectType == ObjectType.GROUP
				|| objectType == ObjectType.ROLE || objectType == ObjectType.NOTIFICATION || objectType == ObjectType.PROPERTY_CONFIG
				|| objectType == ObjectType.LIVE_MESSAGE) {
			return;
		}
		String actPermId = this.instance.getObjectPermissionMap().get(objectType).getActivatePermission().getPermissionId();
		VoPermContainer voObjectPermContainerAct = new VoPermContainer();
		ObjectPermission objectPermissionAct = new ObjectPermission();
		objectPermissionAct.setPermissionId(actPermId);
		voObjectPermContainerAct.setObjectPermission(objectPermissionAct);
		objectPermissionAct.setCreationType(creationType);
		element.setActivatePermission(voObjectPermContainerAct);
		if (creationType == CreationType.ADD) {
			element.setActivate(true);
		}/* else if (creationType == CreationType.DELETE) {
			element.setActivate(false);
		}*/
		this.viewer.getCachedObjPermissionMap().put(actPermId, voObjectPermContainerAct);
		getViewer().refresh(element);
	}

	/* (non-Javadoc)
	 * @see com.magna.xmsystem.xmadmin.ui.parts.role.editingsupport.CheckBoxEditingSupport#canEdit(java.lang.Object)
	 */
	@Override
	protected boolean canEdit(final Object element) {
		if (element instanceof ObjectPermissions) {
			String permissionName = ((ObjectPermissions) element).getPermissionName();
			ObjectType objectType = ObjectType.getObjectType(permissionName);
			if (objectType == ObjectType.ICON) {
				return false;
			}
		}
		return true;
	}
}
