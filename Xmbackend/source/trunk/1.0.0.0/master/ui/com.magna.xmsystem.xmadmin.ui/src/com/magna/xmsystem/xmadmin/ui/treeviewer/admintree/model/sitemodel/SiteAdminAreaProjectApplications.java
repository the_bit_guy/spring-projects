package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.projectapplicationmodel.ProjectApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.refs.RelationObj;

/**
 * The Class SiteAdminAreaChildProjectApp.
 * 
 * @author subash.janarthanan
 * 
 */
public class SiteAdminAreaProjectApplications implements IAdminTreeChild {
	
	/** The parent. */
	private IAdminTreeChild parent;

	/** The site admin area child pro app child. */
	private Map<String, IAdminTreeChild> siteAdminAreaProAppChild;

	/**
	 * Instantiates a new site admin area child project app.
	 *
	 * @param parent the parent
	 */
	public SiteAdminAreaProjectApplications(final IAdminTreeChild parent) {
		this.parent = parent;
		this.siteAdminAreaProAppChild = new LinkedHashMap<>();
		this.addFixedChildren();
	}

	/**
	 * Adds the.
	 *
	 * @param siteAdminAreaChildProAppChildId the site admin area child pro app child id
	 * @param child the child
	 * @return the i admin tree child
	 */
	public IAdminTreeChild add(final String siteAdminAreaChildProAppChildId, final IAdminTreeChild child) {
		child.setParent(this);
		IAdminTreeChild returnVal = this.siteAdminAreaProAppChild.put(siteAdminAreaChildProAppChildId, child);
		if (child instanceof RelationObj && ((RelationObj) child).getRefObject() instanceof ProjectApplication) {
			sort();
		}
		return returnVal;
	}

	/**
	 * Removes the.
	 *
	 * @param siteAdminAreaChildProAppChildId the site admin area child pro app child id
	 * @return the i admin tree child
	 */
	public IAdminTreeChild remove(final String siteAdminAreaChildProAppChildId) {
		return this.siteAdminAreaProAppChild.remove(siteAdminAreaChildProAppChildId);
	}
	
	/**
	 * Method for Adds the fixed children.
	 */
	public void addFixedChildren() {
		siteAdminAreaProAppChild.put(SiteAdminProjectAppNotFixed.class.getName(), new SiteAdminProjectAppNotFixed(
				this));
		siteAdminAreaProAppChild.put(SiteAdminProjectAppFixed.class.getName(), new SiteAdminProjectAppFixed(
				this));
		siteAdminAreaProAppChild.put(SiteAdminProjectAppProtected.class.getName(), new SiteAdminProjectAppProtected(
				this));
	}
	
	/**
	 * Gets the site admin area child pro app collection.
	 *
	 * @return the site admin area child pro app collection
	 */
	public Collection<IAdminTreeChild> getSiteAdminAreaChildProAppCollection() {
		return this.siteAdminAreaProAppChild.values();
	}

	/**
	 * Gets the site admin area child pro app child.
	 *
	 * @return the site admin area child pro app child
	 */
	public Map<String, IAdminTreeChild> getSiteAdminAreaChildProAppChild() {
		return siteAdminAreaProAppChild;
	}
	
	/**
	 * Gets the parent.
	 *
	 * @return the parent
	 */
	/* (non-Javadoc)
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#getParent()
	 */
	@Override
	public IAdminTreeChild getParent() {
		return this.parent;
	}

	/**
	 * Sets the parent.
	 *
	 * @param parent the new parent
	 */
	/* (non-Javadoc)
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#setParent(com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild)
	 */
	@Override
	public void setParent(final IAdminTreeChild parent) {
		this.parent = parent;
	}
	
	/**
	 * Sort.
	 */
	public void sort() {
		final LinkedHashMap<String, IAdminTreeChild> collect = this.siteAdminAreaProAppChild.entrySet().stream().sorted(
				(e1, e2) -> ((ProjectApplication) (((RelationObj) e1.getValue()).getRefObject())).getName()
				.compareTo(((ProjectApplication) (((RelationObj) e2.getValue()).getRefObject())).getName()))
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
		this.siteAdminAreaProAppChild = collect;
	}

}
