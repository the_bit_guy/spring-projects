package com.magna.xmsystem.xmadmin.ui.handlers.toolbar;

import java.util.List;

import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.model.application.ui.menu.MToolItem;
import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreePath;
import org.eclipse.swt.widgets.Tree;

import com.magna.xmsystem.xmadmin.ui.parts.IEditablePart;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.AdminTreeviewer;
import com.magna.xmsystem.xmadmin.util.CommonConstants;
import com.magna.xmsystem.xmadmin.util.XMAdminUtil;

/**
 * The Class CollapseAllToolBarHandler.
 * 
 * @author subash.janarthanan
 * 
 */
public class CollapseAllToolBarHandler {
	
	/**
	 * Execute.
	 */
	@Execute
	public void execute() {
		XMAdminUtil instance = XMAdminUtil.getInstance();
		final AdminTreeviewer adminTree = instance.getAdminTree();
		if (adminTree != null) {
			IStructuredSelection selection = adminTree.getStructuredSelection();
			Object treeSelectionObj;
			adminTree.collapseAll();
			if (selection != null && (treeSelectionObj = selection.getFirstElement()) != null) {
				adminTree.setSelection(new StructuredSelection(treeSelectionObj));
			}
			
		}
	}
	
	/**
	 * Can execute.
	 * @return true, if successful
	 * 
	 * @author shashwat.anand
	 */
	@CanExecute
	public boolean canExecute(final MApplication application, final EModelService service) {
		MPart rightHandMPartOfOldPerspective = (MPart) service.find(CommonConstants.PART_ID.INFORMATION_PART_ID,
				application);
		if (rightHandMPartOfOldPerspective.getObject() instanceof IEditablePart
				&& rightHandMPartOfOldPerspective.isDirty()) {
			return false;
		}
		AdminTreeviewer adminTreeViewer;
		Tree tree;
		if ((adminTreeViewer = XMAdminUtil.getInstance().getAdminTree()) != null
				&& (tree = adminTreeViewer.getTree()) != null && !tree.isDisposed()) {
			final TreePath[] treePaths = adminTreeViewer.getExpandedTreePaths();
			if (treePaths.length > 0) {
				List<MToolItem> mToolItems = service.findElements(application,
						"com.magna.xmsystem.xmadmin.ui.handledtoolitem.collapse", MToolItem.class, null); //$NON-NLS-1$
				if (mToolItems.size() > 0) {
					mToolItems.get(0).setEnabled(true);
				}
				return true;
			}
		}
		return false;
	}
}
