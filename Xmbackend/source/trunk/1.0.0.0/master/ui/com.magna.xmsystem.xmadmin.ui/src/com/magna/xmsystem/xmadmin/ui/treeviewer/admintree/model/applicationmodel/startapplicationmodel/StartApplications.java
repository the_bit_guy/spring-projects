package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.startapplicationmodel;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;

/**
 * Model class for StartApplications.
 *
 * @author Deepak upadhyay
 */
public class StartApplications implements IAdminTreeChild {
	/** The parent. */
	private IAdminTreeChild parent;
	
	/** The start application children. */
	private Map<String, IAdminTreeChild> startApplicationChildren;

	/**
	 * Constructor
	 */
	public StartApplications() {
		this.startApplicationChildren = new LinkedHashMap<>();
	}

	/**
	 * Adds the child
	 * 
	 * @param child
	 *            {@link IAdminTreeChild}
	 * @return {@link boolean}
	 */
	public IAdminTreeChild add(final String startApplicatioId, final IAdminTreeChild child) {
		child.setParent(this);
		final IAdminTreeChild returnVal = this.startApplicationChildren.put(startApplicatioId, child);
		sort();
		return returnVal;
	}

	/**
	 * Removes the child
	 * 
	 * @param child
	 *            {@link IAdminTreeChild}
	 * @return {@link boolean}
	 */
	public IAdminTreeChild remove(final String child) {
		return this.startApplicationChildren.remove(child);
	}

	/**
	 * Method for Removes the all.
	 */
	public void removeAll() {
		this.startApplicationChildren.clear();
	}
	
	/**
	 * @return {@link List} of {@link StartApplication}
	 */
	public Collection<IAdminTreeChild> getStartAppCollection() {
		return this.startApplicationChildren.values();
	}

	/**
	 * @return {@link List} of {@link StartApplication}
	 */
	public Map<String, IAdminTreeChild> getStartApplications() {
		return this.startApplicationChildren;
	}
	
	/**
	 * Returns null as its parent
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#getParent()
	 */
	@Override
	public IAdminTreeChild getParent() {
		return this.parent;
	}

	/**
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#setParent(com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild)
	 */
	@Override
	public void setParent(final IAdminTreeChild parent) {
		this.parent = parent;
	}
	
	/**
	 * Sort.
	 */
	public void sort() {
		final LinkedHashMap<String, IAdminTreeChild> collect = this.startApplicationChildren.entrySet().stream().sorted(
				(e1, e2) -> ((StartApplication) e1.getValue()).getName().compareTo(((StartApplication) e2.getValue()).getName()))
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
		this.startApplicationChildren = collect;
	}
}
