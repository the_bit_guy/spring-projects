package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification;

import com.magna.xmbackend.vo.enums.NotificationEventType;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;

/**
 * The Class NotificationEvent.
 * 
 * @author shashwat.anand
 */
public abstract class INotificationEvent implements IAdminTreeChild {
	
	/** The event type. */
	protected NotificationEventType eventType;
	
	/** The id. */
	private String id;
	
	/** The active. */
	private boolean active;

	/**
	 * Gets the event type.
	 *
	 * @return the eventType
	 */
	public NotificationEventType getEventType() {
		return eventType;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Checks if is active.
	 *
	 * @return true, if is active
	 */
	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}
}
