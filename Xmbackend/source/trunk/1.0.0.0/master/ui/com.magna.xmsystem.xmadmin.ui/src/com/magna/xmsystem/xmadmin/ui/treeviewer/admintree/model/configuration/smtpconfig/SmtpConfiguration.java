package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.smtpconfig;

import java.beans.PropertyChangeEvent;

import com.magna.xmsystem.dependencies.utils.XMSystemUtil;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.BeanModel;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.util.CommonConstants;

// TODO: Auto-generated Javadoc
/**
 * The Class SmtpConfiguration.
 * 
 * @author archita.patel
 */
public class SmtpConfiguration extends BeanModel implements IAdminTreeChild {

	/** The Constant PORT_LIMIT. */
	public static final int PORT_LIMIT = 5;

	/** The Constant PROPERTY_SMTP_EMAIL. */
	public static final String PROPERTY_SMTP_EMAIL = "smtpEmail"; //$NON-NLS-1$
	
	/** The Constant PROPERTY_PASSWORD. */
	public static final String PROPERTY_PASSWORD = "password"; //$NON-NLS-1$

	/** The Constant PROPERTY_SMTP_ID. */
	public static final String PROPERTY_SMTP_ID = "smtpConfigId"; //$NON-NLS-1$

	/** The Constant PROPERTY_SMTP_PORT. */
	public static final String PROPERTY_SMTP_PORT = "smtpPort"; //$NON-NLS-1$

	/** The Constant PROPERTY_SMTP_SERVERNAME. */
	public static final String PROPERTY_SMTP_SERVERNAME = "smtpServerName"; //$NON-NLS-1$

	/** The Constant PROPERTY_OPERATIONMODE. */
	public static final String PROPERTY_OPERATIONMODE = "operationMode"; //$NON-NLS-1$

	/** The smtp config id. */
	private String smtpConfigId;

	/** The smtp email. */
	private String smtpEmail;

	/** The password. */
	private String password;
	
	/** The smtp port. */
	private String smtpPort;

	/** The smtp server name. */
	private String smtpServerName;

	/** The operation mode. */
	private int operationMode;

	/**
	 * Instantiates a new smtp configuration.
	 */
	public SmtpConfiguration() {

	}

	/**
	 * Instantiates a new smtp configuration.
	 *
	 * @param smtpConfigId
	 *            the smtp config id
	 * @param operationMode
	 *            the operation mode
	 */
	public SmtpConfiguration(final String smtpConfigId, final int operationMode) {

		this(smtpConfigId, null,null, null, null, operationMode);
	}

	/**
	 * Instantiates a new smtp configuration.
	 *
	 * @param smtpConfigId
	 *            the smtp config id
	 * @param smtpEmail
	 *            the smtp email
	 * @param smtpPort
	 *            the smtp port
	 * @param smtpServerName
	 *            the smtp server name
	 * @param operationMode
	 *            the operation mode
	 */
	public SmtpConfiguration(final String smtpConfigId, final String smtpEmail,final String password, final String smtpPort,
			final String smtpServerName, final int operationMode) {
		super();
		this.smtpConfigId = smtpConfigId;
		this.smtpEmail = smtpEmail;
		this.password= password;
		this.smtpPort = smtpPort;
		this.smtpServerName = smtpServerName;
		this.operationMode = operationMode;
	}

	/**
	 * Gets the smtp config id.
	 *
	 * @return the smtp config id
	 */
	public String getSmtpConfigId() {
		return smtpConfigId;
	}

	/**
	 * Sets the smtp config id.
	 *
	 * @param smtpConfigId
	 *            the new smtp config id
	 */
	public void setSmtpConfigId(String smtpConfigId) {
		if (smtpConfigId == null)
			throw new IllegalArgumentException();
		this.propertyChangeSupport.firePropertyChange(PROPERTY_SMTP_ID, this.smtpConfigId,
				this.smtpConfigId = smtpConfigId);
		// this.smtpConfigId = smtpCinfigId;
	}

	/**
	 * Gets the smtp email.
	 *
	 * @return the smtp email
	 */
	public String getSmtpEmail() {
		return smtpEmail;
	}

	/**
	 * Sets the smtp email.
	 *
	 * @param smtpEmail
	 *            the new smtp email
	 */
	public void setSmtpEmail(String smtpEmail) {
		if (smtpEmail == null)
			throw new IllegalArgumentException();
		this.propertyChangeSupport.firePropertyChange(PROPERTY_SMTP_EMAIL, this.smtpEmail, this.smtpEmail = smtpEmail);
		// this.smtpEmail = smtpEmail;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		if (password == null)
			throw new IllegalArgumentException();
		this.propertyChangeSupport.firePropertyChange(PROPERTY_PASSWORD, this.password, this.password = password);
		//this.password = password;
	}

	/**
	 * Gets the smtp port.
	 *
	 * @return the smtp port
	 */
	public String getSmtpPort() {
		return smtpPort;
	}

	/**
	 * Sets the smtp port.
	 *
	 * @param smtpPort
	 *            the new smtp port
	 */
	public void setSmtpPort(String smtpPort) {
		if (smtpPort == null)
			throw new IllegalArgumentException();
		this.propertyChangeSupport.firePropertyChange(PROPERTY_SMTP_PORT, this.smtpPort, this.smtpPort = smtpPort);
		// this.smtpPort = smtpPort;
	}

	/**
	 * Gets the smtp server name.
	 *
	 * @return the smtp server name
	 */
	public String getSmtpServerName() {
		return smtpServerName;
	}

	/**
	 * Sets the smtp server name.
	 *
	 * @param smtpServerName
	 *            the new smtp server name
	 */
	public void setSmtpServerName(String smtpServerName) {
		if (smtpServerName == null)
			throw new IllegalArgumentException();
		this.propertyChangeSupport.firePropertyChange(PROPERTY_SMTP_SERVERNAME, this.smtpServerName,
				this.smtpServerName = smtpServerName);

		// this.smtpServerName = smtpServerName;
	}

	/**
	 * Gets the operation mode.
	 *
	 * @return the operation mode
	 */
	public int getOperationMode() {
		return operationMode;
	}

	/**
	 * Sets the operation mode.
	 *
	 * @param operationMode
	 *            the new operation mode
	 */
	public void setOperationMode(int operationMode) {
		this.propertyChangeSupport.firePropertyChange(PROPERTY_OPERATIONMODE, this.operationMode,
				this.operationMode = operationMode);
		// this.operationMode = operationMode;
	}

	/**
	 * Deep copy smtp config model.
	 *
	 * @param update
	 *            the update
	 * @param updateThisObject
	 *            the update this object
	 * @return the smtp configuration
	 */
	public SmtpConfiguration deepCopySmtpConfigModel(boolean update, SmtpConfiguration updateThisObject) {

		SmtpConfiguration clonedSmtpConfig = null;
		try {
			String currentSmtpConfigId = XMSystemUtil.isEmpty(this.getSmtpConfigId()) ? CommonConstants.EMPTY_STR
					: this.getSmtpConfigId();

			String currentEmail = XMSystemUtil.isEmpty(this.getSmtpEmail()) ? CommonConstants.EMPTY_STR
					: this.getSmtpEmail();
			
			String currentPassword = XMSystemUtil.isEmpty(this.getPassword()) ? CommonConstants.EMPTY_STR
					: this.getPassword();

			String currentPort = XMSystemUtil.isEmpty(this.getSmtpPort()) ? CommonConstants.EMPTY_STR
					: this.getSmtpPort();

			String currentServerName = XMSystemUtil.isEmpty(this.getSmtpServerName()) ? CommonConstants.EMPTY_STR
					: this.getSmtpServerName();

			if (update) {
				clonedSmtpConfig = updateThisObject;
			} else {
				clonedSmtpConfig = new SmtpConfiguration(currentSmtpConfigId, currentEmail,currentPassword, currentPort,
						currentServerName, CommonConstants.OPERATIONMODE.VIEW);
			}
			clonedSmtpConfig.setSmtpConfigId(currentSmtpConfigId);
			clonedSmtpConfig.setSmtpEmail(currentEmail);
			clonedSmtpConfig.setPassword(currentPassword);
			clonedSmtpConfig.setSmtpPort(currentPort);
			clonedSmtpConfig.setSmtpServerName(currentServerName);

			return clonedSmtpConfig;
		} catch (Exception e) {
			return null;
		}

	}

	/*

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.BeanModel#
	 * propertyChange(java.beans.PropertyChangeEvent)
	 */
	@Override
	public void propertyChange(PropertyChangeEvent event) {
		this.propertyChangeSupport.firePropertyChange(event.getPropertyName(), event.getOldValue(), event.getNewValue());

	}

}
