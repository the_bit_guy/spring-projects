
package com.magna.xmsystem.xmadmin.ui.handlers.projectapplication;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.inject.Inject;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.core.services.nls.Translation;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.widgets.Display;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.ResourceAccessException;

import com.magna.xmbackend.vo.projectApplication.ProjectApplicationResponse;
import com.magna.xmsystem.dependencies.customExceptions.UnauthorizedAccessException;
import com.magna.xmsystem.ui.controls.widgets.CustomMessageDialog;
import com.magna.xmsystem.xmadmin.message.Message;
import com.magna.xmsystem.xmadmin.restclient.application.ProjectAppController;
import com.magna.xmsystem.xmadmin.ui.parts.IEditablePart;
import com.magna.xmsystem.xmadmin.ui.parts.InformationPart;
import com.magna.xmsystem.xmadmin.ui.parts.objexp.ObjectExpPage;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.AdminTreeviewer;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.AdminTreeFactory;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.projectapplicationmodel.ProjectApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.projectapplicationmodel.ProjectApplicationChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.projectapplicationmodel.ProjectApplications;
import com.magna.xmsystem.xmadmin.util.CommonConstants;
import com.magna.xmsystem.xmadmin.util.MessageType;
import com.magna.xmsystem.xmadmin.util.XMAdminUtil;

/**
 * Class for DeleteProjectAppHandler.
 *
 * @author Deepak upadhyay
 */
public class DeleteProjectAppHandler {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(DeleteProjectAppHandler.class);

	/** Member variable for {@link Message}. */
	@Inject
	@Translation
	private Message messages;

	/** Member variable 'application' for {@link MApplication}. */
	@Inject
	private MApplication application;

	/** Member variable 'model service' for {@link EModelService}. */
	@Inject
	private EModelService modelService;

	/**
	 * Method to delete the projectApplication
	 */
	@Execute
	public void execute() {
		try {
			if (XMAdminUtil.getInstance().isAcessAllowed("PROJECT_APPLICATION-DELETE")) {
				final AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
				IStructuredSelection selection;
				Object selectionObj;
				MPart mPart = (MPart) this.modelService.find(CommonConstants.PART_ID.INFORMATION_PART_ID, application);
				InformationPart rightSidePart2 = (InformationPart) mPart.getObject();
				ObjectExpPage objectExpPage = rightSidePart2.getObjectExpPartUI();
				if (objectExpPage != null
						&& (selection = objectExpPage.getObjExpTableViewer().getStructuredSelection()) != null
						&& (selectionObj = selection.getFirstElement()) != null
						&& selectionObj instanceof ProjectApplication) {
					deleteProjectApplication(selection);
					objectExpPage.refreshExplorer();
				} else if (adminTree != null && (selection = adminTree.getStructuredSelection()) != null
						&& (selectionObj = selection.getFirstElement()) != null
						&& selectionObj instanceof ProjectApplication) {
					deleteProjectApplication(selection);
				}
				XMAdminUtil.getInstance().clearClipBoardContents();
			} else {
				CustomMessageDialog.openError(Display.getCurrent().getActiveShell(),
						messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
			}
		} catch (ClassCastException e) {
			LOGGER.error("Exception occured while deleting projectApplication " + e);
		} catch (UnauthorizedAccessException e) {
			LOGGER.error("Current user is Unauthorized " + e);
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				CustomMessageDialog.openError(Display.getCurrent().getActiveShell(), messages.errorDialogTitile,
						messages.serverNotReachable);
			}
			LOGGER.error("Exception occured in calling access allowed API " + e);
		}
	}

	/**
	 * Delete project application.
	 *
	 * @param selectionObj
	 *            the selection obj
	 */
	@SuppressWarnings("rawtypes")
	private void deleteProjectApplication(IStructuredSelection selection) {
		Object selectionObj = selection.getFirstElement();
		//String projectAppName = "";
		final AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
		final ProjectApplications projectApplications = AdminTreeFactory.getInstance().getProjectApplications();
		String projectAppName = ((ProjectApplication) selectionObj).getName();

		String confirmDialogTitle = messages.deleteConfirmDialogTitle;
		String confirmDialogMsg;
		if (selection.size() == 1) {
			confirmDialogMsg = messages.deleteProjectAppConfirmDialogMsgPart1 + " \'" + projectAppName + "\' "
					+ messages.deleteProjectAppConfirmDialogMsgPart2 + " ?";
		} else {
			confirmDialogMsg = messages.deleteMultiObjectsConfirmDialogMsg + " "
					+ messages.deleteProjectAppConfirmDialogMsgPart2 + " ?";
		}
		boolean isConfirmed = CustomMessageDialog.openConfirm(adminTree.getControl().getShell(), confirmDialogTitle,
				confirmDialogMsg);
		if (isConfirmed) {
			List selectionList = selection.toList();
			Job job = new Job("Deleting Objects...") {

				@Override
				protected IStatus run(IProgressMonitor monitor) {
					monitor.beginTask("Deleting Objects..", 100);
					monitor.worked(30);
					try {
						Set<String> projectAppIds = new HashSet<>();
						Set<String> childProjectAppIds = new HashSet<>();
						ProjectApplicationResponse projectApplicationResponse;
						ProjectApplicationResponse childprojectAppResponse;
						Map<String,String> successObjectNames = new HashMap<>();
						Map<String,String> failObjectNames = new HashMap<>();
						for (int i = 0; i < selectionList.size(); i++) {
							String id = ((ProjectApplication) selectionList.get(i)).getProjectApplicationId();
							String name = ((ProjectApplication) selectionList.get(i)).getName();
							projectAppIds.add(id);
							successObjectNames.put(id, name);
						}
						final ProjectAppController projectAppController = new ProjectAppController();
						for (int i = 0; i < selectionList.size(); i++) {
							ProjectApplication projectApplication = (ProjectApplication) selectionList.get(i);
							if (ProjectApplication.class.getSimpleName()
									.equals(selectionObj.getClass().getSimpleName())) {
								if (projectApplication.getProjectApplicationChildren() != null) {
									Map<String, IAdminTreeChild> projectApplicationChildren = projectApplication
											.getProjectApplicationChildren();
									Collection<IAdminTreeChild> values = projectApplicationChildren.values();
									for (IAdminTreeChild iAdminTreeChild : values) {
										if (iAdminTreeChild instanceof ProjectApplicationChild) {
											ProjectApplicationChild projectApplicationChild = (ProjectApplicationChild) iAdminTreeChild;
											childProjectAppIds.add(projectApplicationChild.getProjectApplicationId());
										}
									}
								}
								// projectAppIds.add(projectApplication.getProjectApplicationId());
							}
						}
						childprojectAppResponse = projectAppController.deleteMultiProjectApp(childProjectAppIds);
						if (childprojectAppResponse != null) {
							if (!childprojectAppResponse.getStatusMaps().isEmpty()) {
								List<Map<String, String>> statusMapList = childprojectAppResponse.getStatusMaps();
								for (Map<String, String> statusMap : statusMapList) {
									String string = statusMap.get("en");
									String[] split = string.split("id");
									childProjectAppIds.remove(split[1].trim());
									failObjectNames.put(split[1].trim(), successObjectNames.get(split[1].trim()));
									successObjectNames.remove(split[1].trim());
								}
								Display.getDefault().asyncExec(new Runnable() {
									public void run() {
										CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
												messages.deleteConfirmDialogTitle,
												messages.deleteMultiObjectsDialogErrMsg);
									}
								});
							} else {
								projectApplicationResponse = projectAppController.deleteMultiProjectApp(projectAppIds);
								if (projectApplicationResponse != null) {
									if (!projectApplicationResponse.getStatusMaps().isEmpty()) {
										List<Map<String, String>> statusMapList = childprojectAppResponse
												.getStatusMaps();
										for (Map<String, String> statusMap : statusMapList) {
											String string = statusMap.get("en");
											String[] split = string.split("id");
											projectAppIds.remove(split[1].trim());
										}
										Display.getDefault().asyncExec(new Runnable() {
											public void run() {
												CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
														messages.deleteConfirmDialogTitle,
														messages.deleteMultiObjectsDialogErrMsg);
											}
										});
									}
									for (String projectAppId : projectAppIds) {
										projectApplications.getProjectApplications().remove(projectAppId);
									}
									Display.getDefault().asyncExec(new Runnable() {
										public void run() {
											adminTree.setSelection(new StructuredSelection(projectApplications), true);
											adminTree.refresh(projectApplications);
										}
									});
									if (successObjectNames.size() > 0) {
										Iterator it = successObjectNames.entrySet().iterator();
										while (it.hasNext()) {
											Map.Entry map = (Map.Entry) it.next();
											XMAdminUtil.getInstance()
													.updateLogFile(
															messages.projectApplicationObject + " '" + map.getValue()
																	+ "' " + messages.objectDelete,
															MessageType.SUCCESS);
										}
									}
									if (failObjectNames.size() > 0) {
										Iterator it = failObjectNames.entrySet().iterator();
										while (it.hasNext()) {
											Map.Entry map = (Map.Entry) it.next();
											XMAdminUtil.getInstance().updateLogFile(messages.deleteErrorMsg + " "
													+ messages.projectApplicationObject + " '" + map.getValue() + "'",
													MessageType.FAILURE);
										}
									}
								}
							}
						}

					} catch (Exception e) {
						LOGGER.error(e.getMessage());
					}

					monitor.worked(70);
					return Status.OK_STATUS;
				}
			};
			job.setUser(true);
			job.schedule();
		}
	}

	/**
	 * Checks if is acess allowed.
	 *
	 * @return true, if is acess allowed
	 */
	/*public boolean isAcessAllowed() {
		boolean returnVal = true;
		try {
			ValidationController controller = new ValidationController();
			ValidationRequest request = new ValidationRequest();
			request.setUserName(RestClientUtil.getInstance().getUserName());
			request.setPermissionType(PermissionType.OBJECT_PERMISSION.name());
			request.setPermissionName("PROJECT_APPLICATION-DELETE"); //$NON-NLS-1$
			returnVal = controller.getAccessAllowed(request);
		} catch (UnauthorizedAccessException e) {
			LOGGER.error("Current user is Unauthorized " + e);
			returnVal = false;
		} catch (Exception e) {
			LOGGER.error("Exception occured in calling access allowed API " + e);
			returnVal = false;
		}
		return returnVal;
	}*/

	/**
	 * Can execute.
	 *
	 * @return true, if successful
	 */
	@CanExecute
	public boolean canExecute() {
		boolean returnVal = true;
		final XMAdminUtil instance = XMAdminUtil.getInstance();
		MPart rightPart;
		Object rightView;
		if ((rightPart = instance.getInformationPart()) != null
				&& (rightView = rightPart.getObject()) != null && rightView instanceof IEditablePart
				&& ((IEditablePart) rightView).isDirty()) {
			returnVal = false;
		}
		return returnVal;
	}
}