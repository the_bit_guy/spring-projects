package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree;

import javax.inject.Inject;

import org.eclipse.e4.core.services.nls.Translation;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.graphics.Image;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magna.xmbackend.vo.enums.NotificationEventType;
import com.magna.xmsystem.dependencies.utils.XMSystemUtil;
import com.magna.xmsystem.xmadmin.message.Message;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.TreeLoading;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaInfo;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaInfoHistory;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaInfoStatus;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaProjectAppFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaProjectAppNotFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaProjectAppProtected;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaProjectApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaProjectStartApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaProjects;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaStartApp;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaStartApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaUserAppFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaUserAppNotFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaUserAppProtected;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaUserApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdministrationArea;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdministrationAreas;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.Applications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.baseapplicationmodel.BaseAppInformationHistory;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.baseapplicationmodel.BaseAppInformationModel;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.baseapplicationmodel.BaseAppInformationStatus;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.baseapplicationmodel.BaseAppProjectApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.baseapplicationmodel.BaseAppStartApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.baseapplicationmodel.BaseAppUserApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.baseapplicationmodel.BaseApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.baseapplicationmodel.BaseApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.projectapplicationmodel.ProjectAppAdminAreaProjects;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.projectapplicationmodel.ProjectAppAdminAreas;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.projectapplicationmodel.ProjectAppGroups;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.projectapplicationmodel.ProjectAppUsers;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.projectapplicationmodel.ProjectApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.projectapplicationmodel.ProjectApplicationChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.projectapplicationmodel.ProjectApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.startapplicationmodel.StartAppAdminAreas;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.startapplicationmodel.StartAppProjects;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.startapplicationmodel.StartAppUsers;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.startapplicationmodel.StartApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.startapplicationmodel.StartApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.userapplicationmodel.UserAppAdminAreas;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.userapplicationmodel.UserAppGroups;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.userapplicationmodel.UserAppUsers;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.userapplicationmodel.UserApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.userapplicationmodel.UserApplicationChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.userapplicationmodel.UserApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.Configurations;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.adminconf.AdminMenu;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.adminconf.AdminProjectApps;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.adminconf.AdminUserApps;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.adminconf.AdminUsers;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.icon.Icon;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.icon.Icons;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.ldapconfig.LdapConfiguration;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.livemsgconfig.LiveMessage;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.livemsgconfig.LiveMessages;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.noProjectMessage.NoProjectPopMessage;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.Notification;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.NotificationTemplate;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.NotificationTemplates;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.Notifications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.NotifyAAProjectUserEvt;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.NotifyProjectUserEvt;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.ProAARelAssignEvt;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.ProAARelRemoveEvt;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.ProjectActivateEvt;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.ProjectActivateEvtAction;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.ProjectCreateEvt;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.ProjectCreateEvtAction;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.ProjectDeactivateEvt;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.ProjectDeactivateEvtAction;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.ProjectDeleteEvt;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.ProjectDeleteEvtAction;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.UserProExpEvt;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.UserProExpGraceEvt;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.UserProjectRelAssignEvt;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.UserProjectRelAssignEvtAction;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.UserProjectRelRemoveEvt;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.UserProjectRelRemoveEvtAction;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.projectexpiryconfig.ProjectExpiryConfig;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.Role;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.RoleScopeObjectUsers;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.RoleScopeObjects;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.RoleUsers;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.Roles;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.singletonapptimeconfig.SingletonAppTimeConfig;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.smtpconfig.SmtpConfiguration;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.directory.Directories;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.directory.Directory;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.directory.DirectoryApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.directory.DirectoryProjectApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.directory.DirectoryProjects;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.directory.DirectoryUserApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.directory.DirectoryUsers;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.Groups;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.projectappgroup.ProjectAppGroupProjectApps;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.projectappgroup.ProjectApplicationGroup;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.projectappgroup.ProjectApplicationGroups;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.projectgroup.ProjectGroupModel;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.projectgroup.ProjectGroupProjects;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.projectgroup.ProjectGroupsModel;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.userappgroup.UserAppGroupUserApps;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.userappgroup.UserApplicationGroup;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.userappgroup.UserApplicationGroups;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.usergroup.UserGroupModel;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.usergroup.UserGroupUsers;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.usergroup.UserGroupsModel;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProUserProjectApp;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.Project;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectAdminAreaProjectAppFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectAdminAreaProjectAppNotFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectAdminAreaProjectAppProtected;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectAdminAreaProjectApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectAdminAreaStartApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectAdminAreas;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectInfoHistory;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectInfoStatus;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectInformation;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectProjectApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectUserAAProjectAppAllowed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectUserAAProjectAppForbidden;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectUserAAProjectApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectUserAdminAreas;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectUsers;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.Projects;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.refs.RelationObj;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.Site;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaInformationHistory;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaInformationStatus;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaInformations;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaProjectApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaProjectStartApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaProjects;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaStartApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaUserAppFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaUserAppNotFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaUserAppProtected;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaUserApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminProjectAppFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminProjectAppNotFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminProjectAppProtected;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdministrations;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.Sites;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.User;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserAAUserAppAllowed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserAAUserAppForbidden;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserAAUserApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserAdminAreas;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserGroups;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserInformationHistory;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserInformationStatus;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserInformations;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserProjectAAProjectAppAllowed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserProjectAAProjectAppForbidden;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserProjectAAProjectApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserProjectAdminAreas;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserProjects;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserStartApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.Users;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UsersNameAlphabet;
import com.magna.xmsystem.xmadmin.util.CommonConstants;

/**
 * Class for label provider of admin tree
 * 
 * @author shashwat.anand
 *
 */
public class AdminTreeLabelProvider extends LabelProvider {
	/**
	 * Logger instance
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(AdminTreeLabelProvider.class);

	/**
	 * Member variable for {@link Message}
	 */
	@Inject
	@Translation
	private Message messages;

	private TreeViewer viewer;

	/**
	 * Constructor.
	 *
	 * @param viewer the viewer
	 */
	@Inject
	public AdminTreeLabelProvider(final TreeViewer viewer) {
		super();
		this.viewer = viewer;
	}

	/**
	 * Get the text for given object
	 * 
	 * @param element
	 *            {@link Object}
	 */
	@Override
	public String getText(final Object element) {
		// FIXME 2017-10-11 SCP: Too many nested if-else-statements, extensively
		// usage of instanceof
		// => move responsibility for returning the correct text to the
		// individual object types
		if (element instanceof TreeLoading) {
			return messages.treeLoading;
		} else if (element instanceof Sites) {
			return messages.sitesNode;
		} else if (element instanceof Site) {
			return ((Site) element).getName();
		} else if (element instanceof SiteAdministrations || element instanceof ProjectAdminAreas) {
			return messages.administrationAreasNode;
		} else if (element instanceof SiteAdminAreaProjects) {
			return messages.projectsNode;
		} else if (element instanceof SiteAdminAreaUserApplications) {
			return messages.userApplicationsNode;
		} else if (element instanceof SiteAdminAreaStartApplications) {
			return messages.startApplicationsNode;
		} else if (element instanceof SiteAdminAreaInformations) {
			return messages.informationNode;
		} else if (element instanceof UserInformations) {
			return messages.informationNode;
		} else if (element instanceof UserInformationStatus) {
			return messages.statusNode;
		} else if (element instanceof UserInformationHistory) {
			return messages.historyNode;
		} else if (element instanceof SiteAdminAreaProjectApplications) {
			return messages.projectApplicationsNode;
		} else if (element instanceof SiteAdminAreaProjectStartApplications) {
			return messages.startApplicationsNode;
		} else if (element instanceof SiteAdminProjectAppNotFixed) {
			return messages.notFixedNode;
		} else if (element instanceof SiteAdminProjectAppFixed) {
			return messages.fixedNode;
		} else if (element instanceof SiteAdminProjectAppProtected) {
			return messages.protectedNode;
		} else if (element instanceof SiteAdminAreaUserAppNotFixed) {
			return messages.notFixedNode;
		} else if (element instanceof SiteAdminAreaUserAppFixed) {
			return messages.fixedNode;
		} else if (element instanceof SiteAdminAreaUserAppProtected) {
			return messages.protectedNode;
		} else if (element instanceof SiteAdminAreaInformationStatus) {
			return messages.statusNode;
		} else if (element instanceof SiteAdminAreaInformationHistory) {
			return messages.historyNode;
		} else if (element instanceof AdministrationAreas) {
			return messages.administrationAreasNode;
		} else if (element instanceof AdministrationArea) {
			AdministrationArea administrationArea = (AdministrationArea) element;
			StringBuilder adminAreaReturnName = new StringBuilder(administrationArea.getName());
			if (viewer instanceof AdminTreeviewer) {
				try {
					if (!XMSystemUtil.isEmpty(administrationArea.getRelatedSiteName())) {
						adminAreaReturnName.append(" [").append(administrationArea.getRelatedSiteName()).append("]");
					}
				} catch (Exception e) {
					LOGGER.info("The admin area " + adminAreaReturnName + " is not assigned to any site");
				}
			}
			return adminAreaReturnName.toString();
		} else if (element instanceof AdminAreaUserApplications) {
			return messages.userApplicationsNode;
		} else if (element instanceof AdminAreaUserAppFixed) {
			return messages.fixedNode;
		} else if (element instanceof AdminAreaUserAppNotFixed) {
			return messages.notFixedNode;
		} else if (element instanceof AdminAreaUserAppProtected) {
			return messages.protectedNode;
		} else if (element instanceof AdminAreaProjectApplications) {
			return messages.projectApplicationsNode;
		} else if (element instanceof AdminAreaProjectAppFixed) {
			return messages.fixedNode;
		} else if (element instanceof AdminAreaProjectAppNotFixed) {
			return messages.notFixedNode;
		} else if (element instanceof AdminAreaProjectAppProtected) {
			return messages.protectedNode;
		} else if (element instanceof AdminAreaStartApplications) {
			return messages.startApplicationsNode;
		} else if (element instanceof AdminAreaProjectStartApplications) {
			return messages.startApplicationsNode;
		} else if (element instanceof AdminAreaProjects) {
			return messages.projectsNode;
		} else if (element instanceof AdminAreaInfo) {
			return messages.informationNode;
		} else if (element instanceof AdminAreaInfoHistory) {
			return messages.historyNode;
		} else if (element instanceof AdminAreaInfoStatus) {
			return messages.statusNode;
		} else if (element instanceof AdminAreaStartApp) {
			return messages.startApplicationsNode;
		} else if (element instanceof Users) {
			return messages.usersNode;
		} else if (element instanceof UsersNameAlphabet) {
			return ((UsersNameAlphabet) element).getFirstChar();
		} else if (element instanceof User) {
			return ((User) element).getName();
		} else if (element instanceof UserProjects) {
			return messages.projectsNode;
		} else if (element instanceof UserProjectAdminAreas) {
			return messages.administrationAreasNode;
		} else if (element instanceof UserProjectAAProjectApplications) {
			return messages.projectApplicationsNode;
		} else if (element instanceof UserProjectAAProjectAppAllowed) {
			return messages.allowedNode;
		} else if (element instanceof UserProjectAAProjectAppForbidden) {
			return messages.forbiddenNode;
		} else if (element instanceof UserAAUserAppAllowed) {
			return messages.allowedNode;
		} else if (element instanceof UserAAUserAppForbidden) {
			return messages.forbiddenNode;
		} else if (element instanceof UserAdminAreas) {
			return messages.administrationAreasNode;
		} else if (element instanceof UserAAUserApplications) {
			return messages.userApplicationsNode;
		} else if (element instanceof UserStartApplications) {
			return messages.startApplicationsNode;
		} else if (element instanceof UserGroups) {
			return messages.groupsNode;
		} else if (element instanceof UserGroupUsers) {
			return messages.usersNode;
		} else if (element instanceof Projects) {
			return messages.projectsNode;
		} else if (element instanceof Project) {
			return ((Project) element).getName();
		} else if (element instanceof ProjectUsers) {
			return messages.usersNode;
		} else if (element instanceof ProjectUserAdminAreas) {
			return messages.administrationAreasNode;
		} else if (element instanceof ProjectUserAAProjectApplications) {
			return messages.projectApplicationsNode;
		} else if (element instanceof ProjectUserAAProjectAppAllowed) {
			return messages.allowedNode;
		} else if (element instanceof ProjectUserAAProjectAppForbidden) {
			return messages.forbiddenNode;
		} else if (element instanceof ProjectProjectApplications) {
			return messages.projectApplicationsNode;
		} else if (element instanceof ProUserProjectApp || element instanceof ProjectAdminAreaProjectApplications) {
			return messages.projectApplicationsNode;
		} else if (element instanceof ProjectAdminAreaProjectAppNotFixed) {
			return messages.notFixedNode;
		} else if (element instanceof ProjectAdminAreaProjectAppFixed) {
			return messages.fixedNode;
		} else if (element instanceof ProjectAdminAreaProjectAppProtected) {
			return messages.protectedNode;
		} else if (element instanceof ProjectAdminAreaStartApplications) {
			return messages.startApplicationsNode;
		} else if (element instanceof ProjectInformation) {
			return messages.informationNode;
		} else if (element instanceof ProjectInfoStatus) {
			return messages.statusNode;
		} else if (element instanceof ProjectInfoHistory) {
			return messages.historyNode;
		} else if (element instanceof ProjectInfoHistory) {
			return messages.historyNode;
		} else if (element instanceof Applications) {
			return messages.applicationsNode;
		} else if (element instanceof UserApplications) {
			return messages.userApplicationsNode;
		} else if (element instanceof UserAppAdminAreas) {
			return messages.administrationAreasNode;
		} else if (element instanceof UserAppUsers) {
			return messages.usersNode;
		} else if (element instanceof UserAppGroups) {
			return messages.groupsNode;
		} else if (element instanceof ProjectAppAdminAreas) {
			return messages.administrationAreasNode;
		} else if (element instanceof ProjectAppAdminAreaProjects) {
			return messages.projectsNode;
		} else if (element instanceof ProjectAppUsers) {
			return messages.usersNode;
		} else if (element instanceof ProjectAppGroups) {
			return messages.groupsNode;
		} else if (element instanceof StartAppUsers) {
			return messages.usersNode;
		} else if (element instanceof StartAppProjects) {
			return messages.projectsNode;
		} else if (element instanceof StartAppAdminAreas) {
			return messages.administrationAreasNode;
		} else if (element instanceof BaseAppProjectApplications) {
			return messages.projectApplicationsNode;
		} else if (element instanceof BaseAppUserApplications) {
			return messages.userApplicationsNode;
		} else if (element instanceof BaseAppStartApplications) {
			return messages.startApplicationsNode;
		} else if (element instanceof BaseAppInformationModel) {
			return messages.informationNode;
		} else if (element instanceof BaseAppInformationStatus) {
			return messages.statusNode;
		} else if (element instanceof BaseAppInformationHistory) {
			return messages.historyNode;
		} else if (element instanceof RoleScopeObjects) {
			final String name = ((RoleScopeObjects) element).getName();
			if (name.equals(AdministrationAreas.class.getName())) {
				return messages.administrationAreasNode;
			}
		} else if (element instanceof UserApplication) {
			return ((UserApplication) element).getName();
		} else if (element instanceof UserApplicationChild) {
			return ((UserApplicationChild) element).getName();
		} else if (element instanceof ProjectApplications) {
			return messages.projectApplicationsNode;
		} else if (element instanceof ProjectApplication) {
			return ((ProjectApplication) element).getName();
		} else if (element instanceof ProjectApplicationChild) {
			return ((ProjectApplicationChild) element).getName();
		} else if (element instanceof StartApplications) {
			return messages.startApplicationsNode;
		} else if (element instanceof StartApplication) {
			return ((StartApplication) element).getName();
		} else if (element instanceof BaseApplications) {
			return messages.baseApplicationsNode;
		} else if (element instanceof BaseApplication) {
			return ((BaseApplication) element).getName();
		} else if (element instanceof Groups) {
			return messages.groupsNode;
		} else if (element instanceof UserGroupsModel) {
			return messages.userGroupsNode;
		} else if (element instanceof UserGroupModel) {
			return ((UserGroupModel) element).getName();
		} else if (element instanceof ProjectGroupsModel) {
			return messages.projectGroupsNode;
		} else if (element instanceof ProjectGroupModel) {
			return ((ProjectGroupModel) element).getName();
		} else if (element instanceof ProjectGroupProjects) {
			return messages.projectsNode;
		} else if (element instanceof UserApplicationGroups) {
			return messages.userAppGroupsNode;
		} else if (element instanceof UserApplicationGroup) {
			return ((UserApplicationGroup) element).getName();
		} else if (element instanceof UserAppGroupUserApps) {
			return messages.userApplicationsNode;
		} else if (element instanceof ProjectApplicationGroups) {
			return messages.projectAppGroupsNode;
		} else if (element instanceof ProjectApplicationGroup) {
			return ((ProjectApplicationGroup) element).getName();
		} else if (element instanceof ProjectAppGroupProjectApps) {
			return messages.projectApplicationsNode;
		} else if (element instanceof Directories) {
			return messages.directoryNode;
		} else if (element instanceof Directory) {
			return ((Directory) element).getName();
		} else if (element instanceof DirectoryUsers) {
			return messages.usersNode;
		} else if (element instanceof DirectoryApplications) {
			return messages.applicationsNode;
		} else if (element instanceof DirectoryProjects) {
			return messages.projectsNode;
		} else if (element instanceof DirectoryUserApplications) {
			return messages.userApplicationsNode;
		} else if (element instanceof DirectoryProjectApplications) {
			return messages.projectApplicationsNode;
		} else if (element instanceof Configurations) {
			return messages.configurationNode;
		} else if (element instanceof Icons) {
			return messages.iconsNode;
		} else if (element instanceof Roles) {
			return messages.rolesNode;
		} else if (element instanceof Role) {
			return ((Role) element).getRoleName();
		} else if (element instanceof RoleUsers) {
			return messages.usersNode;
		} else if (element instanceof RoleScopeObjectUsers) {
			return messages.usersNode;
		} else if (element instanceof AdminMenu) {
			return messages.adminMenuNode;
		} else if (element instanceof AdminUsers) {
			return messages.usersNode;
		} else if (element instanceof AdminUserApps) {
			return messages.userApplicationsNode;
		} else if (element instanceof AdminProjectApps) {
			return messages.projectApplicationsNode;
		} else if (element instanceof SmtpConfiguration) {
			return messages.smtpConfigNode;
		} else if (element instanceof LdapConfiguration) {
			return messages.ldapConfigNode;
		} else if (element instanceof SingletonAppTimeConfig) {
			return messages.singletonAppTimeNode;
		} else if (element instanceof NoProjectPopMessage) {
			return messages.noPopMessageNode;
		} else if (element instanceof ProjectExpiryConfig) {
			return messages.projectExpiryNode;
		} else if (element instanceof LiveMessages) {
			return messages.liveMsgConfigNode;
		} else if (element instanceof LiveMessage) {
			return ((LiveMessage) element).getName();
		} else if (element instanceof Notification) {
			return messages.notificationNode;
		} else if (element instanceof Notifications) {
			return messages.notificationNode;
		} else if (element instanceof ProjectCreateEvt) {
			return NotificationEventType.PROJECT_CREATE.name();
		} else if (element instanceof ProjectCreateEvtAction) {
			return ((ProjectCreateEvtAction) element).getName();
		} else if (element instanceof ProjectDeleteEvt) {
			return NotificationEventType.PROJECT_DELETE.name();
		} else if (element instanceof ProjectDeleteEvtAction) {
			return ((ProjectDeleteEvtAction) element).getName();
		} else if (element instanceof ProjectDeactivateEvt) {
			return NotificationEventType.PROJECT_DEACTIVATE.name();
		} else if (element instanceof ProjectDeactivateEvtAction) {
			return ((ProjectDeactivateEvtAction) element).getName();
		}else if (element instanceof ProjectActivateEvt) {
			return NotificationEventType.PROJECT_ACTIVATE.name();
		} else if (element instanceof ProjectActivateEvtAction) {
			return ((ProjectActivateEvtAction) element).getName();
		} else if (element instanceof UserProjectRelAssignEvt) {
			return NotificationEventType.USER_PROJECT_RELATION_ASSIGN.name();
		} else if (element instanceof UserProjectRelAssignEvtAction) {
			return ((UserProjectRelAssignEvtAction) element).getName();
		} else if (element instanceof UserProjectRelRemoveEvt) {
			return NotificationEventType.USER_PROJECT_RELATION_REMOVE.name();
		} else if (element instanceof UserProjectRelRemoveEvtAction) {
			return ((UserProjectRelRemoveEvtAction) element).getName();
		} else if (element instanceof ProAARelAssignEvt) {
			return NotificationEventType.PROJECT_ADMIN_AREA_RELATION_ASSIGN.name();
		} else if (element instanceof ProAARelRemoveEvt) {
			return NotificationEventType.PROJECT_ADMIN_AREA_RELATION_REMOVE.name();
		} else if (element instanceof UserProExpEvt) {
			return NotificationEventType.USER_PROJECT_RELATION_EXPIRY.name();
		} else if (element instanceof UserProExpGraceEvt) {
			return NotificationEventType.USER_PROJECT_RELATION_GRACE_EXPIRY.name();
		} else if (element instanceof NotifyProjectUserEvt) {
			return NotificationEventType.EMAIL_NOTIFY_TO_PROJECT_USERS.name();
		} else if (element instanceof NotifyAAProjectUserEvt) {
			return NotificationEventType.EMAIL_NOTIFY_TO_AA_PROJECT_USERS.name();
		} else if (element instanceof NotificationTemplates) {
			return messages.templateNode;
		} else if (element instanceof NotificationTemplate) {
			return ((NotificationTemplate)element).getName();
		} else if (element instanceof RelationObj) {
			IAdminTreeChild refObject = ((RelationObj) element).getRefObject();
			if (refObject instanceof Site) {
				return ((Site) refObject).getName();
			} else if (refObject instanceof AdministrationArea) {
				return ((AdministrationArea) refObject).getName();
			} else if (refObject instanceof Project) {
				return ((Project) refObject).getName();
			} else if (refObject instanceof User) {
				return ((User) refObject).getName();
			} else if (refObject instanceof Directory) {
				return ((Directory) refObject).getName();
			} else if (refObject instanceof UserApplication) {
				String name = ((UserApplication) refObject).getName();
				return !XMSystemUtil.isEmpty(name) ? name : ((UserApplication) refObject).getName();
			} else if (refObject instanceof ProjectApplication) {
				String name = ((ProjectApplication) refObject).getName();
				return !XMSystemUtil.isEmpty(name) ? name : ((ProjectApplication) refObject).getName();
			} else if (refObject instanceof StartApplication) {
				String name = ((StartApplication) refObject).getName();
				return !XMSystemUtil.isEmpty(name) ? name : ((StartApplication) refObject).getName();
			} else if (refObject instanceof BaseApplication) {
				String name = ((BaseApplication) refObject).getName();
				return !XMSystemUtil.isEmpty(name) ? name : ((BaseApplication) refObject).getName();
			}
		}

		return super.getText(element.getClass() + " " + element.hashCode());
	}

	/**
	 * Get the image for object
	 * 
	 * @param element
	 *            {@link Object}
	 */
	@Override
	public Image getImage(final Object element) {
		Icon icon;
		try {
			XMSystemUtil instance = XMSystemUtil.getInstance();
			//FIXME 2017-10-11 SCP: Too many nested if-else-statements, extensively usage of instanceof 
			if (element instanceof TreeLoading) {
				return instance.getImage(this.getClass(), "icons/16x16/loading.png");
			} else if (element instanceof Sites) {
				return instance.getImage(this.getClass(), "icons/16x16/sites.png");
			} else if (element instanceof SiteAdministrations || element instanceof ProjectAdminAreas) {
				return instance.getImage(this.getClass(), "icons/16x16/administrationarea.png");
			} else if (element instanceof SiteAdminAreaProjects) {
				return instance.getImage(this.getClass(), "icons/16x16/projects.png");
			} else if (element instanceof SiteAdminAreaUserApplications) {
				return instance.getImage(this.getClass(), "icons/16x16/ausertasks.png");
			} else if (element instanceof UserProjects) {
				return instance.getImage(this.getClass(), "icons/16x16/projects.png");
			} else if (element instanceof UserProjectAdminAreas) {
				return instance.getImage(this.getClass(), "icons/16x16/administrationarea.png");
			} else if (element instanceof UserProjectAAProjectApplications) {
				return instance.getImage(this.getClass(), "icons/16x16/aprojecttasks.png");
			} else if (element instanceof UserProjectAAProjectAppAllowed) {
				return instance.getImage(this.getClass(), "icons/16x16/allowed.gif");
			} else if (element instanceof UserProjectAAProjectAppForbidden) {
				return instance.getImage(this.getClass(), "icons/16x16/dprojecttasks.gif");
			} else if (element instanceof UserAdminAreas) {
				return instance.getImage(this.getClass(), "icons/16x16/administrationarea.png");
			} else if (element instanceof UserAAUserApplications) {
				return instance.getImage(this.getClass(), "icons/16x16/ausertasks.png");
			} else if (element instanceof UserAAUserAppAllowed) {
				return instance.getImage(this.getClass(), "icons/16x16/ausertasks.png");
			} else if (element instanceof UserAAUserAppForbidden) {
				return instance.getImage(this.getClass(), "icons/16x16/dusertasks.gif");
			} else if (element instanceof UserStartApplications) {
				return instance.getImage(this.getClass(), "icons/16x16/startapplications.png");
			} else if (element instanceof UserGroups) {
				return instance.getImage(this.getClass(), "icons/16x16/groups.png");
			} else if (element instanceof UserInformations) {
				return instance.getImage(this.getClass(), "icons/16x16/info.gif");
			} else if (element instanceof UserInformationStatus) {
				return instance.getImage(this.getClass(), "icons/16x16/status.gif");
			} else if (element instanceof UserInformationHistory) {
				return instance.getImage(this.getClass(), "icons/16x16/history.gif");
			} else if (element instanceof SiteAdminAreaStartApplications) {
				return instance.getImage(this.getClass(), "icons/16x16/startapplications.png");
			} else if (element instanceof SiteAdminAreaInformations) {
				return instance.getImage(this.getClass(), "icons/16x16/info.gif");
			} else if (element instanceof SiteAdminAreaProjectApplications) {
				return instance.getImage(this.getClass(), "icons/16x16/aprojecttasks.png");
			} else if (element instanceof SiteAdminAreaProjectStartApplications) {
				return instance.getImage(this.getClass(), "icons/16x16/startapplications.png");
			} else if (element instanceof SiteAdminProjectAppNotFixed) {
				return instance.getImage(this.getClass(), "icons/16x16/notfixed.gif");
			} else if (element instanceof SiteAdminProjectAppFixed) {
				return instance.getImage(this.getClass(), "icons/16x16/fixed.gif");
			} else if (element instanceof SiteAdminProjectAppProtected) {
				return instance.getImage(this.getClass(), "icons/16x16/protected.gif");
			} else if (element instanceof SiteAdminAreaUserAppNotFixed) {
				return instance.getImage(this.getClass(), "icons/16x16/notfixed.gif");
			} else if (element instanceof SiteAdminAreaUserAppFixed) {
				return instance.getImage(this.getClass(), "icons/16x16/fixed.gif");
			} else if (element instanceof SiteAdminAreaInformationStatus) {
				return instance.getImage(this.getClass(), "icons/16x16/status.gif");
			} else if (element instanceof SiteAdminAreaInformationHistory) {
				return instance.getImage(this.getClass(), "icons/16x16/history.gif");
			} else if (element instanceof SiteAdminAreaUserAppProtected) {
				return instance.getImage(this.getClass(), "icons/16x16/protected.gif");
			} else if (element instanceof AdministrationAreas) {
				return instance.getImage(this.getClass(), "icons/16x16/administrationarea.png");
			} else if (element instanceof AdminAreaUserApplications) {
				return instance.getImage(this.getClass(), "icons/16x16/ausertasks.png");
			} else if (element instanceof AdminAreaUserAppFixed) {
				return instance.getImage(this.getClass(), "icons/16x16/fixed.gif");
			} else if (element instanceof AdminAreaUserAppNotFixed) {
				return instance.getImage(this.getClass(), "icons/16x16/notfixed.gif");
			} else if (element instanceof AdminAreaUserAppProtected) {
				return instance.getImage(this.getClass(), "icons/16x16/protected.gif");
			} else if (element instanceof AdminAreaProjectApplications) {
				return instance.getImage(this.getClass(), "icons/16x16/aprojecttasks.png");
			} else if (element instanceof AdminAreaProjectAppFixed) {
				return instance.getImage(this.getClass(), "icons/16x16/fixed.gif");
			} else if (element instanceof AdminAreaProjectAppNotFixed) {
				return instance.getImage(this.getClass(), "icons/16x16/notfixed.gif");
			} else if (element instanceof AdminAreaProjectAppProtected) {
				return instance.getImage(this.getClass(), "icons/16x16/protected.gif");
			} else if (element instanceof AdminAreaStartApplications) {
				return instance.getImage(this.getClass(), "icons/16x16/startapplications.png");
			} else if (element instanceof AdminAreaProjectStartApplications) {
				return instance.getImage(this.getClass(), "icons/16x16/startapplications.png");
			} else if (element instanceof AdminAreaProjects) {
				return instance.getImage(this.getClass(), "icons/16x16/projects.png");
			} else if (element instanceof AdminAreaInfo) {
				return instance.getImage(this.getClass(), "icons/16x16/info.gif");
			} else if (element instanceof AdminAreaInfoHistory) {
				return instance.getImage(this.getClass(), "icons/16x16/history.gif");
			} else if (element instanceof AdminAreaInfoStatus) {
				return instance.getImage(this.getClass(), "icons/16x16/status.gif");
			} else if (element instanceof AdminAreaStartApp) {
				return instance.getImage(this.getClass(), "icons/16x16/startapplications.png");
			} else if (element instanceof Users) {
				return instance.getImage(this.getClass(), "icons/16x16/users.png");
			} else if (element instanceof UsersNameAlphabet) {
				return instance.getImage(this.getClass(), "icons/16x16/users.png");
			} else if (element instanceof Projects) {
				return instance.getImage(this.getClass(), "icons/16x16/projects.png");
			} else if (element instanceof ProjectUsers) {
				return instance.getImage(this.getClass(), "icons/16x16/users.png");
			} else if (element instanceof ProjectUserAdminAreas) {
				return instance.getImage(this.getClass(), "icons/16x16/administrationarea.png");
			} else if (element instanceof ProjectUserAAProjectApplications) {
				return instance.getImage(this.getClass(), "icons/16x16/aprojecttasks.png");
			} else if (element instanceof ProjectUserAAProjectAppAllowed) {
				return instance.getImage(this.getClass(), "icons/16x16/allowed.gif");
			} else if (element instanceof ProjectUserAAProjectAppForbidden) {
				return instance.getImage(this.getClass(), "icons/16x16/dprojecttasks.gif");
			} else if (element instanceof ProUserProjectApp || element instanceof ProjectAdminAreaProjectApplications) {
				return instance.getImage(this.getClass(), "icons/16x16/aprojecttasks.png");
			} else if (element instanceof ProjectAdminAreaProjectAppNotFixed) {
				return instance.getImage(this.getClass(), "icons/16x16/notfixed.gif");
			} else if (element instanceof ProjectAdminAreaProjectAppFixed) {
				return instance.getImage(this.getClass(), "icons/16x16/fixed.gif");
			} else if (element instanceof ProjectAdminAreaProjectAppProtected) {
				return instance.getImage(this.getClass(), "icons/16x16/protected.gif");
			} else if (element instanceof ProjectAdminAreaStartApplications) {
				return instance.getImage(this.getClass(), "icons/16x16/startapplications.png");
			} else if (element instanceof ProjectProjectApplications) {
				return instance.getImage(this.getClass(), "icons/16x16/aprojecttasks.png");
			} else if (element instanceof ProjectInformation) {
				return instance.getImage(this.getClass(), "icons/16x16/info.gif");
			} else if (element instanceof ProjectInfoStatus) {
				return instance.getImage(this.getClass(), "icons/16x16/status.gif");
			} else if (element instanceof ProjectInfoHistory) {
				return instance.getImage(this.getClass(), "icons/16x16/history.gif");
			} else if (element instanceof Applications) {
				return instance.getImage(this.getClass(), "icons/16x16/applications.png");
			} else if (element instanceof UserApplications) {
				return instance.getImage(this.getClass(), "icons/16x16/ausertasks.png");
			} else if (element instanceof UserAppAdminAreas) {
				return instance.getImage(this.getClass(), "icons/16x16/administrationarea.png");
			} else if (element instanceof UserAppUsers) {
				return instance.getImage(this.getClass(), "icons/16x16/users.png");
			} else if (element instanceof UserAppGroups) {
				return instance.getImage(this.getClass(), "icons/16x16/groups.png");
			} else if (element instanceof ProjectApplications) {
				return instance.getImage(this.getClass(), "icons/16x16/aprojecttasks.png");
			} else if (element instanceof ProjectAppAdminAreas) {
				return instance.getImage(this.getClass(), "icons/16x16/administrationarea.png");
			} else if (element instanceof ProjectAppAdminAreaProjects) {
				return instance.getImage(this.getClass(), "icons/16x16/projects.png");
			} else if (element instanceof ProjectAppUsers) {
				return instance.getImage(this.getClass(), "icons/16x16/users.png");
			} else if (element instanceof ProjectAppGroups) {
				return instance.getImage(this.getClass(), "icons/16x16/groups.png");
			} else if (element instanceof StartApplications) {
				return instance.getImage(this.getClass(), "icons/16x16/startapplications.png");
			} else if (element instanceof StartAppAdminAreas) {
				return instance.getImage(this.getClass(), "icons/16x16/administrationarea.png");
			} else if (element instanceof StartAppProjects) {
				return instance.getImage(this.getClass(), "icons/16x16/projects.png");
			} else if (element instanceof StartAppUsers) {
				return instance.getImage(this.getClass(), "icons/16x16/users.png");
			} else if (element instanceof BaseApplications) {
				return instance.getImage(this.getClass(), "icons/16x16/tasks.png");
			} else if (element instanceof BaseAppProjectApplications) {
				return instance.getImage(this.getClass(), "icons/16x16/aprojecttasks.png");
			} else if (element instanceof BaseAppUserApplications) {
				return instance.getImage(this.getClass(), "icons/16x16/ausertasks.png");
			} else if (element instanceof BaseAppStartApplications) {
				return instance.getImage(this.getClass(), "icons/16x16/startapplications.png");
			} else if (element instanceof BaseAppInformationModel) {
				return instance.getImage(this.getClass(), "icons/16x16/info.gif");
			} else if (element instanceof BaseAppInformationStatus) {
				return instance.getImage(this.getClass(), "icons/16x16/status.gif");
			} else if (element instanceof BaseAppInformationHistory) {
				return instance.getImage(this.getClass(), "icons/16x16/history.gif");
			} else if (element instanceof Groups) {
				return instance.getImage(this.getClass(), "icons/16x16/groups.png");
			} else if (element instanceof UserGroupsModel) {
				return instance.getImage(this.getClass(), "icons/16x16/user_group.png");
			} else if (element instanceof ProjectGroupsModel) {
				return instance.getImage(this.getClass(), "icons/16x16/project_group.png");
			} else if (element instanceof ProjectGroupProjects) {
				return instance.getImage(this.getClass(), "icons/16x16/projects.png");
			} else if (element instanceof UserGroupUsers) {
				return instance.getImage(this.getClass(), "icons/16x16/users.png");
			} else if (element instanceof UserApplicationGroups) {
				return instance.getImage(this.getClass(), "icons/16x16/userapp_grp.png");
			} else if (element instanceof UserAppGroupUserApps) {
				return instance.getImage(this.getClass(), "icons/16x16/ausertasks.png");
			} else if (element instanceof ProjectApplicationGroups) {
				return instance.getImage(this.getClass(), "icons/16x16/projectapp_grp.png");
			} else if (element instanceof ProjectAppGroupProjectApps) {
				return instance.getImage(this.getClass(), "icons/16x16/aprojecttasks.png");
			} else if (element instanceof Directories) {
				return instance.getImage(this.getClass(), "icons/16x16/directory.png");
			} else if (element instanceof DirectoryUsers) {
				return instance.getImage(this.getClass(), "icons/16x16/users.png");
			} else if (element instanceof DirectoryProjects) {
				return instance.getImage(this.getClass(), "icons/16x16/projects.png");
			} else if (element instanceof DirectoryApplications) {
				return instance.getImage(this.getClass(), "icons/16x16/tasks.png");
			} else if (element instanceof DirectoryUserApplications) {
				return instance.getImage(this.getClass(), "icons/16x16/ausertasks.png");
			} else if (element instanceof DirectoryProjectApplications) {
				return instance.getImage(this.getClass(), "icons/16x16/aprojecttasks.png");
			} else if (element instanceof Configurations) {
				return instance.getImage(this.getClass(), "icons/16x16/configurations.png");
			} else if (element instanceof Icons) {
				return instance.getImage(this.getClass(), "icons/16x16/icons.png");
			} else if (element instanceof AdminMenu) {
				return instance.getImage(this.getClass(), "icons/16x16/superadmin.png");
			} else if (element instanceof AdminUsers) {
				return instance.getImage(this.getClass(), "icons/16x16/users.png");
			} else if (element instanceof AdminUserApps) {
				return instance.getImage(this.getClass(), "icons/16x16/ausertasks.png");
			} else if (element instanceof AdminProjectApps) {
				return instance.getImage(this.getClass(), "icons/16x16/aprojecttasks.png");
			} else if (element instanceof Roles) {
				return instance.getImage(this.getClass(), "icons/16x16/roles.png");
			} else if (element instanceof Role) {
				String roleName = ((Role) element).getRoleName();
				if (CommonConstants.SuperAdminRole.NAME.equalsIgnoreCase(roleName)) {
					return instance.getImage(this.getClass(), "icons/16x16/superadmin.png");
				}
				return instance.getImage(this.getClass(), "icons/16x16/roles.png");
			} else if (element instanceof RoleUsers) {
				return instance.getImage(this.getClass(), "icons/16x16/users.png");
			} else if (element instanceof RoleScopeObjectUsers) {
				return instance.getImage(this.getClass(), "icons/16x16/users.png");
			} else if (element instanceof RoleScopeObjects) {
				String name = ((RoleScopeObjects) element).getName();
				if (name.equals(AdministrationAreas.class.getName())) {
					return instance.getImage(this.getClass(), "icons/16x16/administrationarea.png");
				}
			} else if (element instanceof SmtpConfiguration) {
				return instance.getImage(this.getClass(), "icons/16x16/smtpconfig.png");
			} else if (element instanceof LdapConfiguration) {
				return instance.getImage(this.getClass(), "icons/16x16/ldapconfig.png");
			} else if (element instanceof SingletonAppTimeConfig) {
				return instance.getImage(this.getClass(), "icons/16x16/singletonapptimeconfig.png");
			} else if (element instanceof NoProjectPopMessage) {
				return instance.getImage(this.getClass(), "icons/16x16/noprojectpopupmsg.png");
			} else if (element instanceof ProjectExpiryConfig) {
				return instance.getImage(this.getClass(), "icons/16x16/projectexpirydaysconfig.png");
			} else if (element instanceof LiveMessages) {
				return instance.getImage(this.getClass(), "icons/16x16/livemessagesconfig.png");
			} else if (element instanceof LiveMessage) {
				return instance.getImage(this.getClass(), "icons/16x16/livemessagesconfig.png");
			} else if (element instanceof Notification) {
				return instance.getImage(this.getClass(), "icons/16x16/notification.png");
			} else if (element instanceof Notifications) {
				return instance.getImage(this.getClass(), "icons/16x16/notification.png");
			} else if (element instanceof ProjectCreateEvt) {
				return getIconObject("icons/16x16/pro_create_evt.png", ((ProjectCreateEvt) element).isActive());
			} else if (element instanceof ProjectCreateEvtAction) {
				return getIconObject("icons/16x16/pro_create_evt.png", ((ProjectCreateEvtAction) element).isActive());
			} else if (element instanceof ProjectDeleteEvt) {
				return getIconObject("icons/16x16/pro_delete_evt.png", ((ProjectDeleteEvt) element).isActive());
			} else if (element instanceof ProjectDeleteEvtAction) {
				return getIconObject("icons/16x16/pro_delete_evt.png", ((ProjectDeleteEvtAction) element).isActive());
			} else if (element instanceof ProjectActivateEvt) {
				return getIconObject("icons/16x16/pro_act_evt.png", ((ProjectActivateEvt) element).isActive());
			} else if (element instanceof ProjectActivateEvtAction) {
				return getIconObject("icons/16x16/pro_act_evt.png", ((ProjectActivateEvtAction) element).isActive());
			} else if (element instanceof ProjectDeactivateEvtAction) {
				return getIconObject("icons/16x16/pro_deact_evt.png", ((ProjectDeactivateEvtAction) element).isActive());
			} else if (element instanceof ProjectDeactivateEvt) {
				return getIconObject("icons/16x16/pro_deact_evt.png", ((ProjectDeactivateEvt) element).isActive());
			} else if (element instanceof UserProjectRelAssignEvt) {
				return getIconObject("icons/16x16/user_pro_assign.png", ((UserProjectRelAssignEvt) element).isActive());
			} else if (element instanceof UserProjectRelAssignEvtAction) {
				return getIconObject("icons/16x16/user_pro_assign.png", ((UserProjectRelAssignEvtAction) element).isActive());
			} else if (element instanceof UserProjectRelRemoveEvt) {
				return getIconObject("icons/16x16/user_pro_remove.png", ((UserProjectRelRemoveEvt) element).isActive());
			} else if (element instanceof UserProjectRelRemoveEvtAction) {
				return getIconObject("icons/16x16/user_pro_remove.png", ((UserProjectRelRemoveEvtAction) element).isActive());
			} else if (element instanceof ProAARelAssignEvt) {
				return getIconObject("icons/16x16/pro_aa_assign.png", ((ProAARelAssignEvt) element).isActive());
			} else if (element instanceof ProAARelRemoveEvt) {
				return getIconObject("icons/16x16/pro_aa_assign.png", ((ProAARelRemoveEvt) element).isActive());
			} else if (element instanceof UserProExpEvt) {
				return getIconObject("icons/16x16/pro_exp_evt.png", ((UserProExpEvt) element).isActive());
			} else if (element instanceof UserProExpGraceEvt) {
				return getIconObject("icons/16x16/pro_exp_gra_evt.png", ((UserProExpGraceEvt) element).isActive());
			} else if (element instanceof NotifyProjectUserEvt) {
				return getIconObject("icons/16x16/email_pro_user.png", ((NotifyProjectUserEvt) element).isActive());
			} else if (element instanceof NotifyAAProjectUserEvt) {
				return getIconObject("icons/16x16/email_aa_pro_user.png", ((NotifyAAProjectUserEvt) element).isActive());
			}else if (element instanceof NotificationTemplates) {
				return instance.getImage(this.getClass(), "icons/16x16/template.png");
			} else if (element instanceof NotificationTemplate) {
				return instance.getImage(this.getClass(), "icons/16x16/template.png");
			} else if (element instanceof Site && (icon = ((Site) element).getIcon()) != null) {
				return getIconObject(icon, ((Site) element).isActive());
			} else if (element instanceof AdministrationArea
					&& (icon = ((AdministrationArea) element).getIcon()) != null) {
				return getIconObject(icon, ((AdministrationArea) element).isActive());
			} else if (element instanceof User && (icon = ((User) element).getIcon()) != null) {
				return getIconObject(icon, ((User) element).isActive());
			} else if (element instanceof Project && (icon = ((Project) element).getIcon()) != null) {
				return getIconObject(icon, ((Project) element).isActive());
			} else if (element instanceof UserGroupModel && (icon = ((UserGroupModel) element).getIcon()) != null) {
				return getIconObject(icon, true);
			} else if (element instanceof ProjectGroupModel
					&& (icon = ((ProjectGroupModel) element).getIcon()) != null) {
				return getIconObject(icon, true);
			} else if (element instanceof UserApplicationGroup
					&& (icon = ((UserApplicationGroup) element).getIcon()) != null) {
				return getIconObject(icon, true);
			} else if (element instanceof ProjectApplicationGroup
					&& (icon = ((ProjectApplicationGroup) element).getIcon()) != null) {
				return getIconObject(icon, true);
			} else if (element instanceof Directory && (icon = ((Directory) element).getIcon()) != null) {
				return getIconObject(icon, true);
			} else if (element instanceof UserApplication && (icon = ((UserApplication) element).getIcon()) != null) {
				return getIconObject(icon, ((UserApplication) element).isActive());
			} else if (element instanceof UserApplicationChild
					&& (icon = ((UserApplicationChild) element).getIcon()) != null) {
				return getIconObject(icon, ((UserApplicationChild) element).isActive());
			} else if (element instanceof ProjectApplication
					&& (icon = ((ProjectApplication) element).getIcon()) != null) {
				return getIconObject(icon, ((ProjectApplication) element).isActive());
			} else if (element instanceof ProjectApplicationChild
					&& (icon = ((ProjectApplicationChild) element).getIcon()) != null) {
				return getIconObject(icon, ((ProjectApplicationChild) element).isActive());
			} else if (element instanceof StartApplication && (icon = ((StartApplication) element).getIcon()) != null) {
				return getIconObject(icon, ((StartApplication) element).isActive());
			} else if (element instanceof BaseApplication && (icon = ((BaseApplication) element).getIcon()) != null) {
				return getIconObject(icon, ((BaseApplication) element).isActive());
			} else if (element instanceof RelationObj) {
				IAdminTreeChild refObject = ((RelationObj) element).getRefObject();
				boolean relationStatus = ((RelationObj) element).isActive();
				if (refObject instanceof Site) {
					if ((icon = ((Site) refObject).getIcon()) != null) {
						return getIconObject(icon, relationStatus);
					}
				} else if (refObject instanceof AdministrationArea) {
					if ((icon = ((AdministrationArea) refObject).getIcon()) != null) {
						return getIconObject(icon, relationStatus);
					}
				} else if (refObject instanceof Project) {
					if ((icon = ((Project) refObject).getIcon()) != null) {
						return getIconObject(icon, relationStatus);
					}
				} else if (refObject instanceof User) {
					if ((icon = ((User) refObject).getIcon()) != null) {
						return getIconObject(icon, relationStatus);
					}
				} else if (refObject instanceof Directory) {
					if ((icon = ((Directory) refObject).getIcon()) != null) {
						return getIconObject(icon, relationStatus);
					}
				} else if (refObject instanceof UserApplication) {
					if ((icon = ((UserApplication) refObject).getIcon()) != null) {
						return getIconObject(icon, relationStatus);
					}
				} else if (refObject instanceof ProjectApplication) {
					if ((icon = ((ProjectApplication) refObject).getIcon()) != null) {
						return getIconObject(icon, relationStatus);
					}
				} else if (refObject instanceof StartApplication) {
					if ((icon = ((StartApplication) refObject).getIcon()) != null) {
						return getIconObject(icon, relationStatus);
					}
				} else if (refObject instanceof BaseApplication) {
					if ((icon = ((BaseApplication) refObject).getIcon()) != null) {
						return getIconObject(icon, relationStatus);
					}
				}
			}
		} catch (Exception e) {
			LOGGER.error("Exception occured in getImage method ! " + e);
		}
		return super.getImage(element);
	}

	/**
	 * Gets the icon object.
	 *
	 * @param icon
	 *            {@link Icon}
	 * @param active
	 *            {@link boolean}
	 * @return the icon object
	 */
	private Image getIconObject(Icon icon, boolean active) {
		Image image = null;
		if (icon != null) {
			String iconPath = icon.getIconPath();
			if (!XMSystemUtil.isEmpty(iconPath)) {
				image = XMSystemUtil.getInstance().getImage(this.getClass(), iconPath, active, true);
			}
		}
		return image;
	}
	
	/**
	 * Gets the icon object.
	 *
	 * @param iconPath the icon path
	 * @param active the active
	 * @return the icon object
	 */
	private Image getIconObject(String iconPath, boolean active) {
		Image image = null;
		if (!XMSystemUtil.isEmpty(iconPath)) {
			if (!XMSystemUtil.isEmpty(iconPath)) {
				image = XMSystemUtil.getInstance().getImage(this.getClass(), iconPath, active, false);
			}
		}
		return image;
	}
}
