package com.magna.xmsystem.xmadmin.message;

import javax.inject.Inject;


import org.eclipse.e4.core.di.annotations.Creatable;
import org.eclipse.e4.core.services.nls.BaseMessageRegistry;
import org.eclipse.e4.core.services.nls.Translation;

/**
 * Class for Message Registry
 * 
 * @author shashwat.anand
 *
 */
@Creatable
public class MessageRegistry extends BaseMessageRegistry<Message> {

	/**
	 * Overriding updateMessages
	 */
	@Inject
	@Override
	public void updateMessages(final @Translation Message messages) {
		super.updateMessages(messages);
	}
}
