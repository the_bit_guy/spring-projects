package com.magna.xmsystem.xmadmin.ui.handlers;

import javax.inject.Inject;

import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.core.services.nls.Translation;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.workbench.modeling.ESelectionService;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.TreeItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.ResourceAccessException;

import com.magna.xmsystem.dependencies.customExceptions.UnauthorizedAccessException;
import com.magna.xmsystem.ui.controls.widgets.CustomMessageDialog;
import com.magna.xmsystem.xmadmin.message.Message;
import com.magna.xmsystem.xmadmin.restclient.adminArea.AdminAreaController;
import com.magna.xmsystem.xmadmin.restclient.application.BaseAppController;
import com.magna.xmsystem.xmadmin.restclient.application.ProjectAppController;
import com.magna.xmsystem.xmadmin.restclient.application.StartAppController;
import com.magna.xmsystem.xmadmin.restclient.application.UserAppController;
import com.magna.xmsystem.xmadmin.restclient.project.ProjectController;
import com.magna.xmsystem.xmadmin.restclient.site.SiteController;
import com.magna.xmsystem.xmadmin.restclient.users.UserController;
import com.magna.xmsystem.xmadmin.ui.parts.IEditablePart;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.AdminTreeviewer;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdministrationArea;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.baseapplicationmodel.BaseApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.projectapplicationmodel.ProjectApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.startapplicationmodel.StartApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.userapplicationmodel.UserApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.Project;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.Site;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.User;
import com.magna.xmsystem.xmadmin.util.MessageType;
import com.magna.xmsystem.xmadmin.util.XMAdminUtil;

/**
 * Class for De-active menu handler.
 *
 * @author Chiranjeevi.Akula
 */
public class DeActiveMenuHandler {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(DeActiveMenuHandler.class);

	/** Member variable 'selection service' for {@link ESelectionService}. */
	@Inject
	private ESelectionService selectionService;

	/** Member variable 'messages' for {@link Message}. */
	@Inject
	@Translation
	private Message messages;

	/**
	 * Method for Execute.
	 */

	@Execute
	public void execute() {

		final Object selectionObj = selectionService.getSelection();
		if (selectionObj == null) {
			return;
		}
		try {
			if (XMAdminUtil.getInstance().isUpdateStatusAllowed(selectionObj)) {
				if (selectionObj instanceof IStructuredSelection) {
					Object firstElement = ((IStructuredSelection) selectionObj).getFirstElement();
					if (firstElement instanceof Site) {
						deActiveSite(firstElement);
					} else if (firstElement instanceof AdministrationArea) {
						deActiveAdminArea(firstElement);
					} else if (firstElement instanceof User) {
						deActiveUser(firstElement);
					} else if (firstElement instanceof Project) {
						deActiveProject(firstElement);
					} else if (firstElement instanceof UserApplication) {
						deActiveUserApplication(firstElement);
					} else if (firstElement instanceof ProjectApplication) {
						deActiveProjectApplication(firstElement);
					} else if (firstElement instanceof StartApplication) {
						deActiveStartApplication(firstElement);
					} else if (firstElement instanceof BaseApplication) {
						deActiveBaseApplication(firstElement);
					}
				}
			} else {
				CustomMessageDialog.openError(Display.getDefault().getActiveShell(), messages.objectPermissionDialogTitle,
						messages.objectPermissionDialogMsg);
			}
		} catch (UnauthorizedAccessException e) {
			LOGGER.error("Current user is Unauthorized " + e);
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				CustomMessageDialog.openError(Display.getCurrent().getActiveShell(), messages.errorDialogTitile,
						messages.serverNotReachable);
			}
			LOGGER.error("Exception occured in calling access allowed API " + e);
		}
	}

	/**
	 * Method for De active site.
	 *
	 * @param firstElement
	 *            {@link Object}
	 */
	private void deActiveSite(final Object firstElement) {
		try {
			String name = "";
			AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
			TreeItem[] selection = adminTree.getTree().getSelection();
			if (selection != null && selection.length > 0) {
				name = name.concat(selection[0].getText());
			}
			String confirmDialogTitle = messages.changeStatusConfirmDialogTitle;
			String confirmDialogMsg = messages.deActivatechangeStatusConfirmDialogMsg + " \'" + name + "\'" + " ?";

			boolean isConfirmed = CustomMessageDialog.openQuestion(adminTree.getControl().getShell(),
					confirmDialogTitle, confirmDialogMsg);
			Site siteModel = (Site) firstElement;

			if (isConfirmed) {
				String id = siteModel.getSiteId();
				String status = com.magna.xmbackend.vo.enums.Status.INACTIVE.name();
				SiteController siteCntr = new SiteController();
				boolean isUpdated = siteCntr.updateSiteStatus(id, status);
				if (isUpdated) {
					siteModel.setActive(false);
					adminTree.refresh(siteModel);
					adminTree.setSelection(new StructuredSelection(siteModel), true);
				}
				XMAdminUtil.getInstance().updateLogFile(messages.objectStatusUpdate + " " + messages.siteObject + " "
						+ "'" + siteModel.getName() + "'" + " " + messages.objectUpdate, MessageType.SUCCESS);
			}
			
		} catch (UnauthorizedAccessException e) {
			CustomMessageDialog.openError(Display.getDefault().getActiveShell(), messages.objectPermissionDialogTitle,
					messages.objectPermissionDialogMsg);
		} catch (Exception e) {

			LOGGER.warn("Unable to update data! ", e);
		}
	}

	/**
	 * Method for De active admin area.
	 *
	 * @param firstElement
	 *            {@link Object}
	 */
	private void deActiveAdminArea(final Object firstElement) {
		try {
			String name = "";
			AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
			TreeItem[] selection = adminTree.getTree().getSelection();
			if (selection != null && selection.length > 0) {
				name = name.concat(selection[0].getText());
			}
			String confirmDialogTitle = messages.changeStatusConfirmDialogTitle;
			String confirmDialogMsg = messages.deActivatechangeStatusConfirmDialogMsg + " \'" + name + "\'" + " ?";

			boolean isConfirmed = CustomMessageDialog.openQuestion(adminTree.getControl().getShell(),
					confirmDialogTitle, confirmDialogMsg);
			AdministrationArea adminAreaModel = (AdministrationArea) firstElement;

			if (isConfirmed) {
				String id = adminAreaModel.getAdministrationAreaId();
				String status = com.magna.xmbackend.vo.enums.Status.INACTIVE.name();
				AdminAreaController adminAreaCntr = new AdminAreaController();
				boolean isUpdated = adminAreaCntr.updateAdminAreaStatus(id, status);
				if (isUpdated) {
					adminAreaModel.setActive(false);
					adminTree.refresh(adminAreaModel);
					adminTree.setSelection(new StructuredSelection(adminAreaModel), true);
				}
				XMAdminUtil.getInstance()
						.updateLogFile(
								messages.objectStatusUpdate + " " + messages.administrationAreaObject + " " + "'"
										+ adminAreaModel.getName() + "'" + " " + messages.objectUpdate,
								MessageType.SUCCESS);
			}
		} catch (UnauthorizedAccessException e) {
			CustomMessageDialog.openError(Display.getDefault().getActiveShell(), messages.objectPermissionDialogTitle,
					messages.objectPermissionDialogMsg);
		} catch (Exception e) {
			LOGGER.warn("Unable to update data! ", e);
		}
	}

	/**
	 * Method for De active user.
	 *
	 * @param firstElement
	 *            {@link Object}
	 */
	private void deActiveUser(Object firstElement) {
		try {
			String name = "";
			AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
			TreeItem[] selection = adminTree.getTree().getSelection();
			if (selection != null && selection.length > 0) {
				name = name.concat(selection[0].getText());
			}
			String confirmDialogTitle = messages.changeStatusConfirmDialogTitle;
			String confirmDialogMsg = messages.deActivatechangeStatusConfirmDialogMsg + " \'" + name + "\'" + " ?";

			boolean isConfirmed = CustomMessageDialog.openQuestion(adminTree.getControl().getShell(),
					confirmDialogTitle, confirmDialogMsg);
			User userModel = (User) firstElement;

			if (isConfirmed) {
				String id = userModel.getUserId();
				String status = com.magna.xmbackend.vo.enums.Status.INACTIVE.name();
				UserController userController = new UserController();
				boolean isUpdated = userController.updateUserStatus(id, status);
				if (isUpdated) {
					userModel.setActive(false);
					adminTree.refresh(userModel);
					adminTree.setSelection(new StructuredSelection(userModel), true);
				}
				XMAdminUtil.getInstance().updateLogFile(messages.objectStatusUpdate + " " + messages.userObject + " "
						+ "'" + userModel.getName() + "'" + " " + messages.objectUpdate,MessageType.SUCCESS);
			}
			adminTree.refresh(userModel);
			adminTree.setSelection(new StructuredSelection(userModel), true);
		} catch (UnauthorizedAccessException e) {
			CustomMessageDialog.openError(Display.getDefault().getActiveShell(), messages.objectPermissionDialogTitle,
					messages.objectPermissionDialogMsg);
		} catch (Exception e) {
			LOGGER.warn("Unable to update data! ", e);
		}
	}

	/**
	 * Method for De active project.
	 *
	 * @param firstElement
	 *            {@link Object}
	 */
	private void deActiveProject(Object firstElement) {
		try {
			String name = "";
			AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
			TreeItem[] selection = adminTree.getTree().getSelection();
			if (selection != null && selection.length > 0) {
				name = name.concat(selection[0].getText());
			}
			String confirmDialogTitle = messages.changeStatusConfirmDialogTitle;
			String confirmDialogMsg = messages.deActivatechangeStatusConfirmDialogMsg + " \'" + name + "\'" + " ?";

			boolean isConfirmed = CustomMessageDialog.openQuestion(adminTree.getControl().getShell(),
					confirmDialogTitle, confirmDialogMsg);
			Project projectModel = (Project) firstElement;

			if (isConfirmed) {
				String id = projectModel.getProjectId();
				String status = com.magna.xmbackend.vo.enums.Status.INACTIVE.name();
				ProjectController projectCntr = new ProjectController();
				boolean isRelationDeleted = false;
				try {
					isRelationDeleted = projectCntr.updateProjectStatus(id, status);
				} catch (UnauthorizedAccessException e) {
					CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
							messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
					return;
				}
				if (isRelationDeleted) {
					projectModel.setActive(false);
					adminTree.refresh(projectModel);
					adminTree.setSelection(new StructuredSelection(projectModel), true);
					XMAdminUtil.getInstance().updateLogFile(
							messages.objectStatusUpdate + " " + messages.projectObject + " " + "'"
									+ projectModel.getName() + "'" + " " + messages.objectUpdate,
							MessageType.SUCCESS);
				}
			}
		} catch (UnauthorizedAccessException e) {
			CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
					messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
		} catch (Exception e) {
			LOGGER.warn("Unable to update data! ", e);
		}

	}

	/**
	 * Method for De active group.
	 *
	 * @param firstElement
	 *            {@link Object}
	 */
/*	private void deActiveGroup(Object firstElement) {
		String name = "";
		AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
		TreeItem[] selection = adminTree.getTree().getSelection();
		if (selection != null && selection.length > 0) {
			name = name.concat(selection[0].getText());
		}
		String confirmDialogTitle = messages.changeStatusConfirmDialogTitle;
		String confirmDialogMsg = messages.deActivatechangeStatusConfirmDialogMsg + " \'" + name + " \'" + " ?";

		boolean isConfirmed = CustomMessageDialog.openQuestion(adminTree.getControl().getShell(), confirmDialogTitle,
				confirmDialogMsg);
		GroupModel groupModel = (GroupModel) firstElement;

		if (isConfirmed) {
			groupModel.setActive(false);
		}

		adminTree.refresh(groupModel);
		adminTree.setSelection(new StructuredSelection(groupModel), true);
	}*/

	/**
	 * Method for De active user application.
	 *
	 * @param firstElement
	 *            {@link Object}
	 */
	private void deActiveUserApplication(Object firstElement) {
		try {
			String name = "";
			AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
			TreeItem[] selection = adminTree.getTree().getSelection();
			if (selection != null && selection.length > 0) {
				name = name.concat(selection[0].getText());
			}
			String confirmDialogTitle = messages.changeStatusConfirmDialogTitle;
			String confirmDialogMsg = messages.deActivatechangeStatusConfirmDialogMsg + " \'" + name + "\'" + " ?";

			boolean isConfirmed = CustomMessageDialog.openQuestion(adminTree.getControl().getShell(),
					confirmDialogTitle, confirmDialogMsg);
			UserApplication userAppModel = (UserApplication) firstElement;

			if (isConfirmed) {
				String id = userAppModel.getUserApplicationId();
				String status = com.magna.xmbackend.vo.enums.Status.INACTIVE.name();
				UserAppController userAppCntr = new UserAppController();
				boolean isUpdated = false;
				try {
					isUpdated = userAppCntr.updateUserAppStatus(id, status);
				} catch (UnauthorizedAccessException e) {
					CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
							messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
					return;
				}
				if (isUpdated) {
					userAppModel.setActive(false);
					adminTree.refresh(userAppModel);
					adminTree.setSelection(new StructuredSelection(userAppModel), true);
					XMAdminUtil.getInstance().updateLogFile(
							messages.objectStatusUpdate + " " + messages.userApplicationObject + " " + "'"
									+ userAppModel.getName() + "'" + " " + messages.objectUpdate,
							MessageType.SUCCESS);
				}
			}
		}  catch (UnauthorizedAccessException e) {
			CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
					messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
		} catch (Exception e) {
			LOGGER.warn("Unable to update data! ", e);
		}
	}

	/**
	 * Method for De active project application.
	 *
	 * @param firstElement
	 *            {@link Object}
	 */
	private void deActiveProjectApplication(Object firstElement) {
		try {
			String name = "";
			AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
			TreeItem[] selection = adminTree.getTree().getSelection();
			if (selection != null && selection.length > 0) {
				name = name.concat(selection[0].getText());
			}

			String confirmDialogTitle = messages.changeStatusConfirmDialogTitle;
			String confirmDialogMsg = messages.deActivatechangeStatusConfirmDialogMsg + " \'" + name + "\'" + " ?";

			boolean isConfirmed = CustomMessageDialog.openQuestion(adminTree.getControl().getShell(),
					confirmDialogTitle, confirmDialogMsg);
			ProjectApplication projectApplication = (ProjectApplication) firstElement;

			if (isConfirmed) {
				String id = projectApplication.getProjectApplicationId();
				String status = com.magna.xmbackend.vo.enums.Status.INACTIVE.name();
				ProjectAppController projectAppController = new ProjectAppController();
				boolean isUpdated = false;

				isUpdated = projectAppController.updateProjectAppStatus(id, status);

				if (isUpdated) {
					projectApplication.setActive(false);
					adminTree.refresh(projectApplication);
					adminTree.setSelection(new StructuredSelection(projectApplication), true);
					XMAdminUtil.getInstance().updateLogFile(
							messages.objectStatusUpdate + " " + messages.projectApplicationObject + " " + "'"
									+ projectApplication.getName() + "'" + " " + messages.objectUpdate,
							MessageType.SUCCESS);
				}
			}
		} catch (UnauthorizedAccessException e) {
			CustomMessageDialog.openError(Display.getDefault().getActiveShell(), messages.objectPermissionDialogTitle,
					messages.objectPermissionDialogMsg);
		} catch (Exception e) {
			LOGGER.warn("Unable to update Start Application data! ", e);
		}
	}

	/**
	 * Method for De active start application.
	 *
	 * @param firstElement
	 *            {@link Object}
	 */
	private void deActiveStartApplication(Object firstElement) {
		try {
			String name = "";
			AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
			TreeItem[] selection = adminTree.getTree().getSelection();
			if (selection != null && selection.length > 0) {
				name = name.concat(selection[0].getText());
			}
			String confirmDialogTitle = messages.changeStatusConfirmDialogTitle;
			String confirmDialogMsg = messages.deActivatechangeStatusConfirmDialogMsg + " \'" + name + "\'" + " ?";

			boolean isConfirmed = CustomMessageDialog.openQuestion(adminTree.getControl().getShell(),
					confirmDialogTitle, confirmDialogMsg);
			StartApplication startAppModel = (StartApplication) firstElement;

			if (isConfirmed) {
				String id = startAppModel.getStartPrgmApplicationId();
				String status = com.magna.xmbackend.vo.enums.Status.INACTIVE.name();
				StartAppController startAppCntr = new StartAppController();
				boolean isUpdated = startAppCntr.updateStartAppStatus(id, status);
				if (isUpdated) {
					startAppModel.setActive(false);
					adminTree.refresh(startAppModel);
					adminTree.setSelection(new StructuredSelection(startAppModel), true);
					XMAdminUtil.getInstance().updateLogFile(
							messages.objectStatusUpdate + " " + messages.startApplicationObject + " " + "'"
									+ startAppModel.getName() + "'" + " " + messages.objectUpdate,
							MessageType.SUCCESS);
				}
			}
		}  catch (UnauthorizedAccessException e) {
			CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
					messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
		} catch (Exception e) {
			LOGGER.warn("Unable to update Start Application data! ", e);
		}
	}

	/**
	 * Method for De active base application.
	 *
	 * @param firstElement
	 *            {@link Object}
	 */
	private void deActiveBaseApplication(Object firstElement) {
		try {
			String name = "";
			AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
			TreeItem[] selection = adminTree.getTree().getSelection();
			if (selection != null && selection.length > 0) {
				name = name.concat(selection[0].getText());
			}

			String confirmDialogTitle = messages.changeStatusConfirmDialogTitle;
			String confirmDialogMsg = messages.deActivatechangeStatusConfirmDialogMsg + " \'" + name + "\'" + " ?";

			boolean isConfirmed = CustomMessageDialog.openQuestion(adminTree.getControl().getShell(),
					confirmDialogTitle, confirmDialogMsg);
			BaseApplication baseApplication = (BaseApplication) firstElement;

			if (isConfirmed) {
				String id = baseApplication.getBaseApplicationId();
				String status = com.magna.xmbackend.vo.enums.Status.INACTIVE.name();
				BaseAppController baseAppController = new BaseAppController();
				boolean isUpdated = baseAppController.updateBaseAppStatus(id, status);
				;

				if (isUpdated) {
					baseApplication.setActive(false);
					adminTree.refresh(baseApplication);
					adminTree.setSelection(new StructuredSelection(baseApplication), true);
					XMAdminUtil.getInstance().updateLogFile(
							messages.objectStatusUpdate + " " + messages.baseApplicationObject + " " + "'"
									+ baseApplication.getName() + "'" + " " + messages.objectUpdate,
							MessageType.SUCCESS);
				}
			}
		} catch (UnauthorizedAccessException e) {
			CustomMessageDialog.openError(Display.getDefault().getActiveShell(), messages.objectPermissionDialogTitle,
					messages.objectPermissionDialogMsg);
			return;
		} catch (Exception e) {
			LOGGER.warn("Unable to update Start Application data! ", e);
		}
	}

	/**
	 * Method for Can execute.
	 *
	 * @return true, if successful
	 */
	@CanExecute
	public boolean canExecute() {
		boolean returnVal = true;
		final XMAdminUtil instance = XMAdminUtil.getInstance();
		MPart rightPart;
		Object rightView;
		if ((rightPart = instance.getInformationPart()) != null
				&& (rightView = rightPart.getObject()) != null && rightView instanceof IEditablePart
				&& ((IEditablePart) rightView).isDirty()) {
			returnVal = false;
		}
		return returnVal;
	}
		/*final Object selectionObj = selectionService.getSelection();
		if (selectionObj == null) {
			return false;
		}
		boolean returnVal = true;
		try {
			final ValidationController controller = new ValidationController();
			final ValidationRequest request = new ValidationRequest();
			request.setUserName(RestClientUtil.getInstance().getUserName());
			request.setPermissionType(PermissionType.OBJECT_PERMISSION.name());
			if (selectionObj instanceof IStructuredSelection) {
				Object firstElement = ((IStructuredSelection) selectionObj).getFirstElement();
				if (firstElement instanceof Site) {
					request.setPermissionName("SITE-ACTIVATE_DEACTIVATE"); //$NON-NLS-1$
				} else if (firstElement instanceof AdministrationArea) {
					request.setPermissionName("ADMINISTRATION_AREA-ACTIVATE_DEACTIVATE");
				} else if (firstElement instanceof User) {
					request.setPermissionName("USER-ACTIVATE_DEACTIVATE");
				} else if (firstElement instanceof Project) {
					request.setPermissionName("PROJECT-ACTIVATE_DEACTIVATE");
				} else if (firstElement instanceof UserApplication) {
					request.setPermissionName("USER_APPLICATION-ACTIVATE_DEACTIVATE");
				} else if (firstElement instanceof ProjectApplication) {
					request.setPermissionName("PROJECT_APPLICATION-ACTIVATE_DEACTIVATE");
				} else if (firstElement instanceof StartApplication) {
					request.setPermissionName("START_APPLICATION-ACTIVATE_DEACTIVATE");
				} else if (firstElement instanceof BaseApplication) {
					request.setPermissionName("BASE_APPLICATION-ACTIVATE_DEACTIVATE");
				}
			}
			returnVal = controller.getAccessAllowed(request);
		} catch (UnauthorizedAccessException e) {
			LOGGER.error("Current user is Unauthorized " + e);
			returnVal = false;
		} catch (Exception e) {
			LOGGER.error("Exception occured in calling access allowed API " + e);
			returnVal = false;
		}
		if (returnVal) {
			final XMAdminUtil instance = XMAdminUtil.getInstance();
			MPart rightPart;
			Object rightView;
			if ((rightPart = instance.getRightHandMPartOfOldPerspective()) != null
					&& (rightView = rightPart.getObject()) != null && rightView instanceof IEditablePart
					&& ((IEditablePart) rightView).isDirty()) {
				returnVal = false;
			}
		}

		return returnVal;
	}*/
}
