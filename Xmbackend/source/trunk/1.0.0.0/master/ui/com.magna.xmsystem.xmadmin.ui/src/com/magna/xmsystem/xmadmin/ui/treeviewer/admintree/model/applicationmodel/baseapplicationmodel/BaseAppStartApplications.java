package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.baseapplicationmodel;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.startapplicationmodel.StartApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.refs.RelationObj;

/**
 * The Class BaseAppStartAppModel.
 */
public class BaseAppStartApplications implements IAdminTreeChild {

	/** The parent. */
	private IAdminTreeChild parent;

	/** The base app start app children. */
	private Map<String, IAdminTreeChild> baseAppStartAppChildren;

	/**
	 * Instantiates a new base app start app model.
	 *
	 * @param parent the parent
	 */
	public BaseAppStartApplications(IAdminTreeChild parent) {
		super();
		this.parent = parent;
		this.baseAppStartAppChildren = new LinkedHashMap<>();
	}
	
	/**
	 * Adds the.
	 *
	 * @param baseAppStartAppChildrenId the base app start app children id
	 * @param child the child
	 * @return the i admin tree child
	 */
	public IAdminTreeChild add(final String baseAppStartAppChildrenId, final IAdminTreeChild child) {
		child.setParent(this);
		IAdminTreeChild returnVal = this.baseAppStartAppChildren.put(baseAppStartAppChildrenId, child);
		if (child instanceof RelationObj && ((RelationObj) child).getRefObject() instanceof StartApplication) {
			sort();
		}
		return returnVal;
	}

	/**
	 * Removes the.
	 *
	 * @param baseAppStartAppChildrenId the base app start app children id
	 * @return the i admin tree child
	 */
	public IAdminTreeChild remove(final String baseAppStartAppChildrenId) {
		return this.baseAppStartAppChildren.remove(baseAppStartAppChildrenId);
	}

	/**
	 * Gets the base app start app collection.
	 *
	 * @return the base app start app collection
	 */
	public Collection<IAdminTreeChild> getBaseAppStartAppCollection() {
		return this.baseAppStartAppChildren.values();
	}

	/**
	 * Gets the base app start app children.
	 *
	 * @return the base app start app children
	 */
	public Map<String, IAdminTreeChild> getBaseAppStartAppChildren() {
		return baseAppStartAppChildren;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#
	 * getParent()
	 */
	@Override
	public IAdminTreeChild getParent() {
		return this.parent;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#
	 * setParent(com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.
	 * IAdminTreeChild)
	 */
	@Override
	public void setParent(IAdminTreeChild parent) {
		this.parent = parent;

	}
	
	/**
	 * Sort.
	 */
	public void sort() {
		final LinkedHashMap<String, IAdminTreeChild> collect = this.baseAppStartAppChildren.entrySet().stream().sorted(
				(e1, e2) -> ((StartApplication) e1.getValue()).getName().compareTo(((StartApplication) e2.getValue()).getName()))
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
		this.baseAppStartAppChildren = collect;
	}

}
