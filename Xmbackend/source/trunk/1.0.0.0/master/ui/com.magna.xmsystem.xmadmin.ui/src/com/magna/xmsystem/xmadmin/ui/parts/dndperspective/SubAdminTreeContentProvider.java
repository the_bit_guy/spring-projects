package com.magna.xmsystem.xmadmin.ui.parts.dndperspective;

import org.eclipse.jface.viewers.ITreeContentProvider;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaInfo;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaProjectApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaProjectChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaProjects;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaUserApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdministrationArea;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdministrationAreas;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.Applications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.baseapplicationmodel.BaseAppInformationModel;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.baseapplicationmodel.BaseApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.baseapplicationmodel.BaseApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.projectapplicationmodel.ProjectAppAdminAreaChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.projectapplicationmodel.ProjectAppAdminAreas;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.projectapplicationmodel.ProjectApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.projectapplicationmodel.ProjectApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.startapplicationmodel.StartApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.startapplicationmodel.StartApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.userapplicationmodel.UserApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.userapplicationmodel.UserApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.Configurations;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.adminconf.AdminMenu;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.livemsgconfig.LiveMessages;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.Role;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.RoleScopeObjectChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.RoleScopeObjects;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.Roles;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.directory.Directories;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.directory.Directory;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.directory.DirectoryApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.Groups;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.projectappgroup.ProjectApplicationGroup;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.projectappgroup.ProjectApplicationGroups;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.projectgroup.ProjectGroupModel;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.projectgroup.ProjectGroupsModel;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.userappgroup.UserApplicationGroup;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.userappgroup.UserApplicationGroups;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.usergroup.UserGroupModel;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.usergroup.UserGroupsModel;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProUserProjectApp;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.Project;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectAdminAreaChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectAdminAreaProjectApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectAdminAreas;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectInformation;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectUserAAProjectApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectUserAdminAreaChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectUserAdminAreas;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectUserChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectUsers;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.Projects;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.refs.RelationObj;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.Site;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaInformations;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaProjectApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaProjectChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaProjects;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaUserApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdministrationChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdministrations;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.Sites;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.User;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserAAUserApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserAdminAreaChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserAdminAreas;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserInformations;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserProjectAAProjectApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserProjectAdminAreaChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserProjectAdminAreas;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserProjectChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserProjects;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.Users;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UsersNameAlphabet;

/**
 * The Class SubAdminTreeContentProvider.
 * 
 * @author shashwat.anand
 */
public class SubAdminTreeContentProvider implements ITreeContentProvider {

	/* (non-Javadoc)
	 * @see org.eclipse.jface.viewers.ITreeContentProvider#getElements(java.lang.Object)
	 */
	@Override
	public Object[] getElements(Object inputElement) {
		if (inputElement instanceof Object[]) {
			return (Object[]) inputElement;
		}
		return new Object[] {};
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.viewers.ITreeContentProvider#getChildren(java.lang.Object)
	 */
	@Override
	public Object[] getChildren(final Object parentElement) {
		if (parentElement instanceof Sites) {
			return ((Sites) parentElement).getSitesCollection().toArray();
		} else if (parentElement instanceof AdministrationAreas) {
			return ((AdministrationAreas) parentElement).getAdministrationAreasCollection().toArray();
		} else if (parentElement instanceof SiteAdministrations) {
			return ((SiteAdministrations) parentElement).getSiteAdministrationAreasCollection().toArray();
		} else if (parentElement instanceof AdminAreaUserApplications) {
			return ((AdminAreaUserApplications) parentElement).getAdminAreaUserAppCollection().toArray();
		} else if (parentElement instanceof AdminAreaProjects) {
			return ((AdminAreaProjects) parentElement).getAdminAreasProjectsCollection().toArray();
		} else if (parentElement instanceof AdminAreaProjectApplications) {
			return ((AdminAreaProjectApplications) parentElement).getAdminAreaProjectAppChildCollection().toArray();
		} else if (parentElement instanceof AdministrationArea) {
			return ((AdministrationArea) parentElement).getAdminAreasCollection().toArray();
		} else if (parentElement instanceof AdminAreaInfo) {
			return ((AdminAreaInfo) parentElement).getAdminAreaInfoCollection().toArray();
		} else if (parentElement instanceof Users) {
			return ((Users) parentElement).getUsersCollection().toArray();
		} else if (parentElement instanceof UsersNameAlphabet) {
			return ((UsersNameAlphabet) parentElement).getUsersCollection().toArray();
		} else if (parentElement instanceof User) {
			return ((User) parentElement).getUserChildren().values().toArray();
		} else if (parentElement instanceof UserProjects) {
			return ((UserProjects) parentElement).getUserProjectsCollection().toArray();
		} else if (parentElement instanceof UserProjectAdminAreas) {
			return ((UserProjectAdminAreas) parentElement).getUserProjectAdminAreaChildCollection().toArray();
		} else if (parentElement instanceof UserAdminAreas) {
			return ((UserAdminAreas) parentElement).getUserAdminAreaChildCollection().toArray();
		} else if (parentElement instanceof UserProjectAAProjectApplications) {
			return ((UserProjectAAProjectApplications) parentElement).getUserProjectAppChildCollection().toArray();
		} else if (parentElement instanceof UserAAUserApplications) {
			return ((UserAAUserApplications) parentElement).getUserAAUserAppChildrenCollection().toArray();
		} else if (parentElement instanceof UserInformations) {
			return ((UserInformations) parentElement).getUserInformationChildCollection().toArray();
		} else if (parentElement instanceof Projects) {
			return ((Projects) parentElement).getProjectsCollection().toArray();
		} else if (parentElement instanceof Applications) {
			return ((Applications) parentElement).getAppCollection().toArray();
		} else if (parentElement instanceof UserApplications) {
			return ((UserApplications) parentElement).getUserAppCollection().toArray();
		} else if (parentElement instanceof UserApplication) {
			return ((UserApplication) parentElement).getUserApplicationChildren().values().toArray();
		} else if (parentElement instanceof ProjectApplications) {
			return ((ProjectApplications) parentElement).getProjectAppCollection().toArray();
		} else if (parentElement instanceof StartApplications) {
			return ((StartApplications) parentElement).getStartAppCollection().toArray();
		} else if (parentElement instanceof BaseApplications) {
			return ((BaseApplications) parentElement).getBaseAppCollection().toArray();
		} else if (parentElement instanceof Groups) {
			return ((Groups) parentElement).getGroupsCollection().toArray();
		} else if (parentElement instanceof UserGroupsModel) {
			return ((UserGroupsModel) parentElement).getUserGroupsCollection().toArray();
		} else if (parentElement instanceof ProjectGroupsModel) {
			return ((ProjectGroupsModel) parentElement).getProjectGroupsCollection().toArray();
		} else if (parentElement instanceof UserGroupModel) {
			return ((UserGroupModel) parentElement).getUserGroupChildren().values().toArray();
		} else if (parentElement instanceof ProjectGroupModel) {
			return ((ProjectGroupModel) parentElement).getProjectGroupChildren().values().toArray();
		} else if (parentElement instanceof UserApplicationGroups) {
			return ((UserApplicationGroups) parentElement).getUserAppGroupsChildren().values().toArray();
		} else if (parentElement instanceof UserApplicationGroup) {
			return ((UserApplicationGroup) parentElement).getUserAppGroupChildren().values().toArray();
		} else if (parentElement instanceof ProjectApplicationGroups) {
			return ((ProjectApplicationGroups) parentElement).getProjectAppGroupsChildren().values().toArray();
		} else if (parentElement instanceof ProjectApplicationGroup) {
			return ((ProjectApplicationGroup) parentElement).getProjectAppGroupChildren().values().toArray();
		}else if (parentElement instanceof Directories) {
			return ((Directories) parentElement).getDirectoriesCollection().toArray();
		} else if (parentElement instanceof Directory) {
			return ((Directory) parentElement).getDirectoryChildren().values().toArray();
		} else if (parentElement instanceof DirectoryApplications) {
			return ((DirectoryApplications) parentElement).getDirectoryApplicationsChildrenChildren().values().toArray();
		} else if (parentElement instanceof Configurations) {
			return ((Configurations) parentElement).getAllConfigurations().toArray();
		} else if (parentElement instanceof Roles) {
			return ((Roles) parentElement).getRolesCollection().toArray();
		} else if (parentElement instanceof LiveMessages) {
			return ((LiveMessages) parentElement).getLiveMsgsCollection().toArray();
		}else if (parentElement instanceof Site) {
			return ((Site) parentElement).getSiteChildren().values().toArray();
		} else if (parentElement instanceof SiteAdministrations) {
			return ((SiteAdministrations) parentElement).getSiteAdministrationAreasCollection().toArray();
		} else if (parentElement instanceof SiteAdminAreaProjects) {
			return ((SiteAdminAreaProjects) parentElement).getSiteAdminProjectsCollection().toArray();
		} else if (parentElement instanceof SiteAdminAreaProjectApplications) {
			return ((SiteAdminAreaProjectApplications) parentElement).getSiteAdminAreaChildProAppCollection().toArray();
		} else if (parentElement instanceof SiteAdminAreaUserApplications) {
			return ((SiteAdminAreaUserApplications) parentElement).getSiteAdminAreaUserAppCollection().toArray();
		} else if (parentElement instanceof SiteAdminAreaInformations) {
			return ((SiteAdminAreaInformations) parentElement).getSiteAdminInfoCollection().toArray();
		} else if (parentElement instanceof Project) {
			return ((Project) parentElement).getProjectChildren().values().toArray();
		} else if (parentElement instanceof ProjectInformation) {
			return ((ProjectInformation) parentElement).getProjectInfoChildCollection().toArray();
		} else if (parentElement instanceof ProjectUsers) {
			return ((ProjectUsers) parentElement).getProjectUserCollection().toArray();
		} else if (parentElement instanceof ProjectAdminAreas) {
			return ((ProjectAdminAreas) parentElement).getProjectAdminAreaCollection().toArray();
		} else if (parentElement instanceof ProjectUserAdminAreas) {
			return ((ProjectUserAdminAreas) parentElement).getProjectUserAdminAreaChildCollection().toArray();
		} else if (parentElement instanceof ProjectUserAAProjectApplications) {
			return ((ProjectUserAAProjectApplications) parentElement).getProjectUserAAProjAppChildCollection().toArray();
		} else if (parentElement instanceof ProjectAdminAreaProjectApplications) {
			return ((ProjectAdminAreaProjectApplications) parentElement).getProjectAdminAreaProjectAppCollection().toArray();
		} else if (parentElement instanceof AdminMenu) {
			return ((AdminMenu) parentElement).getAdminMenuCollection().toArray();
		} else if (parentElement instanceof Role) {
			return ((Role) parentElement).getRoleChildren().values().toArray();
		}   else if (parentElement instanceof ProUserProjectApp) {
			return ((ProUserProjectApp) parentElement).getProjectAppChildCollection().toArray();
		} else if (parentElement instanceof ProjectApplication) {
			return ((ProjectApplication) parentElement).getProjectApplicationChildren().values().toArray();
		} else if (parentElement instanceof ProjectAppAdminAreas) {
			return ((ProjectAppAdminAreas) parentElement).getProjectAppAdminAreaChildrenCollection().toArray();
		} else if (parentElement instanceof StartApplication) {
			return ((StartApplication) parentElement).getStartApplicationChildren().values().toArray();
		} else if (parentElement instanceof BaseApplication) {
			return ((BaseApplication) parentElement).getBaseApplicationChildren().values().toArray();
		} else if (parentElement instanceof BaseAppInformationModel) {
			return ((BaseAppInformationModel) parentElement).getBaseAppInformationChild().values().toArray();
		} else if (parentElement instanceof RoleScopeObjects) {
			return ((RoleScopeObjects) parentElement).getRoleScopeObjectsChildren().values().toArray();
		} else if (parentElement instanceof RelationObj) {
			IAdminTreeChild containerObj = ((RelationObj) parentElement).getContainerObj();
			if (containerObj instanceof SiteAdministrationChild) {
				return ((SiteAdministrationChild) containerObj).getSiteAdminAreasCollection().toArray();
			} else if (containerObj instanceof AdminAreaProjects) {
				return ((AdminAreaProjects) containerObj).getAdminAreasProjectsCollection().toArray();
			} else if (containerObj instanceof SiteAdminAreaProjectChild) {
				return ((SiteAdminAreaProjectChild) containerObj).getSiteAdminAreasProjectCollection().toArray();
			} else if (containerObj instanceof UserProjectChild) {
				return ((UserProjectChild) containerObj).getUserProjectChildrenCollection().toArray();
			} else if (containerObj instanceof UserProjectAdminAreaChild) {
				return ((UserProjectAdminAreaChild) containerObj).getUserProjectAdminAreaChildrenCollection().toArray();
			} else if (containerObj instanceof UserAdminAreaChild) {
				return ((UserAdminAreaChild) containerObj).getUserAdminAreaChildrenCollection().toArray();
			} else if (containerObj instanceof ProjectAdminAreaChild) {
				return ((ProjectAdminAreaChild) containerObj).getProjectAdminAreaChildrenCollection().toArray();
			} else if (containerObj instanceof ProjectUserAdminAreaChild) {
				return ((ProjectUserAdminAreaChild) containerObj).getProjectUserAdminAreaChildrenCollection().toArray();
			} else if (containerObj instanceof ProjectUserChild) {
				return ((ProjectUserChild) containerObj).getProjectUserChildrenCollection().toArray();
			} else if (containerObj instanceof AdminAreaProjectChild) {
				return ((AdminAreaProjectChild) containerObj).getAdminAreasProCollection().toArray();
			} else if (containerObj instanceof ProjectAppAdminAreaChild) {
				return ((ProjectAppAdminAreaChild) containerObj).getProjectAppAdminAreaChildrenCollection().toArray();
			} else if (containerObj instanceof RoleScopeObjectChild) {
				return ((RoleScopeObjectChild) containerObj).getRoleScopeObjectChildren().values().toArray();
			} 
		} 
		return new Object[] {};
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.viewers.ITreeContentProvider#getParent(java.lang.Object)
	 */
	@Override
	public Object getParent(Object element) {
		return null;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.viewers.ITreeContentProvider#hasChildren(java.lang.Object)
	 */
	@Override
	public boolean hasChildren(Object element) {
		return getChildren(element).length > 0;
	}

}
