package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.startapplicationmodel.StartApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.refs.RelationObj;

/**
 * The Class SiteAdminAreaStartApplications.
 * 
 * @author subash.janarthanan
 * 
 */
public class SiteAdminAreaStartApplications implements IAdminTreeChild {
	
	/** The parent. */
	private IAdminTreeChild parent;
	
	/** The site admin area start app child. */
	private Map<String, IAdminTreeChild> siteAdminAreaStartAppChild;

	/**
	 * Instantiates a new site admin area start applications.
	 *
	 * @param parent the parent
	 */
	public SiteAdminAreaStartApplications(final IAdminTreeChild parent) {
		this.parent = parent;
		this.siteAdminAreaStartAppChild = new LinkedHashMap<>();
	}
	
	/**
	 * @return the siteAdminAreaStartAppChild
	 */
	public Map<String, IAdminTreeChild> getSiteAdminAreaStartAppChild() {
		return siteAdminAreaStartAppChild;
	}
	
	/**
	 * Gets the site admin area start app collection.
	 *
	 * @return the site admin area start app collection
	 */
	public Collection<IAdminTreeChild> getSiteAdminAreaStartAppCollection() {
		return this.siteAdminAreaStartAppChild.values();
	}

	/**
	 * Add.
	 *
	 * @param siteAdminAreaStartAppChildId the site admin area start app child id
	 * @param child the child
	 * @return the i admin tree child
	 */
	public IAdminTreeChild add(final String siteAdminAreaStartAppChildId, final IAdminTreeChild child) {
		child.setParent(this);
		IAdminTreeChild returnVal = this.siteAdminAreaStartAppChild.put(siteAdminAreaStartAppChildId, child);
		if (child instanceof RelationObj && ((RelationObj) child).getRefObject() instanceof StartApplication) {
			sort();
		}
		return returnVal;
	}
	
	/**
	 * Remove.
	 *
	 * @param siteAdminAreaStartAppChildId the site admin area start app child id
	 * @return the i admin tree child
	 */
	public IAdminTreeChild remove(final String siteAdminAreaStartAppChildId) {
		return this.siteAdminAreaStartAppChild.remove(siteAdminAreaStartAppChildId);
	}

	/*
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#getParent()
	 */
	@Override
	public IAdminTreeChild getParent() {
		return this.parent;
	}

	/*
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#
	 * setParent(com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild)
	 */
	@Override
	public void setParent(IAdminTreeChild parent) {
		this.parent = parent;
	}
	
	/**
	 * Sort.
	 */
	public void sort() {
		final LinkedHashMap<String, IAdminTreeChild> collect = this.siteAdminAreaStartAppChild.entrySet().stream().sorted(
				(e1, e2) -> ((StartApplication) (((RelationObj) e1.getValue()).getRefObject())).getName()
				.compareTo(((StartApplication) (((RelationObj) e2.getValue()).getRefObject())).getName()))
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
		this.siteAdminAreaStartAppChild = collect;
	}
}
