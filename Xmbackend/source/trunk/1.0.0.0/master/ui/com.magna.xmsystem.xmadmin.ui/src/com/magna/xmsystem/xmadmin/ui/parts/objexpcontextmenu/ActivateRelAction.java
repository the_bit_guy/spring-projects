package com.magna.xmsystem.xmadmin.ui.parts.objexpcontextmenu;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.e4.core.services.nls.Translation;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.widgets.Display;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.ResourceAccessException;

import com.magna.xmbackend.exception.CannotCreateObjectException;
import com.magna.xmbackend.response.rel.adminareaproject.AdminAreaProjectResponseWrapper;
import com.magna.xmbackend.vo.rel.AdminAreaProjectAppRelRequest;
import com.magna.xmbackend.vo.rel.AdminAreaProjectAppRelResponse;
import com.magna.xmbackend.vo.rel.AdminAreaProjectRelRequest;
import com.magna.xmbackend.vo.rel.AdminAreaStartAppRelRequest;
import com.magna.xmbackend.vo.rel.AdminAreaStartAppRelResponse;
import com.magna.xmbackend.vo.rel.AdminAreaUserAppRelBatchResponse;
import com.magna.xmbackend.vo.rel.AdminAreaUserAppRelRequest;
import com.magna.xmbackend.vo.rel.ProjectStartAppRelBatchResponse;
import com.magna.xmbackend.vo.rel.ProjectStartAppRelRequest;
import com.magna.xmbackend.vo.rel.SiteAdminAreaRelBatchResponse;
import com.magna.xmbackend.vo.rel.SiteAdminAreaRelRequest;
import com.magna.xmbackend.vo.rel.UserProjectRelBatchResponse;
import com.magna.xmbackend.vo.rel.UserProjectRelRequest;
import com.magna.xmbackend.vo.rel.UserStartAppRelBatchResponse;
import com.magna.xmbackend.vo.rel.UserStartAppRelRequest;
import com.magna.xmsystem.dependencies.customExceptions.UnauthorizedAccessException;
import com.magna.xmsystem.ui.controls.widgets.CustomMessageDialog;
import com.magna.xmsystem.xmadmin.message.Message;
import com.magna.xmsystem.xmadmin.restclient.relation.AdminAreaProjectAppRelController;
import com.magna.xmsystem.xmadmin.restclient.relation.AdminAreaProjectRelController;
import com.magna.xmsystem.xmadmin.restclient.relation.AdminAreaStartAppRelController;
import com.magna.xmsystem.xmadmin.restclient.relation.AdminAreaUserAppRelController;
import com.magna.xmsystem.xmadmin.restclient.relation.ProjectStartAppRelController;
import com.magna.xmsystem.xmadmin.restclient.relation.SiteAdminAreaRelController;
import com.magna.xmsystem.xmadmin.restclient.relation.UserProjectRelController;
import com.magna.xmsystem.xmadmin.restclient.relation.UserStartAppRelController;
import com.magna.xmsystem.xmadmin.ui.parts.InformationPart;
import com.magna.xmsystem.xmadmin.ui.parts.objexp.ObjectExpPage;
import com.magna.xmsystem.xmadmin.ui.parts.objexp.ObjectExplorer;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.AdminTreeviewer;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaProjectAppFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaProjectAppNotFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaProjectAppProtected;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaProjectChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaProjectStartApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaProjects;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaStartApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaUserAppFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaUserAppNotFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaUserAppProtected;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdministrationArea;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.projectapplicationmodel.ProjectApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.startapplicationmodel.StartApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.userapplicationmodel.UserApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.Project;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectAdminAreaChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectAdminAreaProjectAppFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectAdminAreaProjectAppNotFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectAdminAreaProjectAppProtected;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectAdminAreaStartApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectAdminAreas;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectUserChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectUsers;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.refs.RelationObj;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.Site;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaProjectChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaProjectStartApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaProjects;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaStartApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaUserAppFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaUserAppNotFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaUserAppProtected;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminProjectAppFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminProjectAppNotFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminProjectAppProtected;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdministrationChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.User;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserProjectChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserProjects;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserStartApplications;
import com.magna.xmsystem.xmadmin.util.CommonConstants;
import com.magna.xmsystem.xmadmin.util.JobSchedulingRule;
import com.magna.xmsystem.xmadmin.util.MessageType;
import com.magna.xmsystem.xmadmin.util.XMAdminUtil;

/**
 * The Class ActivateRelAction.
 */
public class ActivateRelAction extends Action {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(ActivateRelAction.class);

	/** The messages. */
	@Inject
	@Translation
	private Message messages;

	/** Inject of {@link EModelService}. */
	@Inject
	private EModelService modelService;

	/** Inject of {@link MApplication}. */
	@Inject
	private MApplication application;

	/** The job scheduling rule. */
	private JobSchedulingRule jobSchedulingRule;

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.action.Action#run()
	 */
	@Override
	public void run() {
		final MPart mPart = (MPart) this.modelService.find(CommonConstants.PART_ID.INFORMATION_PART_ID, application);
		final InformationPart rightSidePart2 = (InformationPart) mPart.getObject();
		final ObjectExpPage objectExpPage = rightSidePart2.getObjectExpPartUI();
		final ObjectExplorer objExpTableViewer = objectExpPage.getObjExpTableViewer();
		IStructuredSelection selections = (IStructuredSelection) objExpTableViewer.getSelection();
		Object selectionObj = selections.getFirstElement();
		if(selectionObj == null){
		    selections = (IStructuredSelection) XMAdminUtil.getInstance().getAdminTree().getSelection();
			selectionObj = selections.getFirstElement();
		}
		final RelationObj relationObj = (RelationObj) selectionObj;
		final IAdminTreeChild containerObj = relationObj.getContainerObj();
		this.jobSchedulingRule = new JobSchedulingRule();
		if (containerObj instanceof SiteAdministrationChild) {
			activateSiteAARel(selections);
		} else if (containerObj instanceof SiteAdminAreaProjectChild || containerObj instanceof AdminAreaProjectChild) {
			activateSiteAAProjectRel(selections);
		} else if (containerObj instanceof SiteAdminAreaUserAppNotFixed
				|| containerObj instanceof SiteAdminAreaUserAppFixed
				|| containerObj instanceof SiteAdminAreaUserAppProtected
				|| containerObj instanceof AdminAreaUserAppProtected || containerObj instanceof AdminAreaUserAppNotFixed
				|| containerObj instanceof AdminAreaUserAppFixed) {
			activateSiteAAUserAppRel(selections);
		} else if (containerObj instanceof SiteAdminProjectAppNotFixed
				|| containerObj instanceof SiteAdminProjectAppFixed
				|| containerObj instanceof SiteAdminProjectAppProtected
				|| containerObj instanceof AdminAreaProjectAppNotFixed
				|| containerObj instanceof AdminAreaProjectAppFixed
				|| containerObj instanceof AdminAreaProjectAppProtected
				|| containerObj instanceof ProjectAdminAreaProjectAppNotFixed
				|| containerObj instanceof ProjectAdminAreaProjectAppFixed
				|| containerObj instanceof ProjectAdminAreaProjectAppProtected) {
			activateSiteAAProjectAppRel(selections);
		} else if (containerObj instanceof SiteAdminAreaStartApplications
				|| containerObj instanceof AdminAreaStartApplications) {
			activateSiteAAStartAppRel(selections);
		} else if (containerObj instanceof SiteAdminAreaProjectStartApplications
				|| containerObj instanceof AdminAreaProjectStartApplications
				|| containerObj instanceof ProjectAdminAreaStartApplications) {
			activateSiteAAProStartAppRel(selections);
		} else if (containerObj instanceof ProjectUserChild) {
			activateProjectUserRel(selections);
		} else if (containerObj instanceof UserProjectChild) {
			activateUserProjectRel(selections);
		} else if (containerObj instanceof UserStartApplications) {
			activateUserStartAppRel(selections);
		} else if (containerObj instanceof ProjectAdminAreaChild) {
			activateProjectAARel(selections);
		}
		Job ruleJob = new Job("refresh..") {

			@Override
			protected IStatus run(IProgressMonitor monitor) {
				Display.getDefault().asyncExec(new Runnable() {
					@Override
					public void run() {
						objExpTableViewer.refresh(true);
					}
				});

				return Status.OK_STATUS;
			}
		};
		ruleJob.setRule(this.jobSchedulingRule);
		ruleJob.schedule();
	}

	/**
	 * Activate project AA rel.
	 *
	 * @param selections the selections
	 */
	@SuppressWarnings("unchecked")
	private void activateProjectAARel(IStructuredSelection selections) {
		Object selectionObj = selections.getFirstElement();
		RelationObj relationObj = (RelationObj) selectionObj;
		final IAdminTreeChild containerObj = relationObj.getContainerObj();
		try {
			if (!XMAdminUtil.getInstance().isRelObjAccessAllowed(containerObj,
					"PROJECT_TO_ADMINISTRATION_AREA-ACTIVATE_DEACTIVATE")) {
				CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
						messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
				return;
			}
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				CustomMessageDialog.openError(Display.getCurrent().getActiveShell(), messages.errorDialogTitile,
						messages.serverNotReachable);
			}
			LOGGER.error("Exception occured in calling access allowed API " + e);
			return;
		}
		AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
		AdministrationArea adminArea = (AdministrationArea) relationObj.getRefObject();
		String adminAreaProjectRelname = adminArea.getName();
		String projectName = ((Project)relationObj.getParent().getParent()).getName();
		String confirmDialogTitle = messages.changeStatusConfirmDialogTitle;
		String confirmDialogMsg;
		if (selections.size() == 1) {
			confirmDialogMsg = messages.activateRelchangeStatusConfirmDialogMsg + " \'" + adminAreaProjectRelname + "\'"
					+ " ?";
		} else {
			confirmDialogMsg = messages.activateMultiRelchangeStatusConfirmDialogMsg + " ?";
		}
		boolean isConfirmed = CustomMessageDialog.openQuestion(adminTree.getControl().getShell(), confirmDialogTitle,
				confirmDialogMsg);

		if (isConfirmed) {
			final List<Object> selectionList = selections.toList();

			Job activateSiteAAProjectRelJob = new Job("Activating..") {

				@Override
				protected IStatus run(final IProgressMonitor monitor) {
					monitor.beginTask("Activating..", 100);
					monitor.worked(30);
					try {
						List<String> objectNames = new ArrayList<>();
						for (int i = 0; i < selectionList.size(); i++) {
							RelationObj relObject = (RelationObj) selectionList.get(i);
							objectNames.add(((AdministrationArea) relObject.getRefObject()).getName());
							String relId = relObject.getRelId();
							String status = com.magna.xmbackend.vo.enums.Status.ACTIVE.name();
							AdminAreaProjectRelController adminAreaProjectRelController = new AdminAreaProjectRelController(
									XMAdminUtil.getInstance().getAAForHeaders(relObject));
							boolean isUpdated = adminAreaProjectRelController.updateAdminAreaProjectRelStatus(relId,
									status);
							if (isUpdated) {
								Display.getDefault().asyncExec(new Runnable() {
									@Override
									public void run() {
										relObject.setActive(true);
										// adminTree.refresh(relObject);
										adminTree.refershBackReference(
												new Class[] { AdminAreaProjects.class, SiteAdminAreaProjects.class });
									}
								});
							}
						}
						Display.getDefault().asyncExec(new Runnable() {
							@Override
							public void run() {
								adminTree.refresh(true);
							}
						});
						
						for (Object name : objectNames) {
							XMAdminUtil.getInstance()
									.updateLogFile(messages.relationLbl + " " + messages.objectStatusUpdate + " "
											+ messages.administrationAreaObject + " '" + name + "' "
											+ messages.withLbl + " " + messages.projectObject + " '" + projectName
											+ "' " + messages.objectUpdate, MessageType.SUCCESS);
						}

					} catch (UnauthorizedAccessException e) {
						Display.getDefault().asyncExec(new Runnable() {

							@Override
							public void run() {
								CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
										messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
							}
						});
					} catch (Exception e) {
						LOGGER.warn("Unable to activate project admin area relation model! ", e);
					}
					monitor.worked(70);
					monitor.done();
					return Status.OK_STATUS;
				}
			};
			activateSiteAAProjectRelJob.setUser(true);
			activateSiteAAProjectRelJob.setRule(this.jobSchedulingRule);
			activateSiteAAProjectRelJob.schedule();
		}

	}

	/**
	 * Activate user start app rel.
	 *
	 * @param selections
	 *            the selections
	 */
	@SuppressWarnings("unchecked")
	private void activateUserStartAppRel(final IStructuredSelection selections) {
		Object selectionObj = selections.getFirstElement();
		RelationObj relationObj = (RelationObj) selectionObj;
		final IAdminTreeChild containerObj = relationObj.getContainerObj();
		try {
			if (!XMAdminUtil.getInstance().isRelObjAccessAllowed(containerObj,
					"START_APPLICATION_TO_USER-ACTIVATE_DEACTIVATE")) {
				CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
						messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
				return;
			}
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				CustomMessageDialog.openError(Display.getCurrent().getActiveShell(), messages.errorDialogTitile,
						messages.serverNotReachable);
			}
			LOGGER.error("Exception occured in calling access allowed API " + e);
			return;
		}
		AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
		StartApplication startApplication = (StartApplication) relationObj.getRefObject();
		String startAppRelname = startApplication.getName();
		IStructuredSelection selection = (IStructuredSelection) adminTree.getSelection();
		String userName = ((User)((IAdminTreeChild)selection.getFirstElement()).getParent()).getName();
		String confirmDialogTitle = messages.changeStatusConfirmDialogTitle;
		String confirmDialogMsg;
		if (selections.size() == 1) {
			confirmDialogMsg = messages.activateRelchangeStatusConfirmDialogMsg + " \'" + startAppRelname + "\'" + " ?";
		} else {
			confirmDialogMsg = messages.activateMultiRelchangeStatusConfirmDialogMsg + " ?";
		}
		boolean isConfirmed = CustomMessageDialog.openQuestion(adminTree.getControl().getShell(), confirmDialogTitle,
				confirmDialogMsg);
		if (isConfirmed) {
			final List<Object> selectionList = selections.toList();

			Job activateUserStartAppRelJob = new Job("Activating..") {

				@Override
				protected IStatus run(final IProgressMonitor monitor) {
					monitor.beginTask("Activating..", 100);
					monitor.worked(30);
					try {
						List<UserStartAppRelRequest> userStartAppRelRequestList = new ArrayList<>();
						List<String> objectNames = new ArrayList<>();
						Map<String, RelationObj> relObjMap = new HashMap<>();
						for (int i = 0; i < selectionList.size(); i++) {
							UserStartAppRelRequest userStartAppRelRequest = new UserStartAppRelRequest();
							RelationObj relObject = (RelationObj) selectionList.get(i);
							objectNames.add(((StartApplication) relObject.getRefObject()).getName());
							userStartAppRelRequest.setId(relObject.getRelId());
							userStartAppRelRequest.setStatus(com.magna.xmbackend.vo.enums.Status.ACTIVE.name());
							userStartAppRelRequestList.add(userStartAppRelRequest);
							relObjMap.put(relObject.getRelId(), relObject);
						}
						UserStartAppRelController userStartAppRelController = new UserStartAppRelController();
						UserStartAppRelBatchResponse response = userStartAppRelController
								.multiUpdateUserStartAppRelStatus(userStartAppRelRequestList);
						if (response != null) {
							List<String> statusUpdatationFailedList = response.getStatusUpdatationFailedList();
							if (!statusUpdatationFailedList.isEmpty()) {
								for (String id : statusUpdatationFailedList) {
									Iterator<Map.Entry<String, RelationObj>> iter = relObjMap.entrySet().iterator();
									while (iter.hasNext()) {
										Map.Entry<String, RelationObj> entry = iter.next();
										if (id.equalsIgnoreCase(entry.getKey())) {
											iter.remove();
										}
									}
								}
								Display.getDefault().asyncExec(new Runnable() {
									public void run() {
										CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
												messages.changeStatusConfirmDialogTitle,
												messages.multiStatusUpadteObjectDilaogErrMsg);
									}
								});
							}
							Iterator<Map.Entry<String, RelationObj>> iter = relObjMap.entrySet().iterator();
							while (iter.hasNext()) {
								Map.Entry<String, RelationObj> entry = iter.next();
								RelationObj obj = entry.getValue();
								obj.setActive(true);
							}

							Display.getDefault().asyncExec(new Runnable() {
								@Override
								public void run() {
									adminTree.refresh(true);
								}
							});
							for (Object name : objectNames) {
								XMAdminUtil.getInstance()
										.updateLogFile(messages.relationLbl + " " + messages.objectStatusUpdate + " "
												+ messages.startApplicationObject + " '" + name + "' "
												+ messages.withLbl + " " + messages.userObject + " '" + userName + "' "
												+ messages.objectUpdate, MessageType.SUCCESS);
							}
						}
					} catch (UnauthorizedAccessException e) {
						Display.getDefault().asyncExec(new Runnable() {

							@Override
							public void run() {
								CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
										messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
							}
						});
					} catch (Exception e) {
						LOGGER.warn("Unable to activate user start application relation model! ", e);
					}
					monitor.worked(70);
					monitor.done();
					return Status.OK_STATUS;
				}
			};
			activateUserStartAppRelJob.setUser(true);
			activateUserStartAppRelJob.setRule(this.jobSchedulingRule);
			activateUserStartAppRelJob.schedule();
		}
	}

	/**
	 * Activate user project rel.
	 *
	 * @param selections
	 *            the selections
	 */
	@SuppressWarnings("unchecked")
	private void activateUserProjectRel(final IStructuredSelection selections) {
		Object selectionObj = selections.getFirstElement();
		RelationObj relationObj = (RelationObj) selectionObj;
		final IAdminTreeChild containerObj = relationObj.getContainerObj();
		try {
			if (!XMAdminUtil.getInstance().isRelObjAccessAllowed(containerObj,
					"USER_TO_PROJECT-ACTIVATE_DEACTIVATE")) {
				CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
						messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
				return;
			}
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				CustomMessageDialog.openError(Display.getCurrent().getActiveShell(), messages.errorDialogTitile,
						messages.serverNotReachable);
			}
			LOGGER.error("Exception occured in calling access allowed API " + e);
			return;
		}
		AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
		Project project = (Project) relationObj.getRefObject();
		String projectUserRelName = project.getName();
		String userName = ((User)relationObj.getParent().getParent()).getName();
		String confirmDialogTitle = messages.changeStatusConfirmDialogTitle;
		String confirmDialogMsg;
		if (selections.size() == 1) {
			confirmDialogMsg = messages.activateRelchangeStatusConfirmDialogMsg + " \'" + projectUserRelName + "\'"
					+ " ?";
		} else {
			confirmDialogMsg = messages.activateMultiRelchangeStatusConfirmDialogMsg + " ?";
		}
		boolean isConfirmed = CustomMessageDialog.openQuestion(adminTree.getControl().getShell(), confirmDialogTitle,
				confirmDialogMsg);
		if (isConfirmed) {
			final List<Object> selectionList = selections.toList();

			Job activateUserProjectRelJob = new Job("Activating..") {

				@Override
				protected IStatus run(final IProgressMonitor monitor) {
					monitor.beginTask("Activating..", 100);
					monitor.worked(30);
					try {
						List<UserProjectRelRequest> userProjectRelRequestList = new ArrayList<>();
						Map<String, RelationObj> relObjMap = new HashMap<>();
						List<String> objectNames = new ArrayList<>();
						for (int i = 0; i < selectionList.size(); i++) {
							UserProjectRelRequest userProjectRelRequest = new UserProjectRelRequest();
							RelationObj relObject = (RelationObj) selectionList.get(i);
							userProjectRelRequest.setId(relObject.getRelId());
							userProjectRelRequest.setStatus(com.magna.xmbackend.vo.enums.Status.ACTIVE.name());
							userProjectRelRequestList.add(userProjectRelRequest);
							relObjMap.put(relObject.getRelId(), relObject);
							objectNames.add(((Project) relObject.getRefObject()).getName());
						}
						UserProjectRelController userProjectRelController = new UserProjectRelController();
						UserProjectRelBatchResponse response = userProjectRelController
								.multiUpdateUserProjectRelStatus(userProjectRelRequestList);
						//TODO
						if (response != null) {
							List<Map<String, String>> statusMaps = response.getStatusMap();
							if (!statusMaps.isEmpty()) {
								Display.getDefault().asyncExec(new Runnable() {
									public void run() {
										String errorMessage = "";
										for (Map<String, String> statusMap : statusMaps) {
											errorMessage += statusMap.get(
													XMAdminUtil.getInstance().getCurrentLocaleEnum().getLangCode());
											errorMessage += "\n";
										}
										if (errorMessage.length() > 0) {
											CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
													messages.dndErrorTitle, errorMessage);
										}
									}
								});
							}
							
							List<String> statusUpdatationFailedList = response.getStatusUpdatationFailedList();
							if (!statusUpdatationFailedList.isEmpty()) {
								for (String id : statusUpdatationFailedList) {
									Iterator<Map.Entry<String, RelationObj>> iter = relObjMap.entrySet().iterator();
									while (iter.hasNext()) {
										Map.Entry<String, RelationObj> entry = iter.next();
										if (id.equalsIgnoreCase(entry.getKey())) {
											iter.remove();
										}
									}
								}
								Display.getDefault().asyncExec(new Runnable() {
									public void run() {
										CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
												messages.changeStatusConfirmDialogTitle,
												messages.multiStatusUpadteObjectDilaogErrMsg);
									}
								});
							}
							
							List<String> updatedRelIds = response.getStatusUpdatationSuccessList();
							Iterator<Map.Entry<String, RelationObj>> iter = relObjMap.entrySet().iterator();
							while (iter.hasNext()) {
								Map.Entry<String, RelationObj> entry = iter.next();
								RelationObj obj = entry.getValue();
								if(!updatedRelIds.isEmpty() && updatedRelIds.contains(obj.getRelId())) {
									obj.setActive(true);
								} else{
									objectNames.remove(((Project) obj.getRefObject()).getName());
								}
							}

							Display.getDefault().asyncExec(new Runnable() {
								@Override
								public void run() {
									adminTree.refresh(true);
									adminTree.refershBackReference(new Class[] { ProjectUsers.class });
								}
							});
							for (Object name : objectNames) {
								XMAdminUtil.getInstance()
										.updateLogFile(messages.relationLbl + " " + messages.objectStatusUpdate + " "
												+ messages.projectObject + " '" + name + "' " + messages.withLbl + " "
												+ messages.userObject + " '" + userName + "' " + messages.objectUpdate,
												MessageType.SUCCESS);
							}
							
							Display.getDefault().asyncExec(new Runnable() {
								public void run() {
									for (Map<String, String> statusMap : statusMaps) {
										String errorMessage = statusMap.get(
												XMAdminUtil.getInstance().getCurrentLocaleEnum().getLangCode());
										XMAdminUtil.getInstance().updateLogFile(
												errorMessage, MessageType.FAILURE);
									}
								}
							});
						}
					} catch (CannotCreateObjectException e) {
						Display.getDefault().asyncExec(new Runnable() {

							@Override
							public void run() {
								CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
										messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
							}
						});
					} catch (UnauthorizedAccessException e) {
						Display.getDefault().asyncExec(new Runnable() {

							@Override
							public void run() {
								CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
										messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
							}
						});
					} catch (Exception e) {
						LOGGER.warn("Unable to activate user project relation model! ", e);
					}
					monitor.worked(70);
					monitor.done();
					return Status.OK_STATUS;
				}
			};
			activateUserProjectRelJob.setUser(true);
			activateUserProjectRelJob.setRule(this.jobSchedulingRule);
			activateUserProjectRelJob.schedule();
		}
	}

	/**
	 * Activate project user rel.
	 *
	 * @param selections
	 *            the selections
	 */
	@SuppressWarnings("unchecked")
	private void activateProjectUserRel(final IStructuredSelection selections) {
		Object selectionObj = selections.getFirstElement();
		RelationObj relationObj = (RelationObj) selectionObj;
		final IAdminTreeChild containerObj = relationObj.getContainerObj();
		try {
			if (!XMAdminUtil.getInstance().isRelObjAccessAllowed(containerObj,
					"USER_TO_PROJECT-ACTIVATE_DEACTIVATE")) {
				CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
						messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
				return;
			}
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				CustomMessageDialog.openError(Display.getCurrent().getActiveShell(), messages.errorDialogTitile,
						messages.serverNotReachable);
			}
			LOGGER.error("Exception occured in calling access allowed API " + e);
			return;
		}
		AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
		User user = (User) relationObj.getRefObject();
		String userRelname = user.getName();
		String projectName = ((Project)relationObj.getParent().getParent()).getName();
		String confirmDialogTitle = messages.changeStatusConfirmDialogTitle;
		String confirmDialogMsg;
		if (selections.size() == 1) {
			confirmDialogMsg = messages.activateRelchangeStatusConfirmDialogMsg + " \'" + userRelname + "\'" + " ?";
		} else {
			confirmDialogMsg = messages.activateMultiRelchangeStatusConfirmDialogMsg + " ?";
		}
		boolean isConfirmed = CustomMessageDialog.openQuestion(adminTree.getControl().getShell(), confirmDialogTitle,
				confirmDialogMsg);
		if (isConfirmed) {
			final List<Object> selectionList = selections.toList();

			Job activateProjectUserRelJob = new Job("Activating..") {

				@Override
				protected IStatus run(final IProgressMonitor monitor) {
					monitor.beginTask("Activating..", 100);
					monitor.worked(30);
					try {
						List<UserProjectRelRequest> userProjectRelRequestList = new ArrayList<>();
						Map<String, RelationObj> relObjMap = new HashMap<>();
						List<String> objectNames = new ArrayList<>();
						for (int i = 0; i < selectionList.size(); i++) {
							UserProjectRelRequest userProjectRelRequest = new UserProjectRelRequest();
							RelationObj relObject = (RelationObj) selectionList.get(i);
							userProjectRelRequest.setId(relObject.getRelId());
							userProjectRelRequest.setStatus(com.magna.xmbackend.vo.enums.Status.ACTIVE.name());
							relObjMap.put(relObject.getRelId(), relObject);
							userProjectRelRequestList.add(userProjectRelRequest);
							objectNames.add(((User) relObject.getRefObject()).getName());
						}
						UserProjectRelController userProjectRelController = new UserProjectRelController();
						UserProjectRelBatchResponse response = userProjectRelController
								.multiUpdateUserProjectRelStatus(userProjectRelRequestList);
						//TODO
						if (response != null) {
							List<Map<String, String>> statusMaps = response.getStatusMap();
							if (!statusMaps.isEmpty()) {
								Display.getDefault().asyncExec(new Runnable() {
									public void run() {
										String errorMessage = "";
										for (Map<String, String> statusMap : statusMaps) {
											errorMessage += statusMap.get(
													XMAdminUtil.getInstance().getCurrentLocaleEnum().getLangCode());
											errorMessage += "\n";
										}
										if (errorMessage.length() > 0) {
											CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
													messages.dndErrorTitle, errorMessage);
										}
									}
								});
							}
							
							List<String> statusUpdatationFailedList = response.getStatusUpdatationFailedList();
							if (!statusUpdatationFailedList.isEmpty()) {
								for (String id : statusUpdatationFailedList) {
									Iterator<Map.Entry<String, RelationObj>> iter = relObjMap.entrySet().iterator();
									while (iter.hasNext()) {
										Map.Entry<String, RelationObj> entry = iter.next();
										if (id.equalsIgnoreCase(entry.getKey())) {
											iter.remove();
										}
									}
								}
								Display.getDefault().asyncExec(new Runnable() {
									public void run() {
										CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
												messages.changeStatusConfirmDialogTitle,
												messages.multiStatusUpadteObjectDilaogErrMsg);
									}
								});
							}
							
							List<String> updatedRelIds = response.getStatusUpdatationSuccessList();
							Iterator<Map.Entry<String, RelationObj>> iter = relObjMap.entrySet().iterator();
							while (iter.hasNext()) {
								Map.Entry<String, RelationObj> entry = iter.next();
								RelationObj obj = entry.getValue();
								if(!updatedRelIds.isEmpty() && updatedRelIds.contains(obj.getRelId())) {
									obj.setActive(true);
								} else{
									objectNames.remove(((User) obj.getRefObject()).getName());
								}
							}

							Display.getDefault().asyncExec(new Runnable() {
								@Override
								public void run() {
									adminTree.refresh(true);
									adminTree.refershBackReference(new Class[] { UserProjects.class });
								}
							});
							for (Object name : objectNames) {
								XMAdminUtil.getInstance()
										.updateLogFile(messages.relationLbl + " " + messages.objectStatusUpdate + " "
												+ messages.userObject + " '" + name + "' " + messages.withLbl + " "
												+ messages.projectObject + " '" + projectName + "' "
												+ messages.objectUpdate, MessageType.SUCCESS);
							}
							
							Display.getDefault().asyncExec(new Runnable() {
								public void run() {
									for (Map<String, String> statusMap : statusMaps) {
										String errorMessage = statusMap.get(
												XMAdminUtil.getInstance().getCurrentLocaleEnum().getLangCode());
										XMAdminUtil.getInstance().updateLogFile(
												errorMessage, MessageType.FAILURE);
									}
								}
							});
						}
					} catch (CannotCreateObjectException e) {
						Display.getDefault().asyncExec(new Runnable() {

							@Override
							public void run() {
								CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
										messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
							}
						});
					} catch (UnauthorizedAccessException e) {
						Display.getDefault().asyncExec(new Runnable() {

							@Override
							public void run() {
								CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
										messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
							}
						});
					} catch (Exception e) {
						LOGGER.warn("Unable to activate project user relation model! ", e);
					}
					monitor.worked(70);
					monitor.done();
					return Status.OK_STATUS;
				}
			};
			activateProjectUserRelJob.setUser(true);
			activateProjectUserRelJob.setRule(this.jobSchedulingRule);
			activateProjectUserRelJob.schedule();
		}
	}

	/**
	 * Activate site AA pro start app rel.
	 *
	 * @param selections
	 *            the selections
	 */
	@SuppressWarnings("unchecked")
	private void activateSiteAAProStartAppRel(final IStructuredSelection selections) {
		Object selectionObj = selections.getFirstElement();
		RelationObj relationObj = (RelationObj) selectionObj;
		final IAdminTreeChild containerObj = relationObj.getContainerObj();
		try {
			if (!XMAdminUtil.getInstance().isRelObjAccessAllowed(containerObj,
					"START_APPLICATION_TO_PROJECT-ACTIVATE_DEACTIVATE")) {
				CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
						messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
				return;
			}
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				CustomMessageDialog.openError(Display.getCurrent().getActiveShell(), messages.errorDialogTitile,
						messages.serverNotReachable);
			}
			LOGGER.error("Exception occured in calling access allowed API " + e);
			return;
		}
		AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
		StartApplication startApplication = (StartApplication) relationObj.getRefObject();
		String startAppRelname = startApplication.getName();
		IStructuredSelection selection = (IStructuredSelection) adminTree.getSelection();
		String projectName;
		String adminAreaName;
		if(containerObj instanceof SiteAdminAreaProjectStartApplications){
			 projectName = ((Project)((RelationObj)((IAdminTreeChild)selection.getFirstElement()).getParent()).getRefObject()).getName();
			 adminAreaName = ((AdministrationArea)((RelationObj)((IAdminTreeChild)selection.getFirstElement()).getParent().getParent().getParent()).getRefObject()).getName();
		}else if(containerObj instanceof AdminAreaProjectStartApplications){
			 projectName = ((Project)((RelationObj)((IAdminTreeChild)selection.getFirstElement()).getParent()).getRefObject()).getName();
			 adminAreaName = ((AdministrationArea)((IAdminTreeChild)selection.getFirstElement()).getParent().getParent().getParent()).getName();
		}else{
			 adminAreaName = ((AdministrationArea)((RelationObj)((IAdminTreeChild)selection.getFirstElement()).getParent()).getRefObject()).getName();
			 projectName = ((Project)((IAdminTreeChild)selection.getFirstElement()).getParent().getParent().getParent()).getName();
		}
		
		String confirmDialogTitle = messages.changeStatusConfirmDialogTitle;
		String confirmDialogMsg;
		if (selections.size() == 1) {
			confirmDialogMsg = messages.activateRelchangeStatusConfirmDialogMsg + " \'" + startAppRelname + "\'" + " ?";
		} else {
			confirmDialogMsg = messages.activateMultiRelchangeStatusConfirmDialogMsg + " ?";
		}
		boolean isConfirmed = CustomMessageDialog.openQuestion(adminTree.getControl().getShell(), confirmDialogTitle,
				confirmDialogMsg);
		if (isConfirmed) {
			final List<Object> selectionList = selections.toList();

			Job activateSiteAAProStartAppRelJob = new Job("Activating..") {

				@Override
				protected IStatus run(final IProgressMonitor monitor) {
					monitor.beginTask("Activating..", 100);
					monitor.worked(30);
					try {
						List<ProjectStartAppRelRequest> projectStartAppRelRequestList = new ArrayList<>();
						Map<String, RelationObj> relObjMap = new HashMap<>();
						List<String> objectNames = new ArrayList<>();
						for (int i = 0; i < selectionList.size(); i++) {
							ProjectStartAppRelRequest projectStartAppRelRequest = new ProjectStartAppRelRequest();
							RelationObj relObject = (RelationObj) selectionList.get(i);
							projectStartAppRelRequest.setId(relObject.getRelId());
							projectStartAppRelRequest.setStatus(com.magna.xmbackend.vo.enums.Status.ACTIVE.name());
							projectStartAppRelRequestList.add(projectStartAppRelRequest);
							relObjMap.put(relObject.getRelId(), relObject);
							objectNames.add(((StartApplication) relObject.getRefObject()).getName());
						}
						IAdminTreeChild parent = ((RelationObj) relationObj).getContainerObj().getParent();
						ProjectStartAppRelController projectStartAppRelController = new ProjectStartAppRelController(
								XMAdminUtil.getInstance().getAAForHeaders(parent));
						ProjectStartAppRelBatchResponse response = projectStartAppRelController
								.multiUpdateProjectStartAppRelStatus(projectStartAppRelRequestList);
						if (response != null) {
							List<String> statusUpdatationFailedList = response.getStatusUpdatationFailedList();
							if (!statusUpdatationFailedList.isEmpty()) {
								for (String id : statusUpdatationFailedList) {
									Iterator<Map.Entry<String, RelationObj>> iter = relObjMap.entrySet().iterator();
									while (iter.hasNext()) {
										Map.Entry<String, RelationObj> entry = iter.next();
										if (id.equalsIgnoreCase(entry.getKey())) {
											iter.remove();
										}
									}
								}
								Display.getDefault().asyncExec(new Runnable() {
									public void run() {
										CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
												messages.changeStatusConfirmDialogTitle,
												messages.multiStatusUpadteObjectDilaogErrMsg);
									}
								});
							}
							Iterator<Map.Entry<String, RelationObj>> iter = relObjMap.entrySet().iterator();
							while (iter.hasNext()) {
								Map.Entry<String, RelationObj> entry = iter.next();
								RelationObj obj = entry.getValue();
								obj.setActive(true);
							}

							Display.getDefault().asyncExec(new Runnable() {
								@Override
								public void run() {
									adminTree.refresh(true);
								}
							});
							for (Object name : objectNames) {
								XMAdminUtil.getInstance()
										.updateLogFile(messages.relationLbl + " " + messages.objectStatusUpdate + " "
												+ messages.startApplicationObject + " '" + name + "' "
												+ messages.withLbl + " " + messages.projectObject + " '" + projectName
												+ "' " + messages.andLbl + " " + messages.administrationAreaObject
												+ " '" + adminAreaName + "' " + messages.objectUpdate,
												MessageType.SUCCESS);
							}
						}

					} catch (UnauthorizedAccessException e) {
						Display.getDefault().asyncExec(new Runnable() {

							@Override
							public void run() {
								CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
										messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
							}
						});
					} catch (Exception e) {
						LOGGER.warn("Unable to activate site admin area project start application relation model! ", e);
					}
					monitor.worked(70);
					monitor.done();
					return Status.OK_STATUS;
				}
			};
			activateSiteAAProStartAppRelJob.setUser(true);
			activateSiteAAProStartAppRelJob.setRule(this.jobSchedulingRule);
			activateSiteAAProStartAppRelJob.schedule();
		}
	}

	/**
	 * Activate site AA start app rel.
	 *
	 * @param selections
	 *            the selections
	 */
	@SuppressWarnings("unchecked")
	private void activateSiteAAStartAppRel(final IStructuredSelection selections) {
		Object selectionObj = selections.getFirstElement();
		RelationObj relationObj = (RelationObj) selectionObj;
		final IAdminTreeChild containerObj = relationObj.getContainerObj();
		try {
			if (!XMAdminUtil.getInstance().isRelObjAccessAllowed(containerObj,
					"START_APPLICATION_TO_ADMINISTRATION_AREA-ACTIVATE_DEACTIVATE")) {
				CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
						messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
				return;
			}
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				CustomMessageDialog.openError(Display.getCurrent().getActiveShell(), messages.errorDialogTitile,
						messages.serverNotReachable);
			}
			LOGGER.error("Exception occured in calling access allowed API " + e);
			return;
		}
		AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
		StartApplication startApplication = (StartApplication) relationObj.getRefObject();
		String startAppRelname = startApplication.getName();
		IStructuredSelection selection = (IStructuredSelection) adminTree.getSelection();
		String adminAreaName;
		if(containerObj instanceof SiteAdminAreaStartApplications){
			 adminAreaName = ((AdministrationArea)((RelationObj)((IAdminTreeChild)selection.getFirstElement()).getParent()).getRefObject()).getName();
		}else {
			 adminAreaName = ((AdministrationArea)(((IAdminTreeChild)selection.getFirstElement()).getParent())).getName();
		}
		String confirmDialogTitle = messages.changeStatusConfirmDialogTitle;
		String confirmDialogMsg;
		if (selections.size() == 1) {
			confirmDialogMsg = messages.activateRelchangeStatusConfirmDialogMsg + " \'" + startAppRelname + "\'" + " ?";
		} else {
			confirmDialogMsg = messages.activateMultiRelchangeStatusConfirmDialogMsg + " ?";
		}
		boolean isConfirmed = CustomMessageDialog.openQuestion(adminTree.getControl().getShell(), confirmDialogTitle,
				confirmDialogMsg);
		if (isConfirmed) {
			final List<Object> selectionList = selections.toList();

			Job activateSiteAAStartAppRelJob = new Job("Activating..") {

				@Override
				protected IStatus run(final IProgressMonitor monitor) {
					monitor.beginTask("Activating..", 100);
					monitor.worked(30);
					try {
						List<AdminAreaStartAppRelRequest> adminAreaStartAppRelRequestList = new ArrayList<>();
						Map<String, RelationObj> relObjMap = new HashMap<>();
						List<String> objectNames = new ArrayList<>();
						for (int i = 0; i < selectionList.size(); i++) {
							AdminAreaStartAppRelRequest adminAreaStartAppRelRequest = new AdminAreaStartAppRelRequest();
							RelationObj relObject = (RelationObj) selectionList.get(i);
							adminAreaStartAppRelRequest.setId(relObject.getRelId());
							adminAreaStartAppRelRequest.setStatus(com.magna.xmbackend.vo.enums.Status.ACTIVE.name());
							adminAreaStartAppRelRequestList.add(adminAreaStartAppRelRequest);
							relObjMap.put(relObject.getRelId(), relObject);
							objectNames.add(((StartApplication) relObject.getRefObject()).getName());
						}
						AdminAreaStartAppRelController adminAreaStartAppRelController = new AdminAreaStartAppRelController(
								XMAdminUtil.getInstance().getAAForHeaders(relationObj.getContainerObj()));
						AdminAreaStartAppRelResponse response = adminAreaStartAppRelController
								.multiUpdateAAStartAppRelStatus(adminAreaStartAppRelRequestList);
						if (response != null) {
							List<String> statusUpdatationFailedList = response.getStatusUpdatationFailedList();
							if (!statusUpdatationFailedList.isEmpty()) {
								for (String id : statusUpdatationFailedList) {
									Iterator<Map.Entry<String, RelationObj>> iter = relObjMap.entrySet().iterator();
									while (iter.hasNext()) {
										Map.Entry<String, RelationObj> entry = iter.next();
										if (id.equalsIgnoreCase(entry.getKey())) {
											iter.remove();
										}
									}
								}
								Display.getDefault().asyncExec(new Runnable() {
									public void run() {
										CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
												messages.changeStatusConfirmDialogTitle,
												messages.multiStatusUpadteObjectDilaogErrMsg);
									}
								});
							}
							Iterator<Map.Entry<String, RelationObj>> iter = relObjMap.entrySet().iterator();
							while (iter.hasNext()) {
								Map.Entry<String, RelationObj> entry = iter.next();
								RelationObj obj = entry.getValue();
								obj.setActive(true);
							}

							Display.getDefault().asyncExec(new Runnable() {
								@Override
								public void run() {
									adminTree.refresh(true);
								}
							});
							for (Object name : objectNames) {
								XMAdminUtil.getInstance().updateLogFile(messages.relationLbl + " "
										+ messages.objectStatusUpdate + " " + messages.startApplicationObject + " '"
										+ name + "' " + messages.withLbl + " " + messages.administrationAreaObject
										+ " '" + adminAreaName + "' " + messages.objectUpdate, MessageType.SUCCESS);
							}
						}
					} catch (UnauthorizedAccessException e) {
						Display.getDefault().asyncExec(new Runnable() {

							@Override
							public void run() {
								CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
										messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
							}
						});
					} catch (Exception e) {
						LOGGER.warn("Unable to activate site admin area start application relation model! ", e);
					}
					monitor.worked(70);
					monitor.done();
					return Status.OK_STATUS;
				}
			};
			activateSiteAAStartAppRelJob.setUser(true);
			activateSiteAAStartAppRelJob.setRule(this.jobSchedulingRule);
			activateSiteAAStartAppRelJob.schedule();
		}

	}

	/**
	 * Activate site AA project app rel.
	 *
	 * @param selections
	 *            the selections
	 */
	@SuppressWarnings("unchecked")
	private void activateSiteAAProjectAppRel(final IStructuredSelection selections) {
		Object selectionObj = selections.getFirstElement();
		RelationObj relationObj = (RelationObj) selectionObj;
		final IAdminTreeChild containerObj = relationObj.getContainerObj();
		try {
			if (!XMAdminUtil.getInstance().isRelObjAccessAllowed(containerObj,
					"PROJECT_APPLICATION_TO_PROJECT-ACTIVATE_DEACTIVATE")) {
				CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
						messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
				return;
			}
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				CustomMessageDialog.openError(Display.getCurrent().getActiveShell(), messages.errorDialogTitile,
						messages.serverNotReachable);
			}
			LOGGER.error("Exception occured in calling access allowed API " + e);
			return;
		}
		AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
		ProjectApplication projectApplication = (ProjectApplication) relationObj.getRefObject();
		String projectAppRelname = projectApplication.getName();
		IStructuredSelection selection = (IStructuredSelection) adminTree.getSelection();
		String adminAreaName;
		String projectName;
		if (containerObj instanceof SiteAdminProjectAppNotFixed || containerObj instanceof SiteAdminProjectAppFixed
				|| containerObj instanceof SiteAdminProjectAppProtected) {
			adminAreaName = ((AdministrationArea) ((RelationObj) ((IAdminTreeChild) selection.getFirstElement())
					.getParent().getParent().getParent().getParent()).getRefObject()).getName();
			projectName = ((Project) ((RelationObj) ((IAdminTreeChild) selection.getFirstElement()).getParent()
					.getParent()).getRefObject()).getName();
		} else if (containerObj instanceof AdminAreaProjectAppNotFixed
				|| containerObj instanceof AdminAreaProjectAppFixed
				|| containerObj instanceof AdminAreaProjectAppProtected) {
			adminAreaName = ((AdministrationArea) (((IAdminTreeChild) selection.getFirstElement()).getParent()
					.getParent().getParent().getParent())).getName();
			projectName = ((Project) ((RelationObj) ((IAdminTreeChild) selection.getFirstElement()).getParent()
					.getParent()).getRefObject()).getName();
		} else {
			adminAreaName = ((AdministrationArea) ((RelationObj) ((IAdminTreeChild) selection.getFirstElement())
					.getParent().getParent()).getRefObject()).getName();
			projectName = ((Project) (((IAdminTreeChild) selection.getFirstElement()).getParent()
					.getParent().getParent().getParent())).getName();
		}
		String confirmDialogTitle = messages.changeStatusConfirmDialogTitle;
		String confirmDialogMsg;
		if (selections.size() == 1) {
			confirmDialogMsg = messages.activateRelchangeStatusConfirmDialogMsg + " \'" + projectAppRelname + "\'"
					+ " ?";
		} else {
			confirmDialogMsg = messages.activateMultiRelchangeStatusConfirmDialogMsg + " ?";
		}
		boolean isConfirmed = CustomMessageDialog.openQuestion(adminTree.getControl().getShell(), confirmDialogTitle,
				confirmDialogMsg);
		if (isConfirmed) {
			final List<Object> selectionList = selections.toList();

			Job activateSiteAAProjectAppRelJob = new Job("Activating..") {

				@Override
				protected IStatus run(final IProgressMonitor monitor) {
					monitor.beginTask("Activating..", 100);
					monitor.worked(30);
					try {
						List<AdminAreaProjectAppRelRequest> adminAreaProjectAppRelRequestList = new ArrayList<>();
						Map<String, RelationObj> relObjMap = new HashMap<>();
						List<String> objectNames = new ArrayList<>();
						for (int i = 0; i < selectionList.size(); i++) {
							AdminAreaProjectAppRelRequest adminAreaProjectAppRelRequest = new AdminAreaProjectAppRelRequest();
							RelationObj relObject = (RelationObj) selectionList.get(i);
							adminAreaProjectAppRelRequest.setId(relObject.getRelId());
							adminAreaProjectAppRelRequest.setStatus(com.magna.xmbackend.vo.enums.Status.ACTIVE.name());
							adminAreaProjectAppRelRequestList.add(adminAreaProjectAppRelRequest);
							relObjMap.put(relObject.getRelId(), relObject);
							objectNames.add(((ProjectApplication) relObject.getRefObject()).getName());
						}
						IAdminTreeChild parent = ((RelationObj) relationObj).getContainerObj().getParent().getParent();
						AdminAreaProjectAppRelController adminAreaProjectAppRelController = new AdminAreaProjectAppRelController(
								XMAdminUtil.getInstance().getAAForHeaders(parent));
						AdminAreaProjectAppRelResponse response = adminAreaProjectAppRelController
								.multiUpdateAAProjectAppRelStatus(adminAreaProjectAppRelRequestList);
						if (response != null) {
							List<String> statusUpdatationFailedList = response.getStatusUpdatationFailedList();
							if (!statusUpdatationFailedList.isEmpty()) {
								for (String id : statusUpdatationFailedList) {
									Iterator<Map.Entry<String, RelationObj>> iter = relObjMap.entrySet().iterator();
									while (iter.hasNext()) {
										Map.Entry<String, RelationObj> entry = iter.next();
										if (id.equalsIgnoreCase(entry.getKey())) {
											iter.remove();
										}
									}
								}
								Display.getDefault().asyncExec(new Runnable() {
									public void run() {
										CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
												messages.changeStatusConfirmDialogTitle,
												messages.multiStatusUpadteObjectDilaogErrMsg);
									}
								});
							}
							Iterator<Map.Entry<String, RelationObj>> iter = relObjMap.entrySet().iterator();
							while (iter.hasNext()) {
								Map.Entry<String, RelationObj> entry = iter.next();
								RelationObj obj = entry.getValue();
								obj.setActive(true);
							}

							Display.getDefault().asyncExec(new Runnable() {
								@Override
								public void run() {
									adminTree.refresh(true);
								}
							});
							for (Object name : objectNames) {
								XMAdminUtil.getInstance()
										.updateLogFile(messages.relationLbl + " " + messages.objectStatusUpdate + " "
												+ messages.projectApplicationObject + " '" + name + "' "
												+ messages.withLbl + " " + messages.projectObject + " '" + projectName
												+ "' " + messages.andLbl + messages.administrationAreaObject + " '"
												+ adminAreaName + "' " + messages.objectUpdate, MessageType.SUCCESS);
							}

						}
					} catch (UnauthorizedAccessException e) {
						Display.getDefault().asyncExec(new Runnable() {

							@Override
							public void run() {
								CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
										messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
							}
						});
					} catch (Exception e) {
						LOGGER.warn("Unable to activate site admin area project application relation model! ", e);
					}
					monitor.worked(70);
					monitor.done();
					return Status.OK_STATUS;
				}
			};
			activateSiteAAProjectAppRelJob.setUser(true);
			activateSiteAAProjectAppRelJob.setRule(this.jobSchedulingRule);
			activateSiteAAProjectAppRelJob.schedule();
		}
	}

	/**
	 * Activate site AA user app rel.
	 *
	 * @param selections
	 *            the selections
	 */
	@SuppressWarnings("unchecked")
	private void activateSiteAAUserAppRel(final IStructuredSelection selections) {
		Object selectionObj = selections.getFirstElement();
		RelationObj relationObj = (RelationObj) selectionObj;
		final IAdminTreeChild containerObj = relationObj.getContainerObj();
		try {
			if (!XMAdminUtil.getInstance().isRelObjAccessAllowed(containerObj,
					"USER_APPLICATION_TO_ADMINISTRATION_AREA-ACTIVATE_DEACTIVATE")) {
				CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
						messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
				return;
			}
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				CustomMessageDialog.openError(Display.getCurrent().getActiveShell(), messages.errorDialogTitile,
						messages.serverNotReachable);
			}
			LOGGER.error("Exception occured in calling access allowed API " + e);
			return;
		}
		AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
		UserApplication userApplication = (UserApplication) relationObj.getRefObject();
		String userAppRelname = userApplication.getName();
		IStructuredSelection selection = (IStructuredSelection) adminTree.getSelection();
		String adminAreaName;
		if(containerObj instanceof SiteAdminAreaUserAppNotFixed
				|| containerObj instanceof SiteAdminAreaUserAppFixed
				|| containerObj instanceof SiteAdminAreaUserAppProtected){
			adminAreaName = ((AdministrationArea) ((RelationObj) ((IAdminTreeChild) selection.getFirstElement())
					.getParent().getParent()).getRefObject()).getName();
		}else{
			adminAreaName = ((AdministrationArea) (((IAdminTreeChild) selection.getFirstElement())
					.getParent().getParent())).getName();
		}
		String confirmDialogTitle = messages.changeStatusConfirmDialogTitle;
		String confirmDialogMsg;
		if (selections.size() == 1) {
			confirmDialogMsg = messages.activateRelchangeStatusConfirmDialogMsg + " \'" + userAppRelname + "\'" + " ?";
		} else {
			confirmDialogMsg = messages.activateMultiRelchangeStatusConfirmDialogMsg + " ?";
		}
		boolean isConfirmed = CustomMessageDialog.openQuestion(adminTree.getControl().getShell(), confirmDialogTitle,
				confirmDialogMsg);
		if (isConfirmed) {
			final List<Object> selectionList = selections.toList();

			Job activateSiteAAUserAppRelJob = new Job("Activating..") {

				@Override
				protected IStatus run(final IProgressMonitor monitor) {
					monitor.beginTask("Activating..", 100);
					monitor.worked(30);
					try {
						List<AdminAreaUserAppRelRequest> adminAreaUserAppRelRequestList = new ArrayList<>();
						Map<String, RelationObj> relObjMap = new HashMap<>();
						List<String> objectNames = new ArrayList<>();
						for (int i = 0; i < selectionList.size(); i++) {
							AdminAreaUserAppRelRequest adminAreaUserAppRelRequest = new AdminAreaUserAppRelRequest();
							RelationObj relObject = (RelationObj) selectionList.get(i);
							adminAreaUserAppRelRequest.setId(relObject.getRelId());
							adminAreaUserAppRelRequest.setStatus(com.magna.xmbackend.vo.enums.Status.ACTIVE.name());
							adminAreaUserAppRelRequestList.add(adminAreaUserAppRelRequest);
							relObjMap.put(relObject.getRelId(), relObject);
							objectNames.add(((UserApplication) relObject.getRefObject()).getName());
						}
						AdminAreaUserAppRelController adminAreaUserAppRelController = new AdminAreaUserAppRelController(
								XMAdminUtil.getInstance().getAAForHeaders(relationObj.getContainerObj()));
						AdminAreaUserAppRelBatchResponse response = adminAreaUserAppRelController
								.multiUpdateAAUserAppRelStatus(adminAreaUserAppRelRequestList);
						if (response != null) {
							List<String> statusUpdatationFailedList = response.getStatusUpdatationFailedList();
							if (!statusUpdatationFailedList.isEmpty()) {
								for (String id : statusUpdatationFailedList) {
									Iterator<Map.Entry<String, RelationObj>> iter = relObjMap.entrySet().iterator();
									while (iter.hasNext()) {
										Map.Entry<String, RelationObj> entry = iter.next();
										if (id.equalsIgnoreCase(entry.getKey())) {
											iter.remove();
										}
									}
								}
								Display.getDefault().asyncExec(new Runnable() {
									public void run() {
										CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
												messages.changeStatusConfirmDialogTitle,
												messages.multiStatusUpadteObjectDilaogErrMsg);
									}
								});
							}
							Iterator<Map.Entry<String, RelationObj>> iter = relObjMap.entrySet().iterator();
							while (iter.hasNext()) {
								Map.Entry<String, RelationObj> entry = iter.next();
								RelationObj obj = entry.getValue();
								obj.setActive(true);
							}

							Display.getDefault().asyncExec(new Runnable() {
								@Override
								public void run() {
									adminTree.refresh(true);
								}
							});
							for (Object name : objectNames) {
								XMAdminUtil.getInstance().updateLogFile(messages.relationLbl + " "
										+ messages.objectStatusUpdate + " " + messages.userApplicationObject + " '"
										+ name + "' " + messages.withLbl + " " + messages.administrationAreaObject
										+ " '" + adminAreaName + "' " + messages.objectUpdate, MessageType.SUCCESS);
							}
						}
					} catch (UnauthorizedAccessException e) {
						Display.getDefault().asyncExec(new Runnable() {

							@Override
							public void run() {
								CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
										messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
							}
						});
					} catch (Exception e) {
						LOGGER.warn("Unable to activate site admin area user application relation model! ", e);
					}
					monitor.worked(70);
					monitor.done();
					return Status.OK_STATUS;
				}
			};
			activateSiteAAUserAppRelJob.setUser(true);
			activateSiteAAUserAppRelJob.setRule(this.jobSchedulingRule);
			activateSiteAAUserAppRelJob.schedule();
		}

	}

	/**
	 * Activate site AA project rel.
	 *
	 * @param selections
	 *            the selections
	 */
	@SuppressWarnings("unchecked")
	private void activateSiteAAProjectRel(final IStructuredSelection selections) {
		Object selectionObj = selections.getFirstElement();
		RelationObj relationObj = (RelationObj) selectionObj;
		final IAdminTreeChild containerObj = relationObj.getContainerObj();
		try {
			if (!XMAdminUtil.getInstance().isRelObjAccessAllowed(containerObj,
					"PROJECT_TO_ADMINISTRATION_AREA-ACTIVATE_DEACTIVATE")) {
				CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
						messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
				return;
			}
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				CustomMessageDialog.openError(Display.getCurrent().getActiveShell(), messages.errorDialogTitile,
						messages.serverNotReachable);
			}
			LOGGER.error("Exception occured in calling access allowed API " + e);
			return;
		}
		AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
		Project project = (Project) relationObj.getRefObject();
		String adminAreaProjectRelname = project.getName();
		String adminAreaName;
		if (containerObj instanceof SiteAdminAreaProjectChild) {
			adminAreaName = ((AdministrationArea) ((RelationObj) relationObj.getParent().getParent()).getRefObject())
					.getName();
		} else {
			adminAreaName = ((AdministrationArea) (relationObj.getParent().getParent())).getName();
		}
		String confirmDialogTitle = messages.changeStatusConfirmDialogTitle;
		String confirmDialogMsg;
		if (selections.size() == 1) {
			confirmDialogMsg = messages.activateRelchangeStatusConfirmDialogMsg + " \'" + adminAreaProjectRelname + "\'"
					+ " ?";
		} else {
			confirmDialogMsg = messages.activateMultiRelchangeStatusConfirmDialogMsg + " ?";
		}
		boolean isConfirmed = CustomMessageDialog.openQuestion(adminTree.getControl().getShell(), confirmDialogTitle,
				confirmDialogMsg);

		if (isConfirmed) {
			final List<Object> selectionList = selections.toList();

			Job activateSiteAAProjectRelJob = new Job("Activating..") {

				@Override
				protected IStatus run(final IProgressMonitor monitor) {
					monitor.beginTask("Activating..", 100);
					monitor.worked(30);
					try {
						List<AdminAreaProjectRelRequest> adminAreaProjectRelRequestList = new ArrayList<>();
						Map<String, RelationObj> relObjMap = new HashMap<>();
						List<String> objectNames = new ArrayList<>();
						for (int i = 0; i < selectionList.size(); i++) {
							AdminAreaProjectRelRequest adminAreaProjectRelRequest = new AdminAreaProjectRelRequest();
							RelationObj relObject = (RelationObj) selectionList.get(i);
							adminAreaProjectRelRequest.setId(relObject.getRelId());
							adminAreaProjectRelRequest.setStatus(com.magna.xmbackend.vo.enums.Status.ACTIVE.name());
							adminAreaProjectRelRequestList.add(adminAreaProjectRelRequest);
							relObjMap.put(relObject.getRelId(), relObject);
							objectNames.add(((Project) relObject.getRefObject()).getName());
						}
						AdminAreaProjectRelController adminAreaProjectRelController = new AdminAreaProjectRelController(
								XMAdminUtil.getInstance().getAAForHeaders(relationObj));
						AdminAreaProjectResponseWrapper response = adminAreaProjectRelController
								.multiUpdateAdminAreaProjectRelStatus(adminAreaProjectRelRequestList);
						if (response != null) {

							List<String> statusUpdatationFailedList = response.getStatusUpdatationFailedList();
							if (!statusUpdatationFailedList.isEmpty()) {
								for (String id : statusUpdatationFailedList) {
									Iterator<Map.Entry<String, RelationObj>> iter = relObjMap.entrySet().iterator();
									while (iter.hasNext()) {
										Map.Entry<String, RelationObj> entry = iter.next();
										if (id.equalsIgnoreCase(entry.getKey())) {
											iter.remove();
										}
									}
								}
								Display.getDefault().asyncExec(new Runnable() {
									public void run() {
										CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
												messages.changeStatusConfirmDialogTitle,
												messages.multiStatusUpadteObjectDilaogErrMsg);
									}
								});
							}
							Iterator<Map.Entry<String, RelationObj>> iter = relObjMap.entrySet().iterator();
							while (iter.hasNext()) {
								Map.Entry<String, RelationObj> entry = iter.next();
								RelationObj obj = entry.getValue();
								obj.setActive(true);
							}

							Display.getDefault().asyncExec(new Runnable() {
								@Override
								public void run() {
									adminTree.refresh(true);
									if (relationObj.getParent() instanceof SiteAdminAreaProjects) {
										adminTree.refershBackReference(new Class[] { AdminAreaProjects.class });
									} else if (relationObj.getParent() instanceof AdminAreaProjects) {
										adminTree.refershBackReference(new Class[] { SiteAdminAreaProjects.class });
									}
									adminTree.refershBackReference(new Class[] { ProjectAdminAreas.class });
								}
							});
							for (Object name : objectNames) {
								XMAdminUtil.getInstance()
										.updateLogFile(messages.relationLbl + " " + messages.objectStatusUpdate + " "
												+ messages.projectObject + " '" + name + "' " + messages.withLbl + " "
												+ messages.administrationAreaObject + " '" + adminAreaName + "' "
												+ messages.objectUpdate, MessageType.SUCCESS);
							}
						}
					} catch (UnauthorizedAccessException e) {
						Display.getDefault().asyncExec(new Runnable() {

							@Override
							public void run() {
								CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
										messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
							}
						});
					} catch (Exception e) {
						LOGGER.warn("Unable to activate site admin area project relation model! ", e);
					}
					monitor.worked(70);
					monitor.done();
					return Status.OK_STATUS;
				}
			};
			activateSiteAAProjectRelJob.setUser(true);
			activateSiteAAProjectRelJob.setRule(this.jobSchedulingRule);
			activateSiteAAProjectRelJob.schedule();
		}

	}

	/**
	 * Activate site AA rel.
	 *
	 * @param selections
	 *            the selections
	 */
	@SuppressWarnings("unchecked")
	private void activateSiteAARel(final IStructuredSelection selections) {
		Object selectionObj = selections.getFirstElement();
		RelationObj relationObj = (RelationObj) selectionObj;
		final IAdminTreeChild containerObj = relationObj.getContainerObj();
		try {
			if (!XMAdminUtil.getInstance().isRelObjAccessAllowed(containerObj,
					"ADMINISTRATION_AREA_TO_SITE-ACTIVATE_DEACTIVATE")) {
				CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
						messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
				return;
			}
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				CustomMessageDialog.openError(Display.getCurrent().getActiveShell(), messages.errorDialogTitile,
						messages.serverNotReachable);
			}
			LOGGER.error("Exception occured in calling access allowed API " + e);
			return;
		}
		AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
		AdministrationArea administrationArea = (AdministrationArea) relationObj.getRefObject();
		String adminAreaRelname = administrationArea.getName();
		String siteName = ((Site) (relationObj.getParent().getParent())).getName();
		String confirmDialogTitle = messages.changeStatusConfirmDialogTitle;
		String confirmDialogMsg;
		if (selections.size() == 1) {
			confirmDialogMsg = messages.activateRelchangeStatusConfirmDialogMsg + " \'" + adminAreaRelname + "\'"
					+ " ?";
		} else {
			confirmDialogMsg = messages.activateMultiRelchangeStatusConfirmDialogMsg + " ?";
		}
		boolean isConfirmed = CustomMessageDialog.openQuestion(adminTree.getControl().getShell(), confirmDialogTitle,
				confirmDialogMsg);
		if (isConfirmed) {
			final List<Object> selectionList = selections.toList();

			Job activateSiteAARelJob = new Job("Activating..") {

				@Override
				protected IStatus run(final IProgressMonitor monitor) {
					monitor.beginTask("Activating..", 100);
					monitor.worked(30);
					try {
						List<SiteAdminAreaRelRequest> siteAdminAreaRelRequestList = new ArrayList<>();
						Map<String, RelationObj> relObjMap = new HashMap<>();
						List<String> objectNames = new ArrayList<>();
						for (int i = 0; i < selectionList.size(); i++) {
							SiteAdminAreaRelRequest siteAdminAreaRelRequest = new SiteAdminAreaRelRequest();
							RelationObj relObject = (RelationObj) selectionList.get(i);
							siteAdminAreaRelRequest.setId(relObject.getRelId());
							siteAdminAreaRelRequest.setStatus(com.magna.xmbackend.vo.enums.Status.ACTIVE.name());
							siteAdminAreaRelRequestList.add(siteAdminAreaRelRequest);
							relObjMap.put(relObject.getRelId(), relObject);
							objectNames.add(((AdministrationArea) relObject.getRefObject()).getName());
						}
						SiteAdminAreaRelController siteAdminAreaRelController = new SiteAdminAreaRelController();
						SiteAdminAreaRelBatchResponse response = siteAdminAreaRelController
								.multiUpdateSiteAdminAreaRelStatus(siteAdminAreaRelRequestList);
						if (response != null) {
							List<String> statusUpdatationFailedList = response.getStatusUpdatationFailedList();
							if (!statusUpdatationFailedList.isEmpty()) {
								for (String id : statusUpdatationFailedList) {
									Iterator<Map.Entry<String, RelationObj>> iter = relObjMap.entrySet().iterator();
									while (iter.hasNext()) {
										Map.Entry<String, RelationObj> entry = iter.next();
										if (id.equalsIgnoreCase(entry.getKey())) {
											iter.remove();
										}
									}
								}
								Display.getDefault().asyncExec(new Runnable() {
									public void run() {
										CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
												messages.changeStatusConfirmDialogTitle,
												messages.multiStatusUpadteObjectDilaogErrMsg);
									}
								});
							}
							Iterator<Map.Entry<String, RelationObj>> iter = relObjMap.entrySet().iterator();
							while (iter.hasNext()) {
								Map.Entry<String, RelationObj> entry = iter.next();
								RelationObj obj = entry.getValue();
								obj.setActive(true);
							}

							Display.getDefault().asyncExec(new Runnable() {
								@Override
								public void run() {
									adminTree.refresh(true);
								}
							});
							for (Object name : objectNames) {
								XMAdminUtil.getInstance()
										.updateLogFile(messages.relationLbl + " " + messages.objectStatusUpdate + " "
												+ messages.administrationAreaObject + " '" + name + "' "
												+ messages.withLbl + " " + messages.siteObject + " '" + siteName + "' "
												+ messages.objectUpdate, MessageType.SUCCESS);
							}
						}
					} catch (UnauthorizedAccessException e) {

						Display.getDefault().asyncExec(new Runnable() {

							@Override
							public void run() {
								CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
										messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
							}
						});
					} catch (Exception e) {
						LOGGER.warn("Unable to activate site admin area relation model! ", e);
					}
					monitor.worked(70);
					monitor.done();
					return Status.OK_STATUS;
				}
			};
			activateSiteAARelJob.setUser(true);
			activateSiteAARelJob.setRule(this.jobSchedulingRule);
			activateSiteAARelJob.schedule();
		}
	}
}
