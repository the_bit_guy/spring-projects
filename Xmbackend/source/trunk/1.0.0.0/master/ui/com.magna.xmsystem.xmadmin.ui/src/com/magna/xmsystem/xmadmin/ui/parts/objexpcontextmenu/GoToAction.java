package com.magna.xmsystem.xmadmin.ui.parts.objexpcontextmenu;

import java.util.Map;

import javax.inject.Inject;

import org.eclipse.e4.core.services.nls.Translation;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreePath;

import com.magna.xmsystem.xmadmin.message.Message;
import com.magna.xmsystem.xmadmin.ui.parts.InformationPart;
import com.magna.xmsystem.xmadmin.ui.parts.objexp.ObjectExpPage;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.AdminTreeviewer;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.AdminTreeFactory;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.User;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.Users;
import com.magna.xmsystem.xmadmin.util.CommonConstants;
import com.magna.xmsystem.xmadmin.util.XMAdminUtil;

// TODO: Auto-generated Javadoc
/**
 * The Class GoToAction.
 */
public class GoToAction extends Action {

	/** The messages. */
	@Inject
	@Translation
	private Message messages;
	
	/** Inject of {@link EModelService}. */
	@Inject
	private EModelService modelService;

	/** Inject of {@link MApplication}. */
	@Inject
	private MApplication application;


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.action.Action#run()
	 */
	@Override
	public void run() {
		MPart mPart = (MPart) this.modelService.find(CommonConstants.PART_ID.INFORMATION_PART_ID, application);
		InformationPart rightSidePart2 = (InformationPart) mPart.getObject();
		ObjectExpPage objectExpPage = rightSidePart2.getObjectExpPartUI();
		IStructuredSelection selections = (IStructuredSelection) objectExpPage.getObjExpTableViewer().getSelection();
		Object selectionObj = selections.getFirstElement();
		AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
		TreePath[] selectionPaths = adminTree.getStructuredSelection().getPaths();
		if (selectionPaths != null && selectionPaths.length > 0) {
			adminTree.setExpandedState(selectionPaths[0], true);

			if (selectionObj instanceof User) {
				Users users = AdminTreeFactory.getInstance().getUsers();
				Map<String, IAdminTreeChild> userChildren = users.getUsersChildren();
				char startChar = (((User) selectionObj).getName().toString().toLowerCase().toCharArray()[0]);
				IAdminTreeChild iAdminTreeChild = userChildren.get(String.valueOf(startChar));
				adminTree.setExpandedState(iAdminTreeChild, true);
			}
		}
		adminTree.setSelection(new StructuredSelection(selectionObj), true);
	}

}
