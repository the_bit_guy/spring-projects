package com.magna.xmsystem.xmadmin.util;

/**
 * The Class LdapLoginCredentials.
 */
public class LdapLoginCredentials {

	/** The Constant LOCALHOST. */
	public static final  String LOCALHOST="ldap://192.168.1.109";
	
	/** The Constant PORT. */
	public static final  String PORT="389";
	
	/** The Constant SEARCH. */
	public static final  String SEARCH="ou=Restrict for users,dc=piterionindia,dc=com";
	
	public static final String ACCOUNT_NAME = "sAMAccountName";
	
	public static final String NAME = "name";
	
	public static final String EMAIL = "userPrincipalName";
		
}
