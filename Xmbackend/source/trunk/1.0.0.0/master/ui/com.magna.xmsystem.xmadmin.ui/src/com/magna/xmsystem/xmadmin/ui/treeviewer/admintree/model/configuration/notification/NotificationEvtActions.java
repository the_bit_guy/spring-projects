package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification;

import java.util.HashSet;
import java.util.Set;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.BeanModel;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;

// TODO: Auto-generated Javadoc
/**
 * The Class ProjectEvtActions.
 */
public abstract class NotificationEvtActions extends BeanModel implements IAdminTreeChild {

	/** Constant variable for desc text limit. */
	public static final int DESC_LIMIT = 240;

	/** Constant variable for name text limit. */
	public static final int NAME_LIMIT = 30;
	
	/** The Constant SUB_LIMIT. */
	public static final int SUB_LIMIT = 100;
	
	/** The Constant MESSAGE_LIMIT. */
	public static final int MESSAGE_LIMIT = 1500;

	/** PROPERTY_SITEID constant. */
	public static final String PROPERTY_ID = "Id"; //$NON-NLS-1$

	/** The Constant PROPERTY_NAME. */
	public static final String PROPERTY_NAME = "name"; //$NON-NLS-1$

	/** The Constant PROPERTY_ACTIVE. */
	public static final String PROPERTY_ACTIVE = "active"; //$NON-NLS-1$

	/** The Constant PROPERTY_DESCRIPTION. */
	public static final String PROPERTY_DESCRIPTION = "description"; //$NON-NLS-1$

	/** The Constant PROPERTY_TO. */
	public static final String PROPERTY_TO = "toUsers"; //$NON-NLS-1$

	/** The Constant PROPERTY_CC. */
	public static final String PROPERTY_CC = "ccUsers"; //$NON-NLS-1$

	/** The Constant PROPERTY_SUBJECT. */
	public static final String PROPERTY_SUBJECT = "subject"; //$NON-NLS-1$

	/** The Constant PROPERTY_MESSAGE. */
	public static final String PROPERTY_MESSAGE = "message"; //$NON-NLS-1$

	/** The Constant PROPERTY_OPERATION_MODE. */
	public static final String PROPERTY_OPERATION_MODE = "operationMode"; //$NON-NLS-1$

	/** The id. */
	private String id;

	/** The name. */
	private String name;

	/** The active. */
	private boolean active;

	/** The description. */
	private String description;

	/** The to users. */
	private Set<String> toUsers;

	/** The cc users. */
	private Set<String> ccUsers;

	/** The operation mode. */
	private int operationMode;

	/** The template. */
	private NotificationTemplate template;

	/**
	 * Instantiates a new project evt actions.
	 *
	 * @param id
	 *            the id
	 * @param name
	 *            the name
	 * @param isActive
	 *            the is active
	 * @param operationMode
	 *            the operation mode
	 */
	public NotificationEvtActions(final String id, final String name, final boolean isActive, final int operationMode) {
		this(id, name, isActive, null, new HashSet<>(), new HashSet<>(), null, operationMode);
	}

	/**
	 * Instantiates a new project evt actions.
	 *
	 * @param id
	 *            the id
	 * @param name
	 *            the name
	 * @param isActive
	 *            the is active
	 * @param description
	 *            the description
	 * @param toUsers
	 *            the to users
	 * @param ccUsers
	 *            the cc users
	 * @param subject
	 *            the subject
	 * @param message
	 *            the message
	 * @param operationMode
	 *            the operation mode
	 */
	public NotificationEvtActions(final String id, final String name, final boolean isActive, final String description,
			final Set<String> toUsers, final Set<String> ccUsers, final NotificationTemplate template,
			final int operationMode) {
		super();
		this.id = id;
		this.name = name;
		this.active = isActive;
		this.description = description;
		this.toUsers = toUsers;
		this.ccUsers = ccUsers;
		this.template = template;
		this.operationMode = operationMode;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id
	 *            the new id
	 */
	public void setId(String id) {
		/*if (id == null)
			throw new IllegalArgumentException();*/
		this.propertyChangeSupport.firePropertyChange(PROPERTY_ID, this.id, this.id = id);
		// this.id = id;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name
	 *            the new name
	 */
	public void setName(String name) {
		if (name == null)
			throw new IllegalArgumentException();
		this.propertyChangeSupport.firePropertyChange(PROPERTY_NAME, this.name, this.name = name.trim());
		// this.name = name;
	}

	/**
	 * Checks if is active.
	 *
	 * @return true, if is active
	 */
	public boolean isActive() {
		return active;
	}

	/**
	 * Sets the active.
	 *
	 * @param isActive
	 *            the new active
	 */
	public void setActive(boolean isActive) {
		this.propertyChangeSupport.firePropertyChange(PROPERTY_ACTIVE, this.active, this.active = isActive);
		// this.active = active;
	}

	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description.
	 *
	 * @param description
	 *            the new description
	 */
	public void setDescription(String description) {
		if (description == null)
			throw new IllegalArgumentException();
		this.propertyChangeSupport.firePropertyChange(PROPERTY_DESCRIPTION, this.description,
				this.description = description.trim());
		// this.description = description;
	}

	/**
	 * Gets the to users.
	 *
	 * @return the to users
	 */
	public Set<String> getToUsers() {
		return toUsers;
	}

	/**
	 * Sets the to users.
	 *
	 * @param toUsers
	 *            the new to users
	 */
	public void setToUsers(Set<String> toUsers) {
		if (toUsers == null)
			throw new IllegalArgumentException();
		this.propertyChangeSupport.firePropertyChange(PROPERTY_TO, this.toUsers, this.toUsers = toUsers);
		// this.toUsers = toUsers;
	}

	/**
	 * Gets the cc users.
	 *
	 * @return the cc users
	 */
	public Set<String> getCcUsers() {
		return ccUsers;
	}

	/**
	 * Sets the cc users.
	 *
	 * @param ccUsers
	 *            the new cc users
	 */
	public void setCcUsers(Set<String> ccUsers) {
		if (ccUsers == null)
			throw new IllegalArgumentException();
		this.propertyChangeSupport.firePropertyChange(PROPERTY_CC, this.ccUsers, this.ccUsers = ccUsers);
		// this.ccUsers = ccUsers;
	}


	/**
	 * Gets the template.
	 *
	 * @return the template
	 */
	public NotificationTemplate getTemplate() {
		return template;
	}

	/**
	 * Sets the template.
	 *
	 * @param template
	 *            the new template
	 */
	public void setTemplate(NotificationTemplate template) {
		this.template = template;
	}

	/**
	 * Gets the operation mode.
	 *
	 * @return the operation mode
	 */
	public int getOperationMode() {
		return operationMode;
	}

	/**
	 * Sets the operation mode.
	 *
	 * @param operationMode
	 *            the new operation mode
	 */
	public void setOperationMode(int operationMode) {
		this.propertyChangeSupport.firePropertyChange(PROPERTY_OPERATION_MODE, this.operationMode,
				this.operationMode = operationMode);
	}

}
