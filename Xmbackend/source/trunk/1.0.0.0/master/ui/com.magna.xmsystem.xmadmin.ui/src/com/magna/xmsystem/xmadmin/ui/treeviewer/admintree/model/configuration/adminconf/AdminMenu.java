package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.adminconf;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;

/**
 * The Class AdminMenu.
 * 
 * @author shashwat.anand
 */
public class AdminMenu implements IAdminTreeChild {
	/** The project children. */
	private Map<String, IAdminTreeChild> adminMenuChildren;
	
	/** The parent. */
	private IAdminTreeChild parent;

	/**
	 * Instantiates a new admin menu.
	 *
	 * @param parent the parent
	 */
	public AdminMenu(IAdminTreeChild parent) {
		this.parent = parent;
		this.adminMenuChildren = new LinkedHashMap<>();
		this.adminMenuChildren.put(AdminUsers.class.getSimpleName(), new AdminUsers(this));
		this.adminMenuChildren.put(AdminUserApps.class.getSimpleName(), new AdminUserApps(this));
		this.adminMenuChildren.put(AdminProjectApps.class.getSimpleName(), new AdminProjectApps(this));
	}
		
	/**
	 * Gets the admin menu collection.
	 *
	 * @return the admin menu collection
	 */
	public Collection<IAdminTreeChild> getAdminMenuCollection() {
		return this.adminMenuChildren.values();
	}

	/**
	 * Gets the roles children.
	 *
	 * @return the roles children
	 */
	public Map<String, IAdminTreeChild> getAdminMenuChildren() {
		return adminMenuChildren;
	}

	/* (non-Javadoc)
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#getParent()
	 */
	@Override
	public IAdminTreeChild getParent() {
		return this.parent;
	}

	/* (non-Javadoc)
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#setParent(com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild)
	 */
	@Override
	public void setParent(IAdminTreeChild parent) {
		this.parent = parent;
	}
}
