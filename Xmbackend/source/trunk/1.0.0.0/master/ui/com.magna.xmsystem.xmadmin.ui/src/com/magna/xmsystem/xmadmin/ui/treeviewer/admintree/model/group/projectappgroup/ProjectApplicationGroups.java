package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.projectappgroup;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;

/**
 * The Class ProjectApplicationGroups.
 */
public class ProjectApplicationGroups implements IAdminTreeChild {
	/** The parent. */
	private IAdminTreeChild parent;
	
	/** The user app groups children. */
	private Map<String, IAdminTreeChild> projectAppGroupsChildren;

	/**
	 * Constructor.
	 */
	public ProjectApplicationGroups() {
		this.parent = null;
		this.projectAppGroupsChildren = new LinkedHashMap<>();
	}

	/**
	 * Adds the child.
	 *
	 * @param projectAppGroupId {@link String}
	 * @param child {@link IAdminTreeChild}
	 * @return {@link IAdminTreeChild}
	 */
	public IAdminTreeChild add(final String projectAppGroupId, final IAdminTreeChild child) {
		child.setParent(this);
		IAdminTreeChild returnVal = this.projectAppGroupsChildren.put(projectAppGroupId, child);
		sort();
		return returnVal;
	}

	/**
	 * Removes the child.
	 *
	 * @param projectAppGroupId the group name
	 * @return {@link IAdminTreeChild}
	 */
	public IAdminTreeChild remove(final String projectAppGroupId) {
		return this.projectAppGroupsChildren.remove(projectAppGroupId);
	}

	

	/**
	 * Gets the project app groups collection.
	 *
	 * @return the project app groups collection
	 */
	public Collection<IAdminTreeChild> getProjectAppGroupsCollection() {
		return this.projectAppGroupsChildren.values();
	}

	
	
	/**
	 * Gets the project app groups children.
	 *
	 * @return the project app groups children
	 */
	public Map<String, IAdminTreeChild> getProjectAppGroupsChildren() {
		return projectAppGroupsChildren;
	}
	
	/**
	 * Returns null as its parent.
	 *
	 * @return the parent
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#getParent()
	 */
	@Override
	public IAdminTreeChild getParent() {
		return this.parent;
	}

	/**
	 * Sets the parent.
	 *
	 * @param parent the new parent
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#setParent(com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild)
	 */
	@Override
	public void setParent(final IAdminTreeChild parent) {
		this.parent = parent;
	}
	
	/**
	 * Sort.
	 */
	public void sort() {
		final LinkedHashMap<String, IAdminTreeChild> collect = this.projectAppGroupsChildren.entrySet().stream().sorted(
				(e1, e2) -> ((ProjectApplicationGroup) e1.getValue()).getName().compareTo(((ProjectApplicationGroup) e2.getValue()).getName()))
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
		this.projectAppGroupsChildren = collect;
	}
}
