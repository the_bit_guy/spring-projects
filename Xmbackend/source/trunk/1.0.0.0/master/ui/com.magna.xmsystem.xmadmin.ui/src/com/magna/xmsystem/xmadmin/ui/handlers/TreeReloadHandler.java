
package com.magna.xmsystem.xmadmin.ui.handlers;

import java.util.List;

import javax.inject.Inject;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.ui.di.UIEventTopic;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

import com.magna.xmsystem.xmadmin.ui.parts.IEditablePart;
import com.magna.xmsystem.xmadmin.ui.parts.InformationPart;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.AdminTreeviewer;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.AdminTreeFactory;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.Sites;
import com.magna.xmsystem.xmadmin.util.CommonConstants;
import com.magna.xmsystem.xmadmin.util.NavigationStack;
import com.magna.xmsystem.xmadmin.util.XMAdminUtil;

/**
 * Class for Tree reload handler.
 *
 * @author Chiranjeevi.Akula
 */
public class TreeReloadHandler {

	/** Member variable 'hide in active items' for {@link Boolean}. */
	private boolean hideInActiveItems;
	
	/**
	 * Inject of {@link EModelService}
	 */
	@Inject
	private EModelService modelService;
	
	/**
	 * Inject of {@link MApplication}
	 */
	@Inject
	private MApplication application;

	/** The tree reload job. */
	private Job treeReloadJob;
	
	/**
	 * Execute method of Reload Admin Tree handler.
	 *
	 * @param shell
	 *            {@link Shell}
	 */
	@Execute
	public void execute() {
		AdminTreeviewer adminTreeViewer;
		AdminTreeFactory adminTreeFactory;
		MPart mPart = (MPart) this.modelService.find(CommonConstants.PART_ID.INFORMATION_PART_ID, application);
		InformationPart rightSidePart = (InformationPart) mPart.getObject();
		Composite rightcomposite = rightSidePart.getComposite();
		/*StackLayout rightCompLayout = rightSidePart.getCompLayout();*/
		if ((adminTreeViewer = XMAdminUtil.getInstance().getAdminTree()) != null
				&& (adminTreeFactory = AdminTreeFactory.getInstance()) != null) {
			
			if (treeReloadJob != null && treeReloadJob.getState() == Job.RUNNING) {
				return;
			}
		    treeReloadJob = new Job("Reloading Tree...") {
				
				@Override
				protected IStatus run(IProgressMonitor monitor) {
					monitor.beginTask("Reloding Tree...", 100);
					monitor.worked(30);
					
					List<IAdminTreeChild> adminTreeChilds = adminTreeFactory.getAdminTreeNodes();
					if (adminTreeChilds != null) {
						adminTreeChilds.clear();
					}
					List<IAdminTreeChild> createInitialModel = adminTreeFactory.createInitialModel(hideInActiveItems);
					Display.getDefault().syncExec(new Runnable() {
						public void run() {
							adminTreeViewer.setInput(null);
							adminTreeViewer.setInput(createInitialModel);
							rightcomposite.layout();
						}
					});
					adminTreeViewer.setBackwardStack(null);
					adminTreeViewer.setForwardStack(null);
					adminTreeViewer.setBackwardStack(new NavigationStack<>());
					adminTreeViewer.setForwardStack(new NavigationStack<>());	
					final Sites sites = adminTreeFactory.getSites();
					Display.getDefault().syncExec(new Runnable() {
						public void run() {
							adminTreeViewer.getTree().setFocus();
							adminTreeViewer.refresh();
							adminTreeViewer.setSelection(new StructuredSelection(sites), true);
						}
					});
					monitor.worked(70);
					return Status.OK_STATUS;
				}
			};
			treeReloadJob.setUser(true);
			treeReloadJob.schedule();
			
			/*WelcomePage welcomePageUI = rightSidePart.getWelcomepage();
			rightCompLayout.topControl = welcomePageUI;
			welcomePageUI.updateContent();*/
			
		}
	}
	
	@Inject
	@Optional
	public void getEvent(@UIEventTopic(CommonConstants.EVENT_BROKER.HIDEINACTIVEITEMS) final boolean isHideMenuItemSelected) {
		hideInActiveItems = isHideMenuItemSelected;
	}

	/**
	 * Method for canExecute.
	 *
	 * @return boolean
	 */
	@CanExecute
	public boolean canExecute() {
		MPart rightHandMPartOfOldPerspective = (MPart) this.modelService
				.find(CommonConstants.PART_ID.INFORMATION_PART_ID, application);
		if (rightHandMPartOfOldPerspective.getObject() instanceof IEditablePart
				&& rightHandMPartOfOldPerspective.isDirty()) {
			return false;
		}else if (treeReloadJob != null && treeReloadJob.getState() == Job.RUNNING) {
			return false;
		}
		return true;
	}

}