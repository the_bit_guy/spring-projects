package com.magna.xmsystem.xmadmin.ui.handlers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.inject.Inject;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.e4.core.contexts.Active;
import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.core.services.nls.Translation;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.model.application.ui.menu.MToolItem;
import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.widgets.Display;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.ResourceAccessException;

import com.magna.xmbackend.exception.CannotCreateObjectException;
import com.magna.xmbackend.vo.adminMenu.AdminMenuConfigResponse;
import com.magna.xmbackend.vo.rel.AdminAreaProjectAppRelResponse;
import com.magna.xmbackend.vo.rel.AdminAreaProjectRelResponse;
import com.magna.xmbackend.vo.rel.AdminAreaStartAppRelResponse;
import com.magna.xmbackend.vo.rel.AdminAreaUserAppRelResponse;
import com.magna.xmbackend.vo.rel.ProjectStartAppRelResponse;
import com.magna.xmbackend.vo.rel.RoleAdminAreaRelResponse;
import com.magna.xmbackend.vo.rel.RoleUserRelResponse;
import com.magna.xmbackend.vo.rel.SiteAdminAreaRelResponse;
import com.magna.xmbackend.vo.rel.UserProjectAppRelResponse;
import com.magna.xmbackend.vo.rel.UserProjectRelResponse;
import com.magna.xmbackend.vo.rel.UserStartAppRelResponse;
import com.magna.xmbackend.vo.rel.UserUserAppRelResponse;
import com.magna.xmsystem.dependencies.customExceptions.UnauthorizedAccessException;
import com.magna.xmsystem.ui.controls.widgets.CustomMessageDialog;
import com.magna.xmsystem.xmadmin.message.Message;
import com.magna.xmsystem.xmadmin.restclient.relation.AdminAreaProjectAppRelController;
import com.magna.xmsystem.xmadmin.restclient.relation.AdminAreaProjectRelController;
import com.magna.xmsystem.xmadmin.restclient.relation.AdminAreaStartAppRelController;
import com.magna.xmsystem.xmadmin.restclient.relation.AdminAreaUserAppRelController;
import com.magna.xmsystem.xmadmin.restclient.relation.AdminMenuConfigRelController;
import com.magna.xmsystem.xmadmin.restclient.relation.DirectoryRelController;
import com.magna.xmsystem.xmadmin.restclient.relation.GroupRelController;
import com.magna.xmsystem.xmadmin.restclient.relation.ProjectStartAppRelController;
import com.magna.xmsystem.xmadmin.restclient.relation.RoleAdminAreaRelController;
import com.magna.xmsystem.xmadmin.restclient.relation.RoleUserRelController;
import com.magna.xmsystem.xmadmin.restclient.relation.SiteAdminAreaRelController;
import com.magna.xmsystem.xmadmin.restclient.relation.UserProjectAppRelController;
import com.magna.xmsystem.xmadmin.restclient.relation.UserProjectRelController;
import com.magna.xmsystem.xmadmin.restclient.relation.UserStartAppRelController;
import com.magna.xmsystem.xmadmin.restclient.relation.UserUserAppRelController;
import com.magna.xmsystem.xmadmin.ui.parts.InformationPart;
import com.magna.xmsystem.xmadmin.ui.parts.objexp.ObjectExpPage;
import com.magna.xmsystem.xmadmin.ui.parts.objexp.ObjectExplorer;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.AdminTreeviewer;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaProjectAppFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaProjectAppNotFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaProjectAppProtected;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaProjectApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaProjectChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaProjectStartApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaProjects;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaStartApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaUserAppFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaUserAppNotFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaUserAppProtected;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaUserApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdministrationArea;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.projectapplicationmodel.ProjectAppAdminAreas;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.projectapplicationmodel.ProjectApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.startapplicationmodel.StartApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.userapplicationmodel.UserApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.adminconf.AdminProjectApps;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.adminconf.AdminUserApps;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.adminconf.AdminUsers;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.Role;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.RoleScopeObjectChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.RoleScopeObjectUsers;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.RoleScopeObjects;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.RoleUsers;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.directory.Directory;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.directory.DirectoryProjectApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.directory.DirectoryProjects;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.directory.DirectoryUserApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.directory.DirectoryUsers;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.projectappgroup.ProjectAppGroupProjectApps;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.projectappgroup.ProjectApplicationGroup;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.projectgroup.ProjectGroupModel;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.projectgroup.ProjectGroupProjects;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.userappgroup.UserAppGroupUserApps;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.userappgroup.UserApplicationGroup;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.usergroup.UserGroupModel;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.usergroup.UserGroupUsers;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.Project;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectAdminAreaChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectAdminAreaProjectAppFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectAdminAreaProjectAppNotFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectAdminAreaProjectAppProtected;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectAdminAreaProjectApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectAdminAreaStartApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectAdminAreas;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectUserAAProjectAppAllowed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectUserAAProjectAppForbidden;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectUserAAProjectApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectUserChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectUsers;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.refs.RelationObj;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.Site;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaProjectApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaProjectChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaProjectStartApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaProjects;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaStartApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaUserAppFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaUserAppNotFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaUserAppProtected;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaUserApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminProjectAppFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminProjectAppNotFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminProjectAppProtected;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdministrationChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdministrations;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.User;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserAAUserAppAllowed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserAAUserAppForbidden;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserAAUserApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserAdminAreas;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserProjectAAProjectAppAllowed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserProjectAAProjectAppForbidden;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserProjectAAProjectApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserProjectAdminAreas;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserProjectChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserProjects;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserStartApplications;
import com.magna.xmsystem.xmadmin.util.CommonConstants;
import com.magna.xmsystem.xmadmin.util.MessageType;
import com.magna.xmsystem.xmadmin.util.XMAdminUtil;

/**
 * The Class RemoveRelationHandler.
 * 
 * @author subash.janarthanan
 * 
 */
public class RemoveRelationHandler {
	
	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(RemoveRelationHandler.class);

	/** Inject of {@link EModelService}. */
	@Inject
	private EModelService modelService;

	/** Inject of {@link MApplication}. */
	@Inject
	private MApplication application;

	/** Member variable for messages. */
	@Inject
	@Translation
	transient private Message messages;
	
	private boolean isAdminTreeObject;

	/**
	 * Execute.
	 *
	 * @throws Exception
	 *             the exception
	 */
	
	@Execute
	public void execute() throws Exception {
		Object selectionObj = null;
		isAdminTreeObject = false;
		MPart mPart = (MPart) this.modelService.find(CommonConstants.PART_ID.INFORMATION_PART_ID, application);
		InformationPart rightSidePart2 = (InformationPart) mPart.getObject();
		ObjectExpPage objectExpPage = rightSidePart2.getObjectExpPartUI();
		final ObjectExplorer objExpTableViewer = objectExpPage.getObjExpTableViewer();
		IStructuredSelection selections = (IStructuredSelection) objExpTableViewer.getSelection();
		selectionObj = selections.getFirstElement();
		if(selectionObj == null){
			isAdminTreeObject = true;
			selections = (IStructuredSelection) XMAdminUtil.getInstance().getAdminTree().getSelection();
			selectionObj = selections.getFirstElement();
		}
		if (objectExpPage != null && selections != null && selectionObj != null) {

			if (selectionObj instanceof RelationObj) {
				RelationObj relationObj = (RelationObj) selectionObj;
				IAdminTreeChild refObject = relationObj.getRefObject();
				if (refObject instanceof AdministrationArea) {
					if (relationObj.getContainerObj() instanceof SiteAdministrationChild) {
						deleteSiteAARel(selections, objectExpPage);
					} else if (relationObj.getContainerObj() instanceof RoleScopeObjectChild) {
						deleteRoleScopeObjectChildRel(selections, objectExpPage);
					} else if(relationObj.getContainerObj() instanceof ProjectAdminAreaChild){
						deleteProjectAARel(selections, objectExpPage);
					}
				} else if (refObject instanceof Project) {
					IAdminTreeChild iAdminTreeChild = relationObj.getContainerObj();
					if (iAdminTreeChild instanceof SiteAdminAreaProjectChild
							|| iAdminTreeChild instanceof AdminAreaProjectChild) {
						deleteSiteAAProjectRel(selections, objectExpPage);
					} else if (iAdminTreeChild instanceof UserProjectChild) {
						deleteUserProjectRel(selections, objectExpPage);
					} else if (iAdminTreeChild instanceof DirectoryProjects) {
						deleteDirectoryProjectsRel(selections, objectExpPage);
					} else if (iAdminTreeChild instanceof ProjectGroupProjects) {
						deleteProjectGroupProjectsRel(selections, objectExpPage);
					}

				} else if (refObject instanceof UserApplication) {
					IAdminTreeChild iAdminTreeChild = getContainerObject(relationObj);
					if (iAdminTreeChild == null) {
						iAdminTreeChild = relationObj.getContainerObj();
					}
					if (iAdminTreeChild instanceof SiteAdminAreaUserApplications
							|| iAdminTreeChild instanceof AdminAreaUserApplications) {
						deleteSiteAAUserAppRel(selections, objectExpPage);
					} else if (iAdminTreeChild instanceof UserAAUserApplications) {
						deleteUserAAUserAppRel(selections, objectExpPage);
					} else if (iAdminTreeChild instanceof AdminUserApps) {
						deleteAdminUserAppRel(selections, objectExpPage);
					} else if (iAdminTreeChild instanceof DirectoryUserApplications) {
						deleteDirectoryUserAppRel(selections, objectExpPage);
					} else if (iAdminTreeChild instanceof UserAppGroupUserApps) {
						deleteUserAppGrpUserAppRel(selections, objectExpPage);
					}
				} else if (refObject instanceof ProjectApplication) {
					IAdminTreeChild iAdminTreeChild = getContainerObject(relationObj);
					if (iAdminTreeChild == null) {
						iAdminTreeChild = relationObj.getContainerObj();
					}

					if (iAdminTreeChild instanceof SiteAdminAreaProjectApplications
							|| iAdminTreeChild instanceof AdminAreaProjectApplications) {
						deleteSiteAAProjectAppRel(selections, objectExpPage);
					} else if (iAdminTreeChild instanceof UserProjectAAProjectApplications
							|| iAdminTreeChild instanceof ProjectUserAAProjectApplications) {
						deleteUserProjectAAProjectAppRel(selections, objectExpPage);
					} else if (relationObj.getContainerObj() instanceof AdminProjectApps) {
						deleteAdminProjectAppRel(selections, objectExpPage);
					} else if (relationObj.getContainerObj() instanceof DirectoryProjectApplications) {
						deleteDirectoryProjectAppRel(selections, objectExpPage);
					} else if (relationObj.getContainerObj() instanceof ProjectAppGroupProjectApps) {
						deleteProjectAppGrpProjectAppRel(selections, objectExpPage);
					} else if (iAdminTreeChild instanceof ProjectAdminAreaProjectApplications) {
						deleteProjectAAProjectAppRel(selections, objectExpPage);
					}
				} else if (refObject instanceof StartApplication) {
					if (relationObj.getContainerObj() instanceof SiteAdminAreaStartApplications
							|| relationObj.getContainerObj() instanceof AdminAreaStartApplications) {
						deleteSiteAAStartAppRel(selections, objectExpPage);
					} else if (relationObj.getContainerObj() instanceof SiteAdminAreaProjectStartApplications) {
						deleteSiteAAProjectStartAppRel(selections, objectExpPage);
					} else if (relationObj.getContainerObj() instanceof AdminAreaProjectStartApplications) {
						deleteAAProjectStartAppRel(selections, objectExpPage);
					} else if (relationObj.getContainerObj() instanceof UserStartApplications) {
						deleteUserStartAppRel(selections, objectExpPage);
					} else if (relationObj.getContainerObj() instanceof ProjectAdminAreaStartApplications) {
						deleteProjectAAProjectStartAppRel(selections, objectExpPage);
					}
				} else if (refObject instanceof User) {
					if (relationObj.getContainerObj() instanceof ProjectUserChild) {
						deleteProjectUserRel(selections, objectExpPage);
					} else if (relationObj.getContainerObj() instanceof AdminUsers) {
						deleteAdminUserRel(selections, objectExpPage);
					} else if (relationObj.getContainerObj() instanceof RoleUsers) {
						deleteRoleUserRel(selections, objectExpPage);
					} else if (relationObj.getContainerObj() instanceof RoleScopeObjectUsers) {
						deleteRoleScopeObjectUserRel(selections, objectExpPage);
					} else if (relationObj.getContainerObj() instanceof DirectoryUsers) {
						deleteDirectoryUsersRel(selections, objectExpPage);
					} else if (relationObj.getContainerObj() instanceof UserGroupUsers) {
						deleteUserGroupUserRel(selections, objectExpPage);
					}
				}
			}
		}
		XMAdminUtil.getInstance().clearClipBoardContents();
	}

	/**
	 * Delete project AA rel.
	 *
	 * @param selection the selection
	 * @param objectExpPage the object exp page
	 */
	@SuppressWarnings("rawtypes")
	private void deleteProjectAARel(IStructuredSelection selection, ObjectExpPage objectExpPage) {
		Object selectionObj = selection.getFirstElement();
		RelationObj relationObj = (RelationObj) selectionObj;
		final IAdminTreeChild containerObj = relationObj.getContainerObj();
		try {
			if (!XMAdminUtil.getInstance().isRelObjAccessAllowed(containerObj,
					"PROJECT_TO_ADMINISTRATION_AREA-REMOVE")) {
				CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
						messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
				return;
			}
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				CustomMessageDialog.openError(Display.getCurrent().getActiveShell(), messages.errorDialogTitile,
						messages.serverNotReachable);
			}
			LOGGER.error("Exception occured in calling access allowed API " + e);
			return;
		}
		final XMAdminUtil instance = XMAdminUtil.getInstance();
		final AdminTreeviewer adminTree = instance.getAdminTree();
		final String confirmDialogTitle = messages.deleteConfirmDialogTitle;
		String confirmDialogMsg = null;
		final List selectionList = selection.toList();
		final Map<String,String> successObjectNames = new HashMap<>();
		final Map<String,String> failObjectNames = new HashMap<>();
		for (int i = 0; i < selectionList.size(); i++) {
			Object seleObj = selectionList.get(i);
			RelationObj relObj = (RelationObj) seleObj;
			successObjectNames.put(relObj.getRelId(),((AdministrationArea)relObj.getRefObject()).getName());
		}
		// RelationObj relationObj = (RelationObj) selection.getFirstElement();
		IAdminTreeChild refObject = relationObj.getRefObject();
		ProjectAdminAreas projectAdminAreas = (ProjectAdminAreas) relationObj.getParent();
		final String adminAreaName = ((AdministrationArea) refObject).getName();
		final String projectName = ((Project) projectAdminAreas.getParent()).getName();
		if (selectionList.size() == 1) {
			confirmDialogMsg = messages.deleteAARelationConfirmDialogMsgPart1 + " \'"
					+ adminAreaName + "\' " + messages.deleteProjectRelationMsg + " \'"
					+ projectName + "' ?";
		} else {
			confirmDialogMsg = messages.removeMultiObjectsRelConfirmDialogMsg + " " + messages.deleteProjectRelationMsg
					+ " \'" + ((Project) projectAdminAreas.getParent()).getName() + "' ?";
		}
		boolean isConfirmed = CustomMessageDialog.openConfirm(adminTree.getControl().getShell(), confirmDialogTitle,
				confirmDialogMsg);

		if (isConfirmed) {
			Job job = new Job("Removing Relations...") {
				@Override
				protected IStatus run(IProgressMonitor monitor) {
					monitor.beginTask("Removing Relations..", 100);
					monitor.worked(30);
					try {
						for (int i = 0; i < selectionList.size(); i++) {
							RelationObj relObject = (RelationObj) selectionList.get(i);
							String relId = relObject.getRelId();
							AdminAreaProjectRelController adminAreaProjectRelController = new AdminAreaProjectRelController(
									XMAdminUtil.getInstance().getAAForHeaders(relObject));
							final Set<String> relIdList = new HashSet<>();
							relIdList.add(relId);
							AdminAreaProjectRelResponse adminAreaProjectRelResponse;
							adminAreaProjectRelResponse = adminAreaProjectRelController
									.deleteMultiAdminAreaProjectRel(relIdList);
							if (adminAreaProjectRelResponse != null) {
								List<Map<String, String>> statusMaps = adminAreaProjectRelResponse.getStatusMaps();
								if (!statusMaps.isEmpty()) {
									for (Map<String, String> statusMap : statusMaps) {
										String string = statusMap.get("en");
										String[] split = string.split("id");
										failObjectNames.put(split[1].trim(), successObjectNames.get(split[1].trim()));
										successObjectNames.remove(split[1].trim());
									}
								} else {
									ProjectAdminAreas projectAdminAreas = (ProjectAdminAreas) relObject.getParent();
									projectAdminAreas.remove(relObject.getRelId());
								}
							}
						}

						Display.getDefault().asyncExec(new Runnable() {
							@SuppressWarnings("unchecked")
							public void run() {
								adminTree.refresh(((RelationObj) (selection.getFirstElement())).getParent());
								adminTree.refresh(true);
								if (isAdminTreeObject) {
									XMAdminUtil.getInstance().getAdminTree().setSelection(new StructuredSelection(((IAdminTreeChild)selection.getFirstElement()).getParent()));
								}
								objectExpPage.refreshExplorer();
								adminTree.refershBackReference(new Class[] { AdminAreaProjects.class,
										SiteAdminAreaProjects.class, UserProjectAdminAreas.class });
							}
						});
						
						if (successObjectNames.size() > 0) {
							Iterator it = successObjectNames.entrySet().iterator();
							while (it.hasNext()) {
								Map.Entry map = (Map.Entry) it.next();
								XMAdminUtil.getInstance().updateLogFile(
										messages.relationLbl + " " + messages.betweenLbl + " "
												+ messages.administrationAreaObject +" '" + map.getValue() + "' "
												+ messages.andLbl + " " + messages.projectObject +" '"
												+ projectName + "' " + messages.objectDelete,
										MessageType.SUCCESS);
							}
						}
						
						if (failObjectNames.size() > 0) {
							Display.getDefault().asyncExec(new Runnable() {
								public void run() {
									CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
											messages.deleteConfirmDialogTitle,
											messages.deleteMultiObjectsDialogErrMsg);
								}
							});
						}
						
						if (failObjectNames.size() > 0) {
							Iterator it = failObjectNames.entrySet().iterator();
							while (it.hasNext()) {
								Map.Entry map = (Map.Entry) it.next();
								XMAdminUtil.getInstance().updateLogFile(
										messages.deleteErrorMsg + " " + messages.administrationAreaObject + " '"
												+ map.getValue() + "' " + messages.relationLbl + " " + messages.withLbl
												+ " " + messages.projectObject + " " + "'" + projectName + "' ",
										MessageType.FAILURE);
							}
						}
					} catch (UnauthorizedAccessException e) {
						Display.getDefault().asyncExec(new Runnable() {
							public void run() {
								CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
										messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
							}
						});
					}
					monitor.worked(70);
					return Status.OK_STATUS;
				}
			};
			job.setUser(true);
			job.schedule();
		}
	}

	/**
	 * Delete project AA project app rel.
	 *
	 * @param selection
	 *            the selection
	 * @param objectExpPage
	 *            the object exp page
	 */
	@SuppressWarnings("rawtypes")
	private void deleteProjectAAProjectAppRel(final IStructuredSelection selection, final ObjectExpPage objectExpPage) {
		Object selectionObj = selection.getFirstElement();
		RelationObj relationObj = (RelationObj) selectionObj;
		final IAdminTreeChild containerObj = relationObj.getContainerObj();
		try {
			if (!XMAdminUtil.getInstance().isRelObjAccessAllowed(containerObj,
					"PROJECT_APPLICATION_TO_PROJECT-REMOVE")) {
				CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
						messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
				return;
			}
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				CustomMessageDialog.openError(Display.getCurrent().getActiveShell(), messages.errorDialogTitile,
						messages.serverNotReachable);
			}
			LOGGER.error("Exception occured in calling access allowed API " + e);
			return;
		}
		final XMAdminUtil instance = XMAdminUtil.getInstance();
		final AdminTreeviewer adminTree = instance.getAdminTree();
		final String confirmDialogTitle = messages.deleteConfirmDialogTitle;
		String confirmDialogMsg = null;
		final List selectionList = selection.toList();
		final Set<String> relationIdList = new HashSet<>();
		final Map<String,String> successObjectNames = new HashMap<>();
		final Map<String,String> failObjectNames = new HashMap<>();
		for (int i = 0; i < selectionList.size(); i++) {
			Object seleObj = selectionList.get(i);
			RelationObj relObj = (RelationObj) seleObj;
			relationIdList.add(relObj.getRelId());
			successObjectNames.put(relObj.getRelId(), ((ProjectApplication)relObj.getRefObject()).getName());
		}
		IAdminTreeChild refObject = relationObj.getRefObject();
		IAdminTreeChild containerObject = getContainerObject(relationObj);
		final String projectAppName = ((ProjectApplication) refObject).getName() ;
		final String adminAreaName =  ((AdministrationArea) ((RelationObj) containerObject.getParent()).getRefObject()).getName();
		final String projectName =  ((Project) (containerObject.getParent().getParent().getParent())).getName();
		if (selectionList.size() == 1) {
			
			confirmDialogMsg = messages.deleteProjectAppConfirmDialogMsgPart1 + " \'"
					+ projectAppName + "\' " + messages.deleteAdminAreaRelationMsg + " \'"
					+ adminAreaName
					+ "' ?";
		} else {
			confirmDialogMsg = messages.removeMultiObjectsRelConfirmDialogMsg + " "
					+ messages.deleteAdminAreaRelationMsg + " \'"
					+ adminAreaName
					+ "' ?";
		}
		AdminAreaProjectAppRelController adminAreaProjectAppRelController = new AdminAreaProjectAppRelController(
				XMAdminUtil.getInstance()
						.getAAForHeaders(getContainerObject((RelationObj) selection.getFirstElement())));
		boolean isConfirmed = CustomMessageDialog.openConfirm(adminTree.getControl().getShell(), confirmDialogTitle,
				confirmDialogMsg);

		if (isConfirmed) {
			Job job = new Job("Removing Relations...") {
				@Override
				protected IStatus run(IProgressMonitor monitor) {
					monitor.beginTask("Removing Relations..", 100);
					monitor.worked(30);
					try {
						AdminAreaProjectAppRelResponse adminAreaProjectAppRelResponse;
						adminAreaProjectAppRelResponse = adminAreaProjectAppRelController
								.deleteMultiAdminAreaProjAppRel(relationIdList);
						if (adminAreaProjectAppRelResponse != null) {
							List<Map<String, String>> statusMaps = adminAreaProjectAppRelResponse.getStatusMaps();
							if (!statusMaps.isEmpty()) {
								for (Map<String, String> statusMap : statusMaps) {
									String string = statusMap.get("en");
									String[] split = string.split("id");
									failObjectNames.put(split[1].trim(), successObjectNames.get(split[1].trim()));
									successObjectNames.remove(split[1].trim());
								}
								XMAdminUtil.getInstance().updateLogFile(
										messages.deleteMultiObjectsDialogErrMsg, MessageType.SUCCESS);
								Display.getDefault().asyncExec(new Runnable() {
									public void run() {
										CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
												messages.deleteConfirmDialogTitle,
												messages.deleteMultiObjectsDialogErrMsg);
									}
								});
							}

							Display.getDefault().asyncExec(new Runnable() {
								public void run() {
									objectExpPage.refreshExplorer();
								}
							});
							
							if (successObjectNames.size() > 0) {
								Iterator it = successObjectNames.entrySet().iterator();
								while (it.hasNext()) {
									Map.Entry map = (Map.Entry) it.next();
									XMAdminUtil.getInstance()
											.updateLogFile(messages.relationLbl + " " + messages.betweenLbl + " "
													+ messages.projectApplicationObject + " '" + map.getValue()
													+ "' " + messages.andLbl + " " + messages.administrationAreaObject
													+ " '" + adminAreaName + "' " + messages.withLbl + " "
													+ messages.projectObject + " '" + projectName + "' "
													+ messages.objectDelete, MessageType.SUCCESS);
								}
							}
							if (failObjectNames.size() > 0) {
								Iterator it = failObjectNames.entrySet().iterator();
								while (it.hasNext()) {
									Map.Entry map = (Map.Entry) it.next();
									XMAdminUtil.getInstance().updateLogFile(messages.deleteErrorMsg + " "
											+messages.projectApplicationObject +" '" + map.getValue()
											+ "' "+messages.relationLbl + " " + messages.withLbl
											+ " " + messages.administrationAreaObject
											+ " '" + adminAreaName + "' " + messages.andLbl + " "
											+ messages.projectObject + " '" + projectName + "' " , MessageType.FAILURE);
								}
							}
						}
					} catch (UnauthorizedAccessException e) {
						Display.getDefault().asyncExec(new Runnable() {
							public void run() {
								CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
										messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
							}
						});
					}
					monitor.worked(70);
					return Status.OK_STATUS;
				}
			};
			job.setUser(true);
			job.schedule();
		}
	}

	/**
	 * Delete project app grp project app rel.
	 *
	 * @param selection
	 *            the selection
	 * @param objectExpPage
	 *            the object exp page
	 */
	@SuppressWarnings("rawtypes")
	private void deleteProjectAppGrpProjectAppRel(final IStructuredSelection selection, final ObjectExpPage objectExpPage) {
		Object selectionObj = selection.getFirstElement();
		RelationObj relationObj = (RelationObj) selectionObj;
		final IAdminTreeChild containerObj = relationObj.getContainerObj();
		try {
			if (!XMAdminUtil.getInstance().isRelObjAccessAllowed(containerObj,
					"GROUP_RELATION-REMOVE")) {
				CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
						messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
				return;
			}
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				CustomMessageDialog.openError(Display.getCurrent().getActiveShell(), messages.errorDialogTitile,
						messages.serverNotReachable);
			}
			LOGGER.error("Exception occured in calling access allowed API " + e);
			return;
		}
		final XMAdminUtil instance = XMAdminUtil.getInstance();
		final AdminTreeviewer adminTree = instance.getAdminTree();
		final String confirmDialogTitle = messages.deleteConfirmDialogTitle;
		String confirmDialogMsg = null;
		final List selectionList = selection.toList();
		final Set<String> relationIdList = new HashSet<>();
		final Map<String,String> successObjectNames = new HashMap<>();
		for (int i = 0; i < selectionList.size(); i++) {
			Object seleObj = selectionList.get(i);
			RelationObj relObj = (RelationObj) seleObj;
			relationIdList.add(relObj.getRelId());
			successObjectNames.put(relObj.getRelId(), ((ProjectApplication)relObj.getRefObject()).getName());
		}
		IAdminTreeChild refObject = relationObj.getRefObject();
		final String projectAppName = ((ProjectApplication) refObject).getName();
		final String projectAppGrpName = ((ProjectApplicationGroup) (relationObj).getContainerObj().getParent().getParent()).getName();
		if (selectionList.size() == 1) {
			confirmDialogMsg = messages.deleteProjectAppConfirmDialogMsgPart1 + " \'"
					+ projectAppName + "\' " + messages.deleteProjectAppGroupRelationMsg
					+ " \'"
					+ projectAppGrpName
					+ "' ?";
		} else {
			confirmDialogMsg = messages.removeMultiObjectsRelConfirmDialogMsg + " "
					+ messages.deleteProjectAppGroupRelationMsg + " \'"
					+ projectAppGrpName
					+ "' ?";
		}
		GroupRelController groupRelController = new GroupRelController();
		boolean isConfirmed = CustomMessageDialog.openConfirm(adminTree.getControl().getShell(), confirmDialogTitle,
				confirmDialogMsg);

		if (isConfirmed) {
			Job job = new Job("Removing Relations...") {
				@Override
				protected IStatus run(IProgressMonitor monitor) {
					monitor.beginTask("Removing Relations..", 100);
					monitor.worked(30);
					try {
						boolean isRelationDeleted;
						isRelationDeleted = groupRelController.deleteMultiGroupRel(relationIdList);
						if (isRelationDeleted) {
							Display.getDefault().asyncExec(new Runnable() {
								public void run() {
									objectExpPage.refreshExplorer();
								}
							});
							if (successObjectNames.size() > 0) {
								Iterator it = successObjectNames.entrySet().iterator();
								while (it.hasNext()) {
									Map.Entry map = (Map.Entry) it.next();
									XMAdminUtil.getInstance()
											.updateLogFile(messages.relationLbl + " " + messages.betweenLbl + " "
													+ messages.projectApplicationObject + " '" + map.getValue()
													+ "' " + messages.andLbl + " " + messages.projectAppGroupObject
													+ " '" + projectAppGrpName + "' " + messages.objectDelete,
													MessageType.SUCCESS);
								}
							}
						}
					} catch (UnauthorizedAccessException e) {
						Display.getDefault().asyncExec(new Runnable() {
							public void run() {
								CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
										messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
							}
						});
					}
					monitor.worked(70);
					return Status.OK_STATUS;
				}
			};
			job.setUser(true);
			job.schedule();
		}
	}

	/**
	 * Delete directory project app rel.
	 *
	 * @param selection
	 *            the selection
	 * @param objectExpPage
	 *            the object exp page
	 */
	@SuppressWarnings("rawtypes")
	private void deleteDirectoryProjectAppRel(final IStructuredSelection selection, final ObjectExpPage objectExpPage) {
		Object selectionObj = selection.getFirstElement();
		RelationObj relationObj = (RelationObj) selectionObj;
		final IAdminTreeChild containerObj = relationObj.getContainerObj();
		try {
			if (!XMAdminUtil.getInstance().isRelObjAccessAllowed(containerObj,
					"DIRECTORY_RELATION-REMOVE")) {
				CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
						messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
				return;
			}
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				CustomMessageDialog.openError(Display.getCurrent().getActiveShell(), messages.errorDialogTitile,
						messages.serverNotReachable);
			}
			LOGGER.error("Exception occured in calling access allowed API " + e);
			return;
		}
		final XMAdminUtil instance = XMAdminUtil.getInstance();
		final AdminTreeviewer adminTree = instance.getAdminTree();
		final String confirmDialogTitle = messages.deleteConfirmDialogTitle;
		String confirmDialogMsg = null;
		final List selectionList = selection.toList();
		final List<String> relationIdList = new ArrayList<>();
		final Map<String,String> successObjectNames = new HashMap<>();
		for (int i = 0; i < selectionList.size(); i++) {
			Object seleObj = selectionList.get(i);
			RelationObj relObj = (RelationObj) seleObj;
			relationIdList.add(relObj.getRelId());
			successObjectNames.put(relObj.getRelId(), ((ProjectApplication)relObj.getRefObject()).getName());
		}
		IAdminTreeChild refObject = relationObj.getRefObject();
		final String projectAppName = ((ProjectApplication) refObject).getName();
		final String directoryName =  ((Directory) (relationObj).getContainerObj().getParent().getParent().getParent()).getName();
		if (selectionList.size() == 1) {
			confirmDialogMsg = messages.deleteProjectAppConfirmDialogMsgPart1 + " \'"
					+ projectAppName + "\' " + messages.deleteDirectoryMsg + " \'"
					+ directoryName
					+ "' ?";
		} else {
			confirmDialogMsg = messages.removeMultiObjectsRelConfirmDialogMsg + " " + messages.deleteDirectoryMsg
					+ " \'"
					+ directoryName
					+ "' ?";
		}
		DirectoryRelController directoryRelController = new DirectoryRelController();
		boolean isConfirmed = CustomMessageDialog.openConfirm(adminTree.getControl().getShell(), confirmDialogTitle,
				confirmDialogMsg);

		if (isConfirmed) {
			Job job = new Job("Removing Relations...") {
				@Override
				protected IStatus run(IProgressMonitor monitor) {
					monitor.beginTask("Removing Relations..", 100);
					monitor.worked(30);
					try {
						boolean isRelationDeleted;
						isRelationDeleted = directoryRelController.deleteMultiDirectory(relationIdList);
						if (isRelationDeleted) {
							Display.getDefault().asyncExec(new Runnable() {
								public void run() {
									objectExpPage.refreshExplorer();
								}
							});
							if (successObjectNames.size() > 0) {
								Iterator it = successObjectNames.entrySet().iterator();
								while (it.hasNext()) {
									Map.Entry map = (Map.Entry) it.next();
									XMAdminUtil.getInstance()
											.updateLogFile(messages.relationLbl + " " + messages.betweenLbl + " "
													+ messages.projectApplicationObject +" '" + map.getValue()
													+ "' " + messages.andLbl + " " + messages.directoryObject +" '"
													+ directoryName + "' " + messages.objectDelete,
													MessageType.SUCCESS);
								}
							}
						}
					} catch (UnauthorizedAccessException e) {
						Display.getDefault().asyncExec(new Runnable() {
							public void run() {
								CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
										messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
							}
						});
					}
					monitor.worked(70);
					return Status.OK_STATUS;
				}
			};
			job.setUser(true);
			job.schedule();
		}
	}

	/**
	 * Delete admin project app rel.
	 *
	 * @param selection
	 *            the selection
	 * @param objectExpPage
	 *            the object exp page
	 */
	@SuppressWarnings("rawtypes")
	private void deleteAdminProjectAppRel(final IStructuredSelection selection, final ObjectExpPage objectExpPage) {
		Object selectionObj = selection.getFirstElement();
		RelationObj relationObj = (RelationObj) selectionObj;
		final IAdminTreeChild containerObj = relationObj.getContainerObj();
		try {
			if (!XMAdminUtil.getInstance().isRelObjAccessAllowed(containerObj,
					"ADMIN_MENU_CONFIG-REMOVE")) {
				CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
						messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
				return;
			}
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				CustomMessageDialog.openError(Display.getCurrent().getActiveShell(), messages.errorDialogTitile,
						messages.serverNotReachable);
			}
			LOGGER.error("Exception occured in calling access allowed API " + e);
			return;
		}
		final XMAdminUtil instance = XMAdminUtil.getInstance();
		final AdminTreeviewer adminTree = instance.getAdminTree();
		final String confirmDialogTitle = messages.deleteConfirmDialogTitle;
		String confirmDialogMsg = null;
		final List selectionList = selection.toList();
		final Set<String> relationIdList = new HashSet<>();
		final Map<String,String> successObjectNames = new HashMap<>();
		final Map<String,String> failObjectNames = new HashMap<>();
		for (int i = 0; i < selectionList.size(); i++) {
			Object seleObj = selectionList.get(i);
			RelationObj relObj = (RelationObj) seleObj;
			relationIdList.add(relObj.getRelId());
			successObjectNames.put(relObj.getRelId(), ((ProjectApplication)relObj.getRefObject()).getName());
		}
		IAdminTreeChild refObject = relationObj.getRefObject();
		final String projectAppName = ((ProjectApplication) refObject).getName();
		if (selectionList.size() == 1) {
			confirmDialogMsg = messages.deleteProjectAppConfirmDialogMsgPart1 + " \'"
					+ projectAppName + "\' " + messages.deleteAdminMenuRelationMsg + " ?";
		} else {
			confirmDialogMsg = messages.removeMultiObjectsRelConfirmDialogMsg + " "
					+ messages.deleteAdminMenuRelationMsg + " ?";
		}
		AdminMenuConfigRelController adminMenuConfigRelController = new AdminMenuConfigRelController();
		boolean isConfirmed = CustomMessageDialog.openConfirm(adminTree.getControl().getShell(), confirmDialogTitle,
				confirmDialogMsg);

		if (isConfirmed) {
			Job job = new Job("Removing Relations...") {
				@Override
				protected IStatus run(IProgressMonitor monitor) {
					monitor.beginTask("Removing Relations..", 100);
					monitor.worked(30);
					try {
						AdminMenuConfigResponse adminMenuConfigResponse;
						adminMenuConfigResponse = adminMenuConfigRelController.deleteMultiAdminMenuRel(relationIdList);
						if (adminMenuConfigResponse != null) {
							List<Map<String, String>> statusMaps = adminMenuConfigResponse.getStatusMaps();
							if (!statusMaps.isEmpty()) {
								for (Map<String, String> statusMap : statusMaps) {
									String string = statusMap.get("en");
									String[] split = string.split("id");
									failObjectNames.put(split[1].trim(), successObjectNames.get(split[1].trim()));
									successObjectNames.remove(split[1].trim());
								}
								XMAdminUtil.getInstance().updateLogFile(
										messages.deleteMultiObjectsDialogErrMsg, MessageType.FAILURE);
								Display.getDefault().asyncExec(new Runnable() {
									public void run() {
										CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
												messages.deleteConfirmDialogTitle,
												messages.deleteMultiObjectsDialogErrMsg);
									}
								});
							}

							Display.getDefault().asyncExec(new Runnable() {
								public void run() {
									objectExpPage.refreshExplorer();
								}
							});
							if (successObjectNames.size() > 0) {
								Iterator it = successObjectNames.entrySet().iterator();
								while (it.hasNext()) {
									Map.Entry map = (Map.Entry) it.next();
									XMAdminUtil.getInstance().updateLogFile(messages.relationLbl + " "
											+ messages.betweenLbl + " " + messages.projectApplicationObject +" '"
											+ map.getValue() + "' " + messages.andLbl + " " + messages.adminMenuNode
											+ " " + messages.objectDelete, MessageType.SUCCESS);
								}
							}
							
							if (failObjectNames.size() > 0) {
								Iterator it = failObjectNames.entrySet().iterator();
								while (it.hasNext()) {
									Map.Entry map = (Map.Entry) it.next();
									XMAdminUtil.getInstance().updateLogFile(
											messages.deleteErrorMsg + " " + messages.projectApplicationObject + " '"
													+ map.getValue() + "' " + messages.relationLbl + " "
													+ messages.withLbl + " " + messages.adminMenuNode,
											MessageType.FAILURE);
								}
							}
						}
					} catch (UnauthorizedAccessException e) {
						Display.getDefault().asyncExec(new Runnable() {
							public void run() {
								CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
										messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
							}
						});
					}
					monitor.worked(70);
					return Status.OK_STATUS;
				}
			};
			job.setUser(true);
			job.schedule();
		}
	}

	/**
	 * Delete user project AA project app rel.
	 *
	 * @param selection
	 *            the selection
	 * @param objectExpPage
	 *            the object exp page
	 */
	@SuppressWarnings("rawtypes")
	private void deleteUserProjectAAProjectAppRel(final IStructuredSelection selection, final ObjectExpPage objectExpPage) {
		Object selectionObj = selection.getFirstElement();
		RelationObj relationObj = (RelationObj) selectionObj;
		final IAdminTreeChild containerObj = relationObj.getContainerObj();
		try {
			if (!XMAdminUtil.getInstance().isRelObjAccessAllowed(containerObj,
					"PROJECT_APPLICATION_TO_PROJECT-REMOVE")) {
				CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
						messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
				return;
			}
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				CustomMessageDialog.openError(Display.getCurrent().getActiveShell(), messages.errorDialogTitile,
						messages.serverNotReachable);
			}
			LOGGER.error("Exception occured in calling access allowed API " + e);
			return;
		}
		final XMAdminUtil instance = XMAdminUtil.getInstance();
		final AdminTreeviewer adminTree = instance.getAdminTree();
		final String confirmDialogTitle = messages.deleteConfirmDialogTitle;
		String confirmDialogMsg = null;
		final List selectionList = selection.toList();
		final Set<String> relationIdList = new HashSet<>();
		final Map<String,String> successObjectNames = new HashMap<>();
		final Map<String,String> failObjectNames = new HashMap<>();
		for (int i = 0; i < selectionList.size(); i++) {
			Object seleObj = selectionList.get(i);
			RelationObj relObj = (RelationObj) seleObj;
			relationIdList.add(relObj.getRelId());
			successObjectNames.put(relObj.getRelId(), ((ProjectApplication)relObj.getRefObject()).getName());
		}
		IAdminTreeChild refObject = relationObj.getRefObject();
		String adminAreaName = null;
		IAdminTreeChild containerObject = getContainerObject(relationObj);
		String projectAppName = ((ProjectApplication) refObject).getName();
		String userName = null;
		String projectName = null;
		if (containerObject instanceof UserProjectAAProjectApplications) {
			adminAreaName = ((AdministrationArea) ((RelationObj) containerObject.getParent()).getRefObject()).getName();
			userName = ((User) (containerObject.getParent().getParent().getParent().getParent().getParent())).getName();
			projectName = ((Project) ((RelationObj) containerObject.getParent().getParent().getParent()).getRefObject()).getName();
		} else if (containerObject instanceof ProjectUserAAProjectApplications) {
			adminAreaName = ((AdministrationArea) ((RelationObj) containerObject.getParent()).getRefObject()).getName();
			userName = ((User) ((RelationObj) containerObject.getParent().getParent().getParent()).getRefObject()).getName();
			projectName = ((Project) (containerObject.getParent().getParent().getParent().getParent().getParent())).getName();
		}
		if (selectionList.size() == 1) {
			confirmDialogMsg = messages.deleteProjectAppConfirmDialogMsgPart1 + " \'"
					+ projectAppName + "\' " + messages.deleteAdminAreaRelationMsg + " \'"
					+ adminAreaName + "' ?";
		} else {
			confirmDialogMsg = messages.removeMultiObjectsRelConfirmDialogMsg + " "
					+ messages.deleteAdminAreaRelationMsg + " \'" + adminAreaName + "' ?";
		}
		UserProjectAppRelController userProjectAppRelController = new UserProjectAppRelController(XMAdminUtil
				.getInstance().getAAForHeaders(getContainerObject((RelationObj) selection.getFirstElement())));
		boolean isConfirmed = CustomMessageDialog.openConfirm(adminTree.getControl().getShell(), confirmDialogTitle,
				confirmDialogMsg);
		final String adminAreaName2 = adminAreaName;
		final String projectName2 = projectName;
		final String userName2 = userName;
		if (isConfirmed) {
			Job job = new Job("Removing Relations...") {
				@Override
				protected IStatus run(IProgressMonitor monitor) {
					monitor.beginTask("Removing Relations..", 100);
					monitor.worked(30);
					try {
						UserProjectAppRelResponse userProjectAppRelResponse;
						userProjectAppRelResponse = userProjectAppRelController
								.deleteMultiUserProjAppRel(relationIdList);
						if (userProjectAppRelResponse != null) {
							List<Map<String, String>> statusMaps = userProjectAppRelResponse.getStatusMaps();
							if (!statusMaps.isEmpty()) {
								for (Map<String, String> statusMap : statusMaps) {
									String string = statusMap.get("en");
									String[] split = string.split("id");
									failObjectNames.put(split[1].trim(), successObjectNames.get(split[1].trim()));
									successObjectNames.remove(split[1].trim());
								}
								XMAdminUtil.getInstance().updateLogFile(
										messages.deleteMultiObjectsDialogErrMsg, MessageType.FAILURE);
								Display.getDefault().asyncExec(new Runnable() {
									public void run() {
										CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
												messages.deleteConfirmDialogTitle,
												messages.deleteMultiObjectsDialogErrMsg);
									}
								});
							}

							Display.getDefault().asyncExec(new Runnable() {
								public void run() {
									objectExpPage.refreshExplorer();
								}
							});
							if (successObjectNames.size() > 0) {
								Iterator it = successObjectNames.entrySet().iterator();
								while (it.hasNext()) {
									Map.Entry map = (Map.Entry) it.next();
									XMAdminUtil.getInstance()
											.updateLogFile(messages.relationLbl + " " + messages.betweenLbl + " "
													+ messages.projectApplicationObject +" '" + map.getValue()
													+ "' " + messages.andLbl + " " + messages.administrationAreaObject
													+ " '" + adminAreaName2 + "' " + messages.withLbl + " "
													+ messages.userObject + " '" + userName2 + "' " + messages.andLbl
													+ " " + messages.projectObject + " '" + projectName2 + "' "
													+ messages.objectDelete, MessageType.SUCCESS);
								}
							}
							
							if (failObjectNames.size() > 0) {
								Iterator it = failObjectNames.entrySet().iterator();
								while (it.hasNext()) {
									Map.Entry map = (Map.Entry) it.next();
									XMAdminUtil.getInstance().updateLogFile(
											messages.deleteErrorMsg + " " + messages.projectApplicationObject + " "
													+ "'" + map.getValue() + "'" + messages.relationLbl + " "
													+ messages.withLbl + " " + messages.administrationAreaObject + " '"
													+ adminAreaName2 + "' " + messages.andLbl + " "
													+ messages.userObject + " '" + userName2 + "' " + messages.withLbl
													+ " " + messages.projectObject + " '" + projectName2 + "' ",
											MessageType.FAILURE);
								}
							}
						}
					} catch (UnauthorizedAccessException e) {
						Display.getDefault().asyncExec(new Runnable() {
							public void run() {
								CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
										messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
							}
						});
					}
					monitor.worked(70);
					return Status.OK_STATUS;
				}
			};
			job.setUser(true);
			job.schedule();
		}
	}

	/**
	 * Delete site AA project app rel.
	 *
	 * @param selection
	 *            the selection
	 * @param objectExpPage
	 *            the object exp page
	 */
	@SuppressWarnings("rawtypes")
	private void deleteSiteAAProjectAppRel(final IStructuredSelection selection, final ObjectExpPage objectExpPage) {
		Object selectionObj = selection.getFirstElement();
		RelationObj relationObj = (RelationObj) selectionObj;
		final IAdminTreeChild containerObj = relationObj.getContainerObj();
		try {
			if (!XMAdminUtil.getInstance().isRelObjAccessAllowed(containerObj,
					"PROJECT_APPLICATION_TO_PROJECT-REMOVE")) {
				CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
						messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
				return;
			}
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				CustomMessageDialog.openError(Display.getCurrent().getActiveShell(), messages.errorDialogTitile,
						messages.serverNotReachable);
			}
			LOGGER.error("Exception occured in calling access allowed API " + e);
			return;
		}
		final XMAdminUtil instance = XMAdminUtil.getInstance();
		final AdminTreeviewer adminTree = instance.getAdminTree();
		final String confirmDialogTitle = messages.deleteConfirmDialogTitle;
		String confirmDialogMsg = null;
		final List selectionList = selection.toList();
		final Set<String> relationIdList = new HashSet<>();
		final Map<String,String> successObjectNames = new HashMap<>();
		final Map<String,String> failObjectNames = new HashMap<>();
		for (int i = 0; i < selectionList.size(); i++) {
			Object sele = selectionList.get(i);
			RelationObj relObj = (RelationObj) sele;
			relationIdList.add(relObj.getRelId());
			successObjectNames.put(relObj.getRelId(), ((ProjectApplication)relObj.getRefObject()).getName());
		}
		IAdminTreeChild refObject = relationObj.getRefObject();
		String projectName = null;
		IAdminTreeChild containerObject = getContainerObject(relationObj);
		String adminAreaName = null;
		if (containerObject instanceof SiteAdminAreaProjectApplications) {
			projectName = ((Project) ((RelationObj) containerObject.getParent()).getRefObject()).getName();
			adminAreaName = ((AdministrationArea) ((RelationObj) containerObject.getParent().getParent().getParent()).getRefObject()).getName();
		} else if (containerObject instanceof AdminAreaProjectApplications) {
			projectName = ((Project) ((RelationObj) containerObject.getParent()).getRefObject()).getName();
			adminAreaName = ((AdministrationArea) (containerObject.getParent().getParent().getParent())).getName();
		}
		String projectAppName = ((ProjectApplication) refObject).getName();
		if (selectionList.size() == 1) {
			confirmDialogMsg = messages.deleteProjectAppConfirmDialogMsgPart1 + " \'"
					+ projectAppName + "\' " + messages.deleteProjectRelationMsg + " \'"
					+ projectName + "' ?";
		} else {
			confirmDialogMsg = messages.removeMultiObjectsRelConfirmDialogMsg + " " + messages.deleteProjectRelationMsg
					+ " \'" + projectName + "' ?";
		}
		AdminAreaProjectAppRelController adminAreaProjectAppRelController = new AdminAreaProjectAppRelController(
				XMAdminUtil.getInstance()
						.getAAForHeaders(getContainerObject((RelationObj) selection.getFirstElement())));
		boolean isConfirmed = CustomMessageDialog.openConfirm(adminTree.getControl().getShell(), confirmDialogTitle,
				confirmDialogMsg);
		final String projectName2 = projectName;
		final String adminAreaName2 = adminAreaName;
		if (isConfirmed) {
			Job job = new Job("Removing Relations...") {
				@Override
				protected IStatus run(IProgressMonitor monitor) {
					monitor.beginTask("Removing Relations..", 100);
					monitor.worked(30);
					try {
						AdminAreaProjectAppRelResponse adminAreaProjectAppRelResponse;
						adminAreaProjectAppRelResponse = adminAreaProjectAppRelController
								.deleteMultiAdminAreaProjAppRel(relationIdList);
						if (adminAreaProjectAppRelResponse != null) {
							List<Map<String, String>> statusMaps = adminAreaProjectAppRelResponse.getStatusMaps();
							if (!statusMaps.isEmpty()) {
								for (Map<String, String> statusMap : statusMaps) {
									String string = statusMap.get("en");
									String[] split = string.split("id");
									failObjectNames.put(split[1].trim(), successObjectNames.get(split[1].trim()));
									successObjectNames.remove(split[1].trim());
								}
								XMAdminUtil.getInstance().updateLogFile(
										messages.deleteMultiObjectsDialogErrMsg, MessageType.FAILURE);
								Display.getDefault().asyncExec(new Runnable() {
									public void run() {
										CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
												messages.deleteConfirmDialogTitle,
												messages.deleteMultiObjectsDialogErrMsg);
									}
								});
							}

							Display.getDefault().asyncExec(new Runnable() {
								@SuppressWarnings("unchecked")
								public void run() {
									objectExpPage.refreshExplorer();
									adminTree.refershBackReference(new Class[] { ProjectAppAdminAreas.class });
								}
							});
							if (successObjectNames.size() > 0) {
								Iterator it = successObjectNames.entrySet().iterator();
								while (it.hasNext()) {
									Map.Entry map = (Map.Entry) it.next();
									XMAdminUtil.getInstance().updateLogFile(messages.relationLbl + " "
											+ messages.betweenLbl + " " + messages.projectApplicationObject +" '"
											+ map.getValue() + "' " + messages.andLbl + " " + messages.projectObject
											+ " '" + projectName2 + "' " + messages.withLbl + " "
											+ messages.administrationAreaObject + " '" + adminAreaName2 + "' "
											+ messages.objectDelete, MessageType.SUCCESS);
								}
							}
							
							if (failObjectNames.size() > 0) {
								Iterator it = failObjectNames.entrySet().iterator();
								while (it.hasNext()) {
									Map.Entry map = (Map.Entry) it.next();
									XMAdminUtil.getInstance().updateLogFile(
											messages.deleteErrorMsg + " " + messages.projectApplicationObject + " '"
													+ map.getValue() + "' " + messages.relationLbl + " "
													+ messages.withLbl + " " + messages.projectObject + " '"
													+ projectName2 + "' " + messages.andLbl + " "
													+ messages.administrationAreaObject + " '" + adminAreaName2 + "' ",
											MessageType.FAILURE);
								}
							}
						}
					} catch (UnauthorizedAccessException e) {
						Display.getDefault().asyncExec(new Runnable() {
							public void run() {
								CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
										messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
							}
						});
					}
					monitor.worked(70);
					return Status.OK_STATUS;
				}
			};
			job.setUser(true);
			job.schedule();
		}
	}

	/**
	 * Delete user app grp user app rel.
	 *
	 * @param selection
	 *            the selection
	 * @param objectExpPage
	 *            the object exp page
	 */
	@SuppressWarnings("rawtypes")
	private void deleteUserAppGrpUserAppRel(final IStructuredSelection selection, final ObjectExpPage objectExpPage) {
		Object selectionObj = selection.getFirstElement();
		RelationObj relationObj = (RelationObj) selectionObj;
		final IAdminTreeChild containerObj = relationObj.getContainerObj();
		try {
			if (!XMAdminUtil.getInstance().isRelObjAccessAllowed(containerObj,
					"GROUP_RELATION-REMOVE")) {
				CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
						messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
				return;
			}
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				CustomMessageDialog.openError(Display.getCurrent().getActiveShell(), messages.errorDialogTitile,
						messages.serverNotReachable);
			}
			LOGGER.error("Exception occured in calling access allowed API " + e);
			return;
		}
		final XMAdminUtil instance = XMAdminUtil.getInstance();
		final AdminTreeviewer adminTree = instance.getAdminTree();
		final String confirmDialogTitle = messages.deleteConfirmDialogTitle;
		String confirmDialogMsg = null;
		final List selectionList = selection.toList();
		final Set<String> relationIdList = new HashSet<>();
		final Map<String,String> successObjectNames = new HashMap<>();
		for (int i = 0; i < selectionList.size(); i++) {
			Object seleObj = selectionList.get(i);
			RelationObj relObj = (RelationObj) seleObj;
			relationIdList.add(relObj.getRelId());
			successObjectNames.put(relObj.getRelId(), ((UserApplication)relObj.getRefObject()).getName());
		}
		IAdminTreeChild refObject = relationObj.getRefObject();
		String userAppName = ((UserApplication) refObject).getName();
		String userAppGroupName = ((UserApplicationGroup) (relationObj).getContainerObj().getParent().getParent())
				.getName();
		if (selectionList.size() == 1) {
			confirmDialogMsg = messages.deleteUserAppConfirmDialogMsgPart1 + " \'" + userAppName + "\' "
					+ messages.deleteUserAppGroupRelationMsg + " \'" + userAppGroupName + "' ?";
		} else {
			confirmDialogMsg = messages.removeMultiObjectsRelConfirmDialogMsg + " "
					+ messages.deleteUserAppGroupRelationMsg + " \'" + userAppGroupName + "' ?";
		}
		GroupRelController groupRelController = new GroupRelController();
		boolean isConfirmed = CustomMessageDialog.openConfirm(adminTree.getControl().getShell(), confirmDialogTitle,
				confirmDialogMsg);

		if (isConfirmed) {
			Job job = new Job("Removing Relations...") {
				@Override
				protected IStatus run(IProgressMonitor monitor) {
					monitor.beginTask("Removing Relations..", 100);
					monitor.worked(30);
					try {
						boolean isRelationDeleted;
						isRelationDeleted = groupRelController.deleteMultiGroupRel(relationIdList);
						if (isRelationDeleted) {
							Display.getDefault().asyncExec(new Runnable() {
								public void run() {
									objectExpPage.refreshExplorer();
								}
							});
							if (successObjectNames.size() > 0) {
								Iterator it = successObjectNames.entrySet().iterator();
								while (it.hasNext()) {
									Map.Entry map = (Map.Entry) it.next();
									XMAdminUtil.getInstance().updateLogFile(
											messages.relationLbl + " " + messages.betweenLbl + " "
													+ messages.userApplicationObject + " '" + map.getValue() + "' "
													+ messages.andLbl + " " + messages.userAppGroupObject + " '"
													+ userAppGroupName + "' " + messages.objectDelete,
											MessageType.SUCCESS);
								}
							}
						}
					} catch (UnauthorizedAccessException e) {
						Display.getDefault().asyncExec(new Runnable() {
							public void run() {
								CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
										messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
							}
						});
					}
					monitor.worked(70);
					return Status.OK_STATUS;
				}
			};
			job.setUser(true);
			job.schedule();
		}
	}

	/**
	 * Delete directory user app rel.
	 *
	 * @param selection
	 *            the selection
	 * @param objectExpPage
	 *            the object exp page
	 */
	@SuppressWarnings("rawtypes")
	private void deleteDirectoryUserAppRel(final IStructuredSelection selection, final ObjectExpPage objectExpPage) {
		Object selectionObj = selection.getFirstElement();
		RelationObj relationObj = (RelationObj) selectionObj;
		final IAdminTreeChild containerObj = relationObj.getContainerObj();
		try {
			if (!XMAdminUtil.getInstance().isRelObjAccessAllowed(containerObj,
					"DIRECTORY_RELATION-REMOVE")) {
				CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
						messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
				return;
			}
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				CustomMessageDialog.openError(Display.getCurrent().getActiveShell(), messages.errorDialogTitile,
						messages.serverNotReachable);
			}
			LOGGER.error("Exception occured in calling access allowed API " + e);
			return;
		}
		final XMAdminUtil instance = XMAdminUtil.getInstance();
		final AdminTreeviewer adminTree = instance.getAdminTree();
		final String confirmDialogTitle = messages.deleteConfirmDialogTitle;
		String confirmDialogMsg = null;
		final List selectionList = selection.toList();
		final List<String> relationIdList = new ArrayList<>();
		final Map<String,String> successObjectNames = new HashMap<>();
		final Map<String,String> failObjectNames = new HashMap<>();
		for (int i = 0; i < selectionList.size(); i++) {
			Object seleObj = selectionList.get(i);
			RelationObj relObj = (RelationObj) seleObj;
			relationIdList.add(relObj.getRelId());
			successObjectNames.put(relObj.getRelId(), ((UserApplication)relObj.getRefObject()).getName());
		}
		IAdminTreeChild refObject = relationObj.getRefObject();
		String userAppName = ((UserApplication) refObject).getName();
		String directoryName = ((Directory) (relationObj).getContainerObj().getParent().getParent().getParent()).getName();
		if (selectionList.size() == 1) {
			confirmDialogMsg = messages.deleteUserAppConfirmDialogMsgPart1 + " \'" + userAppName + "\' "
					+ messages.deleteDirectoryMsg + " \'" + directoryName + "' ?";
		} else {
			confirmDialogMsg = messages.removeMultiObjectsRelConfirmDialogMsg + " " + messages.deleteDirectoryMsg
					+ " \'" + directoryName	+ "' ?";
		}
		DirectoryRelController directoryRelController = new DirectoryRelController();
		boolean isConfirmed = CustomMessageDialog.openConfirm(adminTree.getControl().getShell(), confirmDialogTitle,
				confirmDialogMsg);

		if (isConfirmed) {
			Job job = new Job("Removing Relations...") {
				@Override
				protected IStatus run(IProgressMonitor monitor) {
					monitor.beginTask("Removing Relations..", 100);
					monitor.worked(30);
					try {
						boolean isRelationDeleted;
						isRelationDeleted = directoryRelController.deleteMultiDirectory(relationIdList);
						if (isRelationDeleted) {
							Display.getDefault().asyncExec(new Runnable() {
								public void run() {
									objectExpPage.refreshExplorer();
								}
							});
							
							if (successObjectNames.size() > 0) {
								Iterator it = successObjectNames.entrySet().iterator();
								while (it.hasNext()) {
									Map.Entry map = (Map.Entry) it.next();
									XMAdminUtil.getInstance().updateLogFile(
											messages.relationLbl + " " + messages.betweenLbl + " "
													+ messages.userApplicationObject + " '" + map.getValue() + "' "
													+ messages.andLbl + " " + messages.directoryObject + " '"
													+ directoryName + "' " + messages.objectDelete,
											MessageType.SUCCESS);
								}
							}
							
							if (failObjectNames.size() > 0) {
								Iterator it = failObjectNames.entrySet().iterator();
								while (it.hasNext()) {
									Map.Entry map = (Map.Entry) it.next();
									XMAdminUtil.getInstance()
											.updateLogFile(messages.deleteErrorMsg + " "
													+ messages.userApplicationObject + " '" + map.getValue() + "' "
													+ messages.relationLbl + " " + messages.withLbl + " "
													+ messages.directoryNode + " '" + directoryName + "' ",
													MessageType.FAILURE);
								}
							}
						}
					} catch (UnauthorizedAccessException e) {
						Display.getDefault().asyncExec(new Runnable() {
							public void run() {
								CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
										messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
							}
						});
					}
					monitor.worked(70);
					return Status.OK_STATUS;
				}
			};
			job.setUser(true);
			job.schedule();
		}
	}

	/**
	 * Delete admin user app rel.
	 *
	 * @param selection
	 *            the selection
	 * @param objectExpPage
	 *            the object exp page
	 */
	@SuppressWarnings("rawtypes")
	private void deleteAdminUserAppRel(final IStructuredSelection selection, final ObjectExpPage objectExpPage) {
		Object selectionObj = selection.getFirstElement();
		RelationObj relationObj = (RelationObj) selectionObj;
		final IAdminTreeChild containerObj = relationObj.getContainerObj();
		try {
			if (!XMAdminUtil.getInstance().isRelObjAccessAllowed(containerObj,
					"ADMIN_MENU_CONFIG-REMOVE")) {
				CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
						messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
				return;
			}
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				CustomMessageDialog.openError(Display.getCurrent().getActiveShell(), messages.errorDialogTitile,
						messages.serverNotReachable);
			}
			LOGGER.error("Exception occured in calling access allowed API " + e);
			return;
		}
		final XMAdminUtil instance = XMAdminUtil.getInstance();
		final AdminTreeviewer adminTree = instance.getAdminTree();
		final String confirmDialogTitle = messages.deleteConfirmDialogTitle;
		String confirmDialogMsg = null;
		final List selectionList = selection.toList();
		final Set<String> relationIdList = new HashSet<>();
		final Map<String,String> successObjectNames = new HashMap<>();
		final Map<String,String> failObjectNames = new HashMap<>();
		for (int i = 0; i < selectionList.size(); i++) {
			Object seleObj = selectionList.get(i);
			RelationObj relObj = (RelationObj) seleObj;
			relationIdList.add(relObj.getRelId());
			successObjectNames.put(relObj.getRelId(), ((UserApplication)relObj.getRefObject()).getName());
		}
		IAdminTreeChild refObject = relationObj.getRefObject();
		String userAppName = ((UserApplication) refObject).getName();
		if (selectionList.size() == 1) {
			confirmDialogMsg = messages.deleteUserAppConfirmDialogMsgPart1 + " \'"
					+ userAppName + "\' " + messages.deleteAdminMenuRelationMsg + " ?";
		} else {
			confirmDialogMsg = messages.removeMultiObjectsRelConfirmDialogMsg + " "
					+ messages.deleteAdminMenuRelationMsg + " ?";
		}
		AdminMenuConfigRelController adminMenuConfigRelController = new AdminMenuConfigRelController();
		boolean isConfirmed = CustomMessageDialog.openConfirm(adminTree.getControl().getShell(), confirmDialogTitle,
				confirmDialogMsg);

		if (isConfirmed) {
			Job job = new Job("Removing Relations...") {
				@Override
				protected IStatus run(IProgressMonitor monitor) {
					monitor.beginTask("Removing Relations..", 100);
					monitor.worked(30);
					try {
						AdminMenuConfigResponse adminMenuConfigResponse;
						adminMenuConfigResponse = adminMenuConfigRelController.deleteMultiAdminMenuRel(relationIdList);
						if (adminMenuConfigResponse != null) {
							List<Map<String, String>> statusMaps = adminMenuConfigResponse.getStatusMaps();
							if (!statusMaps.isEmpty()) {
								for (Map<String, String> statusMap : statusMaps) {
									String string = statusMap.get("en");
									String[] split = string.split("id");
									failObjectNames.put(split[1].trim(), successObjectNames.get(split[1].trim()));
									successObjectNames.remove(split[1].trim());
								}
								XMAdminUtil.getInstance().updateLogFile(
										messages.deleteMultiObjectsDialogErrMsg, MessageType.FAILURE);
								Display.getDefault().asyncExec(new Runnable() {
									public void run() {
										CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
												messages.deleteConfirmDialogTitle,
												messages.deleteMultiObjectsDialogErrMsg);
									}
								});
							}

							Display.getDefault().asyncExec(new Runnable() {
								public void run() {
									objectExpPage.refreshExplorer();
								}
							});
							if (successObjectNames.size() > 0) {
								Iterator it = successObjectNames.entrySet().iterator();
								while (it.hasNext()) {
									Map.Entry map = (Map.Entry) it.next();
									XMAdminUtil.getInstance().updateLogFile(messages.relationLbl + " "
											+ messages.betweenLbl + " " + messages.userApplicationObject + " '"
											+ map.getValue() + "' " + messages.andLbl + " " + messages.adminMenuNode
											+ " " + messages.objectDelete, MessageType.SUCCESS);
								}
							}
							
							if (failObjectNames.size() > 0) {
								Iterator it = failObjectNames.entrySet().iterator();
								while (it.hasNext()) {
									Map.Entry map = (Map.Entry) it.next();
									XMAdminUtil.getInstance().updateLogFile(
											messages.deleteErrorMsg + " " + messages.userApplicationObject + " '"
													+ map.getValue() + "' " + messages.relationLbl + " "
													+ messages.withLbl + " " + messages.adminMenuNode,
											MessageType.FAILURE);
								}
							}
						}
					} catch (UnauthorizedAccessException e) {
						Display.getDefault().asyncExec(new Runnable() {
							public void run() {
								CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
										messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
							}
						});
					}
					monitor.worked(70);
					return Status.OK_STATUS;
				}
			};
			job.setUser(true);
			job.schedule();
		}
	}

	/**
	 * Delete user AA user app rel.
	 *
	 * @param selection
	 *            the selection
	 * @param objectExpPage
	 *            the object exp page
	 */
	@SuppressWarnings("rawtypes")
	private void deleteUserAAUserAppRel(final IStructuredSelection selection, final ObjectExpPage objectExpPage) {
		Object selectionObj = selection.getFirstElement();
		RelationObj relationObj = (RelationObj) selectionObj;
		final IAdminTreeChild containerObj = relationObj.getContainerObj();
		try {
			if (!XMAdminUtil.getInstance().isRelObjAccessAllowed(containerObj,
					"USER_APPLICATION_TO_USER-REMOVE")) {
				CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
						messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
				return;
			}
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				CustomMessageDialog.openError(Display.getCurrent().getActiveShell(), messages.errorDialogTitile,
						messages.serverNotReachable);
			}
			LOGGER.error("Exception occured in calling access allowed API " + e);
			return;
		}
		final XMAdminUtil instance = XMAdminUtil.getInstance();
		final AdminTreeviewer adminTree = instance.getAdminTree();
		final String confirmDialogTitle = messages.deleteConfirmDialogTitle;
		String confirmDialogMsg = null;
		final List selectionList = selection.toList();
		final Set<String> relationIdList = new HashSet<>();
		final Map<String,String> successObjectNames = new HashMap<>();
		final Map<String,String> failObjectNames = new HashMap<>();
		for (int i = 0; i < selectionList.size(); i++) {
			Object seleObj = selectionList.get(i);
			RelationObj relObj = (RelationObj) seleObj;
			relationIdList.add(relObj.getRelId());
			successObjectNames.put(relObj.getRelId(), ((UserApplication)relObj.getRefObject()).getName());
		}
		IAdminTreeChild refObject = relationObj.getRefObject();
		IAdminTreeChild containerObject = getContainerObject(relationObj);
		String userAppName = ((UserApplication) refObject).getName();
		String adminAreaName = ((AdministrationArea) ((RelationObj) containerObject.getParent()).getRefObject()).getName();
		String userName = ((User) (containerObject.getParent().getParent().getParent())).getName();
		if (selectionList.size() == 1) {
			confirmDialogMsg = messages.deleteUserAppConfirmDialogMsgPart1 + " \'" + userAppName + "\' "
					+ messages.deleteAdminAreaRelationMsg + " \'" + adminAreaName + "' ?";
		} else {
			confirmDialogMsg = messages.removeMultiObjectsRelConfirmDialogMsg + " "
					+ messages.deleteAdminAreaRelationMsg + " \'" + adminAreaName + "' ?";
		}
		UserUserAppRelController userUserAppRelController = new UserUserAppRelController(XMAdminUtil.getInstance()
				.getAAForHeaders(getContainerObject((RelationObj) selection.getFirstElement())));
		boolean isConfirmed = CustomMessageDialog.openConfirm(adminTree.getControl().getShell(), confirmDialogTitle,
				confirmDialogMsg);

		if (isConfirmed) {
			Job job = new Job("Removing Relations...") {
				@Override
				protected IStatus run(IProgressMonitor monitor) {
					monitor.beginTask("Removing Relations..", 100);
					monitor.worked(30);
					try {
						UserUserAppRelResponse userUserAppRelResponse;
						userUserAppRelResponse = userUserAppRelController.deleteMultiUserUserAppRel(relationIdList);
						if (userUserAppRelResponse != null) {
							List<Map<String, String>> statusMaps = userUserAppRelResponse.getStatusMaps();
							if (!statusMaps.isEmpty()) {
								for (Map<String, String> statusMap : statusMaps) {
									String string = statusMap.get("en");
									String[] split = string.split("id");
									failObjectNames.put(split[1].trim(), successObjectNames.get(split[1].trim()));
									successObjectNames.remove(split[1].trim());
								}
								XMAdminUtil.getInstance().updateLogFile(
										messages.deleteMultiObjectsDialogErrMsg, MessageType.FAILURE);
								Display.getDefault().asyncExec(new Runnable() {
									public void run() {
										CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
												messages.deleteConfirmDialogTitle,
												messages.deleteMultiObjectsDialogErrMsg);
									}
								});
							}

							Display.getDefault().asyncExec(new Runnable() {
								public void run() {
									objectExpPage.refreshExplorer();
								}
							});
							if (successObjectNames.size() > 0) {
								Iterator it = successObjectNames.entrySet().iterator();
								while (it.hasNext()) {
									Map.Entry map = (Map.Entry) it.next();
									XMAdminUtil.getInstance().updateLogFile(messages.relationLbl + " "
											+ messages.betweenLbl + " " + messages.userApplicationObject + " '"
											+ map.getValue() + "' " + messages.andLbl + " "
											+ messages.administrationAreaObject + " '" + adminAreaName + "' "
											+ messages.withLbl + " " + messages.userObject + " '" + userName + "' "
											+ messages.objectDelete, MessageType.SUCCESS);
								}
							}

							if (failObjectNames.size() > 0) {
								Iterator it = failObjectNames.entrySet().iterator();
								while (it.hasNext()) {
									Map.Entry map = (Map.Entry) it.next();
									XMAdminUtil.getInstance()
											.updateLogFile(
													messages.deleteErrorMsg + " " + messages.userApplicationObject
															+ " '" + map.getValue() + "' " + messages.relationLbl
															+ messages.withLbl + " " + messages.administrationAreaObject
															+ " '" + adminAreaName + "' " + messages.andLbl + " "
															+ messages.userObject + " '" + userName + "' ",
													MessageType.FAILURE);
								}
							}
						}
					} catch (UnauthorizedAccessException e) {
						Display.getDefault().asyncExec(new Runnable() {
							public void run() {
								CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
										messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
							}
						});
					}
					monitor.worked(70);
					return Status.OK_STATUS;
				}
			};
			job.setUser(true);
			job.schedule();
		}
	}

	/**
	 * Delete site AA user app rel.
	 *
	 * @param selection
	 *            the selection
	 * @param objectExpPage
	 *            the object exp page
	 */
	@SuppressWarnings("rawtypes")
	private void deleteSiteAAUserAppRel(final IStructuredSelection selection, final ObjectExpPage objectExpPage) {
		Object selectionObj = selection.getFirstElement();
		RelationObj relationObj = (RelationObj) selectionObj;
		final IAdminTreeChild containerObj = relationObj.getContainerObj();
		try {
			if (!XMAdminUtil.getInstance().isRelObjAccessAllowed(containerObj,
					"USER_APPLICATION_TO_ADMINISTRATION_AREA-REMOVE")) {
				CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
						messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
				return;
			}
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				CustomMessageDialog.openError(Display.getCurrent().getActiveShell(), messages.errorDialogTitile,
						messages.serverNotReachable);
			}
			LOGGER.error("Exception occured in calling access allowed API " + e);
			return;
		}
		final XMAdminUtil instance = XMAdminUtil.getInstance();
		final AdminTreeviewer adminTree = instance.getAdminTree();
		final String confirmDialogTitle = messages.deleteConfirmDialogTitle;
		String confirmDialogMsg = null;
		final List selectionList = selection.toList();
		final Set<String> relationIdList = new HashSet<>();
		final Map<String,String> successObjectNames = new HashMap<>();
		final Map<String,String> failObjectNames = new HashMap<>();
		for (int i = 0; i < selectionList.size(); i++) {
			Object seleObj = selectionList.get(i);
			RelationObj relObj = (RelationObj) seleObj;
			relationIdList.add(relObj.getRelId());
			successObjectNames.put(relObj.getRelId(), ((UserApplication)relObj.getRefObject()).getName());
		}
		IAdminTreeChild refObject = relationObj.getRefObject();
		String adminAreaName = null;
		IAdminTreeChild containerObject = getContainerObject(relationObj);
		String userAppName = ((UserApplication) refObject).getName();
		if (containerObject instanceof SiteAdminAreaUserApplications) {
			adminAreaName = ((AdministrationArea) ((RelationObj) containerObject.getParent()).getRefObject()).getName();
		} else if (containerObject instanceof AdminAreaUserApplications) {
			adminAreaName = ((AdministrationArea) containerObject.getParent()).getName();
		}
		if (selectionList.size() == 1) {
			confirmDialogMsg = messages.deleteUserAppConfirmDialogMsgPart1 + " \'"
					+ userAppName + "\' " + messages.deleteAdminAreaRelationMsg + " \'"
					+ adminAreaName + "' ?";
		} else {
			confirmDialogMsg = messages.removeMultiObjectsRelConfirmDialogMsg + " "
					+ messages.deleteAdminAreaRelationMsg + " \'" + adminAreaName + "' ?";
		}
		AdminAreaUserAppRelController adminAreaUserAppRelController = new AdminAreaUserAppRelController(XMAdminUtil
				.getInstance().getAAForHeaders(getContainerObject((RelationObj) selection.getFirstElement())));
		boolean isConfirmed = CustomMessageDialog.openConfirm(adminTree.getControl().getShell(), confirmDialogTitle,
				confirmDialogMsg);

		final String adminAreaName2 = adminAreaName;
		if (isConfirmed) {
			Job job = new Job("Removing Relations...") {
				@Override
				protected IStatus run(IProgressMonitor monitor) {
					monitor.beginTask("Removing Relations..", 100);
					monitor.worked(30);
					try {
						AdminAreaUserAppRelResponse adminAreaUserAppRelResponse;
						adminAreaUserAppRelResponse = adminAreaUserAppRelController
								.deleteMultiAdminAreaUserAppRel(relationIdList);
						if (adminAreaUserAppRelResponse != null) {
							List<Map<String, String>> statusMaps = adminAreaUserAppRelResponse.getStatusMaps();
							if (!statusMaps.isEmpty()) {
								for (Map<String, String> statusMap : statusMaps) {
									String string = statusMap.get("en");
									String[] split = string.split("id");
									failObjectNames.put(split[1].trim(), successObjectNames.get(split[1].trim()));
									successObjectNames.remove(split[1].trim());
								}
								XMAdminUtil.getInstance().updateLogFile(
										messages.deleteMultiObjectsDialogErrMsg, MessageType.FAILURE);
								Display.getDefault().asyncExec(new Runnable() {
									public void run() {
										CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
												messages.deleteConfirmDialogTitle,
												messages.deleteMultiObjectsDialogErrMsg);
									}
								});
							}

							Display.getDefault().asyncExec(new Runnable() {
								public void run() {
									objectExpPage.refreshExplorer();
								}
							});
							if (successObjectNames.size() > 0) {
								Iterator it = successObjectNames.entrySet().iterator();
								while (it.hasNext()) {
									Map.Entry map = (Map.Entry) it.next();
									XMAdminUtil.getInstance()
											.updateLogFile(messages.relationLbl + " " + messages.betweenLbl + " "
													+ messages.userApplicationObject + " '" + map.getValue() + "' "
													+ messages.andLbl + " " + messages.administrationAreaObject + " '"
													+ adminAreaName2 + "' " + messages.objectDelete,
													MessageType.SUCCESS);
								}
							}
							
							if (failObjectNames.size() > 0) {
								Iterator it = failObjectNames.entrySet().iterator();
								while (it.hasNext()) {
									Map.Entry map = (Map.Entry) it.next();
									XMAdminUtil.getInstance()
											.updateLogFile(messages.deleteErrorMsg + " "
													+ messages.userApplicationObject + " '" + map.getValue() + "' "
													+ messages.relationLbl + " " + messages.withLbl + " "
													+ messages.administrationAreaObject + " '" + adminAreaName2 + "' ",
													MessageType.FAILURE);
								}
							}
						}
					} catch (UnauthorizedAccessException e) {
						Display.getDefault().asyncExec(new Runnable() {
							public void run() {
								CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
										messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
							}
						});
					}
					monitor.worked(70);
					return Status.OK_STATUS;
				}
			};
			job.setUser(true);
			job.schedule();
		}
	}

	/**
	 * Delete project group projects rel.
	 *
	 * @param selection
	 *            the selection
	 * @param objectExpPage
	 *            the object exp page
	 */
	@SuppressWarnings("rawtypes")
	private void deleteProjectGroupProjectsRel(final IStructuredSelection selection, final ObjectExpPage objectExpPage) {
		Object selectionObj = selection.getFirstElement();
		RelationObj relationObj = (RelationObj) selectionObj;
		final IAdminTreeChild containerObj = relationObj.getContainerObj();
		try {
			if (!XMAdminUtil.getInstance().isRelObjAccessAllowed(containerObj,
					"GROUP_RELATION-REMOVE")) {
				CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
						messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
				return;
			}
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				CustomMessageDialog.openError(Display.getCurrent().getActiveShell(), messages.errorDialogTitile,
						messages.serverNotReachable);
			}
			LOGGER.error("Exception occured in calling access allowed API " + e);
			return;
		}
		final XMAdminUtil instance = XMAdminUtil.getInstance();
		final AdminTreeviewer adminTree = instance.getAdminTree();
		final String confirmDialogTitle = messages.deleteConfirmDialogTitle;
		String confirmDialogMsg = null;
		final List selectionList = selection.toList();
		final Set<String> relationIdList = new HashSet<>();
		final Map<String,String> successObjectNames = new HashMap<>();
		for (int i = 0; i < selectionList.size(); i++) {
			Object seleObj = selectionList.get(i);
			RelationObj relObj = (RelationObj) seleObj;
			relationIdList.add(relObj.getRelId());
			successObjectNames.put(relObj.getRelId(), ((Project)relObj.getRefObject()).getName());
		}
		IAdminTreeChild refObject = relationObj.getRefObject();
		ProjectGroupProjects projectGroupProjects = (ProjectGroupProjects) relationObj.getContainerObj();
		String projectAppGrpName = ((ProjectGroupModel) projectGroupProjects.getParent().getParent()).getName();
		String projectName = ((Project) refObject).getName();
		if (selectionList.size() == 1) {
			confirmDialogMsg = messages.deleteProjectConfirmDialogMsgPart1 + " \'" + projectName
					+ "\' " + messages.deleteProjectGroupRelationMsg + " \'"
					+ projectAppGrpName + "' ?";
		} else {
			confirmDialogMsg = messages.removeMultiObjectsRelConfirmDialogMsg + " "
					+ messages.deleteProjectGroupRelationMsg + " \'"
					+ projectAppGrpName + "' ?";
		}
		GroupRelController groupRelController = new GroupRelController();
		boolean isConfirmed = CustomMessageDialog.openConfirm(adminTree.getControl().getShell(), confirmDialogTitle,
				confirmDialogMsg);
		if (isConfirmed) {
			Job job = new Job("Removing Relations...") {
				@Override
				protected IStatus run(IProgressMonitor monitor) {
					monitor.beginTask("Removing Relations..", 100);
					monitor.worked(30);
					try {
						boolean isRelationDeleted;
						isRelationDeleted = groupRelController.deleteMultiGroupRel(relationIdList);
						if (isRelationDeleted) {
							for (int i = 0; i < selectionList.size(); i++) {
								Object selectionObj = selectionList.get(i);
								RelationObj relationObj = (RelationObj) selectionObj;
								if (relationIdList.contains(relationObj.getRelId())) {
									ProjectGroupProjects projectGroupProjects = (ProjectGroupProjects) relationObj
											.getContainerObj();
									projectGroupProjects.remove(relationObj.getRelId());
								}
							}
							Display.getDefault().asyncExec(new Runnable() {
								public void run() {
									adminTree.refresh(((RelationObj) (selection.getFirstElement())).getParent());
									adminTree.refresh(true);
									objectExpPage.refreshExplorer();
								}
							});
							if (successObjectNames.size() > 0) {
								Iterator it = successObjectNames.entrySet().iterator();
								while (it.hasNext()) {
									Map.Entry map = (Map.Entry) it.next();
									XMAdminUtil.getInstance()
											.updateLogFile(messages.relationLbl + " " + messages.betweenLbl + " "
													+ messages.projectObject +" '" + map.getValue() + "' "
													+ messages.andLbl + " " + messages.projectAppGroupObject + " '"
													+ projectAppGrpName + "' " + messages.objectDelete,
													MessageType.SUCCESS);
								}
							}
						}
					} catch (UnauthorizedAccessException e) {
						Display.getDefault().asyncExec(new Runnable() {
							public void run() {
								CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
										messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
							}
						});
					}
					monitor.worked(70);
					return Status.OK_STATUS;
				}
			};
			job.setUser(true);
			job.schedule();
		}
	}

	/**
	 * Delete directory projects rel.
	 *
	 * @param selection
	 *            the selection
	 * @param objectExpPage
	 *            the object exp page
	 */
	@SuppressWarnings("rawtypes")
	private void deleteDirectoryProjectsRel(final IStructuredSelection selection, final ObjectExpPage objectExpPage) {
		Object selectionObj = selection.getFirstElement();
		RelationObj relationObj = (RelationObj) selectionObj;
		final IAdminTreeChild containerObj = relationObj.getContainerObj();
		try {
			if (!XMAdminUtil.getInstance().isRelObjAccessAllowed(containerObj,
					"DIRECTORY_RELATION-REMOVE")) {
				CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
						messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
				return;
			}
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				CustomMessageDialog.openError(Display.getCurrent().getActiveShell(), messages.errorDialogTitile,
						messages.serverNotReachable);
			}
			LOGGER.error("Exception occured in calling access allowed API " + e);
			return;
		}
		final XMAdminUtil instance = XMAdminUtil.getInstance();
		final AdminTreeviewer adminTree = instance.getAdminTree();
		final String confirmDialogTitle = messages.deleteConfirmDialogTitle;
		String confirmDialogMsg = null;
		final List selectionList = selection.toList();
		final List<String> relationIdList = new ArrayList<>();
		final Map<String,String> successObjectNames = new HashMap<>();
		for (int i = 0; i < selectionList.size(); i++) {
			Object seleObj = selectionList.get(i);
			RelationObj relObj = (RelationObj) seleObj;
			relationIdList.add(relObj.getRelId());
			successObjectNames.put(relObj.getRelId(), ((Project)relObj.getRefObject()).getName());
		}
		IAdminTreeChild refObject = relationObj.getRefObject();
		DirectoryProjects directoryProjects = (DirectoryProjects) relationObj.getContainerObj();
		String projectName = ((Project) refObject).getName();
		String directoryName = ((Directory) directoryProjects.getParent().getParent()).getName();
		if (selectionList.size() == 1) {
			confirmDialogMsg = messages.deleteProjectConfirmDialogMsgPart1 + " \'" + projectName
					+ "\' " + messages.deleteDirectoryMsg + " \'"
					+ directoryName + "' ?";
		} else {
			confirmDialogMsg = messages.removeMultiObjectsRelConfirmDialogMsg + " " + messages.deleteDirectoryMsg
					+ " \'" + directoryName + "' ?";
		}
		DirectoryRelController directoryRelController = new DirectoryRelController();
		boolean isConfirmed = CustomMessageDialog.openConfirm(adminTree.getControl().getShell(), confirmDialogTitle,
				confirmDialogMsg);
		if (isConfirmed) {
			Job job = new Job("Removing Relations...") {
				@Override
				protected IStatus run(IProgressMonitor monitor) {
					monitor.beginTask("Removing Relations..", 100);
					monitor.worked(30);
					try {
						boolean isRelationDeleted = false;
						isRelationDeleted = directoryRelController.deleteMultiDirectory(relationIdList);
						if (isRelationDeleted) {
							for (int i = 0; i < selectionList.size(); i++) {
								Object selectionObj = selectionList.get(i);
								RelationObj relationObj = (RelationObj) selectionObj;
								if (relationIdList.contains(relationObj.getRelId())) {
									DirectoryProjects directoryProjects = (DirectoryProjects) relationObj
											.getContainerObj();
									directoryProjects.remove(relationObj.getRelId());
								}
							}
							Display.getDefault().asyncExec(new Runnable() {
								public void run() {
									adminTree.refresh(((RelationObj) (selection.getFirstElement())).getParent());
									adminTree.refresh(true);
									objectExpPage.refreshExplorer();
								}
							});
							if (successObjectNames.size() > 0) {
								Iterator it = successObjectNames.entrySet().iterator();
								while (it.hasNext()) {
									Map.Entry map = (Map.Entry) it.next();
									XMAdminUtil.getInstance().updateLogFile(
											messages.relationLbl + " " + messages.betweenLbl + " "
													+ messages.projectObject + " '" + map.getValue() + "' "
													+ messages.andLbl + " " + messages.directoryObject + " '"
													+ directoryName + "' " + messages.objectDelete,
											MessageType.SUCCESS);
								}
							}
						}
					} catch (UnauthorizedAccessException e) {
						Display.getDefault().asyncExec(new Runnable() {
							public void run() {
								CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
										messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
							}
						});
					}
					monitor.worked(70);
					return Status.OK_STATUS;
				}
			};
			job.setUser(true);
			job.schedule();
		}
	}

	/**
	 * Delete user project rel.
	 *
	 * @param selection
	 *            the selection
	 * @param objectExpPage
	 *            the object exp page
	 */
	@SuppressWarnings("rawtypes")
	private void deleteUserProjectRel(final IStructuredSelection selection, final ObjectExpPage objectExpPage) {
		Object selectionObj = selection.getFirstElement();
		RelationObj relationObj = (RelationObj) selectionObj;
		final IAdminTreeChild containerObj = relationObj.getContainerObj();
		try {
			if (!XMAdminUtil.getInstance().isRelObjAccessAllowed(containerObj,
					"USER_TO_PROJECT-REMOVE")) {
				CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
						messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
				return;
			}
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				CustomMessageDialog.openError(Display.getCurrent().getActiveShell(), messages.errorDialogTitile,
						messages.serverNotReachable);
			}
			LOGGER.error("Exception occured in calling access allowed API " + e);
			return;
		}
		final XMAdminUtil instance = XMAdminUtil.getInstance();
		final AdminTreeviewer adminTree = instance.getAdminTree();
		final String confirmDialogTitle = messages.deleteConfirmDialogTitle;
		String confirmDialogMsg = null;
		final List selectionList = selection.toList();
		final Set<String> relationIdList = new HashSet<>();
		final Map<String,String> objectNames = new HashMap<>();
		final Map<String,String> failObjectNames = new HashMap<>();
		for (int i = 0; i < selectionList.size(); i++) {
			Object seleObj = selectionList.get(i);
			RelationObj relObj = (RelationObj) seleObj;
			relationIdList.add(relObj.getRelId());
			objectNames.put(relObj.getRelId(), ((Project)relObj.getRefObject()).getName());
		}
		IAdminTreeChild refObject = relationObj.getRefObject();
		UserProjects userProjects = (UserProjects) relationObj.getParent();
		String userName = ((User) userProjects.getParent()).getName();
		String projectName = ((Project) refObject).getName();
		if (selectionList.size() == 1) {
			confirmDialogMsg = messages.deleteProjectConfirmDialogMsgPart1 + " \'" + projectName
					+ "\' " + messages.deleteUserRelationMsg + " \'" + userName
					+ "' ?";
		} else {
			confirmDialogMsg = messages.removeMultiObjectsRelConfirmDialogMsg + " " + messages.deleteUserRelationMsg
					+ " \'" + userName + "' ?";
		}

		UserProjectRelController userProjectRelController = new UserProjectRelController();
		boolean isConfirmed = CustomMessageDialog.openConfirm(adminTree.getControl().getShell(), confirmDialogTitle,
				confirmDialogMsg);
		if (isConfirmed) {
			Job job = new Job("Removing Relations...") {
				@Override
				protected IStatus run(IProgressMonitor monitor) {
					monitor.beginTask("Removing Relations..", 100);
					monitor.worked(30);
					try {
						UserProjectRelResponse userProjectRelResponse;
						userProjectRelResponse = userProjectRelController.deleteMultiUserProjectRel(relationIdList);
						if (userProjectRelResponse != null) {
							List<Map<String, String>> statusMaps = userProjectRelResponse.getStatusMaps();
							if (!statusMaps.isEmpty()) {
								for (Map<String, String> statusMap : statusMaps) {
									String string = statusMap.get("en");
										String[] split = string.split("id");
										if (split.length >1) {
										relationIdList.remove(split[1].trim());
										objectNames.remove(split[1].trim());
										failObjectNames.put(split[1].trim(),
												objectNames.get(split[1].trim()));
									}
									
								}
								Display.getDefault().asyncExec(new Runnable() {
									public void run() {
										String errorMessage = "";
										for (Map<String, String> statusMap : statusMaps) {
											errorMessage += statusMap.get(
													XMAdminUtil.getInstance().getCurrentLocaleEnum().getLangCode());
											errorMessage += "\n";
										}
										if (errorMessage.length() > 0) {
											CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
													messages.dndErrorTitle, errorMessage);
										}
									}
								});
								/*XMAdminUtil.getInstance().updateLogFile(
										messages.deleteMultiObjectsDialogErrMsg, MessageType.FAILURE);*/
								/*Display.getDefault().asyncExec(new Runnable() {
									public void run() {
										CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
												messages.deleteConfirmDialogTitle,
												messages.deleteMultiObjectsDialogErrMsg);
									}
								});*/
							}

							List<String> deletedRelIds = userProjectRelResponse.getDeletedRelIds();
							for (int i = 0; i < selectionList.size(); i++) {
								Object selectionObj = selectionList.get(i);
								RelationObj relationObj = (RelationObj) selectionObj;
								if (relationIdList.contains(relationObj.getRelId())) {
									UserProjects userProjects = (UserProjects) relationObj.getParent();
									if(deletedRelIds!=null && deletedRelIds.contains(relationObj.getRelId())) {
										userProjects.remove(relationObj.getRelId());
										
									}else{
										objectNames.remove(relationObj.getRelId());
									}
								}
							}
							Display.getDefault().asyncExec(new Runnable() {
								@SuppressWarnings("unchecked")
								public void run() {
									adminTree.refresh(((RelationObj) (selection.getFirstElement())).getParent());
									adminTree.refresh(true);
									if (isAdminTreeObject) {
										XMAdminUtil.getInstance().getAdminTree().setSelection(new StructuredSelection(((IAdminTreeChild)selection.getFirstElement()).getParent()));
									}
									objectExpPage.refreshExplorer();
									adminTree.refershBackReference(new Class[] { ProjectUsers.class });
								}
							});
							if (objectNames.size() > 0) {
								Iterator it = objectNames.entrySet().iterator();
								while (it.hasNext()) {
									Map.Entry map = (Map.Entry) it.next();
									XMAdminUtil.getInstance().updateLogFile(messages.relationLbl + " "
											+ messages.betweenLbl + " " + messages.projectObject + " '" + map.getValue()
											+ "' " + messages.andLbl + " " + messages.userObject + " '" + userName
											+ "' " + messages.objectDelete, MessageType.SUCCESS);
								}
							}
							
							Display.getDefault().asyncExec(new Runnable() {
								public void run() {
									for (Map<String, String> statusMap : statusMaps) {
										String errorMessage = statusMap.get(
												XMAdminUtil.getInstance().getCurrentLocaleEnum().getLangCode());
										XMAdminUtil.getInstance().updateLogFile(
												errorMessage, MessageType.FAILURE);
									}
								}
							});
						}
					} catch (CannotCreateObjectException e) {
						Display.getDefault().asyncExec(new Runnable() {
							public void run() {
								CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
										messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
							}
						});
					} catch (UnauthorizedAccessException e) {
						Display.getDefault().asyncExec(new Runnable() {
							public void run() {
								CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
										messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
							}
						});
					}
					monitor.worked(70);
					return Status.OK_STATUS;
				}
			};
			job.setUser(true);
			job.schedule();
		}
	}

	/**
	 * Delete site AA project rel.
	 *
	 * @param selection
	 *            the selection
	 * @param objectExpPage
	 *            the object exp page
	 */
	@SuppressWarnings({ "rawtypes" })
	private void deleteSiteAAProjectRel(final IStructuredSelection selection, final ObjectExpPage objectExpPage) {
		Object selectionObj = selection.getFirstElement();
		RelationObj relationObj = (RelationObj) selectionObj;
		final IAdminTreeChild containerObj = relationObj.getContainerObj();
		try {
			if (!XMAdminUtil.getInstance().isRelObjAccessAllowed(containerObj,
					"PROJECT_TO_ADMINISTRATION_AREA-REMOVE")) {
				CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
						messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
				return;
			}
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				CustomMessageDialog.openError(Display.getCurrent().getActiveShell(), messages.errorDialogTitile,
						messages.serverNotReachable);
			}
			LOGGER.error("Exception occured in calling access allowed API " + e);
			return;
		}
		final XMAdminUtil instance = XMAdminUtil.getInstance();
		final AdminTreeviewer adminTree = instance.getAdminTree();
		final String confirmDialogTitle = messages.deleteConfirmDialogTitle;
		String confirmDialogMsg = null;
		final List selectionList = selection.toList();
		final Set<String> relationIdList = new HashSet<>();
		final Map<String,String> successObjectNames = new HashMap<>();
		final Map<String,String> failObjectNames = new HashMap<>();
		AdministrationArea adminArea = null;
		for (int i = 0; i < selectionList.size(); i++) {
			Object seleObj = selectionList.get(i);
			RelationObj relObj = (RelationObj) seleObj;
			relationIdList.add(relObj.getRelId());
			successObjectNames.put(relObj.getRelId(), ((Project)relObj.getRefObject()).getName());

		}
		IAdminTreeChild refObject = relationObj.getRefObject();
		if (relationObj.getParent() instanceof SiteAdminAreaProjects) {
			adminArea = ((AdministrationArea) ((RelationObj) relationObj.getParent().getParent()).getRefObject());
		} else if (relationObj.getParent() instanceof AdminAreaProjects) {
			adminArea = ((AdministrationArea) relationObj.getParent().getParent());
		}
		String adminAreaName = adminArea.getName();
		String projectName = ((Project) refObject).getName();
		if (selectionList.size() == 1) {
			confirmDialogMsg = messages.deleteProjectConfirmDialogMsgPart1 + " \'" + projectName
					+ "\' " + messages.deleteAdminAreaRelationMsg + " \'" + adminAreaName + "' ?";
		} else {
			confirmDialogMsg = messages.removeMultiObjectsRelConfirmDialogMsg + " "
					+ messages.deleteAdminAreaRelationMsg + " \'" + adminAreaName + "' ?";
		}
		AdminAreaProjectRelController adminAreaProjectRelController = new AdminAreaProjectRelController(
				XMAdminUtil.getInstance().getAAForHeaders(adminArea));
		boolean isConfirmed = CustomMessageDialog.openConfirm(adminTree.getControl().getShell(), confirmDialogTitle,
				confirmDialogMsg);

		if (isConfirmed) {
			Job job = new Job("Removing Relations...") {
				@SuppressWarnings("unchecked")
				@Override
				protected IStatus run(IProgressMonitor monitor) {
					monitor.beginTask("Removing Relations..", 100);
					monitor.worked(30);
					try {
						AdminAreaProjectRelResponse adminAreaProjectRelResponse;
						adminAreaProjectRelResponse = adminAreaProjectRelController
								.deleteMultiAdminAreaProjectRel(relationIdList);
						if (adminAreaProjectRelResponse != null) {
							List<Map<String, String>> statusMaps = adminAreaProjectRelResponse.getStatusMaps();
							if (!statusMaps.isEmpty()) {
								for (Map<String, String> statusMap : statusMaps) {
									String string = statusMap.get("en");
									String[] split = string.split("id");
									relationIdList.remove(split[1].trim());
									failObjectNames.put(split[1].trim(), successObjectNames.get(split[1].trim()));
									successObjectNames.remove(split[1].trim());
								}
								XMAdminUtil.getInstance().updateLogFile(
										messages.deleteMultiObjectsDialogErrMsg, MessageType.FAILURE);
								Display.getDefault().asyncExec(new Runnable() {
									public void run() {
										CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
												messages.deleteConfirmDialogTitle,
												messages.deleteMultiObjectsDialogErrMsg);
									}
								});
							}

							for (int i = 0; i < selectionList.size(); i++) {
								Object selectionObj = selectionList.get(i);
								RelationObj relationObj = (RelationObj) selectionObj;
								if (relationIdList.contains(relationObj.getRelId())) {
									if (relationObj.getParent() instanceof SiteAdminAreaProjects) {
										((SiteAdminAreaProjects) relationObj.getParent())
												.remove(relationObj.getRelId());
									} else if (relationObj.getParent() instanceof AdminAreaProjects) {
										((AdminAreaProjects) relationObj.getParent()).remove(relationObj.getRelId());
									}
								}
							}
							Display.getDefault().asyncExec(new Runnable() {
								public void run() {
									adminTree.refresh(((RelationObj) (selection.getFirstElement())).getParent());
									adminTree.refresh(true);
									if (isAdminTreeObject) {
										XMAdminUtil.getInstance().getAdminTree().setSelection(new StructuredSelection(((IAdminTreeChild)selection.getFirstElement()).getParent()));
									}
									objectExpPage.refreshExplorer();
									Object selectionObj = selection.getFirstElement();
									RelationObj relationObj = (RelationObj) selectionObj;
									if (relationObj.getParent() instanceof SiteAdminAreaProjects) {
										adminTree.refershBackReference(new Class[] { AdminAreaProjects.class });
									} else if (relationObj.getParent() instanceof AdminAreaProjects) {
										adminTree.refershBackReference(new Class[] { SiteAdminAreaProjects.class });
									}
									adminTree.refershBackReference(
											new Class[] { ProjectAdminAreas.class, UserProjectAdminAreas.class });
								}
							});
							if (successObjectNames.size() > 0) {
								Iterator it = successObjectNames.entrySet().iterator();
								while (it.hasNext()) {
									Map.Entry map = (Map.Entry) it.next();
									XMAdminUtil.getInstance().updateLogFile(
											messages.relationLbl + " " + messages.betweenLbl + " "
													+ messages.projectObject + " '" + map.getValue() + "' "
													+ messages.andLbl + " " + messages.administrationAreaObject + " '"
													+ adminAreaName + "' " + messages.objectDelete,
											MessageType.SUCCESS);
								}
							}
							
							if (failObjectNames.size() > 0) {
								Iterator it = failObjectNames.entrySet().iterator();
								while (it.hasNext()) {
									Map.Entry map = (Map.Entry) it.next();
									XMAdminUtil.getInstance()
											.updateLogFile(messages.deleteErrorMsg + " " + messages.projectObject + " '"
													+ map.getValue() + "' " + messages.relationLbl + " "
													+ messages.withLbl + " " + messages.administrationAreaObject + " '"
													+ adminAreaName + "' ", MessageType.FAILURE);
								}
							}
						}
					} catch (UnauthorizedAccessException e) {

						Display.getDefault().asyncExec(new Runnable() {
							public void run() {
								CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
										messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
							}
						});

					}
					monitor.worked(70);
					return Status.OK_STATUS;
				}
			};
			job.setUser(true);
			job.schedule();
		}
	}

	/**
	 * Delete site AA rel.
	 *
	 * @param selection
	 *            the selection
	 * @param objectExpPage
	 *            the object exp page
	 */
	@SuppressWarnings("rawtypes")
	private void deleteSiteAARel(final IStructuredSelection selection, final ObjectExpPage objectExpPage) {
		Object selectionObj = selection.getFirstElement();
		RelationObj relationObj = (RelationObj) selectionObj;
		final IAdminTreeChild containerObj = relationObj.getContainerObj();
		try {
			if (!XMAdminUtil.getInstance().isRelObjAccessAllowed(containerObj,
					"ADMINISTRATION_AREA_TO_SITE-REMOVE")) {
				CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
						messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
				return;
			}
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				CustomMessageDialog.openError(Display.getCurrent().getActiveShell(), messages.errorDialogTitile,
						messages.serverNotReachable);
			}
			LOGGER.error("Exception occured in calling access allowed API " + e);
			return;
		}
		final XMAdminUtil instance = XMAdminUtil.getInstance();
		final AdminTreeviewer adminTree = instance.getAdminTree();
		final String confirmDialogTitle = messages.deleteConfirmDialogTitle;
		String confirmDialogMsg = null;
		final List selectionList = selection.toList();
		final Set<String> relationIdList = new HashSet<>();
		final Map<String,String> successObjectNames = new HashMap<>();
		final Map<String,String> failObjectNames = new HashMap<>();
		for (int i = 0; i < selectionList.size(); i++) {
			Object seleObj = selectionList.get(i);
			RelationObj relObj = (RelationObj) seleObj;
			relationIdList.add(relObj.getRelId());
			successObjectNames.put(relObj.getRelId(), ((AdministrationArea)relObj.getRefObject()).getName());
		}
		IAdminTreeChild refObject = relationObj.getRefObject();
		SiteAdministrations siteAdministrations = (SiteAdministrations) relationObj.getParent();
		String adminAreaName = ((AdministrationArea) refObject).getName();
		String siteName = ((Site) siteAdministrations.getParent()).getName();
		if (selectionList.size() == 1) {
			confirmDialogMsg = messages.deleteAARelationConfirmDialogMsgPart1 + " \'" + adminAreaName + "\' "
					+ messages.deleteSiteRelationMsg + " \'" + siteName + "' ?";
		} else {
			confirmDialogMsg = messages.removeMultiObjectsRelConfirmDialogMsg + " " + messages.deleteSiteRelationMsg
					+ " \'" + siteName + "' ?";
		}
		SiteAdminAreaRelController siteAdminAreaRelContler = new SiteAdminAreaRelController();
		boolean isConfirmed = CustomMessageDialog.openConfirm(adminTree.getControl().getShell(), confirmDialogTitle,
				confirmDialogMsg);

		if (isConfirmed) {
			Job job = new Job("Removing Relations...") {
				@Override
				protected IStatus run(IProgressMonitor monitor) {
					monitor.beginTask("Removing Relations..", 100);
					monitor.worked(30);
					try {
						SiteAdminAreaRelResponse siteAdminAreaRelResponse;
						siteAdminAreaRelResponse = siteAdminAreaRelContler.deleteMultiSiteAdminAreaRel(relationIdList);
						if (siteAdminAreaRelResponse != null) {
							List<Map<String, String>> statusMaps = siteAdminAreaRelResponse.getStatusMaps();
							if (!statusMaps.isEmpty()) {
								for (Map<String, String> statusMap : statusMaps) {
									String string = statusMap.get("en");
									String[] split = string.split("id");
									relationIdList.remove(split[1].trim());
									failObjectNames.put(split[1].trim(), successObjectNames.get(split[1].trim()));
									successObjectNames.remove(split[1].trim());
								}
								XMAdminUtil.getInstance().updateLogFile(
										messages.deleteMultiObjectsDialogErrMsg, MessageType.FAILURE);
								Display.getDefault().asyncExec(new Runnable() {
									public void run() {
										CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
												messages.deleteConfirmDialogTitle,
												messages.deleteMultiObjectsDialogErrMsg);
									}
								});
							}

							for (int i = 0; i < selectionList.size(); i++) {
								Object selectionObj = selectionList.get(i);
								RelationObj relationObj = (RelationObj) selectionObj;
								if (relationIdList.contains(relationObj.getRelId())) {
									IAdminTreeChild refObject = relationObj.getRefObject();
									SiteAdministrations siteAdministrations = (SiteAdministrations) relationObj
											.getParent();
									siteAdministrations.remove(relationObj.getRelId());
									((AdministrationArea) refObject).setRelationId(null);
									((AdministrationArea) refObject).setRelatedSiteName(null);
								}
							}
							Display.getDefault().asyncExec(new Runnable() {
								@SuppressWarnings("unchecked")
								public void run() {
									adminTree.refresh(((RelationObj) (selection.getFirstElement())).getParent());
									adminTree.refresh(true);
									if (isAdminTreeObject) {
										XMAdminUtil.getInstance().getAdminTree().setSelection(new StructuredSelection(((IAdminTreeChild)selection.getFirstElement()).getParent()));
									}
									objectExpPage.refreshExplorer();
									adminTree.refershBackReference(new Class[] { ProjectAdminAreas.class, UserAdminAreas.class, AdminAreaProjects.class });
								}
							});
							if (successObjectNames.size() > 0) {
								Iterator it = successObjectNames.entrySet().iterator();
								while (it.hasNext()) {
									Map.Entry map = (Map.Entry) it.next();
									XMAdminUtil.getInstance().updateLogFile(messages.relationLbl + " "
											+ messages.betweenLbl + " " + messages.administrationAreaObject + " '"
											+ map.getValue() + "' " + messages.andLbl + " " + messages.siteObject + " '"
											+ siteName + "' " + messages.objectDelete, MessageType.SUCCESS);
								}
							}
							if (failObjectNames.size() > 0) {
								Iterator it = failObjectNames.entrySet().iterator();
								while (it.hasNext()) {
									Map.Entry map = (Map.Entry) it.next();
									XMAdminUtil.getInstance()
											.updateLogFile(messages.deleteErrorMsg + " "
													+ messages.administrationAreaObject + " '" + map.getValue() + "' "
													+ messages.relationLbl + " " + messages.withLbl + " "
													+ messages.siteObject + " '" + siteName + "' ",
													MessageType.FAILURE);
								}
							}
						}
					} catch (UnauthorizedAccessException e) {

						Display.getDefault().asyncExec(new Runnable() {
							public void run() {
								CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
										messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
							}
						});
					}
					monitor.worked(70);
					return Status.OK_STATUS;
				}
			};
			job.setUser(true);
			job.schedule();
		}
	}

	/**
	 * Delete directory users rel.
	 *
	 * @param selection
	 *            the selection
	 * @param objectExpPage
	 *            the object exp page
	 */
	@SuppressWarnings("rawtypes")
	private void deleteDirectoryUsersRel(final IStructuredSelection selection, final ObjectExpPage objectExpPage) {
		Object selectionObj = selection.getFirstElement();
		RelationObj relationObj = (RelationObj) selectionObj;
		final IAdminTreeChild containerObj = relationObj.getContainerObj();
		try {
			if (!XMAdminUtil.getInstance().isRelObjAccessAllowed(containerObj,
					"DIRECTORY_RELATION-REMOVE")) {
				CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
						messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
				return;
			}
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				CustomMessageDialog.openError(Display.getCurrent().getActiveShell(), messages.errorDialogTitile,
						messages.serverNotReachable);
			}
			LOGGER.error("Exception occured in calling access allowed API " + e);
			return;
		}
		final XMAdminUtil instance = XMAdminUtil.getInstance();
		final AdminTreeviewer adminTree = instance.getAdminTree();
		final String confirmDialogTitle = messages.deleteConfirmDialogTitle;
		String confirmDialogMsg = null;
		final List selectionList = selection.toList();
		final List<String> relationIdList = new ArrayList<>();
		final Map<String,String> successObjectNames = new HashMap<>();
		for (int i = 0; i < selectionList.size(); i++) {
			Object seleObj = selectionList.get(i);
			RelationObj relObj = (RelationObj) seleObj;
			relationIdList.add(relObj.getRelId());
			successObjectNames.put(relObj.getRelId(), ((User)relObj.getRefObject()).getName());
		}
		IAdminTreeChild refObject = relationObj.getRefObject();
		String userName = ((User) refObject).getName();
		String directoryName = ((Directory) (relationObj).getContainerObj().getParent().getParent()).getName();
		if (selectionList.size() == 1) {
			confirmDialogMsg = messages.deleteUserConfirmDialogMsgPart1 + " \'" + userName + "\' "
					+ messages.deleteDirectoryMsg + " \'"
					+ directoryName + "' ?";
		} else {
			confirmDialogMsg = messages.removeMultiObjectsRelConfirmDialogMsg + " " + messages.deleteDirectoryMsg
					+ " \'" + directoryName + "' ?";
		}
		DirectoryRelController directoryRelController = new DirectoryRelController();
		boolean isConfirmed = CustomMessageDialog.openConfirm(adminTree.getControl().getShell(), confirmDialogTitle,
				confirmDialogMsg);

		if (isConfirmed) {
			Job job = new Job("Removing Relations...") {
				@Override
				protected IStatus run(IProgressMonitor monitor) {
					monitor.beginTask("Removing Relations..", 100);
					monitor.worked(30);
					try {
						boolean isRelationDeleted;
						isRelationDeleted = directoryRelController.deleteMultiDirectory(relationIdList);
						if (isRelationDeleted) {
							Display.getDefault().asyncExec(new Runnable() {
								public void run() {
									objectExpPage.refreshExplorer();
								}
							});
							if (successObjectNames.size() > 0) {
								Iterator it = successObjectNames.entrySet().iterator();
								while (it.hasNext()) {
									Map.Entry map = (Map.Entry) it.next();
									XMAdminUtil.getInstance()
											.updateLogFile(
													messages.relationLbl + " " + messages.betweenLbl + " "
															+ messages.userObject + " '" + map.getValue() + "' "
															+ messages.andLbl + " " + messages.directoryObject + " '"
															+ directoryName + "' " + messages.objectDelete,
													MessageType.SUCCESS);
								}
							}
						}
					} catch (UnauthorizedAccessException e) {
						Display.getDefault().asyncExec(new Runnable() {
							public void run() {
								CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
										messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
							}
						});
					}
					monitor.worked(70);
					return Status.OK_STATUS;
				}
			};
			job.setUser(true);
			job.schedule();
		}
	}

	/**
	 * Delete site AA start app rel.
	 *
	 * @param selection
	 *            the selection
	 * @param objectExpPage
	 *            the object exp page
	 */
	@SuppressWarnings("rawtypes")
	private void deleteSiteAAStartAppRel(final IStructuredSelection selection, final ObjectExpPage objectExpPage) {
		Object selectionObj = selection.getFirstElement();
		RelationObj relationObj = (RelationObj) selectionObj;
		final IAdminTreeChild containerObj = relationObj.getContainerObj();
		try {
			if (!XMAdminUtil.getInstance().isRelObjAccessAllowed(containerObj,
					"START_APPLICATION_TO_ADMINISTRATION_AREA-REMOVE")) {
				CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
						messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
				return;
			}
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				CustomMessageDialog.openError(Display.getCurrent().getActiveShell(), messages.errorDialogTitile,
						messages.serverNotReachable);
			}
			LOGGER.error("Exception occured in calling access allowed API " + e);
			return;
		}
		final XMAdminUtil instance = XMAdminUtil.getInstance();
		final AdminTreeviewer adminTree = instance.getAdminTree();
		final String confirmDialogTitle = messages.deleteConfirmDialogTitle;
		String confirmDialogMsg = null;
		final List selectionList = selection.toList();
		final Set<String> relationIdList = new HashSet<>();
		final Map<String,String> successObjectNames = new HashMap<>();
		final Map<String,String> failObjectNames = new HashMap<>();
		AdministrationArea administrationArea = null;
		for (int i = 0; i < selectionList.size(); i++) {
			Object seleObj = selectionList.get(i);
			RelationObj relObj = (RelationObj) seleObj;
			relationIdList.add(relObj.getRelId());
			successObjectNames.put(relObj.getRelId(), ((StartApplication)relObj.getRefObject()).getName());
		}
		IAdminTreeChild refObject = relationObj.getRefObject();
		if (relationObj.getContainerObj() instanceof SiteAdminAreaStartApplications) {
			administrationArea = ((AdministrationArea) ((RelationObj) relationObj.getContainerObj().getParent()
					.getParent()).getRefObject());

		} else if (relationObj.getContainerObj() instanceof AdminAreaStartApplications) {
			administrationArea = ((AdministrationArea) relationObj.getContainerObj().getParent().getParent());
		}
		String adminAreaName = administrationArea.getName();
		String startAppName = ((StartApplication) refObject).getName();
		if (selectionList.size() == 1) {
			confirmDialogMsg = messages.deleteStartAppConfirmDialogMsgPart1 + " \'"
					+ startAppName + "\' " + messages.deleteAdminAreaRelationMsg + " \'"
					+ adminAreaName + "' ?";
		} else {
			confirmDialogMsg = messages.removeMultiObjectsRelConfirmDialogMsg + " "
					+ messages.deleteAdminAreaRelationMsg + " \'" + adminAreaName + "' ?";
		}
		AdminAreaStartAppRelController adminAreaStartAppRelController = new AdminAreaStartAppRelController(
				XMAdminUtil.getInstance().getAAForHeaders(administrationArea));
		boolean isConfirmed = CustomMessageDialog.openConfirm(adminTree.getControl().getShell(), confirmDialogTitle,
				confirmDialogMsg);

		if (isConfirmed) {
			Job job = new Job("Removing Relations...") {
				@Override
				protected IStatus run(IProgressMonitor monitor) {
					monitor.beginTask("Removing Relations..", 100);
					monitor.worked(30);
					try {
						AdminAreaStartAppRelResponse adminAreaStartAppRelResponse;
						adminAreaStartAppRelResponse = adminAreaStartAppRelController
								.deleteMultiAdminAreaStartAppRel(relationIdList);
						if (adminAreaStartAppRelResponse != null) {
							List<Map<String, String>> statusMaps = adminAreaStartAppRelResponse.getStatusMaps();
							if (!statusMaps.isEmpty()) {
								for (Map<String, String> statusMap : statusMaps) {
									String string = statusMap.get("en");
									String[] split = string.split("id");
									relationIdList.remove(split[1].trim());
									failObjectNames.put(split[1].trim(), successObjectNames.get(split[1].trim()));
									successObjectNames.remove(split[1].trim());
								}
								XMAdminUtil.getInstance().updateLogFile(
										messages.deleteMultiObjectsDialogErrMsg, MessageType.FAILURE);
								Display.getDefault().asyncExec(new Runnable() {
									public void run() {
										CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
												messages.deleteConfirmDialogTitle,
												messages.deleteMultiObjectsDialogErrMsg);
									}
								});
							}
							Display.getDefault().asyncExec(new Runnable() {
								public void run() {
									adminTree.refresh(((RelationObj) (selection.getFirstElement())).getContainerObj());
									adminTree.refresh(true);
									objectExpPage.refreshExplorer();
								}
							});
							if (successObjectNames.size() > 0) {
								Iterator it = successObjectNames.entrySet().iterator();
								while (it.hasNext()) {
									Map.Entry map = (Map.Entry) it.next();
									XMAdminUtil.getInstance()
											.updateLogFile(messages.relationLbl + " " + messages.betweenLbl + " "
													+ messages.startApplicationObject + " '" + map.getValue() + "' "
													+ messages.andLbl + " " + messages.administrationAreaObject + " '"
													+ adminAreaName + "' " + messages.objectDelete,
													MessageType.SUCCESS);
								}
							}
							
							if (failObjectNames.size() > 0) {
								Iterator it = failObjectNames.entrySet().iterator();
								while (it.hasNext()) {
									Map.Entry map = (Map.Entry) it.next();
									XMAdminUtil.getInstance()
											.updateLogFile(messages.deleteErrorMsg + " "
													+ messages.startApplicationObject + " '" + map.getValue() + "' "
													+ messages.relationLbl + " " + messages.withLbl + " "
													+ messages.administrationAreaObject + " '" + adminAreaName + "' ",
													MessageType.FAILURE);
								}
							}
						}
					} catch (UnauthorizedAccessException e) {
						Display.getDefault().asyncExec(new Runnable() {
							public void run() {
								CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
										messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
							}
						});
					}
					monitor.worked(70);
					return Status.OK_STATUS;
				}
			};
			job.setUser(true);
			job.schedule();
		}
	}

	/**
	 * Delete site AA project start app rel.
	 *
	 * @param selection
	 *            the selection
	 * @param objectExpPage
	 *            the object exp page
	 */
	@SuppressWarnings("rawtypes")
	private void deleteSiteAAProjectStartAppRel(final IStructuredSelection selection, final ObjectExpPage objectExpPage) {
		Object selectionObj = selection.getFirstElement();
		RelationObj relationObj = (RelationObj) selectionObj;
		final IAdminTreeChild containerObj = relationObj.getContainerObj();
		try {
			if (!XMAdminUtil.getInstance().isRelObjAccessAllowed(containerObj,
					"START_APPLICATION_TO_PROJECT-REMOVE")) {
				CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
						messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
				return;
			}
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				CustomMessageDialog.openError(Display.getCurrent().getActiveShell(), messages.errorDialogTitile,
						messages.serverNotReachable);
			}
			LOGGER.error("Exception occured in calling access allowed API " + e);
			return;
		}
		final XMAdminUtil instance = XMAdminUtil.getInstance();
		final AdminTreeviewer adminTree = instance.getAdminTree();
		final String confirmDialogTitle = messages.deleteConfirmDialogTitle;
		String confirmDialogMsg = null;
		final List selectionList = selection.toList();
		final Set<String> relationIdList = new HashSet<>();
		final Map<String,String> successObjectNames = new HashMap<>();
		final Map<String,String> failObjectNames = new HashMap<>();
		for (int i = 0; i < selectionList.size(); i++) {
			Object seleObj = selectionList.get(i);
			RelationObj relObj = (RelationObj) seleObj;
			relationIdList.add(relObj.getRelId());
			successObjectNames.put(relObj.getRelId(), ((StartApplication)relObj.getRefObject()).getName());
		}
		IAdminTreeChild refObject = relationObj.getRefObject();
		String projectName = null;
		String adminAreaName = ((AdministrationArea) ((RelationObj) relationObj.getContainerObj().getParent().getParent().getParent().getParent())
				.getRefObject()).getName();
		if (relationObj.getContainerObj() instanceof SiteAdminAreaProjectStartApplications) {
			projectName = ((Project) ((RelationObj) relationObj.getContainerObj().getParent().getParent())
					.getRefObject()).getName();
		}
		String startAppName = ((StartApplication) refObject).getName();
		if (selectionList.size() == 1) {
			confirmDialogMsg = messages.deleteStartAppConfirmDialogMsgPart1 + " \'" + startAppName + "\' "
					+ messages.deleteProjectRelationMsg + " \'" + projectName + "' ?";
		} else {
			confirmDialogMsg = messages.removeMultiObjectsRelConfirmDialogMsg + " " + messages.deleteProjectRelationMsg
					+ " \'" + projectName + "' ?";
		}
		ProjectStartAppRelController projectStartAppRelController = new ProjectStartAppRelController(
				XMAdminUtil.getInstance().getAAForHeaders(
						((RelationObj) relationObj.getContainerObj().getParent().getParent().getParent().getParent())
								.getRefObject()));
		boolean isConfirmed = CustomMessageDialog.openConfirm(adminTree.getControl().getShell(), confirmDialogTitle,
				confirmDialogMsg);

		if (isConfirmed) {
			Job job = new Job("Removing Relations...") {
				@Override
				protected IStatus run(IProgressMonitor monitor) {
					monitor.beginTask("Removing Relations..", 100);
					monitor.worked(30);
					try {
						ProjectStartAppRelResponse projectStartAppRelResponse;
						projectStartAppRelResponse = projectStartAppRelController
								.deleteMultiProjectStartAppRel(relationIdList);
						if (projectStartAppRelResponse != null) {
							List<Map<String, String>> statusMaps = projectStartAppRelResponse.getStatusMaps();
							if (!statusMaps.isEmpty()) {
								for (Map<String, String> statusMap : statusMaps) {
									String string = statusMap.get("en");
									String[] split = string.split("id");
									relationIdList.remove(split[1].trim());
									failObjectNames.put(split[1].trim(), successObjectNames.get(split[1].trim()));
									successObjectNames.remove(split[1].trim());
								}
								XMAdminUtil.getInstance().updateLogFile(
										messages.deleteMultiObjectsDialogErrMsg, MessageType.FAILURE);
								Display.getDefault().asyncExec(new Runnable() {
									public void run() {
										CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
												messages.deleteConfirmDialogTitle,
												messages.deleteMultiObjectsDialogErrMsg);
									}
								});
							}
							Display.getDefault().asyncExec(new Runnable() {
								public void run() {
									adminTree.refresh(((RelationObj) (selection.getFirstElement())).getContainerObj());
									adminTree.refresh(true);
									objectExpPage.refreshExplorer();
								}
							});
							if (successObjectNames.size() > 0) {
								Iterator it = successObjectNames.entrySet().iterator();
								while (it.hasNext()) {
									Map.Entry map = (Map.Entry) it.next();
									XMAdminUtil.getInstance()
											.updateLogFile(messages.relationLbl + " " + messages.betweenLbl + " "
													+ messages.startApplicationObject + " '" + map.getValue() + "' "
													+ messages.andLbl + " " + messages.administrationAreaObject + " '"
													+ adminAreaName + "' " + messages.objectDelete,
													MessageType.SUCCESS);
								}
							}
							
							if (failObjectNames.size() > 0) {
								Iterator it = failObjectNames.entrySet().iterator();
								while (it.hasNext()) {
									Map.Entry map = (Map.Entry) it.next();
									XMAdminUtil.getInstance()
											.updateLogFile(messages.deleteErrorMsg + " "
													+ messages.startApplicationObject + " '" + map.getValue() + "' "
													+ messages.relationLbl + " " + messages.withLbl + " "
													+ messages.administrationAreaObject + " '" + adminAreaName + "' ",
													MessageType.FAILURE);
								}
							}
						}
					} catch (UnauthorizedAccessException e) {
						Display.getDefault().asyncExec(new Runnable() {
							public void run() {
								CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
										messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
							}
						});
					}
					monitor.worked(70);
					return Status.OK_STATUS;
				}
			};
			job.setUser(true);
			job.schedule();
		}
	}

	/**
	 * Delete AA project start app rel.
	 *
	 * @param selection
	 *            the selection
	 * @param objectExpPage
	 *            the object exp page
	 */
	@SuppressWarnings("rawtypes")
	private void deleteAAProjectStartAppRel(final IStructuredSelection selection, final ObjectExpPage objectExpPage) {
		Object selectionObj = selection.getFirstElement();
		RelationObj relationObj = (RelationObj) selectionObj;
		final IAdminTreeChild containerObj = relationObj.getContainerObj();
		try {
			if (!XMAdminUtil.getInstance().isRelObjAccessAllowed(containerObj,
					"START_APPLICATION_TO_PROJECT-REMOVE")) {
				CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
						messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
				return;
			}
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				CustomMessageDialog.openError(Display.getCurrent().getActiveShell(), messages.errorDialogTitile,
						messages.serverNotReachable);
			}
			LOGGER.error("Exception occured in calling access allowed API " + e);
			return;
		}
		final XMAdminUtil instance = XMAdminUtil.getInstance();
		final AdminTreeviewer adminTree = instance.getAdminTree();
		final String confirmDialogTitle = messages.deleteConfirmDialogTitle;
		String confirmDialogMsg = null;
		final List selectionList = selection.toList();
		final Set<String> relationIdList = new HashSet<>();
		final Map<String,String> successObjectNames = new HashMap<>();
		final Map<String,String> failObjectNames = new HashMap<>();
		for (int i = 0; i < selectionList.size(); i++) {
			Object seleObj = selectionList.get(i);
			RelationObj relObj = (RelationObj) seleObj;
			relationIdList.add(relObj.getRelId());
			successObjectNames.put(relObj.getRelId(), ((StartApplication)relObj.getRefObject()).getName());
		}
		IAdminTreeChild refObject = relationObj.getRefObject();
		String projectName = ((Project) ((RelationObj) relationObj.getContainerObj().getParent().getParent())
				.getRefObject()).getName();
		String startAppName = ((StartApplication) refObject).getName();
		String adminAreaName = ((AdministrationArea) (relationObj.getContainerObj().getParent().getParent().getParent().getParent())
				).getName();
		if (selectionList.size() == 1) {
			confirmDialogMsg = messages.deleteStartAppConfirmDialogMsgPart1 + " \'" + startAppName + "\' "
					+ messages.deleteProjectRelationMsg + " \'" + projectName + "' ?";
		} else {
			confirmDialogMsg = messages.removeMultiObjectsRelConfirmDialogMsg + " " + messages.deleteProjectRelationMsg
					+ " \'" + projectName + "' ?";
		}
		ProjectStartAppRelController projectStartAppRelController = new ProjectStartAppRelController(XMAdminUtil
				.getInstance().getAAForHeaders(((RelationObj) (selection.getFirstElement())).getContainerObj()));
		boolean isConfirmed = CustomMessageDialog.openConfirm(adminTree.getControl().getShell(), confirmDialogTitle,
				confirmDialogMsg);

		if (isConfirmed) {
			Job job = new Job("Removing Relations...") {
				@Override
				protected IStatus run(IProgressMonitor monitor) {
					monitor.beginTask("Removing Relations..", 100);
					monitor.worked(30);
					try {
						ProjectStartAppRelResponse projectStartAppRelResponse;
						projectStartAppRelResponse = projectStartAppRelController
								.deleteMultiProjectStartAppRel(relationIdList);
						if (projectStartAppRelResponse != null) {
							List<Map<String, String>> statusMaps = projectStartAppRelResponse.getStatusMaps();
							if (!statusMaps.isEmpty()) {
								for (Map<String, String> statusMap : statusMaps) {
									String string = statusMap.get("en");
									String[] split = string.split("id");
									relationIdList.remove(split[1].trim());
									failObjectNames.put(split[1].trim(), successObjectNames.get(split[1].trim()));
									successObjectNames.remove(split[1].trim());
								}
								XMAdminUtil.getInstance().updateLogFile(
										messages.deleteMultiObjectsDialogErrMsg, MessageType.FAILURE);
								Display.getDefault().asyncExec(new Runnable() {
									public void run() {
										CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
												messages.deleteConfirmDialogTitle,
												messages.deleteMultiObjectsDialogErrMsg);
									}
								});
							}
							Display.getDefault().asyncExec(new Runnable() {
								public void run() {
									adminTree.refresh(((RelationObj) (selection.getFirstElement())).getContainerObj());
									adminTree.refresh(true);
									objectExpPage.refreshExplorer();
								}
							});
							if (successObjectNames.size() > 0) {
								Iterator it = successObjectNames.entrySet().iterator();
								while (it.hasNext()) {
									Map.Entry map = (Map.Entry) it.next();
									XMAdminUtil.getInstance().updateLogFile(messages.relationLbl + " "
											+ messages.betweenLbl + " " + messages.startApplicationObject + " '"
											+ map.getValue() + "' " + messages.andLbl + " " + messages.projectObject
											+ " '" + projectName + "' " + messages.withLbl + " "
											+ messages.administrationAreaObject + " '" + adminAreaName + "' "
											+ messages.objectDelete, MessageType.SUCCESS);
								}
							}
							
							if (failObjectNames.size() > 0) {
								Iterator it = failObjectNames.entrySet().iterator();
								while (it.hasNext()) {
									Map.Entry map = (Map.Entry) it.next();
									XMAdminUtil.getInstance().updateLogFile(
											messages.deleteErrorMsg + " " + messages.startApplicationObject + " '"
													+ map.getValue() + "' " + messages.relationLbl + " "
													+ messages.withLbl + " " + messages.projectObject + " '"
													+ projectName + "' " + messages.andLbl + " "
													+ messages.administrationAreaObject + " '" + adminAreaName + "' ",
											MessageType.FAILURE);
								}
							}
						}
					} catch (UnauthorizedAccessException e) {
						Display.getDefault().asyncExec(new Runnable() {
							public void run() {
								CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
										messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
							}
						});
					}
					monitor.worked(70);
					return Status.OK_STATUS;
				}
			};
			job.setUser(true);
			job.schedule();
		}
	}

	/**
	 * Delete project AA project start app rel.
	 *
	 * @param selection
	 *            the selection
	 * @param objectExpPage
	 *            the object exp page
	 */
	@SuppressWarnings("rawtypes")
	private void deleteProjectAAProjectStartAppRel(final IStructuredSelection selection, final ObjectExpPage objectExpPage) {
		Object selectionObj = selection.getFirstElement();
		RelationObj relationObj = (RelationObj) selectionObj;
		final IAdminTreeChild containerObj = relationObj.getContainerObj();
		try {
			if (!XMAdminUtil.getInstance().isRelObjAccessAllowed(containerObj,
					"START_APPLICATION_TO_PROJECT-REMOVE")) {
				CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
						messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
				return;
			}
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				CustomMessageDialog.openError(Display.getCurrent().getActiveShell(), messages.errorDialogTitile,
						messages.serverNotReachable);
			}
			LOGGER.error("Exception occured in calling access allowed API " + e);
			return;
		}
		final XMAdminUtil instance = XMAdminUtil.getInstance();
		final AdminTreeviewer adminTree = instance.getAdminTree();
		final String confirmDialogTitle = messages.deleteConfirmDialogTitle;
		String confirmDialogMsg = null;
		final List selectionList = selection.toList();
		final Set<String> relationIdList = new HashSet<>();
		final Map<String,String> successObjectNames = new HashMap<>();
		final Map<String,String> failObjectNames = new HashMap<>();
		for (int i = 0; i < selectionList.size(); i++) {
			Object seleObj = selectionList.get(i);
			RelationObj relObj = (RelationObj) seleObj;
			relationIdList.add(relObj.getRelId());
			successObjectNames.put(relObj.getRelId(), ((StartApplication)relObj.getRefObject()).getName());
		}
		IAdminTreeChild refObject = relationObj.getRefObject();
		String projectName = ((Project) relationObj.getContainerObj().getParent().getParent().getParent().getParent())
				.getName();
		String startAppName = ((StartApplication) refObject).getName();
		String adminAreaName = ((AdministrationArea) ((RelationObj)relationObj.getContainerObj().getParent().getParent()).getRefObject())
				.getName();
		if (selectionList.size() == 1) {
			
			confirmDialogMsg = messages.deleteStartAppConfirmDialogMsgPart1 + " \'" + startAppName + "\' "
					+ messages.deleteProjectRelationMsg + " \'" + projectName + "' ?";
		} else {
			confirmDialogMsg = messages.removeMultiObjectsRelConfirmDialogMsg + " " + messages.deleteProjectRelationMsg
					+ " \'" + projectName + "' ?";
		}

		ProjectStartAppRelController projectStartAppRelController = new ProjectStartAppRelController(XMAdminUtil
				.getInstance().getAAForHeaders(((RelationObj) (selection.getFirstElement())).getContainerObj()));
		boolean isConfirmed = CustomMessageDialog.openConfirm(adminTree.getControl().getShell(), confirmDialogTitle,
				confirmDialogMsg);

		if (isConfirmed) {
			Job job = new Job("Removing Relations...") {
				@Override
				protected IStatus run(IProgressMonitor monitor) {
					monitor.beginTask("Removing Relations..", 100);
					monitor.worked(30);
					try {
						ProjectStartAppRelResponse projectStartAppRelResponse;
						projectStartAppRelResponse = projectStartAppRelController
								.deleteMultiProjectStartAppRel(relationIdList);
						if (projectStartAppRelResponse != null) {
							List<Map<String, String>> statusMaps = projectStartAppRelResponse.getStatusMaps();
							if (!statusMaps.isEmpty()) {
								for (Map<String, String> statusMap : statusMaps) {
									String string = statusMap.get("en");
									String[] split = string.split("id");
									relationIdList.remove(split[1].trim());
									failObjectNames.put(split[1].trim(), successObjectNames.get(split[1].trim()));
									successObjectNames.remove(split[1].trim());
								}
								XMAdminUtil.getInstance().updateLogFile(
										messages.deleteMultiObjectsDialogErrMsg, MessageType.FAILURE);
								Display.getDefault().asyncExec(new Runnable() {
									public void run() {
										CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
												messages.deleteConfirmDialogTitle,
												messages.deleteMultiObjectsDialogErrMsg);
									}
								});
							}
							Display.getDefault().asyncExec(new Runnable() {
								public void run() {
									adminTree.refresh(((RelationObj) (selection.getFirstElement())).getContainerObj());
									adminTree.refresh(true);
									objectExpPage.refreshExplorer();
								}
							});
							if (successObjectNames.size() > 0) {
								Iterator it = successObjectNames.entrySet().iterator();
								while (it.hasNext()) {
									Map.Entry map = (Map.Entry) it.next();
									XMAdminUtil.getInstance().updateLogFile(messages.relationLbl + " "
											+ messages.betweenLbl + " " + messages.startApplicationObject + " '"
											+ map.getValue() + "' " + messages.andLbl + " " + messages.projectObject
											+ " '" + projectName + "' " + messages.withLbl + " "
											+ messages.administrationAreaObject + " '" + adminAreaName + "' "
											+ messages.objectDelete, MessageType.SUCCESS);
								}
							}
							
							if (failObjectNames.size() > 0) {
								Iterator it = failObjectNames.entrySet().iterator();
								while (it.hasNext()) {
									Map.Entry map = (Map.Entry) it.next();
									XMAdminUtil.getInstance().updateLogFile(
											messages.deleteErrorMsg + " " + messages.startApplicationObject + " '"
													+ map.getValue() + "' " + messages.relationLbl + " "
													+ messages.withLbl + " " + messages.projectObject + " '"
													+ projectName + "' " + messages.andLbl + " "
													+ messages.administrationAreaObject + " '" + adminAreaName + "' ",
											MessageType.FAILURE);
								}
							}
						}
					} catch (UnauthorizedAccessException e) {
						Display.getDefault().asyncExec(new Runnable() {
							public void run() {
								CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
										messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
							}
						});

					}
					monitor.worked(70);
					return Status.OK_STATUS;
				}
			};
			job.setUser(true);
			job.schedule();
		}
	}

	/**
	 * Delete project user rel.
	 *
	 * @param selection
	 *            the selection
	 * @param objectExpPage
	 *            the object exp page
	 */
	@SuppressWarnings("rawtypes")
	private void deleteProjectUserRel(final IStructuredSelection selection, final ObjectExpPage objectExpPage) {
		Object selectionObj = selection.getFirstElement();
		RelationObj relationObj = (RelationObj) selectionObj;
		final IAdminTreeChild containerObj = relationObj.getContainerObj();
		try {
			if (!XMAdminUtil.getInstance().isRelObjAccessAllowed(containerObj,
					"USER_TO_PROJECT-REMOVE")) {
				CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
						messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
				return;
			}
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				CustomMessageDialog.openError(Display.getCurrent().getActiveShell(), messages.errorDialogTitile,
						messages.serverNotReachable);
			}
			LOGGER.error("Exception occured in calling access allowed API " + e);
			return;
		}
		final XMAdminUtil instance = XMAdminUtil.getInstance();
		final AdminTreeviewer adminTree = instance.getAdminTree();
		final String confirmDialogTitle = messages.deleteConfirmDialogTitle;
		String confirmDialogMsg = null;
		final List selectionList = selection.toList();
		final Set<String> relationIdList = new HashSet<>();
		final Map<String,String> objectNames = new HashMap<>();
		final Map<String,String> failObjectNames = new HashMap<>();
		for (int i = 0; i < selectionList.size(); i++) {
			Object seleObj = selectionList.get(i);
			RelationObj relObj = (RelationObj) seleObj;
			relationIdList.add(relObj.getRelId());
			objectNames.put(relObj.getRelId(), ((User)relObj.getRefObject()).getName());
		}
		IAdminTreeChild refObject = relationObj.getRefObject();
		String projectName = ((Project) relationObj.getContainerObj().getParent().getParent()).getName();
		String userName = ((User) refObject).getName();
		if (selectionList.size() == 1) {
			confirmDialogMsg = messages.deleteUserConfirmDialogMsgPart1 + " \'" + userName + "\' "
					+ messages.deleteProjectRelationMsg + " \'" + projectName + "' ?";
		} else {
			confirmDialogMsg = messages.removeMultiObjectsRelConfirmDialogMsg + " " + messages.deleteProjectRelationMsg
					+ " \'" + projectName + "' ?";
		}
		UserProjectRelController userProjectRelController = new UserProjectRelController();
		boolean isConfirmed = CustomMessageDialog.openConfirm(adminTree.getControl().getShell(), confirmDialogTitle,
				confirmDialogMsg);

		if (isConfirmed) {
			Job job = new Job("Removing Relations...") {
				@Override
				protected IStatus run(IProgressMonitor monitor) {
					monitor.beginTask("Removing Relations..", 100);
					monitor.worked(30);
					try {
						UserProjectRelResponse userProjectRelResponse;
						userProjectRelResponse = userProjectRelController.deleteMultiUserProjectRel(relationIdList);
						if (userProjectRelResponse != null) {
							List<Map<String, String>> statusMaps = userProjectRelResponse.getStatusMaps();
							if (!statusMaps.isEmpty()) {
								for (Map<String, String> statusMap : statusMaps) {
									String string = statusMap.get("en");
										String[] split = string.split("id");
										if (split.length >1) {
										relationIdList.remove(split[1].trim());
										objectNames.remove(split[1].trim());
										failObjectNames.put(split[1].trim(),
												objectNames.get(split[1].trim()));
									}
									
								}
								Display.getDefault().asyncExec(new Runnable() {
									public void run() {
										String errorMessage = "";
										for (Map<String, String> statusMap : statusMaps) {
											errorMessage += statusMap.get(
													XMAdminUtil.getInstance().getCurrentLocaleEnum().getLangCode());
											errorMessage += "\n";
										}
										if (errorMessage.length() > 0) {
											CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
													messages.dndErrorTitle, errorMessage);
										}
									}
								});
								/*XMAdminUtil.getInstance().updateLogFile(
										messages.deleteMultiObjectsDialogErrMsg, MessageType.FAILURE);
								Display.getDefault().asyncExec(new Runnable() {
									public void run() {
										CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
												messages.deleteConfirmDialogTitle,
												messages.deleteMultiObjectsDialogErrMsg);
									}
								});*/
							}
							List<String> deletedRelIds = userProjectRelResponse.getDeletedRelIds();
							for (int i = 0; i < selectionList.size(); i++) {
								Object selectionObj = selectionList.get(i);
								RelationObj relationObj = (RelationObj) selectionObj;
								if (relationIdList.contains(relationObj.getRelId())) {
									ProjectUsers projectUsers = (ProjectUsers) relationObj.getContainerObj()
											.getParent();
									if (deletedRelIds != null && deletedRelIds.contains(relationObj.getRelId())) {
										projectUsers.remove(relationObj.getRelId());
									} else {
										objectNames.remove(relationObj.getRelId());
									}
								}
							}
							Display.getDefault().asyncExec(new Runnable() {
								@SuppressWarnings("unchecked")
								public void run() {
									adminTree.refresh(((RelationObj) (selection.getFirstElement())).getContainerObj()
											.getParent());
									adminTree.refresh(true);
									if (isAdminTreeObject) {
										XMAdminUtil.getInstance().getAdminTree().setSelection(new StructuredSelection(((IAdminTreeChild)selection.getFirstElement()).getParent()));
									}
									objectExpPage.refreshExplorer();
									adminTree.refershBackReference(new Class[] { UserProjects.class });
								}
							});
							if (objectNames.size() > 0) {
								Iterator it = objectNames.entrySet().iterator();
								while (it.hasNext()) {
									Map.Entry map = (Map.Entry) it.next();
									XMAdminUtil.getInstance().updateLogFile(messages.relationLbl + " "
											+ messages.betweenLbl + " " + messages.userObject + " '" + map.getValue()
											+ "' " + messages.andLbl + " " + messages.projectObject + " '" + projectName
											+ "' " + messages.objectDelete, MessageType.SUCCESS);
								}
							}
							
							Display.getDefault().asyncExec(new Runnable() {
								public void run() {
									for (Map<String, String> statusMap : statusMaps) {
										String errorMessage = statusMap.get(
												XMAdminUtil.getInstance().getCurrentLocaleEnum().getLangCode());
										XMAdminUtil.getInstance().updateLogFile(
												errorMessage, MessageType.FAILURE);
									}
								}
							});
							/*if (failObjectNames.size() > 0) {
								Iterator it = failObjectNames.entrySet().iterator();
								while (it.hasNext()) {
									Map.Entry map = (Map.Entry) it.next();
									XMAdminUtil.getInstance().updateLogFile(
											messages.deleteErrorMsg + " " + messages.userObject + " '" + map.getValue()
													+ "' " + messages.relationLbl + " " + messages.withLbl + " "
													+ messages.projectObject + " '" + projectName + "' ",
											MessageType.FAILURE);
								}
							}*/
						}
					} catch (CannotCreateObjectException e) {
						Display.getDefault().asyncExec(new Runnable() {
							public void run() {
								CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
										messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
							}
						});
					} catch (UnauthorizedAccessException e) {
						Display.getDefault().asyncExec(new Runnable() {
							public void run() {
								CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
										messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
							}
						});
					}
					monitor.worked(70);
					return Status.OK_STATUS;
				}
			};
			job.setUser(true);
			job.schedule();
		}
	}

	/**
	 * Delete admin user rel.
	 *
	 * @param selection
	 *            the selection
	 * @param objectExpPage
	 *            the object exp page
	 */
	@SuppressWarnings("rawtypes")
	private void deleteAdminUserRel(IStructuredSelection selection, ObjectExpPage objectExpPage) {
		Object selectionObj = selection.getFirstElement();
		RelationObj relationObj = (RelationObj) selectionObj;
		final IAdminTreeChild containerObj = relationObj.getContainerObj();
		try {
			if (!XMAdminUtil.getInstance().isRelObjAccessAllowed(containerObj,
					"ADMIN_MENU_CONFIG-REMOVE")) {
				CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
						messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
				return;
			}
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				CustomMessageDialog.openError(Display.getCurrent().getActiveShell(), messages.errorDialogTitile,
						messages.serverNotReachable);
			}
			LOGGER.error("Exception occured in calling access allowed API " + e);
			return;
		}
		final XMAdminUtil instance = XMAdminUtil.getInstance();
		final AdminTreeviewer adminTree = instance.getAdminTree();
		final String confirmDialogTitle = messages.deleteConfirmDialogTitle;
		String confirmDialogMsg = null;
		final List selectionList = selection.toList();
		final Set<String> relationIdList = new HashSet<>();
		final Map<String,String> successObjectNames = new HashMap<>();
		final Map<String,String> failObjectNames = new HashMap<>();
		for (int i = 0; i < selectionList.size(); i++) {
			Object seleObj = selectionList.get(i);
			RelationObj relObj = (RelationObj) seleObj;
			relationIdList.add(relObj.getRelId());
			successObjectNames.put(relObj.getRelId(), ((User)relObj.getRefObject()).getName());
		}
		IAdminTreeChild refObject = relationObj.getRefObject();
		String userName = ((User) refObject).getName();
		if (selectionList.size() == 1) {
			confirmDialogMsg = messages.deleteUserConfirmDialogMsgPart1 + " \'" + userName + "\' "
					+ messages.deleteAdminMenuRelationMsg + " ?";
		} else {
			confirmDialogMsg = messages.removeMultiObjectsRelConfirmDialogMsg + " "
					+ messages.deleteAdminMenuRelationMsg + " ?";
		}
		AdminMenuConfigRelController adminMenuConfigRelController = new AdminMenuConfigRelController();
		boolean isConfirmed = CustomMessageDialog.openConfirm(adminTree.getControl().getShell(), confirmDialogTitle,
				confirmDialogMsg);

		if (isConfirmed) {
			Job job = new Job("Removing Relations...") {
				@Override
				protected IStatus run(IProgressMonitor monitor) {
					monitor.beginTask("Removing Relations..", 100);
					monitor.worked(30);
					try {
						AdminMenuConfigResponse adminMenuConfigResponse;
						adminMenuConfigResponse = adminMenuConfigRelController.deleteMultiAdminMenuRel(relationIdList);
						if (adminMenuConfigResponse != null) {
							List<Map<String, String>> statusMaps = adminMenuConfigResponse.getStatusMaps();
							if (!statusMaps.isEmpty()) {
								for (Map<String, String> statusMap : statusMaps) {
									String string = statusMap.get("en");
									String[] split = string.split("id");
									relationIdList.remove(split[1].trim());
									failObjectNames.put(split[1].trim(), successObjectNames.get(split[1].trim()));
									successObjectNames.remove(split[1].trim());
								}
								XMAdminUtil.getInstance().updateLogFile(
										messages.deleteMultiObjectsDialogErrMsg, MessageType.FAILURE);
								Display.getDefault().asyncExec(new Runnable() {
									public void run() {
										CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
												messages.deleteConfirmDialogTitle,
												messages.deleteMultiObjectsDialogErrMsg);
									}
								});
							}
							Display.getDefault().asyncExec(new Runnable() {
								public void run() {
									objectExpPage.refreshExplorer();
								}
							});
							if (successObjectNames.size() > 0) {
								Iterator it = successObjectNames.entrySet().iterator();
								while (it.hasNext()) {
									Map.Entry map = (Map.Entry) it.next();
									XMAdminUtil.getInstance().updateLogFile(
											messages.relationLbl + " " + messages.betweenLbl + " " + messages.userObject
													+ " '" + map.getValue() + "' " + messages.andLbl + " "
													+ messages.adminMenuNode + " " + messages.objectDelete,
											MessageType.SUCCESS);
								}
							}
							if (failObjectNames.size() > 0) {
								Iterator it = failObjectNames.entrySet().iterator();
								while (it.hasNext()) {
									Map.Entry map = (Map.Entry) it.next();
									XMAdminUtil.getInstance()
											.updateLogFile(
													messages.deleteErrorMsg + " " + messages.userObject + " '"
															+ map.getValue() + "' " + messages.relationLbl + " "
															+ messages.withLbl + " " + messages.adminMenuNode,
													MessageType.FAILURE);
								}
							}
						}
					} catch (UnauthorizedAccessException e) {
						Display.getDefault().asyncExec(new Runnable() {
							public void run() {
								CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
										messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
							}
						});
					}
					monitor.worked(70);
					return Status.OK_STATUS;
				}
			};
			job.setUser(true);
			job.schedule();
		}
	}

	/**
	 * Delete user start app rel.
	 *
	 * @param selection
	 *            the selection
	 * @param objectExpPage
	 *            the object exp page
	 */
	@SuppressWarnings("rawtypes")
	private void deleteUserStartAppRel(final IStructuredSelection selection, final ObjectExpPage objectExpPage) {
		Object selectionObj = selection.getFirstElement();
		RelationObj relationObj = (RelationObj) selectionObj;
		final IAdminTreeChild containerObj = relationObj.getContainerObj();
		try {
			if (!XMAdminUtil.getInstance().isRelObjAccessAllowed(containerObj,
					"START_APPLICATION_TO_USER-REMOVE")) {
				CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
						messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
				return;
			}
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				CustomMessageDialog.openError(Display.getCurrent().getActiveShell(), messages.errorDialogTitile,
						messages.serverNotReachable);
			}
			LOGGER.error("Exception occured in calling access allowed API " + e);
			return;
		}
		final XMAdminUtil instance = XMAdminUtil.getInstance();
		final AdminTreeviewer adminTree = instance.getAdminTree();
		final String confirmDialogTitle = messages.deleteConfirmDialogTitle;
		String confirmDialogMsg = null;
		final List selectionList = selection.toList();
		final Set<String> relationIdList = new HashSet<>();
		final Map<String,String> successObjectNames = new HashMap<>();
		final Map<String,String> failObjectNames = new HashMap<>();
		for (int i = 0; i < selectionList.size(); i++) {
			Object seleObj = selectionList.get(i);
			RelationObj relObj = (RelationObj) seleObj;
			relationIdList.add(relObj.getRelId());
			successObjectNames.put(relObj.getRelId(), ((StartApplication)relObj.getRefObject()).getName());
		}
		IAdminTreeChild refObject = relationObj.getRefObject();
		String userName = ((User) relationObj.getContainerObj().getParent().getParent()).getName();
		String startAppName = ((StartApplication) refObject).getName();
		if (selectionList.size() == 1) {
			confirmDialogMsg = messages.deleteStartAppConfirmDialogMsgPart1 + " \'" + startAppName + "\' "
					+ messages.deleteUserRelationMsg + " \'" + userName + "' ?";
		} else {
			confirmDialogMsg = messages.removeMultiObjectsRelConfirmDialogMsg + " " + messages.deleteUserRelationMsg
					+ " \'" + userName + "' ?";
		}
		UserStartAppRelController userStartAppRelController = new UserStartAppRelController();
		boolean isConfirmed = CustomMessageDialog.openConfirm(adminTree.getControl().getShell(), confirmDialogTitle,
				confirmDialogMsg);

		if (isConfirmed) {
			Job job = new Job("Removing Relations...") {
				@Override
				protected IStatus run(IProgressMonitor monitor) {
					monitor.beginTask("Removing Relations..", 100);
					monitor.worked(30);
					try {
						UserStartAppRelResponse userStartAppRelResponse;
						userStartAppRelResponse = userStartAppRelController.deleteMultiUserStartAppRel(relationIdList);
						if (userStartAppRelResponse != null) {
							List<Map<String, String>> statusMaps = userStartAppRelResponse.getStatusMaps();
							if (!statusMaps.isEmpty()) {
								for (Map<String, String> statusMap : statusMaps) {
									String string = statusMap.get("en");
									String[] split = string.split("id");
									relationIdList.remove(split[1].trim());
									failObjectNames.put(split[1].trim(), successObjectNames.get(split[1].trim()));
									successObjectNames.remove(split[1].trim());
								}
								XMAdminUtil.getInstance().updateLogFile(
										messages.deleteMultiObjectsDialogErrMsg, MessageType.FAILURE);
								Display.getDefault().asyncExec(new Runnable() {
									public void run() {
										CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
												messages.deleteConfirmDialogTitle,
												messages.deleteMultiObjectsDialogErrMsg);
									}
								});
							}
							Display.getDefault().asyncExec(new Runnable() {
								public void run() {
									adminTree.refresh(((RelationObj) (selection.getFirstElement())).getContainerObj());
									adminTree.refresh(true);
									objectExpPage.refreshExplorer();
								}
							});
							if (successObjectNames.size() > 0) {
								Iterator it = successObjectNames.entrySet().iterator();
								while (it.hasNext()) {
									Map.Entry map = (Map.Entry) it.next();
									XMAdminUtil.getInstance()
											.updateLogFile(
													messages.relationLbl + " " + messages.betweenLbl + " "
															+ messages.startApplicationObject + " '" + map.getValue() + "' "
															+ messages.andLbl + " " + messages.userObject
															+ " '" + userName + "' " + messages.objectDelete,
													MessageType.SUCCESS);
								}
							}
							if (failObjectNames.size() > 0) {
								Iterator it = failObjectNames.entrySet().iterator();
								while (it.hasNext()) {
									Map.Entry map = (Map.Entry) it.next();
									XMAdminUtil.getInstance().updateLogFile(
											messages.deleteErrorMsg + " " + messages.startApplicationObject + " '" + map.getValue()
													+ "' " + messages.relationLbl + " " + messages.withLbl + " "
													+ messages.userObject + " '" + userName + "' ",
											MessageType.FAILURE);
								}
							}
						}
					} catch (UnauthorizedAccessException e) {

						Display.getDefault().asyncExec(new Runnable() {
							public void run() {
								CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
										messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
							}
						});

					}
					monitor.worked(70);
					return Status.OK_STATUS;
				}
			};
			job.setUser(true);
			job.schedule();
		}
	}

	/**
	 * Delete role user rel.
	 *
	 * @param selection
	 *            the selection
	 * @param objectExpPage
	 *            the object exp page
	 */
	@SuppressWarnings("rawtypes")
	private void deleteRoleUserRel(final IStructuredSelection selection, final ObjectExpPage objectExpPage) {
		Object selectionObj = selection.getFirstElement();
		RelationObj relationObj = (RelationObj) selectionObj;
		final IAdminTreeChild containerObj = relationObj.getContainerObj();
		try {
			if (!XMAdminUtil.getInstance().isRelObjAccessAllowed(containerObj,
					"USER_TO_ROLE-REMOVE")) {
				CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
						messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
				return;
			}
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				CustomMessageDialog.openError(Display.getCurrent().getActiveShell(), messages.errorDialogTitile,
						messages.serverNotReachable);
			}
			LOGGER.error("Exception occured in calling access allowed API " + e);
			return;
		}
		final XMAdminUtil instance = XMAdminUtil.getInstance();
		final AdminTreeviewer adminTree = instance.getAdminTree();
		final String confirmDialogTitle = messages.deleteConfirmDialogTitle;
		String confirmDialogMsg = null;
		final List selectionList = selection.toList();
		final Set<String> relationIdList = new HashSet<>();
		final Map<String,String> successObjectNames = new HashMap<>();
		final Map<String,String> failObjectNames = new HashMap<>();
		for (int i = 0; i < selectionList.size(); i++) {
			Object seleObj = selectionList.get(i);
			RelationObj relObj = (RelationObj) seleObj;
			relationIdList.add(relObj.getRelId());
			successObjectNames.put(relObj.getRelId(), ((User)relObj.getRefObject()).getName());
		}
		IAdminTreeChild refObject = relationObj.getRefObject();
		RoleUsers roleUsers = (RoleUsers) relationObj.getContainerObj();
		Role role = (Role) roleUsers.getParent().getParent();
		String userName = ((User) refObject).getName();
		String roleName = role.getRoleName();
		if (selectionList.size() == 1) {
			
			confirmDialogMsg = messages.deleteUserConfirmDialogMsgPart1 + " \'" + userName + "\' "
					+ messages.deleteRoleRelationMsg + " \'" + roleName + "' ?";
		} else {
			confirmDialogMsg = messages.removeMultiObjectsRelConfirmDialogMsg + " " + messages.deleteRoleRelationMsg
					+ " \'" + roleName + "' ?";
		}
		RoleUserRelController userStartAppRelController = new RoleUserRelController();
		boolean isConfirmed = CustomMessageDialog.openConfirm(adminTree.getControl().getShell(), confirmDialogTitle,
				confirmDialogMsg);

		if (isConfirmed) {
			Job job = new Job("Removing Relations...") {
				@Override
				protected IStatus run(IProgressMonitor monitor) {
					monitor.beginTask("Removing Relations..", 100);
					monitor.worked(30);
					try {
						RoleUserRelResponse roleUserRelResponse;
						roleUserRelResponse = userStartAppRelController.deleteMultiRoleUserRel(relationIdList);
						if (roleUserRelResponse != null) {
							List<Map<String, String>> statusMaps = roleUserRelResponse.getStatusMap();
							if (!statusMaps.isEmpty()) {
								for (Map<String, String> statusMap : statusMaps) {
									String string = statusMap.get("en");
									String[] split = string.split("id");
									relationIdList.remove(split[1].trim());
									failObjectNames.put(split[1].trim(), successObjectNames.get(split[1].trim()));
									successObjectNames.remove(split[1].trim());
								}
								XMAdminUtil.getInstance().updateLogFile(
										messages.deleteMultiObjectsDialogErrMsg, MessageType.FAILURE);
								Display.getDefault().asyncExec(new Runnable() {
									public void run() {
										CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
												messages.deleteConfirmDialogTitle,
												messages.deleteMultiObjectsDialogErrMsg);
									}
								});
							}
							Display.getDefault().asyncExec(new Runnable() {
								public void run() {
									objectExpPage.refreshExplorer();
								}
							});
							if (successObjectNames.size() > 0) {
								Iterator it = successObjectNames.entrySet().iterator();
								while (it.hasNext()) {
									Map.Entry map = (Map.Entry) it.next();
									XMAdminUtil.getInstance().updateLogFile(messages.relationLbl + " "
											+ messages.betweenLbl + " " + messages.userObject + " '" + map.getValue()
											+ "' " + messages.andLbl + " " + messages.roleObject + " '" + roleName
											+ "' " + messages.objectDelete, MessageType.SUCCESS);
								}
							}
							if (failObjectNames.size() > 0) {
								Iterator it = failObjectNames.entrySet().iterator();
								while (it.hasNext()) {
									Map.Entry map = (Map.Entry) it.next();
									XMAdminUtil.getInstance().updateLogFile(
											messages.deleteErrorMsg + " " + messages.userObject + " '" + map.getValue()
													+ "' " + messages.relationLbl + " " + messages.withLbl + " "
													+ messages.roleObject + " '" + roleName + "' ",
											MessageType.FAILURE);
								}
							}
						}
					} catch (UnauthorizedAccessException e) {

						Display.getDefault().asyncExec(new Runnable() {
							public void run() {
								CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
										messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
							}
						});

					}
					monitor.worked(70);
					return Status.OK_STATUS;
				}
			};
			job.setUser(true);
			job.schedule();
		}
	}

	/**
	 * Delete role scope object user rel.
	 *
	 * @param selection
	 *            the selection
	 * @param objectExpPage
	 *            the object exp page
	 */
	@SuppressWarnings("rawtypes")
	private void deleteRoleScopeObjectUserRel(final IStructuredSelection selection, final ObjectExpPage objectExpPage) {
		Object selectionObj = selection.getFirstElement();
		RelationObj relationObj = (RelationObj) selectionObj;
		final IAdminTreeChild containerObj = relationObj.getContainerObj();
		try {
			if (!XMAdminUtil.getInstance().isRelObjAccessAllowed(containerObj,
					"USER_TO_ROLE-REMOVE")) {
				CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
						messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
				return;
			}
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				CustomMessageDialog.openError(Display.getCurrent().getActiveShell(), messages.errorDialogTitile,
						messages.serverNotReachable);
			}
			LOGGER.error("Exception occured in calling access allowed API " + e);
			return;
		}
		final XMAdminUtil instance = XMAdminUtil.getInstance();
		final AdminTreeviewer adminTree = instance.getAdminTree();
		final String confirmDialogTitle = messages.deleteConfirmDialogTitle;
		String confirmDialogMsg = null;
		final List selectionList = selection.toList();
		final Set<String> relationIdList = new HashSet<>();
		final Map<String,String> successObjectNames = new HashMap<>();
		final Map<String,String> failObjectNames = new HashMap<>();
		for (int i = 0; i < selectionList.size(); i++) {
			Object seleObj = selectionList.get(i);
			RelationObj relObj = (RelationObj) seleObj;
			relationIdList.add(relObj.getRelId());
			successObjectNames.put(relObj.getRelId(), ((User)relObj.getRefObject()).getName());
		}
		IAdminTreeChild refObject = relationObj.getRefObject();
		String userName = ((User) refObject).getName();
		String adminAreaName = ((AdministrationArea) ((RelationObj) relationObj.getContainerObj().getParent().getParent())
				.getRefObject()).getName();
		String roleName = ((Role) (relationObj.getContainerObj().getParent().getParent().getParent().getParent())).getRoleName();
		if (selectionList.size() == 1) {
			confirmDialogMsg = messages.deleteUserConfirmDialogMsgPart1 + " \'" + userName + "\' "
					+ messages.deleteAdminAreaRelationMsg + " \'"
					+ ((AdministrationArea) ((RelationObj) relationObj.getContainerObj().getParent().getParent())
							.getRefObject()).getName()
					+ "' ?";
		} else {
			confirmDialogMsg = messages.removeMultiObjectsRelConfirmDialogMsg + " "
					+ messages.deleteAdminAreaRelationMsg + " \'"
					+ ((AdministrationArea) ((RelationObj) relationObj.getContainerObj().getParent().getParent())
							.getRefObject()).getName()
					+ "' ?";
		}
		RoleUserRelController roleUserRelController = new RoleUserRelController();
		boolean isConfirmed = CustomMessageDialog.openConfirm(adminTree.getControl().getShell(), confirmDialogTitle,
				confirmDialogMsg);

		if (isConfirmed) {
			Job job = new Job("Removing Relations...") {
				@Override
				protected IStatus run(IProgressMonitor monitor) {
					monitor.beginTask("Removing Relations..", 100);
					monitor.worked(30);
					try {
						RoleUserRelResponse roleUserRelResponse;
						roleUserRelResponse = roleUserRelController.deleteMultiRoleUserRel(relationIdList);
						if (roleUserRelResponse != null) {
							List<Map<String, String>> statusMaps = roleUserRelResponse.getStatusMap();
							if (!statusMaps.isEmpty()) {
								for (Map<String, String> statusMap : statusMaps) {
									String string = statusMap.get("en");
									String[] split = string.split("id");
									relationIdList.remove(split[1].trim());
									failObjectNames.put(split[1].trim(), successObjectNames.get(split[1].trim()));
									successObjectNames.remove(split[1].trim());
								}
								XMAdminUtil.getInstance().updateLogFile(
										messages.deleteMultiObjectsDialogErrMsg, MessageType.FAILURE);
								Display.getDefault().asyncExec(new Runnable() {
									public void run() {
										CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
												messages.deleteConfirmDialogTitle,
												messages.deleteMultiObjectsDialogErrMsg);
									}
								});
							}
							Display.getDefault().asyncExec(new Runnable() {
								public void run() {
									objectExpPage.refreshExplorer();
								}
							});
							if (successObjectNames.size() > 0) {
								Iterator it = successObjectNames.entrySet().iterator();
								while (it.hasNext()) {
									Map.Entry map = (Map.Entry) it.next();
									XMAdminUtil.getInstance().updateLogFile(messages.relationLbl + " "
											+ messages.betweenLbl + " " + messages.userObject + " '" + map.getValue()
											+ "' " + messages.andLbl + " " + messages.administrationAreaObject + " '"
											+ adminAreaName + "' " + messages.withLbl + " " + messages.roleObject + " '"
											+ roleName + "' " + messages.objectDelete, MessageType.SUCCESS);
								}
							}
							
							if (failObjectNames.size() > 0) {
								Iterator it = failObjectNames.entrySet().iterator();
								while (it.hasNext()) {
									Map.Entry map = (Map.Entry) it.next();
									XMAdminUtil.getInstance().updateLogFile(messages.deleteErrorMsg + " "
											+ messages.userObject + " '" + map.getValue() + "' " + messages.relationLbl
											+ " " + messages.withLbl + " " + messages.administrationAreaObject + "'"
											+ adminAreaName + "' " + messages.andLbl + " " + messages.roleObject + " '"
											+ roleName + "' ", MessageType.FAILURE);
								}
							}
						}
					} catch (UnauthorizedAccessException e) {

						Display.getDefault().asyncExec(new Runnable() {
							public void run() {
								CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
										messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
							}
						});

					}
					monitor.worked(70);
					return Status.OK_STATUS;
				}
			};
			job.setUser(true);
			job.schedule();
		}
	}

	/**
	 * Delete role scope object child rel.
	 *
	 * @param selection
	 *            the selection
	 * @param objectExpPage
	 *            the object exp page
	 */
	@SuppressWarnings("rawtypes")
	private void deleteRoleScopeObjectChildRel(final IStructuredSelection selection, final ObjectExpPage objectExpPage) {
		Object selectionObj = selection.getFirstElement();
		RelationObj relationObj = (RelationObj) selectionObj;
		final IAdminTreeChild containerObj = relationObj.getContainerObj();
		try {
			if (!XMAdminUtil.getInstance().isRelObjAccessAllowed(containerObj,
					"ADMINISTRATION_AREA_TO_ROLE-REMOVE")) {
				CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
						messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
				return;
			}
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				CustomMessageDialog.openError(Display.getCurrent().getActiveShell(), messages.errorDialogTitile,
						messages.serverNotReachable);
			}
			LOGGER.error("Exception occured in calling access allowed API " + e);
			return;
		}
		final XMAdminUtil instance = XMAdminUtil.getInstance();
		final AdminTreeviewer adminTree = instance.getAdminTree();
		final String confirmDialogTitle = messages.deleteConfirmDialogTitle;
		String confirmDialogMsg = null;
		final List selectionList = selection.toList();
		final Set<String> relationIdList = new HashSet<>();
		final Map<String,String> successObjectNames = new HashMap<>();
		final Map<String,String> failObjectNames = new HashMap<>();
		for (int i = 0; i < selectionList.size(); i++) {
			Object seleObj = selectionList.get(i);
			RelationObj relObj = (RelationObj) seleObj;
			relationIdList.add(relObj.getRelId());
			successObjectNames.put(relObj.getRelId(), ((AdministrationArea)relObj.getRefObject()).getName());
		}
		IAdminTreeChild refObject = relationObj.getRefObject();
		RoleScopeObjects roleScopeObjects = (RoleScopeObjects) relationObj.getParent();
		Role role = (Role) (roleScopeObjects).getParent();
		String roleName = role.getRoleName();
		String adminAreaName = ((AdministrationArea) refObject).getName();
		if (selectionList.size() == 1) {
			confirmDialogMsg = messages.deleteAARelationConfirmDialogMsgPart1 + " \'"
					+ adminAreaName + "\' " + messages.deleteRoleRelationMsg + " \'"
					+ roleName + "' ?";
		} else {
			confirmDialogMsg = messages.removeMultiObjectsRelConfirmDialogMsg + " " + messages.deleteRoleRelationMsg
					+ " \'" + roleName + "' ?";
		}

		RoleAdminAreaRelController roleAdminAreaRelController = new RoleAdminAreaRelController();
		boolean isConfirmed = CustomMessageDialog.openConfirm(adminTree.getControl().getShell(), confirmDialogTitle,
				confirmDialogMsg);

		if (isConfirmed) {
			Job job = new Job("Removing Relations...") {
				@Override
				protected IStatus run(IProgressMonitor monitor) {
					monitor.beginTask("Removing Relations..", 100);
					monitor.worked(30);
					try {
						RoleAdminAreaRelResponse roleAdminAreaRelResponse;
						roleAdminAreaRelResponse = roleAdminAreaRelController
								.deleteMultiRoleAdminAreaRel(relationIdList);
						if (roleAdminAreaRelResponse != null) {
							List<Map<String, String>> statusMaps = roleAdminAreaRelResponse.getStatusMap();
							if (!statusMaps.isEmpty()) {
								for (Map<String, String> statusMap : statusMaps) {
									String string = statusMap.get("en");
									String[] split = string.split("id");
									relationIdList.remove(split[1].trim());
									failObjectNames.put(split[1].trim(), successObjectNames.get(split[1].trim()));
									successObjectNames.remove(split[1].trim());
								}
								XMAdminUtil.getInstance().updateLogFile(
										messages.deleteMultiObjectsDialogErrMsg, MessageType.FAILURE);
								Display.getDefault().asyncExec(new Runnable() {
									public void run() {
										CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
												messages.deleteConfirmDialogTitle,
												messages.deleteMultiObjectsDialogErrMsg);
									}
								});
							}

							for (int i = 0; i < selectionList.size(); i++) {
								Object selectionObj = selectionList.get(i);
								RelationObj relationObj = (RelationObj) selectionObj;
								if (relationIdList.contains(relationObj.getRelId())) {
									IAdminTreeChild refObject = relationObj.getRefObject();
									RoleScopeObjects roleScopeObjects = (RoleScopeObjects) relationObj.getParent();
									roleScopeObjects.remove(relationObj.getRelId());
									((AdministrationArea) refObject).setRelationId(null);
									((AdministrationArea) refObject).setRelatedSiteName(null);
								}
							}
							Display.getDefault().asyncExec(new Runnable() {
								public void run() {
									adminTree.refresh(((RelationObj) (selection.getFirstElement())).getParent());
									adminTree.refresh(true);
									if (isAdminTreeObject) {
										XMAdminUtil.getInstance().getAdminTree().setSelection(new StructuredSelection(((IAdminTreeChild)selection.getFirstElement()).getParent()));
									}
									objectExpPage.refreshExplorer();
								}
							});
							if (successObjectNames.size() > 0) {
								Iterator it = successObjectNames.entrySet().iterator();
								while (it.hasNext()) {
									Map.Entry map = (Map.Entry) it.next();
									XMAdminUtil.getInstance().updateLogFile(messages.relationLbl + " "
											+ messages.betweenLbl + " " + messages.administrationAreaObject + " '"
											+ map.getValue() + "' " + messages.andLbl + " " + messages.roleObject + " '"
											+ roleName + "' " + messages.objectDelete, MessageType.SUCCESS);
								}
							}
							if (failObjectNames.size() > 0) {
								Iterator it = failObjectNames.entrySet().iterator();
								while (it.hasNext()) {
									Map.Entry map = (Map.Entry) it.next();
									XMAdminUtil.getInstance()
											.updateLogFile(messages.deleteErrorMsg + " "
													+ messages.administrationAreaObject + " '" + map.getValue() + "' "
													+ messages.relationLbl + " " + messages.withLbl + " "
													+ messages.roleObject + " '" + roleName + "' ",
													MessageType.FAILURE);
								}
							}
						}
					} catch (UnauthorizedAccessException e) {

						Display.getDefault().asyncExec(new Runnable() {
							public void run() {
								CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
										messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
							}
						});

					}
					monitor.worked(70);
					return Status.OK_STATUS;
				}
			};
			job.setUser(true);
			job.schedule();
		}
	}

	/**
	 * Delete user group user rel.
	 *
	 * @param selection
	 *            the selection
	 * @param objectExpPage
	 *            the object exp page
	 */
	@SuppressWarnings("rawtypes")
	private void deleteUserGroupUserRel(final IStructuredSelection selection, final ObjectExpPage objectExpPage) {
		Object selectionObj = selection.getFirstElement();
		RelationObj relationObj = (RelationObj) selectionObj;
		final IAdminTreeChild containerObj = relationObj.getContainerObj();
		try {
			if (!XMAdminUtil.getInstance().isRelObjAccessAllowed(containerObj,
					"GROUP_RELATION-REMOVE")) {
				CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
						messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
				return;
			}
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				CustomMessageDialog.openError(Display.getCurrent().getActiveShell(), messages.errorDialogTitile,
						messages.serverNotReachable);
			}
			LOGGER.error("Exception occured in calling access allowed API " + e);
			return;
		}
		final XMAdminUtil instance = XMAdminUtil.getInstance();
		final AdminTreeviewer adminTree = instance.getAdminTree();
		final String confirmDialogTitle = messages.deleteConfirmDialogTitle;
		String confirmDialogMsg = null;
		final List selectionList = selection.toList();
		final Set<String> relationIdList = new HashSet<>();
		final Map<String,String> successObjectNames = new HashMap<>();
		for (int i = 0; i < selectionList.size(); i++) {
			Object seleObj = selectionList.get(i);
			RelationObj relObj = (RelationObj) seleObj;
			relationIdList.add(relObj.getRelId());
			successObjectNames.put(relObj.getRelId(), ((User)relObj.getRefObject()).getName());
		}
		IAdminTreeChild refObject = relationObj.getRefObject();
		String userName = ((User) refObject).getName();
		String userGrpName = ((UserGroupModel) relationObj.getContainerObj().getParent().getParent()).getName();
		if (selectionList.size() == 1) {
			confirmDialogMsg = messages.deleteUserConfirmDialogMsgPart1 + " \'" + userName + "\' "
					+ messages.deleteUserGroupRelationMsg + " \'"
					+ userGrpName + "' ?";
		} else {
			confirmDialogMsg = messages.removeMultiObjectsRelConfirmDialogMsg + " "
					+ messages.deleteUserGroupRelationMsg + " \'"
					+ userGrpName + "' ?";
		}
		GroupRelController groupRelController = new GroupRelController();
		boolean isConfirmed = CustomMessageDialog.openConfirm(adminTree.getControl().getShell(), confirmDialogTitle,
				confirmDialogMsg);

		if (isConfirmed) {
			Job job = new Job("Removing Relations...") {
				@Override
				protected IStatus run(IProgressMonitor monitor) {
					monitor.beginTask("Removing Relations..", 100);
					monitor.worked(30);
					try {
						boolean isRelationDeleted;
						isRelationDeleted = groupRelController.deleteMultiGroupRel(relationIdList);
						if (isRelationDeleted) {
							Display.getDefault().asyncExec(new Runnable() {
								public void run() {
									objectExpPage.refreshExplorer();
								}
							});
							if (successObjectNames.size() > 0) {
								Iterator it = successObjectNames.entrySet().iterator();
								while (it.hasNext()) {
									Map.Entry map = (Map.Entry) it.next();
									XMAdminUtil.getInstance().updateLogFile(messages.relationLbl + " "
											+ messages.betweenLbl + " " + messages.userObject + " '" + map.getValue()
											+ "' " + messages.andLbl + " " + messages.userGroupObject + " '"
											+ userGrpName + "' " + messages.objectDelete, MessageType.SUCCESS);
								}
							}
						}
					} catch (UnauthorizedAccessException e) {

						Display.getDefault().asyncExec(new Runnable() {
							public void run() {
								CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
										messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
							}
						});

					}
					monitor.worked(70);
					return Status.OK_STATUS;
				}
			};
			job.setUser(true);
			job.schedule();
		}
	}

	/**
	 * Gets the container object.
	 *
	 * @param relationObj
	 *            the relation obj
	 * @return the container object
	 */
	private IAdminTreeChild getContainerObject(final RelationObj relationObj) {
		IAdminTreeChild iAdminTreeChild = null;
		if (relationObj.getContainerObj() instanceof SiteAdminAreaUserAppProtected
				|| relationObj.getContainerObj() instanceof SiteAdminAreaUserAppFixed
				|| relationObj.getContainerObj() instanceof SiteAdminAreaUserAppNotFixed) {
			iAdminTreeChild = (SiteAdminAreaUserApplications) (relationObj.getContainerObj()).getParent().getParent();
		} else if (relationObj.getContainerObj() instanceof AdminAreaUserAppProtected
				|| relationObj.getContainerObj() instanceof AdminAreaUserAppFixed
				|| relationObj.getContainerObj() instanceof AdminAreaUserAppNotFixed) {
			iAdminTreeChild = (AdminAreaUserApplications) (relationObj.getContainerObj()).getParent().getParent();
		} else if (relationObj.getContainerObj() instanceof UserAAUserAppAllowed
				|| relationObj.getContainerObj() instanceof UserAAUserAppForbidden) {
			iAdminTreeChild = (UserAAUserApplications) (relationObj.getContainerObj()).getParent().getParent();
		}
		if (relationObj.getContainerObj() instanceof SiteAdminProjectAppProtected
				|| relationObj.getContainerObj() instanceof SiteAdminProjectAppFixed
				|| relationObj.getContainerObj() instanceof SiteAdminProjectAppNotFixed) {
			iAdminTreeChild = (SiteAdminAreaProjectApplications) (relationObj.getContainerObj()).getParent()
					.getParent();
		} else if (relationObj.getContainerObj() instanceof AdminAreaProjectAppProtected
				|| relationObj.getContainerObj() instanceof AdminAreaProjectAppFixed
				|| relationObj.getContainerObj() instanceof AdminAreaProjectAppNotFixed) {
			iAdminTreeChild = (AdminAreaProjectApplications) (relationObj.getContainerObj()).getParent().getParent();
		} else if (relationObj.getContainerObj() instanceof UserProjectAAProjectAppAllowed
				|| relationObj.getContainerObj() instanceof UserProjectAAProjectAppForbidden) {
			iAdminTreeChild = (UserProjectAAProjectApplications) (((RelationObj) relationObj).getContainerObj())
					.getParent().getParent();
		} else if (relationObj.getContainerObj() instanceof ProjectAdminAreaProjectAppNotFixed
				|| relationObj.getContainerObj() instanceof ProjectAdminAreaProjectAppFixed
				|| relationObj.getContainerObj() instanceof ProjectAdminAreaProjectAppProtected) {
			iAdminTreeChild = (ProjectAdminAreaProjectApplications) relationObj.getContainerObj().getParent()
					.getParent();
		} else if (relationObj.getContainerObj() instanceof ProjectUserAAProjectAppAllowed
				|| relationObj.getContainerObj() instanceof ProjectUserAAProjectAppForbidden) {
			iAdminTreeChild = (ProjectUserAAProjectApplications) relationObj.getContainerObj().getParent().getParent();
		}

		return iAdminTreeChild;
	}

	/**
	 * Can execute.
	 *
	 * @param part
	 *            the part
	 * @param application
	 *            the application
	 * @param service
	 *            the service
	 * @return true, if successful
	 */
	@CanExecute
	public boolean canExecute(@Active MPart part, MApplication application, EModelService service) {
		boolean returnValue = true;
		boolean checkValue = true;
		//if (part.getElementId().equals(CommonConstants.PART_ID.INFORMATION_PART_ID)) {
			XMAdminUtil instance = XMAdminUtil.getInstance();
			MPart mPart = instance.getInformationPart();
			Object selectionObj = null;
			/*InformationPart rightSidePart = (InformationPart) mPart.getObject();
			ObjectExpPage objectExpPage = rightSidePart.getObjectExpPartUI();
			ITreeSelection selection;
			if (objectExpPage != null
					&& (selection = objectExpPage.getObjExpTableViewer().getStructuredSelection()) != null
					&& selection.getFirstElement() != null) {*/
			InformationPart rightSidePart2 = (InformationPart) mPart.getObject();
			ObjectExpPage objectExpPage = rightSidePart2.getObjectExpPartUI();
			final ObjectExplorer objExpTableViewer = objectExpPage.getObjExpTableViewer();
			IStructuredSelection selections = (IStructuredSelection) objExpTableViewer.getSelection();
			selectionObj = selections.getFirstElement();
			if(selectionObj == null){
				selections = (IStructuredSelection) XMAdminUtil.getInstance().getAdminTree().getSelection();
				selectionObj = selections.getFirstElement();
			}
			if (objectExpPage != null && selections != null && selectionObj != null) {
				checkValue = true;
			} else {
				checkValue = false;
			}
			if (returnValue) {
				List<MToolItem> mToolItems = service.findElements(application,
						"com.magna.xmsystem.xmadmin.ui.handledtoolitem.delete", MToolItem.class, null); //$NON-NLS-1$
				if (mToolItems.size() > 0) {
					mToolItems.get(0).setEnabled(checkValue);
				}
				return checkValue;
			}
			return false;
		}
		//return false;
	//}
}
