package com.magna.xmsystem.xmadmin.ui.dialogs;

import java.io.File;

import org.eclipse.jface.viewers.ColumnLabelProvider;

/**
 * The Class BrowseScriptDialogTreeScriptLabelProvider.
 * 
 * @author Archita.patel
 */
public class BrowseScriptDialogTreeScriptLabelProvider extends ColumnLabelProvider {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.viewers.ColumnLabelProvider#getText(java.lang.Object)
	 */
	@Override
	public String getText(Object element) {
		if (element instanceof File) {
			String fileName = ((File) element).getName();
			return ((String) fileName);
		}
		return super.getText(element);
	}
}
