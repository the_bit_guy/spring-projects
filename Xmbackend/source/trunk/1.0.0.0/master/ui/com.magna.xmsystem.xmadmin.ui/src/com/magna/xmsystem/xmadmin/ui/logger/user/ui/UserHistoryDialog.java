package com.magna.xmsystem.xmadmin.ui.logger.user.ui;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;

import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.IJobManager;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.core.runtime.preferences.IEclipsePreferences;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.resource.ResourceManager;
import org.eclipse.nebula.widgets.nattable.NatTable;
import org.eclipse.nebula.widgets.nattable.export.command.ExportCommand;
import org.eclipse.nebula.widgets.pgroup.PGroup;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.custom.StackLayout;
import org.eclipse.swt.events.ControlAdapter;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.events.ExpandEvent;
import org.eclipse.swt.events.ExpandListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.VerifyEvent;
import org.eclipse.swt.events.VerifyListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;
import org.osgi.service.prefs.BackingStoreException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.ResourceAccessException;

import com.magna.xmbackend.vo.userHistory.UserHistoryRequest;
import com.magna.xmsystem.dependencies.utils.LANG_ENUM;
import com.magna.xmsystem.dependencies.utils.XMSystemUtil;
import com.magna.xmsystem.ui.controls.widgets.CustomDateTime;
import com.magna.xmsystem.ui.controls.widgets.CustomMessageDialog;
import com.magna.xmsystem.xmadmin.message.Message;
import com.magna.xmsystem.xmadmin.message.MessageRegistry;
import com.magna.xmsystem.xmadmin.ui.logger.HistoryTableUtil;
import com.magna.xmsystem.xmadmin.util.CommonConstants;
import com.magna.xmsystem.xmadmin.util.XMAdminUtil;

/**
 * Class for User history dialog.
 *
 * @author Chiranjeevi.Akula
 */
public class UserHistoryDialog extends Dialog {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(UserHistoryDialog.class);

	/** Member variable 'user history page' for {@link Composite}. */
	private Composite userHistoryPage;

	/** Member variable 'main data group' for {@link Group}. */
	private Group mainDataGroup;

	/** Member variable 'user status page' for {@link Composite}. */
	private Composite userStatusPage;

	/** Member variable 'prefs' for {@link IEclipsePreferences}. */
	private IEclipsePreferences prefs;

	/** Member variable 'user status nat table' for {@link NatTable}. */
	private NatTable userStatusNatTable;

	/** Member variable 'user history nat table' for {@link NatTable}. */
	private NatTable userHistoryNatTable;

	/**
	 * Member variable 'user history table container' for
	 * {@link UserHistoryTblNattable}.
	 */
	private UserHistoryTblNattable userHistoryTableContainer;

	/** Member variable 'messages' for {@link Message}. */
	private Message messages;
	
	/** The registry. */
	private MessageRegistry registry;

	/**
	 * Member variable 'user status table container' for
	 * {@link UserHistoryTblNattable}.
	 */
	private UserHistoryTblNattable userStatusTableContainer;

	/** Member variable 'refresh tool item' for {@link ToolItem}. */
	private ToolItem refreshToolItem;

	/** Member variable 'excel export tool item' for {@link ToolItem}. */
	private ToolItem excelExportToolItem;

	/** Member variable 'user histroy start date' for {@link CDateTime}. */
	private CustomDateTime userHistroyStartDate;

	/** Member variable 'user histroy end date' for {@link CDateTime}. */
	private CustomDateTime userHistroyEndDate;

	/** Member variable 'user status start date' for {@link CDateTime}. */
	private CustomDateTime userStatusStartDate;

	/** Member variable 'user status end date' for {@link CDateTime}. */
	private CustomDateTime userStatusEndDate;

	/** Member variable 'parent shell' for {@link Shell}. */
	private Shell parentShell;

	/** Member variable 'user history apply button' for {@link Button}. */
	private Button userHistoryApplyButton;

	/** Member variable 'user status apply button' for {@link Button}. */
	private Button userStatusApplyButton;

	/**
	 * Constructor for UserHistoryDialog Class.
	 *
	 * @param parentShell
	 *            {@link Shell}
	 * @param prefs
	 *            {@link IEclipsePreferences}
	 * @param registry 
	 * @param messages
	 *            {@link Message}
	 */
	public UserHistoryDialog(final Shell parentShell, final IEclipsePreferences prefs, MessageRegistry registry, final Message messages) {
		super(parentShell);
		this.parentShell = parentShell;
		this.prefs = prefs;
		this.messages = messages;
		this.registry = registry;
		setShellStyle(SWT.CLOSE | SWT.TITLE | SWT.MODELESS | SWT.SHELL_TRIM | SWT.RESIZE);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.window.Window#configureShell(org.eclipse.swt.widgets.
	 * Shell)
	 */
	@Override
	protected void configureShell(final Shell newShell) {
		//newShell.setText(this.messages.userHistoryDialogTitle);
		registry.register((text) -> {
			if (newShell != null && !newShell.isDisposed()) {
				newShell.setText(text);
			}
		}, (message) -> {
			if (newShell != null && !newShell.isDisposed()) {
				return message.userHistoryDialogTitle;
			}
			return CommonConstants.EMPTY_STR;
		});
		Image image = parentShell.getImage();
		if (image != null) {
			newShell.setImage(image);
		}
		super.configureShell(newShell);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.dialogs.Dialog#createDialogArea(org.eclipse.swt.widgets
	 * .Composite)
	 */
	@Override
	protected Control createDialogArea(final Composite parent) {
		Composite container = (Composite) super.createDialogArea(parent);
		container.setLayout(new GridLayout());

		final StackLayout stackLayout = new StackLayout();

		final Composite headerComposite = new Composite(container, SWT.NONE);
		final GridLayout headerGridLayout = new GridLayout(3, false);
		headerGridLayout.marginLeft = 10;
		headerGridLayout.marginRight = 10;
		headerGridLayout.marginTop = 0;
		headerGridLayout.marginBottom = 0;
		headerGridLayout.marginHeight = 0;
		headerGridLayout.marginWidth = 0;

		headerComposite.setLayout(headerGridLayout);
		headerComposite.setLayoutData(new GridData(GridData.FILL, GridData.CENTER, true, false));

		Button btnUserHistory = new Button(headerComposite, SWT.RADIO);
		//btnUserHistory.setText(messages.userHistoryBtn);
		registry.register((text) -> {
			if (btnUserHistory != null && !btnUserHistory.isDisposed()) {
				btnUserHistory.setText(text);
			}
		}, (message) -> {
			if (btnUserHistory != null && !btnUserHistory.isDisposed()) {
				return getUpdatedWidgetText(message.userHistoryBtn, btnUserHistory);
			}
			return CommonConstants.EMPTY_STR;
		});
		btnUserHistory.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				stackLayout.topControl = userHistoryPage;
				mainDataGroup.layout();
			}
		});
		btnUserHistory.setData("org.eclipse.e4.ui.css.id", "HistoryRadioButton");
		btnUserHistory.setSelection(true);
		
		Button btnUserStatus = new Button(headerComposite, SWT.RADIO);
		//btnUserStatus.setText(messages.userStatusBtn);
		registry.register((text) -> {
			if (btnUserStatus != null && !btnUserStatus.isDisposed()) {
				btnUserStatus.setText(text);
			}
		}, (message) -> {
			if (btnUserStatus != null && !btnUserStatus.isDisposed()) {
				return getUpdatedWidgetText(message.userStatusBtn, btnUserStatus);
			}
			return CommonConstants.EMPTY_STR;
		});
		btnUserStatus.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				stackLayout.topControl = userStatusPage;
				mainDataGroup.layout();
			}
		});
		btnUserStatus.setData("org.eclipse.e4.ui.css.id", "HistoryRadioButton");

		final ToolBar toolbar = new ToolBar(headerComposite, SWT.FLAT);
		toolbar.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, true, false, 1, 1));

		this.refreshToolItem = new ToolItem(toolbar, SWT.PUSH);
		this.refreshToolItem.setImage(XMSystemUtil.getInstance().getImage(this.getClass(), "icons/32x32/refresh_32.png"));
		//this.refreshToolItem.setToolTipText(messages.historyReload);
		registry.register((text) -> {
			if (refreshToolItem != null && !refreshToolItem.isDisposed()) {
				refreshToolItem.setToolTipText(text);
			}
		}, (message) -> {
			if (refreshToolItem != null && !refreshToolItem.isDisposed()) {
				return message.historyReload;
			}
			return CommonConstants.EMPTY_STR;
		});
		this.refreshToolItem.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {

				if (stackLayout.topControl == userHistoryPage) {
					userHistoryApplyButton.notifyListeners(SWT.Selection, new Event());
				} else {
					userStatusApplyButton.notifyListeners(SWT.Selection, new Event());
				}
			}
		});

		this.excelExportToolItem = new ToolItem(toolbar, SWT.PUSH);
		this.excelExportToolItem.setImage(XMSystemUtil.getInstance().getImage(this.getClass(), "icons/32x32/excel_icon.png"));
		//this.excelExportToolItem.setToolTipText(messages.historyExportBtn);
		registry.register((text) -> {
			if (excelExportToolItem != null && !excelExportToolItem.isDisposed()) {
				excelExportToolItem.setToolTipText(text);
			}
		}, (message) -> {
			if (excelExportToolItem != null && !excelExportToolItem.isDisposed()) {
				return message.historyExportBtn;
			}
			return CommonConstants.EMPTY_STR;
		});
		this.excelExportToolItem.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {

				if (stackLayout.topControl == userHistoryPage) {
					userHistoryNatTable.doCommand(
							new ExportCommand(userHistoryNatTable.getConfigRegistry(), userHistoryNatTable.getShell()));
				} else {
					userStatusNatTable.doCommand(
							new ExportCommand(userStatusNatTable.getConfigRegistry(), userStatusNatTable.getShell()));
				}
			}
		});

		mainDataGroup = new Group(container, SWT.NONE);
		mainDataGroup.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 3, 1));
		mainDataGroup.setLayout(stackLayout);

		try {
			generateUserHistoryTable();
			generateUserStatusTable();
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				Display.getDefault().asyncExec(new Runnable() {
					@Override
					public void run() {
						CustomMessageDialog.openError(Display.getDefault().getActiveShell(), "Error",
								"Server not reachable");
						close();
					}
				});
			}
			LOGGER.error("Exception occured while lodaing userHistory", e.getMessage());
		}

		stackLayout.topControl = userHistoryPage;
		mainDataGroup.layout();
		
		registry.register((text) -> {
		}, (message) -> {
			updateNatTableColumnLabels();
			return CommonConstants.EMPTY_STR;
		});
		

		return container;
	}

	/**
	 * Method for Creates the user history filter composite.
	 *
	 * @param parent
	 *            {@link Composite}
	 */
	private void createUserHistoryFilterComposite(final Composite parent) {

		final PGroup pGroup = new PGroup(parent, SWT.SMOOTH);

		pGroup.addExpandListener(new PGroupExpandListener());

		final GridLayout widgetContLayout = new GridLayout(6, false);
		widgetContLayout.horizontalSpacing = 10;
		pGroup.setLayout(widgetContLayout);
		pGroup.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 3, 1));
		//pGroup.setText(messages.userHistoryFilterLabel);
		registry.register((text) -> {
			if (pGroup != null && !pGroup.isDisposed()) {
				pGroup.setText(text);
			}
		}, (message) -> {
			if (pGroup != null && !pGroup.isDisposed()) {
				return getUpdatedWidgetText(message.userHistoryFilterLabel, pGroup);
			}
			return CommonConstants.EMPTY_STR;
		});

		Label lblLogTime = new Label(pGroup, SWT.NONE);
		//lblLogTime.setText(messages.historyLogTimeLabel);
		registry.register((text) -> {
			if (lblLogTime != null && !lblLogTime.isDisposed()) {
				lblLogTime.setText(text);
			}
		}, (message) -> {
			if (lblLogTime != null && !lblLogTime.isDisposed()) {
				return getUpdatedWidgetText(message.historyLogTimeLabel, lblLogTime);
			}
			return CommonConstants.EMPTY_STR;
		});

		lblLogTime.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		new Label(pGroup, SWT.NONE);

		Label lblCdate = new Label(pGroup, SWT.NONE);
		lblCdate.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false));
		//lblCdate.setText(messages.historyStartDateLabel);
		registry.register((text) -> {
			if (lblCdate != null && !lblCdate.isDisposed()) {
				lblCdate.setText(text);
			}
		}, (message) -> {
			if (lblCdate != null && !lblCdate.isDisposed()) {
				return getUpdatedWidgetText(message.historyStartDateLabel, lblCdate);
			}
			return CommonConstants.EMPTY_STR;
		});
		userHistroyStartDate = new CustomDateTime(pGroup);
		userHistroyStartDate.setLayoutData(new GridData(SWT.FILL, SWT.LEFT, true, false));
		String stDate = prefs.get("userHistory.startDate", "");
		if (!stDate.isEmpty()) {
			try {
				userHistroyStartDate.setDate(XMSystemUtil.getHistoryLogTimeDateFormat().parse(stDate));
			} catch (ParseException e1) {
				LOGGER.error("Exception while parsing the date!" + e1);
			}
		}
		
		registry.register((text) -> {
		}, (message) -> {
			if(userHistroyStartDate != null && !userHistroyStartDate.isDisposed()){
			LANG_ENUM currentLocaleEnum = XMAdminUtil.getInstance().getCurrentLocaleEnum();
			userHistroyStartDate.updateTooltipText(currentLocaleEnum.toString());
			}
			return CommonConstants.EMPTY_STR;
		});
		
		Label lblEndDate = new Label(pGroup, SWT.NONE);
		lblEndDate.setLayoutData(new GridData(SWT.BEGINNING, SWT.CENTER, false, false));
		//lblEndDate.setText(messages.historyEndDateLabel);
		registry.register((text) -> {
			if (lblEndDate != null && !lblEndDate.isDisposed()) {
				lblEndDate.setText(text);
			}
		}, (message) -> {
			if (lblEndDate != null && !lblEndDate.isDisposed()) {
				return getUpdatedWidgetText(message.historyEndDateLabel, lblEndDate);
			}
			return CommonConstants.EMPTY_STR;
		});
		userHistroyEndDate = new CustomDateTime(pGroup);
		userHistroyEndDate.setLayoutData(new GridData(SWT.FILL, SWT.LEFT, true, false));
		String eDate = prefs.get("userHistory.endDate", "");
		if (!eDate.isEmpty()) {
			try {
				userHistroyEndDate.setDate(XMSystemUtil.getHistoryLogTimeDateFormat().parse(eDate));
			} catch (ParseException e1) {
				LOGGER.error("Exception while parsing the date!" + e1);
			}
		}
		
		registry.register((text) -> {
		}, (message) -> {
			if(userHistroyEndDate != null && !userHistroyEndDate.isDisposed()){
			LANG_ENUM currentLocaleEnum = XMAdminUtil.getInstance().getCurrentLocaleEnum();
			userHistroyEndDate.updateTooltipText(currentLocaleEnum.toString());
			}
			return CommonConstants.EMPTY_STR;
		});
		

		Label lblUserName = new Label(pGroup, SWT.NONE);
		lblUserName.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		//lblUserName.setText(messages.userHistoryUserNameLbl);
		registry.register((text) -> {
			if (lblUserName != null && !lblUserName.isDisposed()) {
				lblUserName.setText(text);
			}
		}, (message) -> {
			if (lblUserName != null && !lblUserName.isDisposed()) {
				return getUpdatedWidgetText(message.userHistoryUserNameLbl, lblUserName);
			}
			return CommonConstants.EMPTY_STR;
		});
		lblUserName.setFocus();

		Combo cmbUser = new Combo(pGroup, SWT.NONE);
		cmbUser.setItems(CommonConstants.History.QUERYTYPE);
		GridData gd_cmbUser = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gd_cmbUser.widthHint = 34;
		cmbUser.setLayoutData(gd_cmbUser);

		Text txtUsernameHistoryfiltertext = new Text(pGroup, SWT.BORDER);
		txtUsernameHistoryfiltertext.setText("");
		txtUsernameHistoryfiltertext.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 4, 1));

		String userName = prefs.get("userHistoryTable.userName", "");
		if (!userName.isEmpty()) {
			String[] values = userName.split(",");
			cmbUser.setText(values[0]);
			txtUsernameHistoryfiltertext.setText(values[1]);
		}

		Label lblHost = new Label(pGroup, SWT.NONE);
		lblHost.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		//lblHost.setText(messages.userHistoryHostLbl);
		registry.register((text) -> {
			if (lblHost != null && !lblHost.isDisposed()) {
				lblHost.setText(text);
			}
		}, (message) -> {
			if (lblHost != null && !lblHost.isDisposed()) {
				return getUpdatedWidgetText(message.userHistoryHostLbl, lblHost);
			}
			return CommonConstants.EMPTY_STR;
		});

		Combo cmbHost = new Combo(pGroup, SWT.NONE);
		cmbHost.setItems(CommonConstants.History.QUERYTYPE);
		cmbHost.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));

		Text txtHostUserHistoryfilter = new Text(pGroup, SWT.BORDER);
		txtHostUserHistoryfilter.setText("");
		txtHostUserHistoryfilter.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 4, 1));

		String host = prefs.get("userHistoryTable.host", "");
		if (!host.isEmpty()) {
			String[] values = host.split(",");
			cmbHost.setText(values[0]);
			txtHostUserHistoryfilter.setText(values[1]);
		}

		Label lblNewLabel_1 = new Label(pGroup, SWT.NONE);
		lblNewLabel_1.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		//lblNewLabel_1.setText(messages.userHistorySiteLbl);
		registry.register((text) -> {
			if (lblNewLabel_1 != null && !lblNewLabel_1.isDisposed()) {
				lblNewLabel_1.setText(text);
			}
		}, (message) -> {
			if (lblNewLabel_1 != null && !lblNewLabel_1.isDisposed()) {
				return getUpdatedWidgetText(message.userHistorySiteLbl, lblNewLabel_1);
			}
			return CommonConstants.EMPTY_STR;
		});

		Combo cmbSite = new Combo(pGroup, SWT.NONE);
		cmbSite.setItems(CommonConstants.History.QUERYTYPE);
		cmbSite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));

		Text txtSiteUserHistoryfilter = new Text(pGroup, SWT.BORDER);
		txtSiteUserHistoryfilter.setText("");
		txtSiteUserHistoryfilter.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 4, 1));

		String site = prefs.get("userHistoryTable.site", "");
		if (!site.isEmpty()) {
			String[] values = site.split(",");
			cmbSite.setText(values[0]);
			txtSiteUserHistoryfilter.setText(values[1]);
		}

		Label lblAdminarea = new Label(pGroup, SWT.NONE);
		lblAdminarea.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		//lblAdminarea.setText(messages.userHistoryAdminAreaLbl);
		registry.register((text) -> {
			if (lblAdminarea != null && !lblAdminarea.isDisposed()) {
				lblAdminarea.setText(text);
			}
		}, (message) -> {
			if (lblAdminarea != null && !lblAdminarea.isDisposed()) {
				return getUpdatedWidgetText(message.userHistoryAdminAreaLbl, lblAdminarea);
			}
			return CommonConstants.EMPTY_STR;
		});

		Combo cmbAdminArea = new Combo(pGroup, SWT.NONE);
		cmbAdminArea.setItems(CommonConstants.History.QUERYTYPE);
		cmbAdminArea.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));

		Text txtAdminareafilter = new Text(pGroup, SWT.BORDER);
		txtAdminareafilter.setText("");
		txtAdminareafilter.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 4, 1));
		String adminArea = prefs.get("userHistoryTable.adminArea", "");

		if (!adminArea.isEmpty()) {
			String[] values = adminArea.split(",");
			cmbAdminArea.setText(values[0]);
			txtAdminareafilter.setText(values[1]);
		}

		Label lblProject = new Label(pGroup, SWT.NONE);
		lblProject.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		//lblProject.setText(messages.userHistoryProjectLbl);
		registry.register((text) -> {
			if (lblProject != null && !lblProject.isDisposed()) {
				lblProject.setText(text);
			}
		}, (message) -> {
			if (lblProject != null && !lblProject.isDisposed()) {
				return getUpdatedWidgetText(message.userHistoryProjectLbl, lblProject);
			}
			return CommonConstants.EMPTY_STR;
		});


		Combo cmbProject = new Combo(pGroup, SWT.NONE);
		cmbProject.setItems(CommonConstants.History.QUERYTYPE);
		cmbProject.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));

		Text txtProjectfilter = new Text(pGroup, SWT.BORDER);
		txtProjectfilter.setText("");
		txtProjectfilter.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 4, 1));

		String project = prefs.get("userHistoryTable.projet", "");
		if (!project.isEmpty()) {
			String[] values = project.split(",");
			cmbProject.setText(values[0]);
			txtProjectfilter.setText(values[1]);
		}

		Label lblApplication = new Label(pGroup, SWT.NONE);
		lblApplication.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
	//	lblApplication.setText(messages.userHistoryApplicationLbl);
		registry.register((text) -> {
			if (lblApplication != null && !lblApplication.isDisposed()) {
				lblApplication.setText(text);
			}
		}, (message) -> {
			if (lblApplication != null && !lblApplication.isDisposed()) {
				return getUpdatedWidgetText(message.userHistoryApplicationLbl, lblApplication);
			}
			return CommonConstants.EMPTY_STR;
		});

		Combo cmbApplication = new Combo(pGroup, SWT.NONE);
		cmbApplication.setItems(CommonConstants.History.QUERYTYPE);
		cmbApplication.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));

		Text txtApplicationfilter = new Text(pGroup, SWT.BORDER);
		txtApplicationfilter.setText("");
		txtApplicationfilter.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 4, 1));

		String application = prefs.get("userHistoryTable.application", "");
		if (!application.isEmpty()) {
			String[] values = application.split(",");
			cmbApplication.setText(values[0]);
			txtApplicationfilter.setText(values[1]);
		}

		Label lblResult = new Label(pGroup, SWT.NONE);
		lblResult.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		//lblResult.setText(messages.userHistoryResultLbl);
		registry.register((text) -> {
			if (lblResult != null && !lblResult.isDisposed()) {
				lblResult.setText(text);
			}
		}, (message) -> {
			if (lblResult != null && !lblResult.isDisposed()) {
				return getUpdatedWidgetText(message.userHistoryResultLbl, lblResult);
			}
			return CommonConstants.EMPTY_STR;
		});

		Combo cmbResult = new Combo(pGroup, SWT.NONE);
		cmbResult.setItems(CommonConstants.History.QUERYTYPE);
		cmbResult.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		
		Text txtResultfilter = new Text(pGroup, SWT.BORDER);
		txtResultfilter.setText("");
		txtResultfilter.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 4, 1));

		String result = prefs.get("userHistoryTable.result", "");
		if (!result.isEmpty()) {
			String[] values = result.split(",");
			cmbResult.setText(values[0]);
			txtResultfilter.setText(values[1]);
		}

		Label lblLimit = new Label(pGroup, SWT.NONE);
		lblLimit.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		//lblLimit.setText(messages.historyLimitLbl);
		registry.register((text) -> {
			if (lblLimit != null && !lblLimit.isDisposed()) {
				lblLimit.setText(text);
			}
		}, (message) -> {
			if (lblLimit != null && !lblLimit.isDisposed()) {
				return getUpdatedWidgetText(message.historyLimitLbl, lblLimit);
			}
			return CommonConstants.EMPTY_STR;
		});

		Text limitFilterText = new Text(pGroup, SWT.BORDER);
		String limit = prefs.get("userHistoryTable.limit", CommonConstants.History.USER_HISTORY_ROW_LIMIT);
		limitFilterText.setText(limit);
		limitFilterText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 5, 1));
		limitFilterText.addVerifyListener(new VerifyListener() {

			@Override
			public void verifyText(VerifyEvent e) {
				String string = e.text;
				char[] chars = new char[string.length()];
				string.getChars(0, chars.length, chars, 0);
				for (int i = 0; i < chars.length; i++) {
					if (!('a' <= chars[i] && chars[i] <= 'z')) {
						e.doit = true;

					} else {
						e.doit = false;
					}
				}
			}
		});

		Composite buttonBarComposite = new Composite(pGroup, SWT.NONE);
		buttonBarComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 6, 1));
		buttonBarComposite.setLayout(new GridLayout(2, false));

		Button defaultButton = new Button(buttonBarComposite, SWT.PUSH);
		defaultButton.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, true, false, 1, 1));
		//defaultButton.setText(messages.historyDefaultBtn);
		registry.register((text) -> {
			if (defaultButton != null && !defaultButton.isDisposed()) {
				defaultButton.setText(text);
			}
		}, (message) -> {
			if (defaultButton != null && !defaultButton.isDisposed()) {
				return getUpdatedWidgetText(message.historyDefaultBtn, defaultButton);
			}
			return CommonConstants.EMPTY_STR;
		});
		defaultButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				prefs.put("userHistory.startDate", "");
				userHistroyStartDate.setDate(null);
				prefs.put("userHistory.endDate", "");
				userHistroyEndDate.setDate(null);
				prefs.put("userHistoryTable.userName", "");
				cmbUser.setText("");
				txtUsernameHistoryfiltertext.setText("");
				prefs.put("userHistoryTable.host", "");
				cmbHost.setText("");
				txtHostUserHistoryfilter.setText("");
				prefs.put("userHistoryTable.site", "");
				cmbSite.setText("");
				txtSiteUserHistoryfilter.setText("");
				prefs.put("userHistoryTable.adminArea", "");
				cmbAdminArea.setText("");
				txtAdminareafilter.setText("");
				prefs.put("userHistoryTable.projet", "");
				cmbProject.setText("");
				txtProjectfilter.setText("");
				prefs.put("userHistoryTable.application", "");
				cmbApplication.setText("");
				txtApplicationfilter.setText("");
				prefs.put("userHistoryTable.result", "");
				cmbResult.setText("");
				txtResultfilter.setText("");
				prefs.put("userHistoryTable.limit", CommonConstants.History.USER_HISTORY_ROW_LIMIT);
				limitFilterText.setText(CommonConstants.History.USER_HISTORY_ROW_LIMIT);
			}
		});

		userHistoryApplyButton = new Button(buttonBarComposite, SWT.PUSH);
		userHistoryApplyButton.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		//userHistoryApplyButton.setText(messages.historyApplyBtn);
		registry.register((text) -> {
			if (userHistoryApplyButton != null && !userHistoryApplyButton.isDisposed()) {
				userHistoryApplyButton.setText(text);
			}
		}, (message) -> {
			if (userHistoryApplyButton != null && !userHistoryApplyButton.isDisposed()) {
				return getUpdatedWidgetText(message.historyApplyBtn, userHistoryApplyButton);
			}
			return CommonConstants.EMPTY_STR;
		});
		userHistoryApplyButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				String stDateString = null;
				if (userHistroyStartDate.getDate() != null) {
					Date stDate = userHistroyStartDate.getDate();
					stDateString = XMSystemUtil.getHistoryLogTimeDateFormat().format(stDate);
					prefs.put("userHistory.startDate", stDateString);
				} else {
					prefs.put("userHistory.startDate", "");
				}

				String endDateString = null;
				if (userHistroyEndDate.getDate() != null) {
					Date eDate = userHistroyEndDate.getDate();
					endDateString = XMSystemUtil.getHistoryLogTimeDateFormat().format(eDate);
					prefs.put("userHistory.endDate", endDateString);
				} else {
					prefs.put("userHistory.endDate", "");
				}

				String userRule = cmbUser.getText();
				String userFilterText = txtUsernameHistoryfiltertext.getText();
				if (!userFilterText.isEmpty()) {
					prefs.put("userHistoryTable.userName", userRule + "," + userFilterText);
				} else {
					prefs.put("userHistoryTable.userName", "");
				}

				String hostRule = cmbHost.getText();
				String hostFilter = txtHostUserHistoryfilter.getText();
				if (!hostFilter.isEmpty()) {
					prefs.put("userHistoryTable.host", hostRule + "," + hostFilter);
				} else {
					prefs.put("userHistoryTable.host", "");
				}

				String siteRule = cmbSite.getText();
				String siteFilter = txtSiteUserHistoryfilter.getText();
				if (!siteFilter.isEmpty()) {
					prefs.put("userHistoryTable.site", siteRule + "," + siteFilter);
				} else {
					prefs.put("userHistoryTable.site", "");
				}

				String adminRule = cmbAdminArea.getText();
				String adminFilter = txtAdminareafilter.getText();
				if (!adminFilter.isEmpty()) {
					prefs.put("userHistoryTable.adminArea", adminRule + "," + adminFilter);
				} else {
					prefs.put("userHistoryTable.adminArea", "");
				}

				String projectRule = cmbProject.getText();
				String projectFilter = txtProjectfilter.getText();
				if (!projectFilter.isEmpty()) {
					prefs.put("userHistoryTable.projet", projectRule + "," + projectFilter);
				} else {
					prefs.put("userHistoryTable.projet", "");
				}

				String applicationRule = cmbApplication.getText();
				String applicationFilter = txtApplicationfilter.getText();
				if (!applicationFilter.isEmpty()) {
					prefs.put("userHistoryTable.application", applicationRule + "," + applicationFilter);
				} else {
					prefs.put("userHistoryTable.application", "");
				}

				String resultRule = cmbResult.getText();
				String resultFltr = txtResultfilter.getText().trim();
				if (!resultFltr.isEmpty()) {
					prefs.put("userHistoryTable.result", resultRule + "," + resultFltr);
				} else {
					prefs.put("userHistoryTable.result", "");
				}

				String limitString = limitFilterText.getText();
				if (!limitString.isEmpty()) {
					try {
						Integer queryLimit = Integer.parseInt(limitString);
						prefs.put("userHistoryTable.limit", String.valueOf(queryLimit));
					} catch (NumberFormatException ex) {
						CustomMessageDialog.openError(Display.getCurrent().getActiveShell(),messages.errorDialogTitile,
								messages.historyLimitErrorMsg);
					}
				} else {
					/*
					 * CustomMessageDialog.openError(Display.getCurrent().
					 * getActiveShell(), messages.errorDialogTitile,
					 * messages.historyEmptyLimitMsg); return;
					 */
					prefs.put("userHistoryTable.limit", CommonConstants.History.HISTORY_HIDDEN_ROW_LIMIT);
				}

				UserHistoryRequest userHistoryRequest = new UserHistoryRequest();
				userHistoryRequest.setQueryCondition(getQueryCondition(false));
				userHistoryRequest.setQueryLimit(Integer
						.parseInt(prefs.get("userHistoryTable.limit", CommonConstants.History.USER_HISTORY_ROW_LIMIT)));
				userHistoryTableContainer.updateNatTable(CommonConstants.FALSE, userHistoryRequest);
				if (prefs.get("userHistoryTable.limit", CommonConstants.History.USER_HISTORY_ROW_LIMIT)
						.equals(CommonConstants.History.HISTORY_HIDDEN_ROW_LIMIT)) {
					prefs.put("userHistoryTable.limit", CommonConstants.History.USER_HISTORY_ROW_LIMIT);
				}
				try {
					prefs.flush();
				} catch (BackingStoreException e1) {
					LOGGER.warn("Cannot store login preferences!", e1);
				}
			}

		});

		pGroup.setExpanded(false);
	}

	/**
	 * Method for Creates the user status filter composite.
	 *
	 * @param parent
	 *            {@link Composite}
	 */
	private void createUserStatusFilterComposite(final Composite parent) {

		final PGroup pGroup = new PGroup(parent, SWT.SMOOTH);

		pGroup.addExpandListener(new PGroupExpandListener());

		final GridLayout widgetContLayout = new GridLayout(6, false);
		widgetContLayout.horizontalSpacing = 10;
		pGroup.setLayout(widgetContLayout);
		pGroup.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 3, 1));
		//pGroup.setText(messages.userStatusFilterLbl);
		registry.register((text) -> {
			if (pGroup != null && !pGroup.isDisposed()) {
				pGroup.setText(text);
			}
		}, (message) -> {
			if (pGroup != null && !pGroup.isDisposed()) {
				return getUpdatedWidgetText(message.userStatusFilterLbl, pGroup);
			}
			return CommonConstants.EMPTY_STR;
		});

		Label lblLogTime = new Label(pGroup, SWT.NONE);
		//lblLogTime.setText(messages.historyLogTimeLabel);
		registry.register((text) -> {
			if (lblLogTime != null && !lblLogTime.isDisposed()) {
				lblLogTime.setText(text);
			}
		}, (message) -> {
			if (lblLogTime != null && !lblLogTime.isDisposed()) {
				return getUpdatedWidgetText(message.historyLogTimeLabel, lblLogTime);
			}
			return CommonConstants.EMPTY_STR;
		});
		lblLogTime.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		new Label(pGroup, SWT.NONE);

		Label lblCdate = new Label(pGroup, SWT.NONE);
		lblCdate.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false));
		//lblCdate.setText(messages.historyStartDateLabel);
		registry.register((text) -> {
			if (lblCdate != null && !lblCdate.isDisposed()) {
				lblCdate.setText(text);
			}
		}, (message) -> {
			if (lblCdate != null && !lblCdate.isDisposed()) {
				return getUpdatedWidgetText(message.historyStartDateLabel, lblCdate);
			}
			return CommonConstants.EMPTY_STR;
		});
		userStatusStartDate = new CustomDateTime(pGroup);
		userStatusStartDate.setLayoutData(new GridData(SWT.FILL, SWT.LEFT, true, false));
		String stDate = prefs.get("userStatusTable.startDate", "");
		if (!stDate.isEmpty()) {
			try {
				userStatusStartDate.setDate(XMSystemUtil.getHistoryLogTimeDateFormat().parse(stDate));
			} catch (ParseException e1) {
				LOGGER.error("Exception while parsing date!", e1);
			}
		}
		
		registry.register((text) -> {
		}, (message) -> {
			if(userStatusStartDate != null && !userStatusStartDate.isDisposed()){
			LANG_ENUM currentLocaleEnum = XMAdminUtil.getInstance().getCurrentLocaleEnum();
			userStatusStartDate.updateTooltipText(currentLocaleEnum.toString());
			}
			return CommonConstants.EMPTY_STR;
		});

		Label lblEndDate = new Label(pGroup, SWT.NONE);
		lblEndDate.setLayoutData(new GridData(SWT.BEGINNING, SWT.CENTER, false, false));
		//lblEndDate.setText(messages.historyEndDateLabel);
		registry.register((text) -> {
			if (lblEndDate != null && !lblEndDate.isDisposed()) {
				lblEndDate.setText(text);
			}
		}, (message) -> {
			if (lblEndDate != null && !lblEndDate.isDisposed()) {
				return getUpdatedWidgetText(message.historyEndDateLabel, lblEndDate);
			}
			return CommonConstants.EMPTY_STR;
		});
		userStatusEndDate = new CustomDateTime(pGroup);

		userStatusEndDate.setLayoutData(new GridData(SWT.FILL, SWT.LEFT, true, false));
		String eDate = prefs.get("userStatusTable.endDate", "");
		if (!eDate.isEmpty()) {
			try {
				userStatusEndDate.setDate(XMSystemUtil.getHistoryLogTimeDateFormat().parse(eDate));
			} catch (ParseException e1) {
				LOGGER.error("Exception while parsing date!", e1);
			}
		}
		
		registry.register((text) -> {
		}, (message) -> {
			if(userStatusEndDate != null && !userStatusEndDate.isDisposed()){
			LANG_ENUM currentLocaleEnum = XMAdminUtil.getInstance().getCurrentLocaleEnum();
			userStatusEndDate.updateTooltipText(currentLocaleEnum.toString());
			}
			return CommonConstants.EMPTY_STR;
		});

		Label lblUserName = new Label(pGroup, SWT.NONE);
		lblUserName.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		//lblUserName.setText(messages.userHistoryUserNameLbl);
		registry.register((text) -> {
			if (lblUserName != null && !lblUserName.isDisposed()) {
				lblUserName.setText(text);
			}
		}, (message) -> {
			if (lblUserName != null && !lblUserName.isDisposed()) {
				return getUpdatedWidgetText(message.userHistoryUserNameLbl, lblUserName);
			}
			return CommonConstants.EMPTY_STR;
		});

		Combo cmbUser = new Combo(pGroup, SWT.NONE);
		cmbUser.setItems(CommonConstants.History.QUERYTYPE);
		GridData gd_cmbUser = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gd_cmbUser.widthHint = 34;
		cmbUser.setLayoutData(gd_cmbUser);

		Text txtUsernameHistoryfiltertext = new Text(pGroup, SWT.BORDER);
		txtUsernameHistoryfiltertext.setText("");
		txtUsernameHistoryfiltertext.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 4, 1));

		String userName = prefs.get("userStatusTable.userName", "");
		if (!userName.isEmpty()) {
			String[] values = userName.split(",");
			cmbUser.setText(values[0]);
			txtUsernameHistoryfiltertext.setText(values[1]);
		}

		Label lblHost = new Label(pGroup, SWT.NONE);
		lblHost.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		//lblHost.setText(messages.userHistoryHostLbl);
		registry.register((text) -> {
			if (lblHost != null && !lblHost.isDisposed()) {
				lblHost.setText(text);
			}
		}, (message) -> {
			if (lblHost != null && !lblHost.isDisposed()) {
				return getUpdatedWidgetText(message.userHistoryHostLbl, lblHost);
			}
			return CommonConstants.EMPTY_STR;
		});

		Combo cmbHost = new Combo(pGroup, SWT.NONE);
		cmbHost.setItems(CommonConstants.History.QUERYTYPE);
		cmbHost.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));

		Text txtHostUserHistoryfilter = new Text(pGroup, SWT.BORDER);
		txtHostUserHistoryfilter.setText("");
		txtHostUserHistoryfilter.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 4, 1));

		String host = prefs.get("userStatusTable.host", "");
		if (!host.isEmpty()) {
			String[] values = host.split(",");
			cmbHost.setText(values[0]);
			txtHostUserHistoryfilter.setText(values[1]);
		}

		Label lblNewLabel_1 = new Label(pGroup, SWT.NONE);
		lblNewLabel_1.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		//lblNewLabel_1.setText(messages.userHistorySiteLbl);
		registry.register((text) -> {
			if (lblNewLabel_1 != null && !lblNewLabel_1.isDisposed()) {
				lblNewLabel_1.setText(text);
			}
		}, (message) -> {
			if (lblNewLabel_1 != null && !lblNewLabel_1.isDisposed()) {
				return getUpdatedWidgetText(message.userHistorySiteLbl, lblNewLabel_1);
			}
			return CommonConstants.EMPTY_STR;
		});

		Combo cmbSite = new Combo(pGroup, SWT.NONE);
		cmbSite.setItems(CommonConstants.History.QUERYTYPE);
		cmbSite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));

		Text txtSiteUserHistoryfilter = new Text(pGroup, SWT.BORDER);
		txtSiteUserHistoryfilter.setText("");
		txtSiteUserHistoryfilter.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 4, 1));

		String site = prefs.get("userStatusTable.site", "");
		if (!site.isEmpty()) {
			String[] values = site.split(",");
			cmbSite.setText(values[0]);
			txtSiteUserHistoryfilter.setText(values[1]);
		}

		Label lblAdminarea = new Label(pGroup, SWT.NONE);
		lblAdminarea.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		//lblAdminarea.setText(messages.userHistoryAdminAreaLbl);
		registry.register((text) -> {
			if (lblAdminarea != null && !lblAdminarea.isDisposed()) {
				lblAdminarea.setText(text);
			}
		}, (message) -> {
			if (lblAdminarea != null && !lblAdminarea.isDisposed()) {
				return getUpdatedWidgetText(message.userHistoryAdminAreaLbl, lblAdminarea);
			}
			return CommonConstants.EMPTY_STR;
		});

		Combo cmbAdminArea = new Combo(pGroup, SWT.NONE);
		cmbAdminArea.setItems(CommonConstants.History.QUERYTYPE);
		cmbAdminArea.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));

		Text txtAdminareafilter = new Text(pGroup, SWT.BORDER);
		txtAdminareafilter.setText("");
		txtAdminareafilter.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 4, 1));
		String adminArea = prefs.get("userStatusTable.adminArea", "");

		if (!adminArea.isEmpty()) {
			String[] values = adminArea.split(",");
			cmbAdminArea.setText(values[0]);
			txtAdminareafilter.setText(values[1]);
		}

		Label lblProject = new Label(pGroup, SWT.NONE);
		lblProject.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		//lblProject.setText(messages.userHistoryProjectLbl);
		registry.register((text) -> {
			if (lblProject != null && !lblProject.isDisposed()) {
				lblProject.setText(text);
			}
		}, (message) -> {
			if (lblProject != null && !lblProject.isDisposed()) {
				return getUpdatedWidgetText(message.userHistoryProjectLbl, lblProject);
			}
			return CommonConstants.EMPTY_STR;
		});

		Combo cmbProject = new Combo(pGroup, SWT.NONE);
		cmbProject.setItems(CommonConstants.History.QUERYTYPE);
		cmbProject.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));

		Text txtProjectfilter = new Text(pGroup, SWT.BORDER);
		txtProjectfilter.setText("");
		txtProjectfilter.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 4, 1));

		String project = prefs.get("userStatusTable.projet", "");
		if (!project.isEmpty()) {
			String[] values = project.split(",");
			cmbProject.setText(values[0]);
			txtProjectfilter.setText(values[1]);
		}

		Label lblApplication = new Label(pGroup, SWT.NONE);
		lblApplication.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		//lblApplication.setText(messages.userHistoryApplicationLbl);
		registry.register((text) -> {
			if (lblApplication != null && !lblApplication.isDisposed()) {
				lblApplication.setText(text);
			}
		}, (message) -> {
			if (lblApplication != null && !lblApplication.isDisposed()) {
				return getUpdatedWidgetText(message.userHistoryApplicationLbl, lblApplication);
			}
			return CommonConstants.EMPTY_STR;
		});

		Combo cmbApplication = new Combo(pGroup, SWT.NONE);
		cmbApplication.setItems(CommonConstants.History.QUERYTYPE);
		cmbApplication.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));

		Text txtApplicationfilter = new Text(pGroup, SWT.BORDER);
		txtApplicationfilter.setText("");
		txtApplicationfilter.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 4, 1));

		String application = prefs.get("userStatusTable.application", "");
		if (!application.isEmpty()) {
			String[] values = application.split(",");
			cmbApplication.setText(values[0]);
			txtApplicationfilter.setText(values[1]);
		}

		Label lblResult = new Label(pGroup, SWT.NONE);
		lblResult.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		//lblResult.setText(messages.userHistoryResultLbl);
		registry.register((text) -> {
			if (lblResult != null && !lblResult.isDisposed()) {
				lblResult.setText(text);
			}
		}, (message) -> {
			if (lblResult != null && !lblResult.isDisposed()) {
				return getUpdatedWidgetText(message.userHistoryResultLbl, lblResult);
			}
			return CommonConstants.EMPTY_STR;
		});

		Combo cmbResult = new Combo(pGroup, SWT.NONE);
		cmbResult.setItems(CommonConstants.History.QUERYTYPE);
		cmbResult.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));

		Text txtResultfilter = new Text(pGroup, SWT.BORDER);
		txtResultfilter.setText("");
		txtResultfilter.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 4, 1));

		String result = prefs.get("userStatusTable.result", "");
		if (!result.isEmpty()) {
			String[] values = result.split(",");
			cmbResult.setText(values[0]);
			txtResultfilter.setText(values[1]);
		}

		Label lblLimit = new Label(pGroup, SWT.NONE);
		lblLimit.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		//lblLimit.setText(messages.historyLimitLbl);
		registry.register((text) -> {
			if (lblLimit != null && !lblLimit.isDisposed()) {
				lblLimit.setText(text);
			}
		}, (message) -> {
			if (lblLimit != null && !lblLimit.isDisposed()) {
				return getUpdatedWidgetText(message.historyLimitLbl, lblLimit);
			}
			return CommonConstants.EMPTY_STR;
		});

		Text limitFilterText = new Text(pGroup, SWT.BORDER);

		String limit = prefs.get("userStatusTable.limit", CommonConstants.History.USER_HISTORY_ROW_LIMIT);

		limitFilterText.setText(limit);

		limitFilterText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 5, 1));
		limitFilterText.addVerifyListener(new VerifyListener() {

			@Override
			public void verifyText(VerifyEvent e) {
				String string = e.text;
				char[] chars = new char[string.length()];
				string.getChars(0, chars.length, chars, 0);
				for (int i = 0; i < chars.length; i++) {
					if (!('a' <= chars[i] && chars[i] <= 'z')) {
						e.doit = true;

					} else {
						e.doit = false;
					}
				}
			}
		});

		Composite buttonBarComposite = new Composite(pGroup, SWT.NONE);
		buttonBarComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 6, 1));
		buttonBarComposite.setLayout(new GridLayout(2, false));

		Button defaultButton = new Button(buttonBarComposite, SWT.PUSH);
		defaultButton.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, true, false, 1, 1));
		//defaultButton.setText(messages.historyDefaultBtn);
		registry.register((text) -> {
			if (defaultButton != null && !defaultButton.isDisposed()) {
				defaultButton.setText(text);
			}
		}, (message) -> {
			if (defaultButton != null && !defaultButton.isDisposed()) {
				return getUpdatedWidgetText(message.historyDefaultBtn, defaultButton);
			}
			return CommonConstants.EMPTY_STR;
		});
		defaultButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				prefs.put("userStatusTable.startDate", "");
				userStatusStartDate.setDate(null);
				prefs.put("userStatusTable.endDate", "");
				userStatusEndDate.setDate(null);
				prefs.put("userStatusTable.userName", "");
				cmbUser.setText("");
				txtUsernameHistoryfiltertext.setText("");
				prefs.put("userStatusTable.host", "");
				cmbHost.setText("");
				txtHostUserHistoryfilter.setText("");
				prefs.put("userStatusTable.site", "");
				cmbSite.setText("");
				txtSiteUserHistoryfilter.setText("");
				prefs.put("userStatusTable.adminArea", "");
				cmbAdminArea.setText("");
				txtAdminareafilter.setText("");
				prefs.put("userStatusTable.projet", "");
				cmbProject.setText("");
				txtProjectfilter.setText("");
				prefs.put("userStatusTable.application", "");
				cmbApplication.setText("");
				txtApplicationfilter.setText("");
				prefs.put("userStatusTable.result", "");
				cmbResult.setText("");
				txtResultfilter.setText("");
				prefs.put("userStatusTable.limit", CommonConstants.History.USER_HISTORY_ROW_LIMIT);
				limitFilterText.setText(CommonConstants.History.USER_HISTORY_ROW_LIMIT);
			}
		});

		userStatusApplyButton = new Button(buttonBarComposite, SWT.PUSH);
		userStatusApplyButton.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		//userStatusApplyButton.setText(messages.historyApplyBtn);
		registry.register((text) -> {
			if (userStatusApplyButton != null && !userStatusApplyButton.isDisposed()) {
				userStatusApplyButton.setText(text);
			}
		}, (message) -> {
			if (userStatusApplyButton != null && !userStatusApplyButton.isDisposed()) {
				return getUpdatedWidgetText(message.historyApplyBtn, userStatusApplyButton);
			}
			return CommonConstants.EMPTY_STR;
		});
		userStatusApplyButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {

				String stDateString = null;
				if (userStatusStartDate.getDate() != null) {
					Date stDate = userStatusStartDate.getDate();
					stDateString = XMSystemUtil.getHistoryLogTimeDateFormat().format(stDate);
					prefs.put("userStatusTable.startDate", stDateString);
				} else {
					prefs.put("userStatusTable.startDate", "");
				}
				String endDateString = null;
				if (userStatusEndDate.getDate() != null) {
					Date endDate = userStatusEndDate.getDate();
					endDateString = XMSystemUtil.getHistoryLogTimeDateFormat().format(endDate);
					prefs.put("userStatusTable.endDate", endDateString);
				} else {
					prefs.put("userStatusTable.endDate", "");
				}

				String userRule = cmbUser.getText();
				String userFilterText = txtUsernameHistoryfiltertext.getText();
				if (!userFilterText.isEmpty()) {
					prefs.put("userStatusTable.userName", userRule + "," + userFilterText);
				} else {
					prefs.put("userStatusTable.userName", "");
				}

				String hostRule = cmbHost.getText();
				String hostFilter = txtHostUserHistoryfilter.getText();
				if (!hostFilter.isEmpty()) {
					prefs.put("userStatusTable.host", hostRule + "," + hostFilter);
				} else {
					prefs.put("userStatusTable.host", "");
				}

				String siteRule = cmbSite.getText();
				String siteFilter = txtSiteUserHistoryfilter.getText();
				if (!siteFilter.isEmpty()) {
					prefs.put("userStatusTable.site", siteRule + "," + siteFilter);
				} else {
					prefs.put("userStatusTable.site", "");
				}

				String adminRule = cmbAdminArea.getText();
				String adminFilter = txtAdminareafilter.getText();
				if (!adminFilter.isEmpty()) {
					prefs.put("userStatusTable.adminArea", adminRule + "," + adminFilter);
				} else {
					prefs.put("userStatusTable.adminArea", "");
				}

				String projectRule = cmbProject.getText();
				String projectFilter = txtProjectfilter.getText();
				if (!projectFilter.isEmpty()) {
					prefs.put("userStatusTable.projet", projectRule + "," + projectFilter);
				} else {
					prefs.put("userStatusTable.projet", "");
				}

				String applicationRule = cmbApplication.getText();
				String applicationFilter = txtApplicationfilter.getText();
				if (!applicationFilter.isEmpty()) {
					prefs.put("userStatusTable.application", applicationRule + "," + applicationFilter);
				} else {
					prefs.put("userStatusTable.application", "");
				}

				String resultRule = cmbResult.getText();
				String resultFltr = txtResultfilter.getText().trim();
				if (!resultFltr.isEmpty()) {
					prefs.put("userStatusTable.result", resultRule + "," + resultFltr);
				} else {
					prefs.put("userStatusTable.result", "");
				}

				String limitString = limitFilterText.getText();

				if (!limitString.isEmpty()) {
					try {
						Integer queryLimit = Integer.parseInt(limitString);
						prefs.put("userStatusTable.limit", String.valueOf(queryLimit));
					} catch (NumberFormatException e1) {
						CustomMessageDialog.openError(Display.getCurrent().getActiveShell(), messages.errorDialogTitile,
								messages.historyLimitErrorMsg);
					}
				} else {
					/*
					 * CustomMessageDialog.openError(Display.getCurrent().
					 * getActiveShell(), messages.errorDialogTitile,
					 * messages.historyEmptyLimitMsg); return;
					 */
					prefs.put("userStatusTable.limit", CommonConstants.History.HISTORY_HIDDEN_ROW_LIMIT);
				}

				UserHistoryRequest userHistoryRequest = new UserHistoryRequest();
				userHistoryRequest.setQueryCondition(getQueryCondition(true));
				userHistoryRequest.setQueryLimit(Integer
						.parseInt(prefs.get("userStatusTable.limit", CommonConstants.History.USER_HISTORY_ROW_LIMIT)));
				userStatusTableContainer.updateNatTable(CommonConstants.TRUE, userHistoryRequest);
				if (prefs.get("userStatusTable.limit", CommonConstants.History.USER_HISTORY_ROW_LIMIT)
						.equals(CommonConstants.History.HISTORY_HIDDEN_ROW_LIMIT)) {
					prefs.put("userStatusTable.limit", CommonConstants.History.USER_HISTORY_ROW_LIMIT);
				}
				try {
					prefs.flush();
				} catch (BackingStoreException e1) {
					LOGGER.warn("Cannot store login preferences!", e1);
				}
			}
		});

		pGroup.setExpanded(false);

	}

	/**
	 * Method for Generate user status table.
	 */
	private void generateUserStatusTable() {
		GridLayout gridLayout = new GridLayout();
		gridLayout.marginWidth = 0;
		gridLayout.marginHeight = 0;

		userStatusPage = new Composite(mainDataGroup, SWT.NONE);
		userStatusPage.setLayout(gridLayout);
		userStatusPage.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

		final ScrolledComposite scrolledComposite = new ScrolledComposite(userStatusPage, SWT.V_SCROLL);
		scrolledComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		scrolledComposite.setExpandHorizontal(true);
		scrolledComposite.setExpandVertical(true);

		final Composite pageScrolledComposite = new Composite(scrolledComposite, SWT.NONE);
		pageScrolledComposite.setLayout(gridLayout);
		pageScrolledComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		
		createUserStatusFilterComposite(pageScrolledComposite);
		
		UserHistoryRequest userHistoryRequest = new UserHistoryRequest();
		userHistoryRequest.setQueryCondition(getQueryCondition(true));
		userHistoryRequest.setQueryLimit(
				Integer.parseInt(prefs.get("userStatusTable.limit", CommonConstants.History.USER_HISTORY_ROW_LIMIT)));

		Composite natTableComp = new Composite(pageScrolledComposite, SWT.BORDER);
		GridLayout natTableCompGL = new GridLayout(1, false);
		natTableCompGL.marginWidth = 0;
		natTableCompGL.marginHeight = 0;

		natTableComp.setLayout(natTableCompGL);
		GridData gridData = new GridData(GridData.FILL, SWT.FILL, true, true);
		gridData.widthHint = 600;
		gridData.minimumHeight = 300;
		gridData.heightHint = 300;
		natTableComp.setLayoutData(gridData);

		userStatusTableContainer = new UserHistoryTblNattable();
		userStatusNatTable = userStatusTableContainer.createUserHistoryNatTable(natTableComp, CommonConstants.TRUE,
				userHistoryRequest);

		scrolledComposite.setContent(pageScrolledComposite);
		scrolledComposite.update();

		scrolledComposite.addControlListener(new ControlAdapter() {
			public void controlResized(final ControlEvent e) {
				Rectangle rectangle = scrolledComposite.getClientArea();
				scrolledComposite.setMinSize(pageScrolledComposite.computeSize(rectangle.width, SWT.DEFAULT));
			}
		});

		scrolledComposite.addListener(SWT.Activate, new Listener() {
			public void handleEvent(Event e) {
				scrolledComposite.setFocus();
			}
		});
	}

	
	/**
	 * Update nat table column labels.
	 */
	private void updateNatTableColumnLabels() {
		if (userHistoryNatTable != null && !userHistoryNatTable.isDisposed() &&userStatusNatTable != null && !userStatusNatTable.isDisposed()) {
			HistoryTableUtil.getInstance().getUserHistoryPropToLabelMap();
			userStatusNatTable.refresh();
			userHistoryNatTable.refresh();
		}
	}

	/**
	 * Method for Generate user history table.
	 */
	private void generateUserHistoryTable() {
		GridLayout gridLayout = new GridLayout();
		gridLayout.marginWidth = 0;
		gridLayout.marginHeight = 0;

		userHistoryPage = new Composite(mainDataGroup, SWT.NONE);
		userHistoryPage.setLayout(gridLayout);
		userHistoryPage.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

		final ScrolledComposite scrolledComposite = new ScrolledComposite(userHistoryPage, SWT.V_SCROLL);
		scrolledComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		scrolledComposite.setExpandHorizontal(true);
		scrolledComposite.setExpandVertical(true);

		final Composite pageScrolledComposite = new Composite(scrolledComposite, SWT.NONE);
		createUserHistoryFilterComposite(pageScrolledComposite);
		pageScrolledComposite.setLayout(gridLayout);
		pageScrolledComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		UserHistoryRequest userHistoryRequest = new UserHistoryRequest();
		userHistoryRequest.setQueryCondition(getQueryCondition(false));
		userHistoryRequest.setQueryLimit(
				Integer.parseInt(prefs.get("userHistoryTable.limit", CommonConstants.History.USER_HISTORY_ROW_LIMIT)));

		Composite natTableComp = new Composite(pageScrolledComposite, SWT.BORDER);
		GridLayout natTableCompGL = new GridLayout(1, false);
		natTableCompGL.marginWidth = 0;
		natTableCompGL.marginHeight = 0;

		natTableComp.setLayout(natTableCompGL);
		GridData gridData = new GridData(GridData.FILL, SWT.FILL, true, true);
		gridData.widthHint = 600;
		gridData.minimumHeight = 300;
		natTableComp.setLayoutData(gridData);

		userHistoryTableContainer = new UserHistoryTblNattable();
		userHistoryNatTable = userHistoryTableContainer.createUserHistoryNatTable(natTableComp, CommonConstants.FALSE,
				userHistoryRequest);

		scrolledComposite.setContent(pageScrolledComposite);
		scrolledComposite.update();

		scrolledComposite.addControlListener(new ControlAdapter() {
			public void controlResized(final ControlEvent e) {
				Rectangle rectangle = scrolledComposite.getClientArea();
				scrolledComposite.setMinSize(pageScrolledComposite.computeSize(rectangle.width, SWT.DEFAULT));
			}
		});

		scrolledComposite.addListener(SWT.Activate, new Listener() {
			public void handleEvent(Event e) {
				scrolledComposite.setFocus();
			}
		});
	}

	/**
	 * Gets the query condition.
	 *
	 * @param status
	 *            {@link boolean}
	 * @return the query condition
	 */
	private String getQueryCondition(boolean status) {
		ArrayList<String> conditions = new ArrayList<String>();
		String stDate;
		if (status) {
			stDate = prefs.get("userStatusTable.startDate", "");
		} else {
			stDate = prefs.get("userHistory.startDate", "");
		}
		String eDate;
		if (status) {
			eDate = prefs.get("userStatusTable.endDate", "");
		} else {
			eDate = prefs.get("userHistory.endDate", "");
		}

		if (!stDate.isEmpty() && !eDate.isEmpty()) {
			String condition = "LOG_TIME > " + "'" + stDate + "'" + " AND LOG_TIME < " + "'" + eDate + "'";
			conditions.add(condition);
		} else if (!stDate.isEmpty()) {
			String condition = "LOG_TIME > " + "'" + stDate + "'";
			conditions.add(condition);
		} else if (!eDate.isEmpty()) {
			String condition = "LOG_TIME < " + "'" + eDate + "'";
			conditions.add(condition);
		}
		String userName;
		if (status) {
			userName = prefs.get("userStatusTable.userName", "");
		} else {
			userName = prefs.get("userHistoryTable.userName", "");
		}
		if (!userName.isEmpty()) {
			String[] values = userName.split(",");
			String userRule = values[0];
			String userFilterText = values[1];
			String condition = "USER_NAME " + userRule + " '" + userFilterText + "'";
			conditions.add(condition);
		}
		String host;
		if (status) {
			host = prefs.get("userStatusTable.host", "");
		} else {
			host = prefs.get("userHistoryTable.host", "");
		}
		if (!host.isEmpty()) {
			String[] values = host.split(",");
			String hostRule = values[0];
			String hostFilter = values[1];
			String condition = "HOST " + hostRule + " '" + hostFilter + "'";
			conditions.add(condition);
		}

		String site;
		if (status) {
			site = prefs.get("userStatusTable.site", "");
		} else {
			site = prefs.get("userHistoryTable.site", "");
		}
		if (!site.isEmpty()) {
			String[] values = site.split(",");
			String siteRule = values[0];
			String siteFilter = values[1];
			String condition = "SITE " + siteRule + " '" + siteFilter + "'";
			conditions.add(condition);
		}
		String adminArea;
		if (status) {
			adminArea = prefs.get("userStatusTable.adminArea", "");
		} else {
			adminArea = prefs.get("userHistoryTable.adminArea", "");
		}
		if (!adminArea.isEmpty()) {
			String[] values = adminArea.split(",");
			String adminRule = values[0];
			String adminFilter = values[1];
			String condition = "ADMIN_AREA " + adminRule + " '" + adminFilter + "'";
			conditions.add(condition);
		}
		String projet;
		if (status) {
			projet = prefs.get("userStatusTable.projet", "");
		} else {
			projet = prefs.get("userHistoryTable.projet", "");
		}
		if (!projet.isEmpty()) {
			String[] values = projet.split(",");
			String projectRule = values[0];
			String projectFilter = values[1];
			String condition = "PROJECT " + projectRule + " '" + projectFilter + "'";
			conditions.add(condition);
		}
		String application;
		if (status) {
			application = prefs.get("userStatusTable.application", "");
		} else {
			application = prefs.get("userHistoryTable.application", "");
		}
		if (!application.isEmpty()) {
			String[] values = application.split(",");
			String applicationRule = values[0];
			String applicationFilter = values[1];
			String condition = "APPLICATION " + applicationRule + " '" + applicationFilter + "'";
			conditions.add(condition);
		}
		String result;
		if (status) {
			result = prefs.get("userStatusTable.result", "");
		} else {
			result = prefs.get("userHistoryTable.result", "");
		}
		if (!result.isEmpty()) {
			String[] values = result.split(",");
			String resultRule = values[0];
			String resultFltr = values[1];
			String condition = "RESULT " + resultRule + " '" + resultFltr + "'";
			conditions.add(condition);
		}
		StringBuilder query = new StringBuilder();
		for (String condition : conditions) {
			if (conditions.indexOf(condition) == 0) {
				query.append(condition).toString();
			} else {
				query.append(" AND " + condition).toString();
			}
		}
		if (!query.toString().trim().isEmpty()) {
			return query.toString();
		} else {
			return null;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.dialogs.Dialog#createButtonsForButtonBar(org.eclipse.
	 * swt.widgets.Composite)
	 */
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		// createButton(parent, IDialogConstants.OK_ID,
		// IDialogConstants.OK_LABEL, true);
		// createButton(parent, IDialogConstants.CANCEL_ID,
		// IDialogConstants.CANCEL_LABEL, false);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.dialogs.Dialog#isResizable()
	 */
	@Override
	protected boolean isResizable() {
		return true;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.dialogs.Dialog#getInitialSize()
	 */
	@Override
	protected Point getInitialSize() {
		return new Point(800, 500);
	}

	/**
	 * The listener interface for receiving PGroupExpand events. The class that
	 * is interested in processing a PGroupExpand event implements this
	 * interface, and the object created with that class is registered with a
	 * component using the component's <code>addPGroupExpandListener<code>
	 * method. When the PGroupExpand event occurs, that object's appropriate
	 * method is invoked.
	 *
	 * @see PGroupExpandEvent
	 */
	private class PGroupExpandListener implements ExpandListener {

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * org.eclipse.swt.events.ExpandListener#itemExpanded(org.eclipse.swt.
		 * events.ExpandEvent)
		 */
		@Override
		public void itemExpanded(final ExpandEvent event) {
			pGroupRepaint((PGroup) event.widget);
			((PGroup) event.widget).forceFocus();
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * org.eclipse.swt.events.ExpandListener#itemCollapsed(org.eclipse.swt.
		 * events.ExpandEvent)
		 */
		@Override
		public void itemCollapsed(final ExpandEvent event) {
			pGroupRepaint((PGroup) event.widget);
		}

		/**
		 * Method for P group repaint.
		 *
		 * @param pGroup
		 *            the group
		 */
		private void pGroupRepaint(final PGroup pGroup) {
			pGroup.requestLayout();
			pGroup.redraw();
			pGroup.getParent().update();
			pGroup.getParent().getParent().notifyListeners(SWT.Resize, new Event());
			// groupScrolledComposite.notifyListeners(SWT.Resize, new Event());
		}
	}
	
	@Override
	public boolean close() {
		 final IJobManager manager = Job.getJobManager();
		  final Job[] jobs = manager.find(null); //return all jobs
		  for (Job job: jobs) {
			if(CommonConstants.History.USER_HISTORY_JOB_NAME.equals(job.getName())) {
				if (job.getState() == Job.RUNNING) {
					job.done(Status.OK_STATUS);
				}
			}
		}
		return super.close();
	}
	
	/**
	 * Gets the new text based on new locale.
	 *
	 * @param message
	 *            {@link String}
	 * @param control
	 *            {@link Control}
	 * @return {@link String} new text
	 */
	private String getUpdatedWidgetText(final String message, final Control control) {
		control.requestLayout();
		control.getParent().redraw();
		control.getParent().getParent().update();
		control.getParent().getParent().getParent().update();
		return message;
	}

}
