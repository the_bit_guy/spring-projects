package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification;

import java.beans.PropertyChangeEvent;
import java.util.Set;
import java.util.TreeSet;

import com.magna.xmsystem.dependencies.utils.XMSystemUtil;
import com.magna.xmsystem.xmadmin.util.CommonConstants;

public class ProjectActivateEvtAction extends NotificationEvtActions {

	
	public ProjectActivateEvtAction(final String id, final String name, final boolean isActive,
			final int operationMode) {
		super(id, name, isActive, operationMode);
	}

	
	public ProjectActivateEvtAction deepCopyProActivateEvtAction(boolean update,ProjectActivateEvtAction updateThisObject) {
		ProjectActivateEvtAction clonedProjectActivateEvtAction = null;

		String currentId = XMSystemUtil.isEmpty(this.getId()) ? CommonConstants.EMPTY_STR : this.getId();
		String currentName = XMSystemUtil.isEmpty(this.getName()) ? CommonConstants.EMPTY_STR : this.getName();
		boolean currentIsActive = this.isActive();
		String currentDescription = XMSystemUtil.isEmpty(this.getDescription()) ? CommonConstants.EMPTY_STR
				: this.getDescription();
		Set<String> currentToUserList = this.getToUsers();
		Set<String> currentCCUserList = this.getCcUsers();

		NotificationTemplate currentTemplate = new NotificationTemplate();
		currentTemplate.setTemplateId(this.getTemplate().getTemplateId());
		currentTemplate.setName(this.getTemplate().getName());
		currentTemplate.setSubject(this.getTemplate().getSubject());
		currentTemplate.setMessage(this.getTemplate().getMessage());
		TreeSet<String> currentVariables;
		if (this.getTemplate().getVariables() != null) {
			currentVariables = new TreeSet<>(this.getTemplate().getVariables());
		} else {
			currentVariables = new TreeSet<>();
		}
		currentTemplate.setVariables(currentVariables);

		if (update) {
			clonedProjectActivateEvtAction = updateThisObject;
		} else {
			clonedProjectActivateEvtAction = new ProjectActivateEvtAction(currentId, currentName, currentIsActive,
					CommonConstants.OPERATIONMODE.VIEW);
		}
		clonedProjectActivateEvtAction.setId(currentId);
		clonedProjectActivateEvtAction.setName(currentName);
		clonedProjectActivateEvtAction.setActive(currentIsActive);
		clonedProjectActivateEvtAction.setDescription(currentDescription);
		clonedProjectActivateEvtAction.setToUsers(currentToUserList);
		clonedProjectActivateEvtAction.setCcUsers(currentCCUserList);
		clonedProjectActivateEvtAction.setTemplate(currentTemplate);

		return clonedProjectActivateEvtAction;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.BeanModel#
	 * propertyChange(java.beans.PropertyChangeEvent)
	 */
	@Override
	public void propertyChange(PropertyChangeEvent event) {
		this.propertyChangeSupport.firePropertyChange(event.getPropertyName(), event.getOldValue(),
				event.getNewValue());
	}
}
