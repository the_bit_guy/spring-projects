package com.magna.xmsystem.xmadmin.ui.parts.dndperspective.model;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.Project;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.refs.RelationObj;

/**
 * The Class ProjectWrapperObj.
 * 
 * @author shashwat.anand
 */
public class ProjectWrapperObj {
	
	/** The project. */
	private Project project;
	
	/** The related obj. */
	private IAdminTreeChild relatedObj;
	
	/** The relation obj. */
	private RelationObj relationObj;
	
	/**
	 * Instantiates a new project wrapper obj.
	 *
	 * @param project the project
	 */
	public ProjectWrapperObj(final Project project) {
		this(project, null);
	}
	
	/**
	 * Instantiates a new project wrapper obj.
	 *
	 * @param project the project
	 * @param relatedObj the related obj
	 */
	public ProjectWrapperObj(final Project project, final IAdminTreeChild relatedObj) {
		this.project = project;
		this.relatedObj = relatedObj;
	}
	
	/**
	 * Gets the type name.
	 *
	 * @return the type name
	 */
	public String getTypeName() {
		return Project.class.getSimpleName();
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return this.project.getName();
	}
	
	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public String getId() {
		if (this.relationObj != null) {
			return this.relationObj.getRelId();
		}
		return this.project.getProjectId();
	}

	/**
	 * @return the project
	 */
	public Project getProject() {
		return project;
	}

	/**
	 * @param project the project to set
	 */
	public void setProject(Project project) {
		this.project = project;
	}

	/**
	 * @return the relatedObj
	 */
	public IAdminTreeChild getRelatedObj() {
		return relatedObj;
	}

	/**
	 * @param relatedObj the relatedObj to set
	 */
	public void setRelatedObj(IAdminTreeChild relatedObj) {
		this.relatedObj = relatedObj;
	}

	/**
	 * @return the relationObj
	 */
	public RelationObj getRelationObj() {
		return relationObj;
	}

	/**
	 * @param relationObj the relationObj to set
	 */
	public void setRelationObj(RelationObj relationObj) {
		this.relationObj = relationObj;
	}
}
