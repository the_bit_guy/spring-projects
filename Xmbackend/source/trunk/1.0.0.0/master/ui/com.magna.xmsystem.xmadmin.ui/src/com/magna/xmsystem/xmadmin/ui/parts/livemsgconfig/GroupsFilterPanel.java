package com.magna.xmsystem.xmadmin.ui.parts.livemsgconfig;

import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.events.ControlAdapter;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import com.magna.xmsystem.ui.controls.widgets.MagnaCustomCombo;
import com.magna.xmsystem.xmadmin.message.MessageRegistry;
import com.magna.xmsystem.xmadmin.util.XMAdminUtil;

public class GroupsFilterPanel extends Composite{

	/** The filter label user group. */
	private Label filterLabelUserGroup;
	
	/** The user group filter text. */
	private Text userGroupFilterText;
	
	/** The user group filter button. */
	private Button userGroupFilterButton;
	
	/** The user group filter combo. */
	private MagnaCustomCombo userGroupFilterCombo;
	
	/** The project group filter combo. */
	private Group projectGroupFilterGroup;
	
	/** The project group filter label. */
	private Label filterLabelProjectGroup;
	
	/** The project group filter text. */
	private Text projectGroupFilterText;
	
	/** The project group filter button. */
	private Button projectGroupFilterButton;
	
	/** The project group filter combo. */
	private MagnaCustomCombo projectGroupFilterCombo;

	/**
	 * Instantiates a new user group filter panel.
	 *
	 * @param parent the parent
	 */
	public GroupsFilterPanel(Composite parent) {
		super(parent, SWT.NONE);
		createUserGroupFilter();
	}

	/**
	 * Creates the user group filter.
	 */
	private void createUserGroupFilter() {
		
		ScrolledComposite scrolledComposite = XMAdminUtil.getInstance().createScrolledComposite(this);
		final Composite filterPanelContainer = new Composite(scrolledComposite, SWT.NONE);
		filterPanelContainer.setLayout(new GridLayout(5, false));
		filterPanelContainer.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

		final GridData filterButtonsGridData = new GridData();
		filterButtonsGridData.widthHint = 30;
		filterButtonsGridData.heightHint = 30;
		
		
		final Composite userGrpLblCont = new Composite(filterPanelContainer, SWT.NONE);
		final GridLayout lblContLayout = new GridLayout(1, false);
		lblContLayout.marginRight = 0;
		lblContLayout.marginLeft = 0;
		lblContLayout.marginTop = 0;
		lblContLayout.marginBottom = 0;
		lblContLayout.marginWidth = 0;
		lblContLayout.marginHeight = 0;
		userGrpLblCont.setLayout(lblContLayout);
		GridDataFactory.fillDefaults().grab(false, false).span(1, 1).align(SWT.FILL, SWT.CENTER)
				.applyTo(userGrpLblCont);

		this.filterLabelUserGroup = new Label(userGrpLblCont, SWT.NONE);
		GridDataFactory.fillDefaults().grab(true, false).span(1, 1).align(SWT.FILL, SWT.CENTER).minSize(180, SWT.DEFAULT).applyTo(this.filterLabelUserGroup);
		this.userGroupFilterText = new Text(filterPanelContainer, SWT.BORDER);
		GridDataFactory.fillDefaults().grab(true, false).span(1, 1).align(SWT.FILL, SWT.CENTER).minSize(100, SWT.DEFAULT).applyTo(this.userGroupFilterText);
		this.userGroupFilterButton = new Button(filterPanelContainer, SWT.PUSH);
		this.userGroupFilterButton.setLayoutData(filterButtonsGridData);
		this.userGroupFilterCombo = new MagnaCustomCombo(filterPanelContainer, SWT.BORDER | SWT.READ_ONLY);
		GridDataFactory.fillDefaults().grab(true, false).span(2, 1).align(SWT.FILL, SWT.CENTER).minSize(150, SWT.DEFAULT)
				.applyTo(this.userGroupFilterCombo);
		
		final Composite projGrpLblCont = new Composite(filterPanelContainer, SWT.NONE);
		final GridLayout lblContLayout1 = new GridLayout(1, false);
		lblContLayout1.marginRight = 0;
		lblContLayout1.marginLeft = 0;
		lblContLayout1.marginTop = 0;
		lblContLayout1.marginBottom = 0;
		lblContLayout1.marginWidth = 0;
		lblContLayout1.marginHeight = 0;
		projGrpLblCont.setLayout(lblContLayout1);
		GridDataFactory.fillDefaults().grab(false, false).span(1, 1).align(SWT.FILL, SWT.CENTER)
				.applyTo(projGrpLblCont);
		
		this.filterLabelProjectGroup = new Label(projGrpLblCont, SWT.NONE);
		GridDataFactory.fillDefaults().grab(true, false).span(1, 1).align(SWT.FILL, SWT.CENTER).minSize(180, SWT.DEFAULT).applyTo(this.filterLabelProjectGroup);
		this.projectGroupFilterText = new Text(filterPanelContainer, SWT.BORDER);
		GridDataFactory.fillDefaults().grab(true, false).span(1, 1).align(SWT.FILL, SWT.CENTER).minSize(100, SWT.DEFAULT).applyTo(this.projectGroupFilterText);
		this.projectGroupFilterButton = new Button(filterPanelContainer, SWT.PUSH);
		this.projectGroupFilterButton.setLayoutData(filterButtonsGridData);
		this.projectGroupFilterCombo = new MagnaCustomCombo(filterPanelContainer, SWT.BORDER | SWT.READ_ONLY);
		GridDataFactory.fillDefaults().grab(true, false).span(2, 1).align(SWT.FILL, SWT.CENTER).minSize(150, SWT.DEFAULT)
				.applyTo(this.projectGroupFilterCombo);
		
		scrolledComposite.setContent(filterPanelContainer);
		scrolledComposite.setSize(filterPanelContainer.getSize());
		scrolledComposite.setExpandVertical(true);
		scrolledComposite.setExpandHorizontal(true);
		scrolledComposite.update();

		scrolledComposite.addControlListener(new ControlAdapter() {
			public void controlResized(final ControlEvent e) {
				//Rectangle rectangle = filterPanelContainer.getClientArea();
				scrolledComposite.setMinSize(filterPanelContainer.computeSize(SWT.DEFAULT, SWT.DEFAULT));
			}
		});
	}

	/**
	 * Register messages.
	 *
	 * @param registry the registry
	 */
	public void registerMessages(final MessageRegistry registry) {
		//registry.register(this.userGroupFilterGroup::setText, (message) -> message.userGroupFilterLabel);
		registry.register(this.filterLabelUserGroup::setText, (message) -> message.userGroupFilterLabel/*message.filterLabel*/);
		registry.register(this.userGroupFilterButton::setText, (message) -> message.allBtnLabel);
		
		//registry.register(this.projectGroupFilterGroup::setText, (message) -> message.projectGroupFilterLabel);
		registry.register(this.filterLabelProjectGroup::setText, (message) -> message.projectGroupFilterLabel/*message.filterLabel*/);
		registry.register(this.projectGroupFilterButton::setText, (message) -> message.allBtnLabel);
	}
	
	/**
	 * Enable user group filter.
	 *
	 * @param flag the flag
	 */
	public void enableUserGroupFilter(boolean flag) {
		this.userGroupFilterText.setEnabled(flag);
		this.userGroupFilterButton.setEnabled(flag);
		this.userGroupFilterCombo.setEnabled(flag);
		
		this.projectGroupFilterText.setEnabled(flag);
		this.projectGroupFilterButton.setEnabled(flag);
		this.projectGroupFilterCombo.setEnabled(flag);
	}

	
/*	*//**
	 * Gets the user group filter group.
	 *
	 * @return the user group filter group
	 *//*
	public Group getUserGroupFilterGroup() {
		return userGroupFilterGroup;
	}

	
	*//**
	 * Sets the user group filter group.
	 *
	 * @param userGroupFilterGroup the new user group filter group
	 *//*
	public void setUserGroupFilterGroup(Group userGroupFilterGroup) {
		this.userGroupFilterGroup = userGroupFilterGroup;
	}*/

	
	/**
	 * Gets the filter label user group.
	 *
	 * @return the filter label user group
	 */
	public Label getFilterLabelUserGroup() {
		return filterLabelUserGroup;
	}

	
	/**
	 * Sets the filter label user group.
	 *
	 * @param filterLabelUserGroup the new filter label user group
	 */
	public void setFilterLabelUserGroup(Label filterLabelUserGroup) {
		this.filterLabelUserGroup = filterLabelUserGroup;
	}

	
	/**
	 * Gets the user group filter text.
	 *
	 * @return the user group filter text
	 */
	public Text getUserGroupFilterText() {
		return userGroupFilterText;
	}

	/**
	 * Sets the user group filter text.
	 *
	 * @param userGroupFilterText the new user group filter text
	 */
	public void setUserGroupFilterText(Text userGroupFilterText) {
		this.userGroupFilterText = userGroupFilterText;
	}

	
	/**
	 * Gets the user group filter button.
	 *
	 * @return the user group filter button
	 */
	public Button getUserGroupFilterButton() {
		return userGroupFilterButton;
	}

	
	/**
	 * Sets the user group filter button.
	 *
	 * @param userGroupFilterButton the new user group filter button
	 */
	public void setUserGroupFilterButton(Button userGroupFilterButton) {
		this.userGroupFilterButton = userGroupFilterButton;
	}

	
	/**
	 * Gets the user group filter combo.
	 *
	 * @return the user group filter combo
	 */
	public MagnaCustomCombo getUserGroupFilterCombo() {
		return userGroupFilterCombo;
	}
	
	public Group getProjectGroupFilterGroup() {
		return projectGroupFilterGroup;
	}
	
	public Text getProjectGroupFilterText() {
		return projectGroupFilterText;
	}
	
	public Button getProjectGroupFilterButton() {
		return projectGroupFilterButton;
	}


	public MagnaCustomCombo getProjectGroupFilterCombo() {
		return projectGroupFilterCombo;
	}

	
	
}
