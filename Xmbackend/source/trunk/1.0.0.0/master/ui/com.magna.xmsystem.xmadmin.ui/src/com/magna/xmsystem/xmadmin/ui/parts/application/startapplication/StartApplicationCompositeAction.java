package com.magna.xmsystem.xmadmin.ui.parts.application.startapplication;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.inject.Inject;

import org.eclipse.core.databinding.Binding;
import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.beans.BeanProperties;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.core.databinding.observable.value.IValueChangeListener;
import org.eclipse.core.databinding.observable.value.ValueChangeEvent;
import org.eclipse.e4.core.services.nls.Translation;
import org.eclipse.e4.ui.di.Persist;
import org.eclipse.e4.ui.model.application.ui.MDirtyable;
import org.eclipse.jface.databinding.fieldassist.ControlDecorationSupport;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.fieldassist.ControlDecoration;
import org.eclipse.jface.fieldassist.FieldDecorationRegistry;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreePath;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.VerifyEvent;
import org.eclipse.swt.events.VerifyListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Link;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.ToolItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.ResourceAccessException;

import com.magna.xmbackend.entities.LanguagesTbl;
import com.magna.xmbackend.entities.StartAppTranslationTbl;
import com.magna.xmbackend.entities.StartApplicationsTbl;
import com.magna.xmsystem.dependencies.customExceptions.UnauthorizedAccessException;
import com.magna.xmsystem.dependencies.utils.LANG_ENUM;
import com.magna.xmsystem.dependencies.utils.XMSystemUtil;
import com.magna.xmsystem.ui.controls.widgets.CustomMessageDialog;
import com.magna.xmsystem.xmadmin.message.Message;
import com.magna.xmsystem.xmadmin.message.MessageRegistry;
import com.magna.xmsystem.xmadmin.restclient.application.StartAppController;
import com.magna.xmsystem.xmadmin.ui.Application.dialogs.BrowseApplicationDialog;
import com.magna.xmsystem.xmadmin.ui.dialogs.lang.ControlModel;
import com.magna.xmsystem.xmadmin.ui.dialogs.lang.TextAreaModifyListener;
import com.magna.xmsystem.xmadmin.ui.dialogs.lang.XMAdminLangTextAreaDialog;
import com.magna.xmsystem.xmadmin.ui.dialogs.lang.XMAdminLangTextDialog;
import com.magna.xmsystem.xmadmin.ui.parts.icons.IconDialog;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.AdminTreeviewer;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.AdminTreeFactory;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.baseapplicationmodel.BaseApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.startapplicationmodel.StartApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.startapplicationmodel.StartApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.icon.Icon;
import com.magna.xmsystem.xmadmin.ui.validation.NameValidation;
import com.magna.xmsystem.xmadmin.ui.validation.StatusValidation;
import com.magna.xmsystem.xmadmin.ui.validation.SymbolValidation;
import com.magna.xmsystem.xmadmin.util.CommonConstants;
import com.magna.xmsystem.xmadmin.util.MessageType;
import com.magna.xmsystem.xmadmin.util.XMAdminUtil;

/**
 * Class for StartApplicationCompositeAction.
 *
 * @author Deepak upadhyay
 */
public class StartApplicationCompositeAction extends StartApplicationCompositeUI {
	
	/** The Constant PROGRAMMATICALLY. */
	private static final String PROGRAMMATICALLY = "Programmatically"; //$NON-NLS-1$

	/** Logger instance. */
	private static final Logger LOGGER = LoggerFactory.getLogger(StartApplicationCompositeAction.class);

	/** Member variable for startApplication Model. */
	private StartApplication startApplicationModel;

	/** Member variable for {@link MessageRegistry}. */
	@Inject
	private MessageRegistry registry;

	/** Member variable for message. */
	@Inject
	@Translation
	transient private Message messages;

	/** Member variable for widgetValue. */
	transient private IObservableValue<?> widgetValue;

	/** Member variable for modelValue. */
	transient private IObservableValue<?> modelValue;

	/**
	 * Member variable for data binding context the DataBindingContext object
	 * will manage the data bindings.
	 */
	final transient DataBindingContext dataBindContext = new DataBindingContext();

	/** Member variable to store old model. */
	private StartApplication oldModel;

	/**
	 * MDirtyable flag
	 */
	private MDirtyable dirty;

	/**
	 * Constructor.
	 *
	 * @param parent
	 *            {@link Composite}
	 * @param style
	 *            int
	 * @param messages
	 *            {@link Message}
	 */
	@Inject
	public StartApplicationCompositeAction(final Composite parent) {
		super(parent, SWT.NONE);
		initListeners();
	}

	/**
	 * Method for Binding modal to widget.
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void bindValues() {
		try {
			// Name field binding
			widgetValue = WidgetProperties.text(SWT.Modify).observe(this.txtName);
			modelValue = BeanProperties.value(StartApplication.class, StartApplication.PROPERTY_NAME).observe(this.startApplicationModel);
			widgetValue.addValueChangeListener(new IValueChangeListener() {
				/**
				 * handler to update button status
				 */
				@Override
				public void handleValueChange(final ValueChangeEvent event) {
					updateButtonStatus(event);
				}
			});

			// define the UpdateValueStrategy
			final UpdateValueStrategy update = new UpdateValueStrategy();
			update.setAfterGetValidator(new NameValidation(messages, StatusValidation.APPLICATION));
			Binding bindValue = dataBindContext.bindValue(widgetValue, modelValue, update, null);
			ControlDecorationSupport.create(bindValue, SWT.TOP | SWT.LEFT);

			// Action check box binding
			widgetValue = WidgetProperties.selection().observe(this.activeBtn);
			modelValue = BeanProperties.value(StartApplication.class, StartApplication.PROPERTY_ACTIVE)
					.observe(this.startApplicationModel);
			bindValue = dataBindContext.bindValue(widgetValue, modelValue);

			// Symbol toolItem setup
			Icon icon;
			String iconPath;
			if ((icon = this.startApplicationModel.getIcon()) != null
					&& !XMSystemUtil.isEmpty(iconPath = icon.getIconPath())) {
				if (iconPath.contains("null")) { //$NON-NLS-1$
					toolItem.setImage(XMSystemUtil.getInstance().getImage(this.getClass(), "icons/16x16/browse.png")); //$NON-NLS-1$
				} else {
					toolItem.setImage(XMSystemUtil.getInstance().getImage(this.getClass(), iconPath, true, true));
					// Symbol toolItem binding
					widgetValue = WidgetProperties.tooltipText().observe(this.toolItem);
					modelValue = BeanProperties
							.value(StartApplication.class,
									StartApplication.PROPERTY_ICON + "." + Icon.PROPERTY_ICONNAME, Icon.class)
							.observe(this.startApplicationModel);
					bindValue = dataBindContext.bindValue(widgetValue, modelValue);

					// Symbol Field binding
					widgetValue = WidgetProperties.text(SWT.Modify).observe(this.txtSymbol);
					modelValue = BeanProperties
							.value(StartApplication.class,
									StartApplication.PROPERTY_ICON + "." + Icon.PROPERTY_ICONNAME, Icon.class)
							.observe(this.startApplicationModel);
					final UpdateValueStrategy symbolUpdate = new UpdateValueStrategy();
					symbolUpdate.setAfterGetValidator(new SymbolValidation());
					bindValue = dataBindContext.bindValue(widgetValue, modelValue, symbolUpdate, null);
					ControlDecorationSupport.create(bindValue, SWT.TOP | SWT.LEFT);
				}
			}
		} catch (Exception e) {
			LOGGER.error("Exception occured while databinding", e);
		}
	}
	
	/**
	 * Update button status.
	 *
	 * @param event the event
	 */
	@SuppressWarnings("rawtypes")
	private void updateButtonStatus(ValueChangeEvent event) {
		final String name = (String) event.getObservableValue().getValue();
		if (this.saveBtn != null) {
			if (XMSystemUtil.isEmpty(name) || name.trim().length() == 0
					|| (!name.matches(CommonConstants.RegularExpressions.ALLOWED_APP_NAME_REGEX))) {
				this.saveBtn.setEnabled(false);
			} else {
				this.saveBtn.setEnabled(true);
			}
		}
	}

	/**
	 * Gets the startApplication model.
	 *
	 * @return the startApplicationModel
	 */
	public StartApplication getStartApplicationModel() {
		return startApplicationModel;
	}

	/**
	 * Sets the startApplication model.
	 *
	 * @param startApplicationModel
	 *            the startApplication to set
	 */
	public void setStartApplicationModel(final StartApplication startApplicationModel) {
		this.startApplicationModel = startApplicationModel;
	}

	/**
	 * Add the listener to widgets.
	 */
	private void initListeners() {
		// Add listener to widgets
		if (this.descTranslateLink.getListeners(SWT.Selection).length == 0) {
			this.descTranslateLink.addSelectionListener(new SelectionAdapter() {
				
				@Override
				public void widgetSelected(final SelectionEvent event) {
					final Link linkWidget = (Link) event.widget;
					openDescDialog(linkWidget.getShell());
				}
			});
		}
		
		if (this.remarksTranslateLink.getListeners(SWT.Selection).length == 0) {
			// Event handling when users click on helptext lang links.
			this.remarksTranslateLink.addSelectionListener(new SelectionAdapter() {

				@Override
				public void widgetSelected(SelectionEvent event) {
					final Link linkWidget = (Link) event.widget;
					openRemarksDialog(linkWidget.getShell());
				}
			});
		}
		
		if (this.saveBtn != null && saveBtn.getListeners(SWT.Selection).length == 0) {
			this.saveBtn.addSelectionListener(new SelectionAdapter() {

				/**
				 * Save button handler
				 */
				@Override
				public void widgetSelected(final SelectionEvent event) {
					// CustomMessageDialog.openConfirm(getDisplay().getActiveShell(),
					// "API Error", "API is not ready");
					saveStartProAppHandler();
				}
			});
		}

		if (this.cancelBtn != null && cancelBtn.getListeners(SWT.Selection).length == 0) {
			this.cancelBtn.addSelectionListener(new SelectionAdapter() {

				/**
				 * Cancel button handler
				 */
				@Override
				public void widgetSelected(final SelectionEvent event) {
					cancelStartProAppHandler();
				}
			});
		}

		if (this.toolItem.getListeners(SWT.Selection).length == 0) {
			this.toolItem.addSelectionListener(new SelectionAdapter() {
				
				/**
				 * Symbol button handler
				 */
				@Override
				public void widgetSelected(final SelectionEvent event) {
					if ((boolean) toolItem.getData("editable")) {
						final ToolItem widget = (ToolItem) event.widget;
						final IconDialog dialog = new IconDialog(widget.getParent().getShell(),
								messages.browseIconDialogTitle, messages.icontableviewerSecondColumnLabel);
						
						final int returnVal = dialog.open();
						if (IDialogConstants.OK_ID == returnVal) {
							final Icon checkedIcon = dialog.getCheckedIcon();
							toolItem.setImage(XMSystemUtil.getInstance().getImage(this.getClass(), checkedIcon.getIconPath(), true, true));
							txtSymbol.setText(checkedIcon.getIconName());
							startApplicationModel.setIcon(checkedIcon);
						}
					}
				}
			});
		}
		if (this.txtRemarks.getListeners(SWT.Modify).length == 0) {
			this.txtRemarks.addModifyListener(new TextAreaModifyListener(this.lblRemarksCount, StartApplication.REMARK_LIMIT));
		}

		if (this.programAppToolItem.getListeners(SWT.Selection).length == 0) {
			this.programAppToolItem.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(final SelectionEvent event) {
					if ((boolean) programAppToolItem.getData("editable")) {
						
						final ToolItem widget = (ToolItem) event.widget;
						Map<String, IAdminTreeChild> baseApplications = AdminTreeFactory.getInstance().getBaseApplications()
								.getBaseApplications();
						
						List<IAdminTreeChild> list = new ArrayList<>(baseApplications.values());
						BrowseApplicationDialog scriptDialog = new BrowseApplicationDialog(widget.getParent().getShell(),
								messages, list, false, true);
						final int returnVal = scriptDialog.open();
						if (IDialogConstants.OK_ID == returnVal) {
							Object selectedBaseApp = scriptDialog.getSelectedApplication();
							if (selectedBaseApp != null && selectedBaseApp instanceof BaseApplication) {
								BaseApplication baseApplication = (BaseApplication) selectedBaseApp;
								startApplicationModel.setBaseApplicationId(baseApplication.getBaseApplicationId());
							} else {
								startApplicationModel.setBaseApplicationId(null);
							}
						}
					}
					updateBaseApplication();
				}
			});
		}
		
		if (this.txtBaseApplication.getListeners(SWT.Modify).length == 0) {
			this.txtBaseApplication.addModifyListener(new ModifyListener() {
				ControlDecoration txtBaseAppDecroator = new ControlDecoration(txtBaseApplication, SWT.TOP);
				
				@Override
				public void modifyText(ModifyEvent event) {
					final Image nameDecoratorImage = FieldDecorationRegistry.getDefault()
							.getFieldDecoration(FieldDecorationRegistry.DEC_ERROR).getImage();
					txtBaseAppDecroator.setImage(nameDecoratorImage);
					Text text = (Text) event.widget;
					if (!isMessageBtn.getSelection()) {
						if (text.getText().trim().length() <= 0) {
							txtBaseAppDecroator.setDescriptionText(messages.appBaseAppEmptyError);
							txtBaseAppDecroator.show();
						} else if (text.getText().trim().length() > 0) {
							txtBaseAppDecroator.hide();
						}
					} else {
						txtBaseAppDecroator.hide();
					}
				}
			});
		}
		
		this.txtRemarks.addVerifyListener(new VerifyListener() {

			@Override
			public void verifyText(VerifyEvent event) {
				String source = ((Text) event.widget).getText();
				final String remarkText = source.substring(0, event.start) + event.text + source.substring(event.end);
				int length = remarkText.length();
				if (length > StartApplication.REMARK_LIMIT) {
					event.doit = false;
				}
			}
		});
		if (this.isMessageBtn.getListeners(SWT.Selection).length == 0) {
			this.isMessageBtn.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent event) {
					Button button = (Button) event.widget;
					
					boolean isMessage = button.getSelection();
					if (isMessage) {
						cdtExpiryDate.setEnabled(true);
						txtBaseApplication.setText(CommonConstants.EMPTY_STR);
						txtBaseApplication.setEditable(false);
						programAppToolItem.setEnabled(false);
						startApplicationModel.setBaseApplicationId(null);
						if (!(PROGRAMMATICALLY.equals(event.data))) {
							CustomMessageDialog.openInformation(getShell(), messages.messageEnableInfoTitle,
									messages.messageEnableInfo);
						}
						notifyBaseAppText();
					} else {
						cdtExpiryDate.setEnabled(false);
						cdtExpiryDate.setDate(null);
						txtBaseApplication.setEditable(true);
						programAppToolItem.setEnabled(true);
						notifyBaseAppText();
					}
				}
			});
		}
	}
	
	/**
	 * Notify base app text.
	 */
	private void notifyBaseAppText() {
		Event event1 = new Event();
		event1.data = PROGRAMMATICALLY;
		this.txtBaseApplication.notifyListeners(SWT.Modify, event1);
	}

	/**txtBaseApplication
	 * Method for Update base application.
	 */
	public void updateBaseApplication() {
		if (this.startApplicationModel == null) {
			this.txtBaseApplication.setText(CommonConstants.EMPTY_STR);
			return;
		}
		AdminTreeFactory instance = AdminTreeFactory.getInstance();
		if (instance != null) {
			String applicationComboId = this.startApplicationModel.getBaseApplicationId();
			if (!XMSystemUtil.isEmpty(applicationComboId)) {
				Map<String, IAdminTreeChild> baseApplications = instance.getBaseApplications().getBaseApplications();
				BaseApplication baseApplication = (BaseApplication) baseApplications.get(applicationComboId);
				String name = baseApplication.getName();
				if (!XMSystemUtil.isEmpty(name)) {
					this.txtBaseApplication.setText(name);
				}
			} else {
				this.txtBaseApplication.setText(CommonConstants.EMPTY_STR);
			}
		}
	}

	/**
	 * Validates the model before submit
	 * 
	 * @return boolean
	 */
	private boolean validate() {
		if (!saveBtn.isEnabled()) {
			return false;
		}
		String startApplicationName = this.startApplicationModel.getName();
		Icon icon;
		if (XMSystemUtil.isEmpty(startApplicationName) && (icon = this.startApplicationModel.getIcon()) != null
				&& XMSystemUtil.isEmpty(icon.getIconName())) {
			CustomMessageDialog.openError(this.getShell(), messages.nameErrorTitle, messages.nameError);
			return false;
		}
		if ((icon = this.startApplicationModel.getIcon()) != null && XMSystemUtil.isEmpty(icon.getIconName())) {
			CustomMessageDialog.openError(this.getShell(), messages.symbolErrorTitle, messages.symbolError);
			return false;
		}
		final StartApplications startApplications = AdminTreeFactory.getInstance().getStartApplications();
		final Collection<IAdminTreeChild> startAppsCollection = startApplications.getStartAppCollection();

		if (!XMSystemUtil.isEmpty(startApplicationName)) {
			final Map<String, Long> result = startAppsCollection.parallelStream()
					.collect(Collectors.groupingBy(startApplication -> {
						if (!XMSystemUtil.isEmpty(((StartApplication) startApplication).getName())) {
							return ((StartApplication) startApplication).getName().toUpperCase();
						}
						return CommonConstants.EMPTY_STR;
					}, Collectors.counting()));
			if (result.containsKey(startApplicationName.toUpperCase())) {
				if ((this.startApplicationModel.getOperationMode() == CommonConstants.OPERATIONMODE.CHANGE
						&& !startApplicationName.equalsIgnoreCase(this.oldModel.getName()))
						|| this.startApplicationModel.getOperationMode() == CommonConstants.OPERATIONMODE.CREATE) {
					CustomMessageDialog.openError(this.getShell(), messages.appNameErrorTitle, messages.existingEnNameError);
					return false;
				}
			}
		}
		if ((!this.isMessageBtn.getSelection()) && XMSystemUtil.isEmpty(this.txtBaseApplication.getText())) {
			CustomMessageDialog.openError(this.getShell(), messages.appBaseAppErrorTitle,
					messages.appBaseAppEmptyError);
			return false;
		}
		if (this.isMessageBtn.getSelection() && this.cdtExpiryDate.getDate() == null) {
			CustomMessageDialog.openError(this.getShell(), messages.startAppExpiryDateNotSetErrorTitle,
					messages.startAppExpiryDateNotSetError);
			return false;
		}
		
		Date todaysDate = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.YYYY HH:mm:ss.SSS");
		Date EndDate = this.cdtExpiryDate.getDate();
		if (this.cdtExpiryDate.getDate() != null) {
			if (!(EndDate.after(todaysDate) || sdf.format(EndDate).equals(sdf.format(todaysDate)))) {

				CustomMessageDialog.openError(this.getShell(), messages.startAppExpiryDateNotSetErrorTitle,
						messages.liveMsgInvalidExpiryDateMsg);
				return false;
			}
		}

		return true;

	}

	/**
	 * Method register method function for translation.
	 *
	 * @param registry
	 *            {@link MessageRegistry}
	 */
	public void registerMessages(final MessageRegistry registry) {
		registry.register((text) -> {
			if (grpStartApplication != null && !grpStartApplication.isDisposed()) {
				grpStartApplication.setText(text);
			}
		}, (message) -> {
			if (startApplicationModel != null) {
				final String name = this.startApplicationModel.getName();
				if (startApplicationModel.getOperationMode() == CommonConstants.OPERATIONMODE.VIEW) {
					return message.objectGroupDisaplyLabel + " \'" + name + "\'";
				} else if (startApplicationModel.getOperationMode() == CommonConstants.OPERATIONMODE.CHANGE) {
					return message.objectGroupChangeLabel + " \'" + name + "\'";
				} else if (startApplicationModel.getOperationMode() == CommonConstants.OPERATIONMODE.CREATE) {
					return message.startAppGroupCreateLabel;
				}
			}
			return CommonConstants.EMPTY_STR;
		});
		registry.register((text) -> {
			if (lblName != null && !lblName.isDisposed()) {
				lblName.setText(text);
			}
		}, (message) -> {
			if (lblName != null && !lblName.isDisposed()) {
				return getUpdatedWidgetText(message.objectNameLabel, lblName);
			}
			return CommonConstants.EMPTY_STR;
		});
		registry.register((text) -> {
			if (lblDescrition != null && !lblDescrition.isDisposed()) {
				lblDescrition.setText(text);
			}
		}, (message) -> {
			if (lblDescrition != null && !lblDescrition.isDisposed()) {
				return getUpdatedWidgetText(message.objectDescriptionLabel, lblDescrition);
			}
			return CommonConstants.EMPTY_STR;
		});
		registry.register((text) -> {
			if (lblActive != null && !lblActive.isDisposed()) {
				lblActive.setText(text);
			}
		}, (message) -> {
			if (lblActive != null && !lblActive.isDisposed()) {
				return getUpdatedWidgetText(message.objectActiveLabel, lblActive);
			}
			return CommonConstants.EMPTY_STR;
		});
		registry.register((text) -> {
			if (lblIsMessage != null && !lblIsMessage.isDisposed()) {
				lblIsMessage.setText(text);
			}
		}, (message) -> {
			if (lblIsMessage != null && !lblIsMessage.isDisposed()) {
				return getUpdatedWidgetText(message.startMessageLabel, lblIsMessage);
			}
			return CommonConstants.EMPTY_STR;
		});
		registry.register((text) -> {
			if (lblExpiryDate != null && !lblExpiryDate.isDisposed()) {
				lblExpiryDate.setText(text);
			}
		}, (message) -> {
			if (lblExpiryDate != null && !lblExpiryDate.isDisposed()) {
				return getUpdatedWidgetText(message.expiryDateLabel, lblExpiryDate);
			}
			return CommonConstants.EMPTY_STR;
		});
		registry.register((text) -> {
			if (lblSymbol != null && !lblSymbol.isDisposed()) {
				lblSymbol.setText(text);
			}
		}, (message) -> {
			if (lblSymbol != null && !lblSymbol.isDisposed()) {
				return getUpdatedWidgetText(message.objectSymbolLabel, lblSymbol);
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (lblApplication != null && !lblApplication.isDisposed()) {
				lblApplication.setText(text);
			}
		}, (message) -> {
			if (lblApplication != null && !lblApplication.isDisposed()) {
				return getUpdatedWidgetText(message.objectApplicationLabel, lblApplication);
			}
			return CommonConstants.EMPTY_STR;
		});
		if (saveBtn != null) {
			registry.register((text) -> {
				if (saveBtn != null && !saveBtn.isDisposed()) {
					saveBtn.setText(text);
				}
			}, (message) -> {
				if (saveBtn != null && !saveBtn.isDisposed()) {
					return getUpdatedWidgetText(message.saveButtonText, saveBtn);
				}
				return CommonConstants.EMPTY_STR;
			});
		}
		if (cancelBtn != null) {
			registry.register((text) -> {
				if (cancelBtn != null && !cancelBtn.isDisposed()) {
					cancelBtn.setText(text);
				}
			}, (message) -> {
				if (cancelBtn != null && !cancelBtn.isDisposed()) {
					return getUpdatedWidgetText(message.cancelButtonText, cancelBtn);
				}
				return CommonConstants.EMPTY_STR;
			});
		}
		registry.register((text) -> {
			if (remarksTranslateLink != null && !remarksTranslateLink.isDisposed()) {
				remarksTranslateLink.setText(text);
			}
		}, (message) -> {
			if (remarksTranslateLink != null && !remarksTranslateLink.isDisposed()) {
				return getUpdatedWidgetText("<a>" + message.objectTranslationLinkText + "</a>", remarksTranslateLink);
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (remarksLabel != null && !remarksLabel.isDisposed()) {
				remarksLabel.setText(text);
			}
		}, (message) -> {
			if (remarksLabel != null && !remarksLabel.isDisposed()) {
				return getUpdatedWidgetText(message.objectRemarkLabel, remarksLabel);
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (descTranslateLink != null && !descTranslateLink.isDisposed()) {
				descTranslateLink.setText(text);
			}
		}, (message) -> {
			if (descTranslateLink != null && !descTranslateLink.isDisposed()) {
				return getUpdatedWidgetText("<a>" + message.objectTranslationLinkText + "</a>", descTranslateLink);
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (txtDesc != null && !txtDesc.isDisposed()) {
				txtDesc.setText(text);
				updateDescWidget();
			}
		}, (message) -> {
			if (this.startApplicationModel != null && txtDesc != null && !txtDesc.isDisposed()) {
				LANG_ENUM langEnum = XMAdminUtil.getInstance().getCurrentLocaleEnum();
				return this.startApplicationModel.getDescription(langEnum) == null ? CommonConstants.EMPTY_STR
						: this.startApplicationModel.getDescription(langEnum);
			}
			return CommonConstants.EMPTY_STR;
		});
		registry.register((text) -> {
			if (txtRemarks != null && !txtRemarks.isDisposed()) {
				txtRemarks.setText(text);
				updateRemarksWidget();
			}
		}, (message) -> {
			if (this.startApplicationModel != null && txtRemarks != null && !txtRemarks.isDisposed()) {
				LANG_ENUM langEnum = XMAdminUtil.getInstance().getCurrentLocaleEnum();
				return this.startApplicationModel.getRemarks(langEnum) == null ? CommonConstants.EMPTY_STR
						: this.startApplicationModel.getRemarks(langEnum);
			}
			return CommonConstants.EMPTY_STR;
		});
		registry.register((text) -> {
		}, (message) -> {
			if(cdtExpiryDate != null && !cdtExpiryDate.isDisposed()){
			LANG_ENUM currentLocaleEnum = XMAdminUtil.getInstance().getCurrentLocaleEnum();
			cdtExpiryDate.updateTooltipText(currentLocaleEnum.toString());
			}
			return CommonConstants.EMPTY_STR;
		});
	}

	/**
	 * Gets the new text based on new locale
	 * 
	 * @param message
	 *            {@link String}
	 * @param control
	 *            {@link Control}
	 * @return {@link String} new text
	 */
	private String getUpdatedWidgetText(String message, Control control) {
		control.requestLayout();
		control.getParent().redraw();
		control.getParent().getParent().update();
		control.getParent().getParent().getParent().update();
		return message;
	}

	/**
	 * Gets the old model.
	 *
	 * @return the oldModel
	 */
	public StartApplication getOldModel() {
		return oldModel;
	}

	/**
	 * Sets the old model.
	 *
	 * @param oldModel
	 *            the oldModel to set
	 */
	public void setOldModel(StartApplication oldModel) {
		this.oldModel = oldModel;
	}

	/**
	 * Method to open dialog
	 * 
	 * @param shell
	 */
	private void openDescDialog(final Shell shell) {

		if (startApplicationModel == null) {
			return;
		}
		if (startApplicationModel.getOperationMode() != CommonConstants.OPERATIONMODE.VIEW) {
			String text = txtDesc.getText();
			LANG_ENUM currentLocaleEnum = XMAdminUtil.getInstance().getCurrentLocaleEnum();
			startApplicationModel.setDescription(currentLocaleEnum, text);
		}
		Map<LANG_ENUM, String> obModelMap = new HashMap<>();
		obModelMap.put(LANG_ENUM.ENGLISH, this.startApplicationModel.getDescription(LANG_ENUM.ENGLISH));
		obModelMap.put(LANG_ENUM.GERMAN, this.startApplicationModel.getDescription(LANG_ENUM.GERMAN));
		boolean isEditable = txtDesc.getEditable();
		ControlModel controlModel = new ControlModel(this.messages.objectDescriptionLabel, obModelMap,
				StartApplication.DESC_LIMIT, false, isEditable);
		controlModel.initDefaultLabels(this.messages);
		final XMAdminLangTextDialog dialogArea = new XMAdminLangTextDialog(shell, controlModel);
		int retVal = dialogArea.open();
		if (retVal == IDialogConstants.OK_ID) {
			Map<LANG_ENUM, String> descriptionMap = this.startApplicationModel.getDescriptionMap();
			descriptionMap.put(LANG_ENUM.ENGLISH, controlModel.getObjectModel(LANG_ENUM.ENGLISH));
			descriptionMap.put(LANG_ENUM.GERMAN, controlModel.getObjectModel(LANG_ENUM.GERMAN));
			updateDescWidget();
		}
	}

	/**
	 * Method to open dialog
	 * 
	 * @param shell
	 */
	private void openRemarksDialog(final Shell shell) {
		if (startApplicationModel == null) {
			return;
		}
		if (startApplicationModel.getOperationMode() != CommonConstants.OPERATIONMODE.VIEW) {
			String text = txtRemarks.getText();
			LANG_ENUM currentLocaleEnum = XMAdminUtil.getInstance().getCurrentLocaleEnum();
			startApplicationModel.setRemarks(currentLocaleEnum, text);
		}

		Map<LANG_ENUM, String> obModelMap = new HashMap<>();
		obModelMap.put(LANG_ENUM.ENGLISH, this.startApplicationModel.getRemarks(LANG_ENUM.ENGLISH));
		obModelMap.put(LANG_ENUM.GERMAN, this.startApplicationModel.getRemarks(LANG_ENUM.GERMAN));
		boolean isEditable = txtRemarks.getEditable();

		ControlModel controlModel = new ControlModel(this.messages.objectRemarkLabel, obModelMap,
				StartApplication.REMARK_LIMIT, false, isEditable);
		controlModel.initDefaultLabels(this.messages);
		final XMAdminLangTextAreaDialog dialogArea = new XMAdminLangTextAreaDialog(shell, controlModel);
		int retVal = dialogArea.open();
		if (retVal == IDialogConstants.OK_ID) {
			Map<LANG_ENUM, String> remarksMap = this.startApplicationModel.getRemarksMap();
			remarksMap.put(LANG_ENUM.ENGLISH, controlModel.getObjectModel(LANG_ENUM.ENGLISH));
			remarksMap.put(LANG_ENUM.GERMAN, controlModel.getObjectModel(LANG_ENUM.GERMAN));
			updateRemarksWidget();
		}
	}

	/**
	 * Method for Creates the site operation.
	 */
	private void createStartAppOperation() {
		try {
			StartAppController startAppController = new StartAppController();
			StartApplicationsTbl startAppVo = startAppController.createStartApplication(mapVOObjectWithModel());
			boolean isParent = this.isMessageBtn.getSelection();
			AdminTreeFactory instance = AdminTreeFactory.getInstance();
			String messageUserApp = this.startApplicationModel.getBaseApplicationId();
			String startApplicationId = startAppVo.getStartApplicationId();
			if (!XMSystemUtil.isEmpty(startApplicationId)) {
				startApplicationModel.setStartPrgmApplicationId(startApplicationId);
				Collection<StartAppTranslationTbl> startAppTranslationTblCollection = startAppVo
						.getStartAppTranslationTblCollection();
				for (StartAppTranslationTbl startAppTranslationTbl : startAppTranslationTblCollection) {
					String startAppTranslationId = startAppTranslationTbl.getStartAppTranslationId();
					LanguagesTbl languageCode = startAppTranslationTbl.getLanguageCode();
					LANG_ENUM langEnum = LANG_ENUM.getLangEnum(languageCode.getLanguageCode());
					this.startApplicationModel.setTranslationId(langEnum, startAppTranslationId);
				}
				// Attach this to tree
				if (oldModel == null && (isParent || messageUserApp != null)) { 
					setOldModel(startApplicationModel.deepCopyStartApplication(false, null));
					instance.getStartApplications().add(startApplicationId, getOldModel());
				}
				this.dirty.setDirty(false);
				AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
				adminTree.refresh(true);

				adminTree.setSelection(new StructuredSelection(instance.getApplications()), true);
				TreePath[] selectionPaths = adminTree.getStructuredSelection().getPaths();
				if (selectionPaths != null && selectionPaths.length > 0) {
					adminTree.setExpandedState(selectionPaths[0], true);
				}
				adminTree.setSelection(new StructuredSelection(instance.getStartApplications()), true);
				selectionPaths = adminTree.getStructuredSelection().getPaths();
				if (selectionPaths != null && selectionPaths.length > 0) {
					adminTree.setExpandedState(selectionPaths[0], true);
				}
				adminTree.setSelection(new StructuredSelection(getOldModel()), true);
				XMAdminUtil.getInstance()
						.updateLogFile(messages.startApplicationObject + " " + "'"
								+ this.startApplicationModel.getName() + "'" + " " + messages.objectCreate,
								MessageType.SUCCESS);
			}

		} catch (UnauthorizedAccessException e) {
			CustomMessageDialog.openError(this.getShell(), messages.objectPermissionDialogTitle,
					messages.objectPermissionDialogMsg);
		}catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				CustomMessageDialog.openError(Display.getCurrent().getActiveShell(), messages.errorDialogTitile,
						messages.serverNotReachable);
			}
			LOGGER.warn("Unable to Save Start Application data ! " + e);
		}
	}

	/**
	 * Method for Change start app operation.
	 */
	private void changeStartAppOperation() {
		try {
			StartAppController startAppController = new StartAppController();
			boolean isUpdated = startAppController.updateStartApplication(mapVOObjectWithModel());
			if (isUpdated) {
				setOldModel(startApplicationModel.deepCopyStartApplication(true, getOldModel()));
				this.startApplicationModel.setOperationMode(CommonConstants.OPERATIONMODE.VIEW);
				setOperationMode();
				this.dirty.setDirty(false);
				setShowButtonBar(false);
				final StartApplications startApplications = AdminTreeFactory.getInstance().getStartApplications();
				startApplications.sort();
				XMAdminUtil.getInstance().getAdminTree().refresh(true);
				XMAdminUtil.getInstance().getAdminTree().setSelection(new StructuredSelection(getOldModel()), true);
				XMAdminUtil.getInstance()
						.updateLogFile(messages.startApplicationObject + " " + "'"
								+ this.startApplicationModel.getName() + "'" + " " + messages.objectUpdate,
								MessageType.SUCCESS);
			}
		} catch (UnauthorizedAccessException e) {
			CustomMessageDialog.openError(this.getShell(), messages.objectPermissionDialogTitle,
					messages.objectPermissionDialogMsg);
		}catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				CustomMessageDialog.openError(Display.getCurrent().getActiveShell(), messages.errorDialogTitile,
						messages.serverNotReachable);
			}
			LOGGER.warn("Unable to Update data ! " + e);
		}
	}

	/**
	 * Map VO object with model.
	 *
	 * @return the com.magna.xmbackend.vo.start application. start application request
	 */
	private com.magna.xmbackend.vo.startApplication.StartApplicationRequest mapVOObjectWithModel() {
		com.magna.xmbackend.vo.startApplication.StartApplicationRequest startApplicationRequest = new com.magna.xmbackend.vo.startApplication.StartApplicationRequest();

		startApplicationRequest.setId(startApplicationModel.getStartPrgmApplicationId());
		startApplicationRequest.setName(startApplicationModel.getName());
		startApplicationRequest.setIconId(startApplicationModel.getIcon().getIconId());
		startApplicationRequest.setStatus(startApplicationModel.isActive() == true
				? com.magna.xmbackend.vo.enums.Status.ACTIVE.name() : com.magna.xmbackend.vo.enums.Status.INACTIVE.name());
		startApplicationRequest.setIsMessage(startApplicationModel.isMessage());
		Date expiryDate;
		if (startApplicationModel.isMessage() && (expiryDate = startApplicationModel.getExpiryDate()) != null) {
				startApplicationRequest.setStartMsgExpiryDate(expiryDate);
		}
		startApplicationRequest.setBaseAppId(startApplicationModel.getBaseApplicationId());

		List<com.magna.xmbackend.vo.startApplication.StartApplicationTranslation> startAppTransulationList = new ArrayList<>();
		LANG_ENUM[] lang_values = LANG_ENUM.values();
		for (int index = 0; index < lang_values.length; index++) {
			com.magna.xmbackend.vo.startApplication.StartApplicationTranslation startAppTransulation = new com.magna.xmbackend.vo.startApplication.StartApplicationTranslation();
			startAppTransulation.setLanguageCode(lang_values[index].getLangCode());
			startAppTransulation.setRemarks(startApplicationModel.getRemarks(lang_values[index]));
			startAppTransulation.setDescription(startApplicationModel.getDescription(lang_values[index]));
			if (this.startApplicationModel.getOperationMode() == CommonConstants.OPERATIONMODE.CHANGE) {
				String translationId = startApplicationModel.getTranslationId(lang_values[index]);
				startAppTransulation.setId(XMSystemUtil.isEmpty(translationId) ? CommonConstants.EMPTY_STR : translationId);
			}
			startAppTransulationList.add(startAppTransulation);
		}
		startApplicationRequest.setStartApplicationTranslations(startAppTransulationList);

		return startApplicationRequest;
	}

	/**
	 * Method for Sets the operation mode.
	 */
	public void setOperationMode() {
		if (this.startApplicationModel != null) {
			if (startApplicationModel.getOperationMode() == CommonConstants.OPERATIONMODE.VIEW) {
				this.txtName.setEditable(false);
				this.txtDesc.setEditable(false);
				this.txtRemarks.setEditable(false);
				this.activeBtn.setEnabled(false);
				this.isMessageBtn.setEnabled(false);
				this.cdtExpiryDate.setEnabled(false);
				this.txtBaseApplication.setEditable(false);
				this.programAppToolItem.setEnabled(false);
				setShowButtonBar(false);
				this.toolItem.setData("editable", false);
				this.programAppToolItem.setData("editable", false);
			} else if (startApplicationModel.getOperationMode() == CommonConstants.OPERATIONMODE.CREATE) {
				this.saveBtn.setEnabled(false);
				this.txtName.setEditable(true);
				this.txtDesc.setEditable(true);
				this.txtRemarks.setEditable(true);
				this.activeBtn.setEnabled(true);
				this.isMessageBtn.setEnabled(true);
				this.cdtExpiryDate.setEnabled(this.isMessageBtn.getSelection());
				this.toolItem.setData("editable", true);
				this.programAppToolItem.setData("editable", true);
				this.programAppToolItem.setEnabled(true);
				this.programAppToolItem.setEnabled(!this.isMessageBtn.getSelection());
				this.txtBaseApplication.setEditable(false);
				this.dirty.setDirty(true);
				setShowButtonBar(true);
			} else if (startApplicationModel.getOperationMode() == CommonConstants.OPERATIONMODE.CHANGE) {
				this.txtName.setEditable(true);
				this.txtDesc.setEditable(true);
				this.txtRemarks.setEditable(true);
				this.activeBtn.setEnabled(true);
				this.isMessageBtn.setEnabled(true);
				this.cdtExpiryDate.setEnabled(this.isMessageBtn.getSelection());
				this.toolItem.setData("editable", true);
				this.programAppToolItem.setData("editable", true);
				this.programAppToolItem.setEnabled(!this.isMessageBtn.getSelection());
				this.txtBaseApplication.setEditable(false);
				setShowButtonBar(true);
				this.dirty.setDirty(true);
			} else {
				this.txtName.setEditable(false);
				this.txtDesc.setEditable(false);
				this.txtRemarks.setEditable(false);
				this.activeBtn.setEnabled(false);
				this.isMessageBtn.setEnabled(false);
				this.cdtExpiryDate.setEnabled(false);
				this.toolItem.setData("editable", false);
				this.programAppToolItem.setData("editable", false);
				this.programAppToolItem.setEnabled(this.isMessageBtn.getSelection());
				this.txtBaseApplication.setEditable(false);
				setShowButtonBar(false);
			}
		}
	}

	/**
	 * Save Handler
	 */
	public void saveStartProAppHandler() {
		saveDescExpiryDateAndRemarks();
		// validate the model
		if (validate()) { // check name map values
			if (startApplicationModel.getOperationMode() == CommonConstants.OPERATIONMODE.CREATE) {
				createStartAppOperation();
			} else if (startApplicationModel.getOperationMode() == CommonConstants.OPERATIONMODE.CHANGE) {
				changeStartAppOperation();
			}
		}
	}

	/**
	 * Save desc and remarks.
	 */
	private void saveDescExpiryDateAndRemarks() {
		final LANG_ENUM currentLocaleEnum = XMAdminUtil.getInstance().getCurrentLocaleEnum();
		final String desc = txtDesc.getText();
		startApplicationModel.setDescription(currentLocaleEnum, desc);
		final String remarks = txtRemarks.getText();
		startApplicationModel.setRemarks(currentLocaleEnum, remarks);
		boolean isMessage = isMessageBtn.getSelection();
		startApplicationModel.setMessage(isMessage);
		if (isMessage) {
			Date expiryDate;
			if ((expiryDate = cdtExpiryDate.getDate()) != null) {
				startApplicationModel.setExpiryDate(expiryDate);
			}
		}
	}

	/**
	 * set dirty
	 * 
	 * @param dirty
	 */
	@Persist
	public void setDirtyObject(final MDirtyable dirty) {
		this.dirty = dirty;
	}

	/**
	 * method for update help text
	 */
	public void updateRemarksWidget() {
		if (this.startApplicationModel == null) {
			return;
		}
		int operationMode = this.startApplicationModel.getOperationMode();
		final LANG_ENUM currentLocaleEnum = XMAdminUtil.getInstance().getCurrentLocaleEnum();

		String remarkForCurLocale = this.startApplicationModel.getRemarks(currentLocaleEnum) == null
				? CommonConstants.EMPTY_STR : this.startApplicationModel.getRemarks(currentLocaleEnum);
		if (operationMode != CommonConstants.OPERATIONMODE.VIEW) {
			this.txtRemarks.setText(remarkForCurLocale);
			return;
		}
		if (operationMode == CommonConstants.OPERATIONMODE.VIEW) {
			if (!XMSystemUtil.isEmpty(remarkForCurLocale)) {
				this.txtRemarks.setText(remarkForCurLocale);
				return;
			}
			final String remarkEN = this.startApplicationModel.getRemarks(LANG_ENUM.ENGLISH);
			final String remarkDE = this.startApplicationModel.getRemarks(LANG_ENUM.GERMAN);
			if (!XMSystemUtil.isEmpty(remarkEN)) {
				this.txtRemarks.setText(remarkEN);
				return;
			}

			if (!XMSystemUtil.isEmpty(remarkDE)) {
				this.txtRemarks.setText(remarkDE);
				return;
			}
		}
	}

	/**
	 * Inits the widgets.
	 */
	public void updateDescWidget() {
		if (this.startApplicationModel == null) {
			return;
		}
		int operationMode = this.startApplicationModel.getOperationMode();
		LANG_ENUM currentLocaleEnum = XMAdminUtil.getInstance().getCurrentLocaleEnum();
		String descForCurLocale = this.startApplicationModel.getDescription(currentLocaleEnum) == null
				? CommonConstants.EMPTY_STR : this.startApplicationModel.getDescription(currentLocaleEnum);
		if (operationMode != CommonConstants.OPERATIONMODE.VIEW) {
			this.txtDesc.setText(descForCurLocale);
			return;
		}
		if (operationMode == CommonConstants.OPERATIONMODE.VIEW) {
			if (!XMSystemUtil.isEmpty(descForCurLocale)) {
				this.txtDesc.setText(descForCurLocale);
				return;
			}
			final String descriptionEN = this.startApplicationModel.getDescription(LANG_ENUM.ENGLISH);
			final String descriptionDE = this.startApplicationModel.getDescription(LANG_ENUM.GERMAN);
			if (!XMSystemUtil.isEmpty(descriptionEN)) {
				this.txtDesc.setText(descriptionEN);
				return;
			}

			if (!XMSystemUtil.isEmpty(descriptionDE)) {
				this.txtDesc.setText(descriptionDE);
				return;
			}
		}

	}

	/**
	 * Cancel start handler.
	 */
	public void cancelStartProAppHandler() {
		if (startApplicationModel == null) {
			dirty.setDirty(false);
			return;
		}
		String startProAppId = CommonConstants.EMPTY_STR;
		int operationMode = this.startApplicationModel.getOperationMode();
		StartApplication oldModel = getOldModel();
		if (oldModel != null) {
			startProAppId = oldModel.getStartPrgmApplicationId();
		}
		setStartApplicationModel(null);
		setOldModel(null);
		this.saveBtn.setEnabled(true);
		final AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
		AdminTreeFactory instance = AdminTreeFactory.getInstance();
		final StartApplications startApplications = instance.getStartApplications();
		dirty.setDirty(false);
		if (operationMode == CommonConstants.OPERATIONMODE.CHANGE) {
			final IStructuredSelection selection = (IStructuredSelection) adminTree.getSelection();
			final Object firstElement = selection.getFirstElement();
			Map<String, IAdminTreeChild> startApplicationsObj = startApplications.getStartApplications();
			if (firstElement != null && firstElement.equals(startApplicationsObj.get(startProAppId))) {
				adminTree.setSelection(new StructuredSelection(startApplicationsObj.get(startProAppId)), true);
			}
		} else {
			if (!adminTree.getExpandedState(instance.getApplications())) {
				adminTree.setSelection(new StructuredSelection(instance.getApplications()), true);
			} else {
				adminTree.setSelection(new StructuredSelection(startApplications), true);
			}
		}
	}

	/**
	 * set the side model from selection.
	 */
	public void setStartApplication() {
		try {
			final AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
			final Object selectionObj = adminTree.getSelection();
			if (selectionObj instanceof IStructuredSelection) {
				Object firstElement = ((IStructuredSelection) selectionObj).getFirstElement();
				if (firstElement instanceof StartApplication) {
					setOldModel((StartApplication) firstElement);
					StartApplication rightHandObject = getOldModel().deepCopyStartApplication(false, null);
					setStartApplicationModel(rightHandObject);
					this.isMessageBtn.setSelection(rightHandObject.isMessage());
					bindValues();
					registerMessages(this.registry);
					setOperationMode();
					updateExpiryDate();
					updateBaseApplication();
					
					//initListeners();
				}
			}
		} catch (Exception e) {
			LOGGER.warn("Unable to set start application model selection ! " + e);
		}
	}

	/**
	 * Sets the model.
	 *
	 * @param startApplication
	 *            the new model
	 */
	public void setModel(StartApplication startApplication) {
		try {
			setOldModel(null);
			setStartApplicationModel(startApplication);
			registerMessages(this.registry);
			this.isMessageBtn.setSelection(startApplication.isMessage());
			bindValues();
			setOperationMode();
			updateExpiryDate();
			updateDescWidget();
			updateRemarksWidget();
			updateBaseApplication();
		} catch (Exception e) {
			LOGGER.warn("Unable to set start model ! " + e);
		}
	}

	private void updateExpiryDate() {
		if (this.startApplicationModel == null) {
			return;
		}
		this.isMessageBtn.setSelection(this.startApplicationModel.isMessage());
		if (this.startApplicationModel.isMessage()) {
			Date expiryDate;
			if ((expiryDate = this.startApplicationModel.getExpiryDate()) != null) {
				this.cdtExpiryDate.setDate(expiryDate);
				return;
			}
		}
		this.cdtExpiryDate.setDate(null);
	}
}
