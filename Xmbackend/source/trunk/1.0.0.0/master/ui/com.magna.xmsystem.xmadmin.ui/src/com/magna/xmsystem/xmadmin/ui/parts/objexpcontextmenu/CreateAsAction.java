package com.magna.xmsystem.xmadmin.ui.parts.objexpcontextmenu;

import javax.inject.Inject;

import org.eclipse.core.commands.ParameterizedCommand;
import org.eclipse.e4.core.commands.ECommandService;
import org.eclipse.e4.core.commands.EHandlerService;
import org.eclipse.e4.core.services.nls.Translation;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreePath;

import com.magna.xmsystem.xmadmin.message.Message;
import com.magna.xmsystem.xmadmin.ui.parts.InformationPart;
import com.magna.xmsystem.xmadmin.ui.parts.objexp.ObjectExpPage;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.AdminTreeviewer;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdministrationArea;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.baseapplicationmodel.BaseApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.projectapplicationmodel.ProjectApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.startapplicationmodel.StartApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.userapplicationmodel.UserApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.livemsgconfig.LiveMessage;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.NotificationTemplate;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.ProjectActivateEvtAction;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.ProjectCreateEvtAction;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.ProjectDeactivateEvtAction;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.ProjectDeleteEvtAction;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.UserProjectRelAssignEvtAction;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.UserProjectRelRemoveEvtAction;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.Role;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.directory.Directory;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.projectappgroup.ProjectApplicationGroup;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.projectgroup.ProjectGroupModel;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.userappgroup.UserApplicationGroup;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.usergroup.UserGroupModel;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.Project;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.Site;
import com.magna.xmsystem.xmadmin.util.CommonConstants;
import com.magna.xmsystem.xmadmin.util.XMAdminUtil;

// TODO: Auto-generated Javadoc
/**
 * The Class CreateAsAction.
 */
@SuppressWarnings("restriction")
public class CreateAsAction extends Action {

	/** The messages. */
	@Inject
	@Translation
	private Message messages;

	/** The handler service. */
	@Inject
	private EHandlerService handlerService;

	/** The command service. */
	@Inject
	private ECommandService commandService;
	
	/** Inject of {@link EModelService}. */
	@Inject
	private EModelService modelService;

	/** Inject of {@link MApplication}. */
	@Inject
	private MApplication application;
	

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.action.Action#run()
	 */
	@Override
	public void run() {
		MPart mPart = (MPart) this.modelService.find(CommonConstants.PART_ID.INFORMATION_PART_ID, application);
		InformationPart rightSidePart2 = (InformationPart) mPart.getObject();
		ObjectExpPage objectExpPage = rightSidePart2.getObjectExpPartUI();
		IStructuredSelection selections = (IStructuredSelection) objectExpPage.getObjExpTableViewer().getSelection();
		Object selectionObj = selections.getFirstElement();
		AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
		if (selectionObj instanceof Site) {
			TreePath[] selectionPaths = adminTree.getStructuredSelection().getPaths();
			if (selectionPaths != null && selectionPaths.length > 0) {
				adminTree.setExpandedState(selectionPaths[0], true);
			}
			adminTree.setSelection(new StructuredSelection(selectionObj));
			ParameterizedCommand cmd = commandService.createCommand(CommonConstants.COMMAND_ID.CREATEAS_SITE, null);
			if (handlerService.canExecute(cmd)) {
				handlerService.executeHandler(cmd);
			}
		} else if (selectionObj instanceof AdministrationArea) {
			TreePath[] selectionPaths = adminTree.getStructuredSelection().getPaths();
			if (selectionPaths != null && selectionPaths.length > 0) {
				adminTree.setExpandedState(selectionPaths[0], true);
			}
			adminTree.setSelection(new StructuredSelection(selectionObj));
			ParameterizedCommand cmd = commandService.createCommand(CommonConstants.COMMAND_ID.CREATEAS_ADMINAREA,
					null);
			if (handlerService.canExecute(cmd)) {
				handlerService.executeHandler(cmd);
			}
		} else if (selectionObj instanceof Project) {
			TreePath[] selectionPaths = adminTree.getStructuredSelection().getPaths();
			if (selectionPaths != null && selectionPaths.length > 0) {
				adminTree.setExpandedState(selectionPaths[0], true);
			}
			adminTree.setSelection(new StructuredSelection(selectionObj));
			ParameterizedCommand cmd = commandService.createCommand(CommonConstants.COMMAND_ID.CREATEAS_PROJECT, null);
			if (handlerService.canExecute(cmd)) {
				handlerService.executeHandler(cmd);
			}
		} else if (selectionObj instanceof UserApplication) {
			TreePath[] selectionPaths = adminTree.getStructuredSelection().getPaths();
			if (selectionPaths != null && selectionPaths.length > 0) {
				adminTree.setExpandedState(selectionPaths[0], true);
			}
			adminTree.setSelection(new StructuredSelection(selectionObj));
			ParameterizedCommand cmd = commandService.createCommand(CommonConstants.COMMAND_ID.CREATEAS_USERAPP, null);
			if (handlerService.canExecute(cmd)) {
				handlerService.executeHandler(cmd);
			}
		} else if (selectionObj instanceof ProjectApplication) {
			TreePath[] selectionPaths = adminTree.getStructuredSelection().getPaths();
			if (selectionPaths != null && selectionPaths.length > 0) {
				adminTree.setExpandedState(selectionPaths[0], true);
			}
			adminTree.setSelection(new StructuredSelection(selectionObj));
			ParameterizedCommand cmd = commandService.createCommand(CommonConstants.COMMAND_ID.CREATEAS_PROJECTAPP,
					null);
			if (handlerService.canExecute(cmd)) {
				handlerService.executeHandler(cmd);
			}
		} else if (selectionObj instanceof StartApplication) {
			TreePath[] selectionPaths = adminTree.getStructuredSelection().getPaths();
			if (selectionPaths != null && selectionPaths.length > 0) {
				adminTree.setExpandedState(selectionPaths[0], true);
			}
			adminTree.setSelection(new StructuredSelection(selectionObj));
			ParameterizedCommand cmd = commandService.createCommand(CommonConstants.COMMAND_ID.CREATEAS_STARTAPP, null);
			if (handlerService.canExecute(cmd)) {
				handlerService.executeHandler(cmd);
			}
		} else if (selectionObj instanceof BaseApplication) {
			TreePath[] selectionPaths = adminTree.getStructuredSelection().getPaths();
			if (selectionPaths != null && selectionPaths.length > 0) {
				adminTree.setExpandedState(selectionPaths[0], true);
			}
			adminTree.setSelection(new StructuredSelection(selectionObj));
			ParameterizedCommand cmd = commandService.createCommand(CommonConstants.COMMAND_ID.CREATEAS_BASEAPP, null);
			if (handlerService.canExecute(cmd)) {
				handlerService.executeHandler(cmd);
			}
		} else if (selectionObj instanceof Role) {
			TreePath[] selectionPaths = adminTree.getStructuredSelection().getPaths();
			if (selectionPaths != null && selectionPaths.length > 0) {
				adminTree.setExpandedState(selectionPaths[0], true);
			}
			adminTree.setSelection(new StructuredSelection(selectionObj));
			ParameterizedCommand cmd = commandService.createCommand(CommonConstants.COMMAND_ID.CREATEAS_ROLE, null);
			if (handlerService.canExecute(cmd)) {
				handlerService.executeHandler(cmd);
			}
		} else if (selectionObj instanceof UserGroupModel) {
			TreePath[] selectionPaths = adminTree.getStructuredSelection().getPaths();
			if (selectionPaths != null && selectionPaths.length > 0) {
				adminTree.setExpandedState(selectionPaths[0], true);
			}
			adminTree.setSelection(new StructuredSelection(selectionObj));
			ParameterizedCommand cmd = commandService.createCommand(CommonConstants.COMMAND_ID.CREATEAS_USER_GROUP,
					null);
			if (handlerService.canExecute(cmd)) {
				handlerService.executeHandler(cmd);
			}
		} else if (selectionObj instanceof ProjectGroupModel) {
			TreePath[] selectionPaths = adminTree.getStructuredSelection().getPaths();
			if (selectionPaths != null && selectionPaths.length > 0) {
				adminTree.setExpandedState(selectionPaths[0], true);
			}
			adminTree.setSelection(new StructuredSelection(selectionObj));
			ParameterizedCommand cmd = commandService.createCommand(CommonConstants.COMMAND_ID.CREATEAS_PROJECT_GROUP,
					null);
			if (handlerService.canExecute(cmd)) {
				handlerService.executeHandler(cmd);
			}
		} else if (selectionObj instanceof UserApplicationGroup) {
			TreePath[] selectionPaths = adminTree.getStructuredSelection().getPaths();
			if (selectionPaths != null && selectionPaths.length > 0) {
				adminTree.setExpandedState(selectionPaths[0], true);
			}
			adminTree.setSelection(new StructuredSelection(selectionObj));
			ParameterizedCommand cmd = commandService.createCommand(CommonConstants.COMMAND_ID.CREATEAS_USERAPP_GROUP,
					null);
			if (handlerService.canExecute(cmd)) {
				handlerService.executeHandler(cmd);
			}
		} else if (selectionObj instanceof ProjectApplicationGroup) {
			TreePath[] selectionPaths = adminTree.getStructuredSelection().getPaths();
			if (selectionPaths != null && selectionPaths.length > 0) {
				adminTree.setExpandedState(selectionPaths[0], true);
			}
			adminTree.setSelection(new StructuredSelection(selectionObj));
			ParameterizedCommand cmd = commandService
					.createCommand(CommonConstants.COMMAND_ID.CREATEAS_PROJECTAPP_GROUP, null);
			if (handlerService.canExecute(cmd)) {
				handlerService.executeHandler(cmd);
			}
		} else if (selectionObj instanceof Directory) {
			TreePath[] selectionPaths = adminTree.getStructuredSelection().getPaths();
			if (selectionPaths != null && selectionPaths.length > 0) {
				adminTree.setExpandedState(selectionPaths[0], true);
			}
			adminTree.setSelection(new StructuredSelection(selectionObj));
			ParameterizedCommand cmd = commandService.createCommand(CommonConstants.COMMAND_ID.CREATEAS_DIRECTORY,
					null);
			if (handlerService.canExecute(cmd)) {
				handlerService.executeHandler(cmd);
			}
		} else if (selectionObj instanceof LiveMessage) {
			TreePath[] selectionPaths = adminTree.getStructuredSelection().getPaths();
			if (selectionPaths != null && selectionPaths.length > 0) {
				adminTree.setExpandedState(selectionPaths[0], true);
			}
			adminTree.setSelection(new StructuredSelection(selectionObj));
			ParameterizedCommand cmd = commandService.createCommand(CommonConstants.COMMAND_ID.CREATEAS_LIVEMESSAGE,
					null);
			if (handlerService.canExecute(cmd)) {
				handlerService.executeHandler(cmd);
			}
		} else if (selectionObj instanceof ProjectCreateEvtAction) {
			TreePath[] selectionPaths = adminTree.getStructuredSelection().getPaths();
			if (selectionPaths != null && selectionPaths.length > 0) {
				adminTree.setExpandedState(selectionPaths[0], true);
			}
			adminTree.setSelection(new StructuredSelection(selectionObj));
			ParameterizedCommand cmd = commandService
					.createCommand(CommonConstants.COMMAND_ID.CREATEAS_PRO_CREATE_ACTION, null);
			if (handlerService.canExecute(cmd)) {
				handlerService.executeHandler(cmd);
			}
		} else if (selectionObj instanceof ProjectDeleteEvtAction) {
			TreePath[] selectionPaths = adminTree.getStructuredSelection().getPaths();
			if (selectionPaths != null && selectionPaths.length > 0) {
				adminTree.setExpandedState(selectionPaths[0], true);
			}
			adminTree.setSelection(new StructuredSelection(selectionObj));
			ParameterizedCommand cmd = commandService
					.createCommand(CommonConstants.COMMAND_ID.CREATEAS_PRO_DELETE_ACTION, null);
			if (handlerService.canExecute(cmd)) {
				handlerService.executeHandler(cmd);
			}
		} else if (selectionObj instanceof ProjectDeactivateEvtAction) {
			TreePath[] selectionPaths = adminTree.getStructuredSelection().getPaths();
			if (selectionPaths != null && selectionPaths.length > 0) {
				adminTree.setExpandedState(selectionPaths[0], true);
			}
			adminTree.setSelection(new StructuredSelection(selectionObj));
			ParameterizedCommand cmd = commandService
					.createCommand(CommonConstants.COMMAND_ID.CREATEAS_PRO_DEACTIVATE_ACTION, null);
			if (handlerService.canExecute(cmd)) {
				handlerService.executeHandler(cmd);
			}
		} else if (selectionObj instanceof ProjectActivateEvtAction) {
			TreePath[] selectionPaths = adminTree.getStructuredSelection().getPaths();
			if (selectionPaths != null && selectionPaths.length > 0) {
				adminTree.setExpandedState(selectionPaths[0], true);
			}
			adminTree.setSelection(new StructuredSelection(selectionObj));
			ParameterizedCommand cmd = commandService
					.createCommand(CommonConstants.COMMAND_ID.CREATEAS_PRO_ACTIVATE_ACTION, null);
			if (handlerService.canExecute(cmd)) {
				handlerService.executeHandler(cmd);
			}
		} else if (selectionObj instanceof UserProjectRelAssignEvtAction) {
			TreePath[] selectionPaths = adminTree.getStructuredSelection().getPaths();
			if (selectionPaths != null && selectionPaths.length > 0) {
				adminTree.setExpandedState(selectionPaths[0], true);
			}
			adminTree.setSelection(new StructuredSelection(selectionObj));
			ParameterizedCommand cmd = commandService
					.createCommand(CommonConstants.COMMAND_ID.CREATEAS_USER_PRO_REL_ASSIGN_ACTION, null);
			if (handlerService.canExecute(cmd)) {
				handlerService.executeHandler(cmd);
			}
		} else if (selectionObj instanceof UserProjectRelRemoveEvtAction) {
			TreePath[] selectionPaths = adminTree.getStructuredSelection().getPaths();
			if (selectionPaths != null && selectionPaths.length > 0) {
				adminTree.setExpandedState(selectionPaths[0], true);
			}
			adminTree.setSelection(new StructuredSelection(selectionObj));
			ParameterizedCommand cmd = commandService
					.createCommand(CommonConstants.COMMAND_ID.CREATEAS_USER_PRO_REL_REMOVE_ACTION, null);
			if (handlerService.canExecute(cmd)) {
				handlerService.executeHandler(cmd);
			}
		} else if (selectionObj instanceof NotificationTemplate) {
			TreePath[] selectionPaths = adminTree.getStructuredSelection().getPaths();
			if (selectionPaths != null && selectionPaths.length > 0) {
				adminTree.setExpandedState(selectionPaths[0], true);
			}
			adminTree.setSelection(new StructuredSelection(selectionObj));
			ParameterizedCommand cmd = commandService.createCommand(CommonConstants.COMMAND_ID.CREATEAS_TEMPLATE, null);
			if (handlerService.canExecute(cmd)) {
				handlerService.executeHandler(cmd);
			}
		}
	}

}
