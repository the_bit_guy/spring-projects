package com.magna.xmsystem.xmadmin.ui.userreviewdialogs;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.e4.core.services.nls.Translation;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.IMessageProvider;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.jface.layout.TableColumnLayout;
import org.eclipse.jface.viewers.CheckboxTableViewer;
import org.eclipse.jface.viewers.ColumnWeightData;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.ResourceAccessException;

import com.magna.xmbackend.entities.UserLdapAccountStatusTbl;
import com.magna.xmbackend.entities.UsersTbl;
import com.magna.xmbackend.vo.user.UserRequest;
import com.magna.xmbackend.vo.userAccountStatus.UserAccountStatusResponse;
import com.magna.xmsystem.dependencies.customExceptions.UnauthorizedAccessException;
import com.magna.xmsystem.dependencies.utils.XMSystemUtil;
import com.magna.xmsystem.ui.controls.widgets.CustomMessageDialog;
import com.magna.xmsystem.xmadmin.message.Message;
import com.magna.xmsystem.xmadmin.restclient.users.UserAccountStatusController;
import com.magna.xmsystem.xmadmin.restclient.users.UserController;
import com.magna.xmsystem.xmadmin.util.MessageType;
import com.magna.xmsystem.xmadmin.util.XMAdminUtil;

/**
 * The Class UserReviewDialog.
 */
public class UserReviewDialog extends TitleAreaDialog  {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(UserReviewDialog.class);

	/** The parent shell. */
	private Shell parentShell;

	/** The grp user search. */
	private Group grpUserSearch;

	/** The user review info viewer. */
	private CheckboxTableViewer userReviewInfoViewer;

	/** The select all btn. */
	private Button selectAllBtn;

	/** The de select all btn. */
	private Button deSelectAllBtn;

	/** The de activate btn. */
	private Button deActivateBtn;

	/** The delete btn. */
	private Button deleteBtn;

	/** Member variable for messages. */
	@Inject
	@Translation
	transient private Message messages;

	/** The allUserAccountStatus List. */
	protected List<UserAccountStatus> allUserAccountStatusList;

	/**
	 * Instantiates a new user review dialog.
	 *
	 * @param parentShell
	 *            the parent shell
	 */
	@Inject
	public UserReviewDialog(Shell parentShell) {
		super(parentShell);
		this.parentShell = parentShell;
		setShellStyle(SWT.RESIZE|SWT.CLOSE);
	}
	
	/* (non-Javadoc)
	 * @see org.eclipse.jface.dialogs.Dialog#createButtonsForButtonBar(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL, true);
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.dialogs.Dialog#create()
	 */
	@Override
	public void create() {
		super.create();
		this.setTitle(messages.userReviewDialogTitle);
		this.setMessage(messages.userReviewDialogMsg, IMessageProvider.INFORMATION);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.window.Window#configureShell(org.eclipse.swt.widgets.
	 * Shell)
	 */
	@Override
	public void configureShell(final Shell newShell) {
		try {
			super.configureShell(newShell);
			newShell.setText("CaxStartAdmin");
			newShell.setParent(this.parentShell);
			final Rectangle shellBounds = parentShell.getBounds();
			final int width = 550;
			final int height = 400;
			newShell.setSize(width, height);
			final Point pos = new Point(((shellBounds.width - width) / 2)+shellBounds.x, ((shellBounds.height -height) / 2)+shellBounds.y);
			newShell.setLocation(pos.x, pos.y);
		} catch (Exception e) {
			LOGGER.error("Unable to create UI elements", e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.dialogs.Dialog#createDialogArea(org.eclipse.swt.widgets
	 * .Composite)
	 */
	@Override
	protected Control createDialogArea(final Composite parent) {
		try {
			final Composite widgetContainer = new Composite(parent, SWT.NONE);
			final GridLayout widgetContainerLayout = new GridLayout(1, false);
			widgetContainer.setLayout(widgetContainerLayout);
			widgetContainer.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
			initGUI(widgetContainer);
			initListener();
		} catch (Exception e) {
			LOGGER.error("Unable to create UI elements", e);
		}
		return parent;

	}

	/**
	 * Inits the listener.
	 */
	private void initListener() {
		if (this.selectAllBtn != null && !this.selectAllBtn.isDisposed()) {
			this.selectAllBtn.addSelectionListener(new SelectionAdapter() {

				@Override
				public void widgetSelected(SelectionEvent event) {
					userReviewInfoViewer.setAllChecked(true);
				}

			});
		}

		if (this.deSelectAllBtn != null && !this.deSelectAllBtn.isDisposed()) {
			this.deSelectAllBtn.addSelectionListener(new SelectionAdapter() {

				@Override
				public void widgetSelected(SelectionEvent event) {
					userReviewInfoViewer.setAllChecked(false);
				}

			});
		}
		
		if (this.deleteBtn != null && !this.deleteBtn.isDisposed()) {
			this.deleteBtn.addSelectionListener(new SelectionAdapter() {

				@Override
				public void widgetSelected(SelectionEvent event) {
					try {
						UserController userController = new UserController();
						Object[] selectedUsers = userReviewInfoViewer.getCheckedElements();
						Set<String> userList = new HashSet<>();
						for (Object selectedUser : selectedUsers) {
							if(selectedUser instanceof UserAccountStatus){
								UserAccountStatus userAccountStatus = (UserAccountStatus) selectedUser;
								userList.add(userAccountStatus.getUserId());
							}
						}
						userController.deleteMultiUser(userList);
						List<String> userNames = new ArrayList<>();
						for (Object selectedUser : selectedUsers) {
							if (selectedUser instanceof UserAccountStatus) {
								String userName = ((UserAccountStatus) selectedUser).getUserName();
								userNames.add(userName);
							}
							allUserAccountStatusList.remove(selectedUser);
						}
						userReviewInfoViewer.refresh();
						if (userNames.size() > 0) {
							for (String userName : userNames) {
								XMAdminUtil.getInstance().updateLogFile(
										messages.userObject + " '" + userName + "' " + messages.objectDelete,
										MessageType.SUCCESS);
							}
						}
					} catch (UnauthorizedAccessException e) {
						LOGGER.error("Current user is Unauthorized " + e);
					} catch (Exception e) {
						if (e instanceof ResourceAccessException) {
							CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
									messages.errorDialogTitile, messages.serverNotReachable);
						}
						LOGGER.error(e.getMessage());
					}
				}

			});
		}
		
		if (this.deActivateBtn != null && !this.deActivateBtn.isDisposed()) {
			this.deActivateBtn.addSelectionListener(new SelectionAdapter() {

				@Override
				public void widgetSelected(SelectionEvent event) {
					try {
						UserController userController = new UserController();
						Object[] selectedUsers = userReviewInfoViewer.getCheckedElements();
						List<UserRequest> userRequestList = new ArrayList<>();
						List<String> userNames = new ArrayList<>();
						for (Object selectedUser : selectedUsers) {
							UserRequest userRequest = new UserRequest();
							if(selectedUser instanceof UserAccountStatus){
								UserAccountStatus userAccountStatus = (UserAccountStatus) selectedUser;
								userRequest.setUserName(userAccountStatus.getUserName());
								userRequest.setId(userAccountStatus.getUserId());
								userRequest.setStatus(com.magna.xmbackend.vo.enums.Status.INACTIVE);
								userRequestList.add(userRequest);
								userNames.add(userAccountStatus.getUserName());
							}
						}
						userController.multiUpdateUserStatus(userRequestList);
						for (Object selectedUser : selectedUsers) {
							for (Object userAccountStatus : allUserAccountStatusList) {
								if(userAccountStatus.equals(selectedUser)) {
									((UserAccountStatus) userAccountStatus).setXmSystemStatus(com.magna.xmbackend.vo.enums.Status.INACTIVE.name());
								}
							}
						}
						userReviewInfoViewer.refresh();
						if (userNames.size() > 0) {
							for (String userName : userNames) {
								XMAdminUtil.getInstance().updateLogFile(messages.objectStatusUpdate+" "
										+ messages.userObject + " '" + userName + "' " + messages.objectUpdate,
										MessageType.SUCCESS);
							}
						}
					} catch (UnauthorizedAccessException e) {
						LOGGER.error("Current user is Unauthorized " + e);
					} catch (Exception e) {
						if (e instanceof ResourceAccessException) {
							CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
									messages.errorDialogTitile, messages.serverNotReachable);
						}
						LOGGER.error(e.getMessage());
					}
				}

			});
		}
	}

	/**
	 * Inits the GUI.
	 *
	 * @param parent
	 *            the parent
	 */
	private void initGUI(Composite parent) {
		GridLayoutFactory.fillDefaults().applyTo(parent);
		parent.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

		this.grpUserSearch = new Group(parent, SWT.NONE);
		GridLayoutFactory.fillDefaults().numColumns(1).applyTo(this.grpUserSearch);
		GridDataFactory.fillDefaults().grab(true, true).span(SWT.FILL, SWT.FILL).applyTo(this.grpUserSearch);

		final Composite widgetContainer = new Composite(this.grpUserSearch, SWT.NONE);
		final GridLayout searchResultContainerLayout = new GridLayout(2, false);
		widgetContainer.setLayout(searchResultContainerLayout);
		widgetContainer.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

		createUserInfoTable(widgetContainer);

		final Composite btnContainer = new Composite(widgetContainer, SWT.NONE);
		final GridLayout btnContainerLayout = new GridLayout(1, false);
		btnContainer.setLayout(btnContainerLayout);
		GridDataFactory.fillDefaults().grab(false, false).span(1, 1).align(SWT.FILL, SWT.FILL).applyTo(btnContainer);

		this.selectAllBtn = new Button(btnContainer, SWT.NONE);
		this.selectAllBtn.setText(messages.userReviewSelectAllBtn);
		GridData gridData = new GridData();
		gridData.widthHint = 100;
		this.selectAllBtn.setLayoutData(gridData);

		this.deSelectAllBtn = new Button(btnContainer, SWT.NONE);
		this.deSelectAllBtn.setText(messages.userReviewDeSelectAllBtn);
		this.deSelectAllBtn.setLayoutData(gridData);

		this.deActivateBtn = new Button(widgetContainer, SWT.NONE);
		this.deActivateBtn.setText(messages.userReviewDeActivateBtn);
		gridData = new GridData(SWT.RIGHT, SWT.RIGHT, false, false, 1, 1);
		gridData.widthHint = 100;
		this.deActivateBtn.setLayoutData(gridData);

		this.deleteBtn = new Button(widgetContainer, SWT.NONE);
		this.deleteBtn.setText(messages.userReviewDeleteBtn);
		this.deleteBtn.setLayoutData(gridData);
		
		

		Job job = new Job("Loading Users...") {

			@Override
			protected IStatus run(IProgressMonitor monitor) {
				monitor.beginTask("Loading Users..", 100);
				monitor.worked(30);
				allUserAccountStatusList = getAllUserAccountStatus();
				Display.getDefault().syncExec(new Runnable() {
					public void run() {
						if (userReviewInfoViewer != null && !userReviewInfoViewer.getTable().isDisposed()) {
							userReviewInfoViewer.setInput(allUserAccountStatusList);
						}
					}
				});
				monitor.worked(70);
				return Status.OK_STATUS;
			}
		};
		job.setUser(true);
		job.schedule();

	}

	/**
	 * Creates the user info table.
	 *
	 * @param composite
	 *            the composite
	 */
	private void createUserInfoTable(final Composite composite) {
		final Composite userInfoContainer = new Composite(composite, SWT.NONE);
		final GridLayout userInfoContainerLayout = new GridLayout(1, false);
		userInfoContainer.setLayout(userInfoContainerLayout);
		userInfoContainer.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));

		final Table table = new Table(userInfoContainer,
				SWT.CHECK | SWT.FULL_SELECTION | SWT.V_SCROLL | SWT.BORDER | SWT.H_SCROLL);
		table.setLinesVisible(true);
		table.setHeaderVisible(true);

		this.userReviewInfoViewer = new CheckboxTableViewer(table);
		this.initColumn();

		this.userReviewInfoViewer.getTable().getColumn(0).setText(messages.userReviewUserNameCol);
		this.userReviewInfoViewer.getTable().getColumn(1).setText(messages.userReviewLdapStatusCol);
		this.userReviewInfoViewer.getTable().getColumn(2).setText(messages.userReviewXmSystemStatusCol);

		this.userReviewInfoViewer.setLabelProvider(new UserReviewViewerLabelProvider());
		this.userReviewInfoViewer.setContentProvider(new UserReviewViewerContentProvider());

	}

	/**
	 * Inits the column.
	 */
	private void initColumn() {
		try {
			final TableColumnLayout layout = new TableColumnLayout();
			this.userReviewInfoViewer.getTable().getParent().setLayout(layout);
			createTableViewerColumn(layout);
			createTableViewerColumn(layout);
			createTableViewerColumn(layout);
		} catch (Exception e) {
			LOGGER.error("Execption ocuured at creating the columns", e); //$NON-NLS-1$
		}
	}

	/**
	 * Creates the table viewer column.
	 *
	 * @param layout
	 *            the layout
	 */
	private void createTableViewerColumn(final TableColumnLayout layout) {
		final TableViewerColumn viewerCol = new TableViewerColumn(this.userReviewInfoViewer, SWT.NONE);
		final TableColumn column = viewerCol.getColumn();
		column.setWidth(50);
		layout.setColumnData(column, new ColumnWeightData(10));
	}

	/**
	 * Gets the all user account status.
	 *
	 * @return the all user account status
	 */
	private List<UserAccountStatus> getAllUserAccountStatus() {
		UserAccountStatusController userAccountStatusController = new UserAccountStatusController();
		UserAccountStatusResponse userAccountStatusResponse = userAccountStatusController.getAllUsersAccountStatus();
		Iterable<UserLdapAccountStatusTbl> userLdapAccountStatusTbl = userAccountStatusResponse.getUserLdapAccountStatusTbls();
		List<UserAccountStatus> userAccountStatusList = new ArrayList<>();
		for (UserLdapAccountStatusTbl userAccountStatusTbl : userLdapAccountStatusTbl) {
			UserAccountStatus userAccountStatus = new UserAccountStatus();
			UsersTbl usersTbl = userAccountStatusTbl.getUserId();
			if (usersTbl != null) {
				userAccountStatus.setUserName(usersTbl.getUsername());
				userAccountStatus.setUserId(usersTbl.getUserId());
				userAccountStatus.setLdapAccountStatus(userAccountStatusTbl.getAccountStatus());
				userAccountStatus.setXmSystemStatus(usersTbl.getStatus());
				userAccountStatusList.add(userAccountStatus);
			}
		}
		
		return sortUserList(userAccountStatusList);
	}

	
	/**
	 * Sort user list.
	 *
	 * @param userAccountStatusList the user account status list
	 * @return the list
	 */
	public static List<UserAccountStatus> sortUserList(List<UserAccountStatus> userAccountStatusList) {
		userAccountStatusList.sort((userAccStatus1, userAccStatus2) -> {
			if (userAccStatus1 != null && userAccStatus2 != null) {
				final String userName1 = userAccStatus1.getUserName();
				final String userName2 = userAccStatus2.getUserName();
				if (!XMSystemUtil.isEmpty(userName1) && !XMSystemUtil.isEmpty(userName2)) {
					return userName1.compareTo(userName2);
				}
			}
			return 0;
		});
		return userAccountStatusList;
	}

	/**
	 * Gets the user review info viewer.
	 *
	 * @return the user review info viewer
	 */
	public CheckboxTableViewer getUserReviewInfoViewer() {
		return userReviewInfoViewer;
	}

}
