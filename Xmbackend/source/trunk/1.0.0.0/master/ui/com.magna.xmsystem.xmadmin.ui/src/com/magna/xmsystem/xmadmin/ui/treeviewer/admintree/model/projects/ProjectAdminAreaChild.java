package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.refs.RelationObj;

/**
 * Class for Project admin area child.
 *
 * @author Chiranjeevi.Akula
 */
public class ProjectAdminAreaChild implements IAdminTreeChild {
	
	/** Member variable 'parent' for {@link IAdminTreeChild}. */
	private IAdminTreeChild parent;

	/** Member variable 'project admin area children' for {@link Map<String,IAdminTreeChild>}. */
	private Map<String, IAdminTreeChild> projectAdminAreaChildren;

	/**
	 * Constructor for ProjectAdminAreaChild Class.
	 *
	 * @param parent {@link IAdminTreeChild}
	 */
	public ProjectAdminAreaChild(final IAdminTreeChild parent) {
		this.parent = parent;
		this.projectAdminAreaChildren = new LinkedHashMap<>();
		//this.adminAreaProjectChildren.put(AdminAreaProjectChildrenEnum.PROJECTAPPLICATIONS, new AdminAreaProjectApplications(this));
	}

	/**
	 * Method for Adds the fixed children.
	 *
	 * @param relationObj {@link RelationObj}
	 */
	public void addFixedChildren(RelationObj relationObj) {
		this.projectAdminAreaChildren.put(ProjectAdminAreaProjectApplications.class.getName(), new ProjectAdminAreaProjectApplications(relationObj));
		this.projectAdminAreaChildren.put(ProjectAdminAreaStartApplications.class.getName(), new ProjectAdminAreaStartApplications(relationObj));
	}
	
	/**
	 * Gets the project admin area children collection.
	 *
	 * @return the project admin area children collection
	 */
	public Collection<IAdminTreeChild> getProjectAdminAreaChildrenCollection() {
		return this.projectAdminAreaChildren.values();
	}

	/**
	 * Gets the project admin area children children.
	 *
	 * @return the project admin area children children
	 */
	public Map<String, IAdminTreeChild> getProjectAdminAreaChildrenChildren() {
		return projectAdminAreaChildren;
	}
	
	/* (non-Javadoc)
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#getParent()
	 */
	@Override
	public IAdminTreeChild getParent() {
		return this.parent;
	}

	/* (non-Javadoc)
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#setParent(com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild)
	 */
	@Override
	public void setParent(final IAdminTreeChild parent) {
		this.parent = parent;
	}

}
