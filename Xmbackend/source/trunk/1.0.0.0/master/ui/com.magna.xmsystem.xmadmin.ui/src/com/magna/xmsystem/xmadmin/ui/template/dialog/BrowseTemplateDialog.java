package com.magna.xmsystem.xmadmin.ui.template.dialog;

import java.util.Arrays;
import java.util.TreeSet;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.ITreeSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeColumn;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magna.xmbackend.entities.EmailTemplateTbl;
import com.magna.xmbackend.vo.notification.EmailTemplateResponse;
import com.magna.xmsystem.dependencies.utils.LANG_ENUM;
import com.magna.xmsystem.dependencies.utils.XMSystemUtil;
import com.magna.xmsystem.ui.controls.util.FilteredTreeControl;
import com.magna.xmsystem.ui.controls.util.PatternFilterTree;
import com.magna.xmsystem.xmadmin.message.Message;
import com.magna.xmsystem.xmadmin.restclient.notify.NotificationController;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.NotificationTemplate;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.NotificationTemplates;

// TODO: Auto-generated Javadoc
/**
 * The Class BrowseTemplateDialog.
 */
public class BrowseTemplateDialog extends Dialog {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(BrowseTemplateDialog.class);

	/** The parent shell. */
	private Shell parentShell;

	/** The messages. */
	private Message messages;

	/** The browse template tree viewer. */
	protected BrowseTemplateTreeViewer browseTemplateTreeViewer;

	/** The okbtn. */
	protected Button okbtn;

	/** The selected template. */
	private NotificationTemplate selectedTemplate;

	/** The Constant OK_LABEL_EN. */
	final static private String OK_LABEL_EN = "OK";

	/** The Constant OK_LABEL_DE. */
	final static private String OK_LABEL_DE = "OK";

	/** The Constant CANCEL_LABEL_EN. */
	final static private String CANCEL_LABEL_EN = "Cancel";

	/** The Constant CANCEL_LABEL_DE. */
	final static private String CANCEL_LABEL_DE = "Abbrechen";

	/**
	 * Instantiates a new browse template dialog.
	 *
	 * @param shell
	 *            the shell
	 * @param messages
	 *            the messages
	 */
	public BrowseTemplateDialog(final Shell shell, final Message messages) {
		super(shell);
		this.parentShell = shell;
		this.messages = messages;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.window.Window#configureShell(org.eclipse.swt.widgets.
	 * Shell)
	 */
	@Override
	protected void configureShell(final Shell newShell) {
		try {
			super.configureShell(newShell);
			newShell.setText(messages.browseScriptDialogTitle);
			newShell.setParent(this.parentShell);
			final Rectangle shellBounds = parentShell.getBounds();
			int width = 450;
			int height = 300;
			newShell.setSize(width, height);
			final Point pos = new Point((shellBounds.width - width) / 2, (shellBounds.height - height) / 2);
			newShell.setLocation(pos.x, pos.y);
		} catch (Exception e) {
			LOGGER.error("Execution occurred while configuring shell!", e); //$NON-NLS-1$
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.dialogs.Dialog#createDialogArea(org.eclipse.swt.widgets
	 * .Composite)
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		try {
			final Composite widgetContainer = new Composite(parent, SWT.NONE);
			final GridLayout widgetContainerLayout = new GridLayout(1, false);
			widgetContainer.setLayout(widgetContainerLayout);
			widgetContainer.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
			initGUI(widgetContainer);
		} catch (Exception e) {
			LOGGER.error("Unable to create UI elements", e);
		}
		return parent;

	}

	/**
	 * Init GUI.
	 *
	 * @param parent
	 *            the parent
	 */
	private void initGUI(final Composite parent) {
		try {
			GridLayoutFactory.fillDefaults().applyTo(parent);
			parent.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

			Group grpScript = new Group(parent, SWT.NONE);
			GridLayoutFactory.fillDefaults().numColumns(1).applyTo(grpScript);
			GridDataFactory.fillDefaults().grab(true, true).span(SWT.FILL, SWT.FILL).applyTo(grpScript);

			final Composite widgetContainer = new Composite(grpScript, SWT.NONE);
			final GridLayout widgetContainerLayout = new GridLayout(1, false);
			widgetContainer.setLayout(widgetContainerLayout);
			widgetContainer.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

			createTableViewer(widgetContainer);
			init();
			addTreeColumnSelectionListeners();

		} catch (Exception ex) {
			LOGGER.error("Unable to crete UI elements", ex); //$NON-NLS-1$
		}
	}

	/**
	 * Init.
	 */
	private void init() {
		this.browseTemplateTreeViewer.setContentProvider(new BrowseTemplateViewerContentProvider());
		this.browseTemplateTreeViewer.setLabelProvider(new BrowseTemplateViewerLabelProvider());
		NotificationController notificationController = new NotificationController();
		EmailTemplateResponse emailTemplateResponse = notificationController.findAllNotificationTemplate();
		if (emailTemplateResponse != null) {
			Iterable<EmailTemplateTbl> emailTemplateTbls = emailTemplateResponse.getEmailTemplateTbls();
			NotificationTemplates notificationTemplates = new NotificationTemplates();
			for (EmailTemplateTbl emailTemplateTbl : emailTemplateTbls) {
				String templateVeriables = emailTemplateTbl.getTemplateVeriables();
				TreeSet<String> variablesSet;
				if (templateVeriables != null) {
					String[] varablesArray = templateVeriables.split(",");
					variablesSet = new TreeSet<>(Arrays.asList(varablesArray));
				} else {
					variablesSet = new TreeSet<>();
				}
				NotificationTemplate template = new NotificationTemplate();
				template.setTemplateId(emailTemplateTbl.getEmailTemplateId());
				template.setName(emailTemplateTbl.getName());
				template.setSubject(emailTemplateTbl.getSubject());
				template.setMessage(emailTemplateTbl.getMessage());
				template.setVariables(variablesSet);
				notificationTemplates.add(emailTemplateTbl.getEmailTemplateId(), template);
			}
			browseTemplateTreeViewer.setInput(notificationTemplates);
		}

	}

	/**
	 * Create table viewer.
	 *
	 * @param widgetContainer
	 *            the widget container
	 */
	private void createTableViewer(final Composite widgetContainer) {
		FilteredTreeControl iconControl = new FilteredTreeControl(widgetContainer, SWT.NONE, new PatternFilterTree()) {
			/**
			 * Overrides doCreateTreeViewer method
			 */
			@Override
			protected TreeViewer doCreateTreeViewer(final Composite parentL, final int style) {
				BrowseTemplateDialog.this.browseTemplateTreeViewer = new BrowseTemplateTreeViewer(parentL);
				return BrowseTemplateDialog.this.browseTemplateTreeViewer;
			}
		};
		GridData layoutData = new GridData(SWT.FILL, SWT.FILL, true, true);
		iconControl.setLayoutData(layoutData);
	}

	/**
	 * Add tree column selection listeners.
	 */
	private void addTreeColumnSelectionListeners() {
		TreeColumn column;
		Tree tree = this.browseTemplateTreeViewer.getTree();
		tree.setLinesVisible(true);
		tree.setHeaderVisible(true);
		int totalColumn = tree.getColumns().length;
		for (int i = 0; i < totalColumn; i++) {
			column = tree.getColumn(i);
			addColumnSelectionListener(column);
		}
	}

	/**
	 * Add column selection listener.
	 *
	 * @param column
	 *            the column
	 */
	private void addColumnSelectionListener(final TreeColumn column) {
		browseTemplateTreeViewer.addSelectionChangedListener(new ISelectionChangedListener() {

			@Override
			public void selectionChanged(SelectionChangedEvent event) {
				okbtn.setEnabled(true);
			}
		});

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.dialogs.Dialog#okPressed()
	 */
	@Override
	protected void okPressed() {

		ITreeSelection selection;
		selection = (ITreeSelection) this.browseTemplateTreeViewer.getSelection();
		Object element;
		if (selection != null && (element = selection.getFirstElement()) != null
				&& (element instanceof NotificationTemplate)) {
			this.selectedTemplate = (NotificationTemplate) element;
		}

		super.okPressed();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.dialogs.Dialog#createButtonsForButtonBar(org.eclipse.
	 * swt.widgets.Composite)
	 */
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		super.createButtonsForButtonBar(parent);

		Button cancelButton = getButton(IDialogConstants.CANCEL_ID);
		okbtn = getButton(IDialogConstants.OK_ID);
		LANG_ENUM currentLang = LANG_ENUM.ENGLISH;
		final String localeString = XMSystemUtil.getLanguage();
		if (!XMSystemUtil.isEmpty(localeString)) {
			currentLang = LANG_ENUM.valueOf(localeString.toUpperCase());
		}
		if (currentLang == LANG_ENUM.GERMAN) {
			cancelButton.setText(CANCEL_LABEL_DE);
			okbtn.setText(OK_LABEL_DE);
		} else {
			cancelButton.setText(CANCEL_LABEL_EN);
			okbtn.setText(OK_LABEL_EN);
		}
		setButtonLayoutData(okbtn);
		okbtn.setEnabled(false);
	}

	/**
	 * Gets the selected template.
	 *
	 * @return the selected template
	 */
	public NotificationTemplate getSelectedTemplate() {
		return selectedTemplate;
	}
}
