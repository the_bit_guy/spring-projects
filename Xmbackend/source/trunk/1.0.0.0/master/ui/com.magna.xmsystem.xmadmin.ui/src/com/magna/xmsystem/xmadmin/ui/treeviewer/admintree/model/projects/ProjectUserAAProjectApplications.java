package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.projectapplicationmodel.ProjectApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.refs.RelationObj;

/**
 * Class for Project user AA project applications.
 *
 * @author Chiranjeevi.Akula
 */
public class ProjectUserAAProjectApplications implements IAdminTreeChild {
	
	/** Member variable 'parent' for {@link IAdminTreeChild}. */
	private IAdminTreeChild parent;

	/** Member variable 'project user AA proj app children' for {@link Map<String,IAdminTreeChild>}. */
	private Map<String, IAdminTreeChild> projectUserAAProjAppChildren;

	/**
	 * Constructor for ProjectUserAAProjectApplications Class.
	 *
	 * @param parent {@link IAdminTreeChild}
	 */
	public ProjectUserAAProjectApplications(final IAdminTreeChild parent) {
		this.parent = parent;
		this.projectUserAAProjAppChildren = new LinkedHashMap<>();
		addFixedChildren();
	}

	/**
	 * Method for Adds the fixed children.
	 */
	public void addFixedChildren() {
		this.projectUserAAProjAppChildren.put(ProjectUserAAProjectAppAllowed.class.getName(), new ProjectUserAAProjectAppAllowed(this));
		this.projectUserAAProjAppChildren.put(ProjectUserAAProjectAppForbidden.class.getName(), new ProjectUserAAProjectAppForbidden(this));
	}
	
	/**
	 * Gets the project user AA proj app child collection.
	 *
	 * @return the project user AA proj app child collection
	 */
	public Collection<IAdminTreeChild> getProjectUserAAProjAppChildCollection() {
		return this.projectUserAAProjAppChildren.values();
	}

	/**
	 * Gets the project user AA proj app children.
	 *
	 * @return the project user AA proj app children
	 */
	public Map<String, IAdminTreeChild> getProjectUserAAProjAppChildren() {
		return projectUserAAProjAppChildren;
	}
	
	/**
	 * Method for Adds the.
	 *
	 * @param userProjectAppChildId {@link String}
	 * @param child {@link IAdminTreeChild}
	 * @return the i admin tree child {@link IAdminTreeChild}
	 */
	public IAdminTreeChild add(final String userProjectAppChildId, final IAdminTreeChild child) {
		child.setParent(this);
		IAdminTreeChild returnVal = this.projectUserAAProjAppChildren.put(userProjectAppChildId, child);
		if (child instanceof RelationObj && ((RelationObj) child).getRefObject() instanceof ProjectApplication) {
			sort();
		}
		return returnVal;
	}

	/**
	 * Method for Removes the.
	 *
	 * @param userProjectAppChildId {@link String}
	 * @return the i admin tree child {@link IAdminTreeChild}
	 */
	public IAdminTreeChild remove(final String userProjectAppChildId) {
		return this.projectUserAAProjAppChildren.remove(userProjectAppChildId);
	}

	/* (non-Javadoc)
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#getParent()
	 */
	@Override
	public IAdminTreeChild getParent() {
		return this.parent;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#
	 * setParent(com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.
	 * IAdminTreeChild)
	 */
	@Override
	public void setParent(final IAdminTreeChild parent) {
		this.parent = parent;
	}
	
	/**
	 * Sort.
	 */
	public void sort() {
		final LinkedHashMap<String, IAdminTreeChild> collect = this.projectUserAAProjAppChildren.entrySet().stream().sorted(
				(e1, e2) -> ((ProjectApplication) (((RelationObj) e1.getValue()).getRefObject())).getName()
				.compareTo(((ProjectApplication) (((RelationObj) e2.getValue()).getRefObject())).getName()))
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
		this.projectUserAAProjAppChildren = collect;
	}
}
