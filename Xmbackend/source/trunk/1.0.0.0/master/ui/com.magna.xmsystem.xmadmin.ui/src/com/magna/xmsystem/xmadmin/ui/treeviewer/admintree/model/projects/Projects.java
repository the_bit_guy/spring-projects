package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;

/**
 * The Class Projects.
 * 
 * @author subash.janarthanan
 * 
 */
public class Projects implements IAdminTreeChild {
	/** The parent. */
	private IAdminTreeChild parent;

	/** The projects children. */
	private Map<String, IAdminTreeChild> projectsChildren;

	/**
	 * Instantiates a new projects.
	 */
	public Projects() {
		this.parent = null;
		this.projectsChildren = new LinkedHashMap<>();
	}

	/**
	 * Adds the.
	 *
	 * @param projectId the project id
	 * @param child the child
	 * @return the i admin tree child
	 */
	public IAdminTreeChild add(final String projectId, final IAdminTreeChild child) {
		child.setParent(this);
		IAdminTreeChild returnVal = this.projectsChildren.put(projectId, child);
		sort();
		return returnVal;
	}

	/**
	 * Remove.
	 *
	 * @param projectName
	 *            the project name
	 * @return the i admin tree child
	 */
	public IAdminTreeChild remove(final String projectName) {
		return this.projectsChildren.remove(projectName);
	}

	/**
	 * Method for Removes the all.
	 */
	public void removeAll() {
		this.projectsChildren.clear();
	}
	
	/**
	 * Gets the projects collection.
	 *
	 * @return the projects collection
	 */
	public Collection<IAdminTreeChild> getProjectsCollection() {
		return this.projectsChildren.values();
	}

	/**
	 * Gets the projects children.
	 *
	 * @return the projects children
	 */
	public Map<String, IAdminTreeChild> getProjectsChildren() {
		return projectsChildren;
	}
	/**
	 * Returns null as its parent
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#getParent()
	 */
	@Override
	public IAdminTreeChild getParent() {
		return this.parent;
	}

	/**
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#setParent(com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild)
	 */
	@Override
	public void setParent(final IAdminTreeChild parent) {
		this.parent = parent;
	}
	
	/**
	 * Sort.
	 */
	public void sort() {
		final LinkedHashMap<String, IAdminTreeChild> collect = this.projectsChildren.entrySet().stream().sorted(
				(e1, e2) -> ((Project) e1.getValue()).getName().compareTo(((Project) e2.getValue()).getName()))
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
		this.projectsChildren = collect;
	}
}
