package com.magna.xmsystem.xmadmin.ui.parts.role.editingsupport;

import com.magna.xmbackend.vo.enums.CreationType;
import com.magna.xmbackend.vo.roles.ObjectPermission;
import com.magna.xmsystem.xmadmin.ui.parts.role.ObjectPermissionsTable;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.ObjectType;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.VoPermContainer;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.objectpermissions.ObjectPermissions;
import com.magna.xmsystem.xmadmin.util.XMAdminUtil;

/**
 * Class for Create editing support.
 *
 * @author Roshan.Ekka
 */
public class CreateEditingSupport extends CheckBoxEditingSupport {

	/** Member variable 'instance' for {@link XMAdminUtil}. */
	private XMAdminUtil instance;
	
	/** Member variable 'viewer' for {@link ObjectPermissionsTable}. */
	private ObjectPermissionsTable viewer;

	/**
	 * Constructor for CreateEditingSupport Class.
	 *
	 * @param viewer {@link ObjectPermissionsTable}
	 */
	public CreateEditingSupport(final ObjectPermissionsTable viewer) {
		super(viewer);
		this.viewer = viewer;
		this.instance = XMAdminUtil.getInstance();
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.viewers.EditingSupport#getValue(java.lang.Object)
	 */
	@Override
	protected Object getValue(Object element) {
		if (element instanceof ObjectPermissions) {
			return ((ObjectPermissions) element).isCreate();
		}
		return "N";
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.viewers.EditingSupport#setValue(java.lang.Object, java.lang.Object)
	 */
	@Override
	protected void setValue(Object element, Object value) {
		if (element instanceof ObjectPermissions) {
			ObjectPermissions elem = (ObjectPermissions) element;
			ObjectType objectType = ObjectType.getObjectType(elem.getPermissionName());
			String permissionId = this.instance.getObjectPermissionMap().get(objectType).getCreatePermission().getPermissionId();
			VoPermContainer voObjectPermContainer = new VoPermContainer();
			ObjectPermission objectPermission = new ObjectPermission();
			objectPermission.setPermissionId(permissionId);
			voObjectPermContainer.setObjectPermission(objectPermission);
			if (elem.isCreate() && ! (boolean) value) {
				objectPermission.setCreationType(CreationType.DELETE);
				elem.setCreatePermission(voObjectPermContainer);
			} else if (!elem.isCreate() && (boolean) value) {
				objectPermission.setCreationType(CreationType.ADD);
				elem.setCreatePermission(voObjectPermContainer);
			} else {
				return;
			}
			elem.setCreate((boolean) value);
			this.viewer.getCachedObjPermissionMap().put(permissionId, voObjectPermContainer);
			getViewer().refresh(element);
		}
	}
	
	/* (non-Javadoc)
	 * @see com.magna.xmsystem.xmadmin.ui.parts.role.editingsupport.CheckBoxEditingSupport#canEdit(java.lang.Object)
	 */
	@Override
	protected boolean canEdit(final Object element) {
		if (element instanceof ObjectPermissions) {
			String permissionName = ((ObjectPermissions) element).getPermissionName();
			ObjectType objectType = ObjectType.getObjectType(permissionName);
			if (objectType == ObjectType.PROPERTY_CONFIG) {
				return false;
			}
		}
		return true;
	}
}
