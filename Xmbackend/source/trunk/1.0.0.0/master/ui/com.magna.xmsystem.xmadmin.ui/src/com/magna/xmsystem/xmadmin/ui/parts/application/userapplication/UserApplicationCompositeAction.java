package com.magna.xmsystem.xmadmin.ui.parts.application.userapplication;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.inject.Inject;

import org.eclipse.core.databinding.Binding;
import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.beans.BeanProperties;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.core.databinding.observable.value.IValueChangeListener;
import org.eclipse.core.databinding.observable.value.ValueChangeEvent;
import org.eclipse.e4.core.services.nls.Translation;
import org.eclipse.e4.ui.di.Persist;
import org.eclipse.e4.ui.model.application.ui.MDirtyable;
import org.eclipse.jface.databinding.fieldassist.ControlDecorationSupport;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.fieldassist.ControlDecoration;
import org.eclipse.jface.fieldassist.FieldDecorationRegistry;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreePath;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.VerifyEvent;
import org.eclipse.swt.events.VerifyListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Link;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.ToolItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.ResourceAccessException;

import com.magna.xmbackend.entities.LanguagesTbl;
import com.magna.xmbackend.entities.UserAppTranslationTbl;
import com.magna.xmbackend.entities.UserApplicationsTbl;
import com.magna.xmbackend.vo.userApplication.UserApplicationRequest;
import com.magna.xmsystem.dependencies.customExceptions.UnauthorizedAccessException;
import com.magna.xmsystem.dependencies.utils.LANG_ENUM;
import com.magna.xmsystem.dependencies.utils.XMSystemUtil;
import com.magna.xmsystem.ui.controls.widgets.CustomMessageDialog;
import com.magna.xmsystem.xmadmin.message.Message;
import com.magna.xmsystem.xmadmin.message.MessageRegistry;
import com.magna.xmsystem.xmadmin.restclient.application.UserAppController;
import com.magna.xmsystem.xmadmin.ui.Application.dialogs.BrowseApplicationDialog;
import com.magna.xmsystem.xmadmin.ui.dialogs.lang.ControlModel;
import com.magna.xmsystem.xmadmin.ui.dialogs.lang.TextAreaModifyListener;
import com.magna.xmsystem.xmadmin.ui.dialogs.lang.XMAdminLangTextAreaDialog;
import com.magna.xmsystem.xmadmin.ui.dialogs.lang.XMAdminLangTextDialog;
import com.magna.xmsystem.xmadmin.ui.parts.icons.IconDialog;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.AdminTreeviewer;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.AdminTreeFactory;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.baseapplicationmodel.BaseApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.userapplicationmodel.UserApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.userapplicationmodel.UserApplicationChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.userapplicationmodel.UserApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.icon.Icon;
import com.magna.xmsystem.xmadmin.ui.validation.NameValidation;
import com.magna.xmsystem.xmadmin.ui.validation.StatusValidation;
import com.magna.xmsystem.xmadmin.ui.validation.SymbolValidation;
import com.magna.xmsystem.xmadmin.util.CommonConstants;
import com.magna.xmsystem.xmadmin.util.MessageType;
import com.magna.xmsystem.xmadmin.util.XMAdminUtil;

/**
 * Class for User application composite action.
 *
 * @author Chiranjeevi.Akula
 */
public class UserApplicationCompositeAction extends UserApplicationCompositeUI {

	/** The Constant PROGRAMMATICALLY. */
	private static final String PROGRAMMATICALLY = "Programmatically"; //$NON-NLS-1$

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(UserApplicationCompositeAction.class);

	/** Member variable 'user application model' for {@link UserApplication}. */
	private UserApplication userApplicationModel;

	private UserApplicationChild userApplicationChildModel;

	/** Member variable 'registry' for {@link MessageRegistry}. */
	@Inject
	private MessageRegistry registry;

	/** Member variable 'messages' for {@link Message}. */
	@Inject
	@Translation
	transient private Message messages;

	/** Member variable 'widget value' for {@link IObservableValue<?>}. */
	transient private IObservableValue<?> widgetValue;

	/** Member variable 'model value' for {@link IObservableValue<?>}. */
	transient private IObservableValue<?> modelValue;

	/** Member variable 'data bind context' for {@link DataBindingContext}. */
	final transient DataBindingContext dataBindContext = new DataBindingContext();

	/** Member variable 'old model' for {@link UserApplication}. */
	private UserApplication oldModel;

	/** Member variable 'dirty' for {@link MDirtyable}. */
	private MDirtyable dirty;

	/**
	 * Constructor for UserApplicationCompositeAction Class.
	 *
	 * @param parent
	 *            {@link Composite}
	 */
	@Inject
	public UserApplicationCompositeAction(final Composite parent) {
		super(parent, SWT.NONE);
		initListeners();
	}

	/**
	 * Method for Bind values.
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void bindValues() {
		try {
			// Name field binding
			widgetValue = WidgetProperties.text(SWT.Modify).observe(this.txtName);
			modelValue = BeanProperties.value(UserApplication.class, UserApplication.PROPERTY_NAME).observe(this.userApplicationModel);
			widgetValue.addValueChangeListener(new IValueChangeListener() {
				/**
				 * handler to update button status
				 */
				@Override
				public void handleValueChange(final ValueChangeEvent event) {
					updateButtonStatus(event);
				}
			});

			// define the UpdateValueStrategy
			final UpdateValueStrategy update = new UpdateValueStrategy();
			update.setAfterGetValidator(new NameValidation(messages, StatusValidation.APPLICATION));
			Binding bindValue = dataBindContext.bindValue(widgetValue, modelValue, update, null);
			ControlDecorationSupport.create(bindValue, SWT.TOP | SWT.LEFT);

			// Active check box binding
			widgetValue = WidgetProperties.selection().observe(this.activeBtn);
			modelValue = BeanProperties.value(UserApplication.class, UserApplication.PROPERTY_ACTIVE)
					.observe(this.userApplicationModel);
			bindValue = dataBindContext.bindValue(widgetValue, modelValue);
			
			//description binding
			widgetValue = WidgetProperties.text(SWT.Modify).observe(this.txtDesc);
			modelValue = BeanProperties.value(UserApplication.class, UserApplication.PROPERTY_DESC).observe(this.userApplicationModel);
			bindValue = dataBindContext.bindValue(widgetValue, modelValue);

			// Parent check box binding
			widgetValue = WidgetProperties.selection().observe(this.parentBtn);
			modelValue = BeanProperties.value(UserApplication.class, UserApplication.PROPERTY_PARENT)
					.observe(this.userApplicationModel);
			bindValue = dataBindContext.bindValue(widgetValue, modelValue);

			// Singleton check box binding
			widgetValue = WidgetProperties.selection().observe(this.btnSingleton);
			modelValue = BeanProperties.value(UserApplication.class, UserApplication.PROPERTY_SINGLETON)
					.observe(this.userApplicationModel);
			bindValue = dataBindContext.bindValue(widgetValue, modelValue);

			// Symbol toolItem setup
			Icon icon;
			String iconPath;
			if ((icon = this.userApplicationModel.getIcon()) != null
					&& !XMSystemUtil.isEmpty(iconPath = icon.getIconPath())) {
				if (iconPath.contains("null")) { //$NON-NLS-1$
					toolItem.setImage(XMSystemUtil.getInstance().getImage(this.getClass(), "icons/16x16/browse.png")); //$NON-NLS-1$ 
				} else {
					toolItem.setImage(XMSystemUtil.getInstance().getImage(this.getClass(), iconPath, true, true));
					// Symbol toolItem binding
					widgetValue = WidgetProperties.tooltipText().observe(this.toolItem);
					modelValue = BeanProperties
							.value(UserApplication.class, UserApplication.PROPERTY_ICON + "." + Icon.PROPERTY_ICONNAME, //$NON-NLS-1$
									Icon.class)
							.observe(this.userApplicationModel);
					bindValue = dataBindContext.bindValue(widgetValue, modelValue);

					// Symbol Field binding
					widgetValue = WidgetProperties.text(SWT.Modify).observe(this.txtSymbol);
					modelValue = BeanProperties
							.value(UserApplication.class, UserApplication.PROPERTY_ICON + "." + Icon.PROPERTY_ICONNAME, //$NON-NLS-1$
									Icon.class)
							.observe(this.userApplicationModel);
					final UpdateValueStrategy symbolUpdate = new UpdateValueStrategy();
					symbolUpdate.setAfterGetValidator(new SymbolValidation());
					bindValue = dataBindContext.bindValue(widgetValue, modelValue, symbolUpdate, null);
					ControlDecorationSupport.create(bindValue, SWT.TOP | SWT.LEFT);
				}
			}
		} catch (Exception e) {
			LOGGER.error("Exception occured while databinding", e); //$NON-NLS-1$
		}
	}
	
	/**
	 * Update button status.
	 *
	 * @param event the event
	 */
	@SuppressWarnings("rawtypes")
	private void updateButtonStatus(ValueChangeEvent event) {
		final String name = (String) event.getObservableValue().getValue();
		if (this.saveBtn != null) {
			if (XMSystemUtil.isEmpty(name) || name.trim().length() == 0
					|| (!name.matches(CommonConstants.RegularExpressions.ALLOWED_APP_NAME_REGEX))) {
				this.saveBtn.setEnabled(false);
			} else {
				this.saveBtn.setEnabled(true);
			}
		}
	}

	/**
	 * Gets the user application model.
	 *
	 * @return the user application model
	 */
	public UserApplication getUserApplicationModel() {
		return userApplicationModel;
	}

	/**
	 * Sets the user application model.
	 *
	 * @param userApplicationModel
	 *            the new user application model
	 */
	public void setUserApplicationModel(final UserApplication userApplicationModel) {
		this.userApplicationModel = userApplicationModel;
	}

	public UserApplicationChild getUserApplicationChildModel() {
		return userApplicationChildModel;
	}

	public void setUserApplicationChildModel(UserApplicationChild userApplicationChildModel) {
		this.userApplicationChildModel = userApplicationChildModel;
	}

	/**
	 * Method for Inits the listeners.
	 */
	private void initListeners() {
		// Add listener to widgets
		this.descTranslateLink.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent event) {
				final Link linkWidget = (Link) event.widget;
				openDescDialog(linkWidget.getShell());
			}
		});

		this.nameTranslateLink.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent event) {
				final Link linkWidget = (Link) event.widget;
				openNameDialog(linkWidget.getShell());
			}
		});

		this.remarksTranslateLink.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent event) {
				final Link linkWidget = (Link) event.widget;
				openRemarkDialog(linkWidget.getShell());
			}
		});

		if (this.saveBtn != null) {
			this.saveBtn.addSelectionListener(new SelectionAdapter() {

				/**
				 * Save button handler
				 */
				@Override
				public void widgetSelected(final SelectionEvent event) {
					saveHandler();
				}
			});
		}

		if (this.cancelBtn != null) {
			this.cancelBtn.addSelectionListener(new SelectionAdapter() {

				/**
				 * Cancel button handler
				 */
				@Override
				public void widgetSelected(final SelectionEvent event) {
					cancelHandler();
				}
			});
		}

		this.toolItem.addSelectionListener(new SelectionAdapter() {

			/**
			 * Symbol button handler
			 */
			@Override
			public void widgetSelected(final SelectionEvent event) {
				if ((boolean) toolItem.getData("editable")) { //$NON-NLS-1$
					final ToolItem widget = (ToolItem) event.widget;
					final IconDialog dialog = new IconDialog(widget.getParent().getShell(),
							messages.browseIconDialogTitle, messages.icontableviewerSecondColumnLabel);

					final int returnVal = dialog.open();
					if (IDialogConstants.OK_ID == returnVal) {
						final Icon checkedIcon = dialog.getCheckedIcon();
						// toolItem.setToolTipText(checkedIcon.getIconName());
						toolItem.setImage(XMSystemUtil.getInstance().getImage(this.getClass(), checkedIcon.getIconPath(), true, true));
						txtSymbol.setText(checkedIcon.getIconName());
						userApplicationModel.setIcon(checkedIcon);
					}
				}
			}
		});
		this.txtRemarks
				.addModifyListener(new TextAreaModifyListener(this.lblRemarksCount, UserApplication.REMARK_LIMIT));

		this.programToolItem.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(final SelectionEvent event) {
				if ((boolean) programToolItem.getData("editable")) { //$NON-NLS-1$
					final ToolItem widget = (ToolItem) event.widget;
					Map<String, IAdminTreeChild> getapplication = AdminTreeFactory.getInstance().getUserApplications()
							.getUserApplications();
					List<IAdminTreeChild> list = new ArrayList<>(getapplication.values());
					List<IAdminTreeChild> parentList = new ArrayList<IAdminTreeChild>();

					boolean parentSelection = parentBtn.getSelection();
					if (!parentSelection) {
						for (IAdminTreeChild adminTreeChild : list) {
							if (adminTreeChild instanceof UserApplication) {
								if (((UserApplication) adminTreeChild).isParent() && !((UserApplication) adminTreeChild)
										.getUserApplicationId().equals(userApplicationModel.getUserApplicationId())) {
									parentList.add((IAdminTreeChild) adminTreeChild);
								}
							}
						}
					}
					final AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
					final Object selectionObj = adminTree.getSelection();
					if (selectionObj instanceof IStructuredSelection) {
						Object firstElement = ((IStructuredSelection) selectionObj).getFirstElement();
						if (firstElement instanceof UserApplication) {
							UserApplication userApp = (UserApplication) firstElement;
							if (parentList.contains(userApp.getParent())) {
								parentList.remove(userApp.getParent());
							}
						}
					}
					BrowseApplicationDialog scriptDialog = new BrowseApplicationDialog(widget.getParent().getShell(),
							messages, parentList, true, parentSelection);
					final int returnVal = scriptDialog.open();
					if (IDialogConstants.OK_ID == returnVal) {
						Object selectedIteam = scriptDialog.getSelectedApplication();
						if (selectedIteam != null && selectedIteam instanceof UserApplication) {
							UserApplication userApplication = (UserApplication) selectedIteam;
							/*if (getOldModel()!=null && getOldModel().isParent()) {
								CustomMessageDialog.openError(getShell(), messages.appPositionErrorTitle,
										messages.appChildPositionError);
								parentBtn.setSelection(true);
								txtBaseApplication.setEnabled(false);
								programAppToolItem.setEnabled(false);
								txtBaseApplication.setText(CommonConstants.EMPTY_STR);
								userApplicationModel.setBaseApplicationId(null);
								notifyBaseAppText();
								return;
							}
*/
							userApplicationModel.setPosition(userApplication.getUserApplicationId());
							parentBtn.setEnabled(false);
						} else if (selectedIteam instanceof String) {
							for (String positionType : CommonConstants.Application.POSITIONS) {
								if (selectedIteam.toString()
										.equalsIgnoreCase(CommonConstants.Application.POSITIONS[1])) {
									userApplicationModel.setPosition(selectedIteam.toString());
									parentBtn.setEnabled(false);
									break;
								} else if ((selectedIteam.toString()
										.equalsIgnoreCase(CommonConstants.Application.POSITIONS[0])
										&& userApplicationModel.getBaseApplicationId() == null)
										|| (selectedIteam.toString()
												.equalsIgnoreCase(CommonConstants.Application.POSITIONS[2])
												&& userApplicationModel.getBaseApplicationId() == null)) {
									userApplicationModel.setPosition(selectedIteam.toString());
									boolean flag = true;
									if (userApplicationModel.isParent() /*&& getOldModel()!=null*/) {
										flag = false;
										/*Map<String, IAdminTreeChild> userApplicationChildren = getOldModel()
												.getUserApplicationChildren();
										Collection<IAdminTreeChild> values = userApplicationChildren.values();
										for (IAdminTreeChild iAdminTreeChild : values) {
											if (iAdminTreeChild instanceof UserApplicationChild) {
												flag = false;
												break;
											}
										}*/
									}
									parentBtn.setEnabled(flag);
									break;
								} else if ((selectedIteam.toString()
										.equalsIgnoreCase(CommonConstants.Application.POSITIONS[0])
										|| userApplicationModel.getBaseApplicationId() != null)
										|| (selectedIteam.toString()
												.equalsIgnoreCase(CommonConstants.Application.POSITIONS[2])
												|| userApplicationModel.getBaseApplicationId() != null)) {
									userApplicationModel.setPosition(selectedIteam.toString());
									parentBtn.setEnabled(false);
									break;
								}
							}
						} else {
							userApplicationModel.setPosition(null);
						}
					}
				}
				updatePosition();
			}

		});

		this.programAppToolItem.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(final SelectionEvent event) {
				if ((boolean) programAppToolItem.getData("editable")) { //$NON-NLS-1$

					final ToolItem widget = (ToolItem) event.widget;
					Map<String, IAdminTreeChild> getbaseapplication = AdminTreeFactory.getInstance()
							.getBaseApplications().getBaseApplications();

					List<IAdminTreeChild> list = new ArrayList<>(getbaseapplication.values());
					BrowseApplicationDialog scriptDialog = new BrowseApplicationDialog(widget.getParent().getShell(),
							messages, list, false, true);
					final int returnVal = scriptDialog.open();
					if (IDialogConstants.OK_ID == returnVal) {
						Object selectedBaseApp = scriptDialog.getSelectedApplication();
						if (selectedBaseApp != null && selectedBaseApp instanceof BaseApplication) {
							BaseApplication baseApplication = (BaseApplication) selectedBaseApp;
							String baseApplicationId = baseApplication.getBaseApplicationId();
							userApplicationModel.setBaseApplicationId(baseApplicationId);
							parentBtn.setEnabled(false);
						} else {
							userApplicationModel.setBaseApplicationId(null);
						}
					}
				}
				updateBaseApplication();
			}
		});

		this.parentBtn.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				Button button = (Button) event.widget;

				boolean isParent = button.getSelection();
				if (isParent) {
					txtBaseApplication.setText(CommonConstants.EMPTY_STR);
					txtBaseApplication.setEnabled(false);
					programAppToolItem.setEnabled(false);
					btnSingleton.setEnabled(false);
					btnSingleton.setSelection(false);
					userApplicationModel.setBaseApplicationId(null);
					if (!(PROGRAMMATICALLY.equals(event.data))) {
						CustomMessageDialog.openInformation(getShell(), messages.parentEnableInfoTitle,
								messages.parentEnableInfo);
					}
					notifyBaseAppText();
					if (txtPosition.getText().equalsIgnoreCase(CommonConstants.Application.POSITIONS[1])) {
						txtPosition.setText(CommonConstants.EMPTY_STR);
					}
				} else {
					txtBaseApplication.setEnabled(true);
					programAppToolItem.setEnabled(true);
					if (userApplicationModel.getOperationMode() == CommonConstants.OPERATIONMODE.VIEW) {
						btnSingleton.setEnabled(false);
					} else {
						btnSingleton.setEnabled(true);
					}
					notifyBaseAppText();
				}
			}

		});

		this.txtPosition.addModifyListener(new ModifyListener() {
			ControlDecoration txtPostionDecroator = new ControlDecoration(txtPosition, SWT.TOP);

			@Override
			public void modifyText(ModifyEvent event) {
				final Image nameDecoratorImage = FieldDecorationRegistry.getDefault()
						.getFieldDecoration(FieldDecorationRegistry.DEC_ERROR).getImage();
				txtPostionDecroator.setImage(nameDecoratorImage);
				Text text = (Text) event.widget;
				if (text.getText().trim().length() <= 0) {
					txtPostionDecroator.setDescriptionText(messages.appPostionEmptyError);
					txtPostionDecroator.show();
				} else if (text.getText().trim().length() > 0) {
					txtPostionDecroator.hide();
				}
			}
		});

		this.txtBaseApplication.addModifyListener(new ModifyListener() {
			ControlDecoration txtBaseAppDecroator = new ControlDecoration(txtBaseApplication, SWT.TOP);

			@Override
			public void modifyText(ModifyEvent event) {
				final Image nameDecoratorImage = FieldDecorationRegistry.getDefault()
						.getFieldDecoration(FieldDecorationRegistry.DEC_ERROR).getImage();
				txtBaseAppDecroator.setImage(nameDecoratorImage);
				Text text = (Text) event.widget;
				if (!parentBtn.getSelection()) {
					if (text.getText().trim().length() <= 0) {
						txtBaseAppDecroator.setDescriptionText(messages.appBaseAppEmptyError);
						txtBaseAppDecroator.show();
					} else if (text.getText().trim().length() > 0) {
						txtBaseAppDecroator.hide();
					}
				} else {
					txtBaseAppDecroator.hide();
				}

			}
		});
		
		this.txtRemarks.addVerifyListener(new VerifyListener() {

			@Override
			public void verifyText(VerifyEvent event) {
				String source = ((Text) event.widget).getText();
				final String remarkText = source.substring(0, event.start) + event.text + source.substring(event.end);
				int length = remarkText.length();
				if (length > UserApplication.REMARK_LIMIT) {
					event.doit = false;

				}
			}
		});

	}

	/**
	 * Notify base app text.
	 */
	private void notifyBaseAppText() {
		Event event1 = new Event();
		event1.data = PROGRAMMATICALLY;
		txtBaseApplication.notifyListeners(SWT.Modify, event1);
	}

	/**
	 * Method for Validate.
	 *
	 * @return true, if successful
	 */
	private boolean validate() {
		if (!saveBtn.isEnabled()) {
			return false;
		}
		final String userApplicationName = this.userApplicationModel.getName();
		Icon icon;
		if (XMSystemUtil.isEmpty(userApplicationName) && (icon = this.userApplicationModel.getIcon()) != null
				&& XMSystemUtil.isEmpty(icon.getIconName())) {
			CustomMessageDialog.openError(this.getShell(), messages.nameErrorTitle, messages.nameError);
			return false;
		}
		if (XMSystemUtil.isEmpty(userApplicationName)) {
			CustomMessageDialog.openError(this.getShell(), messages.appNameErrorTitle, messages.appNameEmptyEnError);
			return false;
		}
		if ((icon = this.userApplicationModel.getIcon()) != null && XMSystemUtil.isEmpty(icon.getIconName())) {
			CustomMessageDialog.openError(this.getShell(), messages.symbolErrorTitle, messages.symbolError);
			return false;
		}
		final UserApplications userApplications = AdminTreeFactory.getInstance().getUserApplications();
		final Collection<IAdminTreeChild> userAppsCollection = userApplications.getUserAppCollection();

		if (!XMSystemUtil.isEmpty(userApplicationName)) {
			final Map<String, Long> result = userAppsCollection.stream()
					.collect(Collectors.groupingBy(userApplication -> {
						if (!XMSystemUtil.isEmpty(((UserApplication) userApplication).getName())) {
							return ((UserApplication) userApplication).getName().toUpperCase();
						}
						return CommonConstants.EMPTY_STR;
					}, Collectors.counting()));

			Map<String, String> childResult = new HashMap<String, String>();
			for (IAdminTreeChild iAdminTreeChild : userAppsCollection) {
				if (iAdminTreeChild instanceof UserApplication) {
					UserApplication userApplication = (UserApplication) iAdminTreeChild;
					Map<String, IAdminTreeChild> userApplicationChildren = userApplication.getUserApplicationChildren();
					Collection<IAdminTreeChild> values = userApplicationChildren.values();
					for (IAdminTreeChild userAppChild : values) {
						if (userAppChild instanceof UserApplicationChild) {
							UserApplicationChild childUserApp = (UserApplicationChild) userAppChild;
							childResult.put(childUserApp.getName().toUpperCase(),
									childUserApp.getUserApplicationId());
						}
					}
				}
			}

			if (result.containsKey(userApplicationName.toUpperCase())
					|| childResult.containsKey(userApplicationName.toUpperCase())) {
				if ((this.userApplicationModel.getOperationMode() == CommonConstants.OPERATIONMODE.CHANGE
						&& !userApplicationName.equalsIgnoreCase(this.oldModel.getName()))
						|| this.userApplicationModel.getOperationMode() == CommonConstants.OPERATIONMODE.CREATE) {
					CustomMessageDialog.openError(this.getShell(), messages.appNameErrorTitle, messages.existingEnNameError);
					return false;
				}
			}
		}
		if (XMSystemUtil.isEmpty(this.txtPosition.getText())) {
			CustomMessageDialog.openError(this.getShell(), messages.appPositionErrorTitle,
					messages.appPostionEmptyError);
			return false;
		}
		if ((!this.parentBtn.getSelection()) && XMSystemUtil.isEmpty(this.txtBaseApplication.getText())) {
			CustomMessageDialog.openError(this.getShell(), messages.appBaseAppErrorTitle,
					messages.appBaseAppEmptyError);
			return false;
		}

		return true;
	}

	/**
	 * Method for Register messages.
	 *
	 * @param registry
	 *            {@link MessageRegistry}
	 */
	public void registerMessages(final MessageRegistry registry) {
		registry.register((text) -> {
			if (grpUserApplication != null && !grpUserApplication.isDisposed()) {
				grpUserApplication.setText(text);
			}
		}, (message) -> {
			if (userApplicationModel != null) {
				String name = this.userApplicationModel.getName();
				if (userApplicationModel.getOperationMode() == CommonConstants.OPERATIONMODE.VIEW) {
					return message.objectGroupDisaplyLabel + " \'" + name + "\'"; //$NON-NLS-1$ //$NON-NLS-2$
				} else if (userApplicationModel.getOperationMode() == CommonConstants.OPERATIONMODE.CHANGE) {
					return message.objectGroupChangeLabel + " \'" + name + "\'"; //$NON-NLS-1$ //$NON-NLS-2$
				} else if (userApplicationModel.getOperationMode() == CommonConstants.OPERATIONMODE.CREATE) {
					return message.userAppGroupCreateLabel;
				}
			}
			return CommonConstants.EMPTY_STR;
		});
		registry.register((text) -> {
			if (lblName != null && !lblName.isDisposed()) {
				lblName.setText(text);
			}
		}, (message) -> {
			if (lblName != null && !lblName.isDisposed()) {
				return getUpdatedWidgetText(message.objectNameLabel, lblName);
			}
			return CommonConstants.EMPTY_STR;
		});
		registry.register((text) -> {
			if (lblDescrition != null && !lblDescrition.isDisposed()) {
				lblDescrition.setText(text);
			}
		}, (message) -> {
			if (lblDescrition != null && !lblDescrition.isDisposed()) {
				return getUpdatedWidgetText(message.objectDescriptionLabel, lblDescrition);
			}
			return CommonConstants.EMPTY_STR;
		});
		registry.register((text) -> {
			if (lblActive != null && !lblActive.isDisposed()) {
				lblActive.setText(text);
			}
		}, (message) -> {
			if (lblActive != null && !lblActive.isDisposed()) {
				return getUpdatedWidgetText(message.objectActiveLabel, lblActive);
			}
			return CommonConstants.EMPTY_STR;
		});
		registry.register((text) -> {
			if (lblSymbol != null && !lblSymbol.isDisposed()) {
				lblSymbol.setText(text);
			}
		}, (message) -> {
			if (lblSymbol != null && !lblSymbol.isDisposed()) {
				return getUpdatedWidgetText(message.objectSymbolLabel, lblSymbol);
			}
			return CommonConstants.EMPTY_STR;
		});
		registry.register((text) -> {
			if (lblSingleton != null && !lblSingleton.isDisposed()) {
				lblSingleton.setText(text);
			}
		}, (message) -> {
			if (lblSingleton != null && !lblSingleton.isDisposed()) {
				return getUpdatedWidgetText(message.objectSingletonLabel, lblSingleton);
			}
			return CommonConstants.EMPTY_STR;
		});
		registry.register((text) -> {
			if (lblPosition != null && !lblPosition.isDisposed()) {
				lblPosition.setText(text);
			}
		}, (message) -> {
			if (lblPosition != null && !lblPosition.isDisposed()) {
				return getUpdatedWidgetText(message.objectPositionLabel, lblPosition);
			}
			return CommonConstants.EMPTY_STR;
		});
		registry.register((text) -> {
			if (lblBaseApplication != null && !lblBaseApplication.isDisposed()) {
				lblBaseApplication.setText(text);
			}
		}, (message) -> {
			if (lblBaseApplication != null && !lblBaseApplication.isDisposed()) {
				return getUpdatedWidgetText(message.objectApplicationLabel, lblBaseApplication);
			}
			return CommonConstants.EMPTY_STR;
		});
		if (saveBtn != null) {
			registry.register((text) -> {
				if (saveBtn != null && !saveBtn.isDisposed()) {
					saveBtn.setText(text);
				}
			}, (message) -> {
				if (saveBtn != null && !saveBtn.isDisposed()) {
					return getUpdatedWidgetText(message.saveButtonText, saveBtn);
				}
				return CommonConstants.EMPTY_STR;
			});
		}
		if (cancelBtn != null) {
			registry.register((text) -> {
				if (cancelBtn != null && !cancelBtn.isDisposed()) {
					cancelBtn.setText(text);
				}
			}, (message) -> {
				if (cancelBtn != null && !cancelBtn.isDisposed()) {
					return getUpdatedWidgetText(message.cancelButtonText, cancelBtn);
				}
				return CommonConstants.EMPTY_STR;
			});
		}
		registry.register((text) -> {
			if (remarksTranslateLink != null && !remarksTranslateLink.isDisposed()) {
				remarksTranslateLink.setText(text);
			}
		}, (message) -> {
			if (remarksTranslateLink != null && !remarksTranslateLink.isDisposed()) {
				return getUpdatedWidgetText("<a>" + message.objectTranslationLinkText + "</a>", remarksTranslateLink); //$NON-NLS-1$ //$NON-NLS-2$
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (remarksLabel != null && !remarksLabel.isDisposed()) {
				remarksLabel.setText(text);
			}
		}, (message) -> {
			if (remarksLabel != null && !remarksLabel.isDisposed()) {
				return getUpdatedWidgetText(message.objectRemarkLabel, remarksLabel);
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (descTranslateLink != null && !descTranslateLink.isDisposed()) {
				descTranslateLink.setText(text);
			}
		}, (message) -> {
			if (descTranslateLink != null && !descTranslateLink.isDisposed()) {
				return getUpdatedWidgetText("<a>" + message.objectTranslationLinkText + "</a>", descTranslateLink); //$NON-NLS-1$ //$NON-NLS-2$
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (nameTranslateLink != null && !nameTranslateLink.isDisposed()) {
				nameTranslateLink.setText(text);
			}
		}, (message) -> {
			if (nameTranslateLink != null && !nameTranslateLink.isDisposed()) {
				return getUpdatedWidgetText("<a>" + message.objectTranslationLinkText + "</a>", nameTranslateLink); //$NON-NLS-1$ //$NON-NLS-2$
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (lblParent != null && !lblParent.isDisposed()) {
				lblParent.setText(text);
			}
		}, (message) -> {
			if (lblParent != null && !lblParent.isDisposed()) {
				return getUpdatedWidgetText(message.objectParentLabel, lblParent);
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (this.userApplicationModel != null && txtName != null && !txtName.isDisposed()) {
				txtName.setText(text);
			}
		}, (message) -> {
			if (this.userApplicationModel != null && txtName != null && !txtName.isDisposed()) {
				return XMSystemUtil.isEmpty(this.userApplicationModel.getName()) ? CommonConstants.EMPTY_STR : this.userApplicationModel.getName();
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (this.userApplicationModel != null && txtDesc != null && !txtDesc.isDisposed()) {
				txtDesc.setText(text);
			}
		}, (message) -> {
			if (userApplicationModel != null && txtDesc != null && !txtDesc.isDisposed()) {
				return XMSystemUtil.isEmpty(this.userApplicationModel.getDescription()) ? CommonConstants.EMPTY_STR : this.userApplicationModel.getDescription();
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (txtRemarks != null && !txtRemarks.isDisposed()) {
				txtRemarks.setText(text);
			}
		}, (message) -> {
			if (userApplicationModel != null && txtRemarks != null && !txtRemarks.isDisposed()) {
				LANG_ENUM langEnum = XMAdminUtil.getInstance().getCurrentLocaleEnum();
				return this.userApplicationModel.getRemarks(langEnum) == null ? CommonConstants.EMPTY_STR
						: this.userApplicationModel.getRemarks(langEnum);
			}
			return CommonConstants.EMPTY_STR;
		});

		registry.register((text) -> {
			if (txtPosition != null && !txtPosition.isDisposed()) {
				txtPosition.setText(text);
				updatePosition();
			}
		}, (message) -> {
			if (userApplicationModel != null && txtPosition != null && !txtPosition.isDisposed()) {
				return this.userApplicationModel.getPosition() == null ? CommonConstants.EMPTY_STR
						: this.userApplicationModel.getPosition();
			}
			return CommonConstants.EMPTY_STR;
		});
	}

	/**
	 * Gets the updated widget text.
	 *
	 * @param message
	 *            {@link String}
	 * @param control
	 *            {@link Control}
	 * @return the updated widget text
	 */
	private String getUpdatedWidgetText(String message, Control control) {
		control.requestLayout();
		control.getParent().redraw();
		control.getParent().getParent().update();
		control.getParent().getParent().getParent().update();
		return message;
	}

	/**
	 * Gets the old model.
	 *
	 * @return the old model
	 */
	public UserApplication getOldModel() {
		return oldModel;
	}

	/**
	 * Sets the old model.
	 *
	 * @param oldModel
	 *            the new old model
	 */
	public void setOldModel(UserApplication oldModel) {
		this.oldModel = oldModel;
	}

	/**
	 * Method for Open desc dialog.
	 *
	 * @param shell
	 *            {@link Shell}
	 */
	private void openDescDialog(final Shell shell) {
		if (userApplicationModel == null) {
			return;
		}
		/*if (userApplicationModel.getOperationMode() != CommonConstants.OPERATIONMODE.VIEW) {
			String text = txtDesc.getText();
			this.userApplicationModel.setDescription(LANG_ENUM.ENGLISH, XMSystemUtil.isEmpty(this.userApplicationModel.getDescription(LANG_ENUM.ENGLISH)) ? 
					text : this.userApplicationModel.getDescription(LANG_ENUM.ENGLISH));
			this.userApplicationModel.setDescription(LANG_ENUM.GERMAN, XMSystemUtil.isEmpty(this.userApplicationModel.getDescription(LANG_ENUM.GERMAN)) ? 
					text : this.userApplicationModel.getDescription(LANG_ENUM.GERMAN));
		}*/
		Map<LANG_ENUM, String> obModelMap = new HashMap<>();
		obModelMap.put(LANG_ENUM.ENGLISH, this.userApplicationModel.getDescription(LANG_ENUM.ENGLISH));
		obModelMap.put(LANG_ENUM.GERMAN, this.userApplicationModel.getDescription(LANG_ENUM.GERMAN));
		boolean isEditable = txtDesc.getEditable();
		ControlModel controlModel = new ControlModel(this.messages.objectDescriptionLabel, obModelMap,
				UserApplication.DESCRIPTION_LIMIT, false, isEditable);
		controlModel.initDefaultLabels(this.messages);
		final XMAdminLangTextDialog dialogArea = new XMAdminLangTextDialog(shell, controlModel);
		int retVal = dialogArea.open();
		if (retVal == IDialogConstants.OK_ID) {
			Map<LANG_ENUM, String> descriptionMap = this.userApplicationModel.getDescriptionMap();
			descriptionMap.put(LANG_ENUM.ENGLISH, controlModel.getObjectModel(LANG_ENUM.ENGLISH));
			descriptionMap.put(LANG_ENUM.GERMAN, controlModel.getObjectModel(LANG_ENUM.GERMAN));
			//updateDescWidget();
		}
	}

	/**
	 * Method for Open name dialog.
	 *
	 * @param shell
	 *            {@link Shell}
	 */
	private void openNameDialog(final Shell shell) {
		if (userApplicationModel == null) {
			return;
		}
		/*if (userApplicationModel.getOperationMode() != CommonConstants.OPERATIONMODE.VIEW) {
			String text = txtName.getText();
			this.userApplicationModel.setName(LANG_ENUM.ENGLISH, XMSystemUtil.isEmpty(this.userApplicationModel.getName(LANG_ENUM.ENGLISH)) ? 
					text : this.userApplicationModel.getName(LANG_ENUM.ENGLISH));
			this.userApplicationModel.setName(LANG_ENUM.GERMAN, XMSystemUtil.isEmpty(this.userApplicationModel.getName(LANG_ENUM.GERMAN)) ? 
					text : this.userApplicationModel.getName(LANG_ENUM.GERMAN));
		}*/

		Map<LANG_ENUM, String> obModelMap = new HashMap<>();
		obModelMap.put(LANG_ENUM.ENGLISH, this.userApplicationModel.getName(LANG_ENUM.ENGLISH));
		obModelMap.put(LANG_ENUM.GERMAN, this.userApplicationModel.getName(LANG_ENUM.GERMAN));
		boolean isEditable = txtName.getEditable();

		ControlModel controlModel = new ControlModel(this.messages.objectNameLabel, obModelMap,
				UserApplication.NAME_LIMIT, false, isEditable);
		controlModel.initDefaultLabels(this.messages);
		final XMAdminLangTextDialog dialogArea = new XMAdminLangTextDialog(shell, controlModel, messages);
		int retVal = dialogArea.open();
		if (retVal == IDialogConstants.OK_ID) {
			Map<LANG_ENUM, String> nameMap = this.userApplicationModel.getNameMap();
			nameMap.put(LANG_ENUM.ENGLISH, controlModel.getObjectModel(LANG_ENUM.ENGLISH));
			nameMap.put(LANG_ENUM.GERMAN, controlModel.getObjectModel(LANG_ENUM.GERMAN));
		}
	}

	/**
	 * Method for Open remark dialog.
	 *
	 * @param shell
	 *            {@link Shell}
	 */
	private void openRemarkDialog(final Shell shell) {
		if (userApplicationModel == null) {
			return;
		}
		if (userApplicationModel.getOperationMode() != CommonConstants.OPERATIONMODE.VIEW) {
			String text = txtRemarks.getText();
			LANG_ENUM currentLocaleEnum = XMAdminUtil.getInstance().getCurrentLocaleEnum();
			userApplicationModel.setRemarks(currentLocaleEnum, text);
		}

		Map<LANG_ENUM, String> obModelMap = new HashMap<>();
		obModelMap.put(LANG_ENUM.ENGLISH, this.userApplicationModel.getRemarks(LANG_ENUM.ENGLISH));
		obModelMap.put(LANG_ENUM.GERMAN, this.userApplicationModel.getRemarks(LANG_ENUM.GERMAN));
		boolean isEditable = txtRemarks.getEditable();

		ControlModel controlModel = new ControlModel(this.messages.objectRemarkLabel, obModelMap,
				UserApplication.REMARK_LIMIT, false, isEditable);
		controlModel.initDefaultLabels(this.messages);
		final XMAdminLangTextAreaDialog dialogArea = new XMAdminLangTextAreaDialog(shell, controlModel);
		int retVal = dialogArea.open();
		if (retVal == IDialogConstants.OK_ID) {
			Map<LANG_ENUM, String> remarksMap = this.userApplicationModel.getRemarksMap();
			remarksMap.put(LANG_ENUM.ENGLISH, controlModel.getObjectModel(LANG_ENUM.ENGLISH));
			remarksMap.put(LANG_ENUM.GERMAN, controlModel.getObjectModel(LANG_ENUM.GERMAN));
			updateRemarksWidget();
		}
	}

	/**
	 * Method for Creates the user app operation.
	 */
	private void createUserAppOperation() {

		try {
			UserAppController userAppController = new UserAppController();
			UserApplicationsTbl userAppVo = userAppController.createUserApplication(mapVOObjectWithModel());
			boolean isParent = this.parentBtn.getSelection();
			AdminTreeFactory instance = AdminTreeFactory.getInstance();
			String parentUserApp = this.userApplicationModel.getPosition();
			Map<String, IAdminTreeChild> userApplications = instance.getUserApplications().getUserApplications();
			UserApplication userApplication = (UserApplication) userApplications.get(parentUserApp);

			String userApplicationId = userAppVo.getUserApplicationId();
			if (!XMSystemUtil.isEmpty(userApplicationId)) {
				this.userApplicationModel.setUserApplicationId(userApplicationId);
				Collection<UserAppTranslationTbl> userAppTranslationTblCollection = userAppVo
						.getUserAppTranslationTblCollection();
				for (UserAppTranslationTbl userAppTranslationTbl : userAppTranslationTblCollection) {
					String userAppTranslationId = userAppTranslationTbl.getUserAppTranslationId();
					LanguagesTbl languageCode = userAppTranslationTbl.getLanguageCode();
					LANG_ENUM langEnum = LANG_ENUM.getLangEnum(languageCode.getLanguageCode());
					this.userApplicationModel.setTranslationId(langEnum, userAppTranslationId);
				}

				// Attach model to old model
				if (oldModel == null && (isParent || parentUserApp == null
						|| Arrays.asList(CommonConstants.Application.POSITIONS).contains(parentUserApp))) {
					setOldModel(userApplicationModel.deepCopyUserApplication(false, null));
					instance.getUserApplications().add(userApplicationId, getOldModel());
				} else if (oldModel == null && parentUserApp != null) {
					userApplicationChildModel = new UserApplicationChild(userApplicationId,
							userApplicationModel.getName(),
							userApplicationModel.getDescription(),
							userApplicationModel.getNameMap(), userApplicationModel.isActive(),
							userApplicationModel.getDescriptionMap(), userApplicationModel.getRemarksMap(),
							userApplicationModel.getIcon(), userApplicationModel.isParent(),
							userApplicationModel.isSingleton(), userApplicationModel.getPosition(),
							userApplicationModel.getBaseApplicationId(), userApplicationModel.getOperationMode());
					userApplicationChildModel.setTranslationIdMap(userApplicationModel.getTranslationIdMap());
					setOldModel(userApplicationChildModel.deepCopyUserApplicationChild(false, null));
					userApplication.add(userApplicationChildModel.getUserApplicationId(), getOldModel());
				}
				this.dirty.setDirty(false);
				AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
				adminTree.refresh(true);

				XMAdminUtil.getInstance().updateLogFile(messages.userApplicationObject + " " + "'"
						+ this.userApplicationModel.getName() + "'" + " " + messages.objectCreate,
						MessageType.SUCCESS);
				
				adminTree.setSelection(new StructuredSelection(instance.getApplications()), true);
				TreePath[] selectionPaths = adminTree.getStructuredSelection().getPaths();
				if (selectionPaths != null && selectionPaths.length > 0) {
					adminTree.setExpandedState(selectionPaths[0], true);
				}

				adminTree.setSelection(new StructuredSelection(instance.getUserApplications()), true);
				selectionPaths = adminTree.getStructuredSelection().getPaths();
				if (selectionPaths != null && selectionPaths.length > 0) {
					adminTree.setExpandedState(selectionPaths[0], true);
				}

				if(!getOldModel().isParent()){
					adminTree.setSelection(new StructuredSelection(getOldModel().getParent()), true);
					selectionPaths = adminTree.getStructuredSelection().getPaths();
					if (selectionPaths != null && selectionPaths.length > 0) {
						adminTree.setExpandedState(selectionPaths[0], true);
					}
				}
				adminTree.setSelection(new StructuredSelection(getOldModel()), true);
				
			}
		} catch (UnauthorizedAccessException e) {
			CustomMessageDialog.openError(this.getShell(), messages.objectPermissionDialogTitle,
					messages.objectPermissionDialogMsg);
		}catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				CustomMessageDialog.openError(Display.getCurrent().getActiveShell(), messages.errorDialogTitile,
						messages.serverNotReachable);
			}
			LOGGER.warn("Unable to Save data ! " + e); //$NON-NLS-1$
		}
	}

	/**
	 * Method for Change user app operation.
	 */
	private void changeUserAppOperation() {
		try {
			UserAppController userAppController = new UserAppController();
			AdminTreeFactory instance = AdminTreeFactory.getInstance();
			boolean isUpdated = userAppController.updateUserApplication(mapVOObjectWithModel());
			if (isUpdated) {
				String oldPosition = getOldModel().getPosition();
				String newPostion = userApplicationModel.getPosition();
				boolean isOldPosConstant = false;
				boolean isNewPosConstant = false;
				setOldModel(userApplicationModel.deepCopyUserApplication(true, getOldModel()));
				if (!oldPosition.equals(newPostion)) {
					for (String positionType : CommonConstants.Application.POSITIONS) {
						if (oldPosition.equalsIgnoreCase(positionType)) {
							isOldPosConstant = true;
						}
					}
					for (String positionType : CommonConstants.Application.POSITIONS) {
						if (newPostion.equalsIgnoreCase(positionType)) {
							isNewPosConstant = true;
						}
					}
					if (isNewPosConstant && !isOldPosConstant) {
						UserApplications userApplications = instance.getUserApplications();
						Map<String, IAdminTreeChild> userApplicationsChild = userApplications.getUserApplications();
						UserApplication userApplication = (UserApplication) userApplicationsChild.get(oldPosition);
						userApplication.getUserApplicationChildren()
								.remove(this.userApplicationModel.getUserApplicationId());
						if (UserApplicationChild.class.getSimpleName().equals(getOldModel().getClass().getSimpleName())) {
							UserApplication userApp = new UserApplication(userApplicationModel.getUserApplicationId(),
									userApplicationModel.getName(), userApplicationModel.getDescription(), userApplicationModel.getNameMap(),
									userApplicationModel.isActive(), userApplicationModel.getDescriptionMap(),
									userApplicationModel.getRemarksMap(), userApplicationModel.getIcon(), userApplicationModel.isParent(),
									userApplicationModel.isSingleton(), newPostion,userApplicationModel.getBaseApplicationId(),
									userApplicationModel.getOperationMode());
							userApp.setTranslationIdMap(userApplicationModel.getTranslationIdMap());
							setOldModel(userApp.deepCopyUserApplication(false, null));
						}
						userApplications.add(getOldModel().getUserApplicationId(), getOldModel());
					} else if (!isNewPosConstant && isOldPosConstant) {
						UserApplications userApplications = instance.getUserApplications();
						Map<String, IAdminTreeChild> userApplicationsChild = userApplications.getUserApplications();
						userApplicationsChild.remove(this.userApplicationModel.getUserApplicationId());
						UserApplication userApplication = (UserApplication) userApplicationsChild.get(newPostion);
						userApplicationChildModel = new UserApplicationChild(this.userApplicationModel.getUserApplicationId(),
								this.userApplicationModel.getName(),
								this.userApplicationModel.getDescription(),
								this.userApplicationModel.getNameMap(), this.userApplicationModel.isActive(),
								this.userApplicationModel.getDescriptionMap(), this.userApplicationModel.getRemarksMap(),
								this.userApplicationModel.getIcon(), this.userApplicationModel.isParent(),
								this.userApplicationModel.isSingleton(), this.userApplicationModel.getPosition(),
								this.userApplicationModel.getBaseApplicationId(), this.userApplicationModel.getOperationMode());
						userApplicationChildModel.setTranslationIdMap(this.userApplicationModel.getTranslationIdMap());
						setOldModel(userApplicationChildModel.deepCopyUserApplicationChild(false, null));
						userApplication.add(getOldModel().getUserApplicationId(), getOldModel());
					} else if (!isNewPosConstant && !isOldPosConstant) {
						UserApplications userApplications = instance.getUserApplications();
						Map<String, IAdminTreeChild> userApplicationsChild = userApplications.getUserApplications();
						UserApplication userApplication = (UserApplication) userApplicationsChild.get(oldPosition);
						userApplication.getUserApplicationChildren()
								.remove(this.userApplicationModel.getUserApplicationId());
						userApplication = (UserApplication) userApplicationsChild.get(newPostion);
						userApplication.add(getOldModel().getUserApplicationId(), getOldModel());
					}

				}

				this.userApplicationModel.setOperationMode(CommonConstants.OPERATIONMODE.VIEW);
				setOperationMode();
				this.dirty.setDirty(false);
				setShowButtonBar(false);
				final UserApplications userApplications = AdminTreeFactory.getInstance().getUserApplications();
				userApplications.sort();
				AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
				adminTree.refresh(true);
				
				XMAdminUtil.getInstance().updateLogFile(messages.userApplicationObject + " " + "'"
						+ this.userApplicationModel.getName() + "'" + " " + messages.objectUpdate,
						MessageType.SUCCESS);
				
				if(!getOldModel().isParent()){
					adminTree.setSelection(new StructuredSelection(getOldModel().getParent()), true);
					TreePath[]  selectionPaths = adminTree.getStructuredSelection().getPaths();
					if (selectionPaths != null && selectionPaths.length > 0) {
						adminTree.setExpandedState(selectionPaths[0], true);
					}
				}
				adminTree.setSelection(new StructuredSelection(getOldModel()), true);
				
			}
		} catch (UnauthorizedAccessException e) {
			CustomMessageDialog.openError(this.getShell(), messages.objectPermissionDialogTitle,
					messages.objectPermissionDialogMsg);
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				CustomMessageDialog.openError(Display.getCurrent().getActiveShell(), messages.errorDialogTitile,
						messages.serverNotReachable);
			}
			LOGGER.warn("Unable to Update data ! " + e); //$NON-NLS-1$
		}
	}

	/**
	 * Method for Map VO object with model.
	 *
	 * @return the com.magna.xmbackend.vo.user application. user application
	 *         request {@link UserApplicationRequest}
	 */
	private com.magna.xmbackend.vo.userApplication.UserApplicationRequest mapVOObjectWithModel() {
		com.magna.xmbackend.vo.userApplication.UserApplicationRequest userApplicationRequest = new com.magna.xmbackend.vo.userApplication.UserApplicationRequest();

		userApplicationRequest.setId(userApplicationModel.getUserApplicationId());
		userApplicationRequest.setName(this.userApplicationModel.getName());
		userApplicationRequest.setDescription(this.userApplicationModel.getDescription());
		userApplicationRequest.setIconId(userApplicationModel.getIcon().getIconId());
		userApplicationRequest.setStatus(userApplicationModel.isActive() == true ? com.magna.xmbackend.vo.enums.Status.ACTIVE.name()
						: com.magna.xmbackend.vo.enums.Status.INACTIVE.name());
		userApplicationRequest.setPosition(userApplicationModel.getPosition());
		userApplicationRequest.setIsSingleTon(Boolean.toString(this.userApplicationModel.isSingleton()));

		userApplicationRequest.setBaseAppId(userApplicationModel.getBaseApplicationId());
		userApplicationRequest.setIsParent(Boolean.toString(userApplicationModel.isParent()));

		List<com.magna.xmbackend.vo.userApplication.UserApplicationTranslation> userAppTransulationList = new ArrayList<>();
		LANG_ENUM[] lang_values = LANG_ENUM.values();
		for (int index = 0; index < lang_values.length; index++) {
			com.magna.xmbackend.vo.userApplication.UserApplicationTranslation userAppTransulation = new com.magna.xmbackend.vo.userApplication.UserApplicationTranslation();
			userAppTransulation.setLanguageCode(lang_values[index].getLangCode());
			userAppTransulation.setName(userApplicationModel.getName(lang_values[index]));
			userAppTransulation.setRemarks(userApplicationModel.getRemarks(lang_values[index]));
			userAppTransulation.setDescription(userApplicationModel.getDescription(lang_values[index]));

			if (this.userApplicationModel.getOperationMode() == CommonConstants.OPERATIONMODE.CHANGE) {
				String translationId = userApplicationModel.getTranslationId(lang_values[index]);
				userAppTransulation
						.setId(XMSystemUtil.isEmpty(translationId) ? CommonConstants.EMPTY_STR : translationId);
			}
			userAppTransulationList.add(userAppTransulation);
		}
		userApplicationRequest.setUserApplicationTranslations(userAppTransulationList);
		return userApplicationRequest;
	}

	/**
	 * Method for Sets the operation mode.
	 */
	public void setOperationMode() {
		if (this.userApplicationModel != null) {
			if (userApplicationModel.getOperationMode() == CommonConstants.OPERATIONMODE.VIEW) {
				this.txtName.setEditable(false);
				this.txtDesc.setEditable(false);
				this.txtRemarks.setEditable(false);
				this.activeBtn.setEnabled(false);
				this.parentBtn.setEnabled(false);
				this.btnSingleton.setEnabled(false);
				this.txtBaseApplication.setEnabled(false);
				setShowButtonBar(false);
				this.toolItem.setData("editable", false); //$NON-NLS-1$
				this.programToolItem.setData("editable", false); //$NON-NLS-1$
				this.programAppToolItem.setData("editable", false); //$NON-NLS-1$
			} else if (userApplicationModel.getOperationMode() == CommonConstants.OPERATIONMODE.CREATE) {
				this.saveBtn.setEnabled(false);
				this.txtName.setEditable(true);
				this.txtDesc.setEditable(true);
				this.txtRemarks.setEditable(true);
				this.activeBtn.setEnabled(true);
				this.parentBtn.setEnabled(true);
				this.btnSingleton.setEnabled(true);
				this.txtBaseApplication.setEnabled(!this.parentBtn.getSelection());
				this.toolItem.setData("editable", true); //$NON-NLS-1$
				this.programToolItem.setData("editable", true); //$NON-NLS-1$
				this.programAppToolItem.setData("editable", true); //$NON-NLS-1$
				setShowButtonBar(true);
				this.dirty.setDirty(true);
			} else if (userApplicationModel.getOperationMode() == CommonConstants.OPERATIONMODE.CHANGE) {
				this.txtName.setEditable(true);
				this.txtDesc.setEditable(true);
				this.txtRemarks.setEditable(true);
				this.activeBtn.setEnabled(true);
				this.btnSingleton.setEnabled(!getOldModel().isParent());
				this.txtBaseApplication.setEnabled(!this.parentBtn.getSelection());
				this.toolItem.setData("editable", true); //$NON-NLS-1$
				this.programToolItem.setData("editable", true); //$NON-NLS-1$
				this.programAppToolItem.setData("editable", true); //$NON-NLS-1$
				setShowButtonBar(true);
				this.dirty.setDirty(true);
				if (UserApplication.class.getSimpleName().equals(userApplicationModel.getClass().getSimpleName())) {
					this.parentBtn.setEnabled(!userApplicationModel.isParent());
				} else {
					this.parentBtn.setEnabled(false);
				}
			} else {
				this.txtName.setEditable(false);
				this.txtDesc.setEditable(false);
				this.txtRemarks.setEditable(false);
				this.activeBtn.setEnabled(false);
				this.parentBtn.setEnabled(false);
				this.btnSingleton.setEnabled(false);
				this.txtBaseApplication.setEnabled(false);
				this.toolItem.setData("editable", false); //$NON-NLS-1$
				this.programToolItem.setData("editable", false); //$NON-NLS-1$
				this.programAppToolItem.setData("editable", false); //$NON-NLS-1$
				setShowButtonBar(false);
			}
		}
	}

	/**
	 * Method for Save handler.
	 */
	public void saveHandler() {
		saveNameDescAndRemarks();
		// validate the model
		if (validate()) { // check name map values
			if (userApplicationModel.getOperationMode() == CommonConstants.OPERATIONMODE.CREATE) {
				createUserAppOperation();
			} else if (userApplicationModel.getOperationMode() == CommonConstants.OPERATIONMODE.CHANGE) {
				changeUserAppOperation();
			}
		}
	}

	/**
	 * Method for Save desc and remarks.
	 */
	private void saveNameDescAndRemarks() {
		/*String nameText = txtName.getText();
		userApplicationModel.setName(LANG_ENUM.ENGLISH,
				XMSystemUtil.isEmpty(this.userApplicationModel.getName(LANG_ENUM.ENGLISH)) ? nameText
						: this.userApplicationModel.getName(LANG_ENUM.ENGLISH));
		userApplicationModel.setName(LANG_ENUM.GERMAN,
				XMSystemUtil.isEmpty(this.userApplicationModel.getName(LANG_ENUM.GERMAN)) ? nameText
						: this.userApplicationModel.getName(LANG_ENUM.GERMAN));*/

		final LANG_ENUM currentLocaleEnum = XMAdminUtil.getInstance().getCurrentLocaleEnum();
		final String remarks = txtRemarks.getText();
		userApplicationModel.setRemarks(currentLocaleEnum, remarks);

	}

	/**
	 * Sets the dirty object.
	 *
	 * @param dirty
	 *            the new dirty object
	 */
	@Persist
	public void setDirtyObject(final MDirtyable dirty) {
		this.dirty = dirty;
	}

	/**
	 * Method for Update remarks widget.
	 */
	public void updateRemarksWidget() {
		if (this.userApplicationModel == null) {
			return;
		}
		int operationMode = this.userApplicationModel.getOperationMode();
		final LANG_ENUM currentLocaleEnum = XMAdminUtil.getInstance().getCurrentLocaleEnum();

		String remarkForCurLocale = this.userApplicationModel.getRemarks(currentLocaleEnum) == null
				? CommonConstants.EMPTY_STR : this.userApplicationModel.getRemarks(currentLocaleEnum);
		if (operationMode != CommonConstants.OPERATIONMODE.VIEW) {
			this.txtRemarks.setText(remarkForCurLocale);
			return;
		}
		if (operationMode == CommonConstants.OPERATIONMODE.VIEW) {
			if (!XMSystemUtil.isEmpty(remarkForCurLocale)) {
				this.txtRemarks.setText(remarkForCurLocale);
				return;
			}
			final String remarkEN = this.userApplicationModel.getRemarks(LANG_ENUM.ENGLISH);
			final String remarkDE = this.userApplicationModel.getRemarks(LANG_ENUM.GERMAN);
			if (!XMSystemUtil.isEmpty(remarkEN)) {
				this.txtRemarks.setText(remarkEN);
				return;
			}

			if (!XMSystemUtil.isEmpty(remarkDE)) {
				this.txtRemarks.setText(remarkDE);
				return;
			}
		}
	}

	/**
	 * Method for Update desc widget.
	 */
	/*public void updateDescWidget() {
		if (this.userApplicationModel == null) {
			return;
		}
		int operationMode = this.userApplicationModel.getOperationMode();
		LANG_ENUM currentLocaleEnum = XMAdminUtil.getInstance().getCurrentLocaleEnum();
		String descForCurLocale = this.userApplicationModel.getDescription(currentLocaleEnum) == null
				? CommonConstants.EMPTY_STR : this.userApplicationModel.getDescription(currentLocaleEnum);
		if (operationMode != CommonConstants.OPERATIONMODE.VIEW) {
			this.txtDesc.setText(descForCurLocale);
			return;
		}
		if (operationMode == CommonConstants.OPERATIONMODE.VIEW) {
			if (!XMSystemUtil.isEmpty(descForCurLocale)) {
				this.txtDesc.setText(descForCurLocale);
				return;
			}
			final String descriptionEN = this.userApplicationModel.getDescription(LANG_ENUM.ENGLISH);
			final String descriptionDE = this.userApplicationModel.getDescription(LANG_ENUM.GERMAN);
			if (!XMSystemUtil.isEmpty(descriptionEN)) {
				this.txtDesc.setText(descriptionEN);
				return;
			}

			if (!XMSystemUtil.isEmpty(descriptionDE)) {
				this.txtDesc.setText(descriptionDE);
				return;
			}
		}
	}*/

	/**
	 * Method for Update name widget.
	 */
	/*public void updateNameWidget() {
		if (this.userApplicationModel == null) {
			return;
		}
		int operationMode = this.userApplicationModel.getOperationMode();
		LANG_ENUM currentLocaleEnum = XMAdminUtil.getInstance().getCurrentLocaleEnum();
		final String nameEN = this.userApplicationModel.getName(LANG_ENUM.ENGLISH);
		final String nameDE = this.userApplicationModel.getName(LANG_ENUM.GERMAN);
		String nameForCurLocale = this.userApplicationModel.getName(currentLocaleEnum) == null
				? CommonConstants.EMPTY_STR : this.userApplicationModel.getName(currentLocaleEnum);
		if (operationMode != CommonConstants.OPERATIONMODE.VIEW) {
			this.txtName.setText(nameForCurLocale);
			if (currentLocaleEnum == LANG_ENUM.ENGLISH) {
				GridData layoutData = (GridData) this.txtNameGermanLang.getLayoutData();
				GridData layoutData2 = (GridData) this.lblNameGerman1.getLayoutData();
				GridData layoutData3 = (GridData) this.lblNameGerman2.getLayoutData();
				layoutData.exclude = true;
				layoutData2.exclude = true;
				layoutData3.exclude = true;
				this.txtNameGermanLang.setVisible(false);
				this.lblNameGerman1.setVisible(false);
				this.lblNameGerman2.setVisible(false);

				this.txtNameGermanLang.getParent().layout(false);
				this.lblNameGerman1.getParent().layout(false);
				this.lblNameGerman2.getParent().layout(false);
			}

			if (!XMSystemUtil.isEmpty(nameEN)) {
				this.txtNameGermanLang.setText(nameEN);
				return;
			} else if (!XMSystemUtil.isEmpty(nameDE)) {
				this.txtNameGermanLang.setText(nameDE);
				return;
			}
			return;
		}

		if (operationMode == CommonConstants.OPERATIONMODE.VIEW) {
			if (!XMSystemUtil.isEmpty(nameForCurLocale)) {
				this.txtName.setText(nameForCurLocale);
				return;
			}

			if (!XMSystemUtil.isEmpty(nameEN)) {
				this.txtName.setText(nameEN);
				return;
			}

			if (!XMSystemUtil.isEmpty(nameDE)) {
				this.txtName.setText(nameDE);
				return;
			}
		}
	}*/

	/**
	 * Method for Update position.
	 */
	public void updatePosition() {
		if (this.userApplicationModel == null) {
			return;
		}
		AdminTreeFactory instance = AdminTreeFactory.getInstance();
		if (instance != null) {

			String parent = this.userApplicationModel.getPosition();
			if (!XMSystemUtil.isEmpty(parent)) {
				for (String positionType : CommonConstants.Application.POSITIONS) {
					if (parent.equalsIgnoreCase(positionType)) {
						this.txtPosition.setText(parent);
						return;
					}
				}
				final Map<String, IAdminTreeChild> userApplications = instance.getUserApplications().getUserApplications();
				final UserApplication userApplication = (UserApplication) userApplications.get(parent);
				final String name = userApplication.getName();
				if (!XMSystemUtil.isEmpty(name)) {
					this.txtPosition.setText(name);
				}
			}
		}
	}

	/**
	 * Method for Update base application.
	 */
	public void updateBaseApplication() {
		if (this.userApplicationModel == null) {
			return;
		}
		AdminTreeFactory instance = AdminTreeFactory.getInstance();
		if (instance != null) {
			String baseAppId = userApplicationModel.getBaseApplicationId();
			if (!XMSystemUtil.isEmpty(baseAppId)) {
				Map<String, IAdminTreeChild> baseApplications = instance.getBaseApplications().getBaseApplications();
				BaseApplication baseApplication = (BaseApplication) baseApplications.get(baseAppId);
				String name = baseApplication.getName();
				if (!XMSystemUtil.isEmpty(name)) {
					this.txtBaseApplication.setText(name);
				}
			} else {
				this.txtBaseApplication.setText(CommonConstants.EMPTY_STR);
			}
		}
	}

	/**
	 * Method for Cancel handler.
	 */
	public void cancelHandler() {
		if (this.userApplicationModel == null) {
			dirty.setDirty(false);
			return;
		}
		String userAppId = CommonConstants.EMPTY_STR;
		int operationMode = this.userApplicationModel.getOperationMode();
		final UserApplication oldModel = getOldModel();
		if (oldModel != null) {
			userAppId = oldModel.getUserApplicationId();
		}
		setUserApplicationModel(null);
		setOldModel(null);
		this.saveBtn.setEnabled(true);
		final AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
		AdminTreeFactory instance = AdminTreeFactory.getInstance();
		final UserApplications userApps = instance.getUserApplications();
		dirty.setDirty(false);
		if (operationMode == CommonConstants.OPERATIONMODE.CHANGE) {
			final IStructuredSelection selection = (IStructuredSelection) adminTree.getSelection();
			final Object firstElement = selection.getFirstElement();
			Map<String, IAdminTreeChild> userApplications = userApps.getUserApplications();
			if (firstElement != null) {
				if (firstElement.equals(userApplications.get(userAppId))) {
					adminTree.setSelection(new StructuredSelection(userApplications.get(userAppId)), true);
				} else {
					final Collection<IAdminTreeChild> userAppsCollection = (userApps).getUserAppCollection();
					for (IAdminTreeChild iAdminTreeChild : userAppsCollection) {
						if (iAdminTreeChild instanceof UserApplication) {
							UserApplication userApplication = (UserApplication) iAdminTreeChild;
							if (userApplication.isParent()) {
								UserApplication childuserApp = (UserApplication) userApplication
										.getUserApplicationChildren().get(userAppId);
								if ((childuserApp != null)) {
									if (childuserApp.equals(firstElement)) {
										adminTree.setSelection(new StructuredSelection(childuserApp), true);
										break;
									}
								}
							}
						}
					}
				}
			}
		} else {
			if (!adminTree.getExpandedState(instance.getApplications())) {
				adminTree.setSelection(new StructuredSelection(instance.getApplications()), true);
			} else {
				adminTree.setSelection(new StructuredSelection(userApps), true);
			}
		}
	}

	/**
	 * Method for Sets the user application.
	 */
	public void setUserApplication() {
		try {
			final AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
			final Object selectionObj = adminTree.getSelection();
			if (selectionObj instanceof IStructuredSelection) {
				Object firstElement = ((IStructuredSelection) selectionObj).getFirstElement();
				if (UserApplication.class.getSimpleName().equals(firstElement.getClass().getSimpleName())) {
					setOldModel((UserApplication) firstElement);
					UserApplication rightHandObject = this.getOldModel().deepCopyUserApplication(false, null);
					setUserApplicationModel(rightHandObject);

				} else {
					setOldModel((UserApplicationChild) firstElement);
					UserApplicationChild rightHandObject = ((UserApplicationChild) getOldModel())
							.deepCopyUserApplicationChild(false, null);
					setUserApplicationModel(rightHandObject);

				}
				registerMessages(this.registry);
				bindValues();
				setOperationMode();
				updateBaseApplication();
				updatePosition();
				initDefaultValue();
			}
		} catch (Exception e) {
			LOGGER.warn("Unable to set user application model selection ! " + e); //$NON-NLS-1$
		}
	}

	/**
	 * Sets the model.
	 *
	 * @param userApplication
	 *            the new model
	 */
	public void setModel(UserApplication userApplication) {
		try {
			setOldModel(null);
			setUserApplicationModel(userApplication);
			registerMessages(this.registry);
			bindValues();
			setOperationMode();
			//updateDescWidget();
			//updateNameWidget();
			updateRemarksWidget();
			updateBaseApplication();
			updatePosition();
			initDefaultValue();
		} catch (Exception e) {
			LOGGER.warn("Unable to set user model ! " + e); //$NON-NLS-1$
		}
	}

	private void initDefaultValue() {
		Event event = new Event();
		event.data = PROGRAMMATICALLY;
		this.parentBtn.notifyListeners(SWT.Selection, event);
	}
}
