package com.magna.xmsystem.xmadmin.ui.parts.objexpcontextmenu;

import javax.inject.Inject;

import org.eclipse.core.commands.ParameterizedCommand;
import org.eclipse.e4.core.commands.ECommandService;
import org.eclipse.e4.core.commands.EHandlerService;
import org.eclipse.e4.core.services.nls.Translation;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.viewers.IStructuredSelection;

import com.magna.xmsystem.xmadmin.message.Message;
import com.magna.xmsystem.xmadmin.ui.parts.InformationPart;
import com.magna.xmsystem.xmadmin.ui.parts.objexp.ObjectExpPage;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdministrationArea;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.baseapplicationmodel.BaseApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.projectapplicationmodel.ProjectApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.projectapplicationmodel.ProjectApplicationChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.startapplicationmodel.StartApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.userapplicationmodel.UserApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.userapplicationmodel.UserApplicationChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.livemsgconfig.LiveMessage;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.NotificationTemplate;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.ProjectActivateEvtAction;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.ProjectCreateEvtAction;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.ProjectDeactivateEvtAction;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.ProjectDeleteEvtAction;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.UserProjectRelAssignEvtAction;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.UserProjectRelRemoveEvtAction;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.Role;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.directory.Directory;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.projectappgroup.ProjectApplicationGroup;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.projectgroup.ProjectGroupModel;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.userappgroup.UserApplicationGroup;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.usergroup.UserGroupModel;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.Project;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.Site;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.User;
import com.magna.xmsystem.xmadmin.util.CommonConstants;

// TODO: Auto-generated Javadoc
/**
 * The Class DeleteAction.
 */
@SuppressWarnings("restriction")
public class DeleteAction extends Action {

	/** The messages. */
	@Inject
	@Translation
	private Message messages;

	/** The handler service. */
	@Inject
	private EHandlerService handlerService;

	/** The command service. */
	@Inject
	private ECommandService commandService;

	/** Inject of {@link EModelService}. */
	@Inject
	private EModelService modelService;

	/** Inject of {@link MApplication}. */
	@Inject
	private MApplication application;


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.action.Action#run()
	 */
	@Override
	public void run() {
		MPart mPart = (MPart) this.modelService.find(CommonConstants.PART_ID.INFORMATION_PART_ID, application);
		InformationPart rightSidePart2 = (InformationPart) mPart.getObject();
		ObjectExpPage objectExpPage = rightSidePart2.getObjectExpPartUI();
		IStructuredSelection selections = (IStructuredSelection) objectExpPage.getObjExpTableViewer().getSelection();
		Object selectionObj = selections.getFirstElement();
		if (selectionObj instanceof Site) {
			ParameterizedCommand cmd = commandService.createCommand(CommonConstants.COMMAND_ID.DELETE_SITE, null);
			if (handlerService.canExecute(cmd)) {
				handlerService.executeHandler(cmd);
			}
		} else if (selectionObj instanceof AdministrationArea) {
			ParameterizedCommand cmd = commandService.createCommand(CommonConstants.COMMAND_ID.DELETE_ADMINAREA, null);
			if (handlerService.canExecute(cmd)) {
				handlerService.executeHandler(cmd);
			}
		} else if (selectionObj instanceof Project) {
			ParameterizedCommand cmd = commandService.createCommand(CommonConstants.COMMAND_ID.DELETE_PROJECT, null);
			if (handlerService.canExecute(cmd)) {
				handlerService.executeHandler(cmd);
			}
		} else if (selectionObj instanceof User) {
			ParameterizedCommand cmd = commandService.createCommand(CommonConstants.COMMAND_ID.DELETE_USER, null);
			if (handlerService.canExecute(cmd)) {
				handlerService.executeHandler(cmd);
			}
		} else if (UserApplication.class.getSimpleName().equals(selectionObj.getClass().getSimpleName())) {
			ParameterizedCommand cmd = commandService.createCommand(CommonConstants.COMMAND_ID.DELETE_USERAPP, null);
			if (handlerService.canExecute(cmd)) {
				handlerService.executeHandler(cmd);
			}
		} else if (selectionObj instanceof UserApplicationChild) {
			ParameterizedCommand cmd = commandService.createCommand(CommonConstants.COMMAND_ID.DELETE_USERAPP_CHILD, null);
			if (handlerService.canExecute(cmd)) {
				handlerService.executeHandler(cmd);
			}
		}else if (ProjectApplication.class.getSimpleName().equals(selectionObj.getClass().getSimpleName())) {
			ParameterizedCommand cmd = commandService.createCommand(CommonConstants.COMMAND_ID.DELETE_PROJECTAPP, null);
			if (handlerService.canExecute(cmd)) {
				handlerService.executeHandler(cmd);
			}
		} else if (selectionObj instanceof ProjectApplicationChild) {
			ParameterizedCommand cmd = commandService.createCommand(CommonConstants.COMMAND_ID.DELETE_PROJECTAPP_CHILD, null);
			if (handlerService.canExecute(cmd)) {
				handlerService.executeHandler(cmd);
			}
		}else if (selectionObj instanceof StartApplication) {
			ParameterizedCommand cmd = commandService.createCommand(CommonConstants.COMMAND_ID.DELETE_STARTAPP, null);
			if (handlerService.canExecute(cmd)) {
				handlerService.executeHandler(cmd);
			}
		} else if (selectionObj instanceof BaseApplication) {
			ParameterizedCommand cmd = commandService.createCommand(CommonConstants.COMMAND_ID.DELETE_BASEAPP, null);
			if (handlerService.canExecute(cmd)) {
				handlerService.executeHandler(cmd);
			}
		} else if (selectionObj instanceof Role) {
			ParameterizedCommand cmd = commandService.createCommand(CommonConstants.COMMAND_ID.DELETE_ROLE, null);
			if (handlerService.canExecute(cmd)) {
				handlerService.executeHandler(cmd);
			}
		} else if (selectionObj instanceof UserGroupModel) {
			ParameterizedCommand cmd = commandService.createCommand(CommonConstants.COMMAND_ID.DELETE_USER_GROUP, null);
			if (handlerService.canExecute(cmd)) {
				handlerService.executeHandler(cmd);
			}
		} else if (selectionObj instanceof ProjectGroupModel) {
			ParameterizedCommand cmd = commandService.createCommand(CommonConstants.COMMAND_ID.DELETE_PROJECT_GROUP,
					null);
			if (handlerService.canExecute(cmd)) {
				handlerService.executeHandler(cmd);
			}
		} else if (selectionObj instanceof UserApplicationGroup) {
			ParameterizedCommand cmd = commandService.createCommand(CommonConstants.COMMAND_ID.DELETE_USERAPP_GROUP,
					null);
			if (handlerService.canExecute(cmd)) {
				handlerService.executeHandler(cmd);
			}
		} else if (selectionObj instanceof ProjectApplicationGroup) {
			ParameterizedCommand cmd = commandService.createCommand(CommonConstants.COMMAND_ID.DELETE_PROJECTAPP_GROUP,
					null);
			if (handlerService.canExecute(cmd)) {
				handlerService.executeHandler(cmd);
			}
		} else if (selectionObj instanceof Directory) {
			ParameterizedCommand cmd = commandService.createCommand(CommonConstants.COMMAND_ID.DELETE_DIRECTORY, null);
			if (handlerService.canExecute(cmd)) {
				handlerService.executeHandler(cmd);
			}
		} else if (selectionObj instanceof LiveMessage) {
			ParameterizedCommand cmd = commandService.createCommand(CommonConstants.COMMAND_ID.DELETE_LIVEMESSAGE,
					null);
			if (handlerService.canExecute(cmd)) {
				handlerService.executeHandler(cmd);
			}
		} else if (selectionObj instanceof ProjectCreateEvtAction) {
			ParameterizedCommand cmd = commandService.createCommand(CommonConstants.COMMAND_ID.DELETE_PRO_CREATE_ACTION,
					null);
			if (handlerService.canExecute(cmd)) {
				handlerService.executeHandler(cmd);
			}
		} else if (selectionObj instanceof ProjectDeleteEvtAction) {
			ParameterizedCommand cmd = commandService.createCommand(CommonConstants.COMMAND_ID.DELETE_PRO_DELETE_ACTION,
					null);
			if (handlerService.canExecute(cmd)) {
				handlerService.executeHandler(cmd);
			}
		} else if (selectionObj instanceof ProjectDeactivateEvtAction) {
			ParameterizedCommand cmd = commandService.createCommand(CommonConstants.COMMAND_ID.DELETE_PRO_DEACTIVATE_ACTION,
					null);
			if (handlerService.canExecute(cmd)) {
				handlerService.executeHandler(cmd);
			}
		} else if (selectionObj instanceof ProjectActivateEvtAction) {
			ParameterizedCommand cmd = commandService.createCommand(CommonConstants.COMMAND_ID.DELETE_PRO_ACTIVATE_ACTION,
					null);
			if (handlerService.canExecute(cmd)) {
				handlerService.executeHandler(cmd);
			}
		} else if (selectionObj instanceof UserProjectRelAssignEvtAction) {
			ParameterizedCommand cmd = commandService.createCommand(CommonConstants.COMMAND_ID.DELETE_USER_PRO_REL_ASSIGN_ACTION,
					null);
			if (handlerService.canExecute(cmd)) {
				handlerService.executeHandler(cmd);
			}
		} else if (selectionObj instanceof UserProjectRelRemoveEvtAction) {
			ParameterizedCommand cmd = commandService.createCommand(CommonConstants.COMMAND_ID.DELETE_USER_PRO_REL_REMOVE_ACTION,
					null);
			if (handlerService.canExecute(cmd)) {
				handlerService.executeHandler(cmd);
			}
		} else if (selectionObj instanceof NotificationTemplate) {
			ParameterizedCommand cmd = commandService.createCommand(CommonConstants.COMMAND_ID.DELETE_TEMPLATE, null);
			if (handlerService.canExecute(cmd)) {
				handlerService.executeHandler(cmd);
			}
		}
	}
}
