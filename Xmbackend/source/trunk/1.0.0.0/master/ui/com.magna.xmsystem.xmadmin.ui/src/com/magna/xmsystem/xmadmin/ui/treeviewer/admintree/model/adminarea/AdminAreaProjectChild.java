package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.Project;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.refs.RelationObj;

/**
 * The Class AdminAreaProChild.
 * 
 * @author subash.janarthanan
 * 
 */
public class AdminAreaProjectChild implements IAdminTreeChild {

	/** The parent. */
	private IAdminTreeChild parent;

	/** The admin area pro children. */
	private Map<String, IAdminTreeChild> adminAreaProjectChildren;
	
	/**
	 * Instantiates a new admin area pro child.
	 *
	 * @param parent the parent
	 */
	public AdminAreaProjectChild(final IAdminTreeChild parent) {
		this.parent = parent;
		this.adminAreaProjectChildren = new LinkedHashMap<>();
		/*this.adminAreaProChildren.put(AdminAreaProChildEnum.PROJECTAPPLICATIONS,
				new AdminAreaProjectApplications(this));
		this.adminAreaProChildren.put(AdminAreaProChildEnum.STARTAPPLICATIONS,
				new AdminAreaStartApplications(this));*/
	}

	/**
	 * Gets the admin areas pro collection.
	 *
	 * @return the admin areas pro collection
	 */
	public Collection<IAdminTreeChild> getAdminAreasProCollection() {
		return this.adminAreaProjectChildren.values();
	}

	/**
	 * Gets the admin area pro children.
	 *
	 * @return the admin area pro children
	 */
	public Map<String, IAdminTreeChild> getAdminAreaProChildren() {
		return adminAreaProjectChildren;
	}
	
	/**
	 * Add.
	 *
	 * @param adminAreaProjChildId the admin area proj child id
	 * @param child the child
	 * @return the i admin tree child
	 */
	public IAdminTreeChild add(final String adminAreaProjChildId, final IAdminTreeChild child) {
		child.setParent(this);
		IAdminTreeChild returnVal = this.adminAreaProjectChildren.put(adminAreaProjChildId, child);
		if (child instanceof RelationObj && ((RelationObj) child).getRefObject() instanceof Project) {
			sort();
		}
		return returnVal;
	}
	
	/**
	 * Remove.
	 *
	 * @param adminAreaProjChildId the admin area proj child id
	 * @return the i admin tree child
	 */
	public IAdminTreeChild remove(final String adminAreaProjChildId) {
		return this.adminAreaProjectChildren.remove(adminAreaProjChildId);
	}
	

	/**
	 * Remove all.
	 */
	public void removeAll() {
		this.adminAreaProjectChildren.clear();
	}

	/**
	 * Add fixed children.
	 *
	 * @param adminProjChildRel the admin proj child rel
	 */
	public void addFixedChildren(RelationObj adminProjChildRel) {
		adminAreaProjectChildren.put(AdminAreaProjectApplications.class.getName(), new AdminAreaProjectApplications(adminProjChildRel));
		adminAreaProjectChildren.put(AdminAreaProjectStartApplications.class.getName(), new AdminAreaProjectStartApplications(adminProjChildRel));
	}

	/* (non-Javadoc)
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#getParent()
	 */
	@Override
	public IAdminTreeChild getParent() {
		return this.parent;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#
	 * setParent(com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.
	 * IAdminTreeChild)
	 */
	@Override
	public void setParent(final IAdminTreeChild parent) {
		this.parent = parent;
	}
	
	/**
	 * Sort.
	 */
	public void sort() {
		final LinkedHashMap<String, IAdminTreeChild> collect = this.adminAreaProjectChildren.entrySet().stream().sorted(
				(e1, e2) -> ((Project) (((RelationObj) e1.getValue()).getRefObject())).getName()
				.compareTo(((Project) (((RelationObj) e2.getValue()).getRefObject())).getName()))
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
		this.adminAreaProjectChildren = collect;
	}
}
