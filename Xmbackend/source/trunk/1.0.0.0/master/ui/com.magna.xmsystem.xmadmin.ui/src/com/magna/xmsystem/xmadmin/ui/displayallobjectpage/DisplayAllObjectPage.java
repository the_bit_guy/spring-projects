//TODO: Who still needs that out-commented stuff?
/*package com.magna.xmsystem.xmadmin.ui.displayallobjectpage;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.inject.Inject;

import org.eclipse.e4.core.commands.ECommandService;
import org.eclipse.e4.core.commands.EHandlerService;
import org.eclipse.e4.core.contexts.ContextInjectionFactory;
import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.services.nls.Translation;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.jface.resource.LocalResourceManager;
import org.eclipse.jface.resource.ResourceManager;
import org.eclipse.jface.util.LocalSelectionTransfer;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreePath;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.dnd.Clipboard;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.DragSource;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.events.ControlAdapter;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Text;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magna.xmsystem.dependencies.utils.XMSystemUtil;
import com.magna.xmsystem.xmadmin.message.Message;
import com.magna.xmsystem.xmadmin.message.MessageRegistry;
import com.magna.xmsystem.xmadmin.ui.handlers.CreateAsMenuHandler;
import com.magna.xmsystem.xmadmin.ui.handlers.RemoveRelationHandler;
import com.magna.xmsystem.xmadmin.ui.handlers.toolbar.CopyObjectHandler;
import com.magna.xmsystem.xmadmin.ui.handlers.toolbar.DeleteToolBarHandler;
import com.magna.xmsystem.xmadmin.ui.handlers.toolbar.EditToolBarHandler;
import com.magna.xmsystem.xmadmin.ui.parts.dnd.DispalyAllObjDragListener;
import com.magna.xmsystem.xmadmin.ui.parts.dnd.ObjectTypeTransfer;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.AdminTreeviewer;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.AdminTreeDataLoad;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.AdminTreeFactory;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaProjectAppFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaProjectAppNotFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaProjectAppProtected;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaProjectApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaProjectStartApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaProjects;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaStartApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaUserAppFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaUserAppNotFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaUserAppProtected;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdminAreaUserApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdministrationArea;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdministrationAreas;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.Applications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.baseapplicationmodel.BaseAppProjectApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.baseapplicationmodel.BaseAppStartApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.baseapplicationmodel.BaseAppUserApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.baseapplicationmodel.BaseApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.baseapplicationmodel.BaseApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.projectapplicationmodel.ProjectAppAdminAreaChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.projectapplicationmodel.ProjectAppAdminAreaProjects;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.projectapplicationmodel.ProjectAppAdminAreas;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.projectapplicationmodel.ProjectAppUsers;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.projectapplicationmodel.ProjectApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.projectapplicationmodel.ProjectApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.startapplicationmodel.StartAppAdminAreas;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.startapplicationmodel.StartAppProjects;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.startapplicationmodel.StartAppUsers;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.startapplicationmodel.StartApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.startapplicationmodel.StartApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.userapplicationmodel.UserAppAdminAreas;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.userapplicationmodel.UserAppUsers;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.userapplicationmodel.UserApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.userapplicationmodel.UserApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.Configurations;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.adminconf.AdminProjectApps;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.adminconf.AdminUserApps;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.adminconf.AdminUsers;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.icon.Icon;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.icon.Icons;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.livemsgconfig.LiveMessage;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.livemsgconfig.LiveMessages;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.Role;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.RoleScopeObjectUsers;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.RoleScopeObjects;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.RoleUsers;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.roles.Roles;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.directory.Directories;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.directory.Directory;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.directory.DirectoryApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.directory.DirectoryProjectApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.directory.DirectoryProjects;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.directory.DirectoryUserApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.directory.DirectoryUsers;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.Groups;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.projectappgroup.ProjectAppGroupProjectApps;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.projectappgroup.ProjectApplicationGroup;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.projectappgroup.ProjectApplicationGroups;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.projectgroup.ProjectGroupModel;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.projectgroup.ProjectGroupProjects;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.projectgroup.ProjectGroupsModel;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.userappgroup.UserAppGroupUserApps;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.userappgroup.UserApplicationGroup;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.userappgroup.UserApplicationGroups;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.usergroup.UserGroupModel;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.usergroup.UserGroupUsers;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.usergroup.UserGroupsModel;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.Project;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectAdminAreaChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectAdminAreaProjectAppFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectAdminAreaProjectAppNotFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectAdminAreaProjectAppProtected;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectAdminAreaProjectApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectAdminAreaStartApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectAdminAreas;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectProjectApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectUserAAProjectAppAllowed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectUserAAProjectAppForbidden;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectUserAAProjectApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectUserAdminAreaChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectUserAdminAreas;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.ProjectUsers;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.Projects;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.refs.RelationObj;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.Site;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaProjectApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaProjectStartApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaProjects;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaStartApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaUserAppFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaUserAppNotFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaUserAppProtected;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminAreaUserApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminProjectAppFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminProjectAppNotFixed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdminProjectAppProtected;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.SiteAdministrations;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel.Sites;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.User;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserAAUserAppAllowed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserAAUserAppForbidden;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserAAUserApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserAdminAreaChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserAdminAreas;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserProjectAAProjectAppAllowed;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserProjectAAProjectAppForbidden;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserProjectAAProjectApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserProjectAdminAreaChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserProjectAdminAreas;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserProjects;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UserStartApplications;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.Users;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.UsersNameAlphabet;
import com.magna.xmsystem.xmadmin.util.CommonConstants;
import com.magna.xmsystem.xmadmin.util.XMAdminUtil;

*//**
 * The Class DisplayAllObjectPage.
 * 
 * @author Archita.patel
 *//*
@SuppressWarnings("restriction")
public class DisplayAllObjectPage extends Composite {

	*//** The Constant LOGGER. *//*
	private static final Logger LOGGER = LoggerFactory.getLogger(DisplayAllObjectPage.class);

	*//** The structured selection. *//*
	private Map<String, StructuredSelection> structuredSelection;

	*//** Member variable 'registry' for {@link MessageRegistry}. *//*
	@Inject
	transient private MessageRegistry registry;

	*//** Member variable 'data composite' for {@link Composite}. *//*
	private Composite dataComposite;

	*//** Member variable 'label' for {@link CLabel[]}. *//*
	private CLabel[] label;

	*//** Member variable 'background' for {@link Color}. *//*
	private Color background;

	*//** Member variable 'foreground' for {@link Color}. *//*
	private Color foreground;

	*//** Member variable 'child obj list' for {@link List<IAdminTreeChild>}. *//*
	private List<IAdminTreeChild> childObjList;

	*//** Member variable 'resource manager' for {@link ResourceManager}. *//*
	private ResourceManager resourceManager;

	*//** The selected label. *//*
	private CLabel selectedLabel;

	*//** The is multi select enabled. *//*
	private boolean isMultiSelectEnabled;

	*//** The row layout. *//*
	private RowLayout rowLayout;

	*//** The search txt. *//*
	private Text searchTxt;

	*//** The black. *//*
	private Color black;

	*//** The white. *//*
	private Color white;

	*//** The selected color. *//*
	private Color selectedColor;

	*//** The foucsed color. *//*
	private Color foucsedColor;

	*//** The start X. *//*
	private int startX;

	*//** The start Y. *//*
	private int startY;

	*//** The end X. *//*
	private int endX;

	*//** The end Y. *//*
	private int endY;

	*//** The drag. *//*
	private boolean drag = false;

	*//** The explorer comp. *//*
	private Composite explorerComp;

	*//** The handler service. *//*
	@Inject
	private EHandlerService handlerService;

	*//** The command service. *//*
	@Inject
	private ECommandService commandService;

	*//** Member variable 'messages' for {@link Message}. *//*
	@Inject
	@Translation
	private Message messages;

	*//**
	 * Member variable for {@link IEclipseContext}
	 *//*
	@Inject
	private IEclipseContext eclipseContext;

	private ScrolledComposite scrollComposite;

	*//**
	 * Instantiates a new display all object page.
	 *
	 * @param parent
	 *            the parent
	 *//*
	@Inject
	public DisplayAllObjectPage(final Composite parent) {
		super(parent, SWT.NONE);
		this.isMultiSelectEnabled = false;
		final Display display = parent.getDisplay();
		this.black = new Color(display, 0, 0, 0);
		this.white = new Color(display, 255, 255, 255);
		this.selectedColor = new Color(display, 25, 141, 166);
		this.foucsedColor = new Color(display, 240, 240, 240);
		init();

		parent.addDisposeListener(new DisposeListener() {
			*//**
			 * overriding widget disposed method
			 *//*
			@Override
			public void widgetDisposed(final DisposeEvent event) {
				DisplayAllObjectPage.this.black.dispose();
				DisplayAllObjectPage.this.white.dispose();
				DisplayAllObjectPage.this.selectedColor.dispose();
				DisplayAllObjectPage.this.foucsedColor.dispose();
			}
		});
	}

	*//**
	 * Checks if is multi select enabled.
	 *
	 * @return the isMultiSelectEnabled
	 *//*
	public boolean isMultiSelectEnabled() {
		return isMultiSelectEnabled;
	}

	*//**
	 * Sets the multi select enabled.
	 *
	 * @param isMultiSelectEnabled
	 *            the isMultiSelectEnabled to set
	 *//*
	public void setMultiSelectEnabled(final boolean isMultiSelectEnabled) {
		this.isMultiSelectEnabled = isMultiSelectEnabled;
	}

	*//**
	 * Inits the.
	 *//*
	private void init() {
		GridLayoutFactory.fillDefaults().applyTo(this);
		this.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		createUI();
	}

	*//**
	 * Method for Creates the UI.
	 *//*
	public void createUI() {
		try {
			this.structuredSelection = new HashMap<>();
			buildComponents(this);
		} catch (Exception e) {
			LOGGER.error("Exception occured at creating UI compoents!", e); //$NON-NLS-1$
		}
	}

	*//**
	 * Method for Builds the components.
	 *
	 * @param parent
	 *            {@link Composite}
	 *//*
	final private void buildComponents(final Composite parent) {
		try {
			rowLayout = new RowLayout(SWT.VERTICAL);
			rowLayout.pack = false;
			rowLayout.justify = false;
			rowLayout.wrap = true;
			rowLayout.spacing = 1;
			buildSearchBar();
			this.explorerComp = new Composite(this, SWT.NONE);
			GridLayoutFactory.fillDefaults().numColumns(1).margins(0, 0).extendedMargins(0, 0, 0, 0)
					.applyTo(this.explorerComp);
			this.resourceManager = new LocalResourceManager(JFaceResources.getResources(), this.explorerComp);
			this.explorerComp.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		} catch (Exception e) {
			LOGGER.error("Execution occurred while dialog creation!", e);
		}
	}

	*//**
	 * Builds the search bar.
	 *//*
	private void buildSearchBar() {
		this.searchTxt = new Text(this, SWT.BORDER);
		GridDataFactory.fillDefaults().grab(true, false).align(SWT.FILL, SWT.CENTER).applyTo(this.searchTxt);

		this.searchTxt.setMessage("type filter text");
		this.searchTxt.addListener(SWT.Modify, new Listener() {

			@Override
			public void handleEvent(final Event event) {
				if (getGroup() != null) {
					final String filterText = ((Text) event.widget).getText().trim();
					filterDataComposite(filterText);
					scrollComposite.notifyListeners(SWT.Resize, new Event());
				}
			}
		});
	}

	*//**
	 * Method for Filter data composite.
	 *
	 * @param filterText
	 *            {@link String}
	 *//*
	private void filterDataComposite(final String filterText) {
		clearSelections();
		if (childObjList != null && !childObjList.isEmpty()) {
			List<IAdminTreeChild> childObjTempList = childObjList;
			IAdminTreeChild iAdminTreeChild = childObjList.get(0);
			if (iAdminTreeChild instanceof RelationObj) {
				iAdminTreeChild = ((RelationObj) iAdminTreeChild).getRefObject();
			}

			if (iAdminTreeChild instanceof Site) {
				childObjTempList = childObjList.stream()
						.filter(s -> ((Site) ((s instanceof RelationObj) ? ((RelationObj) s).getRefObject() : s))
								.getName().toLowerCase().contains(filterText.toLowerCase()))
						.collect(Collectors.toList());
			} else if (iAdminTreeChild instanceof AdministrationArea) {
				childObjTempList = childObjList.stream().filter(
						s -> ((AdministrationArea) ((s instanceof RelationObj) ? ((RelationObj) s).getRefObject() : s))
								.getName().toLowerCase().contains(filterText.toLowerCase()))
						.collect(Collectors.toList());
			} else if (iAdminTreeChild instanceof Project) {
				childObjTempList = childObjList.stream()
						.filter(s -> ((Project) ((s instanceof RelationObj) ? ((RelationObj) s).getRefObject() : s))
								.getName().toLowerCase().contains(filterText.toLowerCase()))
						.collect(Collectors.toList());
			} else if (iAdminTreeChild instanceof User) {
				childObjTempList = childObjList.stream()
						.filter(s -> ((User) ((s instanceof RelationObj) ? ((RelationObj) s).getRefObject() : s))
								.getName().toLowerCase().contains(filterText.toLowerCase()))
						.collect(Collectors.toList());
			} else if (iAdminTreeChild instanceof UserGroupModel) {
				childObjTempList = childObjList.stream()
						.filter(s -> ((UserGroupModel) s).getName().toLowerCase().contains(filterText.toLowerCase()))
						.collect(Collectors.toList());
			}else if (iAdminTreeChild instanceof ProjectGroupModel) {
				childObjTempList = childObjList.stream()
						.filter(s -> ((ProjectGroupModel) s).getName().toLowerCase().contains(filterText.toLowerCase()))
						.collect(Collectors.toList());
			}else if (iAdminTreeChild instanceof UserApplicationGroup) {
				childObjTempList = childObjList.stream()
						.filter(s -> ((UserApplicationGroup) s).getName().toLowerCase().contains(filterText.toLowerCase()))
						.collect(Collectors.toList());
			}else if (iAdminTreeChild instanceof ProjectApplicationGroup) {
				childObjTempList = childObjList.stream()
						.filter(s -> ((ProjectApplicationGroup) s).getName().toLowerCase().contains(filterText.toLowerCase()))
						.collect(Collectors.toList());
			}else if (iAdminTreeChild instanceof Directory) {
				childObjTempList = childObjList.stream()
						.filter(s -> ((Directory) s).getName().toLowerCase().contains(filterText.toLowerCase()))
						.collect(Collectors.toList());
			} else if (iAdminTreeChild instanceof UserApplication) {
				childObjTempList = childObjList.stream().filter(s -> {
					UserApplication userApplication = (UserApplication) ((s instanceof RelationObj)
							? ((RelationObj) s).getRefObject() : s);
					String name = userApplication.getName();
					return name.toLowerCase().contains(filterText.toLowerCase());
				}).collect(Collectors.toList());
			} else if (iAdminTreeChild instanceof ProjectApplication) {
				childObjTempList = childObjList.stream().filter(s -> {
					ProjectApplication projectApplication = (ProjectApplication) ((s instanceof RelationObj)
							? ((RelationObj) s).getRefObject() : s);
					String name = projectApplication.getName();
					return name.toLowerCase().contains(filterText.toLowerCase());
				}).collect(Collectors.toList());
			} else if (iAdminTreeChild instanceof StartApplication) {
				childObjTempList = childObjList.stream().filter(s -> {
					StartApplication startApplication = (StartApplication) ((s instanceof RelationObj)
							? ((RelationObj) s).getRefObject() : s);
					String name = startApplication.getName();
					return name.toLowerCase().contains(filterText.toLowerCase());
				}).collect(Collectors.toList());
			} else if (iAdminTreeChild instanceof BaseApplication) {
				childObjTempList = childObjList.stream().filter(s -> {
					BaseApplication baseApplication = (BaseApplication) ((s instanceof RelationObj)
							? ((RelationObj) s).getRefObject() : s);
					String name = baseApplication.getName();
					return name.toLowerCase().contains(filterText.toLowerCase());
				}).collect(Collectors.toList());
			}
			createDataLabels(childObjTempList);
		}
	}

	*//**
	 * Gets the group.
	 *
	 * @return the group
	 *//*
	private Group getGroup() {
		if (this.explorerComp != null && !this.explorerComp.isDisposed()) {
			Control[] children = this.explorerComp.getChildren();
			for (Control control : children) {
				if (control instanceof Group) {
					return (Group) control;
				}
			}
		}
		return null;
	}

	*//**
	 * Refesh part.
	 *//*
	public void refeshPart() {
		this.explorerComp.setRedraw(false);
		this.structuredSelection = new HashMap<>();
		if (this.explorerComp != null) {
			Control[] children = this.explorerComp.getChildren();
			for (Control control : children) {
				control.dispose();
			}
		}

		Group group = new Group(explorerComp, SWT.NONE);
		group.setLayout(new GridLayout(1, false));
		GridDataFactory.fillDefaults().grab(true, true).span(SWT.FILL, SWT.FILL).applyTo(group);

		scrollComposite = new ScrolledComposite(group, SWT.V_SCROLL | SWT.H_SCROLL);
		scrollComposite.setLayout(new GridLayout(1, false));
		GridDataFactory.fillDefaults().grab(true, true).span(SWT.FILL, SWT.FILL).applyTo(scrollComposite);

		dataComposite = new Composite(scrollComposite, SWT.NONE);
		dataComposite.setData("org.eclipse.e4.ui.css.CssClassName", "ImageLabel");
		dataComposite.setLayout(rowLayout);
		
		createDataComposite(group);

		dataComposite.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDown(final MouseEvent event) {
				if (searchTxt.getText().equals(CommonConstants.EMPTY_STR)) {
					dataComposite.setFocus();
				}
			}
		});

		dataComposite.addListener(SWT.MouseDown, new Listener() {
			@Override
			public void handleEvent(final Event event) {
				startX = event.x;
				startY = event.y;
				drag = true;
			}
		});

		dataComposite.addListener(SWT.MouseUp, new Listener() {
			@Override
			public void handleEvent(final Event event) {
				endX = event.x;
				endY = event.y;
				drag = false;

				dataComposite.redraw();
			}
		});

		dataComposite.addListener(SWT.MouseMove, new Listener() {
			@Override
			public void handleEvent(final Event event) {
				if (drag) {
					endX = event.x;
					endY = event.y;

					dataComposite.redraw();
				}
			}
		});

		dataComposite.addListener(SWT.Paint, new Listener() {
			@Override
			public void handleEvent(final Event event) {
				if (drag) {
					GC gc = event.gc;

					gc.setBackground(DisplayAllObjectPage.this.foucsedColor);
					gc.setAlpha(128);

					int minX = Math.min(startX, endX);
					int minY = Math.min(startY, endY);

					int maxX = Math.max(startX, endX);
					int maxY = Math.max(startY, endY);

					int width = maxX - minX;
					int height = maxY - minY;

					gc.fillRectangle(minX, minY, width, height);
					findElements(new Rectangle(minX, minY, width, height), gc);
				}
			}
		});

		scrollComposite.addControlListener(new ControlAdapter() {
			public void controlResized(final ControlEvent e) {
				Rectangle rectangle = scrollComposite.getClientArea();
				scrollComposite.setMinSize(dataComposite.computeSize(SWT.DEFAULT, rectangle.height));
			}
		});
		scrollComposite.notifyListeners(SWT.Resize, new Event());
		
		scrollComposite.setContent(dataComposite);
		scrollComposite.setExpandVertical(true);
		scrollComposite.setExpandHorizontal(true);
		scrollComposite.update();
		
		this.explorerComp.setRedraw(true);
		this.explorerComp.layout();
		this.explorerComp.update();
	}
	
	*//**
	 * Find elements.
	 *
	 * @param rect
	 *            the rect
	 * @param gc
	 *            the gc
	 *//*
	void findElements(final Rectangle rect, final GC gc) {
		List<CLabel> selectedLabels = new ArrayList<>();
		List<CLabel> notselectedLabels = new ArrayList<>();
		final Control[] children = dataComposite.getChildren();
		for (Control control : children) {
			if (control instanceof CLabel) {
				CLabel cLabel = (CLabel) control;
				Rectangle labelRect = cLabel.getBounds();
				if (rect.intersects(labelRect)) {
					selectedLabels.add(cLabel);
				} else {
					notselectedLabels.add(cLabel);
				}
			}
		}

		for (CLabel label : selectedLabels) {
			label.setBackground(DisplayAllObjectPage.this.selectedColor);
			label.setForeground(DisplayAllObjectPage.this.white);
			label.setData(CommonConstants.ISSELECTEDSTR, true);
			structuredSelection.put(label.getText(), new StructuredSelection(label.getData()));
		}

		for (CLabel label : notselectedLabels) {
			label.setBackground(DisplayAllObjectPage.this.white);
			label.setForeground(DisplayAllObjectPage.this.black);
			label.setData(CommonConstants.ISSELECTEDSTR, false);
			structuredSelection.remove(label.getText());
		}
	}

	*//**
	 * Hook context menu.
	 *
	 * @param cLabel
	 *            the c label
	 *//*
	private void hookContextMenu(CLabel cLabel) {
		ContextMenu contextMenu = new ContextMenu(this, cLabel, this.handlerService, this.commandService,
				this.messages);
		Object data = cLabel.getData();
		if (data instanceof RelationObj) {
			RelationObj relationObj = (RelationObj) data;
			if (relationObj.getContainerObj() instanceof BaseAppProjectApplications
					|| relationObj.getContainerObj() instanceof BaseAppUserApplications
					|| relationObj.getContainerObj() instanceof BaseAppStartApplications
					|| relationObj.getContainerObj() instanceof UserAppAdminAreas
					|| relationObj.getContainerObj() instanceof UserAppUsers
					|| relationObj.getContainerObj() instanceof ProjectAppAdminAreaChild
					|| relationObj.getContainerObj() instanceof ProjectAppAdminAreaProjects
					|| relationObj.getContainerObj() instanceof ProjectAppUsers
					|| relationObj.getContainerObj() instanceof StartAppAdminAreas
					|| relationObj.getContainerObj() instanceof StartAppUsers
					|| relationObj.getContainerObj() instanceof StartAppProjects
					|| relationObj.getContainerObj() instanceof ProjectAdminAreaChild
					|| relationObj.getContainerObj() instanceof ProjectAdminAreaProjectApplications
					|| relationObj.getContainerObj() instanceof UserProjectAdminAreaChild
					|| relationObj.getContainerObj() instanceof DirectoryApplications
					|| relationObj.getContainerObj() instanceof ProjectUserAdminAreaChild
					|| relationObj.getContainerObj() instanceof UserAdminAreaChild
					|| relationObj.getContainerObj() instanceof ProjectProjectApplications) {
				return;
			} else {
				contextMenu.createMenuItemForRelationObj();
			}
		} else if (data instanceof Role) {
			Role role = (Role) data;
			if (CommonConstants.SuperAdminRole.NAME.equals(role.getRoleName())) {
				return;
			} else {
				contextMenu.createMenuItemForIAdminTreeChild();
			}
		} else {
			contextMenu.createMenuItemForIAdminTreeChild();
		}
	}

	*//**
	 * Sets the text to item.
	 *
	 * @param item
	 *            the item
	 * @param text
	 *            the text
	 *//*
	private void setTextToItem(final Group item, final String text) {
		if (item != null && !item.isDisposed()) {
			item.setText(text);
		}
	}

	*//**
	 * Register message to item.
	 *
	 * @param group
	 *            the group
	 * @param element
	 *            the element
	 * @param messages
	 *            the messages
	 * @return the string
	 *//*
	private String registerMessageToItem(final Group group, final Object element, final Message messages) {
		if (group != null && !group.isDisposed()) {
			if (element instanceof Sites) {
				return messages.sitesNode;
			} else if (element instanceof AdministrationAreas || element instanceof SiteAdministrations) {
				return messages.administrationAreasNode;
			} else if (element instanceof Users) {
				return messages.usersNode;
			} else if (element instanceof Projects) {
				return messages.projectsNode;
			} else if (element instanceof Applications) {
				return messages.applicationsNode;
			} else if (element instanceof UserApplications || element instanceof SiteAdminAreaUserApplications) {
				return messages.userApplicationsNode;
			} else if (element instanceof UserApplication) {
				return ((UserApplication) element).getName();
			} else if (element instanceof ProjectApplications || element instanceof SiteAdminAreaProjectApplications) {
				return messages.projectApplicationsNode;
			} else if (element instanceof ProjectApplication) {
				return ((ProjectApplication) element).getName();
			} else if (element instanceof StartApplications) {
				return messages.startApplicationsNode;
			} else if (element instanceof StartApplication) {
				return ((StartApplication) element).getName();
			} else if (element instanceof BaseApplications) {
				return messages.baseApplicationsNode;
			} else if (element instanceof BaseApplication) {
				return ((BaseApplication) element).getName();
			} else if (element instanceof Groups) {
				return messages.groupsNode;
			} else if (element instanceof UserGroupsModel) {
				return messages.userGroupsNode;
			} else if (element instanceof ProjectGroupsModel) {
				return messages.projectGroupsNode;
			} else if (element instanceof UserApplicationGroups) {
				return messages.userAppGroupsNode;
			} else if (element instanceof ProjectApplicationGroups) {
				return messages.projectAppGroupsNode;
			} else if (element instanceof Directories) {
				return messages.directoryNode;
			} else if (element instanceof Configurations) {
				return messages.configurationNode;
			} else if (element instanceof Icons) {
				return messages.iconsNode;
			} else if (element instanceof SiteAdminAreaProjects) {
				return messages.projectsNode;
			} else if (element instanceof RoleScopeObjects) {
				final String name = ((RoleScopeObjects) element).getName();
				if (name.equals(AdministrationAreas.class.getName())) {
					return messages.administrationAreasNode;
				}
			} else if (element instanceof SiteAdminProjectAppNotFixed
					|| element instanceof SiteAdminAreaUserAppNotFixed) {
				return messages.notFixedNode;
			} else if (element instanceof SiteAdminProjectAppFixed || element instanceof SiteAdminAreaUserAppFixed) {
				return messages.fixedNode;
			} else if (element instanceof SiteAdminProjectAppProtected
					|| element instanceof SiteAdminAreaUserAppProtected) {
				return messages.protectedNode;
			} else if (element instanceof SiteAdminAreaProjectStartApplications
					|| element instanceof SiteAdminAreaStartApplications) {
				return messages.startApplicationsNode;
			} else if (element instanceof Roles) {
				return messages.rolesNode;
			} else if (element instanceof RoleUsers) {
				return messages.usersNode;
			} else if (element instanceof AdminUsers) {
				return messages.usersNode;
			} else if (element instanceof AdminUserApps) {
				return messages.userApplicationsNode;
			} else if (element instanceof AdminProjectApps) {
				return messages.projectApplicationsNode;
			} else if (element instanceof LiveMessages) {
				return messages.liveMsgConfigNode;
			}
		}
		return CommonConstants.EMPTY_STR;
	}

	*//**
	 * Gets the selection.
	 *
	 * @return the selection
	 *//*
	private IStructuredSelection getSelection() {
		AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
		return (IStructuredSelection) adminTree.getSelection();
	}

	*//**
	 * Method for Creates the data composite.
	 *
	 * @param filterText
	 *            the filter text
	 * @param group
	 *            the group
	 *//*
	private void createDataComposite(final Group group) {
		try {
			if (this.childObjList != null && !this.childObjList.isEmpty()) {
				this.childObjList.clear();
				this.searchTxt.setText("");
			}

			final Object selectionObj = getSelection();
			if (selectionObj instanceof IStructuredSelection) {
				final Object firstElement = ((IStructuredSelection) selectionObj).getFirstElement();

				this.registry.register((text) -> setTextToItem(group, text), (message) -> {
					return registerMessageToItem(group, firstElement, message);
				});

				if (firstElement instanceof Sites) {
					childObjList = new ArrayList<>(((Sites) firstElement).getSitesCollection());
				} else if (firstElement instanceof AdministrationAreas) {
					childObjList = new ArrayList<>(
							((AdministrationAreas) firstElement).getAdministrationAreasCollection());
				} else if (firstElement instanceof Users) {
					childObjList = new ArrayList<>();
					final Collection<IAdminTreeChild> userCollection = ((Users) firstElement).getUsersCollection();
					for (IAdminTreeChild userNameAlphabet : userCollection) {
						if (userNameAlphabet instanceof UsersNameAlphabet) {
							childObjList.addAll(((UsersNameAlphabet) userNameAlphabet).getUsersCollection());
						}
					}
				} else if (firstElement instanceof UsersNameAlphabet) {
					childObjList = new ArrayList<>(((UsersNameAlphabet) firstElement).getUsersCollection());
				} else if (firstElement instanceof Projects) {
					childObjList = new ArrayList<>(((Projects) firstElement).getProjectsCollection());
				} else if (firstElement instanceof UserApplications) {
					childObjList = new ArrayList<>(((UserApplications) firstElement).getUserAppCollection());
				} else if (firstElement instanceof ProjectApplications) {
					childObjList = new ArrayList<>(((ProjectApplications) firstElement).getProjectAppCollection());
				} else if (firstElement instanceof StartApplications) {
					childObjList = new ArrayList<>(((StartApplications) firstElement).getStartAppCollection());
				} else if (firstElement instanceof BaseApplications) {
					childObjList = new ArrayList<>(((BaseApplications) firstElement).getBaseAppCollection());
				} else if (firstElement instanceof Groups) {
					childObjList = new ArrayList<>(((Groups) firstElement).getGroupsCollection());
				} else if (firstElement instanceof UserGroupsModel) {
					childObjList = new ArrayList<>(((UserGroupsModel) firstElement).getUserGroupsCollection());
				} else if (firstElement instanceof ProjectGroupsModel) {
					childObjList = new ArrayList<>(
							((ProjectGroupsModel) firstElement).getProjectGroupsCollection());
				} else if (firstElement instanceof UserApplicationGroups) {
					childObjList = new ArrayList<>(((UserApplicationGroups) firstElement).getUserAppGroupsCollection());
				} else if (firstElement instanceof ProjectApplicationGroups) {
					childObjList = new ArrayList<>(
							((ProjectApplicationGroups) firstElement).getProjectAppGroupsCollection());
				} else if (firstElement instanceof Directories) {
					childObjList = new ArrayList<>(((Directories) firstElement).getDirectoriesCollection());
				} else if (firstElement instanceof ProjectAdminAreas) {
					childObjList = new ArrayList<>(((ProjectAdminAreas) firstElement).getProjectAdminAreaCollection());
				} else if (firstElement instanceof ProjectUsers) {
					childObjList = new ArrayList<>(((ProjectUsers) firstElement).getProjectUserCollection());
				} else if (firstElement instanceof ProjectUserAdminAreas) {
					childObjList = new ArrayList<>(
							((ProjectUserAdminAreas) firstElement).getProjectUserAdminAreaChildCollection());
				} else if (firstElement instanceof SiteAdministrations) {
					childObjList = new ArrayList<>(
							((SiteAdministrations) firstElement).getSiteAdministrationAreasCollection());
				} else if (firstElement instanceof SiteAdminAreaProjects) {
					childObjList = new ArrayList<>(
							((SiteAdminAreaProjects) firstElement).getSiteAdminProjectsCollection());
				} else if (firstElement instanceof AdminAreaProjects) {
					childObjList = new ArrayList<>(
							((AdminAreaProjects) firstElement).getAdminAreasProjectsCollection());
				} else if (firstElement instanceof AdminAreaProjects) {
					childObjList = new ArrayList<>(
							((AdminAreaProjects) firstElement).getAdminAreasProjectsCollection());
				} else if (firstElement instanceof Roles) {
					childObjList = new ArrayList<>(((Roles) firstElement).getRolesCollection());
				} else if (firstElement instanceof RoleScopeObjects) {
					childObjList = new ArrayList<>(
							((RoleScopeObjects) firstElement).getRoleScopeObjectsChildren().values());
				} else if (firstElement instanceof UserAdminAreas) {
					childObjList = new ArrayList<>(((UserAdminAreas) firstElement).getUserAdminAreaChildCollection());
				} else if (firstElement instanceof UserProjects) {
					childObjList = new ArrayList<>(((UserProjects) firstElement).getUserProjectsCollection());
				} else if (firstElement instanceof UserProjectAdminAreas) {
					childObjList = new ArrayList<>(
							((UserProjectAdminAreas) firstElement).getUserProjectAdminAreaChildCollection());
				} else if (firstElement instanceof ProjectAppAdminAreas) {
					childObjList = new ArrayList<>(
							((ProjectAppAdminAreas) firstElement).getProjectAppAdminAreaChildrenCollection());
				} else if (firstElement instanceof LiveMessages) {
					childObjList = new ArrayList<>(((LiveMessages) firstElement).getLiveMsgsCollection());
				}else {
					AdminTreeDataLoad adminTreeDataLoad = AdminTreeDataLoad.getInstance();
					if (firstElement instanceof SiteAdminAreaUserApplications
							|| firstElement instanceof SiteAdminAreaUserAppNotFixed
							|| firstElement instanceof SiteAdminAreaUserAppFixed
							|| firstElement instanceof SiteAdminAreaUserAppProtected
							|| firstElement instanceof AdminAreaUserApplications
							|| firstElement instanceof AdminAreaUserAppNotFixed
							|| firstElement instanceof AdminAreaUserAppFixed
							|| firstElement instanceof AdminAreaUserAppProtected) {
						childObjList = adminTreeDataLoad
								.loadSiteAdminAreaUserAppsFromService((IAdminTreeChild) firstElement);
					} else if (firstElement instanceof SiteAdminAreaProjectApplications
							|| firstElement instanceof SiteAdminProjectAppNotFixed
							|| firstElement instanceof SiteAdminProjectAppFixed
							|| firstElement instanceof SiteAdminProjectAppProtected
							|| firstElement instanceof AdminAreaProjectApplications
							|| firstElement instanceof AdminAreaProjectAppNotFixed
							|| firstElement instanceof AdminAreaProjectAppFixed
							|| firstElement instanceof AdminAreaProjectAppProtected
							|| firstElement instanceof ProjectAdminAreaProjectApplications
							|| firstElement instanceof ProjectAdminAreaProjectAppNotFixed
							|| firstElement instanceof ProjectAdminAreaProjectAppFixed
							|| firstElement instanceof ProjectAdminAreaProjectAppProtected) {
						childObjList = new ArrayList<>(
								adminTreeDataLoad.loadSiteAdminAreaProjAppsFromService((IAdminTreeChild) firstElement));
					} else if (firstElement instanceof SiteAdminAreaStartApplications
							|| firstElement instanceof AdminAreaStartApplications) {
						childObjList = new ArrayList<>(adminTreeDataLoad
								.loadSiteAdminAreaStartAppsFromService((IAdminTreeChild) firstElement));
					} else if (firstElement instanceof SiteAdminAreaProjectStartApplications
							|| firstElement instanceof AdminAreaProjectStartApplications
							|| firstElement instanceof ProjectAdminAreaStartApplications) {
						childObjList = new ArrayList<>(adminTreeDataLoad
								.loadSiteAdminAreaProjStartAppsFromService((IAdminTreeChild) firstElement));
					} else if (firstElement instanceof ProjectProjectApplications) {
						childObjList = adminTreeDataLoad
								.loadProjectProjectAppFromService((ProjectProjectApplications) firstElement);
					} else if (firstElement instanceof BaseAppProjectApplications) {
						childObjList = adminTreeDataLoad
								.loadBaseAppProjectAppFromService((BaseAppProjectApplications) firstElement);
					} else if (firstElement instanceof BaseAppUserApplications) {
						childObjList = adminTreeDataLoad
								.loadBaseAppUserAppFromService((BaseAppUserApplications) firstElement);
					} else if (firstElement instanceof BaseAppStartApplications) {
						childObjList = adminTreeDataLoad
								.loadBaseAppStartAppFromService((BaseAppStartApplications) firstElement);
					} else if (firstElement instanceof UserAAUserApplications
							|| firstElement instanceof UserAAUserAppAllowed
							|| firstElement instanceof UserAAUserAppForbidden) {
						childObjList = new ArrayList<>(
								adminTreeDataLoad.loadUserAAUserAppsFromService((IAdminTreeChild) firstElement));
					} else if (firstElement instanceof UserProjectAAProjectApplications
							|| firstElement instanceof UserProjectAAProjectAppAllowed
							|| firstElement instanceof UserProjectAAProjectAppForbidden
							|| firstElement instanceof ProjectUserAAProjectApplications
							|| firstElement instanceof ProjectUserAAProjectAppAllowed
							|| firstElement instanceof ProjectUserAAProjectAppForbidden) {
						childObjList = new ArrayList<>(adminTreeDataLoad
								.loadUserProjectAAProjectAppFromService((IAdminTreeChild) firstElement));
					} else if (firstElement instanceof UserStartApplications) {
						childObjList = new ArrayList<>(
								adminTreeDataLoad.loadUserStartAppsFromService((IAdminTreeChild) firstElement));
					} else if (firstElement instanceof UserAppAdminAreas) {
						childObjList = new ArrayList<>(
								adminTreeDataLoad.loadUserAppAdminAreasFromService((IAdminTreeChild) firstElement));
					} else if (firstElement instanceof UserAppUsers) {
						childObjList = new ArrayList<>(
								adminTreeDataLoad.loadUserAppUsersFromService((IAdminTreeChild) firstElement));
					} else if (firstElement instanceof ProjectAppAdminAreaProjects) {
						childObjList = new ArrayList<>(adminTreeDataLoad
								.loadProjectAppAdminAreaProjectsFromService((IAdminTreeChild) firstElement));
					} else if (firstElement instanceof ProjectAppUsers) {
						childObjList = new ArrayList<>(
								adminTreeDataLoad.loadProjectAppUsersFromService((IAdminTreeChild) firstElement));
					} else if (firstElement instanceof StartAppAdminAreas) {
						childObjList = new ArrayList<>(
								adminTreeDataLoad.loadStartAppAdminAreasFromService((IAdminTreeChild) firstElement));
					} else if (firstElement instanceof StartAppProjects) {
						childObjList = new ArrayList<>(
								adminTreeDataLoad.loadStartAppProjectsFromService((IAdminTreeChild) firstElement));
					} else if (firstElement instanceof StartAppUsers) {
						childObjList = new ArrayList<>(
								adminTreeDataLoad.loadStartAppUsersFromService((IAdminTreeChild) firstElement));
					} else if (firstElement instanceof DirectoryUsers) {
						childObjList = new ArrayList<>(
								adminTreeDataLoad.loadDirectoryUsersFromService((IAdminTreeChild) firstElement));
					} else if (firstElement instanceof DirectoryProjects) {
						childObjList = new ArrayList<>(
								adminTreeDataLoad.loadDirectoryProjectsFromService((IAdminTreeChild) firstElement));
					} 
						 * else if (firstElement instanceof
						 * DirectoryApplications) { childObjList = new
						 * ArrayList<>( adminTreeDataLoad.
						 * loadDirectoryApplicationsFromService((
						 * IAdminTreeChild) firstElement)); }
						  else if (firstElement instanceof DirectoryUserApplications) {
						childObjList = new ArrayList<>(adminTreeDataLoad
								.loadDirectoryUserApplicationsFromService((IAdminTreeChild) firstElement));
					} else if (firstElement instanceof DirectoryProjectApplications) {
						childObjList = new ArrayList<>(adminTreeDataLoad
								.loadDirectoryProjectApplicationsFromService((IAdminTreeChild) firstElement));
					} else if (firstElement instanceof RoleUsers) {
						childObjList = new ArrayList<>(
								adminTreeDataLoad.loadRoleUsersFromService((IAdminTreeChild) firstElement));
					} else if (firstElement instanceof RoleScopeObjectUsers) {
						childObjList = new ArrayList<>(
								adminTreeDataLoad.loadRoleScopeObjectUsersFromService((IAdminTreeChild) firstElement));
					} else if (firstElement instanceof AdminUsers) {
						childObjList = new ArrayList<>(
								adminTreeDataLoad.loadAdminMenuUsersFromServices((IAdminTreeChild) firstElement));
					} else if (firstElement instanceof AdminUserApps) {
						childObjList = new ArrayList<>(
								adminTreeDataLoad.loadAdminMenuUserAppFromServices((IAdminTreeChild) firstElement));
					} else if (firstElement instanceof AdminProjectApps) {
						childObjList = new ArrayList<>(
								adminTreeDataLoad.loadAdminMenuProjectAppFromServices((IAdminTreeChild) firstElement));
					}  else if (firstElement instanceof UserGroupUsers) {
						childObjList = new ArrayList<>(
								adminTreeDataLoad.loadUserGroupUsersFromService((IAdminTreeChild) firstElement));
					}else if (firstElement instanceof ProjectGroupProjects) {
						childObjList = new ArrayList<>(
								adminTreeDataLoad.loadProjectGroupProjectsFromService((IAdminTreeChild) firstElement));
					}else if (firstElement instanceof UserAppGroupUserApps) {
						childObjList = new ArrayList<>(
								adminTreeDataLoad.loadUserAppGroupUserAppsFromService((IAdminTreeChild) firstElement));
					}else if (firstElement instanceof ProjectAppGroupProjectApps) {
						childObjList = new ArrayList<>(
								adminTreeDataLoad.loadProjectAppGroupProjectAppsFromService((IAdminTreeChild) firstElement));
					} else {
						return;
					}
				}
				createDataLabels(childObjList);
			}
		} catch (Exception e) {
			LOGGER.warn("Unable to set model selection ! " + e);
		}
	}

	*//**
	 * Method for Creates the data labels.
	 *//*
	private void createDataLabels(List<IAdminTreeChild> childObjTempList) {
		setMultiSelectEnabled(false);
		
		 * Job job = new Job("Loading...") {
		 * 
		 * @Override protected IStatus run(IProgressMonitor monitor) {
		 
		removeAllObject();
		if (childObjTempList != null && !childObjTempList.isEmpty()) {
			final int childListSize = childObjTempList.size();
			// monitor.beginTask("Loading Nodes..", childListSize);
			// Display.getDefault().asyncExec(new Runnable() {

			// @Override
			// public void run() {
			label = new CLabel[childListSize];

			for (int index = 0; index < childListSize; index++) {
				label[index] = new CLabel(dataComposite, SWT.NONE | SWT.MULTI);
				label[index].setData(CommonConstants.ISSELECTEDSTR, false);
				label[index].setBackground(DisplayAllObjectPage.this.white);
				label[index].setForeground(DisplayAllObjectPage.this.black);

				final LocalSelectionTransfer transfer = LocalSelectionTransfer.getTransfer();
				LocalSelectionTransfer[] transferTypes = new LocalSelectionTransfer[] {
						LocalSelectionTransfer.getTransfer() };
				DragSource source = new DragSource(label[index], DND.DROP_COPY | DND.DROP_MOVE);
				source.setTransfer(transferTypes);
				source.addDragListener(new DispalyAllObjDragListener(DisplayAllObjectPage.this, transfer));
				label[index].setData(childObjTempList.get(index));
				setLabelAndIcon(label[index]);

				if (background == null && foreground == null) {
					background = label[index].getBackground();
					foreground = label[index].getForeground();
				}

				Display.getDefault().addFilter(SWT.KeyDown, new Listener() {

					@Override
					public void handleEvent(Event keyEvent) {

						if (keyEvent.count == 0) {
							if (keyEvent.keyCode == SWT.CTRL) {
								setMultiSelectEnabled(true);
							} else if (((keyEvent.stateMask & SWT.CTRL) == SWT.CTRL) && keyEvent.keyCode == 'a'
									&& isMultiSelectEnabled) {
								selectAll();
							} else if (((keyEvent.stateMask & SWT.CTRL) == SWT.CTRL) && (keyEvent.keyCode == 'c')) {
								if (structuredSelection.size() > 0) {
									Clipboard clipboard = new Clipboard(Display.getCurrent());
									clipboard.setContents(new Object[] { structuredSelection.values() },
											new Transfer[] { ObjectTypeTransfer.getInstance() });
								}
							} else if (!childObjTempList.isEmpty() && (Character.isAlphabetic(keyEvent.keyCode)
									|| Character.isDigit(keyEvent.keyCode))) {
								for (int index = 0; index < label.length; index++) {
									if (label[index] != null && !label[index].isDisposed()) {
										String labelText = label[index].getText();
										if (!XMSystemUtil.isEmpty(labelText) && labelText.toLowerCase()
												.startsWith(String.valueOf((char) keyEvent.keyCode).toLowerCase())) {
											label[index].setBackground(DisplayAllObjectPage.this.foucsedColor);
											label[index].setForeground(DisplayAllObjectPage.this.black);
										} else {
											label[index].setBackground(DisplayAllObjectPage.this.white);
											label[index].setForeground(DisplayAllObjectPage.this.black);
										}
									}
								}
							}
							keyEvent.count = 1;
						}
					}
				});
				Display.getDefault().addFilter(SWT.KeyUp, new Listener() {
					@Override
					public void handleEvent(final Event keyEvent) {
						if (keyEvent.count == 0) {
							if (keyEvent.keyCode == SWT.CTRL) {
								setMultiSelectEnabled(false);
							}
							keyEvent.count = 1;
						}
					}
				});

				label[index].addMouseListener(new MouseAdapter() {

					
					 * (non-Javadoc)
					 * 
					 * @see
					 * org.eclipse.swt.events.MouseAdapter#mouseDoubleClick(org.
					 * eclipse.swt.events.MouseEvent)
					 
					@Override
					public void mouseDoubleClick(MouseEvent mouseDoubleClickEvent) {
						selectedLabel = (CLabel) mouseDoubleClickEvent.widget;
						gotoSelection();
					}

					
					 * (non-Javadoc)
					 * 
					 * @see
					 * org.eclipse.swt.events.MouseAdapter#mouseDown(org.eclipse
					 * .swt.events.MouseEvent)
					 
					@Override
					public void mouseUp(final MouseEvent mouseEvent) {
						final boolean multiSelect = isMultiSelectEnabled();
						if (!multiSelect) {
							for (int index = 0; index < childListSize; index++) {
								String text = label[index].getText();
								if (!text.equals(selectedLabel.getText())) {
									label[index].setBackground(background);
									label[index].setForeground(foreground);
									label[index].setData(CommonConstants.ISSELECTEDSTR, false);
									structuredSelection.remove(text);
								}
							}
							selectedLabel.setBackground(DisplayAllObjectPage.this.selectedColor);
							selectedLabel.setForeground(DisplayAllObjectPage.this.white);
							selectedLabel.setData(CommonConstants.ISSELECTEDSTR, true);
							structuredSelection.put(selectedLabel.getText(),
									new StructuredSelection(selectedLabel.getData()));
							ContextInjectionFactory.invoke(new RemoveRelationHandler(), CanExecute.class,
									eclipseContext);
							ContextInjectionFactory.invoke(new CopyObjectHandler(), CanExecute.class, eclipseContext);
							ContextInjectionFactory.invoke(new EditToolBarHandler(), CanExecute.class, eclipseContext);
							ContextInjectionFactory.invoke(new DeleteToolBarHandler(), CanExecute.class,
									eclipseContext);
							ContextInjectionFactory.invoke(new CreateAsMenuHandler(), CanExecute.class, eclipseContext);
						}
					}

					@Override
					public void mouseDown(MouseEvent mouseDownEvent) {
						boolean multiSelect = isMultiSelectEnabled();
						selectedLabel = (CLabel) mouseDownEvent.widget;
						selectedLabel.forceFocus();
						hookContextMenu(selectedLabel);
						
						if ((boolean) (selectedLabel.getData(CommonConstants.ISSELECTEDSTR))) {
							selectedLabel.setData(CommonConstants.ISSELECTEDSTR, false);
						} else {
							selectedLabel.setData(CommonConstants.ISSELECTEDSTR, true);
						}
						if (multiSelect) {
							for (int index = 0; index < childListSize; index++) {
								Object data = label[index].getData();
								if ((boolean) (label[index].getData(CommonConstants.ISSELECTEDSTR))) {
									label[index].setBackground(DisplayAllObjectPage.this.selectedColor);
									label[index].setForeground(DisplayAllObjectPage.this.white);
									structuredSelection.put(label[index].getText(), new StructuredSelection(data));
									ContextInjectionFactory.invoke(new DeleteToolBarHandler(), CanExecute.class, eclipseContext);
									ContextInjectionFactory.invoke(new EditToolBarHandler(), CanExecute.class, eclipseContext);
									ContextInjectionFactory.invoke(new CreateAsMenuHandler(), CanExecute.class, eclipseContext);
								}
							}
						} else {
							if (structuredSelection.size() == 1) {
								Set<String> keySet = structuredSelection.keySet();
								String previousSelectedText = null;
								for (String key : keySet) {
									previousSelectedText = key;
								}
								if(!XMSystemUtil.isEmpty(previousSelectedText)) {
									for (int index = 0; index < childListSize; index++) {
										String text = label[index].getText();
										if (!text.equals(previousSelectedText)) {
											label[index].setBackground(background);
											label[index].setForeground(foreground);
											label[index].setData(CommonConstants.ISSELECTEDSTR, false);
											structuredSelection.remove(text);
										}
									}
								}
							}
							selectedLabel.setBackground(DisplayAllObjectPage.this.selectedColor);
							selectedLabel.setForeground(DisplayAllObjectPage.this.white);
							selectedLabel.setData(CommonConstants.ISSELECTEDSTR, true);
							structuredSelection.put(selectedLabel.getText(),
									new StructuredSelection(selectedLabel.getData()));
							ContextInjectionFactory.invoke(new RemoveRelationHandler(), CanExecute.class, eclipseContext);
							ContextInjectionFactory.invoke(new CopyObjectHandler(), CanExecute.class, eclipseContext);
							ContextInjectionFactory.invoke(new EditToolBarHandler(), CanExecute.class, eclipseContext);
							ContextInjectionFactory.invoke(new DeleteToolBarHandler(), CanExecute.class, eclipseContext);
							ContextInjectionFactory.invoke(new CreateAsMenuHandler(), CanExecute.class, eclipseContext);
						} else {
							for (int index = 0; index < childListSize; index++) {
								label[index].setBackground(background);
								label[index].setForeground(foreground);
								label[index].setData(CommonConstants.ISSELECTEDSTR, false);
								structuredSelection.remove(label[index].getText());
							}
							selectedLabel.setBackground(DisplayAllObjectPage.this.selectedColor);
							selectedLabel.setForeground(DisplayAllObjectPage.this.white);
							selectedLabel.setData(CommonConstants.ISSELECTEDSTR, true);
							structuredSelection.put(selectedLabel.getText(),
									new StructuredSelection(selectedLabel.getData()));
							ContextInjectionFactory.invoke(new RemoveRelationHandler(), CanExecute.class,
									eclipseContext);
							ContextInjectionFactory.invoke(new CopyObjectHandler(), CanExecute.class, eclipseContext);
							ContextInjectionFactory.invoke(new EditToolBarHandler(), CanExecute.class, eclipseContext);
							ContextInjectionFactory.invoke(new DeleteToolBarHandler(), CanExecute.class,
									eclipseContext);
							ContextInjectionFactory.invoke(new CreateAsMenuHandler(), CanExecute.class, eclipseContext);
						}
					}
				});
				// monitor.worked(1);
			}
		}
		// });
		// }
		// return Status.OK_STATUS;
		// }
		// };
		// job.setUser(true);
		// job.schedule();

		this.explorerComp.requestLayout();
		this.explorerComp.redraw();
		this.explorerComp.getParent().update();
	}

	*//**
	 * Sets the label and icon.
	 *
	 * @param label
	 *            the new label and icon
	 *//*
	public void setLabelAndIcon(final CLabel label) {
		Icon icon = null;
		if (label != null && !label.isDisposed()) {
			Object data = label.getData();
			if (data instanceof Site) {
				if ((icon = ((Site) data).getIcon()) != null) {
					label.setImage(getIconObject(icon, ((Site) data).isActive()));
				}
				label.setText(((Site) data).getName());
			} else if (data instanceof AdministrationArea) {
				if ((icon = ((AdministrationArea) data).getIcon()) != null) {
					label.setImage(getIconObject(icon, ((AdministrationArea) data).isActive()));
				}
				label.setText(((AdministrationArea) data).getName());
			} else if (data instanceof User) {
				if ((icon = ((User) data).getIcon()) != null) {
					label.setImage(getIconObject(icon, ((User) data).isActive()));
				}
				label.setText(((User) data).getName());
			} else if (data instanceof Project) {
				if ((icon = ((Project) data).getIcon()) != null) {
					label.setImage(getIconObject(icon, ((Project) data).isActive()));
				}
				label.setText(((Project) data).getName());
			} else if (data instanceof UserApplication) {
				if ((icon = ((UserApplication) data).getIcon()) != null) {
					label.setImage(getIconObject(icon, ((UserApplication) data).isActive()));
				}
				registry.register((text) -> updateWidgetText(text, label), message -> {
					return registerMessageToLabel(label);
				});
			} else if (data instanceof ProjectApplication) {
				if ((icon = ((ProjectApplication) data).getIcon()) != null) {
					label.setImage(getIconObject(icon, ((ProjectApplication) data).isActive()));
				}
				registry.register((text) -> updateWidgetText(text, label), message -> {
					return registerMessageToLabel(label);
				});
			} else if (data instanceof StartApplication) {
				if ((icon = ((StartApplication) data).getIcon()) != null) {
					label.setImage(getIconObject(icon, ((StartApplication) data).isActive()));
				}
				registry.register((text) -> updateWidgetText(text, label), message -> {
					return registerMessageToLabel(label);
				});
			} else if (data instanceof BaseApplication) {
				if ((icon = ((BaseApplication) data).getIcon()) != null) {
					label.setImage(getIconObject(icon, ((BaseApplication) data).isActive()));
				}
				registry.register((text) -> updateWidgetText(text, label), message -> {
					return registerMessageToLabel(label);
				});
			} else if (data instanceof UserGroupModel) {
				if ((icon = ((UserGroupModel) data).getIcon()) != null) {
					label.setImage(getIconObject(icon,true));
				}
				label.setText(((UserGroupModel) data).getName());
			}else if (data instanceof ProjectGroupModel) {
				if ((icon = ((ProjectGroupModel) data).getIcon()) != null) {
					label.setImage(getIconObject(icon, true));
				}
				label.setText(((ProjectGroupModel) data).getName());
			}else if (data instanceof UserApplicationGroup) {
				if ((icon = ((UserApplicationGroup) data).getIcon()) != null) {
					label.setImage(getIconObject(icon,true));
				}
				label.setText(((UserApplicationGroup) data).getName());
			} else if (data instanceof ProjectApplicationGroup) {
				if ((icon = ((ProjectApplicationGroup) data).getIcon()) != null) {
					label.setImage(getIconObject(icon, true));
				}
				label.setText(((ProjectApplicationGroup) data).getName());
			} else if (data instanceof Directory) {
				if ((icon = ((Directory) data).getIcon()) != null) {
					label.setImage(getIconObject(icon, true));
				}
				label.setText(((Directory) data).getName());
			} else if (data instanceof Role) {
				String name = ((Role) data).getRoleName();
				if (CommonConstants.SuperAdminRole.NAME.equals(name)) {
					if ((icon = XMAdminUtil.getInstance().getIconByName("superadmin.png")) != null) {
						label.setImage(getIconObject(icon, true));
					}
				} else {
					if ((icon = XMAdminUtil.getInstance().getIconByName("roles.png")) != null) {
						label.setImage(getIconObject(icon, true));
					}
				}
				label.setText(name);
			} else if (data instanceof LiveMessage) {
				if ((icon = XMAdminUtil.getInstance().getIconByName("livemessagesconfig.png")) != null) {
					label.setImage(getIconObject(icon, true));
				}
				label.setText(((LiveMessage) data).getName());
			} else if (data instanceof RelationObj) {
				IAdminTreeChild refObject = ((RelationObj) data).getRefObject();
				boolean relationStatus = ((RelationObj) data).isActive();
				if (refObject instanceof Site) {
					if ((icon = ((Site) refObject).getIcon()) != null) {
						label.setImage(getIconObject(icon, relationStatus));
					}
					label.setText(((Site) refObject).getName());
				} else if (refObject instanceof AdministrationArea) {
					if ((icon = ((AdministrationArea) refObject).getIcon()) != null) {
						label.setImage(getIconObject(icon, relationStatus));
					}
					label.setText(((AdministrationArea) refObject).getName());
				} else if (refObject instanceof User) {
					if ((icon = ((User) refObject).getIcon()) != null) {
						label.setImage(getIconObject(icon, relationStatus));
					}
					label.setText(((User) refObject).getName());
				} else if (refObject instanceof Project) {
					if ((icon = ((Project) refObject).getIcon()) != null) {
						label.setImage(getIconObject(icon, relationStatus));
					}
					label.setText(((Project) refObject).getName());
				} else if (refObject instanceof UserApplication) {
					if ((icon = ((UserApplication) refObject).getIcon()) != null) {
						label.setImage(getIconObject(icon, relationStatus));
					}
					registry.register((text) -> updateWidgetText(text, label), message -> {
						return registerMessageToLabel(label);
					});
				} else if (refObject instanceof ProjectApplication) {
					if ((icon = ((ProjectApplication) refObject).getIcon()) != null) {
						label.setImage(getIconObject(icon, relationStatus));
					}
					registry.register((text) -> updateWidgetText(text, label), message -> {
						return registerMessageToLabel(label);
					});
				} else if (refObject instanceof StartApplication) {
					if ((icon = ((StartApplication) refObject).getIcon()) != null) {
						label.setImage(getIconObject(icon, relationStatus));
					}
					registry.register((text) -> updateWidgetText(text, label), message -> {
						return registerMessageToLabel(label);
					});
				} else if (refObject instanceof BaseApplication) {
					if ((icon = ((BaseApplication) refObject).getIcon()) != null) {
						label.setImage(getIconObject(icon, relationStatus));
					}
					registry.register((text) -> updateWidgetText(text, label), message -> {
						return registerMessageToLabel(label);
					});
				}
			}
			label.requestLayout();
		}
	}

	*//**
	 * Register message to label.
	 *
	 * @param label
	 *            the label
	 * @return the string
	 *//*
	private String registerMessageToLabel(final CLabel label) {
		if (label != null && !label.isDisposed()) {
			if (label != null && !label.isDisposed()) {
				Object element = label.getData();
				if (element != null) {
					if (element instanceof RelationObj) {
						element = ((RelationObj) element).getRefObject();
					}

					if (element instanceof UserApplication) {
						return ((UserApplication) element).getName();
					} else if (element instanceof ProjectApplication) {
						return ((ProjectApplication) element).getName();
					} else if (element instanceof StartApplication) {
						return ((StartApplication) element).getName();
					} else if (element instanceof BaseApplication) {
						return ((BaseApplication) element).getName();
					}
				}
			}
		}
		return CommonConstants.EMPTY_STR;
	}

	*//**
	 * Gets the structured selection.
	 *
	 * @return the structured selection
	 *//*
	public Map<String, StructuredSelection> getStructuredSelection() {
		return structuredSelection;
	}

	*//**
	 * Sets the structured selection.
	 *
	 * @param structuredSelection
	 *            the new structured selection
	 *//*
	public void setStructuredSelection(Map<String, StructuredSelection> structuredSelection) {
		this.structuredSelection = structuredSelection;
	}

	*//**
	 * Gets the icon object.
	 *
	 * @param icon
	 *            {@link Icon}
	 * @param active
	 *            {@link boolean}
	 * @return the icon object
	 *//*
	private Image getIconObject(Icon icon, boolean active) {
		Image image = null;
		if (icon != null) {
			String iconPath = icon.getIconPath();
			if (!XMSystemUtil.isEmpty(iconPath)) {
				image = XMSystemUtil.getInstance().getImage(this.getClass(), iconPath, this.resourceManager, active);
			}
		}
		return image;
	}

	*//**
	 * Method for Update widget text.
	 *
	 * @param message
	 *            {@link String}
	 * @param control
	 *            {@link CLabel}
	 *//*
	private void updateWidgetText(final String message, final CLabel control) {
		if (control != null && !control.isDisposed()) {
			control.setText(message);
			control.requestLayout();
			control.getParent().redraw();
			control.getParent().getParent().update();
			control.getParent().getParent().getParent().update();
		}
	}

	*//**
	 * Removes the all object.
	 *//*
	public void removeAllObject() {
		if (this.dataComposite != null) {
			final Control[] children = this.dataComposite.getChildren();
			for (Control control : children) {
				control.dispose();
			}
		}

	}

	*//**
	 * Select all.
	 *//*
	public void selectAll() {
		clearSelections();
		for (int index = 0; index < label.length; index++) {
			if (!label[index].isDisposed()) {
				Object data = label[index].getData();
				label[index].setData(CommonConstants.ISSELECTEDSTR, true);
				label[index].setBackground(DisplayAllObjectPage.this.selectedColor);
				label[index].setForeground(DisplayAllObjectPage.this.white);
				structuredSelection.put(label[index].getText(), new StructuredSelection(data));
			}
		}
	}

	*//**
	 * Clear selections.
	 *//*
	public void clearSelections() {
		structuredSelection.clear();
		for (int index = 0; index < label.length; index++) {
			if (label[index] != null && !label[index].isDisposed()) {
				label[index].setData(CommonConstants.ISSELECTEDSTR, false);
				label[index].setBackground(DisplayAllObjectPage.this.white);
				label[index].setForeground(DisplayAllObjectPage.this.black);
			}
		}
	}

	*//**
	 * Invert selection.
	 *//*
	public void invertSelection() {
		if (this.structuredSelection.isEmpty()) {
			return;
		}

		for (int index = 0; index < label.length; index++) {
			if (label[index] != null && !label[index].isDisposed()) {
				if ((boolean) (label[index].getData(CommonConstants.ISSELECTEDSTR))) {
					label[index].setBackground(DisplayAllObjectPage.this.white);
					label[index].setForeground(DisplayAllObjectPage.this.black);
					label[index].setData(CommonConstants.ISSELECTEDSTR, false);
					this.structuredSelection.remove(label[index].getText());
				} else {
					label[index].setBackground(DisplayAllObjectPage.this.selectedColor);
					label[index].setForeground(DisplayAllObjectPage.this.white);
					label[index].setData(CommonConstants.ISSELECTEDSTR, true);
					this.structuredSelection.put(label[index].getText(),
							new StructuredSelection(label[index].getData()));
				}
			}
		}
	}
	
	*//**
	 * Goto selection.
	 *//*
	public void gotoSelection() {
		if(selectedLabel == null){
			return;
		}
		if (selectedLabel.isDisposed()) {
			return;
		}
		final AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
		Object selectedObj = selectedLabel.getData();
		TreePath[] selectionPaths = adminTree.getStructuredSelection().getPaths();
		if (selectionPaths != null && selectionPaths.length > 0) {
			adminTree.setExpandedState(selectionPaths[0], true);

			if (selectedObj instanceof User) {
				Users users = AdminTreeFactory.getInstance().getUsers();
				Map<String, IAdminTreeChild> userChildren = users.getUsersChildren();
				char startChar = (((User) selectedObj).getName().toString().toLowerCase()
						.toCharArray()[0]);
				IAdminTreeChild iAdminTreeChild = userChildren.get(String.valueOf(startChar));
				adminTree.setExpandedState(iAdminTreeChild, true);
			}
		}
		adminTree.setSelection(new StructuredSelection(selectedObj), true);
	}

	*//**
	 * Checks if is display enable.
	 *
	 * @return true, if is display enable
	 *//*
	public boolean isDisplayEnable(){
		if (selectedLabel == null || structuredSelection.size() > 1) {
			return false;
		}
		if (selectedLabel.isDisposed()) {
			return false;
		}
		return true;
	}
	
	*//**
	 * Checks if is all enable.
	 *
	 * @return true, if is all enable
	 *//*
	public boolean isAllEnable(){
		if (this.structuredSelection.size() == label.length) {
			return false;
		}
		return true;
	}
	
	*//**
	 * Checks if is clear enable.
	 *
	 * @return true, if is clear enable
	 *//*
	public boolean isClearEnable(){
		if (this.structuredSelection.isEmpty()) {
			return false;
		}
		return true;
	}
	
	
	*//**
	 * Checks if is invert enable.
	 *
	 * @return true, if is invert enable
	 *//*
	public boolean isInvertEnable(){
		if (this.structuredSelection.isEmpty()) {
			return false;
		}
		return true;
	}
}
*/