package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.startapplicationmodel;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.Project;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.refs.RelationObj;

// TODO: Auto-generated Javadoc
/**
 * Class for Start app projects.
 *
 * @author Chiranjeevi.Akula
 */
public class StartAppProjects implements IAdminTreeChild {

	/** Member variable 'parent' for {@link IAdminTreeChild}. */
	private IAdminTreeChild parent;

	/** Member variable 'start app projects children' for {@link Map<String,IAdminTreeChild>}. */
	private Map<String, IAdminTreeChild> startAppProjectsChildren;

	/**
	 * Constructor for StartAppProjects Class.
	 *
	 * @param parent {@link IAdminTreeChild}
	 */
	public StartAppProjects(final IAdminTreeChild parent) {
		this.parent = parent;
		this.startAppProjectsChildren = new LinkedHashMap<>();
	}

	/**
	 * Method for Adds the.
	 *
	 * @param startAppProjectId {@link String}
	 * @param child {@link IAdminTreeChild}
	 * @return the i admin tree child {@link IAdminTreeChild}
	 */
	public IAdminTreeChild add(final String startAppProjectId, final IAdminTreeChild child) {
		child.setParent(this);
		IAdminTreeChild returnVal = this.startAppProjectsChildren.put(startAppProjectId, child);
		if (child instanceof RelationObj && ((RelationObj) child).getRefObject() instanceof Project) {
			sort();
		}
		return returnVal;
	}

	/**
	 * Method for Removes the.
	 *
	 * @param startAppProjectId {@link String}
	 * @return the i admin tree child {@link IAdminTreeChild}
	 */
	public IAdminTreeChild remove(final String startAppProjectId) {
		return this.startAppProjectsChildren.remove(startAppProjectId);
	}

	/**
	 * Method for Removes the all.
	 */
	public void removeAll() {
		this.startAppProjectsChildren.clear();
	}
	
	/**
	 * Gets the start app projects children collection.
	 *
	 * @return the start app projects children collection
	 */
	public Collection<IAdminTreeChild> getStartAppProjectsChildrenCollection() {
		return this.startAppProjectsChildren.values();
	}

	/**
	 * Gets the start app projects children.
	 *
	 * @return the start app projects children
	 */
	public Map<String, IAdminTreeChild> getStartAppProjectsChildren() {
		return startAppProjectsChildren;
	}
	
	/* (non-Javadoc)
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#getParent()
	 */
	@Override
	public IAdminTreeChild getParent() {
		return this.parent;
	}

	/* (non-Javadoc)
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#setParent(com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild)
	 */
	@Override
	public void setParent(final IAdminTreeChild parent) {
		this.parent = parent;
	}
	/**
	 * Sort.
	 */
	public void sort() {
		final LinkedHashMap<String, IAdminTreeChild> collect = this.startAppProjectsChildren.entrySet().stream().sorted(
				(e1, e2) -> ((Project) e1.getValue()).getName().compareTo(((Project) e2.getValue()).getName()))
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
		this.startAppProjectsChildren = collect;
	}

}
