package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.directory;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;

public class DirectoryApplications implements IAdminTreeChild {

	/** The parent. */
	private IAdminTreeChild parent;

	/** The directory application children. */
	final private Map<String, IAdminTreeChild> directoryApplicationChildren;

	/**
	 * Instantiates a new directory application model.
	 *
	 * @param parent
	 *            the parent
	 */
	public DirectoryApplications(final IAdminTreeChild parent) {
		this.parent = parent;
		this.directoryApplicationChildren = new LinkedHashMap<>();
		addFixedChildren();
	}

	/**
	 * Method for Adds the fixed children.
	 */
	public void addFixedChildren() {
		this.directoryApplicationChildren.put(DirectoryUserApplications.class.getName(), new DirectoryUserApplications(this));
		this.directoryApplicationChildren.put(DirectoryProjectApplications.class.getName(), new DirectoryProjectApplications(this));
	}
	
	/**
	 * Adds the.
	 *
	 * @param directoryApplicationsChildrenId
	 *            the directory applications children id
	 * @param child
	 *            the child
	 * @return the i admin tree child
	 */
	public IAdminTreeChild add(final String directoryApplicationsChildrenId, final IAdminTreeChild child) {
		return this.directoryApplicationChildren.put(directoryApplicationsChildrenId, child);
	}

	/**
	 * Removes the.
	 *
	 * @param directoryApplicationsChildrenId
	 *            the directory applications children id
	 * @return the i admin tree child
	 */
	public IAdminTreeChild remove(final String directoryApplicationsChildrenId) {
		return this.directoryApplicationChildren.remove(directoryApplicationsChildrenId);

	}

	/**
	 * Gets the directory applications children collection.
	 *
	 * @return the directory applications children collection
	 */
	public Collection<IAdminTreeChild> getDirectoryApplicationsChildrenCollection() {
		return this.directoryApplicationChildren.values();
	}

	/**
	 * Gets the directory applications children children.
	 *
	 * @return the directory applications children children
	 */
	public Map<String, IAdminTreeChild> getDirectoryApplicationsChildrenChildren() {
		return directoryApplicationChildren;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#
	 * getParent()
	 */
	@Override
	public IAdminTreeChild getParent() {
		// TODO Auto-generated method stub
		return this.parent;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#
	 * setParent(com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.
	 * IAdminTreeChild)
	 */
	@Override
	public void setParent(IAdminTreeChild parent) {
		this.parent = parent;

	}

}
