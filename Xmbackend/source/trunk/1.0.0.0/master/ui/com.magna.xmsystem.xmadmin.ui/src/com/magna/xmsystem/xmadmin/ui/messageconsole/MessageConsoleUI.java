package com.magna.xmsystem.xmadmin.ui.messageconsole;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.core.services.nls.Translation;
import org.eclipse.e4.ui.di.UIEventTopic;
import org.eclipse.e4.ui.di.UISynchronize;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyleRange;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magna.xmsystem.xmadmin.message.Message;
import com.magna.xmsystem.xmadmin.util.CommonConstants;
import com.magna.xmsystem.xmadmin.util.MessageType;
import com.magna.xmsystem.xmadmin.util.XMAdminUtil;

public class MessageConsoleUI extends Composite {

	/** Logger instance. */
	private static final Logger LOGGER = LoggerFactory.getLogger(MessageConsoleUI.class);

	/** {@link UISynchronize} instance. */
	@Inject
	private UISynchronize uiSynchronize;

	private StyledText styleText;

	/** Member variable for messages. */
	@Inject
	@Translation
	transient private Message messages;

	/**
	 * Constructor.
	 *
	 * @param parent
	 *            the parent
	 */
	@Inject
	public MessageConsoleUI(final Composite parent) {
		super(parent, SWT.NONE);
	}

	/**
	 * Creates the UI.
	 */
	@PostConstruct
	private void createUI() {
		final GridLayout gridLayout = new GridLayout(1, false);
		gridLayout.marginHeight = 0;
		gridLayout.marginBottom = 0;
		gridLayout.marginLeft = 0;
		gridLayout.marginRight = 0;
		gridLayout.marginTop = 0;
		gridLayout.horizontalSpacing = 0;
		gridLayout.verticalSpacing = 0;
		gridLayout.marginWidth = 0;
		this.setLayout(gridLayout);
		this.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		initUI();
	}

	/**
	 * Gets the event.
	 *
	 * @param message
	 *            the message
	 * @return the event
	 */
	@Inject
	@Optional
	public void getEvent(@UIEventTopic(CommonConstants.EVENT_BROKER.CONSOLE_MESSAGE) final MessageConsoleLogObj message) {
		updateInterface(message);
	}

	/**
	 * Update interface.
	 *
	 * @param message
	 *            the message
	 */
	private void updateInterface(final MessageConsoleLogObj message) {
		try {
			this.uiSynchronize.syncExec(new Runnable() {
				@Override
				public void run() {
					try {
						if (!styleText.isDisposed()) {
							//setLiveData();
							setLiveData(message);
						}
					} catch (Exception exc) {
						LOGGER.error("Exception occured while setting script live mesaage text", exc); //$NON-NLS-1$
					}
				}
			});
		} catch (Exception exception) {
			LOGGER.error("Exception occured while setting script live message text in update interface method", //$NON-NLS-1$
					exception);
		}
	}

	/**
	 * Method for Inits the UI.
	 */
	private void initUI() {
		this.styleText = new StyledText(this, SWT.WRAP | SWT.V_SCROLL | SWT.MULTI);
		this.styleText.setTextLimit(10);
		styleText.setBounds(10, 10, 100, 100);
		GridDataFactory.fillDefaults().grab(true, true).align(SWT.FILL, SWT.FILL).applyTo(styleText);
		styleText.setEditable(false);
		styleText.setBackground(styleText.getDisplay().getSystemColor(SWT.COLOR_WHITE));
		initMsgConsoleData(XMAdminUtil.getInstance().getMsgConsolObj());
	}

	
	
	private void initMsgConsoleData(List<MessageConsoleLogObj> msgConsolObj) {
		
		for (MessageConsoleLogObj messageConsoleLogObj : msgConsolObj) {
			setLiveData(messageConsoleLogObj);
		}
		
	}

	/**
	 * Sets the live data.
	 *
	 * @param log the new live data
	 */
	private void setLiveData(final MessageConsoleLogObj log) {
		Display display = styleText.getDisplay();
		Color successCol = display.getSystemColor(SWT.COLOR_DARK_GREEN);
		Color failureCol = display.getSystemColor(SWT.COLOR_RED);
		String currentText = this.styleText.getText();
		String logStr = log.getLogStr();

		
		this.styleText.append(logStr);
		this.styleText.setTextLimit(20);
		this.styleText.setCaretOffset(this.styleText.getText().length());
		this.styleText.setTopIndex(this.styleText.getLineCount() - 1);

		StyleRange styleRange = new StyleRange();
		styleRange.start = currentText.length();
		styleRange.length = logStr.length();
		if (log.getMessageType() == MessageType.SUCCESS) {
			styleRange.foreground = successCol;
		} else if (log.getMessageType() == MessageType.FAILURE) {
			styleRange.foreground = failureCol;
			//styleRange.fontStyle = SWT.BOLD;
		}
		this.styleText.setStyleRange(styleRange);

	}

}
