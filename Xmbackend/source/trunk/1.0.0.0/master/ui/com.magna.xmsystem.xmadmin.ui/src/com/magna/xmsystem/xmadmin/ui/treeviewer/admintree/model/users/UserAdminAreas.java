package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.adminarea.AdministrationArea;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.refs.RelationObj;

/**
 * Class for User admin areas.
 *
 * @author Chiranjeevi.Akula
 */
public class UserAdminAreas implements IAdminTreeChild {

	/** Member variable 'parent' for {@link IAdminTreeChild}. */
	private IAdminTreeChild parent;

	/**
	 * Member variable 'user admin areas children' for
	 * {@link Map<String,IAdminTreeChild>}.
	 */
	private Map<String, IAdminTreeChild> userAdminAreasChildren;

	/**
	 * Constructor for UserAdminAreas Class.
	 *
	 * @param parent
	 *            {@link IAdminTreeChild}
	 */
	public UserAdminAreas(final IAdminTreeChild parent) {
		this.parent = parent;
		this.userAdminAreasChildren = new LinkedHashMap<>();
	}

	/**
	 * Method for Adds the.
	 *
	 * @param userAdminAreaChildId
	 *            {@link String}
	 * @param child
	 *            {@link IAdminTreeChild}
	 * @return the i admin tree child {@link IAdminTreeChild}
	 */
	public IAdminTreeChild add(final String userAdminAreaChildId, final IAdminTreeChild child) {
		child.setParent(this);
		IAdminTreeChild returnVal = this.userAdminAreasChildren.put(userAdminAreaChildId, child);
		if (child instanceof RelationObj && ((RelationObj) child).getRefObject() instanceof AdministrationArea) {
			sort();
		}
		return returnVal;
	}

	/**
	 * Method for Removes the.
	 *
	 * @param userAdminAreaChildId
	 *            {@link String}
	 * @return the i admin tree child {@link IAdminTreeChild}
	 */
	public IAdminTreeChild remove(final String userAdminAreaChildId) {
		return this.userAdminAreasChildren.remove(userAdminAreaChildId);
	}

	/**
	 * Method for Removes the all.
	 */
	public void removeAll() {
		this.userAdminAreasChildren.clear();
	}
	
	/**
	 * Gets the user admin area child collection.
	 *
	 * @return the user admin area child collection
	 */
	public Collection<IAdminTreeChild> getUserAdminAreaChildCollection() {
		return this.userAdminAreasChildren.values();
	}

	/**
	 * Gets the user admin area child children.
	 *
	 * @return the user admin area child children
	 */
	public Map<String, IAdminTreeChild> getUserAdminAreaChildChildren() {
		return userAdminAreasChildren;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#
	 * getParent()
	 */
	@Override
	public IAdminTreeChild getParent() {
		return this.parent;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#
	 * setParent(com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.
	 * IAdminTreeChild)
	 */
	@Override
	public void setParent(final IAdminTreeChild parent) {
		this.parent = parent;
	}
	
	/**
	 * Sort.
	 */
	public void sort() {
		final LinkedHashMap<String, IAdminTreeChild> collect = this.userAdminAreasChildren.entrySet().stream().sorted(
				(e1, e2) -> ((AdministrationArea) (((RelationObj) e1.getValue()).getRefObject())).getName()
				.compareTo(((AdministrationArea) (((RelationObj) e2.getValue()).getRefObject())).getName()))
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
		this.userAdminAreasChildren = collect;
	}

}
