package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.group.projectgroup;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.projects.Project;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.refs.RelationObj;

/**
 * The Class ProjectGroupProjects.
 * 
 * @author archita.patel
 */
public class ProjectGroupProjects implements IAdminTreeChild {

	/** The parent. */
	private IAdminTreeChild parent;

	/** The projectgroup children. */
	private Map<String, IAdminTreeChild> projectgroupChildren;

	/**
	 * Instantiates a new project group projects.
	 *
	 * @param parent
	 *            the parent
	 */
	public ProjectGroupProjects(final IAdminTreeChild parent) {
		this.parent = parent;
		this.projectgroupChildren = new LinkedHashMap<>();
	}

	/**
	 * Adds the.
	 *
	 * @param projectGroupChildId
	 *            the project group child id
	 * @param child
	 *            the child
	 * @return the i admin tree child
	 */
	public IAdminTreeChild add(final String projectGroupChildId, final IAdminTreeChild child) {
		IAdminTreeChild returnVal = this.projectgroupChildren.put(projectGroupChildId, child);
		if (child instanceof RelationObj && ((RelationObj) child).getRefObject() instanceof Project) {
			sort();
		}
		return returnVal;
	}

	/**
	 * Removes the.
	 *
	 * @param projectGroupChildId
	 *            the project group child id
	 * @return the i admin tree child
	 */
	public IAdminTreeChild remove(final String projectGroupChildId) {
		return this.projectgroupChildren.remove(projectGroupChildId);

	}

	
	/**
	 * Gets the project group children collection.
	 *
	 * @return the project group children collection
	 */
	public Collection<IAdminTreeChild> getProjectGroupChildrenCollection() {
		return this.projectgroupChildren.values();
	}

	
	/**
	 * Gets the project group children.
	 *
	 * @return the project group children
	 */
	public Map<String, IAdminTreeChild> getProjectGroupChildren() {
		return projectgroupChildren;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#
	 * getParent()
	 */
	@Override
	public IAdminTreeChild getParent() {
		return this.parent;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#
	 * setParent(com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.
	 * IAdminTreeChild)
	 */
	@Override
	public void setParent(IAdminTreeChild parent) {
		this.parent = parent;

	}
	
	/**
	 * Sort.
	 */
	public void sort() {
		final LinkedHashMap<String, IAdminTreeChild> collect = this.projectgroupChildren.entrySet().stream().sorted(
				(e1, e2) -> ((Project) ((RelationObj) e1.getValue()).getRefObject()).getName().compareTo(((Project)((RelationObj) e1.getValue()).getRefObject()).getName()))
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
		this.projectgroupChildren = collect;
	}

}
