package com.magna.xmsystem.xmadmin.ui.validation;

/**
 * The Enum StatusValidation.
 * 
 * @author Deepak Upadhyay
 */
public enum StatusValidation {
	SITE, COMMON_NODE_VALIDATE, PROJECT, APPLICATION, EMAIL, CONTACT, PORT_NUMBER, URL, SMTP_PASSWORD, SMTP_SERVER_NAME, LDAP_BASE, LDAP_USERNAME, DIRECTORY, LIVE_MESSAGE
}
