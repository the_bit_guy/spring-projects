package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.livemsgconfig;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;

// TODO: Auto-generated Javadoc
/**
 * The Class LiveMessagesConfig.
 * 
 * @author archita.patel
 */
public class LiveMessages implements IAdminTreeChild {

	/** The parent. */
	private IAdminTreeChild parent;

	/** The live msgs children. */
	private Map<String, IAdminTreeChild> liveMsgsChildren;

	/**
	 * Instantiates a new live messages.
	 */
	public LiveMessages() {
		this.parent = null;
		this.liveMsgsChildren = new LinkedHashMap<>();
	}

	/**
	 * Adds the.
	 *
	 * @param liveMsgId
	 *            the live msg id
	 * @param child
	 *            the child
	 * @return the i admin tree child
	 */
	public IAdminTreeChild add(final String liveMsgId, final IAdminTreeChild child) {
		child.setParent(this);
		final IAdminTreeChild returnVal = this.liveMsgsChildren.put(liveMsgId, child);
		sort();
		return returnVal;
	}

	/**
	 * Removes the.
	 *
	 * @param liveMsgId
	 *            the live msg id
	 * @return the i admin tree child
	 */
	public IAdminTreeChild remove(final String liveMsgId) {
		return this.liveMsgsChildren.remove(liveMsgId);
	}

	/**
	 * Removes the all.
	 */
	public void removeAll() {
		this.liveMsgsChildren.clear();
	}

	
	/**
	 * Gets the live msgs collection.
	 *
	 * @return the live msgs collection
	 */
	public Collection<IAdminTreeChild> getLiveMsgsCollection() {
		return this.liveMsgsChildren.values();
	}

	
	/**
	 * Gets the live msgs children.
	 *
	 * @return the live msgs children
	 */
	public Map<String, IAdminTreeChild> getLiveMsgsChildren() {
		return liveMsgsChildren;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#
	 * getParent()
	 */
	@Override
	public IAdminTreeChild getParent() {
		return this.parent;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#
	 * setParent(com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.
	 * IAdminTreeChild)
	 */
	@Override
	public void setParent(final IAdminTreeChild parent) {
		this.parent = parent;
	}
	
	/**
	 * Sort.
	 */
	public void sort() {
		final LinkedHashMap<String, IAdminTreeChild> collect = this.liveMsgsChildren.entrySet().stream().sorted(
				(e1, e2) -> ((LiveMessage) e1.getValue()).getName().compareTo(((LiveMessage) e2.getValue()).getName()))
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
		this.liveMsgsChildren = collect;
	}
}
