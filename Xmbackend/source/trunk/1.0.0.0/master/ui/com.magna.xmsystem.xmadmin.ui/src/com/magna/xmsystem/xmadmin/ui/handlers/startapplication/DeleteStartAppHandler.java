
package com.magna.xmsystem.xmadmin.ui.handlers.startapplication;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.inject.Inject;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.core.services.nls.Translation;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.widgets.Display;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.ResourceAccessException;

import com.magna.xmbackend.vo.startApplication.StartApplicationResponse;
import com.magna.xmsystem.dependencies.customExceptions.UnauthorizedAccessException;
import com.magna.xmsystem.ui.controls.widgets.CustomMessageDialog;
import com.magna.xmsystem.xmadmin.message.Message;
import com.magna.xmsystem.xmadmin.restclient.application.StartAppController;
import com.magna.xmsystem.xmadmin.ui.parts.IEditablePart;
import com.magna.xmsystem.xmadmin.ui.parts.InformationPart;
import com.magna.xmsystem.xmadmin.ui.parts.objexp.ObjectExpPage;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.AdminTreeviewer;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.AdminTreeFactory;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.startapplicationmodel.StartApplication;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.startapplicationmodel.StartApplications;
import com.magna.xmsystem.xmadmin.util.CommonConstants;
import com.magna.xmsystem.xmadmin.util.MessageType;
import com.magna.xmsystem.xmadmin.util.XMAdminUtil;

/**
 * Class for Delete start app handler.
 *
 * @author Roshan.Ekka
 */
public class DeleteStartAppHandler {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(DeleteStartAppHandler.class);

	/** Member variable for {@link Message}. */
	@Inject
	@Translation
	private Message messages;

	/** Member variable 'application' for {@link MApplication}. */
	@Inject
	private MApplication application;

	/** Member variable 'model service' for {@link EModelService}. */
	@Inject
	private EModelService modelService;

	/**
	 * Method to delete the eventApplication
	 */
	@Execute
	public void execute() {
		try {
			if (XMAdminUtil.getInstance().isAcessAllowed("START_APPLICATION-DELETE")) {
				final AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
				IStructuredSelection selection;
				Object selectionObj;
				MPart mPart = (MPart) this.modelService.find(CommonConstants.PART_ID.INFORMATION_PART_ID, application);
				InformationPart rightSidePart2 = (InformationPart) mPart.getObject();
				ObjectExpPage objectExpPage = rightSidePart2.getObjectExpPartUI();
				if (objectExpPage != null
						&& (selection = objectExpPage.getObjExpTableViewer().getStructuredSelection()) != null
						&& (selectionObj = selection.getFirstElement()) != null
						&& selectionObj instanceof StartApplication) {
					deleteStartApplication(selection);
					objectExpPage.refreshExplorer();
				} else if (adminTree != null && (selection = adminTree.getStructuredSelection()) != null
						&& (selectionObj = selection.getFirstElement()) != null
						&& selectionObj instanceof StartApplication) {
					deleteStartApplication(selection);
				}
				XMAdminUtil.getInstance().clearClipBoardContents();
			} else {
				CustomMessageDialog.openError(Display.getCurrent().getActiveShell(),
						messages.objectPermissionDialogTitle, messages.objectPermissionDialogMsg);
			}
		} catch (ClassCastException e) {
			LOGGER.error("Exception occured while deleting startApplication " + e);
		} catch (UnauthorizedAccessException e) {
			LOGGER.error("Current user is Unauthorized " + e);
		} catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				CustomMessageDialog.openError(Display.getCurrent().getActiveShell(), messages.errorDialogTitile,
						messages.serverNotReachable);
			}
			LOGGER.error("Exception occured in calling access allowed API " + e);
		}
	}

	/**
	 * Delete start application.
	 *
	 * @param selectionObj
	 *            the selection obj
	 */
	@SuppressWarnings("rawtypes")
	private void deleteStartApplication(IStructuredSelection selection) {
		Object selectionObj = selection.getFirstElement();
		final AdminTreeviewer adminTree = XMAdminUtil.getInstance().getAdminTree();
		final StartApplications startApplications = AdminTreeFactory.getInstance().getStartApplications();
		if (selectionObj instanceof StartApplication) {
			String startAppName = ((StartApplication) selectionObj).getName();
			String confirmDialogTitle = messages.deleteConfirmDialogTitle;
			String confirmDialogMsg;
			if (selection.size() == 1) {
				confirmDialogMsg = messages.deleteStartAppConfirmDialogMsgPart1 + " \'" + startAppName + "\' "
						+ messages.deleteStartAppConfirmDialogMsgPart2 + " ?";
			} else {
				confirmDialogMsg = messages.deleteMultiObjectsConfirmDialogMsg + " "
						+ messages.deleteStartAppConfirmDialogMsgPart2 + " ?";
			}
			boolean isConfirmed = CustomMessageDialog.openConfirm(adminTree.getControl().getShell(), confirmDialogTitle,
					confirmDialogMsg);
			if (isConfirmed) {
				List selectionList = selection.toList();
				Job job = new Job("Deleting Objects...") {

					@Override
					protected IStatus run(IProgressMonitor monitor) {
						monitor.beginTask("Deleting Objects..", 100);
						monitor.worked(30);
						try {
							Set<String> startAppIds = new HashSet<>();
							Map<String,String> successObjectNames = new HashMap<>();
							Map<String,String> failObjectNames = new HashMap<>();
							for (int i = 0; i < selectionList.size(); i++) {
								String id = ((StartApplication) selectionList.get(i)).getStartPrgmApplicationId();
								String name = ((StartApplication) selectionList.get(i)).getName();
								startAppIds.add(id);
								successObjectNames.put(id, name);
							}
							final StartAppController startAppController = new StartAppController();
							final StartApplicationResponse siteResponse = startAppController.deleteMultiStartApp(startAppIds);
							if (siteResponse != null) {
								if (!siteResponse.getStatusMaps().isEmpty()) {
									List<Map<String, String>> statusMapList = siteResponse.getStatusMaps();
									for (Map<String, String> statusMap : statusMapList) {
										String string = statusMap.get("en");
										String[] split = string.split("id");
										startAppIds.remove(split[1].trim());
										failObjectNames.put(split[1].trim(), successObjectNames.get(split[1].trim()));
										successObjectNames.remove(split[1].trim());
									}
									Display.getDefault().asyncExec(new Runnable() {
										public void run() {
											CustomMessageDialog.openError(Display.getDefault().getActiveShell(),
													messages.deleteConfirmDialogTitle,
													messages.deleteMultiObjectsDialogErrMsg);
										}
									});
								}
								for (String startAppId : startAppIds) {
									startApplications.getStartApplications().remove(startAppId);
								}
								
								Display.getDefault().asyncExec(new Runnable() {
									@Override
									public void run() {
										adminTree.refresh();
										adminTree.setSelection(new StructuredSelection(startApplications), true);
									}
								});
								if (successObjectNames.size() > 0) {
									Iterator it = successObjectNames.entrySet().iterator();
									while (it.hasNext()) {
										Map.Entry map = (Map.Entry) it.next();
										XMAdminUtil.getInstance().updateLogFile(messages.startApplicationObject + " '"
												+ map.getValue() + "' " + messages.objectDelete, MessageType.SUCCESS);
									}
								}
								if (failObjectNames.size() > 0) {
									Iterator it = failObjectNames.entrySet().iterator();
									while (it.hasNext()) {
										Map.Entry map = (Map.Entry) it.next();
										XMAdminUtil.getInstance()
												.updateLogFile(messages.deleteErrorMsg + " "
														+ messages.startApplicationObject + " '" + map.getValue() + "'",
														MessageType.FAILURE);
									}
								}
							}

						} catch (Exception e) {
							LOGGER.error(e.getMessage());
						}

						monitor.worked(70);
						return Status.OK_STATUS;
					}
				};
				job.setUser(true);
				job.schedule();
			}
		}
	}

	/**
	 * Checks if is acess allowed.
	 *
	 * @return true, if is acess allowed
	 */
	/*public boolean isAcessAllowed() {
		boolean returnVal = true;
		try {
			ValidationController controller = new ValidationController();
			ValidationRequest request = new ValidationRequest();
			request.setUserName(RestClientUtil.getInstance().getUserName());
			request.setPermissionType(PermissionType.OBJECT_PERMISSION.name());
			request.setPermissionName("START_APPLICATION-DELETE"); //$NON-NLS-1$
			returnVal = controller.getAccessAllowed(request);
		} catch (UnauthorizedAccessException e) {
			LOGGER.error("Current user is Unauthorized " + e);
			returnVal = false;
		} catch (Exception e) {
			LOGGER.error("Exception occured in calling access allowed API " + e);
			returnVal = false;
		}
		return returnVal;
	}*/

	/**
	 * Can execute.
	 *
	 * @return true, if successful
	 */
	@CanExecute
	public boolean canExecute() {
		boolean returnVal = true;
		final XMAdminUtil instance = XMAdminUtil.getInstance();
		MPart rightPart;
		Object rightView;
		if ((rightPart = instance.getInformationPart()) != null
				&& (rightView = rightPart.getObject()) != null && rightView instanceof IEditablePart
				&& ((IEditablePart) rightView).isDirty()) {
			returnVal = false;
		}
		return returnVal;
	}
}