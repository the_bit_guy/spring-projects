package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.baseapplicationmodel;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.userapplicationmodel.UserApplication;

/**
 * Class for Base applications.
 *
 * @author Chiranjeevi.Akula
 */
public class BaseApplications implements IAdminTreeChild {
	/** The parent. */
	private IAdminTreeChild parent;

	/**
	 * Member variable 'base application children' for
	 * {@link List<IAdminTreeChild>}.
	 */
	private Map<String, IAdminTreeChild> baseAppChildren;

	/**
	 * Constructor for BaseApplications Class.
	 */
	public BaseApplications() {
		this.baseAppChildren = new LinkedHashMap<>();
	}

	/**
	 * Adds the child
	 * 
	 * @param child
	 *            {@link IAdminTreeChild}
	 * @return {@link boolean}
	 */
	public IAdminTreeChild add(final String baseApplicatioId,final IAdminTreeChild child) {
		child.setParent(this);
		final IAdminTreeChild returnVal = this.baseAppChildren.put(baseApplicatioId, child);
		sort();
		return returnVal;
	}


	/**
	 * Method for Removes the.
	 *
	 * @param child
	 *            {@link IAdminTreeChild}
	 * @return true, if successful
	 */
	public IAdminTreeChild remove(final String id) {
		return this.baseAppChildren.remove(id);
	}
	
	/**
	 * Method for Removes the all.
	 */
	public void removeAll() {
		this.baseAppChildren.clear();
	}
	
	/**
	 * @return {@link List} of {@link UserApplication} 
	 */
	public Collection<IAdminTreeChild> getBaseAppCollection() {
		return this.baseAppChildren.values();
	}

	/**
	 * Gets the base applications.
	 *
	 * @return the base applications
	 */
	public Map<String, IAdminTreeChild> getBaseApplications() {
		return this.baseAppChildren;
	}
	
	/**
	 * Returns null as its parent
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#getParent()
	 */
	@Override
	public IAdminTreeChild getParent() {
		return this.parent;
	}

	/**
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#setParent(com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild)
	 */
	@Override
	public void setParent(final IAdminTreeChild parent) {
		this.parent = parent;
	}
	
	/**
	 * Sort.
	 */
	public void sort() {
		final LinkedHashMap<String, IAdminTreeChild> collect = this.baseAppChildren.entrySet().stream().sorted(
				(e1, e2) -> ((BaseApplication) e1.getValue()).getName().compareTo(((BaseApplication) e2.getValue()).getName()))
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
		this.baseAppChildren = collect;
	}
}
