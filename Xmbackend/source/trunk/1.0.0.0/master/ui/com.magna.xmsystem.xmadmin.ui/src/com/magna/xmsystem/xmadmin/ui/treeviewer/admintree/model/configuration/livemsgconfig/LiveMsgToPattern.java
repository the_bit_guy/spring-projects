package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.livemsgconfig;

public enum LiveMsgToPattern {

	S(1), A(1), P(1), U(1), S_A(2), S_P(2), S_U(2), S_A_P(3), S_P_U(3), S_A_U(3), S_A_P_U(4), A_P(2), A_U(2), A_P_U(
			3), P_U(2);

	private int size;

	LiveMsgToPattern(int size) {
		this.size = size;
	}

	/**
	 * @return the size
	 */
	public int getSize() {
		return size;
	}

}
