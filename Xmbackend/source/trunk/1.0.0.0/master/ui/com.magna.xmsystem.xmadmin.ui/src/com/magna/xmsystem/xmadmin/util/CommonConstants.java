package com.magna.xmsystem.xmadmin.util;

import com.magna.xmbackend.vo.enums.NotificationVariables;

/**
 * The Class CommonConstants.
 */
public class CommonConstants {

	/** The Constant XMADMIN_UI_BUNDLE. */
	public static final String XMADMIN_UI_BUNDLE = "bundleclass://com.magna.xmsystem.xmadmin.ui"; //$NON-NLS-1$

	/** The Constant USER_HISTROY_PREF. */
	public static final String USER_HISTROY_PREF = "com.magna.xmsystem.xmadmin.ui.userhistory"; //$NON-NLS-1$
	
	/** The Constant ADMIN_HISTROY_PREF. */
	public static final String ADMIN_HISTROY_PREF = "com.magna.xmsystem.xmadmin.ui.adminhistory"; //$NON-NLS-1$

	/** The Constant EMPTY_STR. */
	public static final String EMPTY_STR = "";

	/** The Constant CUSTOM_ICON. */
	public static final String CUSTOM_ICON = "CUSTOM";

	/** The Constant ISSELECTEDSTR. */
	public static final String ISSELECTEDSTR = "isSelected";
	
	/** The Constant TRUE. */
	public static final String TRUE = "true";

	/** The Constant FALSE. */
	public static final String FALSE = "false";
	
	/** The Constant MAX_CONCOLE_LINE. */
	public static final int MAX_CONCOLE_LINE = 40;
	
	/**
	 * The Class PART_ID.
	 */
	public static class PART_ID {

		/** The Constant ADMIN_TREE_PART_ID. */
		public static final String ADMIN_TREE_PART_ID = "com.magna.xmsystem.xmadmin.ui.part.admintreepart"; //$NON-NLS-1$

		/** The Constant INFORMATION_PART_ID. */
		public static final String INFORMATION_PART_ID = "com.magna.xmsystem.xmadmin.ui.part.informationpart"; //$NON-NLS-1$

	}

	/**
	 * The Class COMMAND_ID.
	 */
	public static class COMMAND_ID {

		/** The Constant SELECT_GERMAN. */
		public static final String SELECT_GERMAN = "com.magna.xmsystem.xmadmin.ui.command.selectgerman"; //$NON-NLS-1$

		/** The Constant SELECT_ENGLISH. */
		public static final String SELECT_ENGLISH = "com.magna.xmsystem.xmadmin.ui.command.selectenglish"; //$NON-NLS-1$

		/** The Constant CREATE_SITE. */
		public static final String CREATE_SITE = "com.magna.xmsystem.xmadmin.ui.command.sitecreation"; //$NON-NLS-1$
		
		/** The Constant CREATEAS_SITE. */
		public static final String CREATEAS_SITE = "com.magna.xmsystem.xmadmin.ui.command.popupmenulabelnodecreateas"; //$NON-NLS-1$
		
		/** The Constant CHANGE_SITE. */
		public static final String CHANGE_SITE = "com.magna.xmsystem.xmadmin.ui.command.popupmenulabelnodechange"; //$NON-NLS-1$

		/** The Constant DELETE_SITE. */
		public static final String DELETE_SITE = "com.magna.xmsystem.xmadmin.ui.command.sitedeletion"; //$NON-NLS-1$

		/** The Constant CREATE_PROJECT. */
		public static final String CREATE_PROJECT = "com.magna.xmsystem.xmadmin.ui.command.projectcreation"; //$NON-NLS-1$
		
		/** The Constant CHANGE_PROJECT. */
		public static final String CHANGE_PROJECT = "com.magna.xmsystem.xmadmin.ui.command.projectchange"; //$NON-NLS-1$
		
		/** The Constant CREATEAS_PROJECT. */
		public static final String CREATEAS_PROJECT = "com.magna.xmsystem.xmadmin.ui.command.projectcreateas"; //$NON-NLS-1$

		/** The Constant DELETE_PROJECT. */
		public static final String DELETE_PROJECT = "com.magna.xmsystem.xmadmin.ui.command.projectdeletion"; //$NON-NLS-1$

		/** The Constant SWTICH_PERSPECTIVE. */
		public static final String SWTICH_PERSPECTIVE = "com.magna.xmsystem.xmadmin.ui.command.switchperspective"; //$NON-NLS-1$

		/** The Constant HIDE_INACTIVE_ITEMS. */
		public static final String HIDE_INACTIVE_ITEMS = "com.magna.xmsystem.xmadmin.ui.command.hideinactiveitems"; //$NON-NLS-1$

		/** The Constant CREATE_ADMINAREA. */
		public static final String CREATE_ADMINAREA = "com.magna.xmsystem.xmadmin.ui.command.adminareacreation"; //$NON-NLS-1$

		/** The Constant CREATEAS_ADMINAREA. */
		public static final String CREATEAS_ADMINAREA = "com.magna.xmsystem.xmadmin.ui.command.adminareacreateas"; //$NON-NLS-1$

		/** The Constant DELETE_ADMINAREA. */
		public static final String DELETE_ADMINAREA = "com.magna.xmsystem.xmadmin.ui.command.adminareadeletion"; //$NON-NLS-1$

		/** The Constant CHANGE_ADMINAREA. */
		public static final String CHANGE_ADMINAREA = "com.magna.xmsystem.xmadmin.ui.command.adminareachange"; //$NON-NLS-1$

		/** The Constant DELETE_USER. */
		public static final String DELETE_USER = "com.magna.xmsystem.xmadmin.ui.command.userdelete"; //$NON-NLS-1$

		/** The Constant CHANGE_USER. */
		public static final String CHANGE_USER = "com.magna.xmsystem.xmadmin.ui.command.userchange"; //$NON-NLS-1$
		
		/** The Constant CREATE_BASEAPP. */
		public static final String CREATE_BASEAPP = "com.magna.xmsystem.xmadmin.ui.command.baseapplicationcreation"; //$NON-NLS-1$

		/** The Constant CHANGE_BASEAPP. */
		public static final String CHANGE_BASEAPP = "com.magna.xmsystem.xmadmin.ui.command.baseapplicationchange"; //$NON-NLS-1$
		
		/** The Constant CREATEAS_BASEAPP. */
		public static final String CREATEAS_BASEAPP = "com.magna.xmsystem.xmadmin.ui.command.baseapplicationcreateas"; //$NON-NLS-1$

		/** The Constant DELETE_BASEAPP. */
		public static final String DELETE_BASEAPP = "com.magna.xmsystem.xmadmin.ui.command.baseapplicationdelete"; //$NON-NLS-1$
		
		/** The Constant CREATE_USERAPP. */
		public static final String CREATE_USERAPP = "com.magna.xmsystem.xmadmin.ui.command.userapplicationcreation"; //$NON-NLS-1$

		/** The Constant CHANGE_USERAPP. */
		public static final String CHANGE_USERAPP = "com.magna.xmsystem.xmadmin.ui.command.userapplicationchange"; //$NON-NLS-1$
		
		/** The Constant CREATEAS_USERAPP. */
		public static final String CREATEAS_USERAPP = "com.magna.xmsystem.xmadmin.ui.command.userapplicationcreateas"; //$NON-NLS-1$

		/** The Constant DELETE_USERAPP. */
		public static final String DELETE_USERAPP = "com.magna.xmsystem.xmadmin.ui.command.userapplicationdelete"; //$NON-NLS-1$
		
		/** The Constant DELETE_USERAPP_CHILD. */
		public static final String DELETE_USERAPP_CHILD =  "com.magna.xmsystem.xmadmin.ui.command.userappchilddelete";//$NON-NLS-1$
		
		/** The Constant CREATE_PROJECTAPP. */
		public static final String CREATE_PROJECTAPP = "com.magna.xmsystem.xmadmin.ui.command.projectapplicationcreation"; //$NON-NLS-1$

		/** The Constant CHANGE_PROJECTAPP. */
		public static final String CHANGE_PROJECTAPP = "com.magna.xmsystem.xmadmin.ui.command.projectapplicationchange"; //$NON-NLS-1$
		
		/** The Constant DELETE_PROJECTAPP_CHILD. */
		public static final String DELETE_PROJECTAPP_CHILD = "com.magna.xmsystem.xmadmin.ui.command.projectappchilddetete";//$NON-NLS-1$
		
		/** The Constant CREATEAS_PROJECTAPP. */
		public static final String CREATEAS_PROJECTAPP = "com.magna.xmsystem.xmadmin.ui.command.projectapplicationcreateas"; //$NON-NLS-1$

		/** The Constant DELETE_PROJECTAPP. */
		public static final String DELETE_PROJECTAPP = "com.magna.xmsystem.xmadmin.ui.command.projectapplicationdelete"; //$NON-NLS-1$
		
		/** The Constant CREATE_STARTAPP. */
		public static final String CREATE_STARTAPP = "com.magna.xmsystem.xmadmin.ui.command.startapplicationcreation"; //$NON-NLS-1$

		/** The Constant CHANGE_STARTAPP. */
		public static final String CHANGE_STARTAPP = "com.magna.xmsystem.xmadmin.ui.command.startapplicationchange"; //$NON-NLS-1$
		
		/** The Constant CREATEAS_STARTAPP. */
		public static final String CREATEAS_STARTAPP = "com.magna.xmsystem.xmadmin.ui.command.startapplicationcreateas"; //$NON-NLS-1$

		/** The Constant DELETE_STARTAPP. */
		public static final String DELETE_STARTAPP = "com.magna.xmsystem.xmadmin.ui.command.startapplicationdelete"; //$NON-NLS-1$
		
		/** The Constant TOOLBAR_ROOTSELECTION. */
		public static final String TOOLBAR_ROOTSELECTION = "com.magna.xmsystem.xmadmin.ui.command.toolbarlabelrootselection"; //$NON-NLS-1$

		/** The Constant TOOLBAR_COLLAPSE. */
		public static final String TOOLBAR_COLLAPSE = "com.magna.xmsystem.xmadmin.ui.command.toolbarlabelexpandall"; //$NON-NLS-1$
		
		/** The Constant TOOLBAR_EDIT. */
		public static final String TOOLBAR_EDIT = "com.magna.xmsystem.xmadmin.ui.command.toolbarlabeledit"; //$NON-NLS-1$

		/** The Constant TOOLBAR_DELETE. */
		public static final String TOOLBAR_DELETE = "com.magna.xmsystem.xmadmin.ui.command.toolbarlabeldelete"; //$NON-NLS-1$
		
		/** The Constant TOOLBAR_COPY. */
		public static final String TOOLBAR_COPY = "com.magna.xmsystem.xmadmin.ui.command.toolbarlabelcopy"; //$NON-NLS-1$

		/** The Constant TOOLBAR_PASTE. */
		public static final String TOOLBAR_PASTE = "com.magna.xmsystem.xmadmin.ui.command.toolbarlabelpaste"; //$NON-NLS-1$

		/** The Constant CREATE_ROLE. */
		public static final String CREATE_ROLE ="com.magna.xmsystem.xmadmin.ui.command.rolecreation"; //$NON-NLS-1$
		
		/** The Constant CREATEAS_ROLE. */
		public static final String CREATEAS_ROLE ="com.magna.xmsystem.xmadmin.ui.command.rolecreateas"; //$NON-NLS-1$
		
		/** The Constant CHANGE_ROLE. */
		public static final String CHANGE_ROLE ="com.magna.xmsystem.xmadmin.ui.command.rolechange"; //$NON-NLS-1$
		
		/** The Constant DELETE_ROLE. */
		public static final String DELETE_ROLE ="com.magna.xmsystem.xmadmin.ui.command.roledelete"; //$NON-NLS-1$
		
		/** The Constant CREATE_DIRECTORY. */
		public static final String CREATE_DIRECTORY ="com.magna.xmsystem.xmadmin.ui.command.directorycreate"; //$NON-NLS-1$
		
		/** The Constant CREATEAS_DIRECTORY. */
		public static final String CREATEAS_DIRECTORY ="com.magna.xmsystem.xmadmin.ui.command.directorycreateas"; //$NON-NLS-1$
		
		/** The Constant CHANGE_DIRECTORY. */
		public static final String CHANGE_DIRECTORY ="com.magna.xmsystem.xmadmin.ui.command.directorychange"; //$NON-NLS-1$
		
		/** The Constant DELETE_DIRECTORY. */
		public static final String DELETE_DIRECTORY ="com.magna.xmsystem.xmadmin.ui.command.directorydelete"; //$NON-NLS-1$
		
		/** The Constant REMOVE_RELATION. */
		public static final String REMOVE_RELATION = "com.magna.xmsystem.xmadmin.ui.command.popupmenulabelnoderemoverelation"; //$NON-NLS-1$
		
		/** The Constant CHANGE_SMTP_CONFIG. */
		public static final String CHANGE_SMTP_CONFIG = "com.magna.xmsystem.xmadmin.ui.command.smtpconfigchange"; //$NON-NLS-1$
		
		/** The Constant CHANGE_LDAP_CONFIG. */
		public static final String CHANGE_LDAP_CONFIG = "com.magna.xmsystem.xmadmin.ui.command.ldapconfigchange"; //$NON-NLS-1$
		
		/** The Constant CHANGE_SINGLETON_CONFIG. */
		public static final String CHANGE_SINGLETON_CONFIG = "com.magna.xmsystem.xmadmin.ui.command.singletonappconfigchange"; //$NON-NLS-1$
		
		/** The Constant CHANGE_NOPROJECT_POPUP. */
		public static final String CHANGE_NOPROJECT_POPUP = "com.magna.xmsystem.xmadmin.ui.command.noprojectpopmessagenode"; //$NON-NLS-1$
		
		/** The Constant CHANGE_PROJECT_EXPIRY. */
		public static final String CHANGE_PROJECT_EXPIRY = "com.magna.xmsystem.xmadmin.ui.command.projectexpirychange"; //$NON-NLS-1$
		
		/** The Constant CREATEAS_LIVEMESSAGE. */
		public static final String CREATEAS_LIVEMESSAGE = "com.magna.xmsystem.xmadmin.ui.command.createaslivemsg"; //$NON-NLS-1$
		
		/** The Constant DELETE_LIVEMESSAGE. */
		public static final String DELETE_LIVEMESSAGE = "com.magna.xmsystem.xmadmin.ui.command.deletelivemsg"; //$NON-NLS-1$
		
		/** The Constant CHANGE_LIVEMESSAGE. */
		public static final String CHANGE_LIVEMESSAGE = "com.magna.xmsystem.xmadmin.ui.command.changelivemsg"; //$NON-NLS-1$
		
		/** The Constant CHANGE_USERAPP_GROUP. */
		public static final String CHANGE_USERAPP_GROUP = "com.magna.xmsystem.xmadmin.ui.command.changeuserappgrp"; //$NON-NLS-1$
		
		/** The Constant CREATEAS_USERAPP_GROUP. */
		public static final String CREATEAS_USERAPP_GROUP = "com.magna.xmsystem.xmadmin.ui.command.userappgrpcreateas"; //$NON-NLS-1$
		
		/** The Constant DELETE_USERAPP_GROUP. */
		public static final String DELETE_USERAPP_GROUP = "com.magna.xmsystem.xmadmin.ui.command.deleteuserappgrp"; //$NON-NLS-1$
		
		/** The Constant CHANGE_PROJECTAPP_GROUP. */
		public static final String CHANGE_PROJECTAPP_GROUP = "com.magna.xmsystem.xmadmin.ui.command.projectappgrpchange"; //$NON-NLS-1$
		
		/** The Constant CREATEAS_PROJECTAPP_GROUP. */
		public static final String CREATEAS_PROJECTAPP_GROUP = "com.magna.xmsystem.xmadmin.ui.command.projectappgrpcreateas"; //$NON-NLS-1$
		
		/** The Constant DELETE_PROJECTAPP_GROUP. */
		public static final String DELETE_PROJECTAPP_GROUP = "com.magna.xmsystem.xmadmin.ui.command.projectappgrpdelete"; //$NON-NLS-1$
		
		/** The Constant DELETE_PROJECT_GROUP. */
		public static final String DELETE_PROJECT_GROUP = "com.magna.xmsystem.xmadmin.ui.command.projectgroupdelete"; //$NON-NLS-1$
		
		/** The Constant CHANGE_PROJECT_GROUP. */
		public static final String CHANGE_PROJECT_GROUP = "com.magna.xmsystem.xmadmin.ui.command.projectgroupchange"; //$NON-NLS-1$
		
		/** The Constant CREATEAS_PROJECT_GROUP. */
		public static final String CREATEAS_PROJECT_GROUP = "com.magna.xmsystem.xmadmin.ui.command.projectgroupcreateas"; //$NON-NLS-1$
		
		/** The Constant DELETE_USER_GROUP. */
		public static final String DELETE_USER_GROUP = "com.magna.xmsystem.xmadmin.ui.command.groupdeletion"; //$NON-NLS-1$
		
		/** The Constant CHANGE_USER_GROUP. */
		public static final String CHANGE_USER_GROUP = "com.magna.xmsystem.xmadmin.ui.command.groupchange"; //$NON-NLS-1$
		
		/** The Constant CREATEAS_USER_GROUP. */
		public static final String CREATEAS_USER_GROUP = "com.magna.xmsystem.xmadmin.ui.command.groupcreateas"; //$NON-NLS-1$
		
		/** The Constant USER_REVIEW. */
		public static final String USER_REVIEW = "com.magna.xmsystem.xmadmin.ui.handledmenuitem.userreview"; //$NON-NLS-1$
		
		/** The Constant CREATEAS_PRO_CREATE_ACTION. */
		public static final String CREATEAS_PRO_CREATE_ACTION = "com.magna.xmsystem.xmadmin.ui.command.createasprocreateevtaction"; //$NON-NLS-1$
		
		/** The Constant CREATE_PRO_CREATE_ACTION. */
		public static final String CREATE_PRO_CREATE_ACTION = "com.magna.xmsystem.xmadmin.ui.command.createprocreateevt"; //$NON-NLS-1$
		
		/** The Constant CHANGE_PRO_CREATE_ACTION. */
		public static final String CHANGE_PRO_CREATE_ACTION = "com.magna.xmsystem.xmadmin.ui.command.changeprocreateevtaction"; //$NON-NLS-1$
		
		/** The Constant DELETE_PRO_CREATE_ACTION. */
		public static final String DELETE_PRO_CREATE_ACTION = "com.magna.xmsystem.xmadmin.ui.command.deleteprocreateevtaction"; //$NON-NLS-1$
		
		/** The Constant CREATEAS_PRO_DELETE_ACTION. */
		public static final String CREATEAS_PRO_DELETE_ACTION = "com.magna.xmsystem.xmadmin.ui.command.createasprodeleteevtaction"; //$NON-NLS-1$
		
		/** The Constant CREATE_PRO_DELETE_ACTION. */
		public static final String CREATE_PRO_DELETE_ACTION = "com.magna.xmsystem.xmadmin.ui.command.createprodeleteevtaction"; //$NON-NLS-1$
		
		/** The Constant CHANGE_PRO_DELETE_ACTION. */
		public static final String CHANGE_PRO_DELETE_ACTION = "com.magna.xmsystem.xmadmin.ui.command.changeprodeleteevtaction"; //$NON-NLS-1$
		
		/** The Constant DELETE_PRO_DELETE_ACTION. */
		public static final String DELETE_PRO_DELETE_ACTION = "com.magna.xmsystem.xmadmin.ui.command.deleteprodeleteevtaction"; //$NON-NLS-1$
		
		/** The Constant CREATEAS_PRO_DEACTIVATE_ACTION. */
		public static final String CREATEAS_PRO_DEACTIVATE_ACTION = "com.magna.xmsystem.xmadmin.ui.command.createasprodeactivateevtaction"; //$NON-NLS-1$
		
		/** The Constant CREATE_PRO_DEACTIVATE_ACTION. */
		public static final String CREATE_PRO_DEACTIVATE_ACTION = "com.magna.xmsystem.xmadmin.ui.command.createprodeactivateevtaction"; //$NON-NLS-1$
		
		/** The Constant CHANGE_PRO_DEACTIVATE_ACTION. */
		public static final String CHANGE_PRO_DEACTIVATE_ACTION = "com.magna.xmsystem.xmadmin.ui.command.changeprodeactivateevtaction"; //$NON-NLS-1$
		
		/** The Constant DELETE_PRO_DEACTIVATE_ACTION. */
		public static final String DELETE_PRO_DEACTIVATE_ACTION = "com.magna.xmsystem.xmadmin.ui.command.deleteprodeactivateevtaction"; //$NON-NLS-1$
		
		/** The Constant CREATEAS_PRO_ACTIVATE_ACTION. */
		public static final String CREATEAS_PRO_ACTIVATE_ACTION = "com.magna.xmsystem.xmadmin.ui.command.createasproactivateevtaction"; //$NON-NLS-1$
		
		/** The Constant CREATE_PRO_ACTIVATE_ACTION. */
		public static final String CREATE_PRO_ACTIVATE_ACTION = "com.magna.xmsystem.xmadmin.ui.command.createproactivateevtaction"; //$NON-NLS-1$
		
		/** The Constant CHANGE_PRO_ACTIVATE_ACTION. */
		public static final String CHANGE_PRO_ACTIVATE_ACTION = "com.magna.xmsystem.xmadmin.ui.command.changeproactivateevtaction"; //$NON-NLS-1$
		
		/** The Constant DELETE_PRO_ACTIVATE_ACTION. */
		public static final String DELETE_PRO_ACTIVATE_ACTION = "com.magna.xmsystem.xmadmin.ui.command.deleteproactivateevtaction"; //$NON-NLS-1$
		
		/** The Constant CREATEAS_USER_PRO_REL_ASSIGN_ACTION. */
		public static final String CREATEAS_USER_PRO_REL_ASSIGN_ACTION = "com.magna.xmsystem.xmadmin.ui.command.createasuserprorelassignevtaction"; //$NON-NLS-1$
		
		/** The Constant CREATE_USER_PRO_REL_ASSIGN_ACTION. */
		public static final String CREATE_USER_PRO_REL_ASSIGN_ACTION = "com.magna.xmsystem.xmadmin.ui.command.createuserprorelassignevt"; //$NON-NLS-1$
		
		/** The Constant CHANGE_USER_PRO_REL_ASSIGN_ACTION. */
		public static final String CHANGE_USER_PRO_REL_ASSIGN_ACTION = "com.magna.xmsystem.xmadmin.ui.command.changeuserprorelassignevtaction"; //$NON-NLS-1$
		
		/** The Constant DELETE_USER_PRO_REL_ASSIGN_ACTION. */
		public static final String DELETE_USER_PRO_REL_ASSIGN_ACTION = "com.magna.xmsystem.xmadmin.ui.command.deleteuserprorelassignevtaction"; //$NON-NLS-1$
		
		/** The Constant CREATEAS_USER_PRO_REL_ASSIGN_ACTION. */
		public static final String CREATEAS_USER_PRO_REL_REMOVE_ACTION = "com.magna.xmsystem.xmadmin.ui.command.createasuserprorelremoveevtaction"; //$NON-NLS-1$
		
		/** The Constant CREATE_USER_PRO_REL_ASSIGN_ACTION. */
		public static final String CREATE_USER_PRO_REL_REMOVE_ACTION = "com.magna.xmsystem.xmadmin.ui.command.createuserprorelremoveevtaction"; //$NON-NLS-1$
		
		/** The Constant CHANGE_USER_PRO_REL_ASSIGN_ACTION. */
		public static final String CHANGE_USER_PRO_REL_REMOVE_ACTION = "com.magna.xmsystem.xmadmin.ui.command.changeuserprorelremoveevtaction"; //$NON-NLS-1$
		
		/** The Constant DELETE_USER_PRO_REL_ASSIGN_ACTION. */
		public static final String DELETE_USER_PRO_REL_REMOVE_ACTION = "com.magna.xmsystem.xmadmin.ui.command.deleteuserprorelremoveevtaction"; //$NON-NLS-1$
		
		/** The Constant CREATEAS_PRO_AA_REL_ASIGN_ACTION. */
		public static final String CREATEAS_PRO_AA_REL_ASIGN_ACTION = "com.magna.xmsystem.xmadmin.ui.command.createasproaarelassignevtaction"; //$NON-NLS-1$
		
		/** The Constant CREATE_PRO_AA_REL_ASIGN_ACTION. */
		public static final String CREATE_PRO_AA_REL_ASIGN_ACTION = "com.magna.xmsystem.xmadmin.ui.command.createproaarelassignevtaction"; //$NON-NLS-1$
		
		/** The Constant CHANGE_PRO_AA_REL_ASIGN_ACTION. */
		public static final String CHANGE_PRO_AA_REL_ASIGN_ACTION = "com.magna.xmsystem.xmadmin.ui.command.changeproaarelassignevtaction"; //$NON-NLS-1$
		
		/** The Constant DELETE_PRO_AA_REL_ASIGN_ACTION. */
		public static final String DELETE_PRO_AA_REL_ASIGN_ACTION = "com.magna.xmsystem.xmadmin.ui.command.deleteproaarelassignevtaction"; //$NON-NLS-1$
		
		/** The Constant CREATEAS_TEMPLATE. */
		public static final String CREATEAS_TEMPLATE = "com.magna.xmsystem.xmadmin.ui.command.createastemplate"; //$NON-NLS-1$
		
		/** The Constant CREATE_TEMPLATE. */
		public static final String CREATE_TEMPLATE = "com.magna.xmsystem.xmadmin.ui.command.createnotitemplate"; //$NON-NLS-1$
		
		/** The Constant CHANGE_TEMPLATE. */
		public static final String CHANGE_TEMPLATE = "com.magna.xmsystem.xmadmin.ui.command.changetemplate"; //$NON-NLS-1$
		
		/** The Constant DELETE_TEMPLATE. */
		public static final String DELETE_TEMPLATE = "com.magna.xmsystem.xmadmin.ui.command.deletetemplate"; //$NON-NLS-1$
		
	}

	/**
	 * The Class HANDLER_ID.
	 */
	public static class HANDLER_ID {

		/** The Constant DYNAMIC_MENU_HANDLER. */
		public static final String DYNAMIC_MENU_HANDLER = "com.magna.xmsystem.xmadmin.ui.handler.dynamicmenuhandler";
	}

	/**
	 * The Class OPERATIONMODE.
	 */
	public static class OPERATIONMODE {

		/** The Constant VIEW. */
		public static final int VIEW = 0; // $NON-NLS-1$

		/** The Constant CREATE. */
		public static final int CREATE = 1; // $NON-NLS-1$

		/** The Constant CHANGE. */
		public static final int CHANGE = 2; // $NON-NLS-1$
	}

	/**
	 * The Class EVENT_BROKER.
	 */
	public static class EVENT_BROKER {

		/** The Constant STATUSBAR. */
		public static final String STATUSBAR = "statusbar";

		/** The Constant HIDEINACTIVEITEMS. */
		public static final String HIDEINACTIVEITEMS = "hideinactiveitems";

		/** The Constant CONSOLE_MESSAGE. */
		public static final String CONSOLE_MESSAGE = "livemessage";
	}

	/**
	 * The Class THEMES.
	 */
	public static class THEMES {

		/** The Constant LOGIN_THEME. */
		public static final String LOGIN_THEME = "com.magna.xmadmin.themes.login";
	}

	/**
	 * The Class Application.
	 */
	public static class Application {

		/** The Constant POSITIONS. */
		public static final String[] POSITIONS = new String[] { "*ButtonTask*", "*IconTask*", "*MenuTask*" };
	}

	/**
	 * The Class RegularExpressions.
	 */
	public static class RegularExpressions {

		/** The Constant ALLOWED_SITE_NAME_REGEXP. */
		public static final String ALLOWED_SITE_NAME_REGEXP = "^[a-zA-Z0-9_]*$";
		
		/** The Constant ALLOWED_NAME_REGEX. */
		public static final String ALLOWED_NAME_REGEX = "^[ a-zA-Z0-9@_\\-]*$";

		/** The Constant ALLOWED_APP_NAME_REGEX. */
		public static final String ALLOWED_APP_NAME_REGEX = "^[ a-zA-Z0-9@_\\-\\\\/\\.\\>\\<]*$";

		/** The Constant ALLOWED_DIALOGBOX_REGEX. */
		public static final String ALLOWED_DIALOGBOX_REGEX = "[0-9a-zA-Z@_\\s\\-]+";

		/** The Constant ALLOWED_PROJECT_NAME_REGEX. */
		public static final String ALLOWED_PROJECT_NAME_REGEX = "^[a-zA-Z0-9_-]*$";

		/** The Constant EMAIL_REGEX. */
		public static final String EMAIL_REGEX = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
				+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

		/** The Constant CONTACT_NUMBER_REGEX. */
//		public static final String CONTACT_EMPTY_NUM_REGEX = /*"^\\+[0-9]{1,3}\\ [0-9]{11}(?:x.+)?$"*/"(^\\+\\s*$)";
		
		/** The Constant CONTACT_NUMBER_REGEX. */
		public static final String CONTACT_NUMBER_REGEX = "^\\+([0-9]){1,3}\\-([0-9]){1,3}\\-([0-9]){1,4}\\-([0-9]){1,4}$";
		
		/** The Constant PORT_NUMBER. */
		public static final String PORT_NUMBER = "^([0-9]\\d{0,3}|0|[0-5][0-9]{4}|6[0-9][0-9]{3}|7[0-9][0-9]{3}|8[0-9][0-9]{3}|9[0-9][0-9]{3})$";
		
		/** The Constant LDAP_URL. */
		public static final String LDAP_URL = "^(ldap:\\/\\/).*?";
		
		/** The Constant URL. */
		public static final String URL = "^(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]";
		//public static final String URL ="(http:\\/\\/www\\.|https:\\/\\/www\\.|http:\\/\\/|https:\\/\\/)?[a-z0-9]+([\\-\\.]{1}[a-z0-9]+)*\\.[a-z]{2,5}(:[0-9]{1,5})?(\\/.*)?";
		
	}
	
	/**
	 * The Class Messages.
	 */
	public static class Messages{
		
		/** The Constant NAME_MESSAGE. */
		public static final String NAME_MESSAGE = "Name field - Allowed Special Characters ([A-Z][a-z][0-9]_)";

		/** The Constant CONTACT_MESSAGE. */
		public static final String CONTACT_MESSAGE = "(+XX)-(XXX)-(XXXX)-(XXXX)";
		
		/** The Constant EMAIL_MESSAGE. */
		public static final String EMAIL_MESSAGE = "example@magna.com";
		
		
		/** The Constant FORMATER_MESSAGE. */
		public static final String FORMATER_MESSAGE="+##-###-####-####";
		
	}

	/**
	 * The Class SuperAdminRole.
	 *//*
	public static class RelationElement {

		*//** The Constant USER_APPLICATIONS. *//*
		public static final String USER_APPLICATIONS = "USER_APPLICATIONS";

		*//** The Constant PROJECT_APPLICATIONS. *//*
		public static final String PROJECT_APPLICATIONS = "PROJECT_APPLICATIONS";

		*//** The Constant START_APPLICATIONS. *//*
		public static final String START_APPLICATIONS = "START_APPLICATIONS";

		*//** The Constant BASE_APPLICATIONS. *//*
		public static final String BASE_APPLICATIONS = "BASE_APPLICATIONS";

		*//** The Constant TREE_LOADING. *//*
		public static final String TREE_LOADING = "TREE_LOADING";

		*//** The Constant SITE_ADMINISTRATION_AREA. *//*
		public static final String SITE_ADMINISTRATION_AREA = "SITE_ADMINISTRATION_AREA";
		
		*//** The Constant SITE_ADMINAREA_PROJECTS. *//*
		public static final String SITE_ADMINAREA_PROJECTS = "SITE_ADMINAREA_PROJECTS";
		
		*//** The Constant SITE_ADMINAREA_USERAPPS. *//*
		public static final String SITE_ADMINAREA_USERAPPS = "SITE_ADMINAREA_USERAPPS";
		
		*//** The Constant SITE_ADMINAREA_USERAPP_NOTFIXED. *//*
		public static final String SITE_ADMINAREA_USERAPP_NOTFIXED = "SITE_ADMINAREA_USERAPP_NOTFIXED";
		
		*//** The Constant SITE_ADMINAREA_USERAPP_FIXED. *//*
		public static final String SITE_ADMINAREA_USERAPP_FIXED = "SITE_ADMINAREA_USERAPP_FIXED";
		
		*//** The Constant SITE_ADMINAREA_USERAPP_PROTECTED. *//*
		public static final String SITE_ADMINAREA_USERAPP_PROTECTED = "SITE_ADMINAREA_USERAPP_PROTECTED";
		
		*//** The Constant SITE_ADMINAREA_STARTAPPS. *//*
		public static final String SITE_ADMINAREA_STARTAPPS = "SITE_ADMINAREA_STARTAPPS";
		
		*//** The Constant SITE_ADMINAREA_PROJECTAPPS. *//*
		public static final String SITE_ADMINAREA_PROJECTAPPS = "SITE_ADMINAREA_PROJECTAPPS";
		
		*//** The Constant SITE_ADMINAREA_PROJECTAPP_NOTFIXED. *//*
		public static final String SITE_ADMINAREA_PROJECTAPP_NOTFIXED = "SITE_ADMINAREA_USERAPP_NOTFIXED";
		
		*//** The Constant SITE_ADMINAREA_PROJECTAPP_FIXED. *//*
		public static final String SITE_ADMINAREA_PROJECTAPP_FIXED = "SITE_ADMINAREA_PROJECTAPP_FIXED";
		
		*//** The Constant SITE_ADMINAREA_PROJECTAPP_PROTECTED. *//*
		public static final String SITE_ADMINAREA_PROJECTAPP_PROTECTED = "SITE_ADMINAREA_PROJECTAPP_PROTECTED";
		
		*//** The Constant SITE_ADMINAREA_PROJECT_STARTAPPS. *//*
		public static final String SITE_ADMINAREA_PROJECT_STARTAPPS = "SITE_ADMINAREA_PROJECT_STARTAPPS";
		
		*//** The Constant ROLES_SUPER_ADMINS. *//*
		public static final String ROLES_SUPER_ADMINS = "ROLES_SUPER_ADMINS";

		*//** The Constant ROLE_USERS. *//*
		public static final String ROLE_USERS = "ROLE_USERS";		
	}*/

	public static class SuperAdminRole {
		
		/** The Constant NAME. */
		public static final String NAME = "SuperAdmin";
		
		/** The Constant DESCRIPATION. */
		public static final String DESCRIPATION = "This is superAdmin";
	}
	
	/**
	 * The Class Notification.
	 */
	public static class Notification {

		/** The Constant TRUE. */
		public static final String TRUE = "true";

		/** The Constant FALSE. */
		public static final String FALSE = "false";
		
		/** The Constant PRO_CREATE_VARIABLES. */
		public static final String[] PRO_CREATE_VARIABLES = new String[] { NotificationVariables.TO_USERNAME.toString(),
				NotificationVariables.PROJECT_NAME.toString(), NotificationVariables.PROJECT_CREATE_DATE.toString(),
				NotificationVariables.REMARKS.toString() };
		
		/** The Constant PRO_DELETE_VARIABLES. */
		public static final String[] PRO_DELETE_VARIABLES = new String[] { NotificationVariables.TO_USERNAME.toString(),
				NotificationVariables.PROJECT_NAME.toString(), NotificationVariables.REMARKS.toString() };
		
		/** The Constant PRO_ACTIVATE_VARIABLES. */
		public static final String[] PRO_ACTIVATE_VARIABLES = new String[] { NotificationVariables.TO_USERNAME.toString(),
				NotificationVariables.PROJECT_NAME.toString(), NotificationVariables.REMARKS.toString() };
		
		/** The Constant PRO_DEACTIVATE_VARIABLES. */
		public static final String[] PRO_DEACTIVATE_VARIABLES = new String[] { NotificationVariables.TO_USERNAME.toString(),
				NotificationVariables.PROJECT_NAME.toString(), NotificationVariables.REMARKS.toString() };
		
		/** The Constant PROJECT_ADMIN_AREA_RELATION_ASSIGN_VAR. */
		public static final String[] PROJECT_ADMIN_AREA_RELATION_ASSIGN_VAR = new String[] { NotificationVariables.TO_USERNAME.toString(),
				NotificationVariables.PROJECT_NAME.toString(), NotificationVariables.ADMIN_AREA_NAME.toString() };
		
		/** The Constant PROJECT_ADMIN_AREA_RELATION_REMOVE_VAR. */
		public static final String[] PROJECT_ADMIN_AREA_RELATION_REMOVE_VAR = new String[] { NotificationVariables.TO_USERNAME.toString(),
				NotificationVariables.PROJECT_NAME.toString(), NotificationVariables.ADMIN_AREA_NAME.toString() };
		
		/** The Constant USER_PROJECT_RELATION_ASSIGN_VAR. */
		public static final String[] USER_PROJECT_RELATION_ASSIGN_VAR = new String[] { NotificationVariables.TO_USERNAME.toString(),
				NotificationVariables.PROJECT_NAME.toString(), NotificationVariables.ASSIGNED_USERNAME.toString() };
		
		/** The Constant USER_PROJECT_RELATION_REMOVE_VAR. */
		public static final String[] USER_PROJECT_RELATION_REMOVE_VAR = new String[] { NotificationVariables.TO_USERNAME.toString(),
				NotificationVariables.PROJECT_NAME.toString(), NotificationVariables.ASSIGNED_USERNAME.toString() };
		
		/** The Constant USER_PROJECT_RELATION_EXPIRY_VAR. */
		public static final String[] USER_PROJECT_RELATION_EXPIRY_VAR = new String[] {
				NotificationVariables.TO_USERNAME.toString(), NotificationVariables.PROJECT_NAME.toString(),
				NotificationVariables.PROJECT_EXPIRY_DAYS.toString(), NotificationVariables.GRACE_PERIOD.toString() };
		
		/** The Constant USER_PROJECT_RELATION_GRACE_EXPIRY_VAR. */
		public static final String[] USER_PROJECT_RELATION_GRACE_EXPIRY_VAR = new String[] {
				NotificationVariables.TO_USERNAME.toString(), NotificationVariables.PROJECT_NAME.toString()};
		
		/** The Constant EMAIL_NOTIFY_TO_AA_PROJECT_USERS_VAR. */
		public static final String[] EMAIL_NOTIFY_TO_AA_PROJECT_USERS_VAR = new String[] {
				NotificationVariables.TO_USERNAME.toString() };
		
		/** The Constant EMAIL_NOTIFY_TO_PROJECT_USERS_VAR. */
		public static final String[] EMAIL_NOTIFY_TO_PROJECT_USERS_VAR = new String[] {
				NotificationVariables.TO_USERNAME.toString() };
		
	}
	
	/**
	 * The Class History.
	 */
	public static class History {
		
		/** The Constant QUERYTYPE. */
		public static final String[] QUERYTYPE = new String[] {  "=", "LIKE", "<>" };
		
		/** The Constant RESULT_QUERY_TYPE. */
		public static final String[] RESULT_QUERY_TYPE = new String[] { "=", "<>" };
		
		/** The Constant RESULTTYPE. */
		public static final String[] RESULTTYPE = new String[] { "SUCCESS", "FAILURE" };
		
		/** The Constant OPERATIONTYPE. */
		public static final String[] OPERATIONTYPE = new String[] { "OperationDone", "NotDone" };
		
		/** The Constant STATUS_TYPE. */
		public static final String[] STATUS_TYPE = new String[] { "ACTIVE", "INACTIVE" };
		
		/** The Constant USER_HISTORY_ROW_LIMIT. */
		public static final String USER_HISTORY_ROW_LIMIT = "10000";
		
		/** The Constant ADMIN_HISTORY_ROW_LIMIT. */
		public static final String ADMIN_HISTORY_ROW_LIMIT = "10000";
		
		/** The Constant HISTORY_HIDDEN_ROW_LIMIT. */
		public static final String HISTORY_HIDDEN_ROW_LIMIT = "400000";
		
		/** The Constant ADMIN_HISTORY_JOB_NAME. */
		public static final String ADMIN_HISTORY_JOB_NAME = "Loading AdminHistory...";
		
		/** The Constant USER_HISTORY_JOB_NAME. */
		public static final String USER_HISTORY_JOB_NAME = "Loading UserHistory..." ;
	}
}
