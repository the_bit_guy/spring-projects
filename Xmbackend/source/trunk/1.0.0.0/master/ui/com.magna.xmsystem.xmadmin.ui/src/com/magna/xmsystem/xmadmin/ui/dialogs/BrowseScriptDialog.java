package com.magna.xmsystem.xmadmin.ui.dialogs;

import java.io.File;
import java.text.Collator;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.jface.viewers.ITreeSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerComparator;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Link;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeColumn;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magna.xmsystem.dependencies.utils.LANG_ENUM;
import com.magna.xmsystem.dependencies.utils.XMSystemUtil;
import com.magna.xmsystem.ui.controls.util.FilteredTreeControl;
import com.magna.xmsystem.ui.controls.util.PatternFilterTree;
import com.magna.xmsystem.xmadmin.message.Message;
import com.magna.xmsystem.xmadmin.util.CommonConstants;

/**
 * The Class BrowseScriptDialogNew.
 * 
 * @author Archita.patel
 */
public class BrowseScriptDialog extends Dialog {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(BrowseScriptDialog.class);

	/** Member variable 'parent shell' for {@link Shell}. */
	private Shell parentShell;

	/** Member variable 'script list' for {@link ArrayList<String>}. */
	private ArrayList<File> scriptList;

	/** Member variable 'main composite' for {@link Composite}. */
	private Composite mainComposite;

	/** The browse dialog tree viewer. */
	private BrowseScriptDialogTreeViewer browseDialogTreeViewer;

	/** Member variable for group. */
	private Group grpScript;

	/** The messages. */
	transient private Message messages;

	/** Member variable 'selected script' for {@link String}. */
	private String selectedScript;

	/** The btn browse. */
	private Button btnBrowse;

	/** The txt browse. */
	private Text txtBrowse;

	/** The lnk clear. */
	private Link lnkClear;
	
	/** The Constant OK_LABEL_EN. */
	final static private String OK_LABEL_EN = "OK";
	
	/** The Constant OK_LABEL_DE. */
	final static private String OK_LABEL_DE = "OK";
	
	/** The Constant CANCEL_LABEL_EN. */
	final static private String CANCEL_LABEL_EN = "Cancel";
	
	/** The Constant CANCEL_LABEL_DE. */
	final static private String CANCEL_LABEL_DE = "Abbrechen";

	/**
	 * Instantiates a new browse script dialog new.
	 *
	 * @param shell
	 *            the shell
	 * @param messages
	 *            the messages
	 */
	public BrowseScriptDialog(final Shell shell, final Message messages) {

		super(shell);
		this.parentShell = shell;
		this.messages = messages;
		try {
			final File scriptsLocFile = XMSystemUtil.getXMSystemServerScriptFolder();
			if (scriptsLocFile.isDirectory()) {
				File[] scriptArray = scriptsLocFile.listFiles();
				if (scriptArray != null) {
					scriptList = new ArrayList<File>(Arrays.asList(scriptArray));
				}
			}

		} catch (Exception e) {
			LOGGER.error("Execution occurred while dialog initiation!", e); //$NON-NLS-1$
		}
	}

	/**
	 * Configures the Shell.
	 *
	 * @param newShell
	 *            the new shell
	 * @see org.eclipse.jface.window.Window#configureShell(org.eclipse.swt.widgets.Shell)
	 */
	@Override
	protected void configureShell(final Shell newShell) {
		try {
			super.configureShell(newShell);
			newShell.setText(messages.browseScriptDialogTitle);
			newShell.setParent(this.parentShell);
			final Rectangle shellBounds = parentShell.getBounds();
			int width = 550;
			int height = 400;
			newShell.setSize(width, height);
			final Point pos = new Point((shellBounds.width - width) / 2, (shellBounds.height - height) / 2);
			newShell.setLocation(pos.x, pos.y);
		} catch (Exception e) {
			LOGGER.error("Execution occurred while configuring shell!", e); //$NON-NLS-1$
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.dialogs.Dialog#createDialogArea(org.eclipse.swt.widgets
	 * .Composite)
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		try {
			final Composite widgetContainer = new Composite(parent, SWT.NONE);
			final GridLayout widgetContainerLayout = new GridLayout(1, false);
			widgetContainer.setLayout(widgetContainerLayout);
			widgetContainer.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
			initGUI(widgetContainer);
		} catch (Exception e) {
			LOGGER.error("Unable to create UI elements", e);
		}
		return this.mainComposite;

	}
	
	@Override
	protected void createButtonsForButtonBar(final Composite parent) {
		// create OK and Cancel buttons by default
		parent.setBackgroundMode(SWT.INHERIT_DEFAULT);
		super.createButtonsForButtonBar(parent);
		
		Button cancelButton = getButton(IDialogConstants.CANCEL_ID);
		Button okButton = getButton(IDialogConstants.OK_ID);
		LANG_ENUM currentLang = LANG_ENUM.ENGLISH;
		final String localeString = XMSystemUtil.getLanguage();
		if(!XMSystemUtil.isEmpty(localeString)) {
			currentLang = LANG_ENUM.valueOf(localeString.toUpperCase());
		}
		if (currentLang == LANG_ENUM.GERMAN) {
			cancelButton.setText(CANCEL_LABEL_DE);
			okButton.setText(OK_LABEL_DE);
		}else{
			cancelButton.setText(CANCEL_LABEL_EN);
			okButton.setText(OK_LABEL_EN);
		}
	}


	/**
	 * Inits the GUI.
	 *
	 * @param parent
	 *            the parent
	 */
	private void initGUI(final Composite parent) {
		try {

			this.grpScript = new Group(parent, SWT.NONE);
			GridLayoutFactory.fillDefaults().numColumns(1).applyTo(this.grpScript);
			GridDataFactory.fillDefaults().grab(true, true).span(SWT.FILL, SWT.FILL).applyTo(this.grpScript);

			final Composite widgetContainer = new Composite(this.grpScript, SWT.NONE);
			final GridLayout widgetContainerLayout = new GridLayout(3, false);
			widgetContainer.setLayout(widgetContainerLayout);
			widgetContainer.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

			createTreeViewer(widgetContainer);
			
			this.txtBrowse = new Text(widgetContainer, SWT.BORDER);
			GridDataFactory.fillDefaults().grab(true, false).align(SWT.FILL, SWT.CENTER).span(1, 1)
					.applyTo(this.txtBrowse);

			this.btnBrowse = new Button(widgetContainer, SWT.NONE);
			GridDataFactory.fillDefaults().grab(false, false).align(SWT.LEFT, SWT.CENTER).span(1, 1)
					.applyTo(this.btnBrowse);
			this.btnBrowse.setText(messages.browseScriptDialogBrowseBtn);

			this.lnkClear = new Link(widgetContainer, SWT.NONE);
			GridDataFactory.fillDefaults().grab(false, false).align(SWT.LEFT, SWT.CENTER).span(1, 1)
					.applyTo(this.lnkClear);
			this.lnkClear.setText("<a>" + messages.browseScriptDialogClearLnk + "</a>");

			

			this.browseDialogTreeViewer.setContentProvider(new BrowseScriptDialogContentProvider());
			this.browseDialogTreeViewer.setInput(scriptList);
			this.browseDialogTreeViewer.getFirstColoum().getColumn()
					.setText(messages.browseScriptDialogTreeScriptColumnLabel);
			this.browseDialogTreeViewer.getSecondColumn().getColumn()
					.setText(messages.browseScriptDialogTreeDateColumnLabel);
			initListeners();

			this.browseDialogTreeViewer.setComparator(new ViewerComparator() {
				public int compare(Viewer viewer, Object e1, Object e2) {
					return compareElements(e1, e2);
				}
			});

		} catch (Exception ex) {
			LOGGER.error("Unable to crete UI elements", ex); //$NON-NLS-1$
		}

	}

	/**
	 * Inits the listeners.
	 */
	private void initListeners() {

		browsebtnListener();
		clearLinkListener();
		treeColumnListeners();
	}

	/**
	 * Clear link listener.
	 */
	private void clearLinkListener() {
		this.lnkClear.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent paramSelectionEvent) {
				txtBrowse.setText(CommonConstants.EMPTY_STR);
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent paramSelectionEvent) {
				// TODO Auto-generated method stub

			}
		});
		this.txtBrowse.addModifyListener(new ModifyListener() {

			@Override
			public void modifyText(ModifyEvent paramModifyEvent) {
				if (txtBrowse.getText().trim().length() == 0) {
					//browseDialogTreeViewer.refresh();
					browseDialogTreeViewer.getTree().setEnabled(true);
				} else {
					//browseDialogTreeViewer.getTree().clearAll(true);
					browseDialogTreeViewer.getTree().setEnabled(false);

				}
			}
		});
	}

	/**
	 * Browsebtn listener.
	 */
	private void browsebtnListener() {
		this.btnBrowse.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent event) {
				final Button widget = (Button) event.widget;
				final FileDialog dialog = new FileDialog(widget.getShell(), SWT.OPEN);
				final String selectedFilePath = dialog.open();

				if (selectedFilePath != null) {
					txtBrowse.setText(selectedFilePath);
				}
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub

			}
		});

	}

	/**
	 * Creates the table viewer.
	 *
	 * @param widgetContainer
	 *            the widget container
	 */
	private void createTreeViewer(final Composite widgetContainer) {
		FilteredTreeControl scriptTreeControl = new FilteredTreeControl(widgetContainer, SWT.NONE,
				new PatternFilterTree()) {
			/**
			 * Overrides doCreateTreeViewer method
			 */
			@Override
			protected TreeViewer doCreateTreeViewer(final Composite parentL, final int style) {
				BrowseScriptDialog.this.browseDialogTreeViewer = new BrowseScriptDialogTreeViewer(parentL);
				return BrowseScriptDialog.this.browseDialogTreeViewer;
			}
		};
		/*
		 * GridData layoutData = new GridData(SWT.FILL, SWT.FILL, true, true);
		 * scriptTreeControl.setLayoutData(layoutData);
		 */

		GridDataFactory.fillDefaults().grab(true, true).align(SWT.FILL, SWT.FILL).span(3, 1).applyTo(scriptTreeControl);
	}

	/**
	 * Compare elements.
	 *
	 * @param element1
	 *            the element1
	 * @param element2
	 *            the element2
	 * @return the int
	 */
	protected int compareElements(final Object element1, final Object element2) {
		final Tree tree = this.browseDialogTreeViewer.getTree();
		int result = 0;
		SimpleDateFormat dateFormate = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
		if (tree.getSortColumn().equals(tree.getColumn(0))) {
			String name1 = ((File) element1).getName();
			String name2 = ((File) element2).getName();
			result = Collator.getInstance().compare(name1, name2);
		} else {
			Long date1 = ((File) element1).lastModified();
			Long date2 = ((File) element2).lastModified();
			result = Collator.getInstance().compare(dateFormate.format(date1), dateFormate.format(date2));

		}

		return tree.getSortDirection() == SWT.UP ? result : -result;
	}

	/**
	 * Adds the tree column selection listeners.
	 */
	private void treeColumnListeners() {
		TreeColumn column;
		Tree tree = this.browseDialogTreeViewer.getTree();
		int totalColumn = tree.getColumns().length;
		for (int i = 0; i < totalColumn; i++) {
			column = tree.getColumn(i);
			addColumnSelectionListener(column);
		}
	}

	/**
	 * Adds the column selection listener.
	 *
	 * @param column
	 *            the column
	 */
	private void addColumnSelectionListener(final TreeColumn column) {
		column.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent event) {
				treeColumnClicked((TreeColumn) event.widget);
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) {

			}
		});

	}

	/**
	 * Tree column clicked.
	 *
	 * @param column
	 *            the column
	 */
	private void treeColumnClicked(TreeColumn column) {
		Tree tree = column.getParent();
		if (column.equals(tree.getSortColumn())) {
			tree.setSortDirection(tree.getSortDirection() == SWT.UP ? SWT.DOWN : SWT.UP);
		} else {
			tree.setSortColumn(column);
			tree.setSortDirection(SWT.UP);
		}
		this.browseDialogTreeViewer.refresh();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.dialogs.Dialog#okPressed()
	 */
	@Override
	protected void okPressed() {

		ITreeSelection selection;
		selection = (ITreeSelection) this.browseDialogTreeViewer.getSelection();
		Object element;
		if (selection != null && (element = selection.getFirstElement()) != null
				&& this.browseDialogTreeViewer.getTree().isEnabled()) {
			String fileName = ((File) element).getName();
			this.selectedScript = fileName;
		} else {
			this.selectedScript = this.txtBrowse.getText();
		}

		super.okPressed();
	}

	/**
	 * Gets the selected script.
	 *
	 * @return the selected script
	 */
	public String getSelectedScript() {
		return selectedScript;
	}

}
