
package com.magna.xmsystem.xmadmin.ui.logger.user.handlers;

import org.eclipse.core.runtime.preferences.IEclipsePreferences;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.core.di.extensions.Preference;

import com.magna.xmsystem.xmadmin.util.CommonConstants;
import com.magna.xmsystem.xmadmin.util.XMAdminUtil;

/**
 * The Class UserHistoryHandler.
 * 
 * @author shashwat.anand
 */
@SuppressWarnings("restriction")
public class UserHistoryHandler {
	/**
	 * Execute.
	 *
	 * @param preferences
	 *            the preferences
	 */
	@Execute
	public void execute(
			@Preference(nodePath = CommonConstants.USER_HISTROY_PREF) final IEclipsePreferences preferences) {		
		XMAdminUtil.getInstance().openUserHistory(preferences);
	}
}