package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.applicationmodel.userapplicationmodel;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.refs.RelationObj;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.users.User;

/**
 * The Class UserAppUsersModel.
 */
public class UserAppUsers implements IAdminTreeChild {

	/** The parent. */
	private IAdminTreeChild parent;

	/** The user app user child. */
	private Map<String, IAdminTreeChild> userAppUserChildren;

	/**
	 * Instantiates a new user app users model.
	 *
	 * @param parent the parent
	 */
	public UserAppUsers(final IAdminTreeChild parent) {
		this.parent = parent;
		this.userAppUserChildren = new LinkedHashMap<>();
	}

	/**
	 * Add.
	 *
	 * @param UserAppUserChildId the user app user child id
	 * @param child the child
	 * @return the i admin tree child
	 */
	public IAdminTreeChild add(final String userAppUserChildId, final IAdminTreeChild child) {
		child.setParent(this);
		IAdminTreeChild returnVal = this.userAppUserChildren.put(userAppUserChildId, child);
		if (child instanceof RelationObj && ((RelationObj) child).getRefObject() instanceof User) {
			sort();
		}
		return returnVal;
	}

	/**
	 * Remove.
	 *
	 * @param UserAppUserChildId the user app user child id
	 * @return the i admin tree child
	 */
	public IAdminTreeChild remove(final String userAppUserChildId) {
		return this.userAppUserChildren.remove(userAppUserChildId);
	}
	/**
	 * Method for Removes the all.
	 */
	public void removeAll() {
		this.userAppUserChildren.clear();
	}
	/**
	 * Gets the user app user child collection.
	 *
	 * @return the user app user child collection
	 */
	public Collection<IAdminTreeChild> getUserAppUserChildrenCollection() {
		return this.userAppUserChildren.values();
	}

	/**
	 * Gets the user app user child.
	 *
	 * @return the user app user child
	 */
	public Map<String, IAdminTreeChild> getUserAppUserChildren() {
		return userAppUserChildren;
	}
	
	/* (non-Javadoc)
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#getParent()
	 */
	@Override
	public IAdminTreeChild getParent() {
		return this.parent;
	}

	/* (non-Javadoc)
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#setParent(com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild)
	 */
	@Override
	public void setParent(final IAdminTreeChild parent) {
		this.parent = parent;
	}
	
	/**
	 * Sort.
	 */
	public void sort() {
		final LinkedHashMap<String, IAdminTreeChild> collect = this.userAppUserChildren.entrySet().stream().sorted(
				(e1, e2) -> ((User) e1.getValue()).getName().compareTo(((User) e2.getValue()).getName()))
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
		this.userAppUserChildren = collect;
	}
}
