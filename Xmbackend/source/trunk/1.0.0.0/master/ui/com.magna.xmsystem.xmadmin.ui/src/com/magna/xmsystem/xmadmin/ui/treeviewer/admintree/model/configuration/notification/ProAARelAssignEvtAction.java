package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification;

import java.beans.PropertyChangeEvent;

public class ProAARelAssignEvtAction extends NotificationEvtActions{

	public ProAARelAssignEvtAction(String id, String name, boolean isActive, int operationMode) {
		super(id, name, isActive, operationMode);
	}

	@Override
	public void propertyChange(PropertyChangeEvent event) {
		this.propertyChangeSupport.firePropertyChange(event.getPropertyName(), event.getOldValue(),
				event.getNewValue());
	}

	public ProAARelAssignEvtAction deepCopyProAARelAssignEvtAction(boolean b, Object object) {
		// TODO Auto-generated method stub
		return null;
	}

}
