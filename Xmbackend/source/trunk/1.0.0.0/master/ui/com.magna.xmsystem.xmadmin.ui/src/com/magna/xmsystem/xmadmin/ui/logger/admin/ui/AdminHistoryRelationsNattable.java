package com.magna.xmsystem.xmadmin.ui.logger.admin.ui;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.nebula.widgets.nattable.NatTable;
import org.eclipse.nebula.widgets.nattable.config.AbstractRegistryConfiguration;
import org.eclipse.nebula.widgets.nattable.config.ConfigRegistry;
import org.eclipse.nebula.widgets.nattable.config.DefaultNatTableStyleConfiguration;
import org.eclipse.nebula.widgets.nattable.config.IConfigRegistry;
import org.eclipse.nebula.widgets.nattable.data.ListDataProvider;
import org.eclipse.nebula.widgets.nattable.layer.DataLayer;
import org.eclipse.nebula.widgets.nattable.layer.cell.ColumnOverrideLabelAccumulator;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.ResourceAccessException;

import com.magna.xmbackend.entities.AdminHistoryRelationsTbl;
import com.magna.xmbackend.vo.adminHistory.AdminHistoryRequest;
import com.magna.xmbackend.vo.adminHistory.AdminHistoryResponse;
import com.magna.xmsystem.dependencies.customExceptions.UnauthorizedAccessException;
import com.magna.xmsystem.ui.controls.widgets.CustomMessageDialog;
import com.magna.xmsystem.xmadmin.restclient.adminHistory.AdminHistoryController;
import com.magna.xmsystem.xmadmin.ui.logger.HistoryTableUtil;
import com.magna.xmsystem.xmadmin.ui.logger.admin.ui.enums.AdminHistoryRelColumnEnum;
import com.magna.xmsystem.xmadmin.util.CommonConstants;

/**
 * The Class AdminHistoryRelationsNattable.
 */
public class AdminHistoryRelationsNattable {
	
	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(AdminHistoryRelationsNattable.class);

	/** The nat table. */
	private NatTable natTable;
	
	/** The nat table layer. */
	private AdminHistoryRelationsGridLayer natTableLayer;
	
	/** The admin history job. */
	private Job adminHistoryJob;
	
	/**
	 * Create admin history relations nat table.
	 *
	 * @param parent the parent
	 * @param adminHistoryRequest the admin history request
	 * @return the nat table
	 */
	@SuppressWarnings("rawtypes")
	public NatTable createAdminHistoryRelationsNatTable(final Composite parent, final AdminHistoryRequest adminHistoryRequest) {
		IConfigRegistry configRegistry = new ConfigRegistry();
		ArrayList<AdminHistoryRelationsTbl> userHistoryTableList = retrieveAdminHistoryTableList(adminHistoryRequest);
		natTableLayer = new AdminHistoryRelationsGridLayer(configRegistry, userHistoryTableList);

		DataLayer bodyDataLayer = natTableLayer.getBodyDataLayer();
		ListDataProvider dataProvider = natTableLayer.getBodyDataProvider();

		// NOTE: Register the accumulator on the body data layer.
		// This ensures that the labels are bound to the column index and are
		// unaffected by column order.
		final ColumnOverrideLabelAccumulator columnLabelAccumulator = new ColumnOverrideLabelAccumulator(bodyDataLayer);
		bodyDataLayer.setConfigLabelAccumulator(columnLabelAccumulator);

		natTable = new NatTable(parent, natTableLayer, false);

		GridData gd_table = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		gd_table.widthHint = 600;
		gd_table.minimumHeight = 250;
		gd_table.heightHint = 250;
		natTable.setLayoutData(gd_table);
		
		HistoryTableUtil.getInstance().addCustomStyling(natTable, dataProvider);
		natTable.addConfiguration(new DefaultNatTableStyleConfiguration());
		HistoryTableUtil.getInstance().addColumnHighlight(configRegistry);

		// natTable.addConfiguration(new DebugMenuConfiguration(natTable));
		natTable.addConfiguration(new FilterRowCustomConfiguration());

		natTable.setConfigRegistry(configRegistry);
		natTable.configure();

		return natTable;
	}

	/**
	 * Retrieve admin history table list.
	 *
	 * @param adminHistoryRequest the admin history request
	 * @return the array list
	 */
	private ArrayList<AdminHistoryRelationsTbl> retrieveAdminHistoryTableList(
			final AdminHistoryRequest adminHistoryRequest) {
		ArrayList<AdminHistoryRelationsTbl> adminHistoryTableList = new ArrayList<>();
		try {
			if (adminHistoryRequest != null) {
				AdminHistoryResponse adminHistoryResponse;
				AdminHistoryController adminHistoryController = new AdminHistoryController();

				adminHistoryResponse = adminHistoryController.getAllAdminRelationHistory(adminHistoryRequest);

				if (adminHistoryResponse != null) {
					List<Map<String, Object>> queryResultSet = adminHistoryResponse.getQueryResultSet();
					for (Map<String, Object> map : queryResultSet) {
						AdminHistoryRelationsTbl adminHistoryRelationsTbl = new AdminHistoryRelationsTbl();
						adminHistoryRelationsTbl
								.setId(Long.valueOf(String.valueOf(map.get(AdminHistoryRelColumnEnum.ID.name()))));
						Long logTime = (Long) map.get(AdminHistoryRelColumnEnum.LOG_TIME.name());
						adminHistoryRelationsTbl.setLogTime(new Date(logTime));
						adminHistoryRelationsTbl.setAdminName((String) map.get(AdminHistoryRelColumnEnum.ADMIN_NAME.name()));
						adminHistoryRelationsTbl
								.setApiRequestPath((String) map.get(AdminHistoryRelColumnEnum.API_REQUEST_PATH.name()));
						adminHistoryRelationsTbl.setOperation((String) map.get(AdminHistoryRelColumnEnum.OPERATION.name()));
						adminHistoryRelationsTbl.setSite((String) map.get(AdminHistoryRelColumnEnum.SITE.name()));
						adminHistoryRelationsTbl.setAdminArea((String) map.get(AdminHistoryRelColumnEnum.ADMIN_AREA.name()));
						adminHistoryRelationsTbl
								.setUserApplication((String) map.get(AdminHistoryRelColumnEnum.USER_APPLICATION.name()));
						adminHistoryRelationsTbl.setProject((String) map.get(AdminHistoryRelColumnEnum.PROJECT.name()));
						adminHistoryRelationsTbl
								.setProjectApplication((String) map.get(AdminHistoryRelColumnEnum.PROJECT_APPLICATION.name()));
						adminHistoryRelationsTbl.setUserName((String) map.get(AdminHistoryRelColumnEnum.USER_NAME.name()));
						adminHistoryRelationsTbl
								.setStartApplication((String) map.get(AdminHistoryRelColumnEnum.START_APPLICATION.name()));
						adminHistoryRelationsTbl
								.setRelationType((String) map.get(AdminHistoryRelColumnEnum.RELATION_TYPE.name()));
						adminHistoryRelationsTbl.setStatus((String) map.get(AdminHistoryRelColumnEnum.STATUS.name()));
						adminHistoryRelationsTbl.setGroupName((String) map.get(AdminHistoryRelColumnEnum.GROUP_NAME.name()));
						adminHistoryRelationsTbl.setDirectory((String) map.get(AdminHistoryRelColumnEnum.DIRECTORY.name()));
						adminHistoryRelationsTbl.setRole((String) map.get(AdminHistoryRelColumnEnum.ROLE.name()));
						adminHistoryRelationsTbl
								.setErrorMessage((String) map.get(AdminHistoryRelColumnEnum.ERROR_MESSAGE.name()));
						adminHistoryRelationsTbl.setResult((String) map.get(AdminHistoryRelColumnEnum.RESULT.name()));
						adminHistoryTableList.add(adminHistoryRelationsTbl);
					}
				}
			}

			return adminHistoryTableList;
		} catch (NumberFormatException e) {
			LOGGER.error("Exception occured while loading adminHistory data " + e);
		} catch (UnauthorizedAccessException e) {
			LOGGER.error("Current user is Unauthorized " + e);
		}catch (Exception e) {
			if (e instanceof ResourceAccessException) {
				/*Display.getDefault().asyncExec(new Runnable() {
					
					@Override
					public void run() {
						CustomMessageDialog.openError(Display.getDefault().getActiveShell(),"Error",
								"Server not reachable");
					}
				});*/
				throw e;
			
			}
			LOGGER.error("Exception occured while loading adminHistory data " + e);
		}
		return adminHistoryTableList;
	}

	/**
	 * The Class FilterRowCustomConfiguration.
	 */
	static class FilterRowCustomConfiguration extends AbstractRegistryConfiguration {

		/* (non-Javadoc)
		 * @see org.eclipse.nebula.widgets.nattable.config.IConfiguration#configureRegistry(org.eclipse.nebula.widgets.nattable.config.IConfigRegistry)
		 */
		@Override
		public void configureRegistry(IConfigRegistry configRegistry) {
			// configRegistry.registerConfigAttribute(
			// CellConfigAttributes.CELL_PAINTER,
			// new BackgroundPainter(new PaddingDecorator(new
			// RichTextCellPainter(), 2, 5, 2, 5)),
			// DisplayMode.NORMAL,
			// ColumnLabelAccumulator.COLUMN_LABEL_PREFIX + 4);
		}
	}

	/**
	 * Update nat table.
	 *
	 * @param query the query
	 * @param adminHistoryRequest the admin history request
	 */
	public void updateNatTable(final AdminHistoryRequest adminHistoryRequest) {

		if (adminHistoryJob != null && adminHistoryJob.getState() == Job.RUNNING) {
			//adminHistoryJob.done(Status.OK_STATUS);
			return;
		}

		adminHistoryJob = new Job(CommonConstants.History.ADMIN_HISTORY_JOB_NAME) {
			@Override
			protected IStatus run(IProgressMonitor monitor) {
				monitor.beginTask(CommonConstants.History.ADMIN_HISTORY_JOB_NAME, 100);
				monitor.worked(30);
				try {
					Display.getDefault().syncExec(new Runnable() {
						@Override
						public void run() {
							// if (!adminHistoryJob.cancel()) {
							if (natTableLayer != null) {
								natTableLayer.getBodyDataProvider().getList().clear();
							}
							// }
						}
					});
					ArrayList<AdminHistoryRelationsTbl> retrieveAdminHistoryTableList = retrieveAdminHistoryTableList(
							adminHistoryRequest);
					Display.getDefault().syncExec(new Runnable() {
						@Override
						public void run() {
							// if (!adminHistoryJob.cancel()) {
							if (natTableLayer != null && natTable != null && !natTable.isDisposed()) {
								natTableLayer.getBodyDataProvider().getList().addAll(retrieveAdminHistoryTableList);
								natTable.update();
								natTable.refresh();
							}
							// }
						}
					});

				} catch (Exception e) {
					if (e instanceof ResourceAccessException) {
						Display.getDefault().asyncExec(new Runnable() {
							@Override
							public void run() {
								CustomMessageDialog.openError(Display.getDefault().getActiveShell(), "Error",
										"Server not reachable");
							}
						});
					}
					LOGGER.error(e.getMessage());
				}

				monitor.worked(70);
				return Status.OK_STATUS;
			}
		};
		adminHistoryJob.setUser(true);
		adminHistoryJob.schedule();

	}
		/*natTableLayer.getBodyDataProvider().getList().clear();
		natTableLayer.getBodyDataProvider().getList().addAll(retrieveAdminHistoryTableList(adminHistoryRequest));
		natTable.update();
		natTable.refresh();*/
}

