package com.magna.xmsystem.xmadmin.ui.parts.notifyaaprojectuser;

import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.jface.resource.LocalResourceManager;
import org.eclipse.jface.resource.ResourceManager;
import org.eclipse.nebula.widgets.pgroup.PGroup;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.custom.StackLayout;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.events.ControlAdapter;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.events.ExpandEvent;
import org.eclipse.swt.events.ExpandListener;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magna.xmsystem.ui.controls.widgets.MagnaCustomCombo;
import com.magna.xmsystem.xmadmin.ui.parts.notification.FilterPanel;
import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.configuration.notification.NotifyAAProjectUserEvt;
import com.magna.xmsystem.xmadmin.util.XMAdminUtil;

// TODO: Auto-generated Javadoc
/**
 * The Class NotifyAAProjectUserCompositeUI.
 */
public class NotifyAAProjectUserCompositeUI extends Composite {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(NotifyAAProjectUserCompositeUI.class);

	/** The resource manager. */
	protected ResourceManager resourceManager;

	/** The grp notify AA pro user evt. */
	protected Group grpNotifyAAProUserEvt;

	/** The lbl description. */
	protected Label lblDescription;

	/** The txt description. */
	protected StyledText txtDescription;

	/** The base filter container. */
	private Composite baseFilterContainer;

	/** The filter label user. */
	protected Label filterLabelUser;

	/** The user filter button. */
	protected Button userFilterButton;

	/** The user filter text. */
	protected Text userFilterText;

	/** The user filter combo. */
	protected MagnaCustomCombo userFilterCombo;

	/** The filter label user group. */
	protected Label filterLabelUserGroup;

	/** The user group filter text. */
	protected Text userGroupFilterText;

	/** The user group filter combo. */
	protected MagnaCustomCombo userGroupFilterCombo;

	/** The user group filter button. */
	protected Button userGroupFilterButton;

	/** The addto to btn. */
	protected Button addtoToBtn;

	/** The addto CC btn. */
	protected Button addtoCCBtn;

	/** The lbl to users. */
	protected Label lblToUsers;

	/** The txt to users. */
	protected StyledText txtToUsers;

	/** The lbl CC users. */
	protected Label lblCCUsers;

	/** The txt CC users. */
	protected StyledText txtCCUsers;

	/** The lbl subject. */
	protected Label lblSubject;

	/** The txt subject. */
	protected Text txtSubject;

	/** The lbl message. */
	protected Label lblMessage;

	/** The txt message. */
	protected Text txtMessage;

	/** The user group P group. */
	protected PGroup userGroupPGroup;

	/** The user filer P group. */
	protected PGroup userFilerPGroup;

	/** The radio btn container. */
	protected Composite radioBtnContainer;

	/** The radio btn user. */
	protected Button radioBtnUser;

	/** The radio btn user group. */
	protected Button radioBtnUserGroup;

	/** The stack container. */
	protected Composite stackContainer;

	/** The stack layout. */
	protected StackLayout stackLayout;

	/** The filter panel. */
	protected FilterPanel filterPanel;

	/** The base scrolled composite. */
	protected ScrolledComposite baseScrolledComposite;

	/** The send btn. */
	protected Button sendBtn;

	/** The cancel btn. */
	protected Button cancelBtn;

	/** The site filer P group. */
	protected PGroup siteFilerPGroup;

	/** The admin area filer P group. */
	protected PGroup adminAreaFilerPGroup;

	/** The project filer P group. */
	protected PGroup projectFilerPGroup;

	/** The site filter text. */
	protected Text siteFilterText;

	/** The site filter button. */
	protected Button siteFilterButton;

	/** The site filter combo. */
	protected MagnaCustomCombo siteFilterCombo;

	/** The admin area filter text. */
	protected Text adminAreaFilterText;

	/** The admin area filter button. */
	protected Button adminAreaFilterButton;

	/** The admin area filter combo. */
	protected MagnaCustomCombo adminAreaFilterCombo;

	/** The project filter text. */
	protected Text projectFilterText;

	/** The project filter button. */
	protected Button projectFilterButton;

	/** The project filter combo. */
	protected MagnaCustomCombo projectFilterCombo;

	/** The filters container. */
	protected Composite filtersContainer;

	/** The user grp filter container. */
	Composite userGrpFilterContainer;

	/**
	 * Instantiates a new notify AA project user composite UI.
	 *
	 * @param parent
	 *            the parent
	 * @param style
	 *            the style
	 */
	public NotifyAAProjectUserCompositeUI(final Composite parent, final int style) {
		super(parent, style);
		this.resourceManager = new LocalResourceManager(JFaceResources.getResources(), this);
		this.initGUI();
	}

	/**
	 * Init GUI.
	 */
	private void initGUI() {
		try {
			GridLayoutFactory.fillDefaults().applyTo(this);
			this.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

			this.grpNotifyAAProUserEvt = new Group(this, SWT.NONE);
			this.grpNotifyAAProUserEvt.setBackgroundMode(SWT.INHERIT_FORCE);
			GridLayoutFactory.fillDefaults().numColumns(1).applyTo(this.grpNotifyAAProUserEvt);
			GridDataFactory.fillDefaults().grab(true, true).span(SWT.FILL, SWT.FILL)
					.applyTo(this.grpNotifyAAProUserEvt);
			baseScrolledComposite = XMAdminUtil.getInstance().createScrolledComposite(this.grpNotifyAAProUserEvt);
			baseScrolledComposite.setBackgroundMode(SWT.INHERIT_FORCE);

			final Composite widgetContainer = new Composite(baseScrolledComposite, SWT.NONE);
			final GridLayout widgetContLayout = new GridLayout(3, false);

			widgetContainer.setLayout(widgetContLayout);
			widgetContainer.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

			this.lblDescription = new Label(widgetContainer, SWT.NONE);
			this.lblDescription.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false, false));

			this.txtDescription = new StyledText(widgetContainer,
					SWT.BORDER | SWT.WRAP | SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL);
			GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true);
			gridData.minimumHeight = 10;
			gridData.horizontalSpan = 2;
			this.txtDescription.setLayoutData(gridData);
			this.txtDescription.setEditable(false);
			this.txtDescription.setTextLimit(NotifyAAProjectUserEvt.DESC_LIMIT);

			Label emptyLbl_radionBtn = new Label(widgetContainer, SWT.NONE);
			gridData = new GridData();
			emptyLbl_radionBtn.setLayoutData(gridData);

			this.radioBtnContainer = new Composite(widgetContainer, SWT.NONE);
			GridLayoutFactory.fillDefaults().numColumns(3).equalWidth(false).extendedMargins(0, 0, 0, 0)
					.applyTo(this.radioBtnContainer);
			GridDataFactory.fillDefaults().grab(true, false).span(2, 1).align(SWT.FILL, SWT.CENTER)
					.applyTo(this.radioBtnContainer);

			this.radioBtnUser = new Button(this.radioBtnContainer, SWT.RADIO);
			GridDataFactory.fillDefaults().grab(true, false).span(1, 1).align(SWT.FILL, SWT.CENTER)
					.applyTo(this.radioBtnUser);
			this.radioBtnUser.setSelection(true);

			this.radioBtnUserGroup = new Button(this.radioBtnContainer, SWT.RADIO);
			GridDataFactory.fillDefaults().grab(true, false).span(1, 1).align(SWT.FILL, SWT.CENTER)
					.applyTo(this.radioBtnUserGroup);

			Label emptyLbl_radionBtn2 = new Label(widgetContainer, SWT.NONE);
			gridData = new GridData();
			emptyLbl_radionBtn2.setLayoutData(gridData);

			this.baseFilterContainer = new Composite(widgetContainer, SWT.NONE);
			final GridLayout baseFilterCompLayout = new GridLayout(3, false);
			this.baseFilterContainer.setLayout(baseFilterCompLayout);
			GridDataFactory.fillDefaults().grab(true, false).span(2, 1).align(SWT.FILL, SWT.FILL)
					.applyTo(this.baseFilterContainer);

			this.stackContainer = new Composite(baseFilterContainer, SWT.NONE);
			this.stackLayout = new StackLayout();
			this.stackContainer.setLayout(this.stackLayout);
			GridDataFactory.fillDefaults().grab(true, false).span(2, 1).align(SWT.FILL, SWT.CENTER)
					.applyTo(this.stackContainer);

			// ScrolledComposite scrolledComposite =
			// XMAdminUtil.getInstance().createScrolledComposite(widgetContainer);

			this.filtersContainer = new Composite(stackContainer, SWT.NONE);
			GridLayoutFactory.fillDefaults().numColumns(2).equalWidth(false).extendedMargins(0, 0, 0, 0)
					.applyTo(filtersContainer);
			GridDataFactory.fillDefaults().grab(true, false).span(2, 1).align(SWT.FILL, SWT.FILL)
					.applyTo(filtersContainer);

			this.siteFilerPGroup = new PGroup(filtersContainer, SWT.NONE);
			this.siteFilerPGroup.setLayout(new GridLayout());
			GridDataFactory.fillDefaults().grab(true, false).span(1, 1).align(SWT.FILL, SWT.CENTER)
					.applyTo(this.siteFilerPGroup);
			this.siteFilerPGroup.addExpandListener(new ExpandListener() {

				@Override
				public void itemExpanded(final ExpandEvent event) {
					pGroupRepaint((PGroup) event.widget);
				}

				@Override
				public void itemCollapsed(final ExpandEvent event) {
					pGroupRepaint((PGroup) event.widget);
				}
			});

			this.adminAreaFilerPGroup = new PGroup(filtersContainer, SWT.NONE);
			this.adminAreaFilerPGroup.setLayout(new GridLayout());
			GridDataFactory.fillDefaults().grab(true, false).span(1, 1).align(SWT.FILL, SWT.CENTER)
					.applyTo(this.adminAreaFilerPGroup);
			this.adminAreaFilerPGroup.addExpandListener(new ExpandListener() {

				@Override
				public void itemExpanded(final ExpandEvent event) {
					pGroupRepaint((PGroup) event.widget);
				}

				@Override
				public void itemCollapsed(final ExpandEvent event) {
					pGroupRepaint((PGroup) event.widget);
				}
			});

			this.projectFilerPGroup = new PGroup(filtersContainer, SWT.NONE);
			this.projectFilerPGroup.setLayout(new GridLayout());
			GridDataFactory.fillDefaults().grab(true, false).span(1, 1).align(SWT.FILL, SWT.CENTER)
					.applyTo(this.projectFilerPGroup);
			this.projectFilerPGroup.addExpandListener(new ExpandListener() {

				@Override
				public void itemExpanded(final ExpandEvent event) {
					pGroupRepaint((PGroup) event.widget);
				}

				@Override
				public void itemCollapsed(final ExpandEvent event) {
					pGroupRepaint((PGroup) event.widget);
				}
			});

			this.userFilerPGroup = new PGroup(filtersContainer, SWT.NONE);
			userFilerPGroup.setLayout(new GridLayout());
			GridDataFactory.fillDefaults().grab(true, false).span(1, 1).align(SWT.FILL, SWT.CENTER)
					.applyTo(this.userFilerPGroup);
			userFilerPGroup.addExpandListener(new ExpandListener() {

				@Override
				public void itemExpanded(final ExpandEvent event) {
					pGroupRepaint((PGroup) event.widget);
				}

				@Override
				public void itemCollapsed(final ExpandEvent event) {
					pGroupRepaint((PGroup) event.widget);
				}
			});

			this.userGrpFilterContainer = new Composite(stackContainer, SWT.NONE);
			final GridLayout userGrpFilterCompLayout = new GridLayout(2, false);
			this.userGrpFilterContainer.setLayout(userGrpFilterCompLayout);
			GridDataFactory.fillDefaults().grab(true, false).span(2, 1).align(SWT.FILL, SWT.FILL)
					.applyTo(this.userGrpFilterContainer);

			userGroupPGroup = new PGroup(userGrpFilterContainer, SWT.SMOOTH);
			userGroupPGroup.setLayout(new GridLayout());
			GridDataFactory.fillDefaults().grab(true, false).span(1, 1).align(SWT.FILL, SWT.FILL)
					.applyTo(this.userGroupPGroup);
			userGroupPGroup.addExpandListener(new ExpandListener() {
				@Override
				public void itemExpanded(final ExpandEvent event) {
					pGroupRepaint((PGroup) event.widget);
				}

				@Override
				public void itemCollapsed(final ExpandEvent event) {
					pGroupRepaint((PGroup) event.widget);
				}
			});
			final GridData filterButtonsGridData = new GridData();
			filterButtonsGridData.widthHint = 30;
			filterButtonsGridData.heightHint = 30;

			ScrolledComposite siteFilterScrolledComposite = XMAdminUtil.getInstance()
					.createScrolledComposite(siteFilerPGroup);
			final Composite siteFilerPanelContainer = new Composite(siteFilterScrolledComposite, SWT.NONE);
			siteFilerPanelContainer.setLayout(new GridLayout(4, false));
			siteFilerPanelContainer.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

			this.siteFilterText = new Text(siteFilerPanelContainer, SWT.BORDER);
			GridDataFactory.fillDefaults().grab(true, false).span(1, 1).minSize(100, SWT.DEFAULT)
					.align(SWT.FILL, SWT.CENTER).applyTo(this.siteFilterText);
			this.siteFilterButton = new Button(siteFilerPanelContainer, SWT.PUSH);
			this.siteFilterButton.setLayoutData(filterButtonsGridData);

			this.siteFilterCombo = new MagnaCustomCombo(siteFilerPanelContainer, SWT.BORDER | SWT.READ_ONLY);
			GridDataFactory.fillDefaults().grab(true, false).span(2, 1).minSize(120, SWT.DEFAULT)
					.align(SWT.FILL, SWT.CENTER).applyTo(this.siteFilterCombo);

			ScrolledComposite adminAreaFilterScrolledComposite = XMAdminUtil.getInstance()
					.createScrolledComposite(adminAreaFilerPGroup);
			final Composite adminAreaFilerPanelContainer = new Composite(adminAreaFilterScrolledComposite, SWT.NONE);
			adminAreaFilerPanelContainer.setLayout(new GridLayout(4, false));
			adminAreaFilerPanelContainer.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

			this.adminAreaFilterText = new Text(adminAreaFilerPanelContainer, SWT.BORDER);
			GridDataFactory.fillDefaults().grab(true, false).span(1, 1).minSize(100, SWT.DEFAULT)
					.align(SWT.FILL, SWT.CENTER).applyTo(this.adminAreaFilterText);
			this.adminAreaFilterButton = new Button(adminAreaFilerPanelContainer, SWT.PUSH);
			this.adminAreaFilterButton.setLayoutData(filterButtonsGridData);
			this.adminAreaFilterCombo = new MagnaCustomCombo(adminAreaFilerPanelContainer, SWT.BORDER | SWT.READ_ONLY);
			GridDataFactory.fillDefaults().grab(true, false).span(2, 1).align(SWT.FILL, SWT.CENTER)
					.minSize(120, SWT.DEFAULT).applyTo(this.adminAreaFilterCombo);

			ScrolledComposite projectFilterScrolledComposite = XMAdminUtil.getInstance()
					.createScrolledComposite(projectFilerPGroup);
			final Composite projectFilerPanelContainer = new Composite(projectFilterScrolledComposite, SWT.NONE);
			projectFilerPanelContainer.setLayout(new GridLayout(4, false));
			projectFilerPanelContainer.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

			this.projectFilterText = new Text(projectFilerPanelContainer, SWT.BORDER);
			GridDataFactory.fillDefaults().grab(true, false).span(1, 1).align(SWT.FILL, SWT.CENTER)
					.minSize(100, SWT.DEFAULT).applyTo(this.projectFilterText);
			this.projectFilterButton = new Button(projectFilerPanelContainer, SWT.PUSH);
			this.projectFilterButton.setLayoutData(filterButtonsGridData);
			this.projectFilterCombo = new MagnaCustomCombo(projectFilerPanelContainer, SWT.BORDER | SWT.READ_ONLY);
			GridDataFactory.fillDefaults().grab(true, false).span(2, 1).minSize(120, SWT.DEFAULT)
					.align(SWT.FILL, SWT.CENTER).applyTo(this.projectFilterCombo);

			ScrolledComposite userFilterScrolledComposite = XMAdminUtil.getInstance()
					.createScrolledComposite(userFilerPGroup);
			final Composite userFilerPanelContainer = new Composite(userFilterScrolledComposite, SWT.NONE);
			userFilerPanelContainer.setLayout(new GridLayout(4, false));
			userFilerPanelContainer.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

			this.userFilterText = new Text(userFilerPanelContainer, SWT.BORDER);
			GridDataFactory.fillDefaults().grab(true, false).span(1, 1).align(SWT.FILL, SWT.CENTER)
					.applyTo(this.userFilterText);
			this.userFilterButton = new Button(userFilerPanelContainer, SWT.PUSH);
			this.userFilterButton.setLayoutData(filterButtonsGridData);
			this.userFilterCombo = new MagnaCustomCombo(userFilerPanelContainer, SWT.BORDER | SWT.READ_ONLY);
			GridDataFactory.fillDefaults().grab(true, false).span(2, 1).minSize(120, SWT.DEFAULT)
					.align(SWT.FILL, SWT.CENTER).applyTo(this.userFilterCombo);

			ScrolledComposite userGrpFilterScrolledComposite = XMAdminUtil.getInstance()
					.createScrolledComposite(userGroupPGroup);
			final Composite userGroupFilerPanelContainer = new Composite(userGrpFilterScrolledComposite, SWT.NONE);
			userGroupFilerPanelContainer.setLayout(new GridLayout(4, false));
			userGroupFilerPanelContainer.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

			this.userGroupFilterText = new Text(userGroupFilerPanelContainer, SWT.BORDER);
			GridDataFactory.fillDefaults().grab(true, false).span(1, 1).align(SWT.FILL, SWT.CENTER)
					.minSize(100, SWT.DEFAULT).applyTo(this.userGroupFilterText);
			this.userGroupFilterButton = new Button(userGroupFilerPanelContainer, SWT.PUSH);
			this.userGroupFilterButton.setLayoutData(filterButtonsGridData);
			this.userGroupFilterCombo = new MagnaCustomCombo(userGroupFilerPanelContainer, SWT.BORDER | SWT.READ_ONLY);
			GridDataFactory.fillDefaults().grab(true, false).span(2, 1).align(SWT.FILL, SWT.CENTER)
					.minSize(150, SWT.DEFAULT).applyTo(this.userGroupFilterCombo);

			this.stackLayout.topControl = this.filtersContainer;
			final Composite addBtnContainer = new Composite(baseFilterContainer, SWT.NONE);
			final GridLayout addBtnCompLayout = new GridLayout(1, false);
			addBtnCompLayout.marginRight = 0;
			addBtnCompLayout.marginLeft = 0;
			addBtnCompLayout.marginTop = 0;
			addBtnCompLayout.marginBottom = 0;
			addBtnCompLayout.marginWidth = 0;
			addBtnContainer.setLayout(addBtnCompLayout);
			addBtnContainer.setLayoutData(new GridData(SWT.FILL, SWT.DEFAULT, false, false, 1, 1));

			this.addtoToBtn = new Button(addBtnContainer, SWT.NONE);
			this.addtoToBtn.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
			this.addtoCCBtn = new Button(addBtnContainer, SWT.NONE);
			this.addtoCCBtn.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));

			this.lblToUsers = new Label(widgetContainer, SWT.NONE);
			GridDataFactory.fillDefaults().grab(false, false).span(1, 1).align(SWT.LEFT, SWT.CENTER)
					.applyTo(this.lblToUsers);

			this.txtToUsers = new StyledText(widgetContainer,
					SWT.BORDER | SWT.WRAP | SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL);
			gridData = new GridData(SWT.FILL, SWT.FILL, true, true);
			gridData.minimumHeight = 30;
			gridData.horizontalSpan = 2;
			this.txtToUsers.setLayoutData(gridData);

			this.lblCCUsers = new Label(widgetContainer, SWT.NONE);
			GridDataFactory.fillDefaults().grab(false, false).span(1, 1).align(SWT.LEFT, SWT.CENTER)
					.applyTo(this.lblCCUsers);

			this.txtCCUsers = new StyledText(widgetContainer,
					SWT.BORDER | SWT.WRAP | SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL);
			gridData = new GridData(SWT.FILL, SWT.FILL, true, true);
			gridData.minimumHeight = 30;
			gridData.horizontalSpan = 2;
			this.txtCCUsers.setLayoutData(gridData);

			userGrpFilterScrolledComposite.setContent(userGroupFilerPanelContainer);
			userGrpFilterScrolledComposite.setSize(userGroupFilerPanelContainer.getSize());
			userGrpFilterScrolledComposite.setExpandVertical(true);
			userGrpFilterScrolledComposite.setExpandHorizontal(true);
			userGrpFilterScrolledComposite.update();

			userGrpFilterScrolledComposite.addControlListener(new ControlAdapter() {
				public void controlResized(final ControlEvent e) {
					// Rectangle rectangle =
					// filterPanelContainer.getClientArea();
					userGrpFilterScrolledComposite
							.setMinSize(userGroupFilerPanelContainer.computeSize(SWT.DEFAULT, SWT.DEFAULT));
				}
			});

			userFilterScrolledComposite.setContent(userFilerPanelContainer);
			userFilterScrolledComposite.setSize(userFilerPanelContainer.getSize());
			userFilterScrolledComposite.setExpandVertical(true);
			userFilterScrolledComposite.setExpandHorizontal(true);
			userFilterScrolledComposite.update();

			userFilterScrolledComposite.addControlListener(new ControlAdapter() {
				public void controlResized(final ControlEvent e) {
					userFilterScrolledComposite
							.setMinSize(userFilerPanelContainer.computeSize(SWT.DEFAULT, SWT.DEFAULT));
				}
			});

			projectFilterScrolledComposite.setContent(projectFilerPanelContainer);
			projectFilterScrolledComposite.setSize(projectFilerPanelContainer.getSize());
			projectFilterScrolledComposite.setExpandVertical(true);
			projectFilterScrolledComposite.setExpandHorizontal(true);
			projectFilterScrolledComposite.update();

			projectFilterScrolledComposite.addControlListener(new ControlAdapter() {
				public void controlResized(final ControlEvent e) {
					projectFilterScrolledComposite
							.setMinSize(projectFilerPanelContainer.computeSize(SWT.DEFAULT, SWT.DEFAULT));
				}
			});

			siteFilterScrolledComposite.setContent(siteFilerPanelContainer);
			siteFilterScrolledComposite.setSize(siteFilerPanelContainer.getSize());
			siteFilterScrolledComposite.setExpandVertical(true);
			siteFilterScrolledComposite.setExpandHorizontal(true);
			siteFilterScrolledComposite.update();

			siteFilterScrolledComposite.addControlListener(new ControlAdapter() {
				public void controlResized(final ControlEvent e) {
					siteFilterScrolledComposite
							.setMinSize(siteFilerPanelContainer.computeSize(SWT.DEFAULT, SWT.DEFAULT));
				}
			});

			adminAreaFilterScrolledComposite.setContent(adminAreaFilerPanelContainer);
			adminAreaFilterScrolledComposite.setSize(adminAreaFilerPanelContainer.getSize());
			adminAreaFilterScrolledComposite.setExpandVertical(true);
			adminAreaFilterScrolledComposite.setExpandHorizontal(true);
			adminAreaFilterScrolledComposite.update();

			adminAreaFilterScrolledComposite.addControlListener(new ControlAdapter() {
				public void controlResized(final ControlEvent e) {
					adminAreaFilterScrolledComposite
							.setMinSize(adminAreaFilerPanelContainer.computeSize(SWT.DEFAULT, SWT.DEFAULT));
				}
			});

			baseScrolledComposite.setContent(widgetContainer);
			baseScrolledComposite.setSize(widgetContainer.getSize());
			baseScrolledComposite.setExpandVertical(true);
			baseScrolledComposite.setExpandHorizontal(true);
			baseScrolledComposite.update();

			baseScrolledComposite.addControlListener(new ControlAdapter() {
				public void controlResized(final ControlEvent e) {
					Rectangle rectangle = baseScrolledComposite.getClientArea();
					baseScrolledComposite.setMinSize(widgetContainer.computeSize(rectangle.width, SWT.DEFAULT));
				}
			});

			this.lblSubject = new Label(widgetContainer, SWT.NONE);
			GridDataFactory.fillDefaults().grab(false, false).span(1, 1).align(SWT.LEFT, SWT.CENTER)
					.applyTo(this.lblSubject);

			this.txtSubject = new Text(widgetContainer, SWT.BORDER);
			this.txtSubject.setTextLimit(NotifyAAProjectUserEvt.SUB_LIMIT);
			GridDataFactory.fillDefaults().grab(true, false).span(2, 1).align(SWT.FILL, SWT.CENTER)
					.applyTo(this.txtSubject);

			this.lblMessage = new Label(widgetContainer, SWT.NONE);
			GridDataFactory.fillDefaults().grab(false, false).span(1, 1).align(SWT.LEFT, SWT.TOP)
					.applyTo(this.lblMessage);

			this.txtMessage = new Text(widgetContainer,
					SWT.BORDER | SWT.WRAP | SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL);
			gridData = new GridData(SWT.FILL, SWT.FILL, true, true);
			gridData.minimumHeight = 150;
			gridData.horizontalSpan = 2;
			this.txtMessage.setLayoutData(gridData);
			this.txtMessage.setTextLimit(NotifyAAProjectUserEvt.MESSAGE_LIMIT);

			final Composite buttonBarComp = new Composite(this.grpNotifyAAProUserEvt, SWT.NONE);
			final GridLayout btnBarCompLayout = new GridLayout(2, true);
			btnBarCompLayout.marginRight = 0;
			btnBarCompLayout.marginLeft = 0;
			btnBarCompLayout.marginTop = 0;
			btnBarCompLayout.marginBottom = 0;
			btnBarCompLayout.marginWidth = 0;
			buttonBarComp.setLayout(btnBarCompLayout);
			buttonBarComp.setLayoutData(new GridData(SWT.FILL, SWT.DEFAULT, true, false, 3, 1));
			createButtonBar(buttonBarComp);

		} catch (Exception e) {
			LOGGER.error("Unable to crete UI elements", e); //$NON-NLS-1$
		}

	}

	/**
	 * Create button bar.
	 *
	 * @param buttonBarComp
	 *            the button bar comp
	 */
	private void createButtonBar(final Composite buttonBarComp) {
		this.sendBtn = new Button(buttonBarComp, SWT.NONE);
		this.sendBtn.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, true, false));

		this.cancelBtn = new Button(buttonBarComp, SWT.NONE);
		this.cancelBtn.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false));
	}

	/**
	 * P group repaint.
	 *
	 * @param pGroup
	 *            the group
	 */
	protected void pGroupRepaint(final PGroup pGroup) {
		pGroup.requestLayout();
		pGroup.redraw();
		pGroup.getParent().update();
		baseScrolledComposite.notifyListeners(SWT.Resize, new Event());
	}

	/**
	 * Sets the show button bar.
	 *
	 * @param showButtonBar
	 *            the new show button bar
	 */
	protected void setShowButtonBar(final boolean showButtonBar) {
		if (this.sendBtn != null && !this.sendBtn.isDisposed() && this.cancelBtn != null
				&& !this.cancelBtn.isDisposed()) {
			final GridData layoutData = (GridData) this.sendBtn.getParent().getLayoutData();
			layoutData.exclude = !showButtonBar;
			this.sendBtn.setVisible(showButtonBar);
			this.cancelBtn.setVisible(showButtonBar);
			this.sendBtn.getParent().setVisible(showButtonBar);
			this.sendBtn.getParent().requestLayout();
			this.sendBtn.getParent().redraw();
			this.sendBtn.getParent().getParent().update();
		}
	}

}
