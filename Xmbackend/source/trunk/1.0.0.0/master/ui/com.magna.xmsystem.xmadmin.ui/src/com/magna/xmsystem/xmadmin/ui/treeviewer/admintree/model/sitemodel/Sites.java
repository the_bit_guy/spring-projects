package com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.sitemodel;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild;

/**
 * Container model class for {@link Site}
 * 
 * @author shashwat.anand
 *
 */
public class Sites implements IAdminTreeChild {
	
	/** The parent. */
	private IAdminTreeChild parent;
	/**
	 * List for storing {@link Site}
	 */
	private Map<String, IAdminTreeChild> sitesChildren;

	/**
	 * Constructor
	 */
	public Sites() {
		this.parent = null;
		this.sitesChildren = new LinkedHashMap<>();
	}

	/**
	 * Adds the child
	 * @param child {@link IAdminTreeChild}
	 * @param sideId {@link String}
	 * @return {@link IAdminTreeChild}
	 */
	public IAdminTreeChild add(final String sideId, final IAdminTreeChild child) {
		child.setParent(this);
		IAdminTreeChild returnVal = this.sitesChildren.put(sideId, child);
		sort();
		return returnVal;
	}

	/**
	 * Removes the child
	 * @param sideId {@link String}
	 * @return {@link IAdminTreeChild}
	 */
	public IAdminTreeChild remove(final String sideId) {
		return this.sitesChildren.remove(sideId);
	}

	/**
	 * Method for Removes the all.
	 */
	public void removeAll() {
		this.sitesChildren.clear();
	}
	
	/**
	 * @return {@link List} of {@link Site} 
	 */
	public Collection<IAdminTreeChild> getSitesCollection() {
		return this.sitesChildren.values();
	}

	/**
	 * @return the sitesChildren
	 */
	public Map<String, IAdminTreeChild> getSitesChildren() {
		return sitesChildren;
	}

	/**
	 * Returns null as its parent
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#getParent()
	 */
	@Override
	public IAdminTreeChild getParent() {
		return this.parent;
	}

	/**
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild#setParent(com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.IAdminTreeChild)
	 */
	@Override
	public void setParent(final IAdminTreeChild parent) {
		this.parent = parent;
	}
	
	/**
	 * Sort.
	 */
	public void sort() {
		final LinkedHashMap<String, IAdminTreeChild> collect = this.sitesChildren.entrySet().stream().sorted(
				(e1, e2) -> ((Site) e1.getValue()).getName().compareTo(((Site) e2.getValue()).getName()))
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
		this.sitesChildren = collect;
	}
}
