package com.magna.xmsystem.ui.controls.widgets;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Date;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Path;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.jface.resource.LocalResourceManager;
import org.eclipse.jface.resource.ResourceManager;
import org.eclipse.nebula.widgets.cdatetime.CDT;
import org.eclipse.nebula.widgets.cdatetime.CDateTime;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.ShellAdapter;
import org.eclipse.swt.events.ShellEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.DateTime;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;
import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;

import com.magna.xmbackend.vo.enums.IDateTimeFormatConst;
import com.magna.xmsystem.dependencies.utils.LANG_ENUM;
import com.magna.xmsystem.dependencies.utils.XMSystemUtil;

/**
 * Class for Custom date time.
 *
 * @author Chiranjeevi.Akula
 */
public class CustomDateTime extends Composite {

	/** Member variable 'date time image path' for {@link String}. */
	final private String dateTimeImagePath = "icons/16x16/datetime_16.png";

	/** Member variable 'ok image path' for {@link String}. */
	final private String okImagePath = "icons/16x16/ok_16.png";

	/** Member variable 'cancel image path' for {@link String}. */
	final private String cancelImagePath = "icons/16x16/cancel_16.png";

	/** Member variable 'clear image path' for {@link String}. */
	final private String clearImagePath = "icons/16x16/clear_16.png";

	/** Member variable 'resource manager' for {@link ResourceManager}. */
	final private ResourceManager resourceManager;

	/** Member variable 'date time image' for {@link Image}. */
	final private Image dateTimeImage;

	/** Member variable 'ok image' for {@link Image}. */
	final private Image okImage;

	/** Member variable 'cancel image' for {@link Image}. */
	final private Image cancelImage;

	/** Member variable 'clear image' for {@link Image}. */
	final private Image clearImage;

	/** Member variable 'popup' for {@link Shell}. */
	private Shell popup;

	/** Member variable 'date picker item' for {@link ToolItem}. */
	private ToolItem datePickerItem;

	/** Member variable 'today btn' for {@link Button}. */
	private Button todayBtn;

	/** Member variable 'okbtn' for {@link ToolItem}. */
	private ToolItem okbtn;

	/** Member variable 'cancelbtn' for {@link ToolItem}. */
	private ToolItem cancelbtn;

	/** Member variable 'clearbtn' for {@link ToolItem}. */
	private ToolItem clearbtn;

	/** Member variable 'date picker' for {@link DateTime}. */
	private DateTime datePicker;

	/** Member variable 'time picker' for {@link DateTime}. */
	private DateTime timePicker;

	/** Member variable 'c date time' for {@link CDateTime}. */
	private CDateTime cDateTime;
	
	/** The Constant CANCEL_LABEL_EN. */
	final static private String CANCEL_LABEL_EN = "Cancel";
	
	/** The Constant CANCEL_LABEL_DE. */
	final static private String CANCEL_LABEL_DE = "Abbrechen";
	
	/** The Constant CHOOSE_DATE_LABEL_EN. */
	final static private String CHOOSE_DATE_LABEL_EN = "choose date";
	
	/** The Constant CHOOSE_DATE_LABEL_DE. */
	final static private String CHOOSE_DATE_LABEL_DE = "Datum wählen";
	
	/** The Constant CLEAR_LABEL_EN. */
	final static private String CLEAR_LABEL_EN = "Clear";
	
	/** The Constant CLEAR_LABEL_DE. */
	final static private String CLEAR_LABEL_DE = "Klar";
	
	/** The Constant TODAY_LABEL_EN. */
	final static private String NOW_LABEL_EN = "Now";
	
	/** The Constant TODAY_LABEL_DE. */
	final static private String NOW_LABEL_DE = "jetzt";
	

	/**
	 * Constructor for CustomDateTime Class.
	 *
	 * @param parent
	 *            {@link Composite}
	 */
	public CustomDateTime(final Composite parent) {
		this(parent, SWT.NONE);
	}

	/**
	 * Constructor for CustomDateTime Class.
	 *
	 * @param parent
	 *            {@link Composite}
	 * @param style
	 *            {@link int}
	 */
	public CustomDateTime(final Composite parent, final int style) {
		super(parent, style | SWT.BORDER);
		this.resourceManager = new LocalResourceManager(JFaceResources.getResources());
		this.dateTimeImage = getSavedImage(dateTimeImagePath);
		this.okImage = getSavedImage(okImagePath);
		this.cancelImage = getSavedImage(cancelImagePath);
		this.clearImage = getSavedImage(clearImagePath);

		init(parent);
		initListeners();
	}

	/**
	 * Method for Inits the.
	 *
	 * @param parent
	 *            {@link Composite}
	 */
	private void init(final Composite parent) {
		final GridLayout gridLayout = new GridLayout(2, false);
		gridLayout.marginHeight = 0;
		gridLayout.marginWidth = 0;
		gridLayout.horizontalSpacing = 0;
		gridLayout.verticalSpacing = 0;

		this.setLayout(gridLayout);

		cDateTime = new CDateTime(this,
				CDT.BORDER | CDT.DATE_SHORT | CDT.TIME_SHORT | CDT.COMPACT | CDT.TAB_FIELDS | CDT.CLOCK_DISCRETE);
		cDateTime.setPattern(IDateTimeFormatConst.UI_DATE_TIME_FORMAT);
		cDateTime.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, false));

		final ToolBar buttonToolBar = new ToolBar(this, SWT.FLAT);
		buttonToolBar.setLayoutData(new GridData(GridData.END, GridData.FILL, false, false));

		datePickerItem = new ToolItem(buttonToolBar, SWT.PUSH);
		datePickerItem.setImage(dateTimeImage);
		initDatePopUp();
	}

	/**
	 * Method for Inits the listeners.
	 */
	private void initListeners() {
		popup.addShellListener(new ShellAdapter() {

			@Override
			public void shellDeactivated(ShellEvent arg0) {
				popup.setVisible(false);
			}
		});

		datePickerItem.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent selectionevent) {
				if (!popup.isVisible()) {
					openPopup();
				} else {
					popup.setVisible(false);
				}
			}
		});

		todayBtn.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent selectionevent) {
				LocalDateTime now = LocalDateTime.now();
				datePicker.setDate(now.getYear(), now.getMonthValue() - 1, now.getDayOfMonth());
				timePicker.setTime(now.getHour(), now.getMinute(), now.getSecond());
			}
		});

		okbtn.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent selectionevent) {
				Calendar calInstance = Calendar.getInstance();
				calInstance.set(datePicker.getYear(), datePicker.getMonth(), datePicker.getDay(), timePicker.getHours(),
						timePicker.getMinutes(), timePicker.getSeconds());

				cDateTime.setSelection(calInstance.getTime());
				popup.setVisible(false);
				cDateTime.forceFocus();
			}
		});

		cancelbtn.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent selectionevent) {
				popup.setVisible(false);
				cDateTime.forceFocus();
			}
		});

		clearbtn.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent selectionevent) {
				cDateTime.setSelection(null);
				popup.setVisible(false);
				cDateTime.forceFocus();
			}
		});
	}

	/**
	 * Method for Inits the date pop up.
	 */
	private void initDatePopUp() {
		popup = new Shell(getShell(), SWT.ON_TOP | SWT.BORDER);
		final GridLayout gridLayout = new GridLayout();
		gridLayout.marginHeight = 0;
		gridLayout.marginWidth = 0;
		gridLayout.horizontalSpacing = 0;
		gridLayout.verticalSpacing = 0;
		popup.setLayout(gridLayout);
		popup.setBackgroundMode(SWT.INHERIT_FORCE);

		datePicker = new DateTime(popup, SWT.CALENDAR | SWT.BORDER);
		datePicker.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, false));
		
		timePicker = new DateTime(popup, SWT.TIME | SWT.SHORT | SWT.BORDER);
		timePicker.setTime(0, 0, 0);
		timePicker.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, false));

		final Composite buttonBar = new Composite(popup, SWT.NONE);
		final GridLayout gridLayout2 = new GridLayout(4, false);
		gridLayout2.marginHeight = 0;
		gridLayout2.marginWidth = 0;
		gridLayout2.marginLeft = 2;
		gridLayout2.marginRight = 2;
		gridLayout2.marginBottom = 2;
		gridLayout2.marginTop = 2;
		gridLayout2.horizontalSpacing = 0;
		gridLayout2.verticalSpacing = 0;
		buttonBar.setLayout(gridLayout2);
		buttonBar.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, false));
		buttonBar.setBackgroundMode(SWT.INHERIT_FORCE);

		todayBtn = new Button(buttonBar, SWT.PUSH);
		todayBtn.setLayoutData(new GridData(GridData.BEGINNING, GridData.CENTER, false, false));
		//todayBtn.setText(" Today ");

		final ToolBar buttonToolBar = new ToolBar(buttonBar, SWT.FLAT);
		buttonToolBar.setLayoutData(new GridData(GridData.END, GridData.CENTER, true, false));

		okbtn = new ToolItem(buttonToolBar, SWT.PUSH);
		okbtn.setImage(okImage);
		okbtn.setToolTipText(" Ok ");

		cancelbtn = new ToolItem(buttonToolBar, SWT.PUSH);
		cancelbtn.setImage(cancelImage);
		//cancelbtn.setToolTipText(" Cancel ");

		clearbtn = new ToolItem(buttonToolBar, SWT.PUSH);
		clearbtn.setImage(clearImage);
		
		final String localeString = XMSystemUtil.getLanguage();
		updateTooltipText(localeString);
		
		
		//clearbtn.setToolTipText(" Clear ");
		
	/*	LANG_ENUM currentLang = LANG_ENUM.ENGLISH;
		final String localeString = XMSystemUtil.getLanguage();
		if(!XMSystemUtil.isEmpty(localeString)) {
			currentLang = LANG_ENUM.valueOf(localeString.toUpperCase());
		}*/
		//LANG_ENUM currentLang = LANG_ENUM.ENGLISH;
		
		/*if (currentLang == LANG_ENUM.GERMAN) {
		datePickerItem.setToolTipText(CHOOSE_DATE_LABEL_DE);
		clearbtn.setToolTipText(CLEAR_LABEL_DE);
		todayBtn.setText(TODAY_LABEL_DE);
		cancelbtn.setToolTipText(CANCEL_LABEL_DE);
		}else{
			datePickerItem.setToolTipText(CHOOSE_DATE_LABEL_EN);
			clearbtn.setToolTipText(CLEAR_LABEL_EN);
			todayBtn.setText(TODAY_LABEL_EN);
			cancelbtn.setToolTipText(CANCEL_LABEL_EN);
		}*/
		popup.pack();
	}

	/**
	 * Method for Open popup.
	 */
	private void openPopup() {

		Date date = cDateTime.getSelection();

		if (date != null) {
			Calendar instance = Calendar.getInstance();
			instance.setTime(date);

			int year = instance.get(Calendar.YEAR);
			int month = instance.get(Calendar.MONTH);
			int day = instance.get(Calendar.DAY_OF_MONTH);
			int hours = instance.get(Calendar.HOUR_OF_DAY);
			int minutes = instance.get(Calendar.MINUTE);
			int seconds = instance.get(Calendar.SECOND);

			datePicker.setDate(year, month, day);
			timePicker.setTime(hours, minutes, seconds);
		} else {
			LocalDateTime now = LocalDateTime.now();
			datePicker.setDate(now.getYear(), now.getMonthValue() - 1, now.getDayOfMonth());
			timePicker.setTime(0, 0, 0);
		}

		Rectangle parentRect = getDisplay().map(getParent(), null, getBounds());

		Rectangle displayRect = getMonitor().getClientArea();

		Point popupSize = popup.getSize();
		int overallWidth = popupSize.x;
		int overallHeight = popupSize.y;

		int x = parentRect.x + cDateTime.getSize().x;
		int y = parentRect.y + cDateTime.getSize().y;
		if (y + overallHeight > displayRect.y + displayRect.height) {
			y = parentRect.y - overallHeight;
		}
		if (x + overallWidth > displayRect.x + displayRect.width) {
			x = displayRect.x + displayRect.width - popupSize.x /*- tableRect.width*/;
		}

		popup.setLocation(x, y);
		popup.setVisible(true);
		popup.forceFocus();
	}

	/**
	 * Gets the saved image.
	 *
	 * @param pathString
	 *            {@link String}
	 * @return the saved image
	 */
	private Image getSavedImage(final String pathString) {
		ImageRegistry imageRegistry = JFaceResources.getImageRegistry();
		Image image = imageRegistry.get(pathString);
		if (image == null) {
			final Bundle bundle = FrameworkUtil.getBundle(this.getClass());
			ImageDescriptor imageDescriptor = null;
			URL url = FileLocator.find(bundle, new Path(pathString), null);
			if (url == null) {
				try {
					url = (new File(pathString)).toURI().toURL();
				} catch (MalformedURLException e) {
					e.printStackTrace();
				}
			}
			imageDescriptor = ImageDescriptor.createFromURL(url);
			image = resourceManager.createImage(imageDescriptor);
			imageRegistry.put(pathString, image);
		}
		return image;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.swt.widgets.Control#setEnabled(boolean)
	 */
	@Override
	public void setEnabled(final boolean enabled) {
		super.setEnabled(enabled);
		cDateTime.setEnabled(enabled);
		datePickerItem.setEnabled(enabled);
	}

	/* (non-Javadoc)
	 * @see org.eclipse.swt.widgets.Widget#dispose()
	 */
	@Override
	public void dispose() {
		super.dispose();

		if (cDateTime != null && !cDateTime.isDisposed()) {
			cDateTime.dispose();
		}
		if (datePickerItem != null && !datePickerItem.isDisposed()) {
			datePickerItem.dispose();
		}
		if (datePicker != null && !datePicker.isDisposed()) {
			datePicker.dispose();
		}
		if (timePicker != null && !timePicker.isDisposed()) {
			timePicker.dispose();
		}
	}
	
	/**
	 * Sets the date.
	 *
	 * @param date
	 *            the new date
	 */
	public void setDate(final Date date) {
		cDateTime.setSelection(date);
	}

	/**
	 * Gets the date.
	 *
	 * @return the date
	 */
	public Date getDate() {
		return cDateTime.getSelection();
	}

	public void updateTooltipText(String currentLang) {
		if (this != null && !this.isDisposed()) {
			//LANG_ENUM currentLang = LANG_ENUM.ENGLISH;
			/*final String localeString = XMSystemUtil.getLanguage();
			if (!XMSystemUtil.isEmpty(localeString)) {
				currentLang = LANG_ENUM.valueOf(localeString.toUpperCase());
			}*/
			if (LANG_ENUM.GERMAN.toString().equalsIgnoreCase(currentLang)) {
				datePickerItem.setToolTipText(CHOOSE_DATE_LABEL_DE);
				clearbtn.setToolTipText(CLEAR_LABEL_DE);
				todayBtn.setText(NOW_LABEL_DE);
				cancelbtn.setToolTipText(CANCEL_LABEL_DE);
				cDateTime.setNullText("<"+CHOOSE_DATE_LABEL_DE+">");
			} else {
				datePickerItem.setToolTipText(CHOOSE_DATE_LABEL_EN);
				clearbtn.setToolTipText(CLEAR_LABEL_EN);
				todayBtn.setText(NOW_LABEL_EN);
				cancelbtn.setToolTipText(CANCEL_LABEL_EN);
				cDateTime.setNullText("<"+CHOOSE_DATE_LABEL_EN+">");
			} 
		}
	}

}
