package com.magna.xmsystem.ui.controls.widgets;

import org.eclipse.jface.dialogs.Dialog;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * class for CustomXMAdminDescriptionDialogArea create control.
 *
 * @author Deepak Upadhyay
 */

public class CustomTextFieldDialog extends Dialog {

	/** Logger Instance class. */
	private static final Logger LOGGER = LoggerFactory.getLogger(CustomTextFieldDialog.class);

	/** Instance for textDescription. */
	transient private Text textfield;

	/** Instance for labelDescription. */
	transient protected Label lbltextfield;

	/** Instance for lblCount. */
	protected Label lblCount;

	/** Instance for composite area. */
	protected Composite area;

	/** Member variable for dialog title. */
	transient private String title;

	/** Member variable for dialog title. */
	private boolean editableMode;

	/** Member variable to store the parent shell. */
	transient private Shell parentShell;

	/** Instance for label. */
	private String label;
	/**
	 * Instance of textFieldLimit.
	 */
	private int textFieldLimit;

	/** Instance for textfieldText. */
	private String textfieldText;

	/** Member variable to check OK button pressed. */
	private boolean okPressed;

	/**
	 * Constructor.
	 *
	 * @param shell
	 *            {@link Shell}
	 * @param title
	 *            {@link String}
	 * @param label
	 *            {@link String}
	 * @param editableMode
	 *            {@link boolean}
	 */
	public CustomTextFieldDialog(final Shell shell, final String title, final String label, final int textFieldLimit,
			final boolean editableMode) {
		super(shell);
		this.parentShell = shell;
		this.title = title;
		this.label = label;
		this.textFieldLimit = textFieldLimit;
		this.editableMode = editableMode;

	}

	/**
	 * Configures the Shell.
	 *
	 * @param newShell
	 *            {@link Shell}
	 * @see org.eclipse.jface.window.Window#configureShell(org.eclipse.swt.widgets.Shell)
	 */
	@Override
	protected void configureShell(final Shell newShell) {
		try {
			super.configureShell(newShell);
			newShell.setText(this.title);
			newShell.setParent(this.parentShell);
			final Rectangle shellBounds = parentShell.getBounds();
			final int width = 450;
			final int height = 300;
			newShell.setSize(width, height);
			final Point pos = new Point((shellBounds.width - width) / 2, (shellBounds.height - height) / 2);
			newShell.setLocation(pos.x, pos.y);
		} catch (Exception event) {
			LOGGER.error("Execution occurred while configuring shell!", event); //$NON-NLS-1$
		}
	}

	/**
	 * Sets the style of shell.
	 *
	 * @param newShellStyle
	 *            the new shell style
	 * @see org.eclipse.jface.window.Window#setShellStyle(int)
	 */
	@Override
	protected void setShellStyle(final int newShellStyle) {
		super.setShellStyle(newShellStyle | SWT.RESIZE);
	}

	/**
	 * method createDialogArea for create control dialog.
	 *
	 * @param parent
	 *            {@link Composite}
	 * @return the control {@link Control}
	 */
	@Override
	protected Control createDialogArea(final Composite parent) {
		try {
			parent.setData("org.eclipse.e4.ui.css.CssClassName", "DialogComposite");
			parent.setBackgroundMode(SWT.INHERIT_FORCE);
			final Composite area = (Composite) super.createDialogArea(parent);
			final GridLayout areaLayout = new GridLayout(1, false);
			area.setLayout(areaLayout);
			final Composite widgetContainer = new Composite(area, SWT.NONE);
			final GridLayout widgetContainerLayout = new GridLayout(1, false);
			widgetContainer.setLayout(widgetContainerLayout);
			widgetContainer.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
			createBottom(widgetContainer);
		} catch (Exception e) {
			LOGGER.error("Unable to create UI elements", e);
		}
		return area;
	}

	/**
	 * method for dialog binding.
	 */
	protected void bindDialog() {
		// TODO Auto-generated method stub
	}

	/**
	 * method for create bottom part.
	 *
	 * @param widgetContainer
	 *            {@link Composite}
	 */
	private void createBottom(final Composite widgetContainer) {
		this.lbltextfield = new Label(widgetContainer, SWT.NONE);
		this.lbltextfield.setText(this.label);
		this.textfield = new Text(widgetContainer, SWT.MULTI | SWT.BORDER | SWT.WRAP | SWT.V_SCROLL);
		this.textfield.setTextLimit(textFieldLimit);
		this.textfield.setBackground(this.textfield.getDisplay().getSystemColor(SWT.COLOR_WHITE));
		final GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true, 2, 1);
		gridData.minimumHeight = 150;
		gridData.minimumWidth = 400;
		this.textfield.setLayoutData(gridData);
		this.textfield.addModifyListener(new ModifyListener() {
			/**
			 * Method for Modify Text
			 */
			@Override
			public void modifyText(final ModifyEvent event) {
				final Text text = (Text) event.widget;
				int selectionPosition = text.getCharCount();
				text.setSelection(selectionPosition);
				if (editableMode) {
					getButton(IDialogConstants.OK_ID).setEnabled(true);
				} else {
					getButton(IDialogConstants.OK_ID).setEnabled(false);
				}
			}
		});
		if (!editableMode) {
			this.textfield.setEditable(false);
		}
	}

	/**
	 * Method for control create OK button.
	 *
	 * @param parent
	 *            {@link Composite}
	 * @return the control {@link Control}
	 */
	@Override
	protected Control createButtonBar(final Composite parent) {
		final Control buttonBar = super.createButtonBar(parent);
		getButton(IDialogConstants.OK_ID).setEnabled(false);
		bindDialog();
		return buttonBar;
	}

	/**
	 * boolean method.
	 *
	 * @return true, if is resizable
	 */
	@Override
	protected boolean isResizable() {
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.dialogs.Dialog#okPressed()
	 */
	@Override
	protected void okPressed() {
		okPressed = true;
		super.okPressed();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.dialogs.Dialog#close()
	 */
	@Override
	public boolean close() {
		if (!okPressed) {
			this.textfield.setText(getTextfieldText());
		}
		return super.close();
	}

	/**
	 * Gets the textfield text.
	 *
	 * @return the textfield text
	 */
	public String getTextfieldText() {
		return textfieldText;
	}

	/**
	 * Sets the textfield text.
	 *
	 * @param textfieldText
	 *            the new textfield text
	 */
	public void setTextfieldText(String textfieldText) {
		this.textfieldText = textfieldText;
	}

	/**
	 * Gets the text field.
	 *
	 * @return the txtDescription
	 */
	public Text getTextField() {
		return textfield;
	}

}
