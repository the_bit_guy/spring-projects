package com.magna.xmsystem.ui.controls.widgets;

import java.net.URI;
import java.net.URISyntaxException;

import org.eclipse.jface.dialogs.IconAndMessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.events.ControlAdapter;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.program.Program;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Link;
import org.eclipse.swt.widgets.Shell;

import com.magna.xmsystem.dependencies.utils.LANG_ENUM;
import com.magna.xmsystem.dependencies.utils.XMSystemUtil;

/**
 * Class for Custom message dialog. Fix the dialog background color issue in
 * MessageDialog class
 *
 * @author Chiranjeevi.Akula
 */
public class CustomMessageDialog extends IconAndMessageDialog {
	final static private String OK_LABEL_EN = "OK";
	final static private String OK_LABEL_DE = "OK";
	final static private String CANCEL_LABEL_EN = "Cancel";
	final static private String CANCEL_LABEL_DE = "Abbrechen";
	final static private String YES_LABEL_EN = "Yes";
	final static private String YES_LABEL_DE = "Ja";
	final static private String NO_LABEL_EN = "No";
	final static private String NO_LABEL_DE = "Nein";
	final static private String SAVE_LABEL_EN = "Save";
	final static private String SAVE_LABEL_DE = "Speichern";
	final static private String DISCARD_LABEL_EN = "Discard";
	final static private String DISCARD_LABEL_DE = "Verwerfen";

	
	/** The parent. *//*
	private Shell parent;*/
	/**
	 * Constructor for CustomMessageDialog Class.
	 *
	 * @param parentShell
	 *            {@link Shell}
	 * @param dialogTitle
	 *            {@link String}
	 * @param dialogTitleImage
	 *            {@link Image}
	 * @param dialogMessage
	 *            {@link String}
	 * @param dialogImageType
	 *            {@link int}
	 * @param dialogButtonLabels
	 *            {@link String[]}
	 * @param defaultIndex
	 *            {@link int}
	 */
	public CustomMessageDialog(Shell parentShell, String dialogTitle, Image dialogTitleImage, String dialogMessage,
			int dialogImageType, String dialogButtonLabels[], int defaultIndex) {
		super(parentShell);
		image = null;
		init(dialogTitle, dialogTitleImage, dialogMessage, dialogImageType, defaultIndex, dialogButtonLabels);
	}

	/**
	 * Constructor for CustomMessageDialog Class.
	 *
	 * @param parentShell
	 *            {@link Shell}
	 * @param dialogTitle
	 *            {@link String}
	 * @param dialogTitleImage
	 *            {@link Image}
	 * @param dialogMessage
	 *            {@link String}
	 * @param dialogImageType
	 *            {@link int}
	 * @param defaultIndex
	 *            {@link int}
	 * @param dialogButtonLabels
	 *            {@link String[]}
	 */
	public CustomMessageDialog(Shell parentShell, String dialogTitle, Image dialogTitleImage, String dialogMessage,
			int dialogImageType, int defaultIndex, String dialogButtonLabels[]) {
		super(parentShell);
		//this.parent = parentShell;
		image = null;
		init(dialogTitle, dialogTitleImage, dialogMessage, dialogImageType, defaultIndex, dialogButtonLabels);
	}

	/**
	 * Method for Inits the.
	 *
	 * @param dialogTitle
	 *            {@link String}
	 * @param dialogTitleImage
	 *            {@link Image}
	 * @param dialogMessage
	 *            {@link String}
	 * @param dialogImageType
	 *            {@link int}
	 * @param defaultIndex
	 *            {@link int}
	 * @param dialogButtonLabels
	 *            {@link String[]}
	 */
	private void init(String dialogTitle, Image dialogTitleImage, String dialogMessage, int dialogImageType,
			int defaultIndex, String dialogButtonLabels[]) {
		title = dialogTitle;
		titleImage = dialogTitleImage;
		message = dialogMessage;
		switch (dialogImageType) {
		case 1: // '\001'
			image = getErrorImage();
			break;

		case 2: // '\002'
			image = getInfoImage();
			break;

		case 3: // '\003'
		case 5: // '\005'
		case 6: // '\006'
		case 7: // '\007'
			image = getQuestionImage();
			break;

		case 4: // '\004'
			image = getWarningImage();
			break;
		}
		buttonLabels = dialogButtonLabels;
		defaultButtonIndex = defaultIndex;
	}

	@Override
	protected void buttonPressed(int buttonId) {
		setReturnCode(buttonId);
		close();
	}

	@Override
	protected void configureShell(Shell shell) {
		super.configureShell(shell);
		/*final Rectangle parentShellSize = parent.getBounds();
		int width = 550;
		int height = 180;
		shell.setSize(width, height);
		final Point pos = new Point(((parentShellSize.width - width) / 2)+parentShellSize.x, ((parentShellSize.height -height) / 2)+parentShellSize.y);
		shell.setLocation(pos.x, pos.y);*/
		if (title != null)
			shell.setText(title);
		if (titleImage != null)
			shell.setImage(titleImage);
	}

	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		parent.setData("org.eclipse.e4.ui.css.id", "MyCSSTagForComposite");
		buttons = new Button[buttonLabels.length];
		for (int i = 0; i < buttonLabels.length; i++) {
			String label = buttonLabels[i];
			Button button = createButton(parent, i, label, defaultButtonIndex == i);
			buttons[i] = button;
		}

	}

	/**
	 * Method for Creates the custom area.
	 *
	 * @param parent {@link Composite}
	 * @return the control {@link Control}
	 */
	protected Control createCustomArea(Composite parent) {
		parent.setData("org.eclipse.e4.ui.css.id", "MyCSSTagForComposite");
		if(hyperLink != null && !hyperLink.isEmpty()) {
			Link link = new Link( parent, SWT.WRAP );
			if(linkMessage != null && !linkMessage.isEmpty()) {
				link.setText( linkMessage + "<a>" + hyperLink + "</a>" );
			} else {
				link.setText( "<a>" + hyperLink + "</a>" );
			}
		    link.addSelectionListener(new SelectionAdapter() {
				
				@Override
				public void widgetSelected(SelectionEvent event) {
					try {
						Program.launch(new URI(hyperLink).toString());
					} catch (URISyntaxException e) {
						e.printStackTrace();
					}					
				}
			});
		    return link;
		}
		
		return null;
	}

	@Override
	protected Control createDialogArea(Composite parent1) {
		parent1.setData("org.eclipse.e4.ui.css.id", "MyCSSTagForComposite");
		ScrolledComposite scrolledComposite = new ScrolledComposite(parent1, SWT.V_SCROLL);
		GridData data1 = new GridData(SWT.FILL, SWT.FILL, true, true);
		scrolledComposite.setLayoutData(data1);
		scrolledComposite.setData("org.eclipse.e4.ui.css.id", "MyCSSTagForComposite");

		Composite parent = new Composite(scrolledComposite, SWT.NONE);
		//parent.setLayout(new GridLayout());
		final GridLayout compositeContLayout = new GridLayout(2, false);
		parent.setLayout(compositeContLayout);
		parent.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		parent.setData("org.eclipse.e4.ui.css.id", "MyCSSTagForComposite");

		scrolledComposite.setContent(parent);
		scrolledComposite.setExpandHorizontal(true);
		scrolledComposite.setExpandVertical(true);
		scrolledComposite.addControlListener(new ControlAdapter() {
			public void controlResized(final ControlEvent e) {
				Rectangle rectangle = scrolledComposite.getClientArea();
				scrolledComposite.setMinSize(parent.computeSize(rectangle.width, SWT.DEFAULT));
			}
		});
		parent1.setBackgroundMode(SWT.INHERIT_FORCE);
		createMessageArea(parent);
		Composite composite = new Composite(parent, 0);
		GridLayout layout = new GridLayout();
		layout.marginHeight = 0;
		layout.marginWidth = 45;
		composite.setLayout(layout);
		GridData data = new GridData(GridData.FILL_BOTH);
		data.horizontalSpan = 2;
		composite.setLayoutData(data);
		customArea = createCustomArea(composite);
		if (customArea == null)
			customArea = new Label(composite, 0);
		if (parent1 instanceof Shell && parent1 != null) {
			XMSystemUtil.forceActive((Shell) parent1);
		}
		return composite;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.dialogs.IconAndMessageDialog#createMessageArea(org.eclipse.swt.widgets.Composite)
	 
	@Override
	protected Control createMessageArea(Composite parent) {
		final Composite composite = new Composite(parent, SWT.NONE);
		final GridLayout compositeContLayout = new GridLayout(2, false);
		composite.setLayout(compositeContLayout);
		composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		super.createMessageArea(composite);
		
		return parent;
	}
*/
	@Override
	protected Button getButton(int index) {
		if (buttons == null || index < 0 || index >= buttons.length)
			return null;
		else
			return buttons[index];
	}

	/**
	 * Gets the minimum message width.
	 *
	 * @return the minimum message width
	 */
	protected int getMinimumMessageWidth() {
		return convertHorizontalDLUsToPixels(300);
	}

	@Override
	protected void handleShellCloseEvent() {
		super.handleShellCloseEvent();
		setReturnCode(-1);
	}

	@Override
	public int open() {
		return super.open();
	}

	/**
	 * Method for Open.
	 *
	 * @param kind
	 *            {@link int}
	 * @param parent
	 *            {@link Shell}
	 * @param title
	 *            {@link String}
	 * @param message
	 *            {@link String}
	 * @param style
	 *            {@link int}
	 * @return true, if successful
	 */
	public static boolean open(int kind, Shell parent, String title, String message, String linkMessage, String hyperLink, int style) {
		CustomMessageDialog.linkMessage = linkMessage;
		CustomMessageDialog.hyperLink = hyperLink;
		CustomMessageDialog dialog = new CustomMessageDialog(parent, title, null, message, kind, 0,
				getButtonLabels(kind));
		style &= 268435456;
		dialog.setShellStyle(dialog.getShellStyle() | style);
		return dialog.open() == 0;
	}

	/**
	 * Gets the button labels.
	 *
	 * @param kind
	 *            {@link int}
	 * @return the button labels
	 */
	static String[] getButtonLabels(int kind) {
		final String OK, CANCEL, YES, NO, SAVE, DISCARD;
		LANG_ENUM currentLang = LANG_ENUM.ENGLISH;
		final String localeString = XMSystemUtil.getLanguage();
		if(!XMSystemUtil.isEmpty(localeString)) {
			currentLang = LANG_ENUM.valueOf(localeString.toUpperCase());
		}
		if (currentLang == LANG_ENUM.GERMAN) {
			OK = OK_LABEL_DE;
			CANCEL = CANCEL_LABEL_DE;
			YES = YES_LABEL_DE;
			NO = NO_LABEL_DE;
			SAVE = SAVE_LABEL_DE;
			DISCARD = DISCARD_LABEL_DE;
		} else {
			OK = OK_LABEL_EN;
			CANCEL = CANCEL_LABEL_EN;
			YES = YES_LABEL_EN;
			NO = NO_LABEL_EN;
			SAVE = SAVE_LABEL_EN;
			DISCARD = DISCARD_LABEL_EN;
		}
		String dialogButtonLabels[];
		switch (kind) {
		case 1: // '\001'
		case 2: // '\002'
		case 4: // '\004'
			dialogButtonLabels = (new String[] { OK });
			break;

		case 5: // '\005'
			dialogButtonLabels = (new String[] { OK, CANCEL });
			break;

		case 3: // '\003'
			dialogButtonLabels = (new String[] { YES, NO });
			break;

		case 6: // '\006'
			dialogButtonLabels = (new String[] { YES, NO, CANCEL });
			break;

		case 7: // '\007'
			dialogButtonLabels = (new String[] { SAVE, DISCARD });
			break;

		default:
			throw new IllegalArgumentException("Illegal value for kind in MessageDialog.open()");
		}
		return dialogButtonLabels;
	}

	/**
	 * Method for Open confirm.
	 *
	 * @param parent
	 *            {@link Shell}
	 * @param title
	 *            {@link String}
	 * @param message
	 *            {@link String}
	 * @return true, if successful
	 */
	public static boolean openConfirm(Shell parent, String title, String message) {
		return open(5, parent, title, message, null, null, 0);
	}

	/**
	 * Method for Open error.
	 *
	 * @param parent
	 *            {@link Shell}
	 * @param title
	 *            {@link String}
	 * @param message
	 *            {@link String}
	 */
	public static void openError(Shell parent, String title, String message) {
		open(1, parent, title, message, null, null, 0);
	}

	/**
	 * Method for Open error with hyper link.
	 *
	 * @param parent {@link Shell}
	 * @param title {@link String}
	 * @param message {@link String}
	 * @param hyperLink {@link String}
	 */
	public static void openErrorWithHyperLink(Shell parent, String title, String message, String hyperLink) {
		open(1, parent, title, message, null, hyperLink, 0);
	}
	
	/**
	 * Method for Open information.
	 *
	 * @param parent
	 *            {@link Shell}
	 * @param title
	 *            {@link String}
	 * @param message
	 *            {@link String}
	 */
	public static void openInformation(Shell parent, String title, String message) {
		open(2, parent, title, message, null, null, 0);
	}
	
	/**
	 * Method for Open information with message and hyper link.
	 *
	 * @param parent {@link Shell}
	 * @param title {@link String}
	 * @param message {@link String}
	 * @param linkMessage {@link String}
	 * @param hyperLink {@link String}
	 */
	public static void openInformationWithMessageAndHyperLink(Shell parent, String title, String message, String linkMessage, String hyperLink) {
		open(2, parent, title, message, linkMessage, hyperLink, 0);
	}

	/**
	 * Method for Open question.
	 *
	 * @param parent
	 *            {@link Shell}
	 * @param title
	 *            {@link String}
	 * @param message
	 *            {@link String}
	 * @return true, if successful
	 */
	public static boolean openQuestion(Shell parent, String title, String message) {
		return open(3, parent, title, message, null, null, 0);
	}

	/**
	 * Method for Open warning.
	 *
	 * @param parent
	 *            {@link Shell}
	 * @param title
	 *            {@link String}
	 * @param message
	 *            {@link String}
	 */
	public static void openWarning(Shell parent, String title, String message) {
		open(4, parent, title, message, null, null, 0);
	}

	@Override
	protected Button createButton(Composite parent, int id, String label, boolean defaultButton) {
		Button button = super.createButton(parent, id, label, defaultButton);
		if (defaultButton && !customShouldTakeFocus())
			button.setFocus();
		return button;
	}

	/**
	 * Method for Custom should take focus.
	 *
	 * @return true, if successful
	 */
	protected boolean customShouldTakeFocus() {
		if (customArea instanceof Label)
			return false;
		if (customArea instanceof CLabel)
			return (customArea.getStyle() & SWT.NO_FOCUS) > 0;
		else
			return true;
	}

	@Override
	public Image getImage() {
		return image;
	}

	/**
	 * Gets the button labels.
	 *
	 * @return the button labels
	 */
	protected String[] getButtonLabels() {
		return buttonLabels;
	}

	/**
	 * Gets the default button index.
	 *
	 * @return the default button index
	 */
	protected int getDefaultButtonIndex() {
		return defaultButtonIndex;
	}

	/**
	 * Sets the buttons.
	 *
	 * @param buttons
	 *            the new buttons
	 */
	protected void setButtons(Button buttons[]) {
		if (buttons == null) {
			throw new NullPointerException("The array of buttons cannot be null.");
		} else {
			this.buttons = buttons;
			return;
		}
	}

	/**
	 * Sets the button labels.
	 *
	 * @param buttonLabels
	 *            the new button labels
	 */
	protected void setButtonLabels(String buttonLabels[]) {
		if (buttonLabels == null) {
			throw new NullPointerException("The array of button labels cannot be null.");
		} else {
			this.buttonLabels = buttonLabels;
			return;
		}
	}

	/**
	 * Method for Open confirm.
	 *
	 * @param parent
	 *            {@link Shell}
	 * @param title
	 *            {@link String}
	 * @param message
	 *            {@link String}
	 * @return true, if successful
	 */
	public static boolean openSaveDiscardDialog(Shell parent, String title, String message) {
		return open(7, parent, title, message, null, null, 0);
	}

	/** The Constant NONE. */
	public static final int NONE = 0;

	/** The Constant ERROR. */
	public static final int ERROR = 1;

	/** The Constant INFORMATION. */
	public static final int INFORMATION = 2;

	/** The Constant QUESTION. */
	public static final int QUESTION = 3;

	/** The Constant WARNING. */
	public static final int WARNING = 4;

	/** The Constant CONFIRM. */
	public static final int CONFIRM = 5;

	/** The Constant QUESTION_WITH_CANCEL. */
	public static final int QUESTION_WITH_CANCEL = 6;

	/** The Constant QUESTION_WITH_CANCEL. */
	public static final int SAVE_DISCARD = 7;

	/** Member variable 'button labels' for {@link String[]}. */
	private String buttonLabels[];

	/** Member variable 'buttons' for {@link Button[]}. */
	private Button buttons[];

	/** Member variable 'default button index' for {@link Int}. */
	private int defaultButtonIndex;

	/** Member variable 'title' for {@link String}. */
	private String title;

	/** Member variable 'title image' for {@link Image}. */
	private Image titleImage;

	/** Member variable 'image' for {@link Image}. */
	private Image image;

	/** Member variable 'custom area' for {@link Control}. */
	private Control customArea;
	
	/** Member variable 'hyper link' for {@link String}. */
	private static String hyperLink;
	
	/** The email id. */
	private static String linkMessage;
}