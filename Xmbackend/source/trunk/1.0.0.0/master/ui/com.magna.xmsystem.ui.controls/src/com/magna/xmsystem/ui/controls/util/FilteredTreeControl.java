package com.magna.xmsystem.ui.controls.util;

import org.eclipse.e4.ui.workbench.swt.internal.copy.FilteredTree;
import org.eclipse.e4.ui.workbench.swt.internal.copy.PatternFilter;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Text;

/**
 * Class to create control with filtered tree. Here used internal Filtered Tree
 * as e4 has moved Filter tree api to internal package other wise we can use
 * org.eclipse.ui.workbench in dependency
 * 
 * @author shashwat.anand
 */
@SuppressWarnings("restriction")
public abstract class FilteredTreeControl extends FilteredTree {
	/**
	 * Constructor
	 * @param parent {@link Composite}
	 * @param treeStyle int
	 * @param filter {@link PatternFilter}
	 */
	public FilteredTreeControl(final Composite parent, final int treeStyle, final PatternFilter filter) {
		super(parent, treeStyle, filter);
	}

	/**
	 * @param parent {@link Composite}
	 * @return component {@link Text}
	 */
	@Override
	protected Text doCreateFilterText(final Composite parent) {
		try {
			((GridLayout) parent.getParent().getLayout()).verticalSpacing = 0;
		} catch (Exception ex) {
			/* doesnt matter */}
		return super.doCreateFilterText(parent);
	}

	/**
	 * Build tree
	 * @param parent {@link Composite}
	 * @param style int
	 * @return viewer {@link TreeViewer}
	 */
	@Override
	protected abstract TreeViewer doCreateTreeViewer(Composite parent, int style);

	
	@Override
	public void clearText() {
		super.clearText();
	}
}
