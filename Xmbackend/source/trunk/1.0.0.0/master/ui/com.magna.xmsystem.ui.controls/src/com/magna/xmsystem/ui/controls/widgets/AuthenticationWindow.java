package com.magna.xmsystem.ui.controls.widgets;

import java.net.URL;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Path;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Monitor;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;

import com.magna.xmbackend.vo.enums.Application;
import com.magna.xmbackend.vo.user.AuthResponse;

/**
 * Class for Authentication window.
 *
 * @author Chiranjeevi.Akula
 */
@Deprecated
public class AuthenticationWindow {

	/** Member variable 'username' for {@link String}. */
	private String username;

	/** Member variable 'username text' for {@link Text}. */
	private Text usernameText;

	/** Member variable 'password text' for {@link Text}. */
	private Text passwordText;

	/** Member variable 'login composite' for {@link Composite}. */
	protected Composite loginComposite;

	/** Member variable 'shell image' for {@link Image}. */
	private Image shellImage;
	
	/** Member variable 'background image' for {@link Image}. */
	private Image backgroundImage;

	/** Member variable 'ok button' for {@link Button}. */
	private Button okButton;

	/** Member variable 'cancel button' for {@link Button}. */
	private Button cancelButton;

	/** Member variable 'error msg label' for {@link Label}. */
	private Label errorMsgLabel;

	/** Member variable 'application name' for {@link String}. */
	private String applicationName;

	/** Member variable 'shell title' for {@link String}. */
	private String shellTitle;

	/** Member variable 'auth response' for {@link AuthResponse}. */
	private AuthResponse authResponse;

	/** Member variable 'label font' for {@link Font}. */
	private Font labelFont = new Font(Display.getDefault(), "Arabic Transparent", 9, SWT.BOLD);

	/** Member variable 'login user name' for {@link String}. */
	private String loginUserName;

	/** Member variable 'login password' for {@link String}. */
	private String loginPassword;

	/** Member variable 'login ok btn text' for {@link String}. */
	private String loginOkBtnText;

	/** Member variable 'login cancel btn text' for {@link String}. */
	private String loginCancelBtnText;

	/** Member variable 'shell' for {@link Shell}. */
	private Shell shell;

	/** Member variable 'display' for {@link Display}. */
	private Display display;

	/** Member variable 'button pressed' for {@link Int}. */
	int buttonPressed = Window.CANCEL;

	/**
	 * Constructor for AuthenticationWindow Class.
	 *
	 * @param shellTitle
	 *            {@link String}
	 * @param applicationName
	 *            {@link String}
	 * @param loginUserName
	 *            {@link String}
	 * @param loginPassword
	 *            {@link String}
	 * @param loginOkBtnText
	 *            {@link String}
	 * @param loginCancelBtnText
	 *            {@link String}
	 */
	public AuthenticationWindow(String shellTitle, String applicationName, String loginUserName, String loginPassword,
			String loginOkBtnText, String loginCancelBtnText) {

		this.shellTitle = shellTitle;
		this.applicationName = applicationName;
		this.loginUserName = loginUserName;
		this.loginPassword = loginPassword;
		this.loginOkBtnText = loginOkBtnText;
		this.loginCancelBtnText = loginCancelBtnText;
	}

	/**
	 * Method for Creates the shell.
	 */
	private void createShell() {

		shell = new Shell(SWT.NO_TRIM);
		shell.setText(this.shellTitle);

		final Bundle bundle = FrameworkUtil.getBundle(this.getClass());
		String shellImagePath = "icons/large/login_window.png";
		if (shellImagePath != null) {
			URL url = FileLocator.find(bundle, new Path(shellImagePath), null);
			ImageDescriptor imageDescriptor = imageDescriptorFromURI(url);
			if (imageDescriptor != null) {
				shellImage = imageDescriptor.createImage();
				shell.setImage(shellImage);
			}
		}

		String imagePath = null;
		if (this.applicationName != null && Application.CAX_START_ADMIN.name().equals(this.applicationName)) {
			imagePath = "icons/large/caxstartadminLogin.bmp";
		} else if (this.applicationName != null && Application.CAX_START_HOTLINE.name().equals(this.applicationName)) {
			imagePath = "icons/large/caxstarthotlineLogin.bmp";
		}
		if (imagePath != null) {
			URL url = FileLocator.find(bundle, new Path(imagePath), null);
			ImageDescriptor imageDescriptor = imageDescriptorFromURI(url);
			if (imageDescriptor != null) {
				backgroundImage = imageDescriptor.createImage();
				shell.setBackgroundImage(backgroundImage);
				shell.setBackgroundMode(SWT.INHERIT_FORCE);
				Rectangle imageBounds = backgroundImage.getBounds();
				shell.setSize(imageBounds.width, imageBounds.height);
			}
		}

		display = shell.getDisplay();

		final Monitor primary = display.getPrimaryMonitor();
		final Rectangle displayBounds = primary.getBounds();
		final Point shellSize = shell.getSize();
		int width = shellSize.x;
		int height = shellSize.y;
		int xAxis = (int) (displayBounds.width - width) / 2;
		int yAxis = (int) (displayBounds.height - height) / 2;

		shell.setLocation(xAxis, yAxis);

		GridLayout gridLayout = new GridLayout(2, false);
		gridLayout.marginHeight = 110;
		gridLayout.marginWidth = 80;
		shell.setLayout(gridLayout);
		
		final Color whiteColor = display.getSystemColor(SWT.COLOR_WHITE);
		final Color redColor = display.getSystemColor(SWT.COLOR_RED);

		GridData errorMsgGridData = new GridData(SWT.HORIZONTAL, SWT.TOP, true, false);
		errorMsgGridData.horizontalSpan = 2;
		errorMsgGridData.horizontalIndent = 5;
		errorMsgLabel = new Label(shell, SWT.WRAP);
		errorMsgLabel.setLayoutData(errorMsgGridData);
		errorMsgLabel.setForeground(redColor);
		errorMsgLabel.setVisible(false);

		GridData gridData = new GridData();
		gridData.grabExcessHorizontalSpace = true;
		gridData.horizontalAlignment = GridData.FILL;

		Label userLabel = new Label(shell, SWT.NONE);
		userLabel.setFont(labelFont);
		userLabel.setText(loginUserName);

		usernameText = new Text(shell, SWT.BORDER);
		usernameText.setText(username != null ? username : "");
		usernameText.selectAll();
		usernameText.forceFocus();
		usernameText.setBackground(whiteColor);
		usernameText.setLayoutData(gridData);

		Label passwordLabel = new Label(shell, SWT.NONE);
		passwordLabel.setFont(labelFont);
		passwordLabel.setText(loginPassword);

		passwordText = new Text(shell, SWT.PASSWORD | SWT.BORDER);
		passwordText.setBackground(whiteColor);
		passwordText.setLayoutData(gridData);

		GridData buttonCompositeGridData = new GridData();
		buttonCompositeGridData.grabExcessHorizontalSpace = true;
		buttonCompositeGridData.horizontalAlignment = GridData.END;
		buttonCompositeGridData.horizontalSpan = 2;

		GridLayout buttonCompositeGridLayout = new GridLayout(2, false);
		buttonCompositeGridLayout.marginWidth = 0;
		buttonCompositeGridLayout.marginHeight = 0;
		buttonCompositeGridLayout.marginLeft = 0;
		buttonCompositeGridLayout.marginRight = 0;

		Composite buttonComposite = new Composite(shell, SWT.NONE);
		buttonComposite.setLayoutData(buttonCompositeGridData);
		buttonComposite.setLayout(buttonCompositeGridLayout);

		GridData buttonGridData = new GridData();
		buttonGridData.widthHint = 100;

		okButton = new Button(buttonComposite, SWT.PUSH);
		okButton.setText(loginOkBtnText);
		okButton.setLayoutData(buttonGridData);
		okButton.setEnabled(false);

		cancelButton = new Button(buttonComposite, SWT.PUSH);
		cancelButton.setText(loginCancelBtnText);
		cancelButton.setLayoutData(buttonGridData);

		shell.setDefaultButton(okButton);
	}

	/**
	 * Method for Inits the listeners.
	 */
	private void initListeners() {
		LoginModifyListener loginModifyListener = new LoginModifyListener();
		usernameText.addModifyListener(loginModifyListener);
		passwordText.addModifyListener(loginModifyListener);

		okButton.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent selectionevent) {
				buttonPressed = Window.OK;
				okPressed();
			}
		});

		cancelButton.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent selectionevent) {
				buttonPressed = Window.CANCEL;
				close();
			}
		});
	}

	/**
	 * Method for Image descriptor from URI.
	 *
	 * @param url
	 *            {@link URL}
	 * @return the image descriptor {@link ImageDescriptor}
	 */
	private ImageDescriptor imageDescriptorFromURI(URL url) {
		try {
			return ImageDescriptor.createFromURL(url);
		} catch (Exception e) {
			System.err.println(
					"iconURI \"" + url.toExternalForm() + "\" is invalid, a \"missing image\" icon will be shown");
			return ImageDescriptor.getMissingImageDescriptor();
		}
	}

	/**
	 * Sets the username.
	 *
	 * @param user
	 *            the new username
	 */
	public void setUsername(String user) {
		this.username = user;
	}

	/**
	 * Gets the auth response.
	 *
	 * @return the auth response
	 */
	public AuthResponse getAuthResponse() {
		return authResponse;
	}

	/**
	 * Method for Open.
	 *
	 * @return the int {@link int}
	 */
	public int open() {
		createShell();
		initListeners();
		if (shell != null && !shell.isDisposed()) {
			shell.open();
			while (!shell.isDisposed()) {
				if (!display.readAndDispatch()) {
					display.sleep();
				}
			}
		}

		return buttonPressed;
	}

	/**
	 * Method for Close.
	 */
	public void close() {
		if (shellImage != null) {
			shellImage.dispose();
		}
		if (backgroundImage != null) {
			backgroundImage.dispose();
		}
		if (labelFont != null) {
			labelFont.dispose();
		}
		if (shell != null && !shell.isDisposed()) {
			shell.close();
		}
	}

	/**
	 * Method for Ok pressed.
	 */
	private void okPressed() {
		if (!errorMsgLabel.isDisposed()) {
			errorMsgLabel.setText("");
			errorMsgLabel.setVisible(false);
			errorMsgLabel.pack();
		}

		if (applicationName != null && !applicationName.isEmpty()) {
			String errorMessage = null;
			try {
				// AuthController authController = new AuthController();
				authResponse =  null;
				// authResponse = authController.authorizeLogin(usernameText.getText(), passwordText.getText(), applicationName);
			} catch (Exception e) {
				errorMessage = "Server Not Reachable! Please check the connection..";
			} finally {
				if (authResponse != null) {
					boolean isValidUser = authResponse.isValidUser();
					errorMessage = authResponse.getMessage();
					if (isValidUser) {
						this.close();
					}
				}

				if (errorMessage != null && !errorMsgLabel.isDisposed()) {
					errorMsgLabel.setText("*" + errorMessage);
					errorMsgLabel.setVisible(true);
					errorMsgLabel.pack();
				}
			}
		} else {
			CustomMessageDialog.openError(shell, shell.getText(), "Unauthorized application name!!");
		}
	}

	/**
	 * The listener interface for receiving loginModify events. The class that
	 * is interested in processing a loginModify event implements this
	 * interface, and the object created with that class is registered with a
	 * component using the component's <code>addLoginModifyListener<code>
	 * method. When the loginModify event occurs, that object's appropriate
	 * method is invoked.
	 *
	 * @see LoginModifyEvent
	 */
	private class LoginModifyListener implements ModifyListener {

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * org.eclipse.swt.events.ModifyListener#modifyText(org.eclipse.swt.
		 * events.ModifyEvent)
		 */
		@Override
		public void modifyText(ModifyEvent event) {
			if (usernameText != null && passwordText != null) {
				if (!usernameText.getText().isEmpty() && !passwordText.getText().isEmpty()) {
					if (okButton != null) {
						okButton.setEnabled(true);
					}
				} else {
					if (okButton != null) {
						okButton.setEnabled(false);
					}
				}
			}
		}
	}
}
