package com.magna.xmsystem.ui.controls.widgets;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * class for CustomXMAdminNewDialogArea creating new dialog.
 *
 * @author Deepak Upadhyay
 */
public class CustomTextAreaDialog extends Dialog {

	/** Logger Instance class. */
	private static final Logger LOGGER = LoggerFactory.getLogger(CustomTextAreaDialog.class);

	/** Instance for textDescription. */
	transient private Text textArea;

	/** Instance for labelDescription. */
	protected Label lblTextArea;

	/** Instance for lblCount. */
	transient protected Label lblCount;

	/** Instance for composite area. */
	transient protected Composite area;

	/** Instance for lblMainLabel. */
	private Label lblMainLabel;

	/** Instance for MainLabel. */
	private String strMainLabel;

	/** Instance for maxlimit. */
	private int maxLimit;

	/** Instance for editable mode. */
	private boolean editableMode;

	/** Member variable to store the parent shell. */
	transient private Shell parentShell;

	/** Member variable for dialog title. */
	transient private String title;

	/** Member variable 'text area text' for {@link String}. */
	private String textAreaText;

	/** Member variable to check OK button pressed. */
	private boolean okPressed;

	/**
	 * Parameterized constructor.
	 *
	 * @param shell
	 *            {@link Shell}
	 * @param title
	 *            {@link String}
	 * @param strMainLabel
	 *            {@link String}
	 * @param maxLimit
	 *            {@link int}
	 * @param editableMode
	 *            {@link boolean}
	 */
	public CustomTextAreaDialog(final Shell shell, final String title, final String strMainLabel, final int maxLimit,
			final boolean editableMode) {
		super(shell);
		this.parentShell = shell;
		this.title = title;
		this.strMainLabel = strMainLabel;
		this.maxLimit = maxLimit;
		this.editableMode = editableMode;
	}

	/**
	 * Configures the Shell.
	 *
	 * @param newShell
	 *            {@link Shell}
	 * @see org.eclipse.jface.window.Window#configureShell(org.eclipse.swt.widgets.Shell)
	 */
	@Override
	protected void configureShell(final Shell newShell) {
		try {
			super.configureShell(newShell);
			newShell.setText(this.title);
			newShell.setParent(this.parentShell);
			final Rectangle shellBounds = parentShell.getBounds();
			final int width = 450;
			final int height = 300;
			newShell.setSize(width, height);
			final Point pos = new Point((shellBounds.width - width) / 2, (shellBounds.height - height) / 2);
			newShell.setLocation(pos.x, pos.y);

		} catch (Exception e) {
			LOGGER.error("Execution occurred while configuring shell!", e); //$NON-NLS-1$
		}
	}

	/**
	 * Sets the style of shell.
	 *
	 * @param newShellStyle
	 *            the new shell style
	 * @see org.eclipse.jface.window.Window#setShellStyle(int)
	 */
	@Override
	protected void setShellStyle(int newShellStyle) {
		super.setShellStyle(newShellStyle | SWT.RESIZE);

	}

	/**
	 * method for countCharactor.
	 */
	public void countCharactor() {
		this.lblCount.setText(CustomTextAreaComposite.LIMIT_COUNT_PADDING + this.textArea.getText().length()
				+ CustomTextAreaComposite.SLASH + this.maxLimit + CustomTextAreaComposite.LIMIT_COUNT_PADDING);
		this.lblCount.getParent().layout();
		this.lblCount.getParent().update();
	}

	/**
	 * method createDialogArea for create control dialog.
	 *
	 * @param parent
	 *            {@link Composite}
	 * @return the control {@link Control}
	 */
	@Override
	protected Control createDialogArea(final Composite parent) {
		try {
			parent.setData("org.eclipse.e4.ui.css.CssClassName", "DialogComposite");
			parent.setBackgroundMode(SWT.INHERIT_FORCE);
			final Composite area = (Composite) super.createDialogArea(parent);
			final GridLayout areaLayout = new GridLayout(1, false);
			area.setLayout(areaLayout);
			final Composite widgetContainer = new Composite(area, SWT.NONE);
			final GridLayout widgetContainerLayout = new GridLayout(1, false);
			widgetContainer.setLayout(widgetContainerLayout);
			widgetContainer.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
			createTopBar(widgetContainer);
			createBottom(widgetContainer);
		} catch (Exception e) {
			LOGGER.error("Unable to create UI elements", e);
		}
		return area;
	}

	/**
	 * method for create Top bar.
	 *
	 * @param widgetContainer
	 *            {@link Composite}
	 */
	private void createTopBar(final Composite widgetContainer) {
		final Composite topBar = new Composite(widgetContainer, SWT.None);
		final GridLayout widgetContainerLayout = new GridLayout(4, false);
		widgetContainerLayout.marginRight = 0;
		widgetContainerLayout.marginLeft = 0;
		widgetContainerLayout.marginTop = 0;
		widgetContainerLayout.marginBottom = 0;
		widgetContainerLayout.marginWidth = 0;
		widgetContainerLayout.verticalSpacing = 0;
		widgetContainerLayout.horizontalSpacing = 0;
		topBar.setLayout(widgetContainerLayout);
		topBar.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));

		this.lblMainLabel = new Label(topBar, SWT.NONE);
		this.lblMainLabel.setText(this.strMainLabel);
		this.lblMainLabel.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, false, 1, 1));

		this.lblMainLabel = new Label(topBar, SWT.NONE);
		this.lblMainLabel.setText("");
		this.lblMainLabel.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

		this.lblCount = new Label(topBar, SWT.BORDER);
		this.lblCount.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, false));
	}

	/**
	 * method for create bottom part.
	 *
	 * @param widgetContainer
	 *            {@link Composite}
	 */
	private void createBottom(final Composite widgetContainer) {
		this.textArea = new Text(widgetContainer, SWT.MULTI | SWT.BORDER | SWT.WRAP | SWT.V_SCROLL);
		this.textArea.setBackground(this.textArea.getDisplay().getSystemColor(SWT.COLOR_WHITE));
		final GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true, 2, 1);
		gridData.minimumHeight = 150;
		gridData.minimumWidth = 400;
		this.textArea.setLayoutData(gridData);
		this.lblCount.setText(CustomTextAreaComposite.LIMIT_COUNT_PADDING + this.textArea.getText().length()
				+ CustomTextAreaComposite.SLASH + this.maxLimit + CustomTextAreaComposite.LIMIT_COUNT_PADDING);
		this.textArea.setTextLimit(this.maxLimit);
		this.textArea.addModifyListener(new ModifyListener() {
			/**
			 * Method for Modify Text
			 */
			@Override
			public void modifyText(final ModifyEvent event) {
				countCharactor();
				final Text text = (Text) event.widget;
				int selectionPosition = text.getCharCount();
				text.setSelection(selectionPosition);
				if (editableMode) {
					getButton(IDialogConstants.OK_ID).setEnabled(true);
				} else {
					getButton(IDialogConstants.OK_ID).setEnabled(false);
				}
			}

		});

		if (!editableMode) {
			this.textArea.setEditable(false);
		}
	}

	/**
	 * Method for control create OK button.
	 *
	 * @param parent
	 *            {@link Composite}
	 * @return the control {@link Control}
	 */
	@Override
	protected Control createButtonBar(final Composite parent) {
		final Control buttonBar = super.createButtonBar(parent);
		getButton(IDialogConstants.OK_ID).setEnabled(false);
		bindDialog();
		return buttonBar;
	}

	/**
	 * method for dialog binding.
	 */
	protected void bindDialog() {
		// Nothing
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.dialogs.Dialog#isResizable()
	 */
	@Override
	protected boolean isResizable() {
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.dialogs.Dialog#okPressed()
	 */
	@Override
	protected void okPressed() {
		okPressed = true;
		super.okPressed();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.dialogs.Dialog#close()
	 */
	@Override
	public boolean close() {
		if (!okPressed) {
			this.textArea.setText(getTextAreaText());
		}
		return super.close();
	}

	/**
	 * Method for get string main label.
	 *
	 * @return the str main label
	 */
	public String getStrMainLabel() {
		return strMainLabel;
	}

	/**
	 * setter for strmainlabel.
	 *
	 * @param strMainLabel
	 *            the new str main label
	 */
	public void setStrMainLabel(final String strMainLabel) {
		this.strMainLabel = strMainLabel;
	}

	/**
	 * method for getting max limit for character.
	 *
	 * @return the max limit
	 */
	public int getMaxLimit() {
		return maxLimit;
	}

	/**
	 * method for setting max limit.
	 *
	 * @param maxLimit
	 *            the new max limit
	 */
	public void setMaxLimit(final int maxLimit) {
		this.maxLimit = maxLimit;
	}

	/**
	 * method for get Label.
	 *
	 * @return the lbl main label
	 */
	public Label getLblMainLabel() {
		return lblMainLabel;
	}

	/**
	 * Gets the textarea.
	 *
	 * @return the textarea
	 */
	public Text getTextarea() {
		return textArea;
	}

	/**
	 * method for set Label.
	 *
	 * @param lblMainLabel
	 *            the new lbl main label
	 */
	public void setLblMainLabel(final Label lblMainLabel) {
		this.lblMainLabel = lblMainLabel;
	}

	/**
	 * Gets the text area text.
	 *
	 * @return the text area text
	 */
	public String getTextAreaText() {
		return textAreaText;
	}

	/**
	 * Gets the text area text.
	 *
	 * @param textAreaText
	 *            the new text area text
	 * @return the text area text
	 */
	public void setTextAreaText(String textAreaText) {
		this.textAreaText = textAreaText;
	}
}
