package com.magna.xmsystem.xmenu.ui.rearrangeapps;

import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.swt.graphics.Image;

import com.magna.xmsystem.dependencies.utils.XMSystemUtil;
import com.magna.xmsystem.xmenu.ui.model.Icon;
import com.magna.xmsystem.xmenu.ui.model.ProjectApplication;
import com.magna.xmsystem.xmenu.ui.utils.XmMenuUtil;

/**
 * The Class ProjectAppTableLableProvider.
 * 
 * @author subash.janarthanan
 * 
 */
public class ProjectAppTableLableProvider implements ILabelProvider {

	/**
	 * Instantiates a new project app table lable provider.
	 */
	public ProjectAppTableLableProvider() {
		super();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.viewers.IBaseLabelProvider#addListener(org.eclipse.
	 * jface.viewers.ILabelProviderListener)
	 */
	@Override
	public void addListener(ILabelProviderListener arg0) {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.IBaseLabelProvider#dispose()
	 */
	@Override
	public void dispose() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.viewers.IBaseLabelProvider#isLabelProperty(java.lang.
	 * Object, java.lang.String)
	 */
	@Override
	public boolean isLabelProperty(Object arg0, String arg1) {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.viewers.IBaseLabelProvider#removeListener(org.eclipse.
	 * jface.viewers.ILabelProviderListener)
	 */
	@Override
	public void removeListener(ILabelProviderListener arg0) {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.ILabelProvider#getImage(java.lang.Object)
	 */
	@Override
	public Image getImage(Object element) {
		if (element instanceof ProjectApplication) {
			final Icon icon = ((ProjectApplication) element).getIcon();
			final boolean active = ((ProjectApplication) element).isActive();
			Image image = XmMenuUtil.getInstance().getImage(icon, active);
			return image;
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.ILabelProvider#getText(java.lang.Object)
	 */
	@Override
	public String getText(Object element) {
		if (element instanceof ProjectApplication) {
			ProjectApplication projectApplication = (ProjectApplication) element;				
			String name = projectApplication.getName(XmMenuUtil.getInstance().getCurrentLocaleEnum());
			if (!XMSystemUtil.isEmpty(name)) {
				return name;
			}
			return projectApplication.getName();	
		}
		return null;
	}
}
