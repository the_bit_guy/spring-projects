package com.magna.xmsystem.xmenu.ui.statusbar;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.eclipse.e4.core.contexts.ContextInjectionFactory;
import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.core.di.annotations.Creatable;
import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.ui.di.UIEventTopic;
import org.eclipse.e4.ui.di.UISynchronize;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magna.xmsystem.xmenu.ui.utils.CommonConstants;

/**
 * The Class StatusBarToolControl.
 *
 * @author shashwat.anand
 */
@Creatable
public class StatusBarToolControl extends Composite {
	
	/** Logger instance. */
	private static final Logger LOGGER = LoggerFactory.getLogger(StatusBarToolControl.class);

	/** {@link UISynchronize} instance. */
	@Inject
	private UISynchronize uiSynchronize;
	
	/** Member variable for {@link IEclipseContext}. */
	@Inject
	private IEclipseContext eclipseContext;

	/** Label for status. */
	private Label statusLabel;

	/** The progress control. */
	private MenuProgressControl progressControl;

	/**
	 * Constructor.
	 *
	 * @param parent the parent
	 * @since 20.12.2013
	 */
	@Inject
	public StatusBarToolControl(final Composite parent) {
		super(parent, SWT.BORDER);
	}

	/**
	 * Creates the gui.
	 */
	@PostConstruct
	public void createGui() {
		GridLayoutFactory.fillDefaults()/*.spacing(0, 0).margins(0, 0).extendedMargins(0, 0, 0, 0)*/
				.applyTo(this.getParent());
		GridLayoutFactory.fillDefaults().numColumns(2).equalWidth(false)/*.spacing(0, 0).margins(0, 0)
				.extendedMargins(0, 0, 0, 0)*/.applyTo(this);
		GridDataFactory.fillDefaults().align(SWT.FILL, SWT.FILL).grab(true, true).applyTo(this);
		
		createStatusControl(this);
		
		eclipseContext.set(UISynchronize.class, uiSynchronize);
		eclipseContext.set(Composite.class, this);
		this.progressControl = ContextInjectionFactory.make(MenuProgressControl.class, eclipseContext);
	}
	
	/**
	 * Creates the label in status bar.
	 *
	 * @param parent            {@link Composite}
	 * @return {@link Label}
	 */
	private void createStatusControl(final Composite parent) {
		final Composite statusComposite = new Composite(parent, SWT.NONE);
		GridDataFactory.fillDefaults().align(SWT.FILL, SWT.FILL).grab(true, true).applyTo(statusComposite);
		GridLayoutFactory.fillDefaults().margins(0, 0).spacing(0, 0).applyTo(statusComposite);
		this.statusLabel = new Label(statusComposite, SWT.NONE);
		GridDataFactory.fillDefaults().align(SWT.FILL, SWT.FILL).indent(5, 0).grab(true, true).applyTo(this.statusLabel);
	}
	
	/**
	 * Listen for any event message.
	 *
	 * @param message            {@link String}
	 * @return the event
	 */
	@Inject
	@Optional
	public void getEvent(@UIEventTopic(CommonConstants.EVENT_BROKER.STATUSBAR_MESSAGE) final String message) {
		updateInterface(message);
	}
	
	/**
	 * Updates the UI.
	 *
	 * @param message            {@link String}
	 */
	public void updateInterface(final String message) {
		try {
			this.uiSynchronize.syncExec(new Runnable() {
				/**
				 * Run method
				 */
				@Override
				public void run() {
					try {
						if (statusLabel != null && !statusLabel.isDisposed()) {
							statusLabel.setText(message);
							statusLabel.requestLayout();
							statusLabel.getParent().redraw();
							statusLabel.getParent().getParent().update();
						}
					} catch (Exception exc) {
						LOGGER.error("Exception occured while setting status bar text", exc);
					}
				}
			});
		} catch (Exception exception) {
			LOGGER.error("Exception occured while setting status bar text in update interface method", exception);
		}
	}
	
	/**
	 * Gets the progress control.
	 *
	 * @return the progressControl
	 */
	public MenuProgressControl getProgressControl() {
		return progressControl;
	}
}