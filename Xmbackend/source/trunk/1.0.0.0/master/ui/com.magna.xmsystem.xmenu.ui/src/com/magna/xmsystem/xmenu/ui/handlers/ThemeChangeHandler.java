
package com.magna.xmsystem.xmenu.ui.handlers;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.core.services.nls.Translation;
import org.eclipse.e4.ui.css.core.dom.ExtendedDocumentCSS;
import org.eclipse.e4.ui.css.core.engine.CSSEngine;
import org.eclipse.e4.ui.css.swt.internal.theme.ThemeEngine;
import org.eclipse.e4.ui.css.swt.theme.ITheme;
import org.eclipse.e4.ui.css.swt.theme.IThemeEngine;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.css.sac.CSSParseException;
import org.w3c.dom.stylesheets.StyleSheet;
import org.w3c.dom.stylesheets.StyleSheetList;

import com.magna.xmsystem.xmenu.message.Message;
import com.magna.xmsystem.xmenu.ui.dialogs.ThemeDialog;
import com.magna.xmsystem.xmenu.ui.parts.XmMenuPart;
import com.magna.xmsystem.xmenu.ui.parts.XmMenuPartAction;
import com.magna.xmsystem.xmenu.ui.service.LoadDataFromService;
import com.magna.xmsystem.xmenu.ui.utils.CommonConstants;

/**
 * The Class ThemeChangeHandler.
 */
@SuppressWarnings("restriction")
public class ThemeChangeHandler {
	
	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(ThemeChangeHandler.class);
	
	/** The file input stream. */
	private FileInputStream fileInputStream;
	
	/** Member variable 'messages' for {@link Message}. */
	@Inject
	@Translation
	private Message messages;
	
	/**
	 * Inject of {@link EModelService}
	 */
	@Inject
	private EModelService modelService;

	/**
	 * Inject of {@link MApplication}
	 */
	@Inject
	private MApplication application;

	/**
	 * Execute.
	 *
	 * @param shell the shell
	 * @param themeEngine the theme engine
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws InvocationTargetException the invocation target exception
	 * @throws InterruptedException the interrupted exception
	 */
	@Execute
	public void execute(final Shell shell, final IThemeEngine themeEngine)
			throws IOException, InvocationTargetException, InterruptedException {
		MPart part = (MPart) this.modelService.find(CommonConstants.PART_ID.XMENU_ID, application);
		final XmMenuPart xmMenuPart = (XmMenuPart) part.getObject();
		final XmMenuPartAction xmMenuPartAction = xmMenuPart.getXmMenuPartAction();
		
		ThemeDialog dialog = new ThemeDialog(shell, themeEngine, messages, xmMenuPartAction);
		int open = dialog.open();

		if (open == IDialogConstants.OK_ID) {
			((ThemeEngine) themeEngine).resetCurrentTheme();
			ITheme selectedTheme = dialog.getSelectedTheme();
			if (selectedTheme != null) {
				themeEngine.setTheme(selectedTheme, true);
				
			} else {
				File cssEngine = dialog.getCSSEngine();
				applyCSS(themeEngine, cssEngine);
			}
			xmMenuPartAction.reloadApplications();
			xmMenuPartAction.updateXMenuPanel();
		}
	}

	/**
	 * Apply CSS.
	 *
	 * @param themeEngine the theme engine
	 * @param cssEngine the css engine
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws InvocationTargetException the invocation target exception
	 * @throws InterruptedException the interrupted exception
	 */
	public void applyCSS(IThemeEngine themeEngine, File cssEngine)
			throws IOException, InvocationTargetException, InterruptedException {
		if (themeEngine == null) {
			return;
		}
		
		Job applyCssJob = new Job("Applying Theme...") {
			
			@Override
			protected IStatus run(IProgressMonitor monitor) {
				monitor.beginTask("Applying Theme...", 100);
				monitor.worked(20);		
				
				try {
					long start = System.nanoTime();
					StringBuilder sb = new StringBuilder();
					
					Display.getDefault().asyncExec(new Runnable() {
						
						@Override
						public void run() {
							((ThemeEngine) themeEngine).resetCurrentTheme();
							int count = 0;
							for (CSSEngine engine : ((ThemeEngine) themeEngine).getCSSEngines()) {
								if (count++ > 0) {
									sb.append("\n\n");
								}
								sb.append("Engine[").append(engine.getClass().getSimpleName()).append("]");
								ExtendedDocumentCSS doc = (ExtendedDocumentCSS) engine.getDocumentCSS();
								List<StyleSheet> sheets = new ArrayList<>();
								StyleSheetList list = doc.getStyleSheets();
								for (int i = 0; i < list.getLength(); i++) {
									sheets.add(list.item(i));
									monitor.worked(30);
								}
								
								try {
									fileInputStream = new FileInputStream(cssEngine);
									sheets.add(engine.parseStyleSheet(fileInputStream));
									doc.removeAllStyleSheets();
									for (StyleSheet sheet : sheets) {
										doc.addStyleSheet(sheet);
									}
									monitor.worked(50);
									engine.reapply();
									long nanoDiff = System.nanoTime() - start;
									sb.append("\nTime: ").append(nanoDiff / 1000000).append("ms");
									monitor.worked(20);
								} catch (CSSParseException e) {
									sb.append("\nError: line ").append(e.getLineNumber()).append(" col ")
									.append(e.getColumnNumber()).append(": ").append(e.getLocalizedMessage());
									LOGGER.error(sb.toString()); //$NON-NLS-1$
								} catch (IOException e) {
									sb.append("\nError: ").append(e.getLocalizedMessage());
									LOGGER.error(sb.toString()); //$NON-NLS-1$
								} finally {
									monitor.worked(10);
									try {
										fileInputStream.close();
									} catch (IOException e) {
										LOGGER.error("IOException while closing the fileInputStream!" + e); //$NON-NLS-1$
									}
									monitor.worked(10);
								}
							}
						}
					});
				} catch (Exception e) {
					LOGGER.error("Exception occured while applying custom theme" + e); //$NON-NLS-1$
				}
				
				return Status.OK_STATUS;
			}
		};
		applyCssJob.setUser(true);
		applyCssJob.schedule();
	}
	
	@CanExecute
	public boolean canExceute() {
		return !LoadDataFromService.isInOffline();
	}

}