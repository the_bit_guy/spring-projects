
package com.magna.xmsystem.xmenu.ui.handlers;

import javax.inject.Inject;

import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.core.services.nls.Translation;
import org.eclipse.e4.ui.workbench.IWorkbench;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magna.xmbackend.vo.enums.Application;
import com.magna.xmbackend.vo.user.AuthResponse;
import com.magna.xmsystem.dependencies.utils.XMSystemUtil;
import com.magna.xmsystem.ui.controls.widgets.CustomMessageDialog;
import com.magna.xmsystem.xmadmin.restclient.users.UserController;
import com.magna.xmsystem.xmadmin.restclient.validation.AuthController;
import com.magna.xmsystem.xmenu.message.Message;
import com.magna.xmsystem.xmenu.ui.switchuser.SwitchUserDialog;
import com.magna.xmsystem.xmenu.ui.utils.CommonConstants;
import com.magna.xmsystem.xmenu.ui.utils.XmMenuUtil;

/**
 * The Class SwitchUserHandler.
 * 
 * @author subash.janarthanan
 * 
 */
public class SwitchUserHandler {
	
	/**
	 * Logger instance
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(SwitchUserHandler.class);

	/** The message. */
	@Inject
	@Translation
	private Message message;

	/**
	 * Execute.
	 */
	@Execute
	public void execute(final IWorkbench workbench, final Shell shell) {
		UserController usercontroller = new UserController();
		boolean superAdmin = usercontroller.isSuperAdmin();
		if (superAdmin) {
			final SwitchUserDialog userListDialog = new SwitchUserDialog(shell, message.userListDialogTitle,
					message.userListDialogSelectUserLbl, message.userListDialogSelectBtn,
					message.userListDialogCancelBtn, message.userSwitchErrorTitle, message.userSwitchErrorMessage);
			if (userListDialog.open() == Window.OK) {
				String selectedUser = userListDialog.getSelectedUser();
				if (selectedUser == null) {
					CustomMessageDialog.openWarning(Display.getCurrent().getActiveShell(), message.unableToSwitchUser,
							message.userNotSelected);
				} else {
					String encryptTicket = XmMenuUtil.getInstance().getUserTicket();
					long startTimeMillis = System.currentTimeMillis();
					AuthController authController = new AuthController();
					AuthResponse authResponse = authController.authorizeLogin(encryptTicket,
							Application.CAX_ADMIN_MENU.name());
					long endTimeMillis = System.currentTimeMillis();
					if (authResponse == null) {
						CustomMessageDialog.openError(Display.getCurrent().getActiveShell(), message.windowTitleLabel,
								message.serverNotReachable);
					} else {
						boolean isValidUser = authResponse.isValidUser();
						long ldapResponseTime = authResponse.getLdapResponseTime();
						if (isValidUser) {
							XmMenuUtil.getInstance().setINEclipsePrefs(
									CommonConstants.PREFERENCE_KEY.ADMIN_MENU_ACCESS_USERNAME, selectedUser);
							XmMenuUtil.getInstance().setINEclipsePrefs(
									CommonConstants.PREFERENCE_KEY.ADMIN_MENU_ACCESS_TKT,
									XMSystemUtil.getInstance().getAdminMenuAccessTicket());

							workbench.restart();

							LOGGER.info("Ldap response time :" + ldapResponseTime);
							LOGGER.info("Webservice response time :"
									+ (endTimeMillis - (startTimeMillis + ldapResponseTime) + "ms"));
						}
					}
				}
			}
		} else {
			CustomMessageDialog.openError(Display.getCurrent().getActiveShell(), message.windowTitleLabel,
					message.userSwitchAcessDeniedMessage);
		}

	}
	
	

	/**
	 * Can execute.
	 *
	 * @return true, if successful
	 */
	@CanExecute
	public boolean canExecute() {
		return !XmMenuUtil.getInstance().isSwitchedUser();
	}
}