package com.magna.xmsystem.xmenu.ui.providers;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;

import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.Viewer;

/**
 * The Class ThemeListContentProvider.
 */
public class ThemeListContentProvider implements IStructuredContentProvider {

	/* (non-Javadoc)
	 * @see org.eclipse.jface.viewers.IStructuredContentProvider#getElements(java.lang.Object)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Object[] getElements(Object inputElement) {
		if (inputElement instanceof ArrayList) {
			@SuppressWarnings("rawtypes")
			ArrayList arrayList = (ArrayList) inputElement;
			File file = new File(Platform.getInstanceLocation().getURL().getPath() + "themecss");
			if(file.isDirectory()) {
				File[] listFiles = file.listFiles(new FileFilter() {
					
					@Override
					public boolean accept(File pathname) {
						if (pathname.getName().endsWith(".css")) {
							return true;
						}
						return false;
					}
				});
				
				for (File cssFile : listFiles) {
					arrayList.add(cssFile);
				}
			}
			return arrayList.toArray();
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.viewers.IContentProvider#dispose()
	 */
	@Override
	public void dispose() {
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.viewers.IContentProvider#inputChanged(org.eclipse.jface.viewers.Viewer, java.lang.Object, java.lang.Object)
	 */
	@Override
	public void inputChanged(Viewer arg0, Object arg1, Object arg2) {
	}

}
