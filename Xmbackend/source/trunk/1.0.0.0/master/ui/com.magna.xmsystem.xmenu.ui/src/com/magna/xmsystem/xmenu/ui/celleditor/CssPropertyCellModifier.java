package com.magna.xmsystem.xmenu.ui.celleditor;

import org.eclipse.jface.viewers.ICellModifier;

import com.steadystate.css.dom.Property;

/**
 * The Class CssPropertyCellModifier.
 */
public class CssPropertyCellModifier implements ICellModifier {

	/* (non-Javadoc)
	 * @see org.eclipse.jface.viewers.ICellModifier#getValue(java.lang.Object, java.lang.String)
	 */
	@Override
	public Object getValue(Object element, String property) {
		if (element instanceof Property) {
			Property sheet = (Property) element;
			if (property.equals("0")) {
				return sheet.getName();
			} else if (property.equals("1")) {
				return sheet.getValue().getCssText();
			}
		}

		return null;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.viewers.ICellModifier#canModify(java.lang.Object, java.lang.String)
	 */
	@Override
	public boolean canModify(Object element, String property) {
		if (element instanceof Property) {
			if (property.equals("0")) {
				return false;
			}
			return true;
		}

		return false;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.viewers.ICellModifier#modify(java.lang.Object, java.lang.String, java.lang.Object)
	 */
	@Override
	public void modify(Object element, String property, Object value) {
	}

}
