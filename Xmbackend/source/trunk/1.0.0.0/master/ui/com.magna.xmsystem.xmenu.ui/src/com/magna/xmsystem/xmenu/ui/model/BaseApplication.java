package com.magna.xmsystem.xmenu.ui.model;

import java.beans.PropertyChangeEvent;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.magna.xmsystem.dependencies.utils.LANG_ENUM;

// TODO: Auto-generated Javadoc
/**
 * Class for Base application.
 *
 * @author Chiranjeevi.Akula
 */
public class BaseApplication extends BeanModel implements ICaxStartMenu{

	/** The Constant PROPERTY_BASEAPPLICATIONID. */
	public static final String PROPERTY_BASEAPPLICATIONID = "baseApplicationId"; //$NON-NLS-1$

	/** The Constant PROPERTY_NAME. */
	public static final String PROPERTY_NAME = "name"; //$NON-NLS-1$

	/** The Constant PROPERTY_ACTIVE. */
	public static final String PROPERTY_ACTIVE = "active"; //$NON-NLS-1$

	/** The Constant SUPPORTED_PLATFORMS. */
	public static final List<String> SUPPORTED_PLATFORMS = Arrays.asList("WINDOWS32", "WINDOWS64", "LINUX64"); //$NON-NLS-1$

	/** The Constant PROPERTY_PLATFORM_MAP. */
	public static final String PROPERTY_PLATFORM_MAP = "platformMap"; //$NON-NLS-1$

	/** The Constant PROPERTY_DESC_MAP. */
	public static final String PROPERTY_DESC_MAP = "descriptionMap"; //$NON-NLS-1$

	/** The Constant PROPERTY_REMARKS_MAP. */
	public static final String PROPERTY_REMARKS_MAP = "remarksMap"; //$NON-NLS-1$

	/** The Constant PROPERTY_PROGRAM. */
	public static final String PROPERTY_PROGRAM = "program"; //$NON-NLS-1$

	/** The Constant PROPERTY_ICON. */
	public static final String PROPERTY_ICON = "icon"; //$NON-NLS-1$

	/** Member variable 'base application id' for {@link String}. */
	private String baseApplicationId;

	/** The name. */
	private String name;

	/** Member variable 'active' for {@link Boolean}. */
	private boolean active;

	/** Member variable 'platform map' for {@link Map<String,Boolean>}. */
	private Map<String, Boolean> platformMap;

	/** Member variable 'description map' for {@link Map<LANG_ENUM,String>}. */
	private Map<LANG_ENUM, String> descriptionMap;

	/** Member variable 'remarks map' for {@link Map<LANG_ENUM,String>}. */
	private Map<LANG_ENUM, String> remarksMap;

	/** Member variable 'program' for {@link String}. */
	private String program;

	/** Member variable 'icon' for {@link Icon}. */
	private Icon icon;

	/**
	 * Constructor for BaseApplication Class.
	 *
	 * @param baseApplicationId {@link String}
	 * @param name the name
	 * @param isActive {@link boolean}
	 * @param icon {@link Icon}
	 */
	public BaseApplication(final String baseApplicationId, final String name, final boolean isActive, final Icon icon) {
		this(baseApplicationId, name, isActive, new HashMap<>(), new HashMap<>(), new HashMap<>(), null, icon);
	}

	/**
	 * Constructor for BaseApplication Class.
	 *
	 * @param baseApplicationId {@link String}
	 * @param name the name
	 * @param isActive {@link boolean}
	 * @param descriptionMap {@link Map<LANG_ENUM,String>}
	 * @param remarksMap {@link Map<LANG_ENUM,String>}
	 * @param platformMap {@link Map<String,Boolean>}
	 * @param program {@link String}
	 * @param icon {@link Icon}
	 */
	public BaseApplication(final String baseApplicationId, final String name, final boolean isActive,
			final Map<LANG_ENUM, String> descriptionMap, final Map<LANG_ENUM, String> remarksMap,
			final Map<String, Boolean> platformMap, final String program, final Icon icon) {
		super();
		this.baseApplicationId = baseApplicationId;
		this.name = name;
		this.active = isActive;
		this.descriptionMap = descriptionMap;
		this.remarksMap = remarksMap;
		this.platformMap = platformMap;
		this.program = program;
		this.icon = icon;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(final String name) {
		if (name == null) throw new IllegalArgumentException();
		this.propertyChangeSupport.firePropertyChange(PROPERTY_NAME, this.name, this.name = name.trim());
	}
	
	/**
	 * Gets the platform map.
	 *
	 * @return the platform map
	 */
	public Map<String, Boolean> getPlatformMap() {
		return this.platformMap;
	}

	/**
	 * Gets the platform.
	 *
	 * @param platformKey {@link String}
	 * @return the platform
	 */
	public Boolean getPlatform(final String platformKey) {
		Object value = this.platformMap.get(platformKey);
		if (value == null) {
			return false;
		}
		return (Boolean) value;
	}

	/**
	 * Gets the platform string.
	 *
	 * @return the platform string
	 */
	public String getPlatformString() {
		Set<String> keySet = getPlatformMap().keySet();
		Set<String> tempKeySet = new HashSet<>(keySet);
		for (String key : keySet) {
			if (!getPlatform(key)) {
				tempKeySet.remove(key);
			}
		}
		return String.join(";", tempKeySet);
	}

	/**
	 * Method for Sets the platform.
	 *
	 * @param platformKey {@link String}
	 * @param platform {@link Boolean}
	 */
	public void setPlatform(final String platformKey, final Boolean platform) {
		if (platformKey == null)
			throw new IllegalArgumentException();
		this.propertyChangeSupport.firePropertyChange(PROPERTY_PLATFORM_MAP, this.platformMap,
				this.platformMap.put(platformKey, platform));
	}

	/**
	 * Method for Sets the platform map.
	 *
	 * @param platformMap {@link Map<String,Boolean>}
	 */
	public void setPlatformMap(final Map<String, Boolean> platformMap) {
		if (platformMap == null)
			throw new IllegalArgumentException();
		this.propertyChangeSupport.firePropertyChange(PROPERTY_PLATFORM_MAP, this.platformMap,
				this.platformMap = platformMap);
	}

	/**
	 * Checks if is active.
	 *
	 * @return true, if is active
	 */
	public boolean isActive() {
		return active;
	}

	/**
	 * Sets the active.
	 *
	 * @param isActive the new active
	 */
	public void setActive(final boolean isActive) {
		this.propertyChangeSupport.firePropertyChange(PROPERTY_ACTIVE, this.active, this.active = isActive);
	}

	/**
	 * Gets the description map.
	 *
	 * @return the description map
	 */
	public Map<LANG_ENUM, String> getDescriptionMap() {
		return this.descriptionMap;
	}

	/**
	 * Gets the description.
	 *
	 * @param lang {@link LANG_ENUM}
	 * @return the description
	 */
	public String getDescription(final LANG_ENUM lang) {
		return this.descriptionMap.get(lang);
	}

	/**
	 * Method for Sets the description.
	 *
	 * @param lang {@link LANG_ENUM}
	 * @param description {@link String}
	 */
	public void setDescription(final LANG_ENUM lang, final String description) {
		if (lang == null || description == null)
			throw new IllegalArgumentException();
		this.propertyChangeSupport.firePropertyChange(PROPERTY_DESC_MAP, this.descriptionMap,
				this.descriptionMap.put(lang, description.trim()));
	}

	/**
	 * Method for Sets the description map.
	 *
	 * @param descriptionMap {@link Map<LANG_ENUM,String>}
	 */
	public void setDescriptionMap(final Map<LANG_ENUM, String> descriptionMap) {
		if (descriptionMap == null)
			throw new IllegalArgumentException();
		this.propertyChangeSupport.firePropertyChange(PROPERTY_DESC_MAP, this.descriptionMap,
				this.descriptionMap = descriptionMap);
	}

	/**
	 * Gets the remarks map.
	 *
	 * @return the remarks map
	 */
	public Map<LANG_ENUM, String> getRemarksMap() {
		return this.remarksMap;
	}

	/**
	 * Gets the remarks.
	 *
	 * @param lang {@link LANG_ENUM}
	 * @return the remarks
	 */
	public String getRemarks(final LANG_ENUM lang) {
		return this.remarksMap.get(lang);
	}

	/**
	 * Method for Sets the remarks map.
	 *
	 * @param remarksMap {@link Map<LANG_ENUM,String>}
	 */
	public void setRemarksMap(final Map<LANG_ENUM, String> remarksMap) {
		if (remarksMap == null)
			throw new IllegalArgumentException();
		this.propertyChangeSupport.firePropertyChange(PROPERTY_REMARKS_MAP, this.remarksMap,
				this.remarksMap = remarksMap);
	}

	/**
	 * Method for Sets the remarks.
	 *
	 * @param lang {@link LANG_ENUM}
	 * @param remarks {@link String}
	 */
	public void setRemarks(final LANG_ENUM lang, final String remarks) {
		if (lang == null || remarks == null)
			throw new IllegalArgumentException();
		this.propertyChangeSupport.firePropertyChange(PROPERTY_REMARKS_MAP, this.remarksMap,
				this.remarksMap.put(lang, remarks));
	}

	/**
	 * Gets the base application id.
	 *
	 * @return the base application id
	 */
	public String getBaseApplicationId() {
		return this.baseApplicationId;
	}

	/**
	 * Sets the base application id.
	 *
	 * @param baseApplicationId the new base application id
	 */
	public void setBaseApplicationId(String baseApplicationId) {
		this.propertyChangeSupport.firePropertyChange(PROPERTY_BASEAPPLICATIONID, this.baseApplicationId,
				this.baseApplicationId = baseApplicationId);
	}

	/**
	 * Gets the icon.
	 *
	 * @return the icon
	 */
	public Icon getIcon() {
		return this.icon;
	}

	/**
	 * Sets the icon.
	 *
	 * @param icon the new icon
	 */
	public void setIcon(Icon icon) {
		if (icon == null)
			throw new IllegalArgumentException();
		this.propertyChangeSupport.firePropertyChange(PROPERTY_ICON, this.icon, this.icon = icon);
	}

	/**
	 * Gets the program.
	 *
	 * @return the program
	 */
	public String getProgram() {
		return this.program;
	}

	/**
	 * Sets the program.
	 *
	 * @param program the new program
	 */
	public void setProgram(String program) {
		this.propertyChangeSupport.firePropertyChange(PROPERTY_PROGRAM, this.program, this.program = program);
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.BeanModel#
	 * propertyChange(java.beans.PropertyChangeEvent)
	 */
	@Override
	public void propertyChange(PropertyChangeEvent event) {
		this.propertyChangeSupport.firePropertyChange(event.getPropertyName(), event.getOldValue(),
				event.getNewValue());
	}
}
