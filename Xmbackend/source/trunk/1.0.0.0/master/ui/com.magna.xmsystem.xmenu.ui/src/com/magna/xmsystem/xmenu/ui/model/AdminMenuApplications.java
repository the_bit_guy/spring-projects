package com.magna.xmsystem.xmenu.ui.model;

import java.util.List;

/**
 * Class for Admin menu applications.
 *
 * @author Chiranjeevi.Akula
 */
public class AdminMenuApplications {
	
	/** Member variable 'is admin menu user' for {@link Boolean}. */
	private boolean isAdminMenuUser;
	
	/** Member variable 'user applications list' for {@link List<UserApplication>}. */
	private List<UserApplication> userApplicationsList;
	
	/** Member variable 'project applications list' for {@link List<ProjectApplication>}. */
	private List<ProjectApplication> projectApplicationsList;
	
	/**
	 * Gets the user applications list.
	 *
	 * @return the user applications list
	 */
	public List<UserApplication> getUserApplicationsList() {
		return userApplicationsList;
	}
	
	/**
	 * Sets the user applications list.
	 *
	 * @param userApplicationsList the new user applications list
	 */
	public void setUserApplicationsList(List<UserApplication> userApplicationsList) {
		this.userApplicationsList = userApplicationsList;
	}
	
	/**
	 * Gets the project applications list.
	 *
	 * @return the project applications list
	 */
	public List<ProjectApplication> getProjectApplicationsList() {
		return projectApplicationsList;
	}
	
	/**
	 * Sets the project applications list.
	 *
	 * @param projectApplicationsList the new project applications list
	 */
	public void setProjectApplicationsList(List<ProjectApplication> projectApplicationsList) {
		this.projectApplicationsList = projectApplicationsList;
	}

	/**
	 * Checks if is admin menu user.
	 *
	 * @return true, if is admin menu user
	 */
	public boolean isAdminMenuUser() {
		return isAdminMenuUser;
	}

	/**
	 * Sets the admin menu user.
	 *
	 * @param isAdminMenuUser the new admin menu user
	 */
	public void setAdminMenuUser(boolean isAdminMenuUser) {
		this.isAdminMenuUser = isAdminMenuUser;
	}	
}
