package com.magna.xmsystem.xmenu.ui.switchuser;

import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magna.xmsystem.dependencies.utils.XMSystemUtil;
import com.magna.xmsystem.ui.controls.widgets.CustomMessageDialog;
import com.magna.xmsystem.xmenu.ui.model.User;
import com.magna.xmsystem.xmenu.ui.service.LoadDataFromService;

/**
 * Class for Switch user dialog.
 *
 * @author subash.janarthanan
 */
public class SwitchUserDialog extends Dialog {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(SwitchUserDialog.class);

	/** Member variable 'parent shell' for {@link Shell}. */
	private Shell parentShell;

	/** Member variable 'list of users' for {@link Group}. */
	private Group listOfUsers;

	/** Member variable 'users list tree viewer' for {@link SwitchUserTreeViewer}. */
	private SwitchUserTreeViewer usersListTreeViewer;

	/** Member variable 'users list' for {@link List<User>}. */
	private List<User> usersList;

	/** Member variable 'selected user' for {@link String}. */
	private String selectedUser;

	/** Member variable 'user list dialog title' for {@link String}. */
	private String userListDialogTitle;

	/** Member variable 'user list dialog select user lbl' for {@link String}. */
	private String userListDialogSelectUserLbl;

	/** Member variable 'user list dialog select btn' for {@link String}. */
	private String userListDialogSelectBtn;

	/** Member variable 'user list dialog cancel btn' for {@link String}. */
	private String userListDialogCancelBtn;

	/** Member variable 'user switch error title' for {@link String}. */
	private String userSwitchErrorTitle;

	/** Member variable 'user switch error message' for {@link String}. */
	private String userSwitchErrorMessage;

	/** Member variable 'filter text' for {@link Text}. */
	private Text filterText;

	/**
	 * Constructor for SwitchUserDialog Class.
	 *
	 * @param parentShell {@link Shell}
	 * @param userListDialogTitle {@link String}
	 * @param userListDialogSelectUserLbl {@link String}
	 * @param userListDialogSelectBtn {@link String}
	 * @param userListDialogCancelBtn {@link String}
	 * @param userSwitchErrorTitle {@link String}
	 * @param userSwitchErrorMessage {@link String}
	 */
	public SwitchUserDialog(final Shell parentShell, final String userListDialogTitle,
			final String userListDialogSelectUserLbl, final String userListDialogSelectBtn,
			final String userListDialogCancelBtn, final String userSwitchErrorTitle,
			final String userSwitchErrorMessage) {
		super(parentShell);
		this.parentShell = parentShell;
		this.userListDialogTitle = userListDialogTitle;
		this.userListDialogSelectUserLbl = userListDialogSelectUserLbl;
		this.userListDialogSelectBtn = userListDialogSelectBtn;
		this.userListDialogCancelBtn = userListDialogCancelBtn;
		this.userSwitchErrorTitle = userSwitchErrorTitle;
		this.userSwitchErrorMessage = userSwitchErrorMessage;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.window.Window#configureShell(org.eclipse.swt.widgets.
	 * Shell)
	 */
	@Override
	protected void configureShell(final Shell newShell) {
		try {
			super.configureShell(newShell);
			newShell.setText(this.userListDialogTitle);
			newShell.setParent(this.parentShell);
		} catch (Exception e) {
			LOGGER.error("Execution occurred while configuring list of Users shell!", e); //$NON-NLS-1$
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.dialogs.Dialog#okPressed()
	 */
	@Override
	protected void okPressed() {
		if (usersListTreeViewer != null) {
			final IStructuredSelection selection = (IStructuredSelection) usersListTreeViewer.getSelection();
			Object firstElement = selection.getFirstElement();
			if (firstElement instanceof User) {
				selectedUser = ((User) firstElement).getName();
			}
		}

		if (selectedUser == null) {
			CustomMessageDialog.openWarning(Display.getCurrent().getActiveShell(), this.userSwitchErrorTitle,
					this.userSwitchErrorMessage);
		} else {
			super.okPressed();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.dialogs.Dialog#createButtonsForButtonBar(org.eclipse.
	 * swt.widgets.Composite)
	 */
	@Override
	protected void createButtonsForButtonBar(final Composite parent) {
		parent.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		parent.setData("org.eclipse.e4.ui.css.id", "MyCSSTagForComposite");
		
		Label spacer = new Label(parent, SWT.NONE);
		spacer.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		spacer.setData("org.eclipse.e4.ui.css.id", "MyCSSTagForComposite");

		GridLayout layout = (GridLayout) parent.getLayout();
		layout.numColumns++;
		layout.makeColumnsEqualWidth = false;
		
		super.createButtonsForButtonBar(parent);
		parent.setData("org.eclipse.e4.ui.css.id", "MyCSSTagForComposite");
		
		final GridData buttonGridData = new GridData();
		buttonGridData.widthHint = 70;
		final Button okBtn = getButton(IDialogConstants.OK_ID);
		final Button cancelBtn = getButton(IDialogConstants.CANCEL_ID);
		okBtn.setLayoutData(buttonGridData);
		cancelBtn.setLayoutData(buttonGridData);
		okBtn.setText(this.userListDialogSelectBtn);
		cancelBtn.setText(this.userListDialogCancelBtn);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.dialogs.Dialog#createDialogArea(org.eclipse.swt.widgets
	 * .Composite)
	 */
	@Override
	protected Control createDialogArea(final Composite parent) {
		try {
			final Composite widgetContainer = new Composite(parent, SWT.NONE);
			widgetContainer.setLayout(new GridLayout(1, false));
			GridDataFactory.fillDefaults().grab(true, true).align(SWT.FILL, SWT.FILL).hint(270, 250)
					.applyTo(widgetContainer);
			widgetContainer.setData("org.eclipse.e4.ui.css.id", "MyCSSTagForComposite");
			initGUI(widgetContainer);
			initListeners();
			final Rectangle parentSize = this.parentShell.getBounds();
			Point computeSize = parent.computeSize(SWT.DEFAULT, SWT.DEFAULT);
			Point defaultLocation = new Point((int) (parentSize.width - computeSize.x) / 2,
					(int) (parentSize.height - computeSize.y) / 2);
			parent.getShell().setLocation(defaultLocation);
		} catch (Exception e) {
			LOGGER.error("Unable to create list of users UI elements", e); //$NON-NLS-1$
		}
		return parent;
	}

	/**
	 * Method for Inits the GUI.
	 *
	 * @param widgetContainer {@link Composite}
	 */
	private void initGUI(final Composite widgetContainer) {
		final Composite listOfUsersMainComposite = new Composite(widgetContainer, SWT.NONE);
		listOfUsersMainComposite.setLayout(new GridLayout(1, false));
		listOfUsersMainComposite.setData("org.eclipse.e4.ui.css.id", "MyCSSTagForComposite");
		final GridData layoutData = new GridData(SWT.FILL, SWT.FILL, true, true);
		layoutData.heightHint = 200;
		listOfUsersMainComposite.setLayoutData(layoutData);

		this.listOfUsers = new Group(listOfUsersMainComposite, SWT.NONE);
		GridLayoutFactory.fillDefaults().numColumns(1).applyTo(this.listOfUsers);
		listOfUsers.setData("org.eclipse.e4.ui.css.id", "MyCSSTagForComposite");
		this.listOfUsers.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		this.listOfUsers.setText(this.userListDialogSelectUserLbl);

		final Composite userTblComposite = new Composite(this.listOfUsers, SWT.NONE);
		userTblComposite.setLayout(new GridLayout(1, false));
		userTblComposite.setLayoutData(layoutData);
		userTblComposite.setData("org.eclipse.e4.ui.css.id", "MyCSSTagForComposite");

		filterText = new Text(userTblComposite, SWT.BORDER);
		filterText.setMessage("type filter text");
		filterText.setFocus();
		GridDataFactory.fillDefaults().grab(true, false).align(SWT.FILL, SWT.TOP).applyTo(filterText);
		
		createTreeViewer(userTblComposite);

		loadUserListInTreeViewer();
	}

	/**
	 * Method for Inits the listeners.
	 */
	private void initListeners() {
		this.filterText.addModifyListener(new ModifyListener() {
			
			@Override
			public void modifyText(ModifyEvent event) {
				if(usersList != null && !usersList.isEmpty() && usersListTreeViewer != null) {
					
					String text = ((Text) event.widget).getText();
					List<User> filteredList = usersList.stream()
							.filter(s -> s.getName().toLowerCase().contains(text.toLowerCase()))
							.collect(Collectors.toList());
					
					usersListTreeViewer.getTree().setItemCount(filteredList.size());
					usersListTreeViewer.setInput(filteredList);
					usersListTreeViewer.refresh();
				}				
			}
		});
	}
	
	/**
	 * Method for Creates the tree viewer.
	 *
	 * @param userTblComposite {@link Composite}
	 */
	private void createTreeViewer(final Composite userTblComposite) {
		/*FilteredTreeControl scriptTreeControl = new FilteredTreeControl(userTblComposite, SWT.NONE,
				new PatternFilterTree()) {
			*//**
			 * Overrides doCreateTreeViewer method
			 *//*
			@Override
			protected TreeViewer doCreateTreeViewer(final Composite parentL, final int style) {
				UserListDialog.this.usersListTreeViewer = new UsersListTreeViewer(parentL);
				return UserListDialog.this.usersListTreeViewer;
			}
		};*/

		this.usersListTreeViewer = new SwitchUserTreeViewer(userTblComposite);
		//this.usersListTreeViewer.getControl().setFocus();
		GridDataFactory.fillDefaults().grab(true, true).align(SWT.FILL, SWT.FILL).applyTo(this.usersListTreeViewer.getControl());
	}

	/**
	 * Method for Load user list in tree viewer.
	 */
	private void loadUserListInTreeViewer() {
		Job job = new Job("Loading Users...") {

			@Override
			protected IStatus run(IProgressMonitor monitor) {
				monitor.beginTask("Loading Users..", 100);
				monitor.worked(30);

				usersList = LoadDataFromService.getInstance().getAllUsers();
				String systemUserName = XMSystemUtil.getSystemUserName();
				for (Iterator<User> iterator = usersList.iterator(); iterator.hasNext();) {
				    User user = iterator.next();
				    if (systemUserName.equals(user.getName())) {
				        iterator.remove();
				    }
				}
				if (usersList.size() != 0) {
					Display.getDefault().asyncExec(new Runnable() {
						@Override
						public void run() {
							usersListTreeViewer.setInput(usersList);
						}
					});
				}

				monitor.worked(70);
				return Status.OK_STATUS;
			}
		};
		job.setUser(true);
		job.schedule();
	}

	/**
	 * Gets the selected user.
	 *
	 * @return the selected user
	 */
	public String getSelectedUser() {
		return selectedUser;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.dialogs.Dialog#isResizable()
	 */
	@Override
	protected boolean isResizable() {
		return true;
	}
}
