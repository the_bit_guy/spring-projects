package com.magna.xmsystem.xmenu.ui.dialogs;

import org.eclipse.jface.viewers.ICellModifier;
import org.eclipse.jface.viewers.TreeViewer;

import com.steadystate.css.dom.Property;

/**
 * The Class CssPropertyCellModifier_new.
 */
public class CssPropertyCellModifier_new implements ICellModifier {

	/** The tree viewer. */
	private TreeViewer treeViewer;

	/**
	 * Instantiates a new css property cell modifier new.
	 *
	 * @param treeViewer the tree viewer
	 */
	public CssPropertyCellModifier_new(TreeViewer treeViewer) {
		this.treeViewer = treeViewer;	
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.viewers.ICellModifier#canModify(java.lang.Object, java.lang.String)
	 */
	@Override
	public boolean canModify(Object element, String property) {
		return false;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.viewers.ICellModifier#getValue(java.lang.Object, java.lang.String)
	 */
	@Override
	public Object getValue(Object element, String property) {
		if (element instanceof Property) {
			Property sheet = (Property) element;
			if (property.equals("0")) {
				return sheet.getName();
			} else if (property.equals("1")) {
				  if(sheet.getValue().getCssText().startsWith("rgb")){
				  }
			}
		}

		return null;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.viewers.ICellModifier#modify(java.lang.Object, java.lang.String, java.lang.Object)
	 */
	@Override
	public void modify(Object element, String property, Object value) {
		if (element instanceof Property) {
			Property sheet = (Property) element;
			if (property.equals("0")) {
				sheet.setName((String) value);
			} else if (property.equals("1")) {
				sheet.getValue().setCssText((String) value);
			}
			treeViewer.refresh();
		}
	}
}

