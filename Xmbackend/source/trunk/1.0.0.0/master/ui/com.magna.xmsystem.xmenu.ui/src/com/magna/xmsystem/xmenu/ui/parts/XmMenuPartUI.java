package com.magna.xmsystem.xmenu.ui.parts;

import javax.inject.Inject;

import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.core.services.nls.Translation;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.custom.StackLayout;
import org.eclipse.swt.events.ControlAdapter;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.magna.xmsystem.dependencies.utils.XMSystemUtil;
import com.magna.xmsystem.ui.controls.widgets.MagnaCustomCombo;
import com.magna.xmsystem.xmenu.message.Message;
import com.magna.xmsystem.xmenu.ui.utils.CommonConstants;
import com.magna.xmsystem.xmenu.ui.utils.XmMenuUtil;

/**
 * The Class XmMenuPartUI.
 */
public class XmMenuPartUI extends Composite {
	
	/**
	 * Logger instance
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(XmMenuPartUI.class);

	/** The tool item. */
	protected ToolItem toolItem;

	/** The lbl project list. */
	protected Label lblProjectList;

	/** Member variable 'tb combo projects' for {@link MagnaCustomCombo}. */
	protected MagnaCustomCombo tbComboProjects;
	
	/** The h user app composite. */
	protected Composite hUserAppComposite;
	
	/** The h project app composite. */
	protected Composite hProjectAppComposite;
	
	/** The v user composite. */
	protected Composite vUserAppComposite;
	
	/** The v project comp. */
	protected Composite vProjectAppComposite;
	
	/** The content panel. */
	protected Composite contentPanel;
	
	/** The top composite. */
	protected Composite topComposite;
	
	/** The project list composite. */
	protected Composite projectListComposite;

	/** The content panel layout. */
	protected StackLayout contentPanelLayout;

	/** The horizontal composite. */
	protected Composite horizontalComposite;

	/** The vertical scrolled composite. */
	protected ScrolledComposite verticalScrolledComposite;
	
	/** The v button composite. */
	protected  Composite vButtonComposite;

	/** Member variable 'h lbl user applications' for {@link Label}. */
	protected Label hLblUserApplications;

	/** Member variable 'h lbl project applications' for {@link Label}. */
	protected Label hLblProjectApplications;

	/** Member variable 'v lbl user applications' for {@link Label}. */
	protected Label vLblUserApplications;

	/** Member variable 'v lbl project applications' for {@link Label}. */
	protected Label vLblProjectApplications;
	
	/** Member variable for messages. */
	@Inject
	@Translation
	protected Message messages;

	/** The h user appscrolled composite. */
	protected ScrolledComposite hUserAppscrolledComposite;
	
	/** The h project appscrolled composite. */
	protected ScrolledComposite hProjectAppscrolledComposite;
	
	/** The switch back tool bar. */
	protected ToolBar resetSwitchedUserToolBar;

	/** The tool item switch back. */
	protected ToolItem resetSwitchedUserItem;

	
	/**
	 * Constructor for XmMenuPartUI Class.
	 *
	 * @param parent {@link Composite}
	 * @param style {@link int}
	 */
	public XmMenuPartUI(final Composite parent, final int style) {
		super(parent, style);
	}

	/**
	 * Create UI.
	 *
	 * @param eclipseContext
	 *            the eclipse context
	 */
	protected void createUI(final IEclipseContext eclipseContext) {
		try {
			GridLayoutFactory.fillDefaults().numColumns(1).applyTo(this);
			this.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
			
			createTopComp();
			/*Label lblSeprator = new Label(this, SWT.SEPARATOR | SWT.HORIZONTAL);
			GridDataFactory.fillDefaults().align(SWT.FILL, SWT.CENTER).grab(true, false).applyTo(lblSeprator);*/

			createApplicationComp();
			
			refreshPart(); //Part Initialization
		} catch (Exception ex) {
			LOGGER.error("Unable to crete UI elements", ex); //$NON-NLS-1$
		}
	}

	/**
	 * Create top comp.
	 */
	private void createTopComp() {
		topComposite = new Composite(this, SWT.NONE);
		GridLayoutFactory.fillDefaults().numColumns(3).equalWidth(false).applyTo(topComposite);
		topComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));

		Label lblEmpty = new Label(topComposite, SWT.NONE);
		lblEmpty.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

		Label lblLogo = new Label(topComposite, SWT.NONE);
		lblLogo.setImage(XMSystemUtil.getInstance().getImage(this.getClass(), "icons/large/magna_steyr_logo.png"));
		GridDataFactory.fillDefaults().grab(false, false).align(SWT.CENTER, SWT.TOP).applyTo(lblLogo);

		/*lblEmpty = new Label(topComposite, SWT.NONE);
		lblEmpty.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));*/
		
		this.resetSwitchedUserToolBar = new ToolBar(topComposite, SWT.NONE);
		this.resetSwitchedUserToolBar.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, true, true));
		this.resetSwitchedUserItem = new ToolItem(this.resetSwitchedUserToolBar, SWT.FLAT);
		this.resetSwitchedUserItem.setImage(
				XMSystemUtil.getInstance().getImage(this.getClass(), "icons/32x32/reset_switch_user.png"));
		this.resetSwitchedUserItem.setToolTipText(messages.resetSwitchedUserIconTooltip);

		if (XmMenuUtil.getInstance().isSwitchedUser()) {
			this.resetSwitchedUserToolBar.setVisible(true);
		} else {
			this.resetSwitchedUserToolBar.setVisible(false);
		}

		
		lblEmpty = new Label(topComposite, SWT.NONE);
		lblEmpty.setText("   ");
		lblEmpty.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		
		projectListComposite = new Composite(topComposite, SWT.NONE);
		GridLayoutFactory.fillDefaults().numColumns(2).extendedMargins(0, 0, 0, 0).applyTo(projectListComposite);
		GridData projectListLayoutData = new GridData(SWT.FILL, SWT.CENTER, true, false);
		projectListComposite.setLayoutData(projectListLayoutData);

		this.lblProjectList = new Label(projectListComposite, SWT.NONE);
		this.lblProjectList.setData("org.eclipse.e4.ui.css.id", "ProjectLabel");
		this.lblProjectList.setText(messages.projectListLabel);
		

		this.tbComboProjects = new MagnaCustomCombo(projectListComposite, SWT.READ_ONLY | SWT.BORDER);
		this.tbComboProjects.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		this.tbComboProjects.setVisibleItemCount(10);
		this.tbComboProjects.setEditable(false);
		this.tbComboProjects.setData("org.eclipse.e4.ui.css.id", "MyCSSTagFortbComboProjects");
		
		boolean isHorizontalLayout = false;
		String layoutTooltipIcon = "icons/vertical.png";
		String layoutTooltipText = messages.horizontalIconTootip;
		
		final ToolBar toolbar = new ToolBar(topComposite, SWT.NONE);
		toolbar.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false));
		this.toolItem = new ToolItem(toolbar, SWT.FLAT);
		
		String eclipsePrefs = XmMenuUtil.getInstance().getEclipsePrefs(CommonConstants.PREFERENCE_KEY.HORIZONTAL_VIEW);
		if (!XMSystemUtil.isEmpty(eclipsePrefs) && eclipsePrefs != null) {
			if ((new Gson()).fromJson(eclipsePrefs, Boolean.class)) {
				isHorizontalLayout = true;
				layoutTooltipIcon = "icons/horizontal.png";
				layoutTooltipText = messages.verticalIconTooltip;
			}
		}
		
		this.toolItem.setImage(XMSystemUtil.getInstance().getImage(this.getClass(), layoutTooltipIcon));		
		this.toolItem.setData("Horizontal", isHorizontalLayout);
		this.toolItem.setToolTipText(layoutTooltipText);
	}
	
	/**
	 * Create bottom comp.
	 *//*
	private void createBottomComp() {
		final SashForm mainVerticalSashform = new SashForm(this, SWT.VERTICAL | SWT.BORDER);
		mainVerticalSashform.setLayout(new GridLayout(1, false));
		mainVerticalSashform.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

		createApplicationComp(mainVerticalSashform);

		//eclipseContext.set(SashForm.class, mainVerticalSashform);
		//this.liveMessageConsole = ContextInjectionFactory.make(LiveMessageConsole.class, eclipseContext);

		mainVerticalSashform.setWeights(new int[] { 75, 25 });
	}*/
	
	/**
	 * Create application comp.
	 *
	 * @param mainVerticalSashform
	 *            the main vertical sashform
	 */
	public void createApplicationComp() {
		// create the main composite where stack layout is applied
		this.contentPanel = new Composite(this, SWT.BORDER);
		this.contentPanel.setLayout(new GridLayout(1, false));
		this.contentPanel.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
	}

	/**
	 * Refresh part.
	 */
	protected void refreshPart() {
		if (this.contentPanel == null) {
			return;
		} else {
			Control[] children = this.contentPanel.getChildren();
			for (Control control : children) {
				control.dispose();
			}
		}
		this.contentPanel.setRedraw(false);
		
		this.contentPanelLayout = new StackLayout();
		this.contentPanel.setLayout(contentPanelLayout);
		
		GridData horiCompLayoutData = new GridData(SWT.CENTER, SWT.CENTER, false, true);

		// create the horizontal page's content
		this.horizontalComposite = new Composite(contentPanel, SWT.NONE);
		horizontalComposite.setLayout(new GridLayout(2, false));
		GridDataFactory.fillDefaults().grab(true, true).align(SWT.FILL, SWT.FILL).applyTo(this.horizontalComposite);
		
		hLblUserApplications = new Label(horizontalComposite, SWT.BOLD);	
		hLblUserApplications.setText(messages.userApplicationsLabel);
		hLblUserApplications.setData("org.eclipse.e4.ui.css.id", "ApplicationLabel");
		GridDataFactory.fillDefaults().grab(true, false).indent(2, 0).align(SWT.CENTER, SWT.CENTER)
				.applyTo(hLblUserApplications);
		
		hLblProjectApplications = new Label(horizontalComposite, SWT.BOLD);
		hLblProjectApplications.setText(messages.projectApplicationsLabel);
		hLblProjectApplications.setData("org.eclipse.e4.ui.css.id", "ApplicationLabel");
		GridDataFactory.fillDefaults().grab(true, false).indent(2, 0).align(SWT.CENTER, SWT.CENTER)
				.applyTo(hLblProjectApplications);

		this.hUserAppscrolledComposite = new ScrolledComposite(horizontalComposite, SWT.BORDER | SWT.V_SCROLL);
		this.hUserAppscrolledComposite.setLayout(new GridLayout());
		GridDataFactory.fillDefaults().grab(true, true).align(SWT.FILL, SWT.FILL).applyTo(hUserAppscrolledComposite);
		addActivateListenerToScrollComp(hUserAppscrolledComposite);

		Composite hUserAppPanel = new Composite(hUserAppscrolledComposite, SWT.NONE);
		hUserAppPanel.setLayout(new GridLayout(3, false));
		GridDataFactory.fillDefaults().grab(true, true).align(SWT.FILL, SWT.FILL).applyTo(hUserAppPanel);
		
		this.hUserAppComposite = new Composite(hUserAppscrolledComposite, SWT.NONE);
		this.hUserAppComposite.setLayout(new GridLayout(1, false));
		GridDataFactory.fillDefaults().grab(true, true).align(SWT.FILL, SWT.FILL).applyTo(this.hUserAppComposite);

		this.hProjectAppscrolledComposite = new ScrolledComposite(horizontalComposite, SWT.BORDER | SWT.V_SCROLL);
		this.hProjectAppscrolledComposite.setLayout(new GridLayout());
		GridDataFactory.fillDefaults().grab(true, true).align(SWT.FILL, SWT.FILL).applyTo(hProjectAppscrolledComposite);
		addActivateListenerToScrollComp(hProjectAppscrolledComposite);

		this.hProjectAppComposite = new Composite(hProjectAppscrolledComposite, SWT.NONE);
		hProjectAppComposite.setLayout(new GridLayout(1, false));
		hProjectAppComposite.setLayoutData(horiCompLayoutData);
		
		hUserAppscrolledComposite.setContent(hUserAppComposite);
		hUserAppscrolledComposite.setExpandHorizontal(true);
		hUserAppscrolledComposite.setExpandVertical(true);
		hUserAppscrolledComposite.addControlListener(new ControlAdapter() {
			public void controlResized(final ControlEvent e) {
				Rectangle rectangle = hUserAppscrolledComposite.getClientArea();
				hUserAppscrolledComposite.setMinSize(hUserAppComposite.computeSize(rectangle.width, SWT.DEFAULT));
			}
		});
		//hUserAppscrolledComposite.notifyListeners(SWT.Resize, new Event());
		//hUserAppscrolledComposite.setAlwaysShowScrollBars(true);
		
		hProjectAppscrolledComposite.setContent(hProjectAppComposite);
		hProjectAppscrolledComposite.setExpandHorizontal(true);
		hProjectAppscrolledComposite.setExpandVertical(true);
		hProjectAppscrolledComposite.addControlListener(new ControlAdapter() {
			public void controlResized(final ControlEvent e) {
				Rectangle rectangle = hProjectAppscrolledComposite.getClientArea();
				hProjectAppscrolledComposite.setMinSize(hProjectAppComposite.computeSize(rectangle.width, SWT.DEFAULT));
			}
		});
		//hProjectAppscrolledComposite.notifyListeners(SWT.Resize, new Event());
		//hProjectAppscrolledComposite.setAlwaysShowScrollBars(true);

		// create the vertical page's content
		this.verticalScrolledComposite = new ScrolledComposite(contentPanel, SWT.V_SCROLL);
		this.verticalScrolledComposite.setLayout(new GridLayout());
		addActivateListenerToScrollComp(verticalScrolledComposite);
		
		Composite vMainComposite = new Composite(verticalScrolledComposite, SWT.NONE);
		vMainComposite.setLayout(new GridLayout(3, false));
		GridDataFactory.fillDefaults().grab(true, true).align(SWT.FILL, SWT.FILL).applyTo(vMainComposite);
		vMainComposite.setBackgroundMode(SWT.INHERIT_FORCE);
		
		Composite empty = new Composite(vMainComposite, SWT.NONE);
		empty.setLayout(new GridLayout(1, false));
		empty.setData("org.eclipse.e4.ui.css.id", "Composite");
		
		GridDataFactory.fillDefaults().grab(true, true).align(SWT.FILL, SWT.FILL).applyTo(empty);
		Label emptyLabel = new Label(empty, SWT.NONE);
		GridDataFactory.fillDefaults().grab(true, true).align(SWT.FILL, SWT.FILL).applyTo(emptyLabel);
		
		vButtonComposite = new Composite(vMainComposite, SWT.NONE);
		vButtonComposite.setLayout(new GridLayout(1, false));
		GridDataFactory.fillDefaults().grab(true, true).align(SWT.FILL, SWT.FILL).applyTo(vButtonComposite);
		
		empty = new Composite(vMainComposite, SWT.NONE);
		empty.setLayout(new GridLayout(1, false));
		GridDataFactory.fillDefaults().grab(true, true).align(SWT.FILL, SWT.FILL).applyTo(empty);
		
		emptyLabel = new Label(empty, SWT.NONE);
		GridDataFactory.fillDefaults().grab(true, true).align(SWT.FILL, SWT.FILL).applyTo(emptyLabel);
		
		vLblUserApplications = new Label(vButtonComposite, SWT.BOLD);
		vLblUserApplications.setData("org.eclipse.e4.ui.css.id", "ApplicationLabel");
		vLblUserApplications.setText(messages.userApplicationsLabel);
		GridDataFactory.fillDefaults().grab(true, false).indent(2, 0).align(SWT.CENTER, SWT.CENTER)
				.applyTo(vLblUserApplications);
		
		this.vUserAppComposite = new Composite(vButtonComposite, SWT.NONE);
		vUserAppComposite.setLayout(new GridLayout(1, false));
		GridDataFactory.fillDefaults().grab(true, true).span(SWT.CENTER, SWT.FILL).applyTo(this.vUserAppComposite);
		
		vLblProjectApplications = new Label(vButtonComposite, SWT.BOLD);
		vLblProjectApplications.setData("org.eclipse.e4.ui.css.id", "ApplicationLabel");
		vLblProjectApplications.setText(messages.projectApplicationsLabel);
		GridDataFactory.fillDefaults().grab(true, false).indent(2, 0).align(SWT.CENTER, SWT.CENTER)
				.applyTo(vLblProjectApplications);

		this.vProjectAppComposite = new Composite(vButtonComposite, SWT.NONE);
		vProjectAppComposite.setLayout(new GridLayout(1, false));
		GridDataFactory.fillDefaults().grab(true, true).span(SWT.CENTER, SWT.FILL).applyTo(this.vProjectAppComposite);

		this.verticalScrolledComposite.setContent(vMainComposite);
		this.verticalScrolledComposite.setExpandHorizontal(true);
		this.verticalScrolledComposite.setExpandVertical(true);
		this.verticalScrolledComposite.addControlListener(new ControlAdapter() {
			public void controlResized(final ControlEvent e) {
				Rectangle rectangle = verticalScrolledComposite.getClientArea();
				verticalScrolledComposite.setMinSize(vMainComposite.computeSize(rectangle.width, SWT.DEFAULT));
			}
		});
		
		this.contentPanel.setRedraw(true);
		this.contentPanel.layout();
		this.contentPanel.update();
	}
	
	/**
	 * Method for Update panel.
	 */
	protected void updatePanel() {
		Display.getDefault().asyncExec(new Runnable() {
			
			@Override
			public void run() {
				if ((boolean) toolItem.getData("Horizontal")) {
					contentPanelLayout.topControl = horizontalComposite;
				} else {
					contentPanelLayout.topControl = verticalScrolledComposite;
				}
				contentPanel.layout();
				contentPanel.getParent().update();
				contentPanel.setFocus();
			}
		});		
	}
	
	/**
	 * Update top composite.
	 */
	protected void updateTopComposite() {
		Display.getDefault().asyncExec(new Runnable() {
			@Override
			public void run() {
				topComposite.layout();
				topComposite.getParent().update();
				projectListComposite.layout();
				horizontalComposite.layout();
				vButtonComposite.layout();
			}
		});
	}
	
	/**
	 * Method for Adds the activate listener to scroll comp.
	 *
	 * @param scrollComp {@link ScrolledComposite}
	 */
	private void addActivateListenerToScrollComp(final ScrolledComposite scrollComp) {
		scrollComp.addListener(SWT.Activate, new Listener() {
			public void handleEvent(Event e) {
				scrollComp.setFocus();
			}
		});
	}
	
	/**
	 * Checks if is project list enable.
	 *
	 * @return true, if is project list enable
	 */
	public boolean isProjectListEnabled() {
		return tbComboProjects.getEnabled();
	}
	
}
