package com.magna.xmsystem.xmenu.ui.utils;

import com.magna.xmbackend.entities.SitesTbl;
import com.magna.xmsystem.dependencies.utils.XMSystemUtil;
import com.magna.xmsystem.xmenu.ui.model.AdministrationArea;
import com.magna.xmsystem.xmenu.ui.service.LoadDataFromService;

/**
 * Class for Cax menu object model util.
 *
 * @author Chiranjeevi.Akula
 */
public class CaxMenuObjectModelUtil {
	
	/** Member variable 'this ref' for {@link CaxMenuObjectModelUtil}. */
	private static CaxMenuObjectModelUtil thisRef;
	
	/** Member variable 'site name' for {@link String}. */
	private String siteName;
	
	/** Member variable 'site id' for {@link String}. */
	private String siteId;
	
	/** Member variable 'admin area name' for {@link String}. */
	private String adminAreaName;
	
	/** Member variable 'admin area id' for {@link String}. */
	private String adminAreaId;
	
	/** Member variable 'hotline contact number' for {@link String}. */
	private String hotlineContactNumber;
	
	/** Member variable 'hotline contact email' for {@link String}. */
	private String hotlineContactEmail;

	/** Member variable 'singleton app timeout' for {@link Long}. */
	private Long singletonAppTimeout;
	
	/*private String userName;
	private String userId;*/
	
	/**
	 * Constructor for CaxMenuObjectModelUtil Class.
	 */
	public CaxMenuObjectModelUtil() {
		setInstance(this);
	}
	
	/**
	 * Gets the single instance of CaxMenuObjectModelUtil.
	 *
	 * @return single instance of CaxMenuObjectModelUtil
	 */
	public static CaxMenuObjectModelUtil getInstance() {
		return thisRef;
	}

	/**
	 * Sets the instance.
	 *
	 * @param thisRef the new instance
	 */
	public static void setInstance(CaxMenuObjectModelUtil thisRef) {
		CaxMenuObjectModelUtil.thisRef = thisRef;
	}

	/**
	 * Gets the site name.
	 *
	 * @return the site name
	 */
	public String getSiteName() {
		if(siteName == null || !XMSystemUtil.getSiteName().equals(siteName)) {
			siteName = XMSystemUtil.getSiteName();
		}
		return siteName;
	}

	/**
	 * Gets the site id.
	 *
	 * @return the site id
	 */
	public String getSiteId() {
		String name = XMSystemUtil.getSiteName();
		if(name == null){
			return null;
		} else {
			if(siteId == null || siteName == null || !name.equals(siteName)) {
				siteName = name;
				SitesTbl site = LoadDataFromService.getInstance().getSite(name);
				if(site != null) {
					siteId = site.getSiteId();
				} else {
					return null;
				}
			}
		}
		
		return siteId;
	}

	/**
	 * Gets the admin area name.
	 *
	 * @return the admin area name
	 */
	public String getAdminAreaName() {
		return XMSystemUtil.getAdminAreaName();
	}
	
	/**
	 * Gets the project name.
	 *
	 * @return the project name
	 */
	public String getProjectName() {
		return XMSystemUtil.getProjectName();
	}

	/**
	 * Gets the admin area id.
	 *
	 * @param projectId {@link String}
	 * @return the admin area id
	 */
	public String getAdminAreaId(final String projectId) {
		AdministrationArea administrationArea = LoadDataFromService.getInstance().getUserAdminArea(projectId, siteId);
		if(administrationArea != null) {
			adminAreaId = administrationArea.getAdministrationAreaId();
			adminAreaName =  administrationArea.getName();
			hotlineContactNumber = administrationArea.getHotlineContactNumber();
			hotlineContactEmail = administrationArea.getHotlineContactEmail();
			singletonAppTimeout = administrationArea.getSingletonAppTimeout();
			XMSystemUtil.setAdminAreaName(adminAreaName);
		} else {
			XMSystemUtil.setAdminAreaName(CommonConstants.EMPTY_STR);
			adminAreaId = null;			
		}
		return adminAreaId;
	}

	/**
	 * Gets the admin area id.
	 *
	 * @return the admin area id
	 */
	public String getAdminAreaId() {
		if(!XMSystemUtil.isEmpty(adminAreaName) && adminAreaName.equals(XMSystemUtil.getAdminAreaName())) {
			return adminAreaId;
		}
		return null;
	}

	/**
	 * Gets the hotline contact number.
	 *
	 * @return the hotline contact number
	 */
	public String getHotlineContactNumber() {
		if(!XMSystemUtil.isEmpty(adminAreaName) && adminAreaName.equals(XMSystemUtil.getAdminAreaName())) {
			return hotlineContactNumber;
		}
		return null;
	}

	/**
	 * Gets the hotline contact email.
	 *
	 * @return the hotline contact email
	 */
	public String getHotlineContactEmail() {
		if(!XMSystemUtil.isEmpty(adminAreaName) && adminAreaName.equals(XMSystemUtil.getAdminAreaName())) {
			return hotlineContactEmail;
		}
		return null;
	}	
	
	/**
	 * Gets the singleton app timeout.
	 *
	 * @return the singleton app timeout
	 */
	public Long getSingletonAppTimeout() {
		if(!XMSystemUtil.isEmpty(adminAreaName) && adminAreaName.equals(XMSystemUtil.getAdminAreaName())) {
			return singletonAppTimeout;
		}
		return null;
	}
}
