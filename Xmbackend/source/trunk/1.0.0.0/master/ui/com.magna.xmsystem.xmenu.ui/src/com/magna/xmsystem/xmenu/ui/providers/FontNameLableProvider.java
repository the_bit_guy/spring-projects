package com.magna.xmsystem.xmenu.ui.providers;

import org.eclipse.jface.viewers.LabelProvider;

public class FontNameLableProvider extends LabelProvider {

	@Override
	public String getText(Object element) {
		return element.toString();
	}

}
