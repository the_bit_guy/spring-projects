
package com.magna.xmsystem.xmenu.ui.handlers;

import java.lang.reflect.Type;
import java.util.List;

import javax.inject.Inject;

import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.core.services.nls.Translation;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.swt.widgets.Display;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.magna.xmsystem.dependencies.utils.XMSystemUtil;
import com.magna.xmsystem.xmenu.message.Message;
import com.magna.xmsystem.xmenu.ui.model.ProjectApplication;
import com.magna.xmsystem.xmenu.ui.model.UserApplication;
import com.magna.xmsystem.xmenu.ui.parts.XmMenuPart;
import com.magna.xmsystem.xmenu.ui.parts.XmMenuPartAction;
import com.magna.xmsystem.xmenu.ui.rearrangeapps.ReArrangeApplicationsDialog;
import com.magna.xmsystem.xmenu.ui.service.LoadDataFromService;
import com.magna.xmsystem.xmenu.ui.utils.CommonConstants;
import com.magna.xmsystem.xmenu.ui.utils.XmMenuUtil;

/**
 * The Class ReArrangeHandler.
 * 
 * @author subash.janarthanan
 * 
 */
public class ReArrangeHandler {

	/** Member variable 'messages' for {@link Message}. */
	@Inject
	@Translation
	private Message messages;

	/**
	 * Inject of {@link EModelService}
	 */
	@Inject
	private EModelService modelService;

	/**
	 * Inject of {@link MApplication}
	 */
	@Inject
	private MApplication application;

	/**
	 * Execute.
	 */
	@Execute
	public void execute() {
		MPart part = (MPart) this.modelService.find(CommonConstants.PART_ID.XMENU_ID, application);
		final XmMenuPart xmMenuPart = (XmMenuPart) part.getObject();
		final XmMenuPartAction xmMenuPartAction = xmMenuPart.getXmMenuPartAction();
		
		List<UserApplication> userAppList = xmMenuPartAction.getUserApplicationButtonsMap();
		List<ProjectApplication> projectAppList = xmMenuPartAction.getProjectApplicationButtonsMap();

		//FIXME: To get the active Shell, injection should be used
		ReArrangeApplicationsDialog ReArrangeApplicationsDialog = new ReArrangeApplicationsDialog(
				Display.getDefault().getActiveShell(), messages, userAppList, projectAppList);
		ReArrangeApplicationsDialog.open();
		
		int returnCode = ReArrangeApplicationsDialog.getReturnCode();
		if(returnCode == 0){
			String userAppJSonString = XmMenuUtil.getInstance().getEclipsePrefs(XMSystemUtil.getProjectName()+CommonConstants.ORDERED_PROJECT_KEY.PROJECT_USER_APPLICATION);
			String projectAppJSonString = XmMenuUtil.getInstance().getEclipsePrefs(XMSystemUtil.getProjectName()+CommonConstants.ORDERED_PROJECT_KEY.PROJECT_PROJECT_APPLICATION);
			
			Type userAppClassType = new TypeToken<List<UserApplication>>() {}.getType();
			Type projectAppClassType = new TypeToken<List<ProjectApplication>>() {}.getType();

			//TODO: Check whether it is really necessary to create two Gson instances. 
			//Would only be necessary if fromJson manipulates the Gson object.
			List<UserApplication> userAppFromJSonList = (new Gson()).fromJson(userAppJSonString, userAppClassType);
			List<ProjectApplication> projectAppFromJSonList = (new Gson()).fromJson(projectAppJSonString, projectAppClassType);
			
			String projectId = xmMenuPartAction.getProjectId();
			LoadDataFromService.getInstance().saveOrUpdateReaarangeSettings(userAppFromJSonList, projectAppFromJSonList, projectId);
			xmMenuPartAction.reOrderApplications(userAppFromJSonList, projectAppFromJSonList, projectId);
		} 
	}

	/**
	 * Can execute.
	 *
	 * @return true, if successful
	 */
	@CanExecute
	public boolean canExecute() {
		if (LoadDataFromService.isInOffline()) {
			return false;
		}
		MPart part = (MPart) this.modelService.find(CommonConstants.PART_ID.XMENU_ID, application);
		XmMenuPart xmMenuPart = (XmMenuPart) part.getObject();
		XmMenuPartAction xmMenuPartAction = xmMenuPart.getXmMenuPartAction();
		
		return xmMenuPartAction.isProjectSelected();
	}

}