package com.magna.xmsystem.xmenu.ui.switchuser;

import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.swt.graphics.Image;

import com.magna.xmsystem.xmenu.ui.model.Icon;
import com.magna.xmsystem.xmenu.ui.model.User;
import com.magna.xmsystem.xmenu.ui.utils.XmMenuUtil;

/**
 * Class for Switch user tree lable provider.
 *
 * @author subash.janarthanan
 */
public class SwitchUserTreeLableProvider extends ColumnLabelProvider{

	/**
	 * Constructor for SwitchUserTreeLableProvider Class.
	 */
	public SwitchUserTreeLableProvider() {
		super();
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.viewers.ILabelProvider#getImage(java.lang.Object)
	 */
	@Override
	public Image getImage(Object element) {
		if (element instanceof User) {
			final Icon icon = ((User) element).getIcon();
			final boolean active = ((User) element).isActive();
			Image image = XmMenuUtil.getInstance().getImage(icon, active);
			return image;
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.viewers.ILabelProvider#getText(java.lang.Object)
	 */
	@Override
	public String getText(Object element) {
		if (element instanceof User) {
			return ((User) element).getName();
		}
		return null;
	}
}
