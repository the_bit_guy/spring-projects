
package com.magna.xmsystem.xmenu.ui.handlers;

import java.util.List;

import org.eclipse.e4.ui.di.AboutToShow;
import org.eclipse.e4.ui.model.application.ui.menu.MMenuElement;

/**
 * Class for Dummy menu item.
 *
 * @author Chiranjeevi.Akula
 */
public class DummyMenuItem {

	/**
	 * Method for About to show.
	 *
	 * @param items
	 *            {@link List<MMenuElement>}
	 */
	@AboutToShow
	public void aboutToShow(List<MMenuElement> items) {
		
	}
}