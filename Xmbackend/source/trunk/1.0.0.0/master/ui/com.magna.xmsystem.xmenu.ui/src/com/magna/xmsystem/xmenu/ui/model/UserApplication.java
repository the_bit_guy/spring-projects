package com.magna.xmsystem.xmenu.ui.model;

import java.beans.PropertyChangeEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.magna.xmsystem.dependencies.utils.LANG_ENUM;

/**
 * Class for User application.
 *
 * @author Chiranjeevi.Akula
 */
public class UserApplication extends BeanModel implements ICaxStartMenu {

	/** Member variable 'user application id' for {@link String}. */
	private String userApplicationId;

	/** Member variable 'name map' for {@link Map<LANG_ENUM,String>}. */
	private Map<LANG_ENUM, String> nameMap;

	/** The Constant PROPERTY_NAME_MAP. */
	public static final String PROPERTY_NAME_MAP = "nameMap"; //$NON-NLS-1$

	/** The Constant PROPERTY_PARENT. */
	public static final String PROPERTY_PARENT = "parent"; //$NON-NLS-1$
	
	/** Member variable 'parent' for {@link Boolean}. */
	private boolean parent;
	
	/** Member variable 'active' for {@link Boolean}. */
	private boolean active;

	/** Member variable 'singleton' for {@link Boolean}. */
	private boolean singleton;

	/** Member variable 'icon' for {@link Icon}. */
	private Icon icon;

	/** Member variable 'position' for {@link String}. */
	private String position;

	/** Member variable 'base application' for {@link BaseApplication}. */
	private BaseApplication baseApplication;

	/** Member variable 'description map' for {@link Map<LANG_ENUM,String>}. */
	private Map<LANG_ENUM, String> descriptionMap;

	/** Member variable 'remarks map' for {@link Map<LANG_ENUM,String>}. */
	private Map<LANG_ENUM, String> remarksMap;

	/**
	 * Member variable 'child user applications' for
	 * {@link List<UserApplication>}.
	 */
	private List<UserApplication> childUserApplications;
	
	/** The name. */
	private String name;

	/** The description. */
	private String description;

	/**
	 * Instantiates a new user application.
	 *
	 * @param userApplicationId the user application id
	 * @param name the name
	 * @param description the description
	 * @param nameMap the name map
	 * @param active the active
	 * @param descriptionMap the description map
	 * @param remarksMap the remarks map
	 * @param icon the icon
	 * @param parent the parent
	 * @param singleton the singleton
	 * @param position the position
	 * @param baseApplication the base application
	 */
	public UserApplication(final String userApplicationId, final String name, final String description, final Map<LANG_ENUM, String> nameMap, final boolean active,
			final Map<LANG_ENUM, String> descriptionMap, final Map<LANG_ENUM, String> remarksMap, final Icon icon,
			final boolean parent,final boolean singleton, final String position, final BaseApplication baseApplication) {
		super();
		this.userApplicationId = userApplicationId;
		this.name = name;
		this.description = description;
		this.nameMap = nameMap;
		this.active = active;
		this.descriptionMap = descriptionMap;
		this.remarksMap = remarksMap;
		this.icon = icon;
		this.parent = parent;
		this.singleton=singleton;
		this.position = position;
		this.baseApplication = baseApplication;
		childUserApplications = new ArrayList<UserApplication>();
	}
	
	/**
	 * Gets the user application id.
	 *
	 * @return the user application id
	 */
	public String getUserApplicationId() {
		return userApplicationId;
	}

	/**
	 * Sets the user application id.
	 *
	 * @param userApplicationId
	 *            the new user application id
	 */
	public void setUserApplicationId(String userApplicationId) {
		this.userApplicationId = userApplicationId;
	}
	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Gets the name map.
	 *
	 * @return the name map
	 */
	public Map<LANG_ENUM, String> getNameMap() {
		return nameMap;
	}

	/**
	 * Method for Sets the name map.
	 *
	 * @param nameMap
	 *            {@link Map<LANG_ENUM,String>}
	 */
	public void setNameMap(Map<LANG_ENUM, String> nameMap) {
		this.nameMap = nameMap;
	}

	/**
	 * Gets the name.
	 *
	 * @param lang
	 *            {@link LANG_ENUM}
	 * @return the name
	 */
	public String getName(final LANG_ENUM lang) {
		return this.nameMap.get(lang);
	}

	/**
	 * Method for Sets the name.
	 *
	 * @param lang
	 *            {@link LANG_ENUM}
	 * @param name
	 *            {@link String}
	 */
	public void setName(final LANG_ENUM lang, final String name) {
		if (lang == null || name == null)
			throw new IllegalArgumentException();
		this.propertyChangeSupport.firePropertyChange(PROPERTY_NAME_MAP, this.nameMap,
				this.nameMap.put(lang, name.trim()));
	}

	/**
	 * Checks if is parent.
	 *
	 * @return true, if is parent
	 */
	public boolean isParent() {
		return parent;
	}

	/**
	 * Sets the parent.
	 *
	 * @param parent
	 *            the new parent
	 */
	public void setParent(final boolean parent) {
		this.propertyChangeSupport.firePropertyChange(PROPERTY_PARENT, this.parent, this.parent = parent);
	}
	
	/**
	 * Checks if is active.
	 *
	 * @return true, if is active
	 */
	public boolean isActive() {
		return active;
	}

	/**
	 * Sets the active.
	 *
	 * @param active
	 *            the new active
	 */
	public void setActive(boolean active) {
		this.active = active;
	}

	/**
	 * Checks if is singleton.
	 *
	 * @return true, if is singleton
	 */
	public boolean isSingleton() {
		return singleton;
	}

	/**
	 * Sets the singleton.
	 *
	 * @param singleton
	 *            the new singleton
	 */
	public void setSingleton(boolean singleton) {
		this.singleton = singleton;
	}

	/**
	 * Gets the icon.
	 *
	 * @return the icon
	 */
	public Icon getIcon() {
		return icon;
	}

	/**
	 * Sets the icon.
	 *
	 * @param icon
	 *            the new icon
	 */
	public void setIcon(Icon icon) {
		this.icon = icon;
	}

	/**
	 * Gets the position.
	 *
	 * @return the position
	 */
	public String getPosition() {
		return position;
	}

	/**
	 * Sets the position.
	 *
	 * @param position
	 *            the new position
	 */
	public void setPosition(String position) {
		this.position = position;
	}

	/**
	 * Gets the base application.
	 *
	 * @return the base application
	 */
	public BaseApplication getBaseApplication() {
		return baseApplication;
	}

	/**
	 * Sets the base application.
	 *
	 * @param baseApplication
	 *            the new base application
	 */
	public void setBaseApplication(BaseApplication baseApplication) {
		this.baseApplication = baseApplication;
	}

	/**
	 * Gets the description map.
	 *
	 * @return the description map
	 */
	public Map<LANG_ENUM, String> getDescriptionMap() {
		return descriptionMap;
	}

	/**
	 * Gets the description.
	 *
	 * @param lang {@link LANG_ENUM}
	 * @return the description
	 */
	public String getDescription(final LANG_ENUM lang) {
		return this.descriptionMap.get(lang);
	}
	
	/**
	 * Method for Sets the description map.
	 *
	 * @param descriptionMap
	 *            {@link Map<LANG_ENUM,String>}
	 */
	public void setDescriptionMap(Map<LANG_ENUM, String> descriptionMap) {
		this.descriptionMap = descriptionMap;
	}

	/**
	 * Gets the remarks.
	 *
	 * @param lang {@link LANG_ENUM}
	 * @return the remarks
	 */
	public String getRemarks(final LANG_ENUM lang) {
		return this.remarksMap.get(lang);
	}
	
	/**
	 * Gets the remarks map.
	 *
	 * @return the remarks map
	 */
	public Map<LANG_ENUM, String> getRemarksMap() {
		return remarksMap;
	}

	/**
	 * Method for Sets the remarks map.
	 *
	 * @param remarksMap
	 *            {@link Map<LANG_ENUM,String>}
	 */
	public void setRemarksMap(Map<LANG_ENUM, String> remarksMap) {
		this.remarksMap = remarksMap;
	}

	/**
	 * Gets the child user applications.
	 *
	 * @return the child user applications
	 */
	public List<UserApplication> getChildUserApplications() {
		return childUserApplications;
	}

	/**
	 * Sets the child user applications.
	 *
	 * @param childUserApplications
	 *            the new child user applications
	 */
	public void setChildUserApplications(List<UserApplication> childUserApplications) {
		this.childUserApplications = childUserApplications;
	}

	/**
	 * Method for Adds the child user application.
	 *
	 * @param userApplication {@link UserApplication}
	 */
	public void addChildUserApplication(UserApplication userApplication) {
		if(userApplication != null) {
			this.childUserApplications.add(userApplication);
		}
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.magna.xmsystem.xmadmin.ui.treeviewer.admintree.model.BeanModel#
	 * propertyChange(java.beans.PropertyChangeEvent)
	 */
	@Override
	public void propertyChange(PropertyChangeEvent event) {
		this.propertyChangeSupport.firePropertyChange(event.getPropertyName(), event.getOldValue(),
				event.getNewValue());
	}

}
