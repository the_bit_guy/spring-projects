package com.magna.xmsystem.xmenu.ui.parts;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.e4.ui.css.core.dom.ExtendedDocumentCSS;
import org.eclipse.e4.ui.css.core.engine.CSSEngine;
import org.eclipse.e4.ui.css.swt.internal.theme.ThemeEngine;
import org.eclipse.e4.ui.css.swt.theme.IThemeEngine;
import org.eclipse.swt.widgets.Display;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.css.sac.CSSParseException;
import org.w3c.dom.stylesheets.StyleSheet;
import org.w3c.dom.stylesheets.StyleSheetList;

import com.magna.xmbackend.entities.CssSettingsTbl;
import com.magna.xmbackend.vo.css.CssSettingsResponse;
import com.magna.xmsystem.xmenu.ui.service.LoadDataFromService;

/**
 * The Class ThemeExecution.
 */
@SuppressWarnings("restriction")
public class ThemeExecution {
	
	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(ThemeExecution.class);
	
	/** The theme engine. */
	private IThemeEngine themeEngine;

	/**
	 * Instantiates a new theme execution.
	 *
	 * @param engine the engine
	 */
	@Inject
	public ThemeExecution(final IThemeEngine engine) {
		this.themeEngine = engine;
		loadThemeSettings();
	}

	/**
	 * Load theme settings.
	 */
	private void loadThemeSettings() {
		CssSettingsResponse cssFileExistenceRes = LoadDataFromService.getInstance().getCssFileExistenceRes();
		if (cssFileExistenceRes != null) {
			CssSettingsTbl cssSettingsTbl = cssFileExistenceRes.getCssSettingsTbl();
			File cssFolder = new File(Platform.getInstanceLocation().getURL().getPath() + "themecss");
			File cssFile = new File(cssFolder.getAbsolutePath() + System.getProperty("file.separator") + "custom.css");
			if (cssSettingsTbl != null) {
				String appliedStr = cssSettingsTbl.getIsApplied();
				boolean isApplied = Boolean.valueOf(appliedStr);
				try {
					String cssContent = cssSettingsTbl.getCssContent();
					if (!cssFolder.isDirectory()) {
						cssFolder.mkdirs();
						createOrUpdateCSSFile(cssFile, cssContent);
					} else {
						createOrUpdateCSSFile(cssFile, cssContent);
					}
				} catch (IOException e1) {
					LOGGER.error("IOException while creating CSS file" + e1); //$NON-NLS-1$
				}
				if (isApplied) {
					if (themeEngine != null) {
						try {
							applyingTheme(themeEngine, cssFile);
						} catch (InvocationTargetException | IOException | InterruptedException e) {
							LOGGER.error("Exception occured while applying custom theme!" + e); //$NON-NLS-1$
						}
					}
				}
			} else {
				// If user doesn't have css settings then delete the css file if exist
				if (cssFile.exists()) {
					cssFile.delete();
				}
			}
		}
	}
	
	/**
	 * Creates the or update CSS file.
	 *
	 * @param cssFile the css file
	 * @param cssContent the css content
	 * @throws FileNotFoundException the file not found exception
	 */
	private void createOrUpdateCSSFile(final File cssFile, final String cssContent) throws FileNotFoundException {
		if (!cssFile.exists()) {
			try (PrintWriter out = new PrintWriter(cssFile)) {
				out.println(cssContent);
			}
		} else {
			cssFile.delete();
			try (PrintWriter out = new PrintWriter(cssFile)) {
				out.println(cssContent);
			}
		}
	}
	
	/**
	 * Applying theme.
	 *
	 * @param themeEngine the theme engine
	 * @param cssEngine the css engine
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws InvocationTargetException the invocation target exception
	 * @throws InterruptedException the interrupted exception
	 */
	protected void applyingTheme(IThemeEngine themeEngine, final File cssEngine)
			throws IOException, InvocationTargetException, InterruptedException {
		if (themeEngine == null) {
			return;
		}

		Job applyCssJob = new Job("Applying Theme...") {
			
			@Override
			protected IStatus run(IProgressMonitor monitor) {
				monitor.beginTask("Applying Theme...", 100);
				monitor.worked(20);		
				
				try {
					long start = System.nanoTime();
					StringBuilder sb = new StringBuilder();
					
					Display.getDefault().asyncExec(new Runnable() {
						
						@Override
						public void run() {
							((ThemeEngine) themeEngine).resetCurrentTheme();
							int count = 0;
							for (CSSEngine engine : ((ThemeEngine) themeEngine).getCSSEngines()) {
								if (count++ > 0) {
									sb.append("\n\n");
								}
								sb.append("Engine[").append(engine.getClass().getSimpleName()).append("]");
								ExtendedDocumentCSS doc = (ExtendedDocumentCSS) engine.getDocumentCSS();
								List<StyleSheet> sheets = new ArrayList<>();
								StyleSheetList list = doc.getStyleSheets();
								for (int i = 0; i < list.getLength(); i++) {
									sheets.add(list.item(i));									
									monitor.worked(30);
								}
								FileInputStream fileInputStream = null;
								try {
									fileInputStream = new FileInputStream(cssEngine);
									sheets.add(engine.parseStyleSheet(fileInputStream));
									doc.removeAllStyleSheets();
									for (StyleSheet sheet : sheets) {
										doc.addStyleSheet(sheet);
									}
									monitor.worked(50);
									engine.reapply();
									long nanoDiff = System.nanoTime() - start;
									sb.append("\nTime: ").append(nanoDiff / 1000000).append("ms");
								} catch (CSSParseException e) {
									sb.append("\nError: line ").append(e.getLineNumber()).append(" col ")
									.append(e.getColumnNumber()).append(": ").append(e.getLocalizedMessage());
									LOGGER.error(sb.toString()); //$NON-NLS-1$
								} catch (IOException e) {
									sb.append("\nError: ").append(e.getLocalizedMessage());
									LOGGER.error(sb.toString()); //$NON-NLS-1$
								} finally {
									monitor.worked(10);
									try {
										fileInputStream.close();
									} catch (IOException e) {
										LOGGER.error("IOException while closing the fileInputStream!" + e); //$NON-NLS-1$
									}
									monitor.worked(10);
								}
							}
						}
					});
				} catch (Exception e) {
					LOGGER.error("Exception occured while applying custom theme" + e); //$NON-NLS-1$
				}
				
				return Status.OK_STATUS;
			}
		};
		applyCssJob.setUser(true);
		applyCssJob.schedule();
	}

}
