
package com.magna.xmsystem.xmenu.ui.parts;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.eclipse.e4.core.contexts.ContextInjectionFactory;
import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.ui.css.swt.theme.IThemeEngine;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class XMenuPart.
 * 
 * @author shashwat.anand
 */
@SuppressWarnings("restriction")
public class XmMenuPart {
	
	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(XmMenuPart.class);
	
	/** The eclipse context. */
	@Inject
	private IEclipseContext eclipseContext;

	/** Member variable 'xm menu part action' for {@link XmMenuPartAction}. */
	private XmMenuPartAction xmMenuPartAction;
	
	/** The theme execution. */
	private ThemeExecution themeExecution;

	/**
	 * Post construct.
	 *
	 * @param parent the parent
	 */
	@PostConstruct
	public void postConstruct(final Composite parent, @Optional final IThemeEngine engine) {
		try {
			parent.setLayout(new GridLayout(1, false));
			parent.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
			buildComponents(parent);
			loadThemeSettings(engine);
		} catch (Exception e) {
			LOGGER.error("Exception occured at creating UI compoents!", e); //$NON-NLS-1$
		}
	}

	/**
	 * Load theme settings.
	 *
	 * @param engine the engine
	 */
	private void loadThemeSettings(final IThemeEngine engine) {
		eclipseContext.set(IThemeEngine.class, engine);
		themeExecution = ContextInjectionFactory.make(ThemeExecution.class, eclipseContext);
	}
	
	/**
	 * Build components.
	 *
	 * @param parent the parent
	 */
	private void buildComponents(final Composite parent) {
		eclipseContext.set(Composite.class, parent);
		xmMenuPartAction = ContextInjectionFactory.make(XmMenuPartAction.class, eclipseContext);
	}
	
	/**
	 * Gets the xm menu part action.
	 *
	 * @return the xm menu part action
	 */
	public XmMenuPartAction getXmMenuPartAction() {
		return xmMenuPartAction;
	}

	/**
	 * Sets the xm menu part action.
	 *
	 * @param xmMenuPartAction the new xm menu part action
	 */
	public void setXmMenuPartAction(final XmMenuPartAction xmMenuPartAction) {
		this.xmMenuPartAction = xmMenuPartAction;
	}

	/**
	 * Gets the theme execution.
	 *
	 * @return the theme execution
	 */
	public ThemeExecution getThemeExecution() {
		return themeExecution;
	}
	
}