package com.magna.xmsystem.xmenu.ui.handlers;

import javax.inject.Inject;

import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.core.services.nls.Translation;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.workbench.IWorkbench;
import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.swt.widgets.Shell;

import com.magna.xmsystem.xmenu.message.Message;
import com.magna.xmsystem.xmenu.ui.parts.XmMenuPart;
import com.magna.xmsystem.xmenu.ui.parts.XmMenuPartAction;
import com.magna.xmsystem.xmenu.ui.utils.CommonConstants;

/**
 * The Class ExitHandler.
 * 
 * @author subash.janarthanan
 * 
 */
public class ExitHandler {

	/** Member variable 'message' for {@link Message}. */
	@Inject
	@Translation
	private Message message;

	/**
	 * Inject of {@link EModelService}
	 */
	@Inject
	private EModelService modelService;

	/**
	 * Inject of {@link MApplication}
	 */
	@Inject
	private MApplication application;
	
	/**
	 * Execute.
	 *
	 * @param workbench
	 *            the workbench
	 * @param shell
	 *            the shell
	 */
	@Execute
	public void execute(final IWorkbench workbench, final Shell shell) {
		/*if (CustomMessageDialog.openConfirm(shell, message.exitDialogTitle, message.exitDialogMessage)) {
			String runningAppName = XmMenuUtil.getInstance().getRunningApplication();
			if (!XMSystemUtil.isEmpty(runningAppName)) {
				CustomMessageDialog.openWarning(shell, "Unable to close", "Application is Running : "+runningAppName);
			} else {
				LoadDataFromService.getInstance().updateUserStatusToHistory();
				workbench.close();
			}
		}*/
		MPart part = (MPart) this.modelService.find(CommonConstants.PART_ID.XMENU_ID, application);
		XmMenuPart xmMenuPart = (XmMenuPart) part.getObject();
		XmMenuPartAction xmMenuPartAction = xmMenuPart.getXmMenuPartAction();
		xmMenuPartAction.closeApplication();
	}
}
