--------------------------------------------------------
--  File created - Wednesday-February-01-2017   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Table ICON_TBL
--------------------------------------------------------

  CREATE TABLE "MAGNA"."ICON_TBL" 
   (	"ICON_ID" NUMBER(11,0), 
	"ICON_NAME" VARCHAR2(255 BYTE) DEFAULT NULL
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
REM INSERTING into MAGNA.ICON_TBL
SET DEFINE OFF;
Insert into MAGNA.ICON_TBL (ICON_ID,ICON_NAME) values (1,'allowed.gif');
Insert into MAGNA.ICON_TBL (ICON_ID,ICON_NAME) values (2,'ausertasks.gif');
Insert into MAGNA.ICON_TBL (ICON_ID,ICON_NAME) values (5,'english.gif');
Insert into MAGNA.ICON_TBL (ICON_ID,ICON_NAME) values (4,'denied.gif');
Insert into MAGNA.ICON_TBL (ICON_ID,ICON_NAME) values (3,'eventdelete.gif');
Insert into MAGNA.ICON_TBL (ICON_ID,ICON_NAME) values (13,'eventcopy.gif');
Insert into MAGNA.ICON_TBL (ICON_ID,ICON_NAME) values (7,'dprojecttasks.gif');
Insert into MAGNA.ICON_TBL (ICON_ID,ICON_NAME) values (8,'dusertasks.gif');
Insert into MAGNA.ICON_TBL (ICON_ID,ICON_NAME) values (9,'aprojecttasks.gif');
Insert into MAGNA.ICON_TBL (ICON_ID,ICON_NAME) values (10,'basictasks.gif');
Insert into MAGNA.ICON_TBL (ICON_ID,ICON_NAME) values (11,'french.gif');
Insert into MAGNA.ICON_TBL (ICON_ID,ICON_NAME) values (12,'eventstart.gif');
--------------------------------------------------------
--  DDL for Index SYS_C007492
--------------------------------------------------------

  CREATE UNIQUE INDEX "MAGNA"."SYS_C007492" ON "MAGNA"."ICON_TBL" ("ICON_ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
--------------------------------------------------------
--  Constraints for Table ICON_TBL
--------------------------------------------------------

  ALTER TABLE "MAGNA"."ICON_TBL" ADD PRIMARY KEY ("ICON_ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM"  ENABLE;
  ALTER TABLE "MAGNA"."ICON_TBL" MODIFY ("ICON_ID" NOT NULL ENABLE);
